#include "CCS/UIInterface.h"
#include "CCS/LogicInterface.h"
#include <windows.h>         // MFC core and standard components
#include "Header.h"

__declspec(dllexport) char __stdcall CUI_WaitDisplayACK()
{
	return CUI_WaitDisplayACKDlg();
}

__declspec(dllexport) void __stdcall CUI_SendDisplayData(const char* pBuf, int length)
{
	int n = 0;
	for ( ;n < length; n++)
	{
		if (0x0AA == (UCHAR)pBuf[n])
		{
			CUI_SendDisplayDataDlg(pBuf + n, 1);
		}
		CUI_SendDisplayDataDlg(pBuf + n, 1);
	}
}

__declspec(dllexport) void __stdcall CUI_StartDisplayData(char chStartSeq)
{
	CUI_SendDisplayDataDlg(&chStartSeq, 1);
}

__declspec(dllexport) void __stdcall CUI_GetTextExtent(uint16_t* punX, uint16_t* punY)
{
	CUI_GetTextExtentDlg(punX, punY);
}

/** **********************************************************************
 *
 * Shows error message on the screen
 * @param error text to show
 *
 *********************************************************************** */
__declspec(dllexport) void __stdcall CUI_SetLastError(char* pszError)
{
	UI_ShowError(pszError);
}

extern void ReportError(const char *err)
{
	UI_ShowError(err);
}

HANDLE handlePort = NULL; 
__declspec(dllexport) void __stdcall CUI_SendDisplayData_COM(const char* pBuf, int length)
{
	DWORD dwWritelen = length;

	if (!handlePort)
	{
		DCB config;
		handlePort	= CreateFile(TEXT("COM4"),  // Specify port device: default "COM1"
		GENERIC_READ | GENERIC_WRITE,       // Specify mode that open device.
		0,                                  // the devide isn't shared.
		NULL,                               // the object gets a default security.
		OPEN_EXISTING,                      // Specify which action to take on file. 
		0,                                  // default.
		NULL);                              // default.

		if (GetCommState(handlePort, &config) != 0)
		{
			config.BaudRate = 115200;

			if (SetCommState(handlePort, &config) == 0)
			{
				int BreakPointPlace = 0;
			}
		}
		else
		{
			int BreakPointPlace = 0;
		}
	}

	if (WriteFile(handlePort,   // handle to file to write to
		pBuf,              // pointer to data to write to file
		dwWritelen,              // number of bytes to write
		&dwWritelen,NULL) == 0)      // pointer to number of bytes written
	{
		int BreakPointPlace = 0;
	}

	//CloseHandle(handlePort_)
}

__declspec(dllexport) const char* __stdcall CUI_PostInit(INITSTEP eInitStep, void* pCtx)
{
	const char* pszRet = NULL;
	if (INITSTEP_AARM == eInitStep)
	{
		pszRet = Logic_PostInit();
	}

	return pszRet;
}