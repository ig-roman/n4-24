#include "AARM/ArmInterface.h"
#include "Header.h"
#include "../LogicInterface.h"
#include "../UIInterface.h"
#include "CommonCUI.h"

AERROR g_unError = AERROR_NO_ERROR;

/** **********************************************************************
 *
 * Shows error message on the screen
 * @param error text to show
 *
 *********************************************************************** */
__declspec(dllexport) void __stdcall CARM_SetLastError(AERROR unError)
{
	g_unError = unError;
}

__declspec(dllexport) AERROR __stdcall CARM_ClearLastError()
{
	AERROR unError = g_unError;
	g_unError = AERROR_NO_ERROR;
	return unError;
}

__declspec(dllexport) void __stdcall CARM_SendCommandString(const char* pBuf, unsigned int length)
{
	// Skip responces from virtal AARM when software is connected to real device.
	if (!handlePortArm)
	{
		Logic_HandleControlData(pBuf, length);
	}
}

__declspec(dllexport) bool __stdcall CARM_GetOverloadStatus()
{
	UCHAR nState = (UCHAR)GetKeyState(VK_SHIFT);
	return (nState & 0x80) > 0;
}

__declspec(dllexport) void __stdcall CARM_LLProg_Status(char bStart) {}


__declspec(dllexport) void __stdcall CARM_SPI_Start(unsigned char unAddr)
{
	CARM_SPI_StartDlg(unAddr);
}

__declspec(dllexport) void __stdcall CARM_SPI_Send(char byByte)
{
	CARM_SPI_SendDlg(byByte);
}

__declspec(dllexport) void __stdcall CARM_SPI_Stop()
{
	CARM_SPI_StopDlg();
}

__declspec(dllexport) void __stdcall CARM_SetPWMValue(unsigned long unPwm)
{
	CARM_SetPWMValueDlg(unPwm);
}

__declspec(dllexport) bool __stdcall CARM_DelayMs(unsigned long unDelay, bool bSkipLoop, bool bCheckOverload)
{
	return CARM_DelayMsDlg(unDelay, bSkipLoop, bCheckOverload);
}
