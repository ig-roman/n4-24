#include "CommonTypes.h"

typedef enum { ACKDISP_NONE, ACKDISP_WAIT, ACKDISP_OK, ACKDISP_FAILED } ACKDISP;

typedef struct tagUI_FEEDBACK_DATA
{
	PPOINT_T pUITouchPoint;
	PPOINT_T pUIReleasePoint;
	int8_t unKeyCodePush;
	int8_t unKeyCodeRelease;
	ACKDISP UIACKCode;
	ACKDISP	eTextExtened;
	POINT_T UITextSize;
} UI_FEEDBACK_DATA, *PUI_FEEDBACK_DATA;

extern UI_FEEDBACK_DATA gs_UIFeedbackData;

void ManageSerial();
bool SCP_ReadPortData(PORTNAME ePort, char* pBuf, unsigned int* pnBuffDataLen);
bool SCP_ReadPortDataCustom(PORTNAME ePort, char* pBuf, unsigned int* pnBuffDataLen, uint32_t unWaitPeriod, uint32_t unBytePeriod, bool bDetectNewLines);
void RouteAndSendData(PORTNAME ePort, const char* pBuf, unsigned int nBuffDataLen);
void RouteAndSendRemoteData(const char* pBuf, unsigned int nBuffDataLen);
