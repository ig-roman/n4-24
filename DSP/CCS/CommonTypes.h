#ifndef COMMONCT_H_
#define COMMONCT_H_

#if defined(WIN32) || defined(DOCKER)
	#include "../TopCommon.h"
#else
	#include "TopCommon.h"
#endif


#define NULL 0

// 16 because XPort has same password and XPort allows 16 characters but configuration API supports only 15 digits
#define PAS_MAX_LEN 15

#define EEPROM_ADDR_CONF_CRC 0UL
#define EEPROM_ADDR_CONF 4UL

#define EEPROM_ADDR_CALIBRATION 512UL			// calibration size at 2016.11.15 is 8944(1118 doubles (1 double = 8 bytes here)) bytes
#define EEPROM_ADDR_FLAGS_DC	(EEPROM_ADDR_CALIBRATION - 2)
#define EEPROM_ADDR_FLAGS_AC	(EEPROM_ADDR_CALIBRATION - 3)
#define EEPROM_SIZE_DCU_POINTS	30
#define EEPROM_ADDR_DCU_START	EEPROM_ADDR_CALIBRATION
#define EEPROM_ADDR_DCU_END		(EEPROM_ADDR_DCU_START + EEPROM_SIZE_DCU_POINTS * EXBAND_DCU_MAX_COUNT * sizeof(double))
#define EEPROM_SIZE_ACU_POINTS	12
#define EEPROM_SIZE_ACU_FREQS	21
#define EEPROM_ADDR_ACU_REAL_START	4608		// 512 * 9
#ifdef USE_ACU_1MV
#define EEPROM_ADDR_ACU_START	EEPROM_ADDR_ACU_REAL_START
#else
#define EEPROM_ADDR_ACU_START	(EEPROM_ADDR_ACU_REAL_START + EEPROM_SIZE_ACU_POINTS * EEPROM_SIZE_ACU_FREQS * sizeof(double))
#endif
#define EEPROM_ADDR_ACU_END		(EEPROM_ADDR_ACU_START + EEPROM_SIZE_ACU_POINTS * EEPROM_SIZE_ACU_FREQS * EXBAND_ACU_MAX_COUNT * sizeof(double))
#define EEPROM_SIZE_DCI_POINTS	18
#define EEPROM_ADDR_DCI_START	20992UL			// 512 * 41
#define EEPROM_ADDR_DCI_END		(EEPROM_ADDR_DCI_START + EEPROM_SIZE_DCI_POINTS * EXTBAND_DCI_MAX_COUNT * sizeof(double))
#define EEPROM_SIZE_ACI_POINTS	18
#define EEPROM_SIZE_ACI_FREQS	12
#define EEPROM_ADDR_ACI_START	24576UL			// 512 * 48
#define EEPROM_ADDR_ACI_END		(EEPROM_ADDR_ACI_START + EEPROM_SIZE_ACI_POINTS * EEPROM_SIZE_ACI_FREQS * EXTBAND_ACI_MAX_COUNT * sizeof(double))
#define EEPROM_ADDR_R_START		45568UL			// 512 * 89
#define EEPROM_ADDR_R_END		(EEPROM_ADDR_R_START + EXTBAND_R_MAX_COUNT * 2 * sizeof(double))

#define EEPROM_ADDR_AUTO_MODES	65536UL			// End of this section is EEPROM_ADDRESS_AUTO_MODES + (MAX_GROUPS_COUNT * 20)
#define EEPROM_FILE_SIZE 131072UL
// Max eeprom size is 2Mbit = 250000bytes
#define EEPROM_ADDR_END 249999UL

#define MAX_BACKLIGHT_INTENSITY			15
#define MIN_BACKLIGHT_INTENSITY			0

#define DEFAULT_LAB_DATE				"01012000"

#define DEFAULT_PASSWORD				"12345"
#define DEFAULT_CALIBRATION_PASSWORD	"54321"

#define DEFAULT_GPIB_ADDR				22

#define MAX_HV_THRESHOLD_THRESHOLD_VAL	5
#define DEFAULT_HV_SAFETY_VALUE			95
#define DEFAULT_HV_SAFETY_VALUE_UBOUND (DEFAULT_HV_SAFETY_VALUE + MAX_HV_THRESHOLD_THRESHOLD_VAL)

#define DEFAULT_SCPI_RAW_PORT			5025
#define DEFAULT_SCPI_MASK				8

#define DEFAULT_RS232_STOP				1
#define DEFAULT_RS232_PAR				RS232PAR_NONE
#define DEFAULT_RS232_SPEED				9600

#define DEFAULT_LAB						0

#define INITSTEP_DEFS \
	INITSTEP_DEF(INITSTEP_UART	, MSG_INIT_SERIAL),	\
	INITSTEP_DEF(INITSTEP_GPIB	, MSG_INIT_GPIB),	\
	INITSTEP_DEF(INITSTEP_USB	, MSG_INIT_USB),	\
	INITSTEP_DEF(INITSTEP_KBD	, MSG_INIT_KBD),	\
	INITSTEP_DEF(INITSTEP_XPORT	, MSG_INIT_LAN),	\
	INITSTEP_DEF(INITSTEP_AARM	, MSG_INIT_AARM),	\
	INITSTEP_DEF(INITSTEP_END	, MSG_NONE)

#define INITSTEP_DEF( eVal, msg)  eVal
typedef enum { INITSTEP_DEFS } INITSTEP;
#undef INITSTEP_DEF

typedef struct tagLAN_CONF
{
	uint8_t unIP1, unIP2, unIP3, unIP4;
	uint8_t unMask;
	uint16_t unPort;
	char bForceUpdate; // char because in C++ it one byte and in C it 4 bytes
} LAN_CONF, *PLAN_CONF;

typedef struct tagLAB_CONF
{
	int16_t nTemp;
	uint16_t unTempErr, unHum, unHumErr;
	char szDate[9];
} LAB_CONF, *PLAB_CONF;

typedef enum { RS232PAR_EVEN, RS232PAR_ODD, RS232PAR_ONE, RS232PAR_ZERO, RS232PAR_NONE, RS232PAR_MAX } RS232PAR;

typedef struct tagRS232_CONF
{
	uint32_t unSpeed;
	RS232PAR eParity;
	uint8_t unStopBits;
} RS232_CONF, *PRS232_CONF;

#ifdef __cplusplus
extern "C" {
#endif

int16_t ParseFirstDigit(const char **ppszText);

#ifdef __cplusplus
}
#endif

typedef struct tagPOINT_T
{
	uint16_t	X;
	uint16_t	Y;
} POINT_T, *PPOINT_T;

#endif
