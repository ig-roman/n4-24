/*
 * Common.c
 */

#include "Common.h"
#include "Drivers/panel.h"
#include <stdio.h>

volatile uint32_t g_ulMilis = 0;

/** **********************************************************************
 *
 * Periodically returns true for defined period without system blocking.
 *
 * @param unPeriod			constant value of period
 * @param punTriggerTime	new time in future for triggering. If punTriggerTime is 0 function sets new time and returns true.
 *
 * @return true if punTriggerTime equals or less than actual time
 *
 *********************************************************************** */
bool CheckUpdateTimeForLoop(uint32_t unPeriod, uint32_t* punTriggerTime)
{
	// ST TODO: TIVA MIGRATION
	//KickDog();
	ManageBuzzer();

	return CheckUpdateTime(unPeriod, punTriggerTime);
}

void SetUpdateTime(uint32_t unPeriod, uint32_t* punTriggerTime)
{
	uint32_t unNewTime = GetTime();
	*punTriggerTime = unNewTime + unPeriod;
}

void DelayMS(uint32_t unTime)
{
	uint32_t unNextTime;
	SetUpdateTime(unTime, &unNextTime);
	while (!CheckUpdateTimeForLoop(unTime, &unNextTime));
}

uint32_t g_unBeepTime = 0, g_unQuietTime = 0, g_unBeepTimeNext = 0, g_unQuietTimeNext = 0;
void Beep(uint32_t unBeepTime, uint32_t unQuietTime)
{
	if (unBeepTime)
	{
		g_unBeepTimeNext = g_ulMilis + unBeepTime;
	}
	g_unBeepTime = unBeepTime;
	g_unQuietTime = unQuietTime;
}

/** **********************************************************************
 *
 * @return period in ms when system has been started
 *
 *********************************************************************** */
uint32_t GetTime()
{
	uint32_t ulTimeRet = g_ulMilis;

	while (ulTimeRet != g_ulMilis)
	{
		ulTimeRet = g_ulMilis;
	}

	return ulTimeRet;
}

void ManageBuzzer()
{
	if (g_unBeepTimeNext && g_unBeepTimeNext >= g_ulMilis)
	{
		// Buzzer on
		PANEL_SetBuzzerState(true);
	}
	else if (g_unBeepTimeNext && g_unBeepTimeNext < g_ulMilis || 0 == g_unBeepTime)
	{
		// Buzzer off
		PANEL_SetBuzzerState(false);
		g_unBeepTimeNext = 0;
		if (g_unQuietTime)
		{
			g_unQuietTimeNext = g_ulMilis + g_unQuietTime;
		}
	}
	else if (g_unQuietTimeNext && g_unQuietTimeNext < g_ulMilis)
	{
		g_unQuietTimeNext = 0;
		Beep(g_unBeepTime, g_unQuietTime);
	}
}

void KeyBeep()
{
	if (!g_unBeepTime || (g_unBeepTime && g_unQuietTimeNext))
	{
		// Buzzer on
		PANEL_SetBuzzerState(true);

		DelayMS(BEEP_SHORT);

		// Buzzer off
		PANEL_SetBuzzerState(false);
	}
}

void SysTickIntHandler(void)
{
	g_ulMilis++;
}
