#include "LogicInterface.h"
#include "SerialCmdParser.h"
#include <string.h>
#include <stdlib.h>
#include "Drivers/eeprom.h"
#include "Drivers/panel.h"
#include "Drivers/keyboard.h"
#include "UI/Pages/UILocalization.h"

#ifndef WIN32
	#include "Common.h"
#endif

uint32_t g_BlinkPeriod = (BEEP_DELAY_HV + BEEP_PERIOD_HV_BEFORE) * 2;
char g_bHVConfirmRequire = 0;
bool gs_bHVBeepMute = false;
uint32_t g_bHVLedOnTime = 0;
uint32_t g_bHVLedOffTime = 0;
char g_HVBuzzerSuppress = 0;

void CLogic_Loop()
{
	if (g_bHVConfirmRequire)
	{
		if (CheckUpdateTime(g_BlinkPeriod, &g_bHVLedOnTime))
		{
			PANEL_SetLedState(PANELLED_ON);
		}

		if (CheckUpdateTime(g_BlinkPeriod, &g_bHVLedOffTime))
		{
			PANEL_SetLedState(PANELLED_HOLD);
		}
	}
}

void CLogic_StoreByte(uint32_t unAddress, uint8_t un8Data)
{
	EEPROM_writeBytes(unAddress, 1, &un8Data);
}

uint8_t CLogic_RestoreByte(uint32_t unAddress)
{
	uint8_t un8Data = 0;
	EEPROM_readBytes(unAddress, 1, &un8Data);
	return un8Data;
}

void CLogic_StoreBytes(uint32_t unAddress, uint32_t unLength, const uint8_t* punData)
{
	EEPROM_writeBytes(unAddress, unLength, punData);
}

void CLogic_RestoreBytes(uint32_t unAddress, uint32_t unLength, uint8_t* punData)
{
	EEPROM_readBytes(unAddress, unLength, punData);
}

void SetOnLedBlinking(bool bOn)
{
	if (bOn)
	{
		SetUpdateTime(g_BlinkPeriod >> 1, &g_bHVLedOnTime);
		SetUpdateTime(0, &g_bHVLedOffTime);

		// Update led immediately
		CLogic_Loop();
	}
	g_bHVConfirmRequire = bOn;
}

bool CLogic_SwitchOut(char bOn, char bHV, char bHVConfirmRequire, char bForce)
{
	bool bCancel = false;
	if (bOn)
	{
		if (bHVConfirmRequire)
		{
			SetOnLedBlinking(true);
		}
		else
		{
			if (g_bHVConfirmRequire)
			{
				if (bHV)
				{
					// To synchronize led and buzzer
					SetUpdateTime(g_BlinkPeriod >> 1, &g_bHVLedOffTime);
					SetUpdateTime(0, &g_bHVLedOnTime);

					if (!g_HVBuzzerSuppress)
						Beep(BEEP_DELAY_HV, BEEP_PERIOD_HV_BEFORE);

					uint32_t unNextTime;
					SetUpdateTime(BEEP_TIME_HV_BEFORE, &unNextTime);
					while (!bForce && !bCancel && !CheckUpdateTimeForLoop(BEEP_TIME_HV_BEFORE, &unNextTime))
					{
						// Allows led blinking
						CLogic_Loop();

						int8_t nKeyCode = 0;
						uint8_t	bPressed = 0;
						bCancel = ManageKeyboard(&nKeyCode, &bPressed) && bPressed;
						if (bCancel)
						{
							bOn = false;
						}
					}
				}
				SetOnLedBlinking(false);
			}

			if (bHV)
			{
				// It start working when UI finishes work.
				if (!g_HVBuzzerSuppress)
					Beep(BEEP_DELAY_HV, BEEP_PERIOD_HV_NORMAL);
			}
		}

		PANEL_SetLedState(bOn ? PANELLED_ON : PANELLED_OFF);
	}
	else if (bHVConfirmRequire)
	{
		SetOnLedBlinking(true);
	}
	else
	{
		SetOnLedBlinking(false);
		PANEL_SetLedState(PANELLED_OFF);
	}

	if (bOn && bHV)
	{
		if (!bHVConfirmRequire && !g_bHVConfirmRequire)
		{
			if (gs_bHVBeepMute)
			{
				// Disable buzzer
				Beep(0, BEEP_PERIOD_NO_REPEAT);
				gs_bHVBeepMute = false;
			}
			else
			{
				gs_bHVBeepMute = true;
			}
		}
	}
	else
	{
		// Disable buzzer
		Beep(0, BEEP_PERIOD_NO_REPEAT);
		gs_bHVBeepMute = false;
	}
	return bCancel;
}

/** **********************************************************************
 * Sends control data to the analogue part (AARM) of device
 * After data package function sends CRC 8 checksum
 *
 * @param pBuf			data to send
 * @param length		length of buffer
 *
 *********************************************************************** */
__declspec(dllexport) void __stdcall CLogic_SendControlData(const char* pBuf, int length)
{
	RouteAndSendData(PORTNAME_AARM, pBuf, length);
	uint8_t unCrc = countCrc8((const uint8_t*)pBuf, length, 0);
	unCrc = countCrc8((const uint8_t*)STR_SW_VERSION, strlen(STR_SW_VERSION), unCrc);
	RouteAndSendData(PORTNAME_AARM, (const char *)&unCrc, 1);
}

/** **********************************************************************
 * Sends control data to the analogue part (AARM) of device
 * After data package function sends CRC 8 checksum
 *
 * @param pBuf			data to send
 * @param length		length of buffer
 *
 *********************************************************************** */
__declspec(dllexport) void __stdcall CLogic_SendRemoteCtrlData(const char* pBuf, int length)
{
	RouteAndSendRemoteData(pBuf, length);
}

/** **********************************************************************
 *
 * Provides error message from logic part
 * @param error text to log
 *
 *********************************************************************** */
__declspec(dllexport) void __stdcall CLogic_SetLastError(const char* pszErrorText)
{
	ReportError(pszErrorText);
}

__declspec(dllexport) void __stdcall CLogic_ManageSerial()
{
	ManageSerial();
}
