#include "qei.h"
#include "driverlib/qei.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"

#define QEI_POSITION_MAX 3999
#define QEI_POSITION_MID (QEI_POSITION_MAX / 2)

#define QEI_BASE QEI1_BASE
#define SYSCTL_PERIPH_GPIO SYSCTL_PERIPH_GPIOG
#define SYSCTL_PERIPH_QEI SYSCTL_PERIPH_QEI1
#ifndef LAUNCHPAD
#define GPIO_PHB GPIO_PG1_PHB1
#define GPIO_PHA GPIO_PG0_PHA1
#define GPIO_PORT_BASE GPIO_PORTG_BASE
#define QEI_GPIO_PINS (GPIO_PIN_0 | GPIO_PIN_1)
#else
#define GPIO_PHB GPIO_PC6_PHB1
#define GPIO_PHA GPIO_PC5_PHA1
#define GPIO_PORT_BASE GPIO_PORTC_BASE
#define QEI_GPIO_PINS (GPIO_PIN_5 | GPIO_PIN_6)
#endif

void QEI_Init()
{
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIO);
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_QEI);


	// Configure IO
	MAP_GPIOPinConfigure(GPIO_PHB);
	MAP_GPIOPinConfigure(GPIO_PHA);

	// Configure QEI pins
	MAP_GPIOPinTypeQEI(GPIO_PORT_BASE, QEI_GPIO_PINS);

	// Configure the quadrature encoder to capture edges on both signals and
	// maintain an absolute position by resetting on index pulses. Using a
	// 1000 line encoder at four edges per line, there are 4000 pulses per
	// revolution; therefore set the maximum position to 3999 as the count
	// is zero based.
	MAP_QEIConfigure(QEI_BASE, (QEI_CONFIG_CAPTURE_A_B | QEI_CONFIG_RESET_IDX | QEI_CONFIG_QUADRATURE | QEI_CONFIG_NO_SWAP), QEI_POSITION_MAX);

	// Enable the quadrature encoder.
	MAP_QEIEnable(QEI_BASE);

	// Set default position
	MAP_QEIPositionSet(QEI_BASE, QEI_POSITION_MID);

}

/** **********************************************************************
 *
 * Read the encoder position and sets internal counter to default value.
 * @return position
 *
 *********************************************************************** */
int32_t QEI_GetAndResetPosition()
{
	uint32_t unNewPos = MAP_QEIPositionGet(QEI_BASE);

	// Set default position
	MAP_QEIPositionSet(QEI_BASE, QEI_POSITION_MID);

	return (int32_t)unNewPos - QEI_POSITION_MID;
}
