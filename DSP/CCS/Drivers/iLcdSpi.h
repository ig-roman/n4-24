#ifndef ILCDSPI_H_
#define ILCDSPI_H_

#include "TopCommon.h"

void ILCDSPI_Init();
void ILCDSPI_SendCmd(const uint8_t *unILcdCmd, uint32_t unCount);
int ILCDSPI_GetCmd(char *mem, int count, bool bMoveReadIdx);

#endif /* SPI_H_ */
