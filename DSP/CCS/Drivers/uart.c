#include "uart.h"

#include "driverlib/uart.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/systick.h"
#include "driverlib/interrupt.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"

#include "circ_buf.h"

#define UART_SPEED_DEFAULT 9600
#define UART_SPEED_AARM 115200

#define UART0_RX_BUFFER_SIZE     INST_INPUT_BUFFER_MAX_SIZE
#define UART0_TX_BUFFER_SIZE     INST_OUTPUT_BUFFER_MAX_SIZE

#define UART1_RX_BUFFER_SIZE     INST_INPUT_BUFFER_MAX_SIZE
#define UART1_TX_BUFFER_SIZE     INST_OUTPUT_BUFFER_MAX_SIZE

#define UART7_RX_BUFFER_SIZE     64
#define UART7_TX_BUFFER_SIZE     128

char UART0RxBuf[UART0_RX_BUFFER_SIZE];
char UART0TxBuf[UART0_TX_BUFFER_SIZE];

char UART1RxBuf[UART1_RX_BUFFER_SIZE];
char UART1TxBuf[UART1_TX_BUFFER_SIZE];

char UART7RxBuf[UART7_RX_BUFFER_SIZE];
char UART7TxBuf[UART7_TX_BUFFER_SIZE];

bool s_bIsUartInitialized = false;

typedef struct _SCI_bufs
{
	CIRC_BUF rxCBuf;
	CIRC_BUF txCBuf;
} SCI_bufs;

SCI_bufs UART0CircBufs;
SCI_bufs UART1CircBufs;
SCI_bufs UART7CircBufs;

void ConfigurePeripheral(void);
void UART0IntHandler(void);
void UART1IntHandler(void);
void UART7IntHandler(void);
void UARTIntHandler(uint32_t ui32Base);
void UARTPrimeTransmit(uint32_t ui32Base);
SCI_bufs* GetPortBuff(uint32_t ui32Base);

uint32_t ConvertParity(RS232PAR ePar)
{
	uint32_t ui32ConfigPar = UART_CONFIG_PAR_NONE;

	switch (ePar)
	{
		case RS232PAR_EVEN:	ui32ConfigPar = UART_CONFIG_PAR_EVEN; break;
		case RS232PAR_ODD:	ui32ConfigPar = UART_CONFIG_PAR_ODD; break;
		case RS232PAR_ONE:	ui32ConfigPar = UART_CONFIG_PAR_ONE; break;
		case RS232PAR_ZERO: ui32ConfigPar = UART_CONFIG_PAR_ZERO; break;
	}

	return ui32ConfigPar;
}

const char* UART_PostInit(PRS232_CONF pRs232Conf)
{
    //
    // UART0 configuration PORTNAME_REAR_COM
	//
	CIRC_BUF_init(&UART0CircBufs.rxCBuf, UART0RxBuf, UART0_RX_BUFFER_SIZE);
	CIRC_BUF_init(&UART0CircBufs.txCBuf, UART0TxBuf, UART0_TX_BUFFER_SIZE);
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA); // Enable the GPIO Peripheral used by the UART.
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0); // Enable UART
    MAP_GPIOPinConfigure(GPIO_PA0_U0RX); // Configure GPIO Pins for UART mode.
    MAP_GPIOPinConfigure(GPIO_PA1_U0TX); // Configure GPIO Pins for UART mode.
    MAP_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1); // Configure GPIO Pins for UART mode.

    // MAP_UARTConfigSetExpClk(UART0_BASE, MAP_SysCtlClockGet(), UART_SPEED_DEFAULT, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)); // Configure the UART for 115200, 8-N-1 operation.
	MAP_UARTConfigSetExpClk(UART0_BASE, MAP_SysCtlClockGet(),
			pRs232Conf->unSpeed,
			(UART_CONFIG_WLEN_8 |
				(2 == pRs232Conf->unStopBits ? UART_CONFIG_STOP_TWO : UART_CONFIG_STOP_ONE) |
				ConvertParity(pRs232Conf->eParity)));

	UARTIntRegister(UART0_BASE, UART0IntHandler);
	MAP_IntEnable(INT_UART0); // Enable the UART interrupt.
	MAP_UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT); // Enable the UART interrupt.

	if (!s_bIsUartInitialized)
	{
		//
		// UART1 configuration PORTNAME_XPORT
		//
		CIRC_BUF_init(&UART1CircBufs.rxCBuf, UART1RxBuf, UART1_RX_BUFFER_SIZE);
		CIRC_BUF_init(&UART1CircBufs.txCBuf, UART1TxBuf, UART1_TX_BUFFER_SIZE);
		MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC); // Enable the GPIO Peripheral used by the UART.
		MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1); // Enable UART
		MAP_GPIOPinConfigure(GPIO_PC4_U1RX); // Configure GPIO Pins for UART mode.
		MAP_GPIOPinConfigure(GPIO_PC5_U1TX); // Configure GPIO Pins for UART mode.
		MAP_GPIOPinTypeUART(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5); // Configure GPIO Pins for UART mode.
		MAP_UARTConfigSetExpClk(UART1_BASE, MAP_SysCtlClockGet(), UART_SPEED_DEFAULT, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)); // Configure the UART for 115200, 8-N-1 operation.
		UARTIntRegister(UART1_BASE, UART1IntHandler);
		MAP_IntEnable(INT_UART1); // Enable the UART interrupt.
		MAP_UARTIntEnable(UART1_BASE, UART_INT_RX | UART_INT_RT); // Enable the UART interrupt.
		//
		// UART7 configuration PORTNAME_AARM
		//
		CIRC_BUF_init(&UART7CircBufs.rxCBuf, UART7RxBuf, UART7_RX_BUFFER_SIZE);
		CIRC_BUF_init(&UART7CircBufs.txCBuf, UART7TxBuf, UART7_TX_BUFFER_SIZE);
		MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE); // Enable the GPIO Peripheral used by the UART.
		MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART7); // Enable UART
		MAP_GPIOPinConfigure(GPIO_PE0_U7RX); // Configure GPIO Pins for UART mode.
		MAP_GPIOPinConfigure(GPIO_PE1_U7TX); // Configure GPIO Pins for UART mode.
		MAP_GPIOPinTypeUART(GPIO_PORTE_BASE, GPIO_PIN_0 | GPIO_PIN_1); // Configure GPIO Pins for UART mode.
		MAP_UARTConfigSetExpClk(UART7_BASE, MAP_SysCtlClockGet(), UART_SPEED_AARM, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)); // Configure the UART for 115200, 8-N-1 operation.
		UARTIntRegister(UART7_BASE, UART7IntHandler);
		MAP_IntEnable(INT_UART7); // Enable the UART interrupt.
		MAP_UARTIntEnable(UART7_BASE, UART_INT_RX | UART_INT_RT); // Enable the UART interrupt.

		s_bIsUartInitialized = true;
	}

	return NULL;
}

void UART0IntHandler(void) { UARTIntHandler(UART0_BASE); }
void UART1IntHandler(void) { UARTIntHandler(UART1_BASE); }
void UART7IntHandler(void) { UARTIntHandler(UART7_BASE); }

uint32_t GetPortBase(PORTNAME uartPort)
{
	switch (uartPort)
	{
		case PORTNAME_AARM: return UART7_BASE;
		case PORTNAME_XPORT: return UART1_BASE;
		case PORTNAME_REAR_COM: return UART0_BASE;
	}
	return 0;
}


uint32_t GetPortInt(uint32_t ui32Base)
{
	switch (ui32Base)
	{
		case UART0_BASE: return INT_UART0;
		case UART1_BASE: return INT_UART1;
		case UART7_BASE: return INT_UART7;
	}
	return 0;
}

SCI_bufs* GetPortBuff(uint32_t ui32Base)
{
	switch (ui32Base)
	{
		case UART0_BASE: return &UART0CircBufs;
		case UART1_BASE: return &UART1CircBufs;
		case UART7_BASE: return &UART7CircBufs;
	}

	return NULL;
}

//*****************************************************************************
//
// The UART interrupt handler.
//
//*****************************************************************************
void UARTIntHandler(uint32_t ui32Base)
{
	SCI_bufs* pUARTCircBufs = GetPortBuff(ui32Base);

    // Get the interrrupt status.
	uint32_t ui32Status = MAP_UARTIntStatus(ui32Base, true);

    // Clear the asserted interrupts.
    MAP_UARTIntClear(ui32Base, ui32Status);

    if(ui32Status & UART_INT_TX)
    {
    	if(!CIRC_BUF_ISEMPTY(&pUARTCircBufs->txCBuf))
    	{
    		UARTPrimeTransmit(ui32Base);
		}

		// If the output buffer is empty, turn off the transmit interrupt.
		if(CIRC_BUF_ISEMPTY(&pUARTCircBufs->txCBuf))
		{
			MAP_UARTIntDisable(ui32Base, UART_INT_TX);
		}
    }

	// Are we being interrupted due to a received character?
	//
	if(ui32Status & (UART_INT_RX | UART_INT_RT))
	{
		// Loop while there are characters in the receive FIFO.
		while(MAP_UARTCharsAvail(ui32Base))
		{
			// Read a character
			int32_t i32Char = MAP_UARTCharGetNonBlocking(ui32Base);
			char cChar = (char)(i32Char & 0xFF);
#ifndef LAUNCHPAD
			CIRC_BUF_put(&pUARTCircBufs->rxCBuf, &cChar, 1);
#else
			CIRC_BUF_put(&pUARTCircBufs->rxCBuf, &cChar, 0);
#endif
		}
	}
}

//*****************************************************************************
//
// Take as many bytes from the transmit buffer as we have space for and move
// them into the UART transmit FIFO.
//
//*****************************************************************************
void UARTPrimeTransmit(uint32_t ui32Base)
{
	SCI_bufs* pUARTCircBufs = GetPortBuff(ui32Base);

	//
	// Do we have any data to transmit?
	//
	if(!CIRC_BUF_ISEMPTY(&pUARTCircBufs->txCBuf))
	{
		uint32_t ui32PortInt = GetPortInt(ui32Base);

		// Disable the UART interrupt.  If we don't do this there is a race
		// condition which can cause the read index to be corrupted.
		MAP_IntDisable(ui32PortInt);

		// Yes - take some characters out of the transmit buffer and feed them to the UART transmit FIFO.
		char chByte = NULL;
		while(MAP_UARTSpaceAvail(ui32Base) && CIRC_BUF_get(&pUARTCircBufs->txCBuf, &chByte, 1, true))
		{
			MAP_UARTCharPutNonBlocking(ui32Base, chByte);
		}

		// Reenable the UART interrupt.
		MAP_IntEnable(ui32PortInt);
    }
}

int UARTDataGetBuffered(PORTNAME uartPort, char *mem, int count, bool bMoveReadIdx)
{
	int nRet = 0;
	if (s_bIsUartInitialized)
	{
		uint32_t ui32Base = GetPortBase(uartPort);
		SCI_bufs* pUARTCircBufs = GetPortBuff(ui32Base);
		uint32_t ui32PortInt = GetPortInt(ui32Base);

		// Disable the UART interrupt.  If we don't do this there is a race
		// condition which can cause the read index to be corrupted.
		MAP_IntDisable(ui32PortInt);

		nRet = CIRC_BUF_get(&(pUARTCircBufs->rxCBuf), mem, count, bMoveReadIdx);

		// Reenable the UART interrupt.
		MAP_IntEnable(ui32PortInt);
	}

	return nRet;
}

void UARTDataPutBuffered(PORTNAME uartPort, const char *mem, int count)
{
	if (s_bIsUartInitialized)
	{
		uint32_t ui32Base = GetPortBase(uartPort);
		SCI_bufs* pUARTCircBufs = GetPortBuff(ui32Base);
		CIRC_BUF_put(&(pUARTCircBufs->txCBuf), mem, count);

		if(!CIRC_BUF_ISEMPTY(&(pUARTCircBufs->txCBuf)))
		{
			// Set up UART to transmit data.
			UARTPrimeTransmit(ui32Base);
			MAP_UARTIntEnable(ui32Base, UART_INT_TX);
		}
	}
}
