#ifndef TA9554_H_
#define TA9554_H_

#include "../Common.h"

bool TA9554_CfgPinsAs(uint8_t unI2cAddr, bool bInput);
uint8_t TA9554_PortGet(uint8_t unI2cAddr);
void TA9554_PortSet(uint8_t unI2cAddr, uint8_t unData);

#endif /* TA9554_H_ */
