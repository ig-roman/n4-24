#ifndef UART_H_
#define UART_H_

#include "../CommonTypes.h"

const char* UART_PostInit(PRS232_CONF pRs232Conf);
void UARTDataPutBuffered(PORTNAME, const char *mem, int count);
int UARTDataGetBuffered(PORTNAME, char *mem, int count, bool bMoveReadIdx);

#endif /* UART_H_ */
