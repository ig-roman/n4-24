#ifndef I2C_H_
#define I2C_H_

#include "TopCommon.h"

#define KEYS_DEF \
    KEY_DEF( IS2PORT_KBD, 		&i2cConfKBD),  \
    KEY_DEF( IS2PORT_EEPROM, 	&i2cConfEEPROM )

#define KEY_DEF( port, conf )  port
typedef enum  { KEYS_DEF } IS2PORT;
#undef KEY_DEF

void I2C_Init(IS2PORT eI2CPort);
uint8_t I2C_ReadByte(IS2PORT eI2CPort, uint8_t unAddr);
void I2C_WriteByte(IS2PORT eI2CPort, uint8_t unAddr, const uint8_t unByte);
bool I2C_WriteBytes(IS2PORT eI2CPort, uint8_t unAddr, const uint32_t unDataLen, const uint8_t* punData, bool bSendStopBit, bool bReportACKErrors);


#endif /* I2C_H_ */
