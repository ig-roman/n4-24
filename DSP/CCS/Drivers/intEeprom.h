#ifndef INTEEPROM_H_
#define INTEEPROM_H_

#include "../Common.h"

void INTEEPROM_readBytes(uint32_t unStartAddress, uint32_t unLength, uint8_t* punData);
void INTEEPROM_writeBytes(uint32_t unAddress, uint32_t unLength, const uint8_t* punData);
void INTEEPROM_Init(void);

#endif /* INTEEPROM_H_ */
