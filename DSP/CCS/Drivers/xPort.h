#ifndef XPORT_H_
#define XPORT_H_

#include "../CommonTypes.h"

const char* XPORT_PostInit(PLAN_CONF pConfig, const char* pszPassword);
void XPORT_OnDataRecieved(const char* pBuf, unsigned int nBuffDataLen);
void XPORT_Send(const char* pBuf, unsigned int nBuffDataLen);
void XPORT_HandleInterrupts(void);

#endif /* XPORT_H_ */
