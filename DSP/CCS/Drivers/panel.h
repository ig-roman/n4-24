#ifndef PANEL_H_
#define PANEL_H_

#include "../Common.h"

typedef enum { PANELLED_OFF, PANELLED_ON, PANELLED_HOLD} PANELLED;

const char* PANEL_PostInit();
void PANEL_SetLedState(PANELLED nNewState);
void PANEL_SetBuzzerState(bool bOn);

#endif /* PANEL_H_ */
