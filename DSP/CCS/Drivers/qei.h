#ifndef QEI_H_
#define QEI_H_

#include "../Common.h"

void QEI_Init();
int32_t QEI_GetAndResetPosition();

#endif /* QEI_H_ */
