// For CIO only
#include <stdio.h>

#include "TA9554.h"
#include "qei.h"
#include "keyboard.h"
#include <stdlib.h>

#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "inc/hw_memmap.h"
#include "driverlib/rom.h"

#define BTN_UNUSED_BITS_MASK 0x00FF

#define KBD_SCAN_DELAY 10

#define KBD_REPEAT_DELAY 250
#define KBD_FIRST_REPEAT_DELAY 1000

#define KBD_PIN_INT GPIO_PIN_5
#define KBD_PIN_INT_GPIO_PORT GPIO_PORTB_BASE
#define KBD_PIN_INT_SYSCTL_PERIPH SYSCTL_PERIPH_GPIOB

uint32_t g_unTimeRepeatDelay = 0;

#define KBD_COUNT_KEYS_PER_IC 8

#define KBD_IC_D1_ADDR 0x21
#define KBD_IC_D2_ADDR 0x22
#define KBD_IC_D3_ADDR 0x23
#define KBD_IC_D4_ADDR 0x24
#define KBD_IC_D5_ADDR 0x25

#define ROTARY_DEFAULT_COUNT 0xEFFF

bool s_bIsKbdlInitialized = false;

uint8_t gs_arrKbdICs[] = { KBD_IC_D1_ADDR, KBD_IC_D2_ADDR, KBD_IC_D3_ADDR, KBD_IC_D4_ADDR, KBD_IC_D5_ADDR };

/** **********************************************************************
 *
 * Configures TA9554 ICs as input devices
 *
 *********************************************************************** */
bool KBD_Init()
{
	// Configure INT pin as input
	//MAP_SysCtlPeripheralEnable(KBD_PIN_INT_SYSCTL_PERIPH);
	//MAP_GPIOPinTypeGPIOInput(KBD_PIN_INT_GPIO_PORT, KBD_PIN_INT);

	// Configure all pins for all ICs as input
	uint8_t n = 0;
	s_bIsKbdlInitialized = true;
 	for (; s_bIsKbdlInitialized && n < sizeof(gs_arrKbdICs) / sizeof(uint8_t); n++)
	{
 		s_bIsKbdlInitialized = TA9554_CfgPinsAs(gs_arrKbdICs[n], true);
	}

 	return s_bIsKbdlInitialized;
}

/** **********************************************************************
 * Reads register state from  TA9554 ICs which is connected to keyboard buttons
 *
 * @return key code from keyboard
 *
 *********************************************************************** */
uint8_t ScanKeyboard()
{
	uint8_t unKeyCode = 0;
	uint8_t n = 0, m;

	for (; n < sizeof(gs_arrKbdICs) / sizeof(uint8_t); n++)
	{
		// Read from input register of TA9554 IC
		uint8_t unPinState = ~TA9554_PortGet(gs_arrKbdICs[n]);
		if (unPinState)
		{
			for (m = 0; m < 8; m++)
			{
				uint8_t unBitMask = 1 << m;
				if ((unBitMask & unPinState) > 0)
				{
					unKeyCode = KBD_COUNT_KEYS_PER_IC * n + m;
					unKeyCode++; // Starts from 1
					break;
				}
			}
			break;
		}
	}

	if (0 == unKeyCode)
	{
		//printf("ScanKeyboard() - KeyCode: is null");
	}

	return unKeyCode;
}

/** **********************************************************************
 * Limits scan frequency to KBD_SCAN_DELAY ms
 *
 * @return key code from keyboard or previous code if function is called earlier than KBD_SCAN_DELAY
 *
 *********************************************************************** */
uint8_t ScanKeyboardPeriodically()
{
	uint32_t s_unKbdTimeScan = 0;
	static uint8_t s_unPrevKey = 0;
	if (CheckUpdateTime(KBD_SCAN_DELAY, &s_unKbdTimeScan))
	{
		s_unPrevKey = ScanKeyboard();
	}
	return s_unPrevKey;
}

/** **********************************************************************
 * Simulates periodically repeating of long pressed button
 *
 * @param punKeyCode	key code of pressed button
 * @param pbPressed		is true action id 'pressing' otherwise 'releasing'
 *
 * @return true if key code received from any button of keyboard or RotaryEncoder
 *
 *********************************************************************** */
bool ManageKeyboardDelays(int8_t* punKeyCode, uint8_t* pbPressed)
{
	static int8_t g_nOldKeyCode = 0;
	bool bRet = 0;
	int8_t nNewKeyCode = 0;
#ifndef LAUNCHPAD
	// All scanned key are less than 0 to avoid overlap with ASCII
	nNewKeyCode = -ScanKeyboardPeriodically();
#endif
	if (nNewKeyCode && !g_nOldKeyCode)
	{
		// First press
		SetUpdateTime(KBD_FIRST_REPEAT_DELAY, &g_unTimeRepeatDelay);
		g_nOldKeyCode = nNewKeyCode;
		*punKeyCode = nNewKeyCode;
		*pbPressed = 1;
		bRet = true;
	}
	else if (nNewKeyCode && g_nOldKeyCode)
	{
		// Auto repeat
		if (CheckUpdateTime(KBD_REPEAT_DELAY, &g_unTimeRepeatDelay))
		{
			*punKeyCode = g_nOldKeyCode;
			*pbPressed = 1;
			bRet = true;
		}
	}
	else if (!nNewKeyCode && g_nOldKeyCode)
	{
		// Button released
		bRet = true;
		*pbPressed = 0;
		*punKeyCode = g_nOldKeyCode;
		g_nOldKeyCode = 0;
	}

	return bRet;
}

uint8_t ScanRotaryEncoder(int8_t* punKeyCode)
{
	static int32_t s_nPosBuffer = 0;
	int32_t nNewPos = QEI_GetAndResetPosition();

	s_nPosBuffer += nNewPos;

	int8_t bRet = 0;
	if (0 != s_nPosBuffer)
	{
		if (s_nPosBuffer > 0)
		{
			*punKeyCode = ROTARY_UP_BTN_CODE;
			s_nPosBuffer--;
		}
		else
		{
			*punKeyCode = ROTARY_DOWN_BTN_CODE;
			s_nPosBuffer++;
		}
		bRet = 1;
	}
	return bRet;
}

bool ManageRotaryEncoder(int8_t* punKeyCode, uint8_t* pbPressed)
{
	static int8_t g_nOldRotaryCode = 0;
	bool bRet = 0;
	if (g_nOldRotaryCode)
	{
		*punKeyCode = g_nOldRotaryCode;
		g_nOldRotaryCode = 0;
		pbPressed = 0;
		bRet = 1;
	}
	else
	{
#ifndef LAUNCHPAD
		bRet = ScanRotaryEncoder(punKeyCode);
#endif
		if (bRet)
		{
			*pbPressed = 1;
			g_nOldRotaryCode = *punKeyCode;
		}
	}

	return bRet;
}

int8_t unKeyOldCode = 0, bOldPressed = 0;
uint8_t ManageKeyboardAutoTest(int8_t* punKeyCode, uint8_t* pbPressed)
{
	if (!bOldPressed)
	{
		unKeyOldCode = rand() % 255 - 128;
		*punKeyCode = unKeyOldCode;
		*pbPressed = 1;
		bOldPressed = 1;
	}
	else
	{
		bOldPressed = 0;
		*pbPressed = 0;
		*punKeyCode = unKeyOldCode;
	}
	return 0;
}

/** **********************************************************************
 * Checks for new keyboard or RotaryEncoder events
 *
 * @param punKeyCode	key code of pressed button
 * @param pbPressed		is true action id 'pressing' otherwise 'releasing'
 *
 * @return true if key code received from any button of keyboard or RotaryEncoder
 *
 *********************************************************************** */
bool ManageKeyboard(int8_t* punKeyCode, uint8_t* pbPressed)
{
	bool bRet = false;
	if (s_bIsKbdlInitialized)
	{
		bRet = ManageKeyboardDelays(punKeyCode, pbPressed);

		if (!bRet)
		{
			bRet = ManageRotaryEncoder(punKeyCode, pbPressed);
		}

		if (bRet)
		{
			printf("ManageKeyboard() - KeyCode: '%d',  IsPresed: '%d'\n", *punKeyCode, *pbPressed);
		}
	}
	return bRet;
}
