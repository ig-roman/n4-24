#ifndef EEPROM_H_
#define EEPROM_H_

#include "../Common.h"

void EEPROM_readBytes(uint32_t unStartAddress, uint32_t unLength, uint8_t* punData);
void EEPROM_writeBytes(uint32_t unAddress, uint32_t unLength, const uint8_t* punData);
void EEPROM_Init(void);

#endif /* EEPROM_H_ */
