#ifndef SPI_H_
#define SPI_H_

#include "TopCommon.h"

void SPI_Init(void);
void SPI_Write(const uint8_t *unCmd, uint32_t unCount);
uint8_t SPI_SwapByte(const uint8_t unByte);
void SPI_WriteByteSafe(const uint8_t unByte);
uint8_t SPI_Read();
uint8_t SPI_ReadByte();

#endif /* SPI_H_ */
