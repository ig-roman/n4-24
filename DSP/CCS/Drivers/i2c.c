#include <stdio.h>

#include "../Common.h"
#include "i2c.h"
#include <stdio.h>

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/i2c.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"

typedef struct tagI2CCF
{
	// Defines the I2C peripheral device and port
	const uint32_t ui32Base;
	const uint32_t ui32PeripheralI2C;
	const uint32_t ui32PeripheralSys;
	const uint32_t ui32GPIOPort;

	// Defines the GPIO pin configuration macros for the pins that are used for the I2C function.
	const uint32_t ui32PinConfigSCL;
	const uint32_t ui32PinConfigSDA;
	const uint8_t ui8PinsSCL;
	const uint8_t ui8PinsSDA;
	const bool bIsFast;
} I2CCONF, * PI2CCONF;
typedef const I2CCONF* PCI2CCONF;

const I2CCONF i2cConfKBD	=	{ I2C0_BASE, SYSCTL_PERIPH_I2C0, SYSCTL_PERIPH_GPIOB, GPIO_PORTB_BASE, GPIO_PB2_I2C0SCL, GPIO_PB3_I2C0SDA, GPIO_PIN_2, GPIO_PIN_3, false };
const I2CCONF i2cConfEEPROM	=	{ I2C3_BASE, SYSCTL_PERIPH_I2C3, SYSCTL_PERIPH_GPIOD, GPIO_PORTD_BASE, GPIO_PD0_I2C3SCL, GPIO_PD1_I2C3SDA, GPIO_PIN_0, GPIO_PIN_1, true  };

#define KEY_DEF( port, conf ) { conf }
PCI2CCONF arrPort2Conf[] = { KEYS_DEF };
#undef KEY_DEF

#define I2C_WAIT_TIME_OUT 10


/** **********************************************************************
 *
 * Checks and reports for all available errors.
 *
 * @param I2C_BASE			I2C port
 * @param unAddr			address of slave device
 * @param unErrorCode	error code from I2C api
 *
 * @return Return true on success
 *
 *********************************************************************** */
void ReportI2CError(const uint32_t I2C_BASE, const uint8_t unAddr, const char* pszText)
{
	char szBuff[65];
	sprintf(szBuff, "I2C error - %s\nport: 0x%08x\naddress: 0x%02x", pszText, I2C_BASE, unAddr);
	ReportError(szBuff);

}
/** **********************************************************************
 *
 * Checks and reports for all available errors.
 *
 * @param I2C_BASE			I2C port
 * @param unAddr			address of slave device
 * @param unErrorCode	error code from I2C api
 *
 * @return Return true on success
 *
 *********************************************************************** */
bool CheckForErrors(const uint32_t I2C_BASE, const uint8_t unAddr, uint32_t unErrorCode, bool bReportACKErrors)
{
	bool bRet = I2C_MASTER_ERR_NONE == unErrorCode;
	if (!bRet && bReportACKErrors)
	{
		if (unErrorCode & I2C_MASTER_ERR_ADDR_ACK)
		{
			ReportI2CError(I2C_BASE, unAddr, "no address ACK");
		}
		if (unErrorCode & I2C_MASTER_ERR_DATA_ACK)
		{
			ReportI2CError(I2C_BASE, unAddr, "no data ACK");
		}
		if (unErrorCode & I2C_MASTER_ERR_ARB_LOST)
		{
			ReportI2CError(I2C_BASE, unAddr, "aborted");
		}
	}

	return bRet;
}

/** **********************************************************************
 *
 * Waiting for command completion or error.
 *
 * @param I2C_BASE			I2C port
 * @param unAddr			address of slave device
 * @param bBus 				if value true function waits for buss became accessible otherwise for command completion
 * @param bReportACKErrors	if value true it reports errors otherwise skips them
 *
 * @return Return true on success
 *
 *********************************************************************** */
bool I2C_WaitFor(const uint32_t I2C_BASE, const uint8_t unAddr, bool bBus, bool bReportACKErrors)
{
	static uint32_t s_uni2cWaitTimeOut = 0;
	bool bBussy = true;
	bool bTimeOut = false;
	SetUpdateTime(I2C_WAIT_TIME_OUT, &s_uni2cWaitTimeOut);
	while (bBussy && !bTimeOut)
	{
		bBussy = bBus? MAP_I2CMasterBusBusy(I2C_BASE) : MAP_I2CMasterBusy(I2C_BASE);
		bTimeOut = CheckUpdateTime(I2C_WAIT_TIME_OUT, &s_uni2cWaitTimeOut);
		if (bTimeOut)
		{
			ReportI2CError(I2C_BASE, unAddr, "time out");
		}
	}

	return CheckForErrors(I2C_BASE, unAddr, MAP_I2CMasterErr(I2C_BASE), bReportACKErrors) && !bTimeOut;
}

/** **********************************************************************
 *
 * Initialization of I2C Peripheral device
 * @param eI2CPort	I2C port of device
 *
 *********************************************************************** */
void I2C_Init(IS2PORT eI2CPort)
{
	PCI2CCONF pi2cConf = arrPort2Conf[eI2CPort];

	MAP_SysCtlPeripheralEnable(pi2cConf->ui32PeripheralI2C);
	MAP_SysCtlPeripheralEnable(pi2cConf->ui32PeripheralSys);
	MAP_GPIOPinTypeI2CSCL(pi2cConf->ui32GPIOPort, pi2cConf->ui8PinsSCL);
	MAP_GPIOPinTypeI2C(pi2cConf->ui32GPIOPort, pi2cConf->ui8PinsSDA);
	MAP_GPIOPinConfigure(pi2cConf->ui32PinConfigSCL);
	MAP_GPIOPinConfigure(pi2cConf->ui32PinConfigSDA);

	uint32_t I2C_BASE = arrPort2Conf[eI2CPort]->ui32Base;

	// Initialize Master at 100 kHz if last argument is false otherwise 400
	MAP_I2CMasterInitExpClk(I2C_BASE, MAP_SysCtlClockGet(), pi2cConf->bIsFast);
}

/** **********************************************************************
 * Sends bytes to I2C slave device
 *
 * @param eI2CPort		I2C port of device
 * @param unAddr		address of slave device
 * @param unDataLen		length of buffer to send
 * @param punData		data buffer
 * @param bSendStopBit	if value true function finalizes transmission otherwise another function may use and complete this command.
 *
 * @return Return true on success
 *
 *********************************************************************** */
bool I2C_WriteBytes(IS2PORT eI2CPort, uint8_t unAddr, const uint32_t unDataLen, const uint8_t* punData, bool bSendStopBit, bool bReportACKErrors)
{
	bool bRet = unDataLen == 0;
	if (!bRet)
	{
		uint32_t unByteIdx = 0;
		uint32_t I2C_BASE = arrPort2Conf[eI2CPort]->ui32Base;

		// Specify slave address
		MAP_I2CMasterSlaveAddrSet(I2C_BASE, unAddr, false);

		bool bCloseTx = 1 == unDataLen && bSendStopBit;

		MAP_I2CMasterDataPut(I2C_BASE, punData[unByteIdx]);
		MAP_I2CMasterControl(I2C_BASE, bCloseTx ? I2C_MASTER_CMD_BURST_SEND_FINISH : I2C_MASTER_CMD_BURST_SEND_START);

		bRet = I2C_WaitFor(I2C_BASE, unAddr, false, bReportACKErrors);

		unByteIdx++;
		while (bRet && unByteIdx < unDataLen)
		{
			bCloseTx = unByteIdx == unDataLen - 1 && bSendStopBit;

			MAP_I2CMasterDataPut(I2C_BASE, punData[unByteIdx]);
			MAP_I2CMasterControl(I2C_BASE, bCloseTx ? I2C_MASTER_CMD_BURST_SEND_FINISH : I2C_MASTER_CMD_BURST_SEND_CONT);
			bRet = I2C_WaitFor(I2C_BASE, unAddr, bCloseTx, bReportACKErrors);

			unByteIdx++;
		}
	}
	return bRet;
}

/** **********************************************************************
 * Sends byte to I2C slave device
 *
 * @param eI2CPort	I2C port of device
 * @param unAddr	address of slave device
 * @param unByte	byte to send
 *
 *********************************************************************** */
void I2C_WriteByte(IS2PORT eI2CPort, uint8_t unAddr, const uint8_t unByte)
{
	uint32_t I2C_BASE = arrPort2Conf[eI2CPort]->ui32Base;

	MAP_I2CMasterSlaveAddrSet(I2C_BASE, unAddr, false);
	MAP_I2CMasterDataPut(I2C_BASE, unByte);
	MAP_I2CMasterControl(I2C_BASE, I2C_MASTER_CMD_SINGLE_SEND);
	I2C_WaitFor(I2C_BASE, unAddr, true, true);
}

/** **********************************************************************
 * Receives byte from I2C slave device
 *
 * @param eI2CPort	I2C port of device
 * @param unAddr	address of slave device
 * @return byte from slave device
 *
 *********************************************************************** */
uint8_t I2C_ReadByte(IS2PORT eI2CPort, uint8_t unAddr)
{
	uint32_t I2C_BASE = arrPort2Conf[eI2CPort]->ui32Base;

	uint8_t unRet;
	MAP_I2CMasterSlaveAddrSet(I2C_BASE, unAddr, true);

	MAP_I2CMasterControl(I2C_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);
	I2C_WaitFor(I2C_BASE, unAddr, true, true);
	unRet = MAP_I2CMasterDataGet(I2C_BASE);

	return unRet;
}
