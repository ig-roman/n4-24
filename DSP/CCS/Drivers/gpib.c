/*
 * File:    gpib.c
 * Title:
 * Created: Fri Sep 22 09:17:40 2006
 *
 */
#include <stdio.h>
#include <string.h>
#include "gpib.h"

#define DBGU_printf printfVoid // printf
#define DELAY_NS_TEST_TINE 10 // ms
//#define DBGU_printf printf

// Local functions
void handle_DCAS_int(void);
void handle_BI_int(void);
void handle_BO_int(void);
void handle_GET_trigger(void);
void set_status_byte_bits(uint8_t);
void clear_status_byte_bits(uint8_t);
unsigned char NAT9914_inp(unsigned char io_address);
void NAT9914_outp(unsigned char io_address, unsigned char o_data);
void NAT9914_DelayNs(uint32_t lDelayNs);
void GPIB_OnDataRecievedInt(void);
int printfVoid(const char *_format, ...) { return 1; }

/* to input and output on a microcontroller, read or write to a memory location */
#define inp(io_address) NAT9914_inp(io_address)
#define outp(io_address, o_data) NAT9914_outp(io_address, o_data)

bool bIsGPIBInitialized = false;

void SetOutputInl(uint32_t ui32Port, uint8_t ui8Pins)
{
	MAP_GPIOPinWrite(ui32Port, ui8Pins, ui8Pins);
}

//#pragma interrupt_handler GPIB_HandleInterrupts
//#define USING_INTERRUPTS

GPIBSTATE s_eGpibState;

/* buffer variables
 *  index is an offset from the base address of the i/o buffer.
 *  io_buffer is both the GPIB input and output buffer.
 */
uint8_t index;
uint8_t io_buffer[BUFFER_SIZE];

/* isr0_byte and isr1_byte hold data from the NAT9914 status registers which has not yet
 * been processed.
 */
uint8_t isr0_byte;
uint8_t isr1_byte;

/** **********************************************************************
 *
 * This routine initializes the NAT9914 and enables the device to receive commands from
 * the GPIB network.
 *
 *********************************************************************** */
void initialize_NAT9914(uint8_t unAddress)
{
	// Testing NAT9914_DelayNs function at 10ms
	uint32_t unTimeD, unTime = GetTime();
	NAT9914_DelayNs(DELAY_NS_TEST_TINE * 1000);
	unTimeD = GetTime() - unTime;
	ASSERT(DELAY_NS_TEST_TINE == unTimeD);

    DBGU_printf("GPIB initialize_NAT9914()\n");

    /* ...Make sure GPIB chip is in 9914 mode.
     */
    outp(r7210_auxmr, c7210_sw9914);

    /* ...Reset the NAT9914. Logically remove device from the GPIB. The device will
     * ignore GPIB activity until it is initialized.
     */
    outp(r_auxcr, c_ch_rst);
    /* ...clear the status registers.
     */
    isr0_byte = 0;
    isr1_byte = 0;
    /* ...set clock speed
     */
    outp(r_accr, f_icr | f_4mhz);

    /* ...set T1 delay
     */
    outp(r_auxcr, c_vstdl);
    /* ...enable interrupts
     */
//    outp(r_imr0, 0xFF);
//    outp(r_imr1, 0xFF);
    outp(r_imr0, b_bo_ie | b_bi_ie);
    outp(r_imr1, b_dcas_ie | b_get_ie);

    //#ifndef USING_INTERRUPTS
    outp(r_auxcr, c_piimr2);
    outp(r_imr2, 0x00); /* clear GLINT bit */
    //#endif
    /* ...configure the NEWLINE character to end incoming and outgoing GPIB messages
     */
    outp(r_auxcr, c_pieosr);
    outp(r_eosr, NEWLINE);
    outp(r_auxcr, c_piaccr);
    outp(r_accr, f_accra | b_xeos | b_reos);
    /* ...set the GPIB address of the device
     */
    GPIB_SetAddress(unAddress);

    /* ...enable the device to receive data and commands from the GPIB network
    */
    outp(r_auxcr, c_nswrst);
}

/** **********************************************************************
 *
 * Initialization routines
 *
 *********************************************************************** */
const char* GPIB_PostInit(uint8_t unAddress)
{
    //DBGU_printf("GPIB initialize_device()\n");
	// Initialization sequences for both the microcontroller and the NAT9914
	NAT9914_ioInit();

    // Initialize software variables before the device begins accepting GPIB commands
    index = 0;
    s_eGpibState = GPIBSTATE_IDLE;
    initialize_NAT9914(unAddress);

    bIsGPIBInitialized = true;
    return NULL;
}

/** **********************************************************************
 *
 * convert the invalid address, 31(decimal) into 30
 *
 *********************************************************************** */
void GPIB_SetAddress(uint8_t addr)
{
    if(addr >= 31)
    {
        addr = 30;
    }

    outp(r_adr, addr);

}

/** **********************************************************************
 *
 * Handle Interrupt Routines
 * GPIB_HandleInterrupts handles the hardware interrupt from the NAT9914.
 * It determines what caused the interrupt and calls the appropriate function.
 * If no interrupts are pending, then it does nothing.
 *
 *********************************************************************** */
void GPIB_HandleInterrupts(void)
{
	if (bIsGPIBInitialized)
	{
		/* ...read isr0 and isr1, the values must be saved because the act of reading the bits
		 * in the registers clears the bits.
		 */
		isr0_byte = isr0_byte | inp(r_isr0);
		isr1_byte = isr1_byte | inp(r_isr1);

		/* ...determine the cause of the interrupt and handle it */
		if (isr0_byte & b_int0) {
			isr0_byte = isr0_byte & ~b_int0;
			DBGU_printf("GPIB isr0 int0\n");
		}
		if (isr0_byte & b_int1) {
			isr0_byte = isr0_byte & ~b_int1;
			DBGU_printf("GPIB isr0 int1\n");
		}
		if (isr0_byte & b_bi) {
			handle_BI_int();
		}
		if (isr0_byte & b_bo) {
			handle_BO_int();
		}
		if (isr0_byte & b_end) {
			isr0_byte = isr0_byte & ~b_end;
			DBGU_printf("GPIB isr0 end\n");
		}
		if (isr0_byte & b_spas) {
			isr0_byte = isr0_byte & ~b_spas;
			DBGU_printf("GPIB isr0 spas\n");
		}
		if (isr0_byte & b_rlc) {
			isr0_byte = isr0_byte & ~b_rlc;
			DBGU_printf("GPIB isr0 rlc\n");
		}
		if (isr0_byte & b_mac) {
			isr0_byte = isr0_byte & ~b_mac;
			DBGU_printf("GPIB isr0 mac\n");
		}

		if (isr1_byte & b_get) {
			handle_GET_trigger();
		}
		if (isr1_byte & b_err) {
			isr1_byte = isr1_byte & ~b_err;
			DBGU_printf("GPIB isr1 err\n");
		}
		if (isr1_byte & b_unc) {
			isr1_byte = isr1_byte & ~b_unc;
			DBGU_printf("GPIB isr1 enc\n");
		}
		if (isr1_byte & b_apt) {
			isr1_byte = isr1_byte & ~b_apt;
			DBGU_printf("GPIB isr1 abt\n");
		}
		if (isr1_byte & b_dcas) {
			handle_DCAS_int();
		}
		if (isr1_byte & b_ma) {
			isr1_byte = isr1_byte & ~b_ma;
			DBGU_printf("GPIB isr1 ma\n");
		}
		if (isr1_byte & b_srq) {
			isr1_byte = isr1_byte & ~b_srq;
			DBGU_printf("GPIB isr1 srq\n");
		}
		if (isr1_byte & b_ifc) {
			isr1_byte = isr1_byte & ~b_ifc;
			DBGU_printf("GPIB isr1 ifc\n");
		}
	}
}


/** **********************************************************************
 *
 * This routine resets only the GPIB interface of the device and not
 * the device itself. Its primary use is recovering after an error on the GPIB.
 * If a GPIB error occurs and the device locks up or appears to hang, the GPIB
 * controller can issue the SDC or DCL command and place the device into its idle GPIB
 * state, clear its buffers and start over.
 *
 *********************************************************************** */
void handle_DCAS_int(void)
{
    DBGU_printf("GPIB handle_DCAS_int()\n");

    /* ...reinitialize variables and buffers
     */
    index = 0;
    s_eGpibState = GPIBSTATE_IDLE;
    outp(r_auxcr, c_rhdf);
    isr0_byte = 0x00;
    isr1_byte = 0x00;
    /* ...update serial poll response byte
     */
    clear_status_byte_bits(b_mav |
                           b_rsv |
                           error_reading |
                           error_writing |
                           error_unknown_command);
    /* ...acknowledge command received and processed by releasing DAC holdoff
     */
    outp(r_auxcr, c_nonvalid);
}

/** **********************************************************************
 *
 * The Byte In handler reads a byte from the NAT9914 and stores it in the input buffer.
 * If the device has not finished writing data from a previous command message, the
 * device overwrites the old data and issues an error message to the GPIB controller.
 *
 *********************************************************************** */
void handle_BI_int(void)
{
    DBGU_printf("GPIB handle_BI_int()\n");

    /* ...update the gpib state */
    if(s_eGpibState == GPIBSTATE_IDLE)
    {
        index = 0;
        s_eGpibState = GPIBSTATE_READING;
    }
    /* ...if the controller is sending data to the device while the device is trying to
     * send data to the controller, set an error flag in the status byte to warn the
     * controller.
    */
    else if (s_eGpibState == GPIBSTATE_WRITING)
    {
        clear_status_byte_bits(b_mav);
        set_status_byte_bits(b_rsv | error_reading);
        s_eGpibState = GPIBSTATE_READING;
        index = 0;
        outp(r_auxcr, c_nbaf);
    }

	/* read data from the NAT9914 into the input buffer. To improve performance loop
	 * instead of exiting the interrupt handler and then calling the interrupt handler
	 * again.
	 */
	do {
		/* read the data into the buffer. If the buffer is full, then
		 * the routine doesn't accept the new byte.
		 */
		if (index < BUFFER_SIZE)
		{
			io_buffer[index] = (uint8_t)inp(r_dir);
			index++;
		}
		else
		{
			ASSERT(false);
		}

		/* check for another incoming byte unless the end of the string
		 * is detected.
		 */
		isr0_byte &= ~b_bi;
		if (!(isr0_byte & b_end))
		{
			isr0_byte |= (uint8_t) inp(r_isr0);
		}
	}
    while (isr0_byte & b_bi);

    s_eGpibState = GPIBSTATE_IDLE;

    /* ..if a complete message has been received, interpret before exiting */
    if(isr0_byte & b_end)
    {
    	/* ...reset the state variables, since the device now has stopped reading GPIB data */
    	isr0_byte &= ~b_end;
    }

    GPIB_OnDataRecievedInt();
}

/** **********************************************************************
 *
 * This routine places the next byte from the output buffer into the NAT9914. If
 * the device was reading data and received a spurious command to write, the function
 * issues an error message to the GPIB controller and exits.
 *
 *********************************************************************** */
void handle_BO_int(void)
{
    DBGU_printf("GPIB handle_BO_int()\n");

    /* ...update the GPIB state */
    if(s_eGpibState == GPIBSTATE_IDLE) {
    /* ...only write if data has been written to the output buffer
     */
        if(index == 0) {
            isr0_byte= isr0_byte & ~b_bo;
        } else {
            index = 0;
            s_eGpibState = GPIBSTATE_WRITING;
        }
    }
    /* ...If the controller and the device are both trying to read data, set an error
     * flag in the serial poll response byte
     */
    else if(s_eGpibState == GPIBSTATE_READING)
    {
        set_status_byte_bits(error_writing);
        isr0_byte = isr0_byte & ~b_bo;
    }
    /* ...write the data bytes to the NAT9914 until the listener stops listening or the
     * device runs out of data. To improve performance loop instead of exiting the
     * interrupt handler and then calling the interrupt handler again.
     */
    while (isr0_byte & b_bo)
    {
        //ASSERT(index <= BUFFER_SIZE);

        /* ...output a byte from the output buffer
         */
        outp(r_cdor, io_buffer[index]);

        index++;
        /* ...check if NAT9914 is ready to send another byte
         */
        isr0_byte = isr0_byte & ~b_bo;
        isr0_byte = isr0_byte | (uint8_t) inp(r_isr0);
        /* ...If the device is out of data, update the serial poll response register, stop
         * sending data and reset the buffer
         */
        if(!io_buffer[index] || io_buffer[index - 1] == NEWLINE || BUFFER_SIZE - 1 == index)
        {
        	ASSERT(BUFFER_SIZE != index);
            clear_status_byte_bits(b_mav | b_rsv);
            s_eGpibState = GPIBSTATE_IDLE;
            index = 0;
            io_buffer[index] = NEWLINE;
            io_buffer[index + 1] = NULL;

            isr0_byte = isr0_byte & ~b_bo;
        }
    }

    ASSERT(0 == index);
}

/** **********************************************************************
 *
 * Device Dependent Routines
 *
 * This routine performs a device specific trigger action.
 *
 *********************************************************************** */
void handle_GET_trigger(void)
{
    DBGU_printf("GPIB handle_GET_int()\n");

	/*
	 * insert code to implement the device specific trigger action
	 */

    /* release DAC holdoff to acknowledge to other routines and to the GPIB controller
     * that the device specific trigger action has been completed.
     */
    isr1_byte = isr1_byte & ~b_get;
    outp(r_auxcr, c_nonvalid);
}

/** **********************************************************************
 *
 * GPIB Buffer Routines
 *
 * GPIB_SendChar writes data to the output buffer. Because its input is
 * provided by other functions, it has no error checking.
 *
 *********************************************************************** */
void GPIB_SendChar(uint8_t data_out)
{
    /* ...check ranges on variables */
    ASSERT(s_eGpibState == GPIBSTATE_IDLE);
    ASSERT(index < BUFFER_SIZE);

    /* ..place the data byte on the output buffer */
    io_buffer[index] = data_out;
    index = index + 1;
}

/** **********************************************************************
 *
 * Status Byte management routines
 *
 * this routine encapsulates writes to the NAT9914's spmr register. It allows the
 * calling routine to set any combination of bits without affecting the others. It
 * presents a consistent interface for requesting serial polls. Its counterpart is
 * clear_status_byte_bits()
 *
 *********************************************************************** */
void set_status_byte_bits(uint8_t srq_byte)
{
	uint8_t srq_response_byte;

    DBGU_printf("GPIB set_status_byte_bits(%d)\n", srq_byte);

    srq_response_byte = (uint8_t) inp(r_spsr);
    srq_response_byte = srq_response_byte | srq_byte;
    srq_response_byte = srq_response_byte & ~b_rsv;
    outp(r_spmr, srq_response_byte );
    /* ...use the rsv2 command to request a serial poll instead of the rsv bit. The rsv2
     * command clears itself after the serial poll and mixing rsv2 and rsv can cause
     * undefined behavior.
     */
    if(srq_byte & b_rsv){
        outp(r_auxcr, c_rsv2);
    }
}

/** **********************************************************************
 *
 * this routine encapsulates writes to the NAT9914's spmr register. It allows the
 * calling routine to clear any combination of bits without affecting the others by
 * clearing the bits corresponding to the asserted bits of its input. It presents a
 * consistent interface for ceasing to request serial polls. Its counterpart is
 * set_status_byte_bits().
 *
 *********************************************************************** */
void clear_status_byte_bits(uint8_t srq_byte)
{
	uint8_t srq_response_byte;

    DBGU_printf("GPIB clear_status_byte_bits(%d)\n", srq_byte);

    srq_response_byte = (uint8_t)inp(r_spsr);
    srq_response_byte = srq_response_byte & ~srq_byte;
    srq_response_byte = srq_response_byte & ~b_rsv;
    outp(r_spmr, srq_response_byte );
    /* ...use the rsv2 command to stop requesting a serial poll instead of the rsv bit.
     * The rsv2 command clears itself after the serial poll and mixing rsv2 and rsv can
     * cause undefined behavior.
     */
    if(srq_byte & b_rsv) {
        outp(r_auxcr, c_nrsv2);
    }
}

unsigned char NAT9914_inp(unsigned char io_address)
{
	unsigned char in_data = 0;

	// "CE pulse width 80ns" used as delay for these pins
    NAT9914_dirIn();

    if (io_address & 0x01)	NAT9914_RS0_1();	else NAT9914_RS0_0();
    if (io_address & 0x02)	NAT9914_RS1_1();	else NAT9914_RS1_0();
    if (io_address & 0x04)	NAT9914_RS2_1();	else NAT9914_RS2_0();

    // Address setup to CE , WE, and DBIN 0ns
    // NAT9914_delay(0);

    NAT9914_CE_0();

    // CE pulse width 80ns
    NAT9914_DelayNs(80);

	if (NAT9914_AD0_in()) 	in_data |= 0x01;	else in_data &= ~0x01;
	if (NAT9914_AD1_in())	in_data |= 0x02;	else in_data &= ~0x02;
	if (NAT9914_AD2_in())	in_data |= 0x04;	else in_data &= ~0x04;
	if (NAT9914_AD3_in())	in_data |= 0x08;	else in_data &= ~0x08;
	if (NAT9914_AD4_in())	in_data |= 0x10;	else in_data &= ~0x10;
	if (NAT9914_AD5_in())	in_data |= 0x20;	else in_data &= ~0x20;
	if (NAT9914_AD6_in())	in_data |= 0x40;	else in_data &= ~0x40;
	if (NAT9914_AD7_in())	in_data |= 0x80;	else in_data &= ~0x80;

    NAT9914_CE_1();
    NAT9914_DelayNs(80); // CE recovery width 80ns

    return in_data;
}

void NAT9914_outp(unsigned char io_address, unsigned char o_data)
{
	// "Data setup to WE^ 60ns" used as delay for these pins
    NAT9914_dirOut();

    if (io_address & 0x01)	NAT9914_RS0_1();	else NAT9914_RS0_0();
    if (io_address & 0x02)	NAT9914_RS1_1();	else NAT9914_RS1_0();
    if (io_address & 0x04)	NAT9914_RS2_1();	else NAT9914_RS2_0();

    // Address setup to CE , WE, and DBIN 0ns
    // NAT9914_delay(0);

    NAT9914_CE_0();
    NAT9914_RD_WR_0();

    if (o_data & 0x01)		NAT9914_AD0_1();	else NAT9914_AD0_0();
    if (o_data & 0x02)		NAT9914_AD1_1();	else NAT9914_AD1_0();
    if (o_data & 0x04)		NAT9914_AD2_1();	else NAT9914_AD2_0();
    if (o_data & 0x08)		NAT9914_AD3_1();	else NAT9914_AD3_0();
    if (o_data & 0x10)		NAT9914_AD4_1();	else NAT9914_AD4_0();
    if (o_data & 0x20)		NAT9914_AD5_1();	else NAT9914_AD5_0();
    if (o_data & 0x40)		NAT9914_AD6_1();	else NAT9914_AD6_0();
    if (o_data & 0x80)		NAT9914_AD7_1();	else NAT9914_AD7_0();

    // Data setup to WE^ 60ns
    NAT9914_DelayNs(60);

    NAT9914_RD_WR_1();
    NAT9914_CE_1();

    // Data hold from WE^ 0ns
    // NAT9914_delay(0);

    // Address hold from CE, WE,and DBIN 0ns
    // NAT9914_delay(0);
}

void NAT9914_DelayNs(uint32_t lDelayNs)
{
	volatile uint32_t i, k, lLoops;

	// Has been calibrated for TIVA MCU at delay = 1sec
	lLoops = (lDelayNs * 1.064881074076493);

    for(i = 0; i < lLoops; i++)
    {
        k++;
    }
}

void GPIB_Send(const char* pBuf, unsigned int nBuffDataLen)
{
	if (nBuffDataLen)
	{
		int i;

		DBGU_printf("GPIB readback: %s", pBuf);
		index = 0;

		for (i = 0; i < nBuffDataLen; i++)
		{
			GPIB_SendChar(pBuf[i]);
		}

		set_status_byte_bits(b_mav | b_rsv);
	}
}

/** **********************************************************************
 *
 * parse the message and call the correct routine. Also, reset the buffer and set the
 * device in idle state. If the command just received is invalid, send an error message
 * to the GPIB controller.
 *
 *********************************************************************** */
void GPIB_OnDataRecievedInt()
{
	int i;

	DBGU_printf("GPIB io_buffer:");
	for(i = 0; i < index; i++)
	{
		DBGU_printf("%c", io_buffer[i]);
	}

	/* Handle command */
	GPIB_OnDataRecieved((const char*)io_buffer, index);
}
