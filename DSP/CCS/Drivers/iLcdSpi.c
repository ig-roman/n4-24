#include "driverlib/rom.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom_map.h"

#include "../Common.h"
#include "spi.h"
#include "iLcdSpi.h"
#include "circ_buf.h"

// For CIO only
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

#include "inc/hw_ints.h"
#include "driverlib/gpio.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"


// Commands for iLCD SPI controller details on http://demmel.com/download/ilcd/spi_appnote.pdf
#define SPI_CMD_END 0x00
#define SPI_CMD_START 0xAA
#define SPI_CMD_GET_SIZE_TO_READ 0x02
#define SPI_CMD_GET_INFO 0x03
#define SPI_CMD_BLOCK_READ 0x04
#define SPI_CMD_BLOCK_WRITE 0x05
#define SPI_CMD_CONT_BLOCK_WRITE 0x06
#define SPI_CMD_GET_FREE_SPACE_QUEUE 0x07

#define LCD_ALERT_GPIO_PERIPH	SYSCTL_PERIPH_GPIOD
#define LCD_ALERT_PORT 			GPIO_PORTD_BASE
#define LCD_ALERT_GPIO_PIN		GPIO_PIN_3

#define SPI_MAX_PACKAGE_SIZE 127

#define SPI_RX_BUFFER_SIZE 128
char arrSpiRxBuf[SPI_RX_BUFFER_SIZE];
CIRC_BUF cbRxBuf;

/** **********************************************************************
 *
 * Reads iLCD data throw iLCD SPI controller using SPI(SSI) and dedicated ALERT wire
 *
 *********************************************************************** */
void ILCDSPI_ReadSPIBuff()
{
	/*
	// Test iLCD SPI controller and SPI bus by getting queue size from. Usually unFreeSpace should be 0x80 in iLCD controller
	SPI_WriteByteSafe(SPI_CMD_START);
	SPI_WriteByteSafe(SPI_CMD_GET_FREE_SPACE_QUEUE);
	SPI_WriteByteSafe(SPI_CMD_END);
	uint8_t unFreeSpace = SPI_ReadByte(SPI_CMD_END);
	if (!unFreeSpace)
	{
		SetLastError("ILCDSPI_ReadSPIBuff - iLCD queue length is null");
	}
	*/

	/*
	// SPI controller status and buffer len
	SPI_WriteByteSafe(SPI_CMD_START);
	SPI_WriteByteSafe(SPI_CMD_GET_INFO);
	SPI_WriteByteSafe(SPI_CMD_END);
	uint8_t unStatus = SPI_SwapByte(SPI_CMD_END);
	uint8_t unDispBuffLen = SPI_SwapByte(SPI_CMD_END);
	//printf("status: %d, buff len: %d", unStatus, unDispBuffLen);
	*/

	/*
	// Reads size of buffer and then reads it as sequence of bytes. Works faster for sequences of bytes
	SPI_SwapByte(SPI_CMD_START);
	SPI_SwapByte(SPI_CMD_GET_SIZE_TO_READ);
	SPI_SwapByte(SPI_CMD_END);
	uint8_t unDispBuffLen = SPI_SwapByte(SPI_CMD_END);
	if (unDispBuffLen)
	{
		SPI_SwapByte(SPI_CMD_START);
		SPI_SwapByte(SPI_CMD_BLOCK_READ);
		SPI_SwapByte(unDispBuffLen);
		SPI_SwapByte(SPI_CMD_END);

		while (unDispBuffLen)
		{
			uint8_t unBuffByte = SPI_SwapByte(SPI_CMD_END);
			CIRC_BUF_put(&cbRxBuf, &unBuffByte, 1);
			unDispBuffLen--;
		}
	}
	*/

	// In 99% of cases response is 0xAA. Code below works faster than above because it reads just one byte.
	while (!MAP_GPIOPinRead(LCD_ALERT_PORT, LCD_ALERT_GPIO_PIN))
	{
		SPI_WriteByteSafe(SPI_CMD_START);
		SPI_WriteByteSafe(SPI_CMD_BLOCK_READ);
		SPI_WriteByteSafe(1);
		SPI_WriteByteSafe(SPI_CMD_END);

		char cBuffByte = SPI_ReadByte();
		CIRC_BUF_put(&cbRxBuf, &cBuffByte, 1);
	}
}

/** **********************************************************************
 *
 * Handles changes on ALERT wire.
 * ALERT wire is used when iLCD controller has some data in output buffer.
 * When buffer is empty ALERT wire has HIGH level other wise LOW.
 * This handler is called when ALERT wire goes LOW to read data from iLCD controller
 *
 *********************************************************************** */
void GPIOIntHandler()
{
	ILCDSPI_ReadSPIBuff();
	MAP_GPIOIntClear(LCD_ALERT_PORT, LCD_ALERT_GPIO_PIN); // Clear interrupt flag
}

/** **********************************************************************
 *
 * Initialization of SPI iLCD controller
 * @param enumPage name of page
 * @return new page. Caller must delete this page from heap.
 *
 *********************************************************************** */
void ILCDSPI_Init()
{
    SPI_Init();

	CIRC_BUF_init(&cbRxBuf, arrSpiRxBuf, SPI_RX_BUFFER_SIZE);

	// Configuring ALERT pin as input with interrupt
	MAP_SysCtlPeripheralEnable(LCD_ALERT_GPIO_PERIPH);

	MAP_GPIOPinTypeGPIOInput(LCD_ALERT_PORT, LCD_ALERT_GPIO_PIN);
	MAP_GPIOPadConfigSet(LCD_ALERT_PORT, LCD_ALERT_GPIO_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
	MAP_GPIOIntTypeSet(LCD_ALERT_PORT, LCD_ALERT_GPIO_PIN, GPIO_FALLING_EDGE);

	DelayMS(1000); // Wait for SPI iLCD controller becomes ready

	// Flush the unexpected data
	ILCDSPI_ReadSPIBuff();
	char cByte;
	while (CIRC_BUF_get(&cbRxBuf, &cByte, 1, true))

	// Starts monitoring of ALERT pin
	GPIOIntRegister(LCD_ALERT_PORT, GPIOIntHandler);
	MAP_GPIOIntEnable(LCD_ALERT_PORT, LCD_ALERT_GPIO_PIN);
}

/** **********************************************************************
 * Sends iLcd command via SPI bus to iLCD controller
 *
 * @param unILcdCmd	iLCD command. Should be started from \i(0xAA)
 * @param unCount length of command
 *
 *********************************************************************** */
void ILCDSPI_SendCmd(const uint8_t *unILcdCmd, uint32_t unCount)
{
	// Remove iLCD command
	unILcdCmd++;
	unCount--;

	uint32_t unCountLeft = unCount;

	while (unCountLeft)
	{
		SPI_WriteByteSafe(SPI_CMD_START);
		SPI_WriteByteSafe(
				unCountLeft == unCount
				? SPI_CMD_BLOCK_WRITE
				: SPI_CMD_CONT_BLOCK_WRITE);

		uint32_t unPackageSize = MIN(SPI_MAX_PACKAGE_SIZE, unCountLeft);

		SPI_WriteByteSafe(unPackageSize);
		SPI_Write(unILcdCmd, unPackageSize);

		unCountLeft -= unPackageSize;
		unILcdCmd += unPackageSize;
	}
}

/** **********************************************************************
 *
 * Returns iLCD command received via SPI bus and stored to internal buffer
 *
 * @param pMem			pointer to memory buffer
 * @param count			count bytes to copy/move from internal buffer to pMem
 * @param bMoveReadIdx	if value true function moves data otherwise copies
 *
 * @return	returns count of copied/moved data
 *
 *********************************************************************** */
int ILCDSPI_GetCmd(char *pMem, int count, bool bMoveReadIdx)
{
	// If ALERT pin has LOW state and no interrupt accrued(connection problem) data from iLCD SPI controller should be read.
	while (!MAP_GPIOPinRead(LCD_ALERT_PORT, LCD_ALERT_GPIO_PIN))
	{
		ILCDSPI_ReadSPIBuff();
	}

	int nRet = 0;
	if (!CIRC_BUF_ISEMPTY(&cbRxBuf))
	{
		// Disable the interrupt. If we don't do this there is a race
		// condition which can cause the read index to be corrupted.
		MAP_GPIOIntDisable(LCD_ALERT_PORT, LCD_ALERT_GPIO_PIN);

		nRet = CIRC_BUF_get(&cbRxBuf, pMem, count, bMoveReadIdx);

		MAP_GPIOIntEnable(LCD_ALERT_PORT, LCD_ALERT_GPIO_PIN);
	}
	return nRet;
}


