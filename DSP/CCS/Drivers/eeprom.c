/** **********************************************************************
 *
 * Driver library for M24M02-DR EEPROM
 *
 *********************************************************************** */

#include "eeprom.h"
#include "i2c.h"
#include "../Common.h"
#include <stdlib.h>
#include <string.h>

#define EEPROM_SIZE					256000L
#define EEPROM_E2_VALUE				0 		// if E2 pin is connected to ground value should be 0 if to VCC 1
#define EEPROM_I2C_ADDR_MEM			0x50
#define EEPROM_WRITE_TIME_OUT_MS	11		// Maximum write cycle is 10ms see Tw CD00290537.pdf
#define EEPROM_WRITE_PAGE_SIZE		255		// Maximum bytes per single page

/** **********************************************************************
 *
 * Initializes EEPROM functions.
 *
 *********************************************************************** */
void EEPROM_Init(void)
{
    I2C_Init(IS2PORT_EEPROM);
}

/** **********************************************************************
 *
 * @return Returns I2C address of EEPROM IC
 *
 *********************************************************************** */
uint8_t GetI2CAddr()
{
	uint8_t unAddrRet = EEPROM_I2C_ADDR_MEM;

	if (EEPROM_E2_VALUE)
	{
		unAddrRet |= EEPROM_E2_VALUE << 2;
	}

	return unAddrRet;
}

/** **********************************************************************
 *
 * Prepares address for read or write operation
 * @param unAddress		address of data
 * @param pSplitAddr	splits unAddress to 3 bytes. The first contains address bits from 17-16, the second 15-8, the third 7-0
 * @return true if address in the range of eeprom
 *
 *********************************************************************** */
bool GetEepromAddr(uint32_t unAddress, uint8_t* pSplitAddr)
{
	bool bRet = (unAddress < EEPROM_SIZE);
	if (bRet)
	{
		pSplitAddr[2] = (uint8_t)unAddress;

		unAddress = unAddress >> 8;
		pSplitAddr[1] = (uint8_t)unAddress;

		unAddress = unAddress >> 8;
		pSplitAddr[0] = (uint8_t)(unAddress & 0x03);
	}

	return bRet;
}

bool EEPROM_writeBytesWithPool(const uint32_t unDataLen, const uint8_t* punData, bool bSendStopBit)
{
#ifndef LAUNCHPAD
	static uint32_t s_unEepromWaitTimeOut = 0;
	// Attempt to store data
	bool bRet = I2C_WriteBytes(IS2PORT_EEPROM, punData[0], unDataLen - 1, punData + 1, bSendStopBit, false);
	if (!bRet)
	{
		// When EEPROM is busy it disconnects from I2C line. Max disconnect time is 10 ms but this time can be less than 10 ms.
		// 5.1.6 Minimizing Write delays by polling on ACK. Chapter from  from CD00290537.pdf
		SetUpdateTime(EEPROM_WRITE_TIME_OUT_MS, &s_unEepromWaitTimeOut);
		while (!bRet)
		{
			if (CheckUpdateTime(EEPROM_WRITE_TIME_OUT_MS, &s_unEepromWaitTimeOut))
			{
				ReportError("EEPROM_writeByte() - Time out");
				break;
			}
			bRet = I2C_WriteBytes(IS2PORT_EEPROM, punData[0], unDataLen - 1, punData + 1, bSendStopBit, false);
		}
	}
	return bRet;
#else
	return true;
#endif
}

/** **********************************************************************
 *
 * Writes byte to the provided address
 * @param unAddress		address of data
 * @param unData		one byte of data
 *
 *********************************************************************** */
void EEPROM_writeByte(uint32_t unAddress, const uint8_t unData)
{

	uint8_t arrSplitAddr[4] = {0};
	if (GetEepromAddr(unAddress, arrSplitAddr))
	{
		arrSplitAddr[0] |= GetI2CAddr();
		arrSplitAddr[3] = unData;

		EEPROM_writeBytesWithPool(sizeof(arrSplitAddr), arrSplitAddr, true);
	}
	else
	{
		ReportError("EEPROM_writeByte() - EEPROM address out of range");
	}
}

/** **********************************************************************
 *
 * Reads byte from the provided address
 * @param unAddress		address of data
 * @return Returns data from eeprom
 *
 *********************************************************************** */
uint8_t EEPROM_readByte(uint32_t unAddress)
{
	uint8_t unRet = 0xFF; // 0xFF - default state for empty EEPROM
#ifndef LAUNCHPAD
	uint8_t arrSplitAddr[3] = {0};
	if (GetEepromAddr(unAddress, arrSplitAddr))
	{
		arrSplitAddr[0] |= GetI2CAddr();
		if (EEPROM_writeBytesWithPool(sizeof(arrSplitAddr), arrSplitAddr, false))
		{
			unRet = I2C_ReadByte(IS2PORT_EEPROM, arrSplitAddr[0]);
		}
	}
	else
	{
		ReportError("EEPROM_readByte() - EEPROM address out of range");
	}
#endif
	return unRet;
}

/** **********************************************************************
 *
 * Reads bytes from the provided address
 *
 * @param unAddress		address of data
 * @param unLength		length of data
 * @param punData		buffer to write data
 *
 *********************************************************************** */
void EEPROM_readBytes(uint32_t unAddress, uint32_t unLength, uint8_t* punData)
{
	// TODO: implement burst mode if it required
	uint32_t  unByteIdx = 0;
	while (unByteIdx < unLength)
	{
		punData[unByteIdx] = EEPROM_readByte(unAddress + unByteIdx);
		unByteIdx++;
	}
}

/** **********************************************************************
 *
 * Writes bytes to the provided address
 *
 * @param unAddress		address of data
 * @param unLength		length of data
 * @param punData		buffer to read data
 *
 *********************************************************************** */
void EEPROM_writeBytes(uint32_t unStartAddress, uint32_t unLength, const uint8_t* punData)
{
	if (1 == unLength)
	{
		EEPROM_writeByte(unStartAddress, *punData);
	}
	else if (1 < unLength)
	{
		uint32_t  unByteIdx = 0;
		while (unByteIdx < unLength)
		{
			/*
			 * The Page Write mode allows up to 256 bytes to be written in a single Write cycle,
			 * provided that they are all located in the same page in the memory: that is, the most significant memory address bits, A17-A8, are the same.
			 * If more bytes are sent than will fit up to the end of the page, a �roll-over� occurs,
			 * i.e. the bytes exceeding the page end are written on the same page, from location 0.
			 */

			// Limit page size to maximum size
			uint32_t unPageSize = MIN((unLength - unByteIdx), EEPROM_WRITE_PAGE_SIZE);

			// Limit page size to boundary when A17-A8 are same
			uint32_t unPageAddress = unStartAddress + unByteIdx;
			uint32_t unMaskedPageAddr = unPageAddress & 0xFF;
			unPageSize = MIN(unPageSize, 0xFF - unMaskedPageAddr + 1);

			uint32_t unBufferSize = unPageSize + 3;
			uint8_t* punBuff = (uint8_t*)malloc(unBufferSize * sizeof(uint8_t));
			if (GetEepromAddr(unPageAddress, punBuff))
			{
				punBuff[0] |= GetI2CAddr();
				memcpy(punBuff + 3, punData + unByteIdx, unPageSize);
				// memset(punBuff + 3, '1', unPageSize);

				EEPROM_writeBytesWithPool(unBufferSize, punBuff, true);
			}
			else
			{
				ReportError("EEPROM_writeBytes() - EEPROM address out of range");
			}

			free (punBuff);

			unByteIdx += unPageSize;
		}
	}
}


