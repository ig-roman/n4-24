#include "panel.h"
#include "TA9554.h"
#include "keyboard.h"
#include "qei.h"

#include "i2c.h"

#define LED_BUZZER_IC_ADDR 0x27

#define LED_PINS_MASK 0x07

#define LED1_GREEN 0x01
#define LED1_RED 0x02
#define LED2_RED 0x04

#define BUZZER_PINS_MASK 0x08

uint8_t s_unPortData;
bool s_bIsPanelInitialized = false;

/** **********************************************************************
 *
 * Configures front panel of device
 *
 *********************************************************************** */
const char* PANEL_PostInit()
{
	// Quadrature encoder
    QEI_Init();

	// I2C port is used for keyboard, buzzer and LEDs.
	I2C_Init(IS2PORT_KBD);

	const char* pszError = KBD_Init() ? NULL : STR_ERROR;

	if (!pszError)
	{
		// Configure all pins for all ICs as output
		pszError = TA9554_CfgPinsAs(LED_BUZZER_IC_ADDR, false) ? NULL : "KEYBOARD LED ERROR";

		s_bIsPanelInitialized = !pszError;

		if (s_bIsPanelInitialized)
		{
			// s_bIsPanelInitialized should be true to make changes
			PANEL_SetLedState(PANELLED_OFF);

			// s_bIsPanelInitialized should be true to make changes
			PANEL_SetBuzzerState(true);
			PANEL_SetBuzzerState(false);
		}
	}

 	return pszError;
}

/** **********************************************************************
 *
 * Changes configuration of panel LEDs
 * @param nNewState use PANELLED to modify state of LEDs
 *
 *********************************************************************** */
void PANEL_SetLedState(PANELLED eNewState)
{
	if (s_bIsPanelInitialized)
	{
		uint8_t unLedData;
		switch (eNewState)
		{
			case PANELLED_ON:	unLedData = LED1_GREEN;	break;
			case PANELLED_HOLD:	unLedData = LED1_RED;	break;
			default: 			unLedData = LED2_RED;
		}

		s_unPortData &= ~LED_PINS_MASK;
		s_unPortData |= unLedData;

		TA9554_PortSet(LED_BUZZER_IC_ADDR, s_unPortData);
	}
}

/** **********************************************************************
 *
 * Enables or disables buzzer
 * @param bOn true to enable buzzer false to disable it
 *
 *********************************************************************** */
void PANEL_SetBuzzerState(bool bOn)
{
	if (s_bIsPanelInitialized)
	{
		if (((s_unPortData & BUZZER_PINS_MASK) > 0) != bOn)
		{
			if (bOn)
			{
				s_unPortData |= BUZZER_PINS_MASK;
			}
			else
			{
				s_unPortData &= ~BUZZER_PINS_MASK;
			}

			TA9554_PortSet(LED_BUZZER_IC_ADDR, s_unPortData);
		}
	}
}

