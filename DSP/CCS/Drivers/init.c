#include "init.h"

#include <stdbool.h>
#include <stdint.h>

#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/systick.h"
#include "driverlib/interrupt.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_nvic.h"

#include "eeprom.h"
#include "iLcdSpi.h"

void ConfigurePeripheral(void);

//*****************************************************************************
//
// External declarations for the interrupt handlers used by the application.
//
//*****************************************************************************
extern void SysTickIntHandler(void);

void JumpToBootLoader()
{
    uint32_t ui32SysClock;
	int ui32Addr;

//	Unclock PortD Pin7
	HWREG(GPIO_PORTD_BASE | GPIO_O_LOCK) = 0x4C4F434B;
//  Commit it
	HWREG(GPIO_PORTD_BASE | GPIO_O_CR) = 0xFF;
//  Enable it
	HWREG(GPIO_PORTD_BASE | GPIO_O_DEN) = 0xFF;
	if (MAP_GPIOPinRead(GPIO_PORTD_BASE, GPIO_PIN_7))
		return;
	//��������� ��� ����������, ������������� ������������
	MAP_IntMasterDisable();
    MAP_SysTickIntDisable();
    MAP_SysTickDisable();
    MAP_SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);
    ui32SysClock = MAP_SysCtlClockGet();
	//���������� ���������� ���� USB
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	MAP_GPIOPinTypeUSBAnalog(GPIO_PORTD_BASE, GPIO_PIN_4 | GPIO_PIN_5);
	MAP_SysTickPeriodSet(ROM_SysCtlClockGet() / 100);//������� �������
	//���������� ��� ����������
	HWREG(NVIC_DIS0) = 0xffffffff;
	HWREG(NVIC_DIS1) = 0xffffffff;
	HWREG(NVIC_DIS2) = 0xffffffff;
	HWREG(NVIC_DIS3) = 0xffffffff;
	HWREG(NVIC_DIS4) = 0xffffffff;

	for (ui32Addr = NVIC_PRI0; ui32Addr <= NVIC_PRI34; ui32Addr += 4)
	  HWREG(ui32Addr) = 0;
	HWREG(NVIC_SYS_PRI1) = 0;
	HWREG(NVIC_SYS_PRI2) = 0;
	HWREG(NVIC_SYS_PRI3) = 0;
	//������������� ������������ USB
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_USB0);
	MAP_SysCtlPeripheralReset(SYSCTL_PERIPH_USB0);
	MAP_SysCtlUSBPLLEnable();
	MAP_SysCtlDelay(ui32SysClock * 2 / 3);
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_UDMA);
	//���������� ���������� � ��������� DFU
	MAP_IntMasterEnable();
	ROM_UpdateUSB(0);
	for(;;);
}

void Init()
{
    // Enable lazy stacking for interrupt handlers.  This allows floating-point
    // instructions to be used within interrupt handlers, but at the expense of
    // extra stack usage.
	MAP_FPUEnable();
	MAP_FPULazyStackingEnable();

	// Set the clocking to run directly from the crystal.
	MAP_SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    // Initialize the SysTick interrupt to process colors and buttons.
	SysTickIntRegister(SysTickIntHandler);
	MAP_SysTickPeriodSet(SysCtlClockGet() / 1000); // 1 ms
	MAP_SysTickEnable();
	MAP_SysTickIntEnable();
	MAP_IntMasterEnable();

    ConfigurePeripheral();
	JumpToBootLoader(); // If need

    EEPROM_Init();
    ILCDSPI_Init();
}

void ConfigurePeripheral(void)
{
    // Enable the GPIO Port turn on RUN led on top of board.
	MAP_SysCtlPeripheralEnable(RUN_LED_SYSCTL_PERIPH);
	MAP_GPIOPinTypeGPIOOutput(RUN_LED_GPIO_PORT, RUN_LED);
}

void ManageRunLed()
{
	static uint32_t s_unBlinkTime = 0;
	static bool s_bOnState = false;
	if (CheckUpdateTimeForLoop(50, &s_unBlinkTime))
	{
		MAP_GPIOPinWrite(RUN_LED_GPIO_PORT, RUN_LED, s_bOnState ? 0 : RUN_LED);
		s_bOnState = !s_bOnState;
	}
}
