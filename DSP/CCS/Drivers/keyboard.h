#ifndef KYBD_H_
#define KYBD_H_

#include "../Common.h"

#define ROTARY_UP_BTN_CODE -50
#define ROTARY_DOWN_BTN_CODE -51

bool KBD_Init();
bool ManageKeyboard(int8_t* punKeyCode, uint8_t* pbPressed);

#endif /* KYBD_H_ */
