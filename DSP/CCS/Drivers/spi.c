#include <stdio.h>
#include "spi.h"
#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/ssi.h"
#include "driverlib/sysctl.h"
#include "driverlib/rom.h"
#include "driverlib/pin_map.h"

#include "inc/hw_memmap.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"

// Defines the SSI and GPIO peripherals
#define SSI_PERIPH          SYSCTL_PERIPH_SSI0
#define SSI_GPIO_PERIPH     SYSCTL_PERIPH_GPIOA

// Defines the GPIO pin configuration macros for the pins that are used for the SSI function.
#define PINCFG_SSICLK       GPIO_PA2_SSI0CLK
#define PINCFG_SSIFSS       GPIO_PA3_SSI0FSS
#define PINCFG_SSITX        GPIO_PA5_SSI0TX
#define PINCFG_SSIRX        GPIO_PA4_SSI0RX

// Defines the port and pins for the SSI peripheral.
#define SSI_PORT            GPIO_PORTA_BASE
#define SSI_PINS            (GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5)

// Defines the SSI peripheral used and the data speed.
#define SSI_BASE            SSI0_BASE
#define SSI_CLOCK           1000000

/** **********************************************************************
 * Writes byte to SPI and checks that response is 0x00, it checks that SIP bus works properly.
 *
 * @param unByte	byte o send
 *
 *********************************************************************** */
void SPI_WriteByteSafe(const uint8_t unByte)
{
	uint8_t unAlwaysNull = SPI_SwapByte(unByte);
	if (unAlwaysNull)
	{
		ReportError("SPI - unexpected byte has been received ");
	}
}

/** **********************************************************************
 *
 * Reads byte from SPI bus
 * @return Returns byte from slave
 *
 *********************************************************************** */
uint8_t SPI_ReadByte()
{
	return SPI_SwapByte(0);
}

/** **********************************************************************
 * Writes byte to SPI and reads received byte from slave back
 *
 * @param unByte	byte o send
 * @return Returns byte from slave
 *
 *********************************************************************** */
uint8_t SPI_SwapByte(const uint8_t unByte)
{
	MAP_SSIDataPut(SSI_BASE, unByte);
	while (MAP_SSIBusy(SSI_BASE));

	uint32_t unSpiword;
	while(MAP_SSIDataGetNonBlocking(SSI_BASE, &unSpiword));
	return unSpiword;
}

/** **********************************************************************
 * Writes bytes to SPI
 *
 * @param unCmd		data to send
 * @param unCount	size of data to send
 *
 *********************************************************************** */
void SPI_Write(const uint8_t *unCmd, uint32_t unCount)
{
    while(unCount--)
    {
    	SPI_WriteByteSafe(*unCmd);
        unCmd++;
    }
}

/** **********************************************************************
 *
 * Initialization of SPI Peripheral device
 *
 *********************************************************************** */
void SPI_Init(void)
{
	// Configure peripheral SPI device to use
 	MAP_SysCtlPeripheralEnable(SSI_GPIO_PERIPH);
 	MAP_SysCtlPeripheralEnable(SSI_PERIPH);

	// Configure IO
	MAP_GPIOPinConfigure(PINCFG_SSICLK);
	MAP_GPIOPinConfigure(PINCFG_SSIRX);
	MAP_GPIOPinConfigure(PINCFG_SSITX);
	MAP_GPIOPinConfigure(PINCFG_SSIFSS);

	// Configure SPI pins
	MAP_GPIOPinTypeSSI(SSI_PORT, SSI_PINS);

	// Configure SPI parameters
	MAP_SSIConfigSetExpClk(SSI_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_0, SSI_MODE_MASTER, SSI_CLOCK, 8);

	// Enable it
	MAP_SSIEnable(SSI_BASE);
}
