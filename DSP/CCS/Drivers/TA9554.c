#include "TA9554.h"
#include "i2c.h"

#define TA9554_CMD_REG_SEL_INPUT 0x00
#define TA9554_CMD_REG_SEL_OUTPUT 0x01
#define TA9554_CMD_REG_SEL_INVERT 0x02
#define TA9554_CMD_REG_SEL_CONFIG 0x03

/** **********************************************************************
 * Configures TA9554 IC as input device
 *
 * @param unI2cAddr	address of TA9554 to configure
 *
 *********************************************************************** */
bool TA9554_CfgPinsAs(uint8_t unI2cAddr, bool bInput)
{
#ifndef LAUNCHPAD
	// Configure all ports of TA9554 as inputs (0xFF) or outputs (0x00)
	uint8_t arrData[2] = { TA9554_CMD_REG_SEL_CONFIG };
	arrData[1] = bInput ? 0xFF : 0x00;
	return I2C_WriteBytes(IS2PORT_KBD, unI2cAddr, sizeof(arrData) / sizeof(uint8_t), arrData, true, true);
#else
	return true;
#endif
}

/** **********************************************************************
 * Reads data from input register
 *
 * @param unI2cAddr	address of TA9554 IC
 * @return new register value which mapped to input pins
 *
 *********************************************************************** */
uint8_t TA9554_PortGet(uint8_t unI2cAddr)
{
#ifndef LAUNCHPAD
	uint8_t unCmdRegSelInput = TA9554_CMD_REG_SEL_INPUT;
	I2C_WriteBytes(IS2PORT_KBD, unI2cAddr, 1, &unCmdRegSelInput, false, true);
	return I2C_ReadByte(IS2PORT_KBD, unI2cAddr);
#else
	return 0;
#endif
}

/** **********************************************************************
 * Sets data to output register
 *
 * @param unI2cAddr	address of TA9554 IC
 * @param unData	new register value which mapped to output pins
 *
 *********************************************************************** */
void TA9554_PortSet(uint8_t unI2cAddr, uint8_t unData)
{
#ifndef LAUNCHPAD
	uint8_t arrData[2] = { TA9554_CMD_REG_SEL_OUTPUT };
	arrData[1] = unData;
	I2C_WriteBytes(IS2PORT_KBD, unI2cAddr, sizeof(arrData) / sizeof(uint8_t), arrData, true, true);
#endif
}
