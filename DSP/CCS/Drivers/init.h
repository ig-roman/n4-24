#ifndef INIT_H_
#define INIT_H_

// IO configuration for Run LED
#define RUN_LED GPIO_PIN_6
#define RUN_LED_GPIO_PORT GPIO_PORTD_BASE
#define RUN_LED_SYSCTL_PERIPH SYSCTL_PERIPH_GPIOD

void Init();
void ManageRunLed();

#endif /* INIT_H_ */
