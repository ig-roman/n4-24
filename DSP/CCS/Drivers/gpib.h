/*
 * File:    gpib.h
 * Title:
 * Created: Fri Sep 22 09:17:32 2006
 *
 */

#ifndef GPIB_H
#define GPIB_H

#include "TopCommon.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"

#define ClearOutput(mPin) MAP_GPIOPinWrite(mPin, 0)
#define SetOutput(mPin) SetOutputInl(mPin)
#define IsInputSet(mPin) MAP_GPIOPinRead(mPin)

#define CfgInput(mPin) MAP_GPIOPinTypeGPIOInput(mPin)
#define CfgOutput(mPin) MAP_GPIOPinTypeGPIOOutput(mPin)

#define CfgPorts()	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);\
					MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);\
					MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);\
					MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);\
					MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);\
					MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);\
					MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG)

/* GPIB - NAT9914 */
#define NAT9914_MASK_INT     GPIO_PORTA_BASE, GPIO_PIN_7
#define NAT9914_MASK_RD_WR   GPIO_PORTG_BASE, GPIO_PIN_3
#define NAT9914_MASK_CE      GPIO_PORTG_BASE, GPIO_PIN_2
#define NAT9914_MASK_RS0     GPIO_PORTG_BASE, GPIO_PIN_4
#define NAT9914_MASK_RS1     GPIO_PORTF_BASE, GPIO_PIN_3
#define NAT9914_MASK_RS2     GPIO_PORTF_BASE, GPIO_PIN_2

#define NAT9914_MASK_AD0     GPIO_PORTA_BASE, GPIO_PIN_6
#define NAT9914_MASK_AD1     GPIO_PORTC_BASE, GPIO_PIN_6
#define NAT9914_MASK_AD2     GPIO_PORTC_BASE, GPIO_PIN_7
#define NAT9914_MASK_AD3     GPIO_PORTE_BASE, GPIO_PIN_2
#define NAT9914_MASK_AD4     GPIO_PORTE_BASE, GPIO_PIN_3
#define NAT9914_MASK_AD5     GPIO_PORTF_BASE, GPIO_PIN_4
#define NAT9914_MASK_AD6     GPIO_PORTB_BASE, GPIO_PIN_7
#define NAT9914_MASK_AD7     GPIO_PORTB_BASE, GPIO_PIN_6

#define NAT9914_RD_WR_0()    ClearOutput(NAT9914_MASK_RD_WR)
#define NAT9914_RD_WR_1()    SetOutput(NAT9914_MASK_RD_WR)
#define NAT9914_CE_0()       ClearOutput(NAT9914_MASK_CE)
#define NAT9914_CE_1()       SetOutput(NAT9914_MASK_CE)

#define NAT9914_ioInit()	CfgPorts(); \
							CfgInput(NAT9914_MASK_INT); \
							NAT9914_dirOut(); \
                            CfgOutput(NAT9914_MASK_RD_WR); \
							CfgOutput(NAT9914_MASK_CE); \
                            CfgOutput(NAT9914_MASK_RS0); \
                            CfgOutput(NAT9914_MASK_RS1); \
                            CfgOutput(NAT9914_MASK_RS2); \
                            NAT9914_CE_1()

#define NAT9914_dirIn()		CfgInput(NAT9914_MASK_AD0); \
							CfgInput(NAT9914_MASK_AD1); \
							CfgInput(NAT9914_MASK_AD2); \
							CfgInput(NAT9914_MASK_AD3); \
							CfgInput(NAT9914_MASK_AD4); \
							CfgInput(NAT9914_MASK_AD5); \
							CfgInput(NAT9914_MASK_AD6); \
							CfgInput(NAT9914_MASK_AD7)

#define NAT9914_dirOut()	CfgOutput(NAT9914_MASK_AD0); \
							CfgOutput(NAT9914_MASK_AD1); \
							CfgOutput(NAT9914_MASK_AD2); \
							CfgOutput(NAT9914_MASK_AD3); \
							CfgOutput(NAT9914_MASK_AD4); \
							CfgOutput(NAT9914_MASK_AD5); \
							CfgOutput(NAT9914_MASK_AD6); \
							CfgOutput(NAT9914_MASK_AD7)

#define NAT9914_RS0_0()      ClearOutput(NAT9914_MASK_RS0)
#define NAT9914_RS0_1()      SetOutput(NAT9914_MASK_RS0)
#define NAT9914_RS1_0()      ClearOutput(NAT9914_MASK_RS1)
#define NAT9914_RS1_1()      SetOutput(NAT9914_MASK_RS1)
#define NAT9914_RS2_0()      ClearOutput(NAT9914_MASK_RS2)
#define NAT9914_RS2_1()      SetOutput(NAT9914_MASK_RS2)
#define NAT9914_AD0_0()      ClearOutput(NAT9914_MASK_AD0)
#define NAT9914_AD0_1()      SetOutput(NAT9914_MASK_AD0)
#define NAT9914_AD1_0()      ClearOutput(NAT9914_MASK_AD1)
#define NAT9914_AD1_1()      SetOutput(NAT9914_MASK_AD1)
#define NAT9914_AD2_0()      ClearOutput(NAT9914_MASK_AD2)
#define NAT9914_AD2_1()      SetOutput(NAT9914_MASK_AD2)
#define NAT9914_AD3_0()      ClearOutput(NAT9914_MASK_AD3)
#define NAT9914_AD3_1()      SetOutput(NAT9914_MASK_AD3)
#define NAT9914_AD4_0()      ClearOutput(NAT9914_MASK_AD4)
#define NAT9914_AD4_1()      SetOutput(NAT9914_MASK_AD4)
#define NAT9914_AD5_0()      ClearOutput(NAT9914_MASK_AD5)
#define NAT9914_AD5_1()      SetOutput(NAT9914_MASK_AD5)
#define NAT9914_AD6_0()      ClearOutput(NAT9914_MASK_AD6)
#define NAT9914_AD6_1()      SetOutput(NAT9914_MASK_AD6)
#define NAT9914_AD7_0()      ClearOutput(NAT9914_MASK_AD7)
#define NAT9914_AD7_1()      SetOutput(NAT9914_MASK_AD7)

#define NAT9914_AD0_in()     IsInputSet(NAT9914_MASK_AD0)
#define NAT9914_AD1_in()     IsInputSet(NAT9914_MASK_AD1)
#define NAT9914_AD2_in()     IsInputSet(NAT9914_MASK_AD2)
#define NAT9914_AD3_in()     IsInputSet(NAT9914_MASK_AD3)
#define NAT9914_AD4_in()     IsInputSet(NAT9914_MASK_AD4)
#define NAT9914_AD5_in()     IsInputSet(NAT9914_MASK_AD5)
#define NAT9914_AD6_in()     IsInputSet(NAT9914_MASK_AD6)
#define NAT9914_AD7_in()     IsInputSet(NAT9914_MASK_AD7)
/* GPIB - NAT9914 end */


#define Base_Address 0x0
#define error_reading 1<<0
#define error_writing 1<<1
#define error_unknown_command 1<<2
#define NEWLINE 0x0a /* line feed */
#define PORT_D 0x1008
#define interrupt_vector 0xFFF2

/* define i/o addresses of the registers on the nat9914 */
#define r_isr0 (Base_Address )
#define r_imr0 (Base_Address )
#define r_isr1 (Base_Address+1 )
#define r_imr1 (Base_Address+1 )
#define r_adsr (Base_Address+2 )
#define r_imr2 (Base_Address+2 )
#define r_eosr (Base_Address+2 )
#define r_bcr (Base_Address+2 )
#define r_accr (Base_Address+2 )
#define r_bsr (Base_Address+3 )
#define r_auxcr (Base_Address+3 )
#define r_isr2 (Base_Address+4 )
#define r_adr (Base_Address+4 )
#define r_spmr (Base_Address+5)
#define r_spsr (Base_Address+5)
#define r_cptr (Base_Address+6)
#define r_ppr (Base_Address+6)
#define r_dir (Base_Address+7)
#define r_cdor (Base_Address+7)

/* NAT9914 registers in 7210 mode */
#define r7210_auxmr (Base_Address+5)

/* isr0 bits*/
#define b_int0 0x80
#define b_int1 0x40
#define b_bi 0x20
#define b_bo 0x10
#define b_end 0x08
#define b_spas 0x04
#define b_rlc 0x02
#define b_mac 0x01

/* imr0 bits*/
#define b_dma0 0x80
#define b_dma1 0x40
#define b_bi_ie 0x20
#define b_bo_ie 0x10
#define b_end_ie 0x08
#define b_spas_ie 0x04
#define b_rlc_ie 0x02
#define b_mac_ie 0x01

/* isr1 bits*/
#define b_get 0x80
#define b_err 0x40
#define b_unc 0x20
#define b_apt 0x10
#define b_dcas 0x08
#define b_ma 0x04
#define b_srq 0x02
#define b_ifc 0x01

/* imr1 bits*/
#define b_get_ie 0x80
#define b_err_ie 0x40
#define b_unc_ie 0x20
#define b_apt_ie 0x10
#define b_dcas_ie 0x08
#define b_ma_ie 0x04
#define b_srq_ie 0x02
#define b_ifc_ie 0x01

/* adsr bits */
#define b_rem 0x80
#define b_llo 0x40
#define b_atn 0x20
#define b_lpas 0x10
#define b_tpas 0x08
#define b_la 0x04
#define b_ta 0x02
#define b_ulpa 0x01

/* imr2 bits */
#define b_glint 0x80
#define b_stbo_ie 0x40
#define b_nlen 0x20
#define b_lloc_ie 0x08
#define b_atni_ie 0x04
#define b_cic_ie 0x01

/* bcr bits */
#define b_bcr_atn 0x80
#define b_bcr_dav 0x40
#define b_bcr_ndac 0x20
#define b_bcr_nrfd 0x10
#define b_bcr_eoi 0x08
#define b_bcr_srq 0x04
#define b_bcr_ifc 0x02
#define b_bcr_ren 0x01

/* accr fields (shadow registers)*/
#define f_icr 0x20
#define f_accra 0x80
#define f_accrb 0xa0
#define f_accre 0xc0
#define f_accrf 0xd0
#define f_accri 0xe0

/* bsr bits are identical to bcr*/
/* isr2 bits */
#define b_nba 0x80
#define b_stbo 0x40
#define b_nl 0x20
#define b_eos 0x10
#define b_lloc 0x08
#define b_atni 0x04
#define b_cic 0x01

/* adr bits */
#define b_edpa 0x80
#define b_dal 0x40
#define b_dat 0x20

/* spmr/spsr bits */
#define b_rsv 0x40
#define b_mav 0x10

/* accra bits */
#define b_bin 0x10
#define b_xeos 0x08
#define b_reos 0x04

/* accrb bits */
#define b_iss 0x10
#define b_inv 0x08
#define b_lwc 0x04
#define b_speoi 0x02
#define b_atct 0x01

/* accre bits */
#define b_dhadt 0x08
#define b_dhadc 0x04

/* accrf bits */
#define b_dhata 0x08
#define b_dhala 0x04
#define b_dhuntl 0x02
#define b_dhall 0x01

/* accri bits */
#define b_ustd 0x08
#define b_pp1 0x04
#define b_dmae 0x01

/* accr~icr bits */
#define f_1mhz  0x01
#define f_2mhz  0x02
#define f_3mhz  0x03
#define f_4mhz  0x04
#define f_5mhz  0x05
#define f_6mhz  0x06
#define f_7mhz  0x07
#define f_8mhz  0x08
#define f_10mhz 0x05
#define f_16mhz 0x08
#define f_20mhz 0x0A


/* auxcr commands */
#define c_nswrst 0x00
#define c_swrst 0x80
#define c_nonvalid 0x01
#define c_valid 0x81
#define c_rhdf 0x02
#define c_nhdfa 0x03
#define c_hdfa 0x83
#define c_nhdfe 0x04
#define c_hdfe 0x84
#define c_nbaf 0x05
#define c_nfget 0x06
#define c_fget 0x86
#define c_nrtl 0x07
#define c_rtl 0x87
#define c_feoi 0x08
#define c_nlon 0x09
#define c_lon 0x89
#define c_nton 0x0a
#define c_ton 0x8a
#define c_gts 0x0b
#define c_tca 0x0c
#define c_tcs 0x0d
#define c_nrpp 0x0e
#define c_rpp 0x8e
#define c_nsic 0x0f
#define c_sic 0x8f
#define c_nsre 0x10
#define c_sre 0x90
#define c_rqc 0x11
#define c_rlc 0x12
#define c_ndai 0x13
#define c_dai 0x93
#define c_pts 0x14
#define c_nstdl 0x15
#define c_stdl 0x95
#define c_nshdw 0x16
#define c_shdw 0x96
#define c_nvstdl 0x17
#define c_vstdl 0x97
#define c_nrsv2 0x18
#define c_rsv2 0x98
#define c_sw7210 0x99
#define c_reqf 0x1a
#define c_reqt 0x9a
#define c_ch_rst 0x1c
#define c_nist 0x1d
#define c_ist 0x9d
#define c_piimr2 0x1e
#define c_pibcr 0x1f
#define c_clrpi 0x9c
#define c_pieosr 0x9e
#define c_piaccr 0x9f

/* NAT9914 in 7210 mode commands */
#define c7210_sw9914 0x15

/* buffer and GPIB state declarations */
#define BUFFER_SIZE INST_INPUT_BUFFER_MAX_SIZE

typedef enum { GPIBSTATE_IDLE, GPIBSTATE_READING, GPIBSTATE_WRITING } GPIBSTATE;

/* public function declarations */
const char* GPIB_PostInit(uint8_t unAddress);
void GPIB_OnDataRecieved(const char* pBuf, unsigned int nBuffDataLen);
void GPIB_Send(const char* pBuf, unsigned int nBuffDataLen);
void GPIB_SendChar(uint8_t);
void GPIB_HandleInterrupts(void);
void GPIB_SetAddress(uint8_t addr);

#endif /* #ifndef GPIB_H */
