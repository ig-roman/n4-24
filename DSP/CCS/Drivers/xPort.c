#include "uart.h"
#include "xPort.h"
#include "../SerialCmdParser.h"
#include <string.h>
#include "../Common.h"
#include <ctype.h>
#include <stdlib.h>

// For CIO only
#include <stdio.h>

#define  DATA_WAIT_PERIOD		500
#define  DATA_NEXT_BYTE_PERIOD	100

typedef bool (*FNXPRCheck)(const char *pszText, PLAN_CONF pConfig);

bool XPORT_IPCheck(const char *pszText, PLAN_CONF pConfig);
bool XPORT_PortCheck(const char *pszText, PLAN_CONF pConfig);
bool XPORT_ValidateConfig(PLAN_CONF pConfig);

#define XPR_DEFS \
		XPR_DEF( "IP addr "								,  XPORT_IPCheck), \
		XPR_DEF( "TFTP Download is     disabled"		,  NULL), \
		XPR_DEF( "Enhanced Password is enabled"			,  NULL), \
		XPR_DEF( "Baudrate 9600, I/F Mode 4C, Flow 00"	,  NULL), \
		XPR_DEF( "Port "								,  XPORT_PortCheck), \
		XPR_DEF( "Connect Mode : C0"					,  NULL), \
		XPR_DEF( "Disconn Mode : 00"					,  NULL), \
		XPR_DEF( "Flush   Mode : 00"					,  NULL) \

#define XPR_DEF( pszVal, fn)  pszVal
const char* const s_arrMandatoryValues[] = { XPR_DEFS };
#undef XPR_DEF
#define TOTAL_CHECKS (sizeof(s_arrMandatoryValues) / sizeof(char*))

#define XPR_DEF( pszVal, fn)  fn
extern const FNXPRCheck s_arrCheckFn[] = { XPR_DEFS };
#undef XPR_DEF

// Scan from begining
const char* const STR_SETUP_ENTER_KEY = "xxx";

const char* const STR_SETUP_SERVER_KEY = "0\r";
#define IP_BASE \
	"%d\r\n"\
	"%d\r\n"\
	"%d\r\n"\
	"%d\r\n"\
	"N\n"\
	"%d\r\n"\
	"N\n"\
	"N\n"\
// IP Address
// IP Address
// IP Address
// IP Address
// Set Gateway IP Address
// Netmask: Number of Bits for Host Part
// Set DNS Server IP addr
// Change Telnet/Web Manager password

const char* const STR_SETUP_SERVER_CONF_STATIC = IP_BASE;
const char* const STR_SETUP_SERVER_CONF_DHCP =
	IP_BASE
	"Y\n"					// Change DHCP device name
	ID_PRODUCT "\r\n"		// Enter new DHCP device Name
	"Y\n";					// Enable DHCP FQDN option

const char* const STR_SETUP_SERIAL_CHANNEL_KEY = "1\r";
const char* const STR_SETUP_SERIAL_CHANNEL_CONF =
	"9600\r\n"				// Baudrate
	"4C\r\n"				// I/F Mode
	"00\r\n"				// Flow
	"%d\r\n"				// Port No
	"C0\r\n"				// ConnectMode (C0)
	"N\n"					// Send '+++' in Modem Mode
	"N\n"					// Show IP addr after 'RING'
	"N\n"					// Auto increment source port
	"000\r\n"				// IP Address
	"000\r\n"				// IP Address
	"000\r\n"				// IP Address
	"000\r\n"				// IP Address
	"0\r\n"					// Remote Port
	"00\r\n"				// DisConnMode
	"00\r\n"				// FlushMode
	"00\r\n"				// DisConnTime
	"00\r\n"				// DisConnTime
	"00\r\n"				// SendChar 1
	"00\r\n";				// SendChar 2

const char* const STR_SETUP_SECURITY_KEY = "6\r";
const char* const STR_SETUP_SECURITY_CONF =
	"N\n"					// Disable SNMP
	ID_PRODUCT "\r\n"		// SNMP Community Name
	"N\n"					// Disable Telnet Setup
	"Y\n"					// Disable TFTP Firmware Update. Disabled because this configuration tool and parser may stop working with other versions. It tested only with V6.10.0.1
	"N\n"					// Disable Port 77FEh
	"0\r\n"					// Access Mode (0=Read & Write, 1=Read Only)
	"N\n"					// Disable Web Server
	"N\n"					// Disable Web Setup
	"N\n"					// Disable ECHO ports
	"Y\n"					// Enable Enhanced Password
	"Y\n"					// Change the Password
	"%s\r\n"				// New password
	"N\n";					// Disable Port 77F0h

const char* const STR_SETUP_EXIT_KEY = "8\r";
const char* const STR_SETUP_EXIT_SAVE_KEY = "9\r";

// Local functions
void XPORT_SendConfCmd(const char* pszCmd, bool bFlushRecievedLine);
void XPORT_SendConfCmds(char* pszCmd);

typedef enum { XPORTSTAT_UNKNOWN, XPORTSTAT_OK, XPORTSTAT_ERROR } XPORTSTAT;
XPORTSTAT gs_eXPortStatus = XPORTSTAT_UNKNOWN;

const char* XPORT_PostInit(PLAN_CONF pConfig, const char* pszPassword)
{
	const char* pszRet = NULL;
	if (XPORTSTAT_ERROR == gs_eXPortStatus)
	{
		pszRet = "XPort was not configured before";
	}
	else if (XPORTSTAT_OK == gs_eXPortStatus)
	{
		pszRet = "Skipped";
	}
	else
	{
		// Check that XPort is configured
		// Wait 3 seconds when xPort become avaliable
		uint8_t nCnt = 0;
		bool bHasResponse = false;
		bool bIsValidConfig = false;
		uint32_t unWaitTimeOut = 0;

		for (; nCnt < 6 && !bHasResponse; nCnt++)
		{
			// Entering to setup mode
			XPORT_SendConfCmd(STR_SETUP_ENTER_KEY, false);

			SetUpdateTime(DATA_WAIT_PERIOD, &unWaitTimeOut);
			while (!bHasResponse && !CheckUpdateTimeForLoop(DATA_WAIT_PERIOD, &unWaitTimeOut))
			{
				bHasResponse = UARTDataGetBuffered(PORTNAME_XPORT, NULL, 1, false);
			}
		}

		if (bHasResponse)
		{
			// Never skip ValidateConfig because XPort always shows configuration parameters after entering to the setup mode.
			bIsValidConfig = XPORT_ValidateConfig(pConfig);
			bIsValidConfig = bIsValidConfig && !pConfig->bForceUpdate;

			//int n = 0; while (!n);
			if (bIsValidConfig)
			{
				XPORT_SendConfCmd(STR_SETUP_EXIT_KEY,  true);
			}
			else
			{
				// Entering to security section
				// Should be executed first because is configures "enhanced password"
				char szBuff[100] = {0};
				XPORT_SendConfCmd(STR_SETUP_SECURITY_KEY,  true);
				sprintf(szBuff, STR_SETUP_SECURITY_CONF, pszPassword);
				XPORT_SendConfCmds(szBuff);

				bool bIsDhcp = !pConfig->unIP1 && !pConfig->unIP2 && !pConfig->unIP3 && !pConfig->unIP4;
				// Configure Ethernet channel
				XPORT_SendConfCmd(STR_SETUP_SERVER_KEY,  true);
				sprintf(szBuff, bIsDhcp ? STR_SETUP_SERVER_CONF_DHCP : STR_SETUP_SERVER_CONF_STATIC, pConfig->unIP1, pConfig->unIP2, pConfig->unIP3, pConfig->unIP4, pConfig->unMask);
				XPORT_SendConfCmds(szBuff);

				// Configure serial channel
				XPORT_SendConfCmd(STR_SETUP_SERIAL_CHANNEL_KEY,  true);
				sprintf(szBuff, STR_SETUP_SERIAL_CHANNEL_CONF, pConfig->unPort);
				XPORT_SendConfCmds(szBuff);

				// Check new configuration
				bIsValidConfig = XPORT_ValidateConfig(pConfig);

				// Saving changes
				XPORT_SendConfCmd(bIsValidConfig ? STR_SETUP_EXIT_SAVE_KEY : STR_SETUP_EXIT_KEY,  true);

				if (!bIsValidConfig)
				{
					pszRet = "XPort cannot be configured";
				}
			}
		}
		else
		{
			pszRet = "No response from XPort";
		}

		gs_eXPortStatus = bIsValidConfig ? XPORTSTAT_OK : XPORTSTAT_ERROR;
	}

	return pszRet;
}

bool XPORT_ValidateConfig(PLAN_CONF pConfig)
{
	bool bRet = false;

	uint8_t unUnCompletedChecks = TOTAL_CHECKS;
	char chBuff[INST_INPUT_BUFFER_MAX_SIZE];

	// Reports configuration parameters
	XPORT_SendConfCmd("\r",  false);

	while (true)
	{
		uint32_t nBuffDataLen = sizeof(chBuff) - 1;
		if (SCP_ReadPortDataCustom(PORTNAME_XPORT, chBuff, &nBuffDataLen, DATA_WAIT_PERIOD, DATA_NEXT_BYTE_PERIOD, true))
		{
			uint8_t unIdx = 0;
			chBuff[nBuffDataLen] = NULL;
			unIdx = 0;
			for (; unIdx < TOTAL_CHECKS; ++unIdx)
			{
				const char* pszMandatoryValue = s_arrMandatoryValues[unIdx];
				size_t nKeyLen = strlen(pszMandatoryValue);
				if (0 == strncmp(chBuff, pszMandatoryValue, nKeyLen))
				{
					FNXPRCheck pFnCheck = s_arrCheckFn[unIdx];
					if (!pFnCheck || pFnCheck(chBuff + nKeyLen, pConfig))
					{
						unUnCompletedChecks--;
					}

					// Check is found
					break;
				}
			}
		}
		else
		{
			// No more data
			break;
		}
	}

	bRet = 0 == unUnCompletedChecks;
	if (!bRet)
	{
		printf("Invalid XPort configuration\n Total checks are failed %d\n", unUnCompletedChecks);
	}

	return bRet;
}

void XPORT_Send(const char* pBuf, unsigned int nLen)
{
	ASSERT(XPORTSTAT_OK == gs_eXPortStatus);
	if (XPORTSTAT_OK == gs_eXPortStatus)
	{
		UARTDataPutBuffered(PORTNAME_XPORT, pBuf, nLen);
	}
}

void XPORT_HandleInterrupts(void)
{
	// Check that data in avaliable in serial port
	if (UARTDataGetBuffered(PORTNAME_XPORT, NULL, 1, false))
	{
		// Read until new line or CR character is come
		char chBuff[INST_INPUT_BUFFER_MAX_SIZE];
		uint32_t nBuffDataLen = sizeof(chBuff) - 1;
		if (SCP_ReadPortDataCustom(PORTNAME_XPORT, chBuff, &nBuffDataLen, DATA_WAIT_PERIOD, DATA_NEXT_BYTE_PERIOD, true))
		{
			XPORT_OnDataRecieved(chBuff, nBuffDataLen);
		}
	}
}

void XPORT_SendConfCmd(const char* pszCmd, bool bFlushRecievedLine)
{
	// Send command to XPort
	UARTDataPutBuffered(PORTNAME_XPORT, pszCmd, strlen(pszCmd));

	if (bFlushRecievedLine)
	{
		char chBuff[INST_INPUT_BUFFER_MAX_SIZE];
		uint32_t nBuffDataLen = sizeof(chBuff);
		while (SCP_ReadPortDataCustom(PORTNAME_XPORT, chBuff, &nBuffDataLen, DATA_WAIT_PERIOD, DATA_NEXT_BYTE_PERIOD, false) && INST_INPUT_BUFFER_MAX_SIZE - 1 == nBuffDataLen);
	}
}

void XPORT_SendConfCmds(char* pszCmd)
{
	char* pszConfData = pszCmd;

	// Create NULL terminated list
	// '\n' characters are used only for printf function and can be replaced to any other.
	// XPort works with '\r' new line terminator or without it
	while (*pszConfData)
	{
		if ('\n' == *pszConfData)
		{
			*pszConfData = NULL;
		}
		pszConfData++;
	}

	pszConfData = pszCmd;
	while (*pszConfData)
	{
		// Send command to XPort and ignore responses because they already known
		XPORT_SendConfCmd(pszConfData, true);

		// Get next command.
		pszConfData += strlen(pszConfData) + 1;
	}
}

bool XPORT_IPCheck(const char *pszText, PLAN_CONF pConfig)
{
	// When address is DHCP IP address contains "- " suffix
	while (*pszText && !isdigit(*pszText)) pszText++;

	bool bRet = (pConfig->unIP1 == ParseFirstDigit(&pszText)
			&& pConfig->unIP2 == ParseFirstDigit(&pszText)
			&& pConfig->unIP3 == ParseFirstDigit(&pszText)
			&& pConfig->unIP4 == ParseFirstDigit(&pszText));

	return bRet;
}

bool XPORT_PortCheck(const char *pszText, PLAN_CONF pConfig)
{
	bool bRet = false;
	size_t nLen = strlen(pszText);
	if (nLen > 2 && nLen < 8)
	{
		bRet = isdigit(pszText[nLen - 3]) && isdigit(pszText[0]);
		if (bRet)
		{
			uint16_t lConfiguredPort = atol(pszText);
			bRet = lConfiguredPort == pConfig->unPort;
		}
	}

	return bRet;
}
