
#include "intEeprom.h"
#include "driverlib/eeprom.h"
#include "driverlib/sysctl.h"
#include "driverlib/rom_map.h"

void INTEEPROM_readBytes(uint32_t unAddress, uint32_t unLength, uint8_t* punData)
{
	if (unAddress % sizeof(uint32_t) || unLength % sizeof(uint32_t) )
	{
		ReportError("EEPROM_readByte() - Can't read not aligned data");
	}
	else
	{
		if (EEPROMStatusGet())
		{
			ReportError("EEPROM_readByte() - failed");
		}
		else
		{
			MAP_EEPROMRead((uint32_t*)punData, unAddress, unLength);
		}
	}
}

void INTEEPROM_writeBytes(uint32_t unAddress, uint32_t unLength, const uint8_t* punData)
{
	if (unAddress % sizeof(uint32_t) || unLength % sizeof(uint32_t) )
	{
		ReportError("EEPROM_readByte() - Can't save not aligned data");
	}
	else
	{
		if (MAP_EEPROMProgram((uint32_t*)punData, unAddress, unLength)) // 0 - success
		{
			ReportError("EEPROM_writeByte() - failed");
		}
	}
}

void INTEEPROM_Init(void)
{
	// Enable the UART peripheral for use.
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_EEPROM0);
	while(!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_EEPROM0));

	if (EEPROM_INIT_OK != MAP_EEPROMInit())
	{
		ReportError("EEPROMInit() - failed");
	}
}
