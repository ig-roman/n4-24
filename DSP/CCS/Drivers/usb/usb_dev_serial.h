#ifndef USBDEVSERIAL_H
#define USBDEVSERIAL_H

#define USB_RX_BUFFER_SIZE     INST_INPUT_BUFFER_MAX_SIZE

const char* USB_PostInit();
int USBDataGetBuffered(char *mem, int count, bool bMoveReadIdx);
void USB_Send(const char* pBuf, unsigned int nBuffDataLen);

#endif /* #ifndef USBDEVSERIAL_H */
