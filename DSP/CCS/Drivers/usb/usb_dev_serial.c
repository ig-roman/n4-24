//*****************************************************************************
//
// usb_dev_serial.c - Main routines for the USB CDC serial example.
//
// Copyright (c) 2012-2014 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
// 
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
// 
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
// 
// This is part of revision 2.1.0.12573 of the EK-TM4C123GXL Firmware Package.
//
//*****************************************************************************

#include <stdlib.h>
#include "../../Common.h"
#include "../../../circ_buf.h"
#include "usb_dev_serial.h"
#include "inc/hw_memmap.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/sysctl.h"
#include "driverlib/usb.h"
#include "usblib/usblib.h"
#include "usblib/usbcdc.h"
#include "usblib/device/usbdevice.h"
#include "usblib/device/usbdcdc.h"
#include "usb_serial_structs.h"

CIRC_BUF USBRxCBuf;
char USBRxBuf[USB_RX_BUFFER_SIZE];

//*****************************************************************************
//
//! \addtogroup example_list
//! <h1>USB Serial Device (usb_dev_serial)</h1>
//!
//! This example application turns the evaluation kit into a virtual serial
//! port when connected to the USB host system.  The application supports the
//! USB Communication Device Class, Abstract Control Model to redirect UART0
//! traffic to and from the USB host system.
//!
//! Assuming you installed TivaWare C Series in the default directory, a
//! driver information (INF) file for use with Windows XP, Windows Vista and
//! Windows7 can be found in C:/ti/TivaWare-for-C-Series/windows_drivers.  For
//! Windows 2000, the required INF file is in
//! C:/ti/TivaWare-for-C-Series/windows_drivers/win2K.
//
//*****************************************************************************

//*****************************************************************************
//
// Read as many characters from the UART FIFO as we can and move them into
// the CDC transmit buffer.
//
// \return Returns UART error flags read during data reception.
//
//*****************************************************************************
void USB_Send(const char* pBuf, unsigned int nBuffDataLen)
{
    uint32_t ui32Space = USBBufferSpaceAvailable((tUSBBuffer *)&g_sTxBuffer);
    if (nBuffDataLen <= ui32Space)
    {
    	USBBufferWrite((tUSBBuffer *)&g_sTxBuffer, (const uint8_t*)pBuf, nBuffDataLen);
    }
    else
    {
    	ReportError("USB_Send() - TX buffer size less than data to transfer");
    }
}

//*****************************************************************************
//
// Get the communication parameters in use on the UART.
//
//*****************************************************************************
static void GetLineCoding(tLineCoding *psLineCoding)
{
	psLineCoding->ui8Databits = 8;
	psLineCoding->ui8Parity = USB_CDC_PARITY_NONE;
	psLineCoding->ui8Stop = USB_CDC_STOP_BITS_1;
	psLineCoding->ui32Rate = 9600;
}

//*****************************************************************************
//
// Handles CDC driver notifications related to control and setup of the device.
//
// \param pvCBData is the client-supplied callback pointer for this channel.
// \param ui32Event identifies the event we are being notified about.
// \param ui32MsgValue is an event-specific value.
// \param pvMsgData is an event-specific pointer.
//
// This function is called by the CDC driver to perform control-related
// operations on behalf of the USB host.  These functions include setting
// and querying the serial communication parameters, setting handshake line
// states and sending break conditions.
//
// \return The return value is event-specific.
//
//*****************************************************************************
uint32_t
ControlHandler(void *pvCBData, uint32_t ui32Event,
               uint32_t ui32MsgValue, void *pvMsgData)
{
    // Which event are we being asked to process?
    switch(ui32Event)
    {
        // We are connected to a host and communication is now possible.
        case USB_EVENT_CONNECTED:

            // Flush our buffers.
            USBBufferFlush(&g_sTxBuffer);
            USBBufferFlush(&g_sRxBuffer);
            break;

        // The host has disconnected.
        case USB_EVENT_DISCONNECTED:
        {
        	if (ui32Event)
        	{
        		ASSERT(false);
        	}
            break;
        }

        // Return the current serial communication parameters.
        case USBD_CDC_EVENT_GET_LINE_CODING:
            GetLineCoding(pvMsgData);
            break;

        // Unused events
        case USBD_CDC_EVENT_SET_LINE_CODING:		// Set the current serial communication parameters.
        case USBD_CDC_EVENT_SET_CONTROL_LINE_STATE:	// Set the current serial communication parameters.
        case USB_EVENT_SUSPEND:						// Ignore SUSPEND and RESUME for now.
        case USB_EVENT_RESUME:						// Ignore SUSPEND and RESUME for now.
        case USBD_CDC_EVENT_SEND_BREAK: 			// Send a break condition on the serial line.
        case USBD_CDC_EVENT_CLEAR_BREAK:			// Clear the break condition on the serial line.
        	break;

        // We don't expect to receive any other events.  Ignore any that show
        // up in a release build or hang in a debug build.
        default: ASSERT(false);
        	break;

    }

    return(0);
}

//*****************************************************************************
//
// Handles CDC driver notifications related to the transmit channel (data to
// the USB host).
//
// \param ui32CBData is the client-supplied callback pointer for this channel.
// \param ui32Event identifies the event we are being notified about.
// \param ui32MsgValue is an event-specific value.
// \param pvMsgData is an event-specific pointer.
//
// This function is called by the CDC driver to notify us of any events
// related to operation of the transmit data channel (the IN channel carrying
// data to the USB host).
//
// \return The return value is event-specific.
//
//*****************************************************************************
uint32_t
TxHandler(void *pvCBData, uint32_t ui32Event, uint32_t ui32MsgValue,
          void *pvMsgData)
{
    //
    // Which event have we been sent?
    //
    switch(ui32Event)
    {
        case USB_EVENT_TX_COMPLETE:
            //
            // Since we are using the USBBuffer, we don't need to do anything
            // here.
            //
            break;

        //
        // We don't expect to receive any other events.  Ignore any that show
        // up in a release build or hang in a debug build.
        //
        default:
        	 ASSERT(false);
        	 break;
    }
    return(0);
}

//*****************************************************************************
//
// Handles CDC driver notifications related to the receive channel (data from
// the USB host).
//
// \param ui32CBData is the client-supplied callback data value for this channel.
// \param ui32Event identifies the event we are being notified about.
// \param ui32MsgValue is an event-specific value.
// \param pvMsgData is an event-specific pointer.
//
// This function is called by the CDC driver to notify us of any events
// related to operation of the receive data channel (the OUT channel carrying
// data from the USB host).
//
// \return The return value is event-specific.
//
//*****************************************************************************
uint32_t
RxHandler(void *pvCBData, uint32_t ui32Event, uint32_t ui32MsgValue,
          void *pvMsgData)
{
    // Which event are we being sent?
    switch (ui32Event)
    {
        // A new packet has been received.
        case USB_EVENT_RX_AVAILABLE:
        {
        	uint32_t nBuffDataLen = USBBufferDataAvailable((tUSBBuffer *)&g_sRxBuffer);
        	if (nBuffDataLen)
        	{
        		uint8_t *buf = (uint8_t *)malloc(nBuffDataLen);
        		// Get data from the buffer.
        		USBBufferRead((tUSBBuffer *)&g_sRxBuffer, buf, nBuffDataLen);
        		CIRC_BUF_put(&USBRxCBuf, (char *)buf, nBuffDataLen);
        		free(buf);
        	}
            break;
        }

        // We are being asked how much unprocessed data we have still to
        // process. We return 0 if the UART is currently idle or 1 if it is
        // in the process of transmitting something. The actual number of
        // bytes in the UART FIFO is not important here, merely whether or
        // not everything previously sent to us has been transmitted.
        case USB_EVENT_DATA_REMAINING:
        {
            return 0;
        }

        // We are being asked to provide a buffer into which the next packet
        // can be read. We do not support this mode of receiving data so let
        // the driver know by returning 0. The CDC driver should not be sending
        // this message but this is included just for illustration and
        // completeness.
        case USB_EVENT_REQUEST_BUFFER:
        {
            return 0;
        }

        //
        // We don't expect to receive any other events.  Ignore any that show
        // up in a release build or hang in a debug build.
        default:
        	ASSERT(false);
        	break;
    	}

    return(0);
}

int USBDataGetBuffered(char *mem, int count, bool bMoveReadIdx)
{
	int nRet = 0;
	// Disable interrupts.  If we don't do this there is a race
	// condition which can cause the read index to be corrupted.
	uint32_t ui32IntsOff = MAP_IntMasterDisable();

	nRet = CIRC_BUF_get(&USBRxCBuf, mem, count, bMoveReadIdx);

	// Reenable interrupts.
	if (!ui32IntsOff)
	{
		MAP_IntMasterEnable();
	}
	return nRet;
}

const char* USB_PostInit()
{
	uint32_t ui32IntsOff;
	uint32_t ui32Power = USBLIB_FEATURE_POWER_SELF;

	// Enable USB peripherial
	SysCtlPeripheralEnable(SYSCTL_PERIPH_USB0);
	SysCtlUSBPLLEnable();
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UDMA);

	// Configure the required pins for USB operation.
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	MAP_GPIOPinTypeUSBAnalog(GPIO_PORTD_BASE, GPIO_PIN_5 | GPIO_PIN_4); // Enable the D+/D- as USB pins.
	MAP_GPIOPinTypeUSBAnalog(GPIO_PORTB_BASE, GPIO_PIN_1); // Enable PB1 as VBUS pin.

#ifdef TARGET_IS_TM4C123_RA1 // Erratum workaround for silicon revision A1.  VBUS must have pull-down.
    HWREG(GPIO_PORTB_BASE + GPIO_O_PDR) |= GPIO_PIN_1;
#endif
	// Initialize the transmit and receive buffers.
	USBBufferInit(&g_sTxBuffer);
	USBBufferInit(&g_sRxBuffer);
	CIRC_BUF_init(&USBRxCBuf, USBRxBuf, USB_RX_BUFFER_SIZE);

	// Set the USB stack mode to Device mode
	USBStackModeSet(0, eUSBModeForceDevice, 0);

	// Disable VBUS power supply
	USBDCDFeatureSet(0, USBLIB_FEATURE_POWER, &ui32Power);

	// Disable interrupts because in some cases after devise restarts
	ui32IntsOff = MAP_IntMasterDisable();

	USBIntRegister(USB0_BASE, USB0DeviceIntHandler);

	// Pass our device information to the USB library and place the device on the bus.
	const char* pszRet = USBDCDCInit(0, &g_sCDCDevice) ? NULL : STR_ERROR;

	if (!ui32IntsOff)
	{
		MAP_IntMasterEnable();
	}

	return pszRet;
}
