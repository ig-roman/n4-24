#include "SerialCmdParser.h"
#include "Common.h"
#include "LogicInterface.h"
#include <stdlib.h>
#include "Drivers/uart.h"
#include "Drivers/iLcdSpi.h"
#include "Drivers/gpib.h"
#include "Drivers/xPort.h"
#include "Drivers/usb/usb_dev_serial.h"

UI_FEEDBACK_DATA gs_UIFeedbackData = {0};
POINT_T s_touchPoint = {0};

PORTNAME s_eReplyPort = PORTNAME_END;

#define INST_MAX_LINE_LEN (INST_INPUT_BUFFER_MAX_SIZE - 1)

#define LCD_SCI_WAIT_TIME_OUT 100
#define LCD_SCI_NEXT_BYTE_TIME_OUT 2

uint32_t s_unWaitTimeOut = 0;

/** **********************************************************************
 * Returns data from I/O port which is stored in internal buffer of port
 *
 * @param ePort			required port
 * @param pMem			pointer to memory buffer
 * @param nCount		count bytes to copy/move from internal buffer to pMem
 * @param bMoveReadIdx	(IN) buffer length.  (OUT)count bytes retrieved from internal buffer to pMem
 *
 * @return	returns count of copied/moved data
 *
 *********************************************************************** */
int GetPortDataBuffered(PORTNAME ePort, char *pMem, int nCount, bool bMoveReadIdx)
{
	int nRet;
 	if (PORTNAME_DISP == ePort)
	{
		nRet = ILCDSPI_GetCmd(pMem, nCount, bMoveReadIdx);
	}
	else if (PORTNAME_USB == ePort)
	{
		nRet = USBDataGetBuffered(pMem, nCount, bMoveReadIdx);
	}
	else
	{
		nRet = UARTDataGetBuffered(ePort, pMem, nCount, bMoveReadIdx);
	}
	return nRet;
}

/** **********************************************************************
 * Reading some known data from I/O port which is already received to internal buffer or will be received soon
 *
 * @param ePort			required port
 * @param pBuf			pointer to memory buffer
 * @param pnBuffDataLen	buffer length
 *
 * @return	returns true on success
 *
 *********************************************************************** */
bool SCP_ReadPortData(PORTNAME ePort, char* pBuf, unsigned int* pnBuffDataLen)
{
	return SCP_ReadPortDataCustom(ePort, pBuf, pnBuffDataLen, LCD_SCI_WAIT_TIME_OUT, LCD_SCI_NEXT_BYTE_TIME_OUT, false);
}

/** **********************************************************************
 * Reading some known data from I/O port which is already received to internal buffer or will be received soon
 *
 * @param ePort			required port
 * @param pBuf			pointer to memory buffer
 * @param pnBuffDataLen	buffer length
 *
 * @return	returns true on success
 *
 *********************************************************************** */
bool SCP_ReadPortDataCustom(PORTNAME ePort, char* pBuf, unsigned int* pnBuffDataLen, uint32_t unWaitPeriod, uint32_t unBytePeriod, bool bDetectNewLines)
{
	char bRet = 0;

	// Set maximum data receive timeout
	SetUpdateTime(unWaitPeriod, &s_unWaitTimeOut);

	// Wait response and read expected data from buffer
	unsigned int unMaxBuffSize = *pnBuffDataLen;
	*pnBuffDataLen = 0;
	while (*pnBuffDataLen < unMaxBuffSize)
	{
		char chData = 0;
		if (GetPortDataBuffered(ePort, &chData, 1, true)) // ST TODO: optimize
		{
			// Response stream detected.
			pBuf[*pnBuffDataLen] = chData;
			(*pnBuffDataLen)++;

			// Reduce time out delay, because delay between bytes usually limited by speed of serial port.
			SetUpdateTime(unBytePeriod, &s_unWaitTimeOut);
			bRet = 1;

			if (bDetectNewLines && ('\r' == chData || '\n' == chData))
			{
				// Get all trailing new line characters
				while (1 == GetPortDataBuffered(ePort, &chData, 1, false) && ('\r' == chData || '\n' == chData))
				{
					GetPortDataBuffered(ePort, &chData, 1, true);
					pBuf[*pnBuffDataLen] = chData;
					(*pnBuffDataLen)++;
				}
				break;
			}
		}

		if (CheckUpdateTimeForLoop(unWaitPeriod, &s_unWaitTimeOut))
		{
			if (bRet)
			{
				// Incomplete data received. Less than expected.
				ReportError("ReadPortData() - Incomplete data is received. Less than expected.");
				break;
			}
			else
			{
				// Nothing received in given timeout.
				ReportError("ReadPortData() - Nothing is received in given timeout.");
				break;
			}
		}
	}

	return bRet;
}

/** **********************************************************************
 * Removes trailing '\n' and '\r' characters from buffer
 *
 * @param pMem			pointer to memory buffer
 * @param pnBuffDataLen	(IN) buffer length. (OUT) new buffer length
 *
 *********************************************************************** */
void ClearNewLines(const char* pBuf, unsigned int* pnBuffDataLen)
{
	// Remove new line character from then of line
	while ('\n' == pBuf[(*pnBuffDataLen) - 1] || '\r' == pBuf[(*pnBuffDataLen) - 1])
	{
		*pnBuffDataLen -= 1;
	}
}

/** **********************************************************************
 * Adds port ID for response to same as receivers and routes data to target port
 *
 * @param pMem			pointer to memory buffer
 * @param pnBuffDataLen	(IN) buffer length. (OUT) new buffer length
 *
 *********************************************************************** */
void RouteAndSendRemoteData(const char* pBuf, unsigned int nBuffDataLen)
{
	RouteAndSendData(s_eReplyPort, pBuf, nBuffDataLen);
}

/** **********************************************************************
 * Handles new incoming data from I/O ports
 *
 * @param ePort	required port
 * @param pBuf	pointer to memory buffer
 * @param nLen	buffer length
 *
 *********************************************************************** */
void OnRemoteDataRecieved(PORTNAME ePort, const char* pBuf, unsigned int nLen)
{
	s_eReplyPort = ePort;
	ClearNewLines(pBuf, &nLen);
	Logic_HandleRemoteCtrlData(pBuf, nLen);
}

void GPIB_OnDataRecieved(const char* pBuf, unsigned int nLen)	{ OnRemoteDataRecieved(PORTNAME_GPIB, pBuf, nLen); }
void XPORT_OnDataRecieved(const char* pBuf, unsigned int nLen)	{ OnRemoteDataRecieved(PORTNAME_XPORT, pBuf, nLen); }

/** **********************************************************************
 * Sends data to appropriate hardware driver according to port ID
 *
 * @param ePort	port ID
 * @param pBuf	pointer to memory buffer
 * @param nLen	langht of buffer
 *
 *********************************************************************** */
void RouteAndSendData(PORTNAME ePort, const char* pBuf, unsigned int nLen)
{
	switch (ePort)
	{
		case PORTNAME_GPIB:		GPIB_Send			(pBuf, nLen);			break;
		case PORTNAME_DISP:		ILCDSPI_SendCmd		((uint8_t*)pBuf, nLen); break;
		case PORTNAME_USB:		USB_Send			(pBuf, nLen); 			break;
		case PORTNAME_XPORT:	XPORT_Send			(pBuf, nLen); 			break;
		default:				UARTDataPutBuffered	(ePort, pBuf, nLen); 	break;
	}
}

/** **********************************************************************
 *
 * Periodically checks data in UART buffers.
 *
 *********************************************************************** */
void ManageSerial()
{
	const int c_portCount = 3;
	PORTNAME activePort[c_portCount];
	activePort[0] = PORTNAME_XPORT;
	activePort[1] = PORTNAME_USB;
	activePort[2] = PORTNAME_REAR_COM;

#ifndef LAUNCHPAD
	GPIB_HandleInterrupts();
#endif

	int i;
    // Check data size in buffer and get copy of them
	for(i = 0; i < c_portCount; i++)
	{
		int nRemoteBuffLen = GetPortDataBuffered(activePort[i], NULL, INST_MAX_LINE_LEN, false);
		if (nRemoteBuffLen > 0)
		{
			char* szBuff = (char*)malloc((nRemoteBuffLen + 1) * sizeof(char));
			nRemoteBuffLen = GetPortDataBuffered(activePort[i], szBuff, nRemoteBuffLen, false);

			// Handle each line separately.
			char* pszCmdStart = szBuff;
			char* pszCmdStop = szBuff;
			while (pszCmdStop - szBuff < nRemoteBuffLen)
			{
				// Protection from long invalid string
				int nCmdLen = pszCmdStop - pszCmdStart;
				bool bOverload = nCmdLen == INST_MAX_LINE_LEN;

				if (NULL == pszCmdStop || '\n' == *pszCmdStop || '\r' == *pszCmdStop || bOverload)
				{
					// If something is parsed don't execute the command handler
					// If nothing is parsed command handler returns error for such string
					if (!bOverload || pszCmdStart == szBuff)
					{
						// For zero size string new line character must be cleared out of buffer
						GetPortDataBuffered(activePort[i], NULL, nCmdLen + 1, true);

						*pszCmdStop = NULL;
						if (nCmdLen > 1)
						{
							OnRemoteDataRecieved(activePort[i], pszCmdStart, nCmdLen);
						}
						// Don't start string from zero
						pszCmdStart = pszCmdStop + 1;
					}
				}

				pszCmdStop++;
			}
			free(szBuff);
		}
	}

	char chData;
	if (GetPortDataBuffered(PORTNAME_AARM, &chData, 1, true))
	{
		// N.B! Reading of known data with known size works more quickly because no additional timeouts in ReadPortData() function
		if (ACK == (uint8_t)chData || NACK == (uint8_t)chData)
		{
			Logic_HandleControlData(&chData, 1);
		}
		else
		{
			char buffData[AARM_STATUS_RESPONSE_LENGTH] = {0};
			unsigned int buffDataLen = sizeof(buffData) - 1;
			// N.B! Reading of known data with known size works more quickly because no additional timeouts in ReadPortData() function
			if (SCP_ReadPortData(PORTNAME_AARM, buffData + 1, &buffDataLen) && (AARM_STATUS_RESPONSE_LENGTH - 1) == buffDataLen && STRCMD_END == buffData[AARM_STATUS_RESPONSE_LENGTH - 1])
			{
				buffData[0] = chData;
				Logic_HandleControlData(buffData, AARM_STATUS_RESPONSE_LENGTH);
			}
			else
			{
				ReportError("ManageSerial() - Unknown data received from AARM port");
			}
		}
	}

	// Text extend response has no prefix.
	if (ACKDISP_WAIT == gs_UIFeedbackData.eTextExtened)
	{
		char buffData[5] = {0};
		unsigned int buffDataLen = sizeof(buffData);
		if (SCP_ReadPortData(PORTNAME_DISP, buffData, &buffDataLen))
		{
			// Buffer contains some info
			if (5 == buffDataLen && 0x06 == buffData[4])
			{
				// ACK code and data received.
				gs_UIFeedbackData.UITextSize.X = (((uint8_t)buffData[0]) << 8) + (uint8_t)buffData[1];
				gs_UIFeedbackData.UITextSize.Y = (((uint8_t)buffData[2]) << 8) + (uint8_t)buffData[3];
				gs_UIFeedbackData.eTextExtened = ACKDISP_OK;
			}
			else
			{
				gs_UIFeedbackData.eTextExtened = ACKDISP_FAILED;
				ReportError("ManageSerial() - ACK failed for GetTextextend function");
			}
		}
	}
	else
	{
		if (GetPortDataBuffered(PORTNAME_DISP, &chData, 1, true))
		{
			char bIsValidData = 0;

			uint8_t unData = (uint8_t)chData;
			switch (unData)
			{
				// Scan port for touch events
				// 4B, FF, 0016, 0007, 06 - Push event (K key_char ev_coord_x ev_coord_y [ACK])
				// 6B, FF, 0016, 0007, 06 - Release event (k key_char ev_coord_x ev_coord_y [ACK])
				case 0x4B:
				case 0x6B:
				{
					char buffData[6] = {0};
					unsigned int buffDataLen = sizeof(buffData);
					uint8_t bIsPush = 0x4B == unData;

					// N.B! Reading of known data with known size works more quickly because no additional timeouts in ReadPortData() function
					buffDataLen = 2;
					bool bReadOk = SCP_ReadPortData(PORTNAME_DISP, buffData, &buffDataLen);

					if (bReadOk && 2 == buffDataLen && 0xFF != buffData[0] && LCD_ACK == buffData[1])
					{
						int8_t* punCode = bIsPush ? &gs_UIFeedbackData.unKeyCodePush : &gs_UIFeedbackData.unKeyCodeRelease;

						*punCode = buffData[0];

						bIsValidData = true;
					}
					else
					{
						buffDataLen = 4;
						bool bReadOk = SCP_ReadPortData(PORTNAME_DISP, buffData + 2, &buffDataLen);
						if (bReadOk && 4 == buffDataLen && 0xFF == buffData[0] && LCD_ACK == buffData[5])
						{
							// Make sure that previous command is completely (touch and release) handled.
							if (!gs_UIFeedbackData.pUIReleasePoint)
							{

								if (bIsPush)
								{
									s_touchPoint.X = ((uint16_t)buffData[1] << 8) + (uint8_t)buffData[2];
									s_touchPoint.Y = ((uint16_t)buffData[3] << 8) + (uint8_t)buffData[4];
								}

								// UI part resets this pointer when event is handled
								PPOINT_T* ppPoint = bIsPush ? &gs_UIFeedbackData.pUITouchPoint : &gs_UIFeedbackData.pUIReleasePoint;
								*ppPoint = &s_touchPoint;
							}
							bIsValidData = true;
						}
						else
						{
							ReportError("ManageSerial() - Invalid touch event");
						}
					}

					break;
				}
				default:
				{
					if (ACKDISP_WAIT == gs_UIFeedbackData.UIACKCode && (LCD_ACK == unData || LCD_NACK == unData ))
					{
						// UI part changes UIACKCode to ACKDISP_NONE when value is handled.
						gs_UIFeedbackData.UIACKCode = LCD_ACK == unData ? ACKDISP_OK : ACKDISP_FAILED;
						bIsValidData = 1;
					}
				}
			}

			if (!bIsValidData)
			{
				ReportError("ManageSerial() - Unknown data received from LCD port");
			}
		}
	}
}
