
// For CIO only
#include <stdio.h>

#include "Common.h"
#include "Drivers/init.h"
#include "UI.h"
#include "UIInterface.h"
#include "LogicInterface.h"
#include "SerialCmdParser.h"

void ReportError(const char* error)
{
	printf("%s\n", error);
	UI_ShowError(error);
}

/** **********************************************************************
 *
 * Start up entry point. Contains main program loop
 *
 *********************************************************************** */
int main(void)
{
	printf("Starting\n");

	// Drivers and hardware initialization
	Init();

	Logic_Init();	// C++ logic
	UI_Init();		// UI logic

	while (true)
	{
		ManageRunLed();
		ManageBuzzer();
		ManageSerial();
		ManageUI();
		CLogic_Loop();
		Logic_Loop();
	}
}
