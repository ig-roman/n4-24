#ifndef LOGIC_INTERFACE_H
#define LOGIC_INTERFACE_H

#include "CommonTypes.h"
#ifdef __cplusplus
extern "C" {
#endif

#ifndef WIN32
	#define __declspec(dllexport)
	#define __stdcall
#endif

// Calls from C++ to C
__declspec(dllexport) bool __stdcall CLogic_SwitchOut(char bOn, char bHV, char bHVConfirmRequire, char bForce);

__declspec(dllexport) void __stdcall CLogic_StoreByte(uint32_t unAddress, uint8_t un8Data);
__declspec(dllexport) uint8_t __stdcall CLogic_RestoreByte(uint32_t unAddress);
__declspec(dllexport) void __stdcall CLogic_StoreBytes(uint32_t unStartAddress, uint32_t unLength, const uint8_t* punData);
__declspec(dllexport) void __stdcall CLogic_RestoreBytes(uint32_t unStartAddress, uint32_t unLength, uint8_t* punData);


__declspec(dllexport) void __stdcall CLogic_SendControlData(const char* pBuf, int length);
__declspec(dllexport) void __stdcall CLogic_SendRemoteCtrlData(const char* pBuf, int length);

__declspec(dllexport) void __stdcall CLogic_SetLastError(const char*);

__declspec(dllexport) void __stdcall CLogic_ManageSerial();

// Calls from C to C++
__declspec(dllexport) void __stdcall Logic_Loop();
__declspec(dllexport) void __stdcall Logic_Init();
__declspec(dllexport) const char* __stdcall Logic_PostInit();
__declspec(dllexport) void __stdcall Logic_HandleControlData(const char* pBuf, int length);
__declspec(dllexport) void __stdcall Logic_HandleRemoteCtrlData(const char* pSingleLineCmd, int length);
__declspec(dllexport) void __stdcall Logic_SendLLCommand(const char* pBuf, int length);

// Other
__declspec(dllexport) void __stdcall CLogic_Loop();

#ifdef __cplusplus
}
#endif

#endif //LOGIC_INTERFACE_H
