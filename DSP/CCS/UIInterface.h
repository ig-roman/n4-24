#ifndef UI_INTERFACE_H
#define UI_INTERFACE_H

#include "CommonTypes.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef WIN32
	#define __declspec(dllexport)
	#define __stdcall
#endif

__declspec(dllexport) void __stdcall UI_Init();
__declspec(dllexport) void __stdcall UI_Loop();
__declspec(dllexport) void __stdcall UI_ButtonChanged(int8_t, char);
__declspec(dllexport) void __stdcall UI_TouchEvent(uint16_t unX, uint16_t unY, char bIsTouched);
__declspec(dllexport) void __stdcall UI_ShowError(const char* pszError);

__declspec(dllexport) void __stdcall CUI_StartDisplayData(char chStartSeq);
__declspec(dllexport) void __stdcall CUI_SendDisplayData(const char* pBuf, int length);
__declspec(dllexport) char __stdcall CUI_ReadDisplayData(char* pBuf, unsigned int* pnBuffDataLen);
__declspec(dllexport) char __stdcall CUI_WaitDisplayACK();
__declspec(dllexport) void __stdcall CUI_GetTextExtent(uint16_t* punX, uint16_t* punY);
__declspec(dllexport) void __stdcall CUI_SetLastError(char*);
__declspec(dllexport) const char* __stdcall CUI_PostInit(INITSTEP eInitStep, void* pCtx);

#ifdef __cplusplus
}
#endif

#endif // UI_INTERFACE_H
