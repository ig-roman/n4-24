#ifndef COMMONC_H_
#define COMMONC_H_

#include "CommonTypes.h"

#define BEEP_SHORT				1		// ms
#define BEEP_DEFAULT			100		// ms
#define BEEP_LONG				1000	// ms
#define BEEP_DELAY_HV			BEEP_DEFAULT

#define BEEP_PERIOD_HV_BEFORE	150 // ms
#define BEEP_PERIOD_HV_NORMAL	900 // ms
#define BEEP_PERIOD_NO_REPEAT	0

#define BEEP_TIME_HV_BEFORE		(BEEP_PERIOD_HV_BEFORE + BEEP_DELAY_HV) * 10 // ms

#define LCD_ACK				ACK
#define LCD_NACK			NACK
#define LCD_FILED_KEY_CHAR	0x15

extern volatile uint32_t g_ulMilis;

void DelayMS(uint32_t unTime);
void SetUpdateTime(uint32_t unPeriod, uint32_t* punTriggerTime);
bool CheckUpdateTimeForLoop(uint32_t unPeriod, uint32_t* punTriggerTime);

void Beep(uint32_t unTime, uint32_t unRepeatAfter);
void KeyBeep();
void ManageBuzzer();

#endif /*COMMONC_H_*/
