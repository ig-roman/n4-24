/*
 * DrawContext.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "../UIInterface.h"
#include "DrawContext.h"
#include <string.h>

#define SCI_RX_BUF_LEN 512
#define UNUSED_COLOR 0xFF000000

void DrawContext::ResetCache()
{
	m_unOldColor = 0x000000; // iLCD default color is used for optimization
	m_unOldBkgColor = 0xFFFFFF; // iLCD default color is used for optimization

	m_unOldTextAlignFlags = 0;
	m_unOldFontId = 0;
}

void DrawContext::Init()
{
	// \i! { Init }
	StartCommand();
	SendText("!");
	WaitACK();

	// \iDE { Erase screen }
	StartCommand();
	SendText("DE");
	WaitACK();

	// Touch Screen Related Commands
	// \iTW\D799
	// \iTH\D480
	// \iTA\0%
	// \iTK\1
	// \iTO\0

	// \iTW\D799 - Set Touch Field Width
	StartCommand();
	SendText("TW");
	SendWord(MAX_DISPLAY_LENGTH);
	WaitACK();

	// \iTH\D479 - Set Touch Field Height
	StartCommand();
	SendText("TH");
	SendWord(MAX_DISPLAY_HEIGHT);
	WaitACK();

	// \iTA\0x - Create/Define Touch Field
	StartCommand();
	SendText("TA");
	SendByte(0);
	SendByte(255); // Touch event KEY CHAR
	WaitACK();

	// \iTK\1 - Enable/Disable Reporting Touch-Coordinates
	StartCommand();
	SendText("TK");
	SendByte(0x01);
	WaitACK();

	// \iTO\0 - Enable/Disable Reporting Movements
	StartCommand();
	SendText("TO");
	SendByte(0x00);
	WaitACK();

///*
// ST TODO: cuncomment to skip double buffering
	// \iMD\0 - Set Draw Screen
	StartCommand();
	SendText("MD");
	SendByte(0x00);
	WaitACK();

	// \iMVM - Set View Screen (M - Main screen)
	StartCommand();
	SendText("MV");
	SendByte('M');
	WaitACK();
//*/
	ResetCache();
}

void DrawContext::CopyBuffToMain()
{
///*
	// \iMSSM - Copy Screen To
	StartCommand();
	SendText("MSS");
	SendByte('M');
	WaitACK();
//*/
}

void DrawContext::WriteText(const char* pszText)
{
	// \iDTHello World!\0
	StartCommand();
	SendText("DT");
	SendText(pszText);
	SendByte(0);
	WaitACK();
}

void DrawContext::EraseDisplayArea(uint16_t unDX, uint16_t unDY)
{
	if (MAX_DISPLAY_LENGTH == unDX && MAX_DISPLAY_HEIGHT == unDY)
	{
		// \iDE
		StartCommand();
		SendText("DE");
		WaitACK();
	}
	else
	{
		// \iDe\D400\D0
		StartCommand();
		SendText("De");
		SendWord(unDX);
		SendWord(unDY);
		WaitACK();
	}
}

void DrawContext::InvertDisplayArea(uint16_t unDX, uint16_t unDY)
{
	// \iDi\D100\D240
	StartCommand();
	SendText("Di");
	SendWord(unDX);
	SendWord(unDY);
	WaitACK();
}
void DrawContext::DefineViewport(uint8_t unPortNumber, uint16_t unX, uint16_t unY, uint16_t unDX, uint16_t unDY)
{
	SetAbsoluteCursorPosition(unX, unY);

	// \iCVD\1\0\D100\D100		{ Define Viewport 1 with size of 100x100 }
	StartCommand();
	SendText("CVD");
	SendByte(unPortNumber);
	SendByte(0);
	SendWord(unDX);
	SendWord(unDY);
	WaitACK();

	SelectViewPort(unPortNumber);

	// \iCF\0 { Turn auto-linefeed on (normally on) }
	StartCommand();
	SendText("CF");
	SendByte(0);
	WaitACK();

	ResetCache();
}

void DrawContext::SetTextAlignment(uint8_t unFlags)
{
	if (m_unOldTextAlignFlags != unFlags)
	{
		if (unFlags)
		{
			unFlags |= AL_TURN_ALIGNMENT_ON;
		}
		// \iCT\x81\D0\D0
		StartCommand();
		SendText("CT");
		SendByte(unFlags);
		SendWord(0);
		SendWord(0);
		WaitACK();

		m_unOldTextAlignFlags = unFlags;
	}
}

void DrawContext::SelectViewPort(uint8_t unPortNumber)
{
	// \iCVS\0					{ Select Viewport XX }
	StartCommand();
	SendText("CVS");
	SendByte(unPortNumber);
	WaitACK();

	//ResetCache();
	m_unOldColor = UNUSED_COLOR;
	m_unOldBkgColor = UNUSED_COLOR;
	m_unOldTextAlignFlags = 0xFF;
	m_unOldFontId = 0xFFFF;
}

void DrawContext::SetAbsoluteCursorPosition(uint16_t unX, uint16_t unY)
{
	SelectViewPort(0);
	SetCursorPosition(unX, unY);
}

void DrawContext::SetCursorPosition(uint16_t unX, uint16_t unY)
{
	// \iCK\D0\D0
	StartCommand();
	SendText("CK");
	SendWord(unX);
	SendWord(unY);
	WaitACK();
}

void DrawContext::SetBackgroundColor(uint32_t unColor)
{
	if (m_unOldBkgColor != unColor)
	{
		// \iACB\#E0E0E0
		StartCommand();
		SendText("ACB");
		SendColor(unColor);
		WaitACK();

		m_unOldBkgColor = unColor;
	}
}

void DrawContext::SetColor(uint32_t unColor)
{
	if (m_unOldColor != unColor)
	{
		// \iACF\#FF0000
		StartCommand();
		SendText("ACF");
		SendColor(unColor);
		WaitACK();

		m_unOldColor = unColor;
	}
}

void DrawContext::DrawRectangle(uint8_t unFlags, uint16_t unX, uint16_t unY, uint16_t unDX, uint16_t unDY)
{
	SetCursorPosition(unX, unY);

	// \iDR\x3\D50\D30
	StartCommand();
	SendText("DR");
	SendByte(unFlags);
	SendWord(unDX);
	SendWord(unDY);
	WaitACK();
}

void DrawContext::SetFont(uint16_t unFontId)
{
	if (m_unOldFontId != unFontId)
	{
		// \iAF\D0
		StartCommand();
		SendText("AF");
		SendWord(unFontId);
		WaitACK();
		m_unOldFontId = unFontId;
	}
}

// 0 - backlight off
// 1 - backlight on
// 2 - blink backlight
// 3 - fade-out backlight (when previously on or blinking)
void DrawContext::SetBacklightMode(uint8_t unVal)
{
	// \iCI\0
	StartCommand();
	SendText("CB");
	SendByte(unVal);
	WaitACK();
}

void DrawContext::SetBacklightIntensity(uint8_t unVal)
{
	// \iCB\1
	StartCommand();
	SendText("CI");
	SendByte(unVal);
	WaitACK();
}

// For sleep only
#ifdef WIN32
#include <stdlib.h>
#endif

void DrawContext::WaitACK()
{
	if (!CUI_WaitDisplayACK())
	{
		CUI_SetLastError("WaitACK() - No or invalid acknowledge from display");
	}
}

void DrawContext::GetTextExtent(const char* pszText, uint16_t* punX, uint16_t* punY)
{
	// \iC?TTEXT\0
	StartCommand();
	SendText("C?T");
	SendText(pszText);
	SendByte(0);
	WaitACK();
#ifndef LAUNCHPAD
	CUI_GetTextExtent(punX, punY);
#endif
	#ifdef WIN32
	StartCommand();
	#endif
}

void DrawContext::SendWord(uint16_t unWordData)
{
	uint8_t buffData[] = {(uint8_t)(unWordData >> 8), (uint8_t)unWordData};
	SendDisplayData((char*)buffData, 2);
}

void DrawContext::SendByte(uint8_t unByteData)
{
	SendDisplayData((char*)&unByteData, 1);
}

void DrawContext::SendText(const char* pszText)
{
	SendDisplayData(pszText, strlen(pszText));
}

void DrawContext::SendColor(uint32_t unColor)
{
	uint8_t buffData[] = {(uint8_t)(unColor >> 16), (uint8_t)(unColor >> 8), (uint8_t)unColor & 0x0000FF};
	SendDisplayData((char*)buffData, 3);
}

void DrawContext::SendDisplayData(const char* pBuf, int nLength)
{
	CUI_SendDisplayData(pBuf, nLength);
}

void DrawContext::StartCommand()
{
	char chCmdStart = (char)0xAA;
	CUI_StartDisplayData(chCmdStart);
}
