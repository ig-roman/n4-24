/*
 * PageManager.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

// For CIO only
#include <stdio.h>

#include "PageManager.h"
#include "Common.h"
#include <iostream>
#include <vector>
#include "../UIInterface.h"
#include "../Logic/DeviceState.h"

PageManager g_pageManager;

PageManager::PageManager()
	: m_pActivePage(NULL),
	m_pOldActivePage(NULL),
	m_pNewPage(NULL),
	m_bDCInitialized(false)
{}

PageManager::~PageManager()
{
	DeleteActivePage();
}

/** **********************************************************************
 *
 * Creates instance of page
 * @param enumPage name of page
 *
 *********************************************************************** */
BasePage* PageManager::CreatePage(PAGE enumPage)
{
	BasePage* pNewPage = NULL;
	if (m_pActivePage && m_pOldActivePage && m_pOldActivePage != m_pActivePage)
	{
		// ChangePage() or CreatePage() method is called twice from PageManager::OnButtonChanged()
		// Page forwarding is supported only from one page to another for one key press.
		CUI_SetLastError("CreatePage() - Page forwarding is supported only from one page to another for one key press");
	}
	else
	{
		//int nMemBefore = GetFreeMem();
		pNewPage = m_pageFactory.CreatePage(enumPage);
		//int nMemAfter = GetFreeMem();
		//printf("Page size is:%d\n", nMemBefore - nMemAfter);

	}
	return pNewPage;
}

/** **********************************************************************
 *
 * Shows created page
 * @param pPage created page
 *
 *********************************************************************** */
void PageManager::ShowPage(BasePage* pPage)
{
	const char* pszInfo = NULL;
	const char* pszError = NULL;
	if (m_pActivePage)
	{
		pszInfo = m_pActivePage->GetFrameText(false);
		pszError = m_pActivePage->GetFrameText(true);
		m_pActivePage->SetDC(NULL);
	}

	m_pActivePage = pPage;

	if (!m_bDCInitialized)
	{
		m_DC.Init();
		m_bDCInitialized = true;
	}

	//int nMemBefore = GetFreeMem();

	pPage->SetDC(&m_DC);
	pPage->OnInitPageBase();

	// Copy error and message text from old page to new one
	if (pszInfo)
	{
		pPage->ShowInfo(pszInfo);
	}

	if (pszError)
	{
		pPage->ShowError(pszError);
	}

	//int nMemAfter = GetFreeMem();
	//printf("Page OnInit is:%d Left:%d\n", nMemBefore - nMemAfter, nMemAfter);
}

/** **********************************************************************
 *
 * Creates and sets as active new page. Only 2 pages can be created together and only one can be active.
 * @param enumPage name of page
 *
 *********************************************************************** */
void PageManager::ChangePage(PAGE enumPage)
{
	BasePage* pNewPage = CreatePage(enumPage);
	ShowPage(pNewPage);
}

/** **********************************************************************
 *
 * Removes active page from memory. Only 2 pages can be created together and only one can be active.
 *
 *********************************************************************** */
void PageManager::DeleteActivePage()
{
	if (m_pActivePage)
	{
		delete m_pActivePage;
		m_pActivePage = NULL;
	}
}

/** **********************************************************************
 *
 * Finds active page and makes ButtonChanged call
 *
 * @param chButtonCode	key code of pressed, repeated or released button.
 * @param unY			Y position of toch event
 * @param bIsTouched	true if it is Press event or Repeat event otherwise it is Release event
 *
 *********************************************************************** */
void PageManager::OnButtonChanged(int8_t chButtonCode, bool bIsPressed)
{
	if (m_pActivePage && ValidateCtrlMode())
	{
		m_pOldActivePage = m_pActivePage;
		m_pActivePage->OnButtonChanged(chButtonCode, bIsPressed);

		if (m_pOldActivePage && m_pOldActivePage != m_pActivePage)
		{
			delete m_pOldActivePage;
			m_pOldActivePage = NULL;
		}
	}
}

/** **********************************************************************
 *
 * Finds active page and makes TouchEvent call
 *
 * @param unX			X position of toch event
 * @param unY			Y position of toch event
 * @param bIsTouched	true if it is touch event otherwise it is release event
 *
 *********************************************************************** */
void PageManager::TouchEvent(uint16_t unX, uint16_t unY, char bIsTouched)
{
	if (m_pActivePage && ValidateCtrlMode())
	{
		m_pOldActivePage = m_pActivePage;
		m_pActivePage->TouchEvent(unX, unY, bIsTouched);

		if (m_pOldActivePage && m_pOldActivePage != m_pActivePage)
		{
			delete m_pOldActivePage;
			m_pOldActivePage = NULL;
		}
	}
}

/** **********************************************************************
 *
 * The periodic function. Is invoked always.
 * Use CheckUpdateTime() function for custom period.
 *
 *********************************************************************** */
void PageManager::Loop()
{
	if (m_pActivePage)
	{
		m_pOldActivePage = m_pActivePage;
		m_pActivePage->Loop();
		if (m_pOldActivePage && m_pOldActivePage != m_pActivePage)
		{
			delete m_pOldActivePage;
			m_pOldActivePage = NULL;
		}
	}
}

/** **********************************************************************
 *
 * Shows error message on the active page
 * @param error text to show
 *
 *********************************************************************** */
void PageManager::ShowError(const char* pszError) const
{
	// m_bDCInitialized is required because ShowError may be called on UI initialization phase
	if (m_pActivePage && m_bDCInitialized)
	{
		m_pActivePage->ShowError(pszError);
	}
}

void PageManager::HideFrames()
{
	m_pActivePage->HideError();
	m_pActivePage->HideInfo();
}

bool PageManager::ValidateCtrlMode() const
{
	CTRLMODE eCtrlMode = g_deviceState.GetConfig().GetCtrlModePub();
	bool bRet = CTRLMODE_MIXED == eCtrlMode || CTRLMODE_LOCAL == eCtrlMode;

	if (!bRet)
	{
		ShowError(GetLangString(MSG_UI_LOCKED));
	}

	return bRet;
}
