/*
 * CommonDefs.cpp
 *
 *  Created on: 6 ���� 2019 �.
 *      Author: Admin
 */

// N.B. This file created because TI compiler moves const struct arrays to RAM instead of ROM if they are located in Common.h or Common.cpp (is it a bug?)

#include "Common.h"
#include "Pages/UILocalization.h"

const UNITDATA UNITS_CURRENT[] =
{
	{ RANGE_u, MSG_UA, 1000000 },
	{ RANGE_m, MSG_MA, 1000 },
	{ RANGE_X, MSG_A, 1 },
	{ RANGE_UNK }
};

const UNITDATA UNITS_VOLTAGE[] =
{
	{ RANGE_u, MSG_UV, 1000000 },
	{ RANGE_m, MSG_MV, 1000 },
	{ RANGE_X, MSG_V, 1 },
	{ RANGE_K, MSG_KV, 0.001 },
	{ RANGE_UNK }
};

const UNITDATA UNITS_FREQUENCY[] =
{
	{ RANGE_X, MSG_HZ, 1 },
	{ RANGE_K, MSG_KHZ, 0.001 },
	{ RANGE_M, MSG_MHZ, 0.000001 },
	{ RANGE_UNK }
};

const UNITDATA UNITS_RESISTANCE[] =
{
	{ RANGE_X, MSG_OHM, 1 },
	{ RANGE_K, MSG_KOHM, 0.001 },
	{ RANGE_M, MSG_MOHM, 0.000001 },
	{ RANGE_UNK }
};

const UNITDATA UNITS_TIME[] =
{
	{ RANGE_X, MSG_MSEC, 1000 },
	{ RANGE_X, MSG_SEC, 1 },
	{ RANGE_K, MSG_MIN, 0.01666666666666666 },
	{ RANGE_M, MSG_HOUR, 0.0002777777777777777 },
	{ RANGE_UNK }
};

const MINMAX MINMAX_FREQ	= { MIN_FREQUENCY_VALUE, MAX_FREQUENCY_VALUE, 0 };
const MINMAX MINMAX_VOL		= { MIN_VOLTAGE_DC_VALUE, MAX_VOLTAGE_DC_VALUE, 0 };
const MINMAX MINMAX_CUR		= { MIN_CURENT_VALUE, MAX_CURENT_VALUE, 0 };
const MINMAX MINMAX_RES		= { MIN_IMP_VALUE, MAX_IMP_VALUE, 0 };

