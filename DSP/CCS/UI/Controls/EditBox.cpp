/*
 * EditBox.cpp
 *
 *  Created on: Nov 11, 2013
 *      Author: Sergei Tero
 */

#include "EditBox.h"
#include <stdio.h>

EditBox::EditBox(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY) : WithNavigation(pParent, unCtrlId, unX, unY),
	m_unCursorPos(0),
	m_unTextLimit(EDIT_DEFAULT_TEXT_LIMIT),
	m_unTextScrollIdx(0),
	m_fnOnBeforeMoveCursor(NULL),
	m_fnOnBeforeCharChange(NULL),
	m_pvCtxBeforeCharChange(NULL),
	m_fnOnBeforeValueChange(NULL),
	m_pvCtxBeforeValueChange(NULL),
	m_fnOnCursorLimit(NULL),
	m_pvCtxOnCursorLimit(NULL),
	m_fnOnAfterValueChange(NULL),
	m_pvCtxAfterValueChange(NULL),
	m_nSizeOfChar(-1),
	m_bIsPasswordField(false)
{
	m_rect.size.X = EDIT_LENGHT;
	m_rect.size.Y = EDIT_HEIGHT;	

	uint16_t unMaxRight = MAX_DISPLAY_LENGTH - unX;
	uint16_t unMaxBottomn = MAX_DISPLAY_HEIGHT - unY;

	if (m_rect.size.X > unMaxRight)
	{
		m_rect.size.X = unMaxRight;
	}

	if (m_rect.size.Y > unMaxBottomn)
	{
		m_rect.size.Y = unMaxBottomn;
	}

	SetCursorMode(CURSOR_MODE_REPLACE);
}

void EditBox::SetSize(uint16_t unDX, uint16_t unDY)
{
	UIControl::SetSize(unDX, unDY);
}

void EditBox::SetFont(FONT enumFont)
{
	m_nSizeOfChar = -1;
	WithNavigation::SetFont(enumFont);
}

void EditBox::Draw()
{
	UIControl::Draw();
	DrawContext* pDC = GetDC();
	if (pDC && GetVisible())
	{
		uint32_t unBkgColor = GetBackgroundColor();

		pDC->SelectViewPort(0);
		pDC->SetBackgroundColor(unBkgColor);
		pDC->SetCursorPosition(GetRect().position.X, GetRect().position.Y);
		pDC->EraseDisplayArea(GetRect().size.X, GetRect().size.Y);

		pDC->DefineViewport(1, GetRect().position.X, GetRect().position.Y, GetRect().size.X - GetRightOffset(), GetRect().size.Y);

		uint8_t unAL = GetAlignment();
		const char* pszText = GetValue();
		std::string strVal = m_bIsPasswordField ? std::string(m_strValue.length(), '*') :  std::string(pszText ? m_strValue : "");
		if (pszText)
		{
			// Limit text according to textbox length.
		
			uint16_t m_unEditLeftBound = m_unTextScrollIdx + m_unEditLen;
			if (strVal.size() >= m_unEditLeftBound)
			{
				strVal = strVal.substr(strVal.length() - m_unEditLeftBound, m_unEditLen);
			}

			pDC->SetFont(GetFont());
			pDC->SetTextAlignment(unAL);

			pDC->SetColor(GetColor());
			pDC->SetBackgroundColor(unBkgColor);

			pDC->WriteText(strVal.c_str());
		}

		// Draw cusrssor
		if (GetActive() && m_unCursorPos >= m_unTextScrollIdx)
		{
			uint16_t m_unCursorPosRev = (strVal.length() - m_unCursorPos) + m_unTextScrollIdx;
			uint16_t unXTotal = 0, unXSelChar = 0, unY = 0;

			char c = (strVal.length() > m_unCursorPosRev) ? strVal[m_unCursorPosRev] : '\0';
			char szBuf[] = {c, NULL};
			pDC->GetTextExtent(szBuf, &unXSelChar, &unY);

			if ((unAL & AL_RIGHT_JUSTIFY_TEXT))
			{
				const std::string& strAfter = strVal.substr(m_unCursorPosRev, strVal.size() - m_unCursorPosRev);
				pDC->GetTextExtent(strAfter.c_str(), &unXTotal, &unY);
				unXTotal = GetRect().size.X - unXTotal - GetRightOffset();
			}
			else
			{
				const std::string& strBefore = strVal.substr(0, m_unCursorPosRev);
				pDC->GetTextExtent(strBefore.c_str(), &unXTotal, &unY);
			}


			// Draw cursor when no content in edit box
			unXSelChar = MAX(1, unXSelChar);
			unY = MIN(m_rect.size.Y, unY ? unY : m_rect.size.Y);

			pDC->SetCursorPosition(unXTotal, 0);
			pDC->InvertDisplayArea(unXSelChar, unY);

			//pDC->SetTextAlignment(AL_DO_NOT_WORD_WRAP_TEXT);
			//pDC->SetBackgroundColor(GetColor());
			//pDC->SetColor(unBkgColor);
			//char szBuf[] = {strVal[m_unCursorPosRev], NULL};
			//pDC->WriteText(szBuf);
		}
	}
}

void EditBox::MoveCursor(bool bRight, bool bLRButton /* = false */)
{
	bool bCanMove = OnBeforeMoveCursor(bRight);

	if (bCanMove)
	{
		bCanMove = NULL == m_fnOnBeforeMoveCursor || m_fnOnBeforeMoveCursor(this, m_unEditLen, m_unCursorPos, bRight, NULL);
	}
	
	if (bCanMove)
	{
		uint16_t bOldPos = m_unCursorPos;
		if (bRight)
		{
			if (m_unCursorPos > 0)
			{
				if (CURSOR_MODE_INSERT == m_enumCursorMode || 1 != m_unCursorPos)
				{
					m_unCursorPos--;

					if (m_unCursorPos <= m_unTextScrollIdx && m_unTextScrollIdx > 0)
					{
						// Scroll text to left if visible area too small and cursor position is out of right bound.
						m_unTextScrollIdx = m_unCursorPos - 1;
					}
					SetInvalid();
				}
			}
		}
		else
		{
			if (m_strValue.size() > m_unCursorPos)
			{
				m_unCursorPos++;
				if (m_unCursorPos > m_unTextScrollIdx + m_unEditLen)
				{
					// Scroll text to right if visible area too small and cursor position is out of left bound.
					m_unTextScrollIdx = m_unCursorPos - m_unEditLen;
				}
				SetInvalid();
			}
		}

		if (bOldPos == m_unCursorPos && bLRButton)
		{
			if (OnCursorLimit(bRight) && m_fnOnCursorLimit)
			{
				m_fnOnCursorLimit(this, m_unEditLen, m_unCursorPos, bRight, m_pvCtxOnCursorLimit);
			}
		}
	}
}

bool EditBox::OnBeforeCharChange(int8_t & chVal, bool nInsert, bool nDelete)
{
	return m_fnOnBeforeCharChange == NULL || m_fnOnBeforeCharChange(this, chVal, nInsert, nDelete, m_pvCtxBeforeCharChange);
}

/** **********************************************************************
 *
 * Virtual function to handle or lock any events from physical keyboard
 *
 * @param chButtonCode	key code of pressed, repeated or released button.
 * @param bIsPressed	true if button is pressed or repeat event otherwise it was released
 *
 *********************************************************************** */
void EditBox::OnButtonChanged(int8_t chButtonCode, bool bIsPressed)
{
	WithNavigation::OnButtonChanged(chButtonCode, bIsPressed);

	if (GetVisible() && bIsPressed  && BTN_TAB != chButtonCode)
	{
		string strValue = m_strValue;
		if (BTN_LEFT == chButtonCode)
		{
			MoveCursor(false, true);
		}
		else if (BTN_RIGHT == chButtonCode)
		{
			MoveCursor(true, true);
		}
		else
		{
			if (CURSOR_MODE_INSERT == m_enumCursorMode)
			{
				if (BTN_CLEAR == chButtonCode)
				{
					if (strValue.size() > 0)
					{
						if (OnBeforeCharChange(chButtonCode, true, true))
						{
							strValue.clear();
							m_unCursorPos = 0;
							m_unTextScrollIdx = 0;
							SetValue(strValue.c_str());
						}
					}
				}
				else if (BTN_BACKSPACE == chButtonCode)
				{
					if (strValue.size() > m_unCursorPos)
					{
						uint8_t unPos = strValue.size() - 1 - m_unCursorPos;
						int8_t chChar = strValue[unPos];
						if (OnBeforeCharChange(chChar, true, true))
						{
							strValue.erase(unPos, 1);

							if ((m_unTextScrollIdx > 0) && ((uint16_t)strValue.size() < m_unTextScrollIdx + m_unEditLen))
							{
								m_unTextScrollIdx--;
							}
							SetValue(strValue.c_str());
						}
					}
				}
				else
				{
					if (strValue.size() < m_unTextLimit)
					{
						int8_t chButtonCodeCopy = chButtonCode;
						if (OnBeforeCharChange(chButtonCodeCopy, true, false))
						{
							// Insert only printable characters
							if (!IsCtrlKeys(chButtonCode))
							{
								strValue.insert(strValue.size() - m_unCursorPos, 1, chButtonCodeCopy);
								SetValue(strValue.c_str());
							}
						}
					}
				}
			}
			else
			{
				if (BTN_BACKSPACE != chButtonCode)
				{
					if (strValue.size() > 0)
					{
						int8_t chButtonCodeCopy = chButtonCode;
						if (OnBeforeCharChange(chButtonCodeCopy, false, false))
						{
							// Insert only printable characters
							if (!IsCtrlKeys(chButtonCode))
							{
								strValue[strValue.size() - m_unCursorPos] = chButtonCodeCopy;
								if (SetValue(strValue.c_str()))
								{
									MoveCursor(true);
								}
							}
						}
					}
				}
				else
				{
					MoveCursor(false);
				}
			}
		}
	}
}

bool EditBox::SetValue(const char* pszVal, bool bCopyString)
{
	// Ignores bCopyString argument
	bool bRet = false;
	string strValue = pszVal;
	string strOldValue = m_strValue;
	if (strValue.length() > m_unTextLimit)
	{
		strValue = strValue.substr(0, m_unTextLimit);
	}

	if (NULL == m_fnOnBeforeValueChange || m_fnOnBeforeValueChange(this, m_strValue, strValue, m_pvCtxBeforeValueChange))
	{
		m_strValue = strValue;

		// Ajust curssor
		if (m_unCursorPos > m_strValue.length())
		{
			m_unCursorPos = m_strValue.length();
		}

		bRet = AlphaNumeric::SetValue(m_strValue.c_str());

		if (m_fnOnAfterValueChange)
		{
			m_fnOnAfterValueChange(this, strOldValue, m_strValue, m_pvCtxAfterValueChange);
		}
	}

	if (0 != strOldValue.compare(m_strValue))
	{
		SetInvalid();
	}
	return bRet;
}

void EditBox::SetCursorMode(CURSOR_MODE enumCursorMode)
{
	m_enumCursorMode = enumCursorMode;
	m_unCursorPos = CURSOR_MODE_REPLACE == m_enumCursorMode ? 1 : 0;
}

/** **********************************************************************
 *
 * Moves cursor under touch point.
 *
 * @param unTouchX		X axis of touch event
 * @param unTouchY		Y axis of touch event
 * @param bIsTouched	if true it is touch event, otherwise release
 *
 *********************************************************************** */
void EditBox::TouchEvent(uint16_t unTouchX, uint16_t unTouchY, char bIsTouched)
{
	if (-1 == m_nSizeOfChar)
	{
		// Measure size of character
		uint16_t unSizeOfChar, unY = 0;
		char szBuf[] = "0";
		GetDC()->SetFont(GetFont());
		GetDC()->GetTextExtent(szBuf, &unSizeOfChar, &unY);
		m_nSizeOfChar = unSizeOfChar;
	}

	bool bToRight = (GetAlignment() & AL_RIGHT_JUSTIFY_TEXT) != 0;
	int16_t nTouchInCtrl = 0;
	if (bToRight)
	{
		nTouchInCtrl = GetRect().position.X - unTouchX + GetRect().size.X - GetRightOffset();
	}
	else
	{
		nTouchInCtrl = unTouchX - GetRect().position.X;
	}

	int16_t nSizeInChars = nTouchInCtrl / m_nSizeOfChar;
		
	int16_t nCursorPos = bToRight ? m_unCursorPos :  (MIN((uint16_t)m_strValue.length(), m_unEditLen)  - m_unCursorPos + 1 + m_unTextScrollIdx);
	int16_t nCharsToCursor =  nSizeInChars - nCursorPos + 1;

	if (!bToRight)
	{
		nCharsToCursor = -nCharsToCursor;
	}

	// Perform curssor movements
	int8_t nIdx = nCharsToCursor < 0 ? -nCharsToCursor : nCharsToCursor;
	while (nIdx > 0)
	{
		uint16_t unOldPos = m_unCursorPos;
		MoveCursor(nCharsToCursor < 0, true);

		int16_t nDiffPos =  unOldPos > m_unCursorPos ? unOldPos - m_unCursorPos : m_unCursorPos - unOldPos;
		if (!nDiffPos)
		{
			// Some callback may prevent curssor movement
			break;
		}

		// Cursor may jump over the dot
		nIdx -= nDiffPos; 
	}
}
