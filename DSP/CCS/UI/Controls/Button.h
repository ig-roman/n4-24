/*
 * Button.h
 *
 *  Created on: Oct 29, 2013
 *      Author: Sergei Tero
 */

#ifndef BUTTON_H_
#define BUTTON_H_

#include "WithNavigation.h"

class Button;
typedef void (* FNOnClick)(Button* pBtn, void* pContext);

/** **********************************************************************
 *
 * Command button, allows to call the provided callback function.
 *
 *********************************************************************** */
class Button : public WithNavigation
{
public:
	Button(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY);
	virtual ~Button() {};
	virtual CTRL_TYPE GetControlType() { return CTRL_BUTTON; };
	
	virtual void Draw();
	virtual void OnButtonChanged(int8_t chButtonCode, bool bIsPressed);
	virtual uint32_t GetColor();
	virtual uint32_t GetBackgroundColor() const;
	virtual void SetTextColor(uint32_t unTextColor)			{ m_unTextColor = unTextColor; };

	void DrawButtonImpl(DrawContext*, const RECT_T& rect);

	void SetOnClickEvent(FNOnClick fnOnClick, void* pContext) { m_fnOnClick = fnOnClick; m_pContext = pContext; };
private:
	uint32_t m_unTextColor;
	FNOnClick m_fnOnClick;
	void* m_pContext;
};

#endif /* BUTTON_H_ */
