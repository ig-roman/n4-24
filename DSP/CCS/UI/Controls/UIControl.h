/*
 * UIControl.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef UICONTROL_H_
#define UICONTROL_H_
#include "../IParent.h"
#include "../Common.h"
#include "../DrawContext.h"

/** **********************************************************************
 *
 * Base implementation for all UI controls.
 *
 *********************************************************************** */
class UIControl
{
public:
	UIControl(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY);
	virtual ~UIControl() {};

	virtual void Draw();
	virtual bool GetWithNavigation()					{ return false; }
	virtual CTRL_TYPE GetControlType()					= 0;
	
	virtual void SetDC (DrawContext* pDC)				{ m_pDC = pDC; }
	uint8_t GetId()										{ return m_unCtrlId; };
	void SetId(uint8_t unCtrlId)						{ m_unCtrlId = unCtrlId; };

	virtual void SetSize(uint16_t unDX, uint16_t unDY)	{ m_rect.size.X = unDX; m_rect.size.Y = unDY; };
	void SetPosition(uint16_t unX, uint16_t unY)		{ m_rect.position.X = unX; m_rect.position.Y = unY; };
	const RECT_T& GetRect()								{ return m_rect; };

	virtual void SetVisible(bool bIsVisible = true)		{ if (m_bIsVisible != bIsVisible) { m_bIsVisible = bIsVisible; SetInvalid();};};
	bool GetVisible()									{ return m_bIsVisible; };

	virtual void SetInvalid(bool bVal = true)			{ m_bIsInvalid = bVal; if (m_bIsInvalid) { m_pParent->HasInvalidChild(); } };
	bool GetInvalid()									{ return m_bIsInvalid; };
	DrawContext* GetDC()								{ return m_pDC; };

	virtual uint32_t GetBackgroundColor() const;
	virtual void SetBackgroundColor(uint32_t unColor);

	virtual void TouchEvent(uint16_t unTouchX, uint16_t unTouchY, char bIsTouched) {};

protected:
	RECT_T m_rect;

private:
	uint8_t m_unCtrlId;
	DrawContext* m_pDC;
	bool m_bIsVisible;
	bool m_bIsInvalid;
	IParent* m_pParent;
	bool m_bUseCustomColor;
	uint32_t m_unBkgColor;
};

#endif /* UICONTROL_H_ */
