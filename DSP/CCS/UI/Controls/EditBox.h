/*
 * EditBox.h
 *
 *  Created on: Nov 11, 2013
 *      Author: Sergei Tero
 */

#ifndef EDITBOXBASE_H_
#define EDITBOXBASE_H_

#include "WithNavigation.h"
#include <string>

#define EDIT_LENGHT 250
#define EDIT_HEIGHT 90

#define EDIT_DEFAULT_TEXT_LIMIT 14

enum CURSOR_MODE { CURSOR_MODE_INSERT, CURSOR_MODE_REPLACE};

using std::string;

class EditBox;

typedef bool (* FNOnValueChange)(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext);
typedef bool (* FNOnBeforeCharChange)(EditBox* pEdit, int8_t& chVal, bool nInsert, bool nDelete, void* pContext);
typedef bool (* FNOnBeforeMoveCursor)(EditBox* pEdit, uint16_t unEditLen, uint16_t unCursorPos, bool bRight,void* pContext);
typedef bool (* FNOnCursorLimit)(EditBox* pEdit, uint16_t unEditLen, uint16_t unCursorPos, bool bRightBound, void* pContext);

class EditBox : public WithNavigation
{
public:
	EditBox(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY);
	virtual ~EditBox() {};
	virtual CTRL_TYPE GetControlType() { return CTRL_EDITBOX; };

	virtual void Draw();
	virtual void OnButtonChanged(int8_t chButtonCode, bool bIsPressed);

	virtual bool SetValue(const char* pszVal, bool bCopyString = false);
	virtual void SetSize(uint16_t unDX, uint16_t unDY);
	virtual bool OnBeforeCharChange(int8_t & chVal, bool nInsert, bool nDelete);

	void SetOnBeforeValueChange(FNOnValueChange fn, void* pCtx)			{ m_fnOnBeforeValueChange = fn;	m_pvCtxBeforeValueChange = pCtx; };
	void SetOnAfterValueChange(FNOnValueChange fn, void* pCtx)			{ m_fnOnAfterValueChange = fn;	m_pvCtxAfterValueChange = pCtx; };
	void SetOnBeforeCharChange(FNOnBeforeCharChange fn, void* pCtx)		{ m_fnOnBeforeCharChange = fn;	m_pvCtxBeforeCharChange = pCtx; };
	void SetOnBeforeMoveCursor(FNOnBeforeMoveCursor fn)					{ m_fnOnBeforeMoveCursor = fn; };
	void SetOnCursorLimit(FNOnCursorLimit fn, void* pCtx)				{ m_fnOnCursorLimit = fn; m_pvCtxOnCursorLimit = pCtx; };
	FNOnCursorLimit GetOnCursorLimitCtx()								{ return (FNOnCursorLimit)m_pvCtxOnCursorLimit; };

	virtual bool OnBeforeMoveCursor(bool bRight) { return true; };
	void SetCursorMode(CURSOR_MODE enumCursorMode);

	virtual void TouchEvent(uint16_t unTouchX, uint16_t unTouchY, char bIsTouched);
	virtual void SetFont(FONT enumFont);

	void SetPasswordField(bool bIsPasswordField)						{ m_bIsPasswordField = bIsPasswordField; };

	template<typename T>
	static T* CreateInstance(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY, uint16_t unDX, uint16_t unDY, uint8_t unAlignmentFlags, uint16_t unEditLen)
	{
		T* pRet = new T(pParent, unCtrlId, unX, unY);
		pRet->SetAlignment(unAlignmentFlags);
		pRet->SetSize(unDX, unDY);
		pRet->m_unEditLen = unEditLen;

		return pRet;
	}

public:
	// ST TODO: make protected when text maesuring will be implemented or create getter
	uint16_t m_unEditLen;
	uint16_t m_unTextLimit;
protected:
	virtual void MoveCursor(bool bRight, bool bLRButton = false);
	virtual bool OnCursorLimit(bool bRight) { return true; };
	virtual uint16_t GetRightOffset() { return 0; };

protected:
	uint16_t m_unCursorPos;
	CURSOR_MODE m_enumCursorMode;

	string m_strValue;
	uint16_t m_unTextScrollIdx;
	FNOnBeforeMoveCursor m_fnOnBeforeMoveCursor;

	FNOnBeforeCharChange m_fnOnBeforeCharChange;
	void* m_pvCtxBeforeCharChange;

	FNOnValueChange m_fnOnBeforeValueChange;
	void* m_pvCtxBeforeValueChange;

	FNOnValueChange m_fnOnAfterValueChange;
	void* m_pvCtxAfterValueChange;

	FNOnCursorLimit m_fnOnCursorLimit;
	void* m_pvCtxOnCursorLimit;

	int16_t m_nSizeOfChar;
	bool m_bIsPasswordField;
};

#endif /* EDITBOXBASE_H_ */
