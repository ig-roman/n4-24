/*
 * Label.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "Label.h"

Label::Label(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY) : AlphaNumeric(pParent, unCtrlId, unX, unY),
	m_bTextUpdated(false)
{}

void Label::Draw()
{
	UIControl::Draw();

	if (GetVisible())
	{
		DrawContext* pDC = GetDC();

		if (m_bTextUpdated)
		{
			pDC->SelectViewPort(0);
			pDC->SetBackgroundColor(GetBackgroundColor());
			pDC->SetCursorPosition(GetRect().position.X, GetRect().position.Y);
			pDC->EraseDisplayArea(m_rect.size.X - 1, m_rect.size.Y - 1);
			m_bTextUpdated = false;
		}
		pDC->DefineViewport(1, GetRect().position.X, GetRect().position.Y, GetRect().size.X, GetRect().size.Y);
		pDC->SetBackgroundColor(GetBackgroundColor());

		// Helps to alighn controls
		//pDC->DrawRectangle(0, 0, 0, GetRect().size.X, GetRect().size.Y);

		const char* pszVal = GetValue();
		if (pszVal)
		{
			// These attributes are cahed on API level
			pDC->SetFont(GetFont());
			pDC->SetTextAlignment(GetAlignment());
			pDC->SetColor(GetColor());

			pDC->WriteText(pszVal);
		}
	}
}

bool Label::SetValue(const char* pszVal, bool bCopyString)
{
	bool bRet = AlphaNumeric::SetValue(pszVal, bCopyString);

	if (GetInvalid())
	{
		m_bTextUpdated = true;
	}
	return bRet;
}

bool Label::SetValue(const char* pszVal)
{
	return SetValue(pszVal, false);
}
