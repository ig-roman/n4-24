/*
 * AlphaNumeric.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef ALPHANUMERIC_H_
#define ALPHANUMERIC_H_

#include "UIControl.h"

/** **********************************************************************
 *
 * Base implementation for controls that may contain letters and numbers.
 *
 *********************************************************************** */
class AlphaNumeric : public UIControl 
{
public:
	AlphaNumeric(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY);
	virtual ~AlphaNumeric();

	virtual const char* GetValue() const		{ return m_pszVal; };
	virtual bool SetValue(const char* pszVal, bool bCopyString = false);

	virtual uint32_t GetColor()					{ return m_unFgColor; };
	virtual void SetColor(uint32_t unColor)		{ m_unFgColor = unColor; };

	virtual void SetFont(FONT enumFont)	{ m_font = enumFont; };
	FONT GetFont()						{ return m_font; };

	void SetAlignment(uint8_t unFlags)	{ n_unAlignmentFlags = unFlags; };
	uint8_t GetAlignment()				{ return n_unAlignmentFlags; };

protected:
	char* m_pszVal;
	uint32_t m_unFgColor;

private:
	FONT m_font;
	uint8_t n_unAlignmentFlags;
	bool m_bCopiedText;
};

#endif /* ALPHANUMERIC_H_ */
