/*
 * EditBoxBand.h
 *
 *  Created on: Nov 13, 2013
 *      Author: Sergei Tero
 */

#ifndef EDITBOXBAND_H_
#define EDITBOXBAND_H_

#include "EditBoxDigit.h"

class EditBoxBand;
typedef bool (* FNOnBeforeByCharInputChange)(EditBoxBand* pEdit, bool bSet, bool bParentFocusChage, const char* pszVal, void* pContext);

/** **********************************************************************
 *
 * A combined UI control which consists of two edit boxes: one for value
 * display, the other for input. Values are constrained to a specific
 * band (range).
 *
 *********************************************************************** */
class EditBoxBand : public EditBoxDigit
{
public:
	EditBoxBand(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY);
	virtual ~EditBoxBand() {};
	virtual CTRL_TYPE GetControlType() { return CTRL_EDITBOX_BAND; };

	virtual void OnButtonChanged(int8_t chButtonCode, bool bIsPressed);
	virtual bool OnBeforeCharChange(int8_t & chVal, bool nInsert, bool nDelete);
	virtual void SetSize(uint16_t unDX, uint16_t unDY);
	virtual bool OnBeforeMoveCursor(bool bRight);
	virtual void SetActive(bool bActive);
	virtual void SetVisible(bool bIsVisible = true);

	EditBoxDigit& GetSlaveCtrl()			{ return *m_editSlave; };
	void SetSlaveCtrl(EditBoxDigit* pCtrl);

	bool SetByCharInput(bool bSet, bool bParentFocusChage);
	void SetOnBeforeByCharInputChange(FNOnBeforeByCharInputChange fn, void* pCtx)	{ m_fnOnBeforeByCharInputChange = fn;	m_pvCtxOnBeforeByCharInputChange = pCtx; };
	virtual void SetOnBeforeActivate(FNOnBeforeActivate fn, void* pCtx);

	PWORKUNIT GetUnit() { m_unitVal.value = GetDValue(); return &m_unitVal; };
	void SetUnit(WORKUNIT& unit, double dMultiplier)
	{
		m_unitVal.value = unit.value;
		m_unitVal.pBand = unit.pBand;
		SetDValue(m_unitVal.value, m_unitVal.pBand->scale, m_unitVal.pBand->fraction, dMultiplier);
	};

private:
	void SetActiveInternal(bool bActive);

private:
	WORKUNIT m_unitVal;
	EditBoxDigit* m_editSlave; // ST TODO: rename
	bool m_bIsByCharInput;

	FNOnBeforeByCharInputChange m_fnOnBeforeByCharInputChange;
	void* m_pvCtxOnBeforeByCharInputChange;

	bool m_bActiveStateStored;
	char m_chFirstButtonCode;
};

#endif /* EDITBOXBAND_H_ */
