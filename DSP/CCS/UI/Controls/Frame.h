/*
 * Button.h
 *
 *  Created on: Oct 29, 2013
 *      Author: Sergei Tero
 */

#ifndef FRAME_H_
#define FRAME_H_

#include "Button.h"
#include "Label.h"
#include "WithNavigation.h"

/** **********************************************************************
 *
 * Dialog frame like a message box
 *
 *********************************************************************** */
class Frame : public WithNavigation, public IParent
{
public:
	Frame(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY);
	virtual ~Frame();
	virtual CTRL_TYPE GetControlType() { return CTRL_FRAME; };
	virtual void SetDC (DrawContext* pDC);

	void SetCaption(const char* pszCaption)								{ m_pLabelCaption->SetValue(pszCaption); };
	virtual bool SetValue(const char* pszVal, bool bCopyString = false)	{ return m_pLabelDescription->SetValue(pszVal, bCopyString); };
	virtual const char* GetValue() const								{ return m_pLabelDescription->GetValue(); };
	
	virtual void SetSize(uint16_t unDX, uint16_t unDY);

	virtual void Draw();

	virtual void HasInvalidChild();
	virtual uint32_t GetBackgroundColor() const;

	virtual void SetColor(uint32_t unColor);
	virtual void SetBackgroundColor(uint32_t unColor);

	virtual void SetVisible(bool bIsVisible = true);
	virtual void SetInvalid(bool bVal = true);
	virtual void OnButtonChanged(int8_t chButtonCode, bool bIsPressed);

private:
	void AdjustPos(uint16_t unX, uint16_t unY, uint16_t unDX, uint16_t unDY);
	void SetChildrenColor(uint32_t unColorFG, uint32_t unColorBG);

private:
	Label* m_pLabelCaption;
	Label* m_pLabelDescription;
	Button* m_pButton;
};

#endif /* FRAME_H_ */
