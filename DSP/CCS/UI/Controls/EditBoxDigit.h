/*
 * EditBoxDigit.h
 *
 *  Created on: Nov 11, 2013
 *      Author: Sergei Tero
 */

#ifndef EDITBOXDIGIT_H_
#define EDITBOXDIGIT_H_

#include "EditBox.h"
#include <string.h>

#include "../Pages/UILocalization.h"
class DigiBoxValidator;

/** **********************************************************************
 *
 * An edit box that allows only floating point numbers, which can be limited
 * to a specified scale and fraction. Also allows to change the digit that
 * is being incremented or decremented.
 *
 *********************************************************************** */
class EditBoxDigit : public EditBox
{
public:
	EditBoxDigit(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY);
	virtual ~EditBoxDigit();
	virtual CTRL_TYPE GetControlType() { return CTRL_EDITBOX_DIGIT; };

	virtual void Draw();
	virtual void OnButtonChanged(int8_t chButtonCode, bool bIsPressed);
	virtual bool OnBeforeCharChange(int8_t & chVal, bool nInsert, bool nDelete);
	virtual void SetSize(uint16_t unDX, uint16_t unDY);

	void SetUnitsText(const char* pszUnitsText, uint8_t unUnitsTextSize);
	const char* GetUnitsText() { return m_pszUnitsText; };

	virtual uint16_t GetRightOffset() { return m_unUnitsTextSize; };

	double GetDValue();
	void SetDValue(double dVal, uint8_t unScale = 0, int8_t nFraction = 0, double dMultiplier = 1);

	uint8_t GetScale()		{ return m_unScale; };
	int8_t GetFraction()	{ return m_nFraction; };
	double GetMultiplier()	{ return m_dMultiplier; };

	static double ParseDValue(const char* pszValue);

	void SetValidator(const Validator* pValidator);
	DigiBoxValidator* GetDigiBoxValidator() { return m_pValidator; };

protected:
	virtual void MoveCursor(bool bRight, bool bLRButton = false);
	virtual bool OnBeforeActivate(bool bBecomeActive);
	virtual bool OnCursorLimit(bool bRight);

private:
	const char* m_pszUnitsText;
	uint8_t m_unScale;
	int8_t m_nFraction;
	double m_dMultiplier;
	uint8_t m_unUnitsTextSize;
	DigiBoxValidator* m_pValidator;
};

class DigiBoxValidator
{
private:
	const Validator* m_pValidator;
	EditBoxDigit* const m_pEdit;
	const UNITDATA* m_pPrevValUnit;

public:
	DigiBoxValidator(EditBoxDigit* pEdit, const Validator* pValidator) : m_pValidator(pValidator), m_pEdit(pEdit), m_pPrevValUnit(NULL) {};

	void OnBeforeActivateValidate(bool bBecomeActive)
	{
		bool bValid = false;
		double dVal = 0;
		if (NULL != m_pEdit->GetValue() && strlen(m_pEdit->GetValue()) > 0)
		{
			dVal = m_pEdit->GetDValue();
			bValid = m_pValidator->validate(dVal);
		}

		if (!bValid)
		{
			m_pValidator->LimitValue(dVal);
		}

		double dMultiplier;
		uint8_t unScale;
		uint16_t unUnitsTextId;
		int8_t nFraction;

		m_pValidator->Normalize(dVal, unScale, nFraction, dMultiplier, unUnitsTextId, m_pPrevValUnit);
		m_pEdit->SetDValue(dVal, unScale, nFraction, dMultiplier);
		m_pEdit->SetUnitsText(::GetLangString(unUnitsTextId), STYLE_UNITS_SIZE_SMALL_CTRLS_UNIVERSAL);
	}

void OnCursorLimitValidate(bool bRightBound)
{
	if (m_pValidator->pUnits)
	{
		double dValue = m_pEdit->GetDValue();

		// Use previous unit to change value range when value is zero 
		const UNITDATA* pValUnit = m_pValidator->FindRangeUnitData(dValue, m_pPrevValUnit);
		if (pValUnit 
			&& (!bRightBound || pValUnit != m_pValidator->pUnits)
			&& (bRightBound || (pValUnit + 1)->eRange != RANGE_UNK))
		{
			const UNITDATA* pValUnitNext = bRightBound ? pValUnit - 1 : pValUnit + 1;
			m_pPrevValUnit = pValUnitNext;
			double dRangeDiffMul = pValUnit->dMul / pValUnitNext->dMul;

			dValue *= dRangeDiffMul;

			m_pEdit->SetDValue(dValue);
			OnBeforeActivateValidate(false);
		}
	}
}

void OnButtonChangedValidate(int8_t chButtonCode, bool bIsPressed)
{
	if (bIsPressed && (BTN_BAND_UP == chButtonCode || BTN_BAND_DOWN == chButtonCode))
	{
		OnCursorLimitValidate(BTN_BAND_DOWN == chButtonCode);
	}
}

};

#endif /* EDITBOXDIGIT_H_ */
