/*
 * Label.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef LABEL_H_
#define LABEL_H_

#include "AlphaNumeric.h"
#include <cstring>

/** **********************************************************************
 *
 * A non-editable control that shows a STATIC text message. When a value
 * is assigned, it is NOT copied into this class and must exist for as long
 * as the control exists.
 *
 *********************************************************************** */
class Label : public AlphaNumeric
{
public:
	Label(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY);
	virtual ~Label() {};
	virtual CTRL_TYPE GetControlType() { return CTRL_LABEL; };
	bool SetValue(const char* pszVal, bool bCopyString);
	virtual bool SetValue(const char* pszVal);

	virtual void Draw();

private:
	bool m_bTextUpdated;
};

#endif /* LABEL_H_ */
