/*
 * WithNavigation.h
 *
 *  Created on: Oct 29, 2013
 *      Author: Sergei Tero
 */

#ifndef WITHNAVIGATION_H_
#define WITHNAVIGATION_H_

#include "AlphaNumeric.h"

class WithNavigation;
typedef bool (* FNOnBeforeActivate)(WithNavigation* pEdit, bool bBecomeActive, void* pContext);

/** **********************************************************************
 *
 * Base implementation for controls that may be navigated using the Tab key
 * or touch events.
 *
 *********************************************************************** */
class WithNavigation :
	public AlphaNumeric
{
public:
	WithNavigation(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY);
	virtual ~WithNavigation() {};

	virtual void OnButtonChanged(int8_t chButtonCode, bool bIsPressed) {};

	virtual bool GetWithNavigation()					{ return m_bWithNavigation; };
	void SetWithNavigation(bool bWithNavigation)		{ m_bWithNavigation = bWithNavigation; };

	virtual void SetActive(bool bActive = true);
	bool GetActive()									{ return m_bActive; };

	virtual void SetOnBeforeActivate(FNOnBeforeActivate fn, void* pCtx)	{ m_fnOnBeforeActivate = fn;	m_pvCtxOnBeforeActivate = pCtx; };

	virtual void SetPressed(bool bPressed = true)		{ if (m_bIsPressed != bPressed) {m_bIsPressed = bPressed; SetInvalid(); } };
	bool GetPressed()									{ return m_bIsPressed; };
		
	virtual void SetToggled(bool bToggled = true)		{ if (m_bIsToggled != bToggled) {m_bIsToggled = bToggled; SetInvalid(); } };
	bool GetToggled()									{ return m_bIsToggled; };

protected:
	virtual bool OnBeforeActivate(bool bBecomeActive)	{ return true;};

protected:
	bool m_bActive;
	bool m_bIsPressed;
	bool m_bIsToggled;

private:
	bool m_bWithNavigation;

	FNOnBeforeActivate m_fnOnBeforeActivate;
	void* m_pvCtxOnBeforeActivate;
};

#endif /* WITHNAVIGATION_H_ */
