/*
 * Button.cpp
 *
 *  Created on: Oct 29, 2013
 *      Author: Sergei Tero
 */

#include "Button.h"

Button::Button(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY) : WithNavigation(pParent, unCtrlId, unX, unY),
	m_fnOnClick(NULL),
	m_pContext(NULL),
	m_unTextColor(COLOR_FG_DEFAULT_BTN)
{
	SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);
	SetBackgroundColor(COLOR_BG_DEFAULT_BTN);
}

void Button::Draw()
{
	UIControl::Draw();
	DrawContext* pDC = GetDC();
	DrawButtonImpl(GetDC(), GetRect());
}

void Button::DrawButtonImpl(DrawContext* pDC, const RECT_T& rect)
{
	if (pDC && GetVisible())
	{
		// Common button area
		pDC->DefineViewport(1, rect.position.X, rect.position.Y, rect.size.X, rect.size.Y);

		uint16_t unBtnClientSizeX = rect.size.X - STYLE_BTN_SHADOW_DEPTH - 1;
		uint16_t unBtnClientSizeY = rect.size.Y - STYLE_BTN_SHADOW_DEPTH - 1;

		// Draw button 3D like frame
		bool bDown = GetToggled() || GetPressed();
		if (bDown)
		{
			pDC->SetBackgroundColor(0x1e3588); // Left-top border
			pDC->SetCursorPosition(0, 0);
			pDC->EraseDisplayArea(rect.size.X - 1, rect.size.Y - 1);

			pDC->SetBackgroundColor(0x3d82be); // Right-bottomn border
			pDC->SetCursorPosition(STYLE_BTN_SHADOW_DEPTH, STYLE_BTN_SHADOW_DEPTH);
			pDC->EraseDisplayArea(unBtnClientSizeX, unBtnClientSizeY);
		}
		else
		{
			pDC->SetBackgroundColor(0x3669c6); // Right-bottomn border
			pDC->SetCursorPosition(0, 0);
			pDC->EraseDisplayArea(rect.size.X - 1, rect.size.Y - 1);
		
			pDC->SetBackgroundColor(0x5cc5de); // Left-top border
			pDC->SetCursorPosition(0, 0);
			pDC->EraseDisplayArea(unBtnClientSizeX, unBtnClientSizeY);
		}
		uint32_t unBkgColor = GetBackgroundColor();

		// Draw button background
		pDC->SetBackgroundColor(unBkgColor);
		unBtnClientSizeX -= STYLE_BTN_SHADOW_DEPTH;
		unBtnClientSizeY -= STYLE_BTN_SHADOW_DEPTH;
		pDC->SetCursorPosition(STYLE_BTN_SHADOW_DEPTH, STYLE_BTN_SHADOW_DEPTH);
		pDC->EraseDisplayArea(unBtnClientSizeX, unBtnClientSizeY);

		if (GetActive())
		{
			// Paint rectangle inside the button to separate active and inactive buttons
			pDC->SetColor(GetColor());
			pDC->DrawRectangle(0, STYLE_BTN_SHADOW_DEPTH + (bDown ? STYLE_BTN_SHADOW_DEPTH : 0), STYLE_BTN_SHADOW_DEPTH + (bDown ? STYLE_BTN_SHADOW_DEPTH : 0), unBtnClientSizeX - (bDown ? STYLE_BTN_SHADOW_DEPTH : 0), unBtnClientSizeY - (bDown ? STYLE_BTN_SHADOW_DEPTH : 0));
		}

		// Lomit area for button text
		unBtnClientSizeX -= 2;
		unBtnClientSizeY -= 2;
		pDC->DefineViewport(2, rect.position.X + 1 + STYLE_BTN_SHADOW_DEPTH + (bDown ? STYLE_BTN_SHADOW_DEPTH : 0), rect.position.Y + 1 + STYLE_BTN_SHADOW_DEPTH + (bDown ? STYLE_BTN_SHADOW_DEPTH : 0), unBtnClientSizeX, unBtnClientSizeY);

		// Draw button's text
		const char* pszText = GetValue();
		if (pszText)
		{
			pDC->SetFont(GetFont());
			pDC->SetTextAlignment(GetAlignment());
			pDC->SetBackgroundColor(unBkgColor);
			pDC->SetColor(GetColor());
			pDC->WriteText(GetValue());
		}
	}
}

/** **********************************************************************
 *
 * Virtual function to handle or lock any events from physical keyboard
 *
 * @param chButtonCode	key code of pressed, repeated or released button.
 * @param bIsPressed	true if button is pressed or repeat event otherwise it was released
 *
 *********************************************************************** */
void Button::OnButtonChanged(int8_t chButtonCode, bool bIsPressed)
{
	if (chButtonCode == BTN_ENTER && m_fnOnClick && GetVisible())
	{
		SetPressed(bIsPressed);
		if (!bIsPressed)
		{
			m_fnOnClick(this, m_pContext);
		}
	}
}

uint32_t Button::GetColor()
{
	uint32_t unColor = WithNavigation::GetColor();
	unColor = GetToggled() ? unColor : m_unTextColor;
	return unColor;
}

uint32_t Button::GetBackgroundColor() const
{
	uint32_t unColor = AlphaNumeric::GetBackgroundColor();

	if (GetValue())
	{
		if (m_bIsPressed)
		{
			unColor = COLOR_BG_PRESSED;
		}
		else if (m_bIsToggled)
		{
			unColor = COLOR_BG_TOGGLED;
		}
	}

	return unColor;
}
