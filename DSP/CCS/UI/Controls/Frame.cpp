/*
 * Button.cpp
 *
 *  Created on: Oct 29, 2013
 *      Author: Sergei Tero
 */

#include "Frame.h"

#define FRAME_CAPTION_HEIGHT 50
#define FRAME_BUTTON_HEIGHT 60

void OnFrmCloseBtnClicked(Button* pButton, void* pContext)
{
	Frame* pThis = (Frame*)pContext;
	pThis->SetVisible(false);
}

Frame::Frame(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY) : WithNavigation(pParent, unCtrlId, unX, unY),
	m_pLabelCaption(NULL),
	m_pLabelDescription(NULL),
	m_pButton(NULL)
{
	m_pLabelCaption = new Label(this, 0, unX, unY);
	m_pLabelCaption->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	m_pLabelDescription = new Label(this, 0, unX, unY);
	m_pLabelDescription->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	m_pButton = new Button(this, 0, unX, unY);
	m_pButton->SetValue("Close");
	m_pButton->SetOnClickEvent(OnFrmCloseBtnClicked, this);
}

Frame::~Frame()
{
	if (m_pLabelCaption)
	{
		delete m_pLabelCaption;
	}
	if (m_pLabelDescription)
	{
		delete m_pLabelDescription;
	}
	if (m_pButton)
	{
		delete m_pButton;
	}
}

uint32_t Frame::GetBackgroundColor() const
{
	return WithNavigation::GetBackgroundColor();
}

void Frame::SetBackgroundColor(uint32_t unColor)
{
	WithNavigation::SetBackgroundColor(unColor);
	SetChildrenColor(GetColor(), unColor);
}

void Frame::SetColor(uint32_t unColor)
{
	WithNavigation::SetColor(unColor);
	SetChildrenColor(unColor, GetBackgroundColor());
}

void Frame::SetVisible(bool bIsVisible)
{
	WithNavigation::SetVisible(bIsVisible);
	m_pLabelCaption->SetVisible(bIsVisible);
	m_pLabelDescription->SetVisible(bIsVisible);
	m_pButton->SetVisible(bIsVisible);
}

void  Frame::SetInvalid(bool bVal /* = true */)
{
	WithNavigation::SetInvalid(bVal);
}

void Frame::SetChildrenColor(uint32_t unColorFG, uint32_t unColorBG)
{
	m_pLabelCaption->SetColor(unColorBG);
	m_pLabelCaption->SetBackgroundColor(unColorFG);

	m_pLabelDescription->SetColor(unColorFG);
	m_pLabelDescription->SetBackgroundColor(unColorBG);

	m_pButton->SetColor(unColorBG);
}

void Frame::HasInvalidChild()
{
	SetInvalid();
}

void Frame::SetSize(uint16_t unDX, uint16_t unDY)
{
	WithNavigation::SetSize(unDX, unDY);
	AdjustPos(GetRect().position.X, GetRect().position.Y, unDX, unDY);
}

/** **********************************************************************
 *
 * Virtual function to handle or lock any events from physical keyboard
 *
 * @param chButtonCode	key code of pressed, repeated or released button.
 * @param bIsPressed	true if button is pressed or repeat event otherwise it was released
 *
 *********************************************************************** */
void Frame::OnButtonChanged(int8_t chButtonCode, bool bIsPressed)
{
	WithNavigation::OnButtonChanged(chButtonCode, bIsPressed);
	m_pButton->OnButtonChanged(chButtonCode, bIsPressed);
}

void Frame::AdjustPos(uint16_t unX, uint16_t unY, uint16_t unDX, uint16_t unDY)
{
	unX += 2;
	unY += 2;

	unDX -= 4;
	unDY -= 4;

	m_pLabelCaption->SetPosition(unX, unY);
	m_pLabelCaption->SetSize(unDX, FRAME_CAPTION_HEIGHT);

	m_pLabelDescription->SetPosition(unX, unY + FRAME_CAPTION_HEIGHT);
	m_pLabelDescription->SetSize(unDX, unDY - FRAME_CAPTION_HEIGHT - FRAME_BUTTON_HEIGHT);

	m_pButton->SetPosition(((unDX - m_pButton->GetRect().size.X) / 2) + unX, unY + unDY - ((FRAME_BUTTON_HEIGHT - m_pButton->GetRect().size.Y) / 2) - m_pButton->GetRect().size.Y);
}

void Frame::SetDC(DrawContext* pDC)
{
	m_pLabelCaption->SetDC(pDC);
	m_pLabelDescription->SetDC(pDC);
	m_pButton->SetDC(pDC);

	WithNavigation::SetDC(pDC);
}

void Frame::Draw()
{
	DrawContext* pDC = GetDC();
	if (pDC && GetVisible())
	{
		// Common button area
		pDC->DefineViewport(1, GetRect().position.X, GetRect().position.Y, GetRect().size.X, GetRect().size.Y);

		uint16_t unClientSizeX = GetRect().size.X - 1;
		uint16_t unClientSizeY = GetRect().size.Y - 1;
		pDC->SetBackgroundColor(GetBackgroundColor());
		pDC->SetColor(GetColor());
		pDC->DrawRectangle(DR_INNER_FILLED_BACKGROUND_COLOR, 0, 0, unClientSizeX, unClientSizeY);
	}

	m_pLabelCaption->SetInvalid();
	m_pLabelCaption->SetValue(m_pLabelCaption->GetValue()); // To repaint background
	m_pLabelCaption->Draw();

	m_pLabelDescription->SetInvalid();
	m_pLabelDescription->Draw();

	m_pButton->SetInvalid();
	m_pButton->Draw();

	// Clears isInvalid flag
	// Don't put this line before m_pButton->SetInvalid because button calls parent->HasInvalidChild->SetInvalid() and this frame always has invalid flag
	WithNavigation::Draw();
}
