/*
 * AlphaNumeric.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "AlphaNumeric.h"
#include <string.h>

AlphaNumeric::AlphaNumeric(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY)
	: UIControl(pParent, unCtrlId, unX, unY),
	  m_font(FONT_DEFAULT),
	  n_unAlignmentFlags(AL_NONE),
	  m_pszVal(NULL),
	  m_unFgColor(COLOR_FG_DEFAULT),
	  m_bCopiedText(false)
{}

AlphaNumeric::~AlphaNumeric()
{
	if (m_bCopiedText)
	{
		delete m_pszVal;
	}
}

bool AlphaNumeric::SetValue(const char* pszVal, bool bCopyString)
{
	if (m_bCopiedText)
	{
		delete m_pszVal;
		m_bCopiedText = false;
	}

	if (m_pszVal != pszVal || (NULL != pszVal && 0 != strcmp(m_pszVal, pszVal)))
	{
		if (bCopyString && pszVal)
		{
			m_pszVal = new char[strlen(pszVal) + 1];
			strcpy(m_pszVal, pszVal);
			m_bCopiedText = true;
		}
		else
		{
			m_pszVal = (char*)pszVal;
		}
		SetInvalid();
	}
	return true;
}
