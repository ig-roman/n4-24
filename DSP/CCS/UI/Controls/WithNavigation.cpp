/*
 * WithNavigation.cpp
 *
 *  Created on: Oct 29, 2013
 *      Author: Sergei Tero
 */

#include "WithNavigation.h"

WithNavigation::WithNavigation(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY)
	: AlphaNumeric(pParent, unCtrlId, unX, unY),
	m_bActive(false),
	m_bWithNavigation(true),
	m_bIsPressed(false),
	m_bIsToggled(false),
	m_fnOnBeforeActivate(NULL),
	m_pvCtxOnBeforeActivate(NULL)
{}

void WithNavigation::SetActive(bool bActive)
{
	if (m_bActive != bActive)
	{
		if (OnBeforeActivate(bActive))
		{
			if (NULL == m_fnOnBeforeActivate || m_fnOnBeforeActivate(this, bActive, m_pvCtxOnBeforeActivate))
			{
				m_bActive = bActive;
				SetInvalid();
				// ST TODO: return bool?
			}
		}
	}
}
