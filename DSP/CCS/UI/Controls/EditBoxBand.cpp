/*
 * EditBoxBand.cpp
 *
 *  Created on: Nov 13, 2013
 *      Author: Sergei Tero
 */

#include "EditBoxBand.h"
#include <string.h>

bool OnBeforeCharChangeSlave(EditBox* pEdit, char& chVal, bool nInsert, bool nDelete, void* pContext);

EditBoxBand::EditBoxBand(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY) : EditBoxDigit(pParent, unCtrlId, unX, unY),
	m_bIsByCharInput(false),
	m_fnOnBeforeByCharInputChange(NULL),
	m_pvCtxOnBeforeByCharInputChange(NULL),
	m_chFirstButtonCode(0),
	m_editSlave(NULL)
{
}

void EditBoxBand::SetSize(uint16_t unDX, uint16_t unDY)
{
	EditBox::SetSize(unDX, unDY);
	if (m_editSlave)
	{
		m_editSlave->SetSize(180, 35);
		m_editSlave->m_unEditLen = 10;
		m_editSlave->SetPosition(m_rect.position.X + m_rect.size.X - m_editSlave->GetRect().size.X - GetRightOffset() , m_rect.position.Y + m_rect.size.Y);
	}
}

/** **********************************************************************
 *
 * Virtual function to handle or lock any events from physical keyboard
 *
 * @param chButtonCode	key code of pressed, repeated or released button.
 * @param bIsPressed	true if button is pressed or repeat event otherwise it was released
 *
 *********************************************************************** */
void EditBoxBand::OnButtonChanged(int8_t chButtonCode, bool bIsPressed)
{
	const bool bWasShortPress = !bIsPressed && m_chFirstButtonCode <= 1;
	if (!m_bIsByCharInput)
	{
		if (bIsPressed && IsDigitCustom(chButtonCode, true))
		{
			m_chFirstButtonCode++;
		}
		else 
		{
			m_chFirstButtonCode = 0;	
		}
		EditBoxDigit::OnButtonChanged(chButtonCode, bIsPressed);
	}

	if (GetVisible())
	{
		if (m_bIsByCharInput)
		{
			// Send all key events to the slave control
			m_editSlave->OnButtonChanged(chButtonCode, bIsPressed);
		}
		else if (BTN_ENTER == chButtonCode && bIsPressed)
		{
			// N.B. COPY VALUE TO SLAVE - look for this comment for linked use cases 
			// Before activating the slave control the value shoul be copied to it.
			m_editSlave->SetDValue(GetDValue(), GetScale(), GetFraction(), GetMultiplier());
			SetByCharInput(true, false);
		}
		else if (IsDigitCustom(chButtonCode, false) && bWasShortPress)
		{
			// Show the slave control with pressed digit when user has pressed and released the button quickly.

			// N.B. COPY VALUE TO SLAVE - look for this comment for linked use cases 
			// Enable the slave control with an empty value (value shuld be empty here) it pevents setting of devie output value.
			SetByCharInput(true, false);
			
			// Set new digit to slave edit box
			const char pszChar[] = { chButtonCode, NULL };
			m_editSlave->SetValue(pszChar);
		}
	}
}

bool EditBoxBand::OnBeforeCharChange(int8_t & chVal, bool nInsert, bool nDelete)
{	
	return 2 == m_chFirstButtonCode && EditBoxDigit::OnBeforeCharChange(chVal, nInsert, nDelete);
}

bool OnBeforeCharChangeSlave(EditBox* pEdit, int8_t& chVal, bool nInsert, bool nDelete, void* pContext)
{
	if ((nDelete && strlen(pEdit->GetValue()) <= 1) || BTN_ENTER == chVal || BTN_CLEAR == chVal)
	{
		((EditBoxBand*)pContext)->SetByCharInput(false, BTN_ENTER != chVal || nDelete || BTN_CLEAR == chVal);
	}
	return true;
}

bool EditBoxBand::OnBeforeMoveCursor(bool bRight)
{
	bool bRet = !m_bIsByCharInput &&  m_chFirstButtonCode <= 1;

	if (bRet)
	{
		bRet = EditBox::OnBeforeMoveCursor(bRight);
	}
	return bRet;
}

void EditBoxBand::SetVisible(bool bIsVisible)
{
	if (m_bIsByCharInput && !bIsVisible)
	{
		// Disable by char input mode, parent become  invisible.
		SetByCharInput(false, true);
	}

	EditBoxDigit::SetVisible(bIsVisible);
}

bool EditBoxBand::SetByCharInput(bool bSet, bool bParentFocusChage)
{
	bool bRet = false;
	if (m_bIsByCharInput != bSet)
	{
		if (NULL == m_fnOnBeforeByCharInputChange || m_fnOnBeforeByCharInputChange(this, bSet, bParentFocusChage, m_editSlave->GetValue(), m_pvCtxOnBeforeByCharInputChange))
		{
			m_bIsByCharInput = bSet;

			m_editSlave->SetVisible(bSet);

			// Order is mandatory here. Because active control shows his buttons and inactive restores default buttons
			if (bSet)
			{
				SetActiveInternal(false);
				m_editSlave->SetActive(true);
				m_bActiveStateStored = true;
			}
			else
			{
				m_editSlave->SetActive(false);

				SetActiveInternal(m_bActiveStateStored);
			}

			if (!bSet)
			{
				// N.B. COPY VALUE TO SLAVE - look for this comment for linked use cases 
				m_editSlave->SetValue("");
			}
			bRet = true;
		}
	}
	return bRet;
}

void EditBoxBand::SetOnBeforeActivate(FNOnBeforeActivate fn, void* pCtx)
{
	EditBoxDigit::SetOnBeforeActivate(fn, pCtx);
	m_editSlave->SetOnBeforeActivate(fn, pCtx);
}

void EditBoxBand::SetActiveInternal(bool bActive)
{
	WithNavigation::SetActive(bActive);
}

void EditBoxBand::SetActive(bool bActive)
{
	if (m_bIsByCharInput)
	{
		// May happen that activation removed by callback from SetByCharInput();
		// The refore control should inactive after leaving he focus
		// for example use MODE_U open DELTA enter some value and selection should be restored to control wich wa selected before perssinf of delta button.
		m_bActiveStateStored = bActive;
	}

	SetByCharInput(false, true);
	SetActiveInternal(bActive);
}

void EditBoxBand::SetSlaveCtrl(EditBoxDigit* pCtrl)
{
	m_editSlave = pCtrl; 
	m_editSlave->SetCursorMode(CURSOR_MODE_INSERT);
	m_editSlave->SetVisible(false);
	m_editSlave->SetOnBeforeCharChange(OnBeforeCharChangeSlave, this);
	m_editSlave->SetWithNavigation(false);
	m_editSlave->SetColor(COLOR_BG_DEFAULT);
	m_editSlave->SetBackgroundColor(COLOR_FG_DEFAULT);

	// Recalculate slave dimensions
	SetSize(m_rect.size.X, m_rect.size.Y);
}
