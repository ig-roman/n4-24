/*
 * UIControl.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "UIControl.h"
#include <algorithm>

UIControl::UIControl(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY)
	: m_unCtrlId(unCtrlId),
	m_pDC(NULL),
	m_bIsVisible(true),
	m_bIsInvalid(true),
	m_unBkgColor(0),
	m_pParent(pParent),
	m_bUseCustomColor(false)
{
	m_rect.position.X = unX;
	m_rect.position.Y = unY;
	m_rect.size.X = std::min((uint16_t)(MAX_DISPLAY_LENGTH - unX), (uint16_t)DEFAULT_CONTROL_LENGTH);
	m_rect.size.Y = std::min((uint16_t)(MAX_DISPLAY_HEIGHT - unY), (uint16_t)DEFAULT_CONTROL_HEIGHT);
}

uint32_t UIControl::GetBackgroundColor() const
{
	return m_bUseCustomColor ? m_unBkgColor : m_pParent->GetBackgroundColor();
}

void UIControl::SetBackgroundColor(uint32_t unColor)
{
	m_bUseCustomColor = true;
	if (m_unBkgColor != unColor)
	{
		m_unBkgColor = unColor;
		SetInvalid();
	}
}

void UIControl::Draw() 
{
	if (GetDC())
	{
		m_bIsInvalid = false; 
	}
}
