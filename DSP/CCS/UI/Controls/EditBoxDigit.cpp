/*
 * EditBoxDigit.cpp
 *
 *  Created on: Nov 11, 2013
 *      Author: Sergei Tero
 */

#include "EditBoxDigit.h"
#include <stdio.h>
#include <math.h>
#include "string.h"
#include <ctype.h>
#include <stdlib.h>

EditBoxDigit::EditBoxDigit(IParent* pParent, uint8_t unCtrlId, uint16_t unX, uint16_t unY) : EditBox(pParent, unCtrlId, unX, unY),
	m_pszUnitsText(NULL),
	m_unScale(0),
	m_nFraction(0),
	m_dMultiplier(1),
	m_unUnitsTextSize(0),
	m_pValidator(NULL)
{
	SetFont(FONT_DEFAULT_DIGITS);
}

void EditBoxDigit::SetUnitsText(const char* pszUnitsText, uint8_t unUnitsTextSize)
{
	m_pszUnitsText = pszUnitsText;
	m_unUnitsTextSize = unUnitsTextSize;
	// ST TODO: Support units text
	//m_unEditLen = (m_rect.size.X / EDIT_CHAR_LENGTH) - (m_pszUnitsText ? (strlen(m_pszUnitsText) + 1) : 0);
}

/** **********************************************************************
 *
 * Sets value validation and normalization for this element.
 * @param pValidator	rules how to values must be validates and normalized
 *
 *********************************************************************** */
void EditBoxDigit::SetValidator(const Validator* pValidator)
{
	if (m_pValidator)
	{
		delete m_pValidator;
	}

	if (pValidator)
	{
		m_pValidator = new DigiBoxValidator(this, pValidator);
		m_pValidator->OnBeforeActivateValidate(false);
	}
}

bool EditBoxDigit::OnBeforeActivate(bool bBecomeActive)
{
	if (m_pValidator)
	{
		m_pValidator->OnBeforeActivateValidate(bBecomeActive);
	}
	return true;
}

EditBoxDigit::~EditBoxDigit()
{
	if (m_pValidator)
	{
		delete m_pValidator;
	}
}

bool EditBoxDigit::OnCursorLimit(bool bRight)
{
	if (m_pValidator)
	{
		m_pValidator->OnCursorLimitValidate(bRight);
	}
	return true;
}

void EditBoxDigit::SetSize(uint16_t unDX, uint16_t unDY)
{
	EditBox::SetSize(unDX, unDY);
	//if (m_pszUnitsText)
	//{
	//	m_unEditLen -= strlen(m_pszUnitsText) + 1;
	//}
}

double EditBoxDigit::ParseDValue(const char* pszValue)
{
	double dRet = 0;
	if (pszValue && strlen(pszValue) > 0)
	{
		const char*  pszValueTrimmed = (' ' == pszValue[0] ? pszValue + 1 : pszValue);
		dRet = atof(pszValueTrimmed);
	}

	return dRet;
}

double EditBoxDigit::GetDValue()
{
	return EditBoxDigit::ParseDValue(m_strValue.c_str()) / m_dMultiplier;
}

void EditBoxDigit::SetDValue(double dValRaw, uint8_t unScale, int8_t nFraction, double dMultiplier)
{
	double dVal = dValRaw * dMultiplier;
	dVal = -0.0 == dVal ? 0 : dVal;
	double dMulScale = log10(dMultiplier);

	char szFormatBuff[10] = {0};

	double dValAbs = fabs(dVal);
	int8_t nFloorCounted = dValAbs < 1 ? 1 : (int8_t)(log10(dValAbs) + 1);
	int8_t nUiFraction = (unScale ? nFraction : 6) - (int8_t)dMulScale; // 6 default fraction size
	int8_t nFloorDefined = unScale - nUiFraction;
	int8_t nUiScale = MAX(nFloorCounted, nFloorDefined) + nUiFraction;

	bool bNegative = dVal < 0;
	uint8_t unFormatArgResolution = nUiScale + (bNegative ? 1 : 0) + 1; // +1 (+/-)sign, +1 dot, +1 for leading zero if value < 1
	char* pszBuff = new char[unFormatArgResolution + 3]; // +1 (+/-)sign, +1 zero byte, +1 value 9.9999997 rouns to 10.00 and memory leak accures

	if (nUiFraction)
	{
		sprintf(szFormatBuff, "%s%%0%d.%df", bNegative ? "" : " ", unFormatArgResolution, nUiFraction);
		sprintf(pszBuff, szFormatBuff, dVal);
	}
	else
	{
		sprintf(szFormatBuff, "%s%%0%dd", bNegative ? "" : " ", unFormatArgResolution - 1);
		sprintf(pszBuff, szFormatBuff, (int32_t)dVal);
	}

	// Store values to restore them if callback retuns false.
	double dMultiplierCopy = m_dMultiplier;
	int8_t nUiFractionCopy = m_nFraction;
	int8_t nUiScaleCopy = m_unScale;

	// Set scale and fraction before SetValue() because it calls callback and it is used scale and fraction
	m_nFraction = nFraction;
	m_unScale = unScale;
	m_dMultiplier = dMultiplier;

	if (!SetValue(pszBuff))
	{
		m_dMultiplier = dMultiplierCopy;
		m_nFraction = nUiFractionCopy;
		m_unScale = nUiScaleCopy;
	}

	delete[] pszBuff;
}

void EditBoxDigit::MoveCursor(bool bRight, bool bLRButton /* = false */)
{
	EditBox::MoveCursor(bRight, bLRButton);
	
	if (CURSOR_MODE_REPLACE == m_enumCursorMode && m_pszVal)
	{
		char chChar = m_pszVal[strlen(m_pszVal) - m_unCursorPos];
		if ('.' == chChar)
		{
			MoveCursor(bRight);
		}
		else if (!IsDigitCustom(chChar))
		{
			MoveCursor(!bRight);
			if (OnCursorLimit(bRight) && m_fnOnCursorLimit)
			{
				m_fnOnCursorLimit(this, m_unEditLen, m_unCursorPos, bRight, m_pvCtxOnCursorLimit);
			}
		}
	}
}

void EditBoxDigit::Draw()
{
	EditBox::Draw();

	DrawContext* pDC = GetDC();
	if (pDC && GetVisible())
	{
		if (m_pszUnitsText)
		{
			pDC->DefineViewport(1, (GetRect().position.X + GetRect().size.X) - m_unUnitsTextSize + m_unUnitsTextSize / STYLE_UNITS_SPASE_CTRLS_PART, GetRect().position.Y, m_unUnitsTextSize, GetRect().size.Y);

			//uint16_t unDigitsSize = GetRect().size.X - m_unUnitsTextSize;

			pDC->SetTextAlignment(0);
			//pDC->SetCursorPosition(unDigitsSize, 0);
			pDC->SetBackgroundColor(GetBackgroundColor());
			pDC->SetColor(GetColor());
			pDC->SetFont(GetFont());
			pDC->WriteText(m_pszUnitsText);
		}
	}
}

/** **********************************************************************
 *
 * Virtual function to handle or lock any events from physical keyboard
 *
 * @param chButtonCode	key code of pressed, repeated or released button.
 * @param bIsPressed	true if button is pressed or repeat event otherwise it was released
 *
 *********************************************************************** */
void EditBoxDigit::OnButtonChanged(int8_t chButtonCode, bool bIsPressed)
{
	EditBox::OnButtonChanged(chButtonCode, bIsPressed);

	if (GetVisible() && bIsPressed && CURSOR_MODE_REPLACE == m_enumCursorMode)
	{
		if (BTN_UP == chButtonCode || BTN_DOWN == chButtonCode || BTN_UP_WHEEL == chButtonCode || BTN_DOWN_WHEEL == chButtonCode)
		{
			int8_t unDiplayFraction = (int8_t)(m_nFraction - log10(m_dMultiplier));

			double bMul = Pow10Fast(-m_nFraction);

			// Jump over decimal dot if it exists.
			bMul = bMul * Pow10Fast(m_unCursorPos - (unDiplayFraction > 0 && m_unCursorPos > unDiplayFraction ? 2 : 1));

			double dVal = GetDValue();
			double dValNew = BTN_UP == chButtonCode || BTN_UP_WHEEL == chButtonCode ? dVal + bMul : dVal - bMul;

			int8_t nPrevFraction = m_nFraction;
			uint8_t unPrevScale = m_unScale;
			double dPrevMultiplier = m_dMultiplier;

			SetDValue(dValNew, m_unScale, m_nFraction, m_dMultiplier);

			if (nPrevFraction != m_nFraction || unPrevScale != m_unScale || dPrevMultiplier != m_dMultiplier)
			{
				// Move curssor to same valuable position.
				int8_t unFraction = (int8_t)log10(bMul);
				int8_t unNewDiplayFraction = (int8_t)(m_nFraction - log10(m_dMultiplier));
				int8_t unNewCussorPos =  (int8_t)(log10(m_dMultiplier) + unFraction + unNewDiplayFraction + 1);
				if (unNewCussorPos > unNewDiplayFraction)
				{
					unNewCussorPos++;
				}

				if (unNewCussorPos <= 0 )
				{
					m_unCursorPos = 1;
				}
				else if (unNewCussorPos > m_unScale + 1)
				{
					m_unCursorPos = m_unScale + 1;
				}
				else
				{
					m_unCursorPos = unNewCussorPos;
				}
			}
		}
	}

	if (m_pValidator)
	{
		m_pValidator->OnButtonChangedValidate(chButtonCode, bIsPressed);
	}
}

bool EditBoxDigit::OnBeforeCharChange(int8_t & chVal, bool nInsert, bool nDelete)
{
	bool bRet = EditBox::OnBeforeCharChange(chVal, nInsert, nDelete);

	if (bRet)
	{
		if ('-' == chVal && CURSOR_MODE_REPLACE == m_enumCursorMode)
		{
			// Update value to negative
			SetDValue(-GetDValue(), m_unScale, m_nFraction, m_dMultiplier);

			// And return false to skip insertion
			bRet = false;
		}
		else
		{
			bRet = IsCtrlKeys(chVal) || IsDigitCustom(chVal, CURSOR_MODE_REPLACE == m_enumCursorMode);
		}
	}

	return bRet;
}
