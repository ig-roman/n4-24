/*
 * DisplayPage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef LANCONFIGPAGE_H_
#define LANCONFIGPAGE_H_

#include "BaseSettingsPage.h"
#include "../../Controls/EditBoxDigit.h"

class LanConfigPage : public BaseSettingsPage
{
public:
	LanConfigPage();
	virtual ~LanConfigPage() {};

	virtual PAGE GetPageId() { return PAGE_LAN_CONFIG; };
	virtual void OnInitPageProtected();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
private:

private:
	EditBoxDigit* m_pEditIP;
	EditBoxDigit* m_pEditMask;
	EditBoxDigit* m_pEditPort;
};

#endif /* LANCONFIGPAGE_H_ */
