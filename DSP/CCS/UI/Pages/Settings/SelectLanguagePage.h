/*
 * DisplayPage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef SELDISPLAYLANGPAGE_H_
#define SELDISPLAYLANGPAGE_H_

#include "PasswordPage.h"

class SelectLanguagePage : public PasswordPage
{
public:
	SelectLanguagePage();
	virtual ~SelectLanguagePage() {};

	virtual PAGE GetPageId() { return PAGE_SETTINGS_LANGUAGE; };
	virtual void OnInitPageProtected();

	void OnBtnLangClicked(FNBTN enumBtn);

private:
	void SyncButtons();
};

#endif /* SELDISPLAYLANGPAGE_H_ */
