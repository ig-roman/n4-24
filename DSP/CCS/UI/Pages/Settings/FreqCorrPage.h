#ifndef FREQCORRPAGE_H_
#define FREQCORRPAGE_H_

#include "BaseSettingsPage.h"
#include "../../Controls/EditBoxDigit.h"

class FreqCorrPage : public BaseSettingsPage
{
public:
	FreqCorrPage();
	virtual ~FreqCorrPage();

	virtual PAGE GetPageId() { return PAGE_FREQ_CORR; };
	virtual void OnInitPageProtected();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
private:

private:
	EditBoxDigit* m_pEditFreqVal;
};

#endif /* FREQCORRPAGE_H_ */
