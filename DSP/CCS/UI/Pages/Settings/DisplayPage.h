/*
 * DisplayPage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef DISPLAYPAGE_H_
#define DISPLAYPAGE_H_

#include "BaseSettingsPage.h"
#include "../Menu.h"
#include "../IOnClickCompatible.h"

class DisplayPage : public Menu, public IOnClickCompatible
{
public:
	DisplayPage();
	virtual ~DisplayPage() {};

	virtual PAGE GetPageId() { return PAGE_SETTINGS_DISPLAY; };
	virtual void OnInitPage();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);

	void ChangeBacklightIntensity(bool bIncrease);

private:
	uint8_t m_unBacklightIntensity;
};

#endif /* DISPLAYPAGE_H_ */
