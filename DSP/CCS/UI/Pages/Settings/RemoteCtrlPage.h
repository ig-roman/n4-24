/*
 * RemoteCtrlPage.h
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#ifndef REMOTECTRLPAGE_H_
#define REMOTECTRLPAGE_H_


#include "../IOnClickCompatible.h"
#include "../Menu.h"

class RemoteCtrlPage : public Menu, public IOnClickCompatible
{
public:
	RemoteCtrlPage();
	virtual ~RemoteCtrlPage() {};

	virtual PAGE GetPageId() { return PAGE_REMOTE_CTRL; };
	virtual void OnInitPage();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);

private:
	void UpdateButtons();
};

#endif /* REMOTECTRLPAGE_H_ */
