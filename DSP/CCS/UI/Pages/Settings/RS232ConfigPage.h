/*
 * DisplayPage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef RS232ConfigPage_H_
#define RS232ConfigPage_H_

#include "BaseSettingsPage.h"
#include "../../Controls/EditBoxDigit.h"

class RS232ConfigPage : public BaseSettingsPage
{
public:
	RS232ConfigPage() : BaseSettingsPage() {};
	virtual ~RS232ConfigPage() {};

	virtual PAGE GetPageId() { return PAGE_RS232_CONFIG; };
	virtual void OnInitPageProtected();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
	static void OnParBtnClicked(Button* pButton, void* pContext);

private:
	void SyncButtons();

private:
	EditBoxDigit* m_pEditSpeed;
	EditBoxDigit* m_pEditStop;
	RS232_CONF m_rs232Conf;
};

#endif /* RS232ConfigPage_H_ */
