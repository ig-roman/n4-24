/*
 * RemoteCtrlPage.cpp
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#include "RemoteCtrlPage.h"
#include "../../PageManager.h"
#include "../../Controls/Label.h"
#include "../../../Logic/DeviceState.h"

#define FNBTN_MANUAL FNBTN_1
#define FNBTN_MIXED FNBTN_2
#define FNBTN_INTERF FNBTN_3
RemoteCtrlPage::RemoteCtrlPage() : Menu()
{}
/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void RemoteCtrlPage::OnInitPage()
{
	Menu::OnInitPage();

	Label* pLabel = CreateLabel(0, 10, MSG_CTRL_CAPTION, FNBTN_CONF_VERT_START_X, 130);
	pLabel->SetFont(FONT_CAPTION);
	pLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	Label* pLabelInfo = CreateLabel(100, 140, MSG_CTRL_INFO, FNBTN_CONF_VERT_START_X - 100, 280);
	pLabelInfo->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	RegisterFnButton(FNBTN_EXIT, GetLangString(MSG_EXIT), PAGE_SELECT_MODE);

	RegisterFnButton(FNBTN_MANUAL, GetLangString(MSG_MODE_MANUAL),	this, (void*)CTRLMODE_LOCAL);
	RegisterFnButton(FNBTN_MIXED, GetLangString(MSG_MODE_JOINT),	this, (void*)CTRLMODE_MIXED);
	RegisterFnButton(FNBTN_INTERF, GetLangString(MSG_MODE_INTERF),	this, (void*)CTRLMODE_REMOTE);

	UpdateButtons();
}

void RemoteCtrlPage::UpdateButtons()
{
	CTRLMODE eCtrlMode = g_deviceState.GetConfig().GetCtrlModePub();

	Menu::GetFnButton(FNBTN_MANUAL)->SetToggled(CTRLMODE_LOCAL == eCtrlMode);
	Menu::GetFnButton(FNBTN_MIXED)->SetToggled(CTRLMODE_MIXED == eCtrlMode);
	Menu::GetFnButton(FNBTN_INTERF)->SetToggled(CTRLMODE_REMOTE == eCtrlMode);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool RemoteCtrlPage::OnClick(FNBTN enumBtn, void* pContext)
{
	CTRLMODE eCtrlMode = (CTRLMODE)((long)pContext);
	g_deviceState.GetConfig().SetCtrlModePub(eCtrlMode, false);
	UpdateButtons();

	if (CTRLMODE_REMOTE == eCtrlMode)
	{
		g_pageManager.ChangePage(PAGE_SELECT_MODE);
	}

	return true;
}
