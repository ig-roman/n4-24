/*
 * DisplayPage.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "RS232ConfigPage.h"

#include "../../Controls/Label.h"
#include "../../Controls/Button.h"
#include "../../../Logic/DeviceState.h"
#include "../../../UIInterface.h"
#include "../../PageManager.h"
#include <stdio.h>

const double s_arrFixedValues[] = { 400, 1200, 2400, 4800, 9600, 14400, 19200, 28800, 38400, 57600, 115200 };

#define PAR_BUTTON_ID_OFFSET 100

class FixValidator : public Validator
{
public:
	FixValidator(const MINMAX* pMinMax, const UNITDATA* pUnits, uint32_t unFlags = VAL_FLAG_REAL_NUM, uint8_t unScale = 9, uint8_t unFraction = 6) :
		Validator(pMinMax, pUnits, unFlags, unScale, unFraction)
	{};

	void Normalize(double& dValRaw, uint8_t& unScale, int8_t& nFraction, double& dMultiplier, uint16_t& unUnitsTextId, const UNITDATA* pPrevValUnit) const
	{
		double dValPrev = *s_arrFixedValues; 

		bool bIsFound = dValPrev > dValRaw;
		if (bIsFound)
		{
			dValRaw = dValPrev;
		}

		for (int nIdx = 0; !bIsFound && nIdx < sizeof(s_arrFixedValues) / sizeof(double); nIdx++)
		{
			double dVal = s_arrFixedValues[nIdx];

			// Check that value between 2 ranges and select the nearest.
			bIsFound = dValPrev <= dValRaw && dVal >= dValRaw;
			if (bIsFound)
			{
				double dDToPrev = dValRaw - dValPrev;
				double dDToNext = dVal - dValRaw;
				dValRaw = dDToPrev < dDToNext ? dValPrev : dVal;
			}

			dValPrev = dVal;
		}

		// Value more than covered by ranges
		// Use last range
		if (!bIsFound)
		{
			dValRaw = s_arrFixedValues[(sizeof(s_arrFixedValues) / sizeof(double)) - 1];
		}

		Validator::Normalize(dValRaw, unScale, nFraction, dMultiplier, unUnitsTextId, pPrevValUnit);
	};
};

const UNITDATA UNITS_BOD[] =
{
	{ RANGE_X, MSG_BOD, 1 },
	{ RANGE_UNK }
};

const MINMAX MINMAX_RSSPEED = { 300, 115200, DEFAULT_RS232_SPEED };
FixValidator s_validatorRsSpeed(&MINMAX_RSSPEED,	UNITS_BOD, VAL_FLAG_POS_INT_NUM, 6, 0);

const MINMAX MINMAX_RSSTOP = { 1, 2, 1 };
Validator s_validatorRsStop(&MINMAX_RSSTOP,	UNITS_HOST_BITS, VAL_FLAG_POS_INT_NUM, 1, 0);

void RS232ConfigPage::OnParBtnClicked(Button* pButton, void* pContext)
{
	RS232ConfigPage* pThis = ((RS232ConfigPage*)pContext);
	pThis->m_rs232Conf.eParity = (RS232PAR)(pButton->GetId() - PAR_BUTTON_ID_OFFSET);
	pThis->SyncButtons();

	Button* pSaveBtn = pThis->GetFnButton(FNBTN_SAVE);
	pSaveBtn->SetVisible(true);
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void RS232ConfigPage::OnInitPageProtected()
{
	BaseSettingsPage::OnInitPageProtected();

	m_rs232Conf = g_deviceState.GetConfig().GetRs232();

	Label* pCaptionLabel = CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET, MSG_RS232_SETTINGS_CAPTION);
	pCaptionLabel->SetSize(800, STYLE_PAGE_CAPTION_HEIGHT);
	pCaptionLabel->SetFont(FONT_CAPTION);
	pCaptionLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	uint16_t unYoffset = 150;

	CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_BAUD_RATE, STYLE_PAGE_KEY_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);
	m_pEditSpeed = EditBox::CreateInstance<EditBoxDigit>(this, 0, STYLE_PAGE_VALUE_LABEL_XOFFSET, unYoffset, STYLE_PAGE_VALUE_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT, AL_NONE, 16);
	RegisterControl(m_pEditSpeed);
	m_pEditSpeed->SetDValue(m_rs232Conf.unSpeed, 6, 0);
	m_pEditSpeed->SetOnAfterValueChange(LCOnChange, this);
	m_pEditSpeed->SetValidator(&s_validatorRsSpeed);

	IncOffset(unYoffset);
	CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_STOP_BIT, STYLE_PAGE_KEY_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);
	m_pEditStop = EditBox::CreateInstance<EditBoxDigit>(this, 0, STYLE_PAGE_VALUE_LABEL_XOFFSET, unYoffset, STYLE_PAGE_VALUE_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT, AL_NONE, 16);
	RegisterControl(m_pEditStop);
	m_pEditStop->SetDValue(m_rs232Conf.unStopBits, 1, 0);
	m_pEditStop->SetOnAfterValueChange(LCOnChange, this);
	m_pEditStop->SetValidator(&s_validatorRsStop);

	IncOffset(unYoffset, DEFAULT_CONTROL_HEIGHT * 2);
	CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_PARITY, STYLE_PAGE_KEY_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);

	IncOffset(unYoffset, DEFAULT_CONTROL_HEIGHT);
	RS232PAR ePar = (RS232PAR)0;
	uint16_t unXoffset = STYLE_PAGE_LEFT_OFFSET;
	while (RS232PAR_MAX > ePar)
	{
		Button* pButton = new Button(this, PAR_BUTTON_ID_OFFSET + ePar, unXoffset, unYoffset);
		RegisterControl(pButton);
		pButton->SetSize(DEFAULT_CONTROL_LENGTH, FNBTN_CONF_HOR_HEIGHT); 
		pButton->SetValue(GetLangString(GetRS232ParityName(ePar)));
		pButton->SetWithNavigation(true);
		pButton->SetOnClickEvent(OnParBtnClicked, this);

		IncOffset(unXoffset, DEFAULT_CONTROL_LENGTH + 4);
		ePar = (RS232PAR)(ePar + 1);
	}

	SyncButtons();
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool RS232ConfigPage::OnClick(FNBTN enumBtn, void* pContext)
{
	// Trigger on change call back to validate value
	WithNavigation* pCtrl = (WithNavigation*)GetActiveControl();
	if (pCtrl)
	{
		pCtrl->SetActive(false);
		pCtrl->SetActive(true);
	}

	m_rs232Conf.unSpeed = (uint32_t)roundX(m_pEditSpeed->GetDValue(), 0);
	m_rs232Conf.unStopBits = (uint8_t)roundX(m_pEditStop ->GetDValue(), 0);

	// Updating address
	g_deviceState.GetConfig().SetRs232(m_rs232Conf);
	CUI_PostInit(INITSTEP_UART, (void*)&m_rs232Conf);

	return BaseSettingsPage::OnClick(enumBtn, pContext);
}

void RS232ConfigPage::SyncButtons()
{
	RS232PAR eParity = m_rs232Conf.eParity;
	RS232PAR ePar = (RS232PAR)0;

	while (RS232PAR_MAX > ePar)
	{
		Button* pButton = (Button*)FindControlByCtrlId(PAR_BUTTON_ID_OFFSET + ePar);
		pButton->SetPressed(ePar == eParity);
		ePar = (RS232PAR)(ePar + 1);
	}
}
