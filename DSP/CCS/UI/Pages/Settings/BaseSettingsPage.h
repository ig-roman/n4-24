/*
 * BaseSettingsPage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef BASESETTINGSPAGE_H_
#define BASESETTINGSPAGE_H_

#include "PasswordPage.h"
#include "../IOnClickCompatible.h"

extern const UNITDATA UNITS_HOST_BITS[];
class BaseSettingsPage : public PasswordPage, public IOnClickCompatible
{
public:
	BaseSettingsPage();
	virtual ~BaseSettingsPage() {};
	virtual void OnInitPageProtected();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);

protected:
	static bool LCOnChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext);
};

#endif /* BASESETTINGSPAGE_H_ */
