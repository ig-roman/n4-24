/*
 * DisplayPage.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "LanConfigPage.h"

#include "../../Controls/Label.h"
#include "../../Controls/Button.h"
#include "../../../Logic/DeviceState.h"
#include "../../PageManager.h"
#include <stdio.h>

const MINMAX MINMAX_PORT = { 0, 0xFFFF, DEFAULT_SCPI_RAW_PORT };
Validator s_validatorPort(&MINMAX_PORT,	NULL, VAL_FLAG_POS_INT_NUM, 5, 0);

// Class A: 24 bits; Class B: 16 bits; Class C: 8 bits
// Network Class	Host Bits	Netmask
// A				24			255.0.0.0
// B				16			255.255.0.0
// C				8			255.255.255.0
const MINMAX MINMAX_HOST_BITS = { 0, 24, DEFAULT_SCPI_MASK };
Validator s_validatorHostBist(&MINMAX_HOST_BITS, UNITS_HOST_BITS, VAL_FLAG_POS_INT_NUM, 2, 0);

LanConfigPage::LanConfigPage() : BaseSettingsPage()
{
}

bool LCFNOnValueChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext)
{
	return strOldVal.length() == strNewVal.length() && '.' == strNewVal[4] && '.' == strNewVal[8] && '.' == strNewVal[12];
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void LanConfigPage::OnInitPageProtected()
{
	BaseSettingsPage::OnInitPageProtected();

	Label* pCaptionLabel = CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET, MSG_LAN_SETTINGS_CAPTION);
	pCaptionLabel->SetSize(800, STYLE_PAGE_CAPTION_HEIGHT);
	pCaptionLabel->SetFont(FONT_CAPTION);
	pCaptionLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	Label* pInfoLabel = CreateLabel(0, 110, MSG_MASK_INFO);
	pInfoLabel->SetSize(800, 180);
	pInfoLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);
	
	char szBuff[20];
	const LAN_CONF& lanConf = g_deviceState.GetConfig().GetLan();

	uint16_t unYoffset = 300;

	// IP address
	CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_IP_ADDR, STYLE_PAGE_KEY_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);
	m_pEditIP = EditBox::CreateInstance<EditBoxDigit>(this, 0, STYLE_PAGE_VALUE_LABEL_XOFFSET, unYoffset, STYLE_PAGE_VALUE_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT, AL_NONE, 16);
	m_pEditIP->m_unTextLimit = 16;
	RegisterControl(m_pEditIP);
	sprintf(szBuff, " %0.3d.%0.3d.%0.3d.%0.3d", lanConf.unIP1, lanConf.unIP2, lanConf.unIP3, lanConf.unIP4);
	m_pEditIP->SetValue(szBuff, true);
	m_pEditIP->SetOnAfterValueChange(LCOnChange, this);
	m_pEditIP->SetOnBeforeValueChange(LCFNOnValueChange, this);

	// Mask
	IncOffset(unYoffset);
	CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_MASK, STYLE_PAGE_KEY_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);
	m_pEditMask = EditBox::CreateInstance<EditBoxDigit>(this, 0, STYLE_PAGE_VALUE_LABEL_XOFFSET, unYoffset, STYLE_PAGE_VALUE_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT, AL_NONE, 16);
	RegisterControl(m_pEditMask);
	sprintf(szBuff, "%0.3d", lanConf.unMask);
	m_pEditMask->SetValue(szBuff, true);
	m_pEditMask->SetValidator(&s_validatorHostBist);
	m_pEditMask->SetOnAfterValueChange(LCOnChange, this);

	// Port
	IncOffset(unYoffset);
	CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_PORT, STYLE_PAGE_KEY_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);
	m_pEditPort = EditBox::CreateInstance<EditBoxDigit>(this, 0, STYLE_PAGE_VALUE_LABEL_XOFFSET, unYoffset, STYLE_PAGE_VALUE_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT, AL_NONE, 16);
	RegisterControl(m_pEditPort);
	sprintf(szBuff, "%0.5d", lanConf.unPort);
	m_pEditPort->SetValue(szBuff, true);
	m_pEditPort->SetValidator(&s_validatorPort);
	m_pEditPort->SetOnAfterValueChange(LCOnChange, this);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool LanConfigPage::OnClick(FNBTN enumBtn, void* pContext)
{
	LAN_CONF lanConfNew;
	const char* pszIPAddr = m_pEditIP->GetValue();
	pszIPAddr++;
	lanConfNew.unIP1 = (uint8_t)ParseFirstDigit(&pszIPAddr);
	lanConfNew.unIP2 = (uint8_t)ParseFirstDigit(&pszIPAddr);
	lanConfNew.unIP3 = (uint8_t)ParseFirstDigit(&pszIPAddr);
	lanConfNew.unIP4 = (uint8_t)ParseFirstDigit(&pszIPAddr);

	lanConfNew.unMask = (uint8_t)roundX(m_pEditMask->GetDValue(), 0);
	lanConfNew.unPort = (uint16_t)roundX(m_pEditPort->GetDValue(), 0);
	lanConfNew.bForceUpdate = true;

	g_deviceState.GetConfig().SetLan(lanConfNew);

	BaseSettingsPage::OnClick(enumBtn, pContext);
	ShowInfo(GetLangString(MSG_RESTART_REQ));

	return true;
}
