/*
 * BaseSettingsPage.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "BaseSettingsPage.h"

BaseSettingsPage::BaseSettingsPage() : PasswordPage()
{}

bool BaseSettingsPage::LCOnChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext)
{
	if (strOldVal != strNewVal)
	{
		Button* pSaveBtn = ((BaseSettingsPage*)pContext)->GetFnButton(FNBTN_SAVE);
		pSaveBtn->SetVisible(true);
	}
	return true;
}

void BaseSettingsPage::OnInitPageProtected()
{
	RegisterFnButton(FNBTN_SAVE, GetLangString(MSG_SAVE), this)->SetVisible(false);
	RegisterFnButton(FNBTN_EXIT, GetLangString(MSG_EXIT), PAGE_SETTINGS_ADVANCED);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool BaseSettingsPage::OnClick(FNBTN enumBtn, void* pContext)
{
	ShowInfo(GetLangString(MSG_SAVE_DONE));
	GetFnButton(FNBTN_SAVE)->SetVisible(false);

	return true;
}

extern const UNITDATA UNITS_HOST_BITS[] =
{
	{ RANGE_X, MSG_BITS, 1 },
	{ RANGE_UNK }
};
