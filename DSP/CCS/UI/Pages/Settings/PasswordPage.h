/*
 * DisplayPage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef PSWPAGE_H_
#define PSWPAGE_H_

#include "../Menu.h"
#include "../../Controls/EditBox.h"
enum PSWSTATE { PSWSTATE_UNKNOWN, PSWSTATE_INVALID, PSWSTATE_VALID };

/** **********************************************************************
 *
 * Base implementation of all password-protected pages.
 *
 *********************************************************************** */
class PasswordPage : public Menu
{
public:
	PasswordPage();
	virtual ~PasswordPage() {};
	virtual void OnInitPage();
	virtual void OnInitPageProtected() = 0;
	virtual void OnButtonChanged(int8_t chButtonCode, bool bIsPressed);
	void ValidatePassword(const char* pszPass);
	void ClearPassword();
	bool OnBeforePassCharChange(int8_t& chVal);

protected:
	virtual const char* GetPassword() const;
	virtual uint16_t GetForModeTextId()		{ return MSG_FOR_CONFIG; };
	virtual PAGE GetRetPageId()				{ return PAGE_CONFIGURATION; };

private:
	Label* m_pInfoLabel;
	EditBox* m_pEditPassword;
};

#endif /* PSWPAGE_H_ */
