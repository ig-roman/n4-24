/*
 * DisplayPage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef LabParamsBase_H_
#define LabParamsBase_H_

#include "../Menu.h"
#include "../../Controls/EditBoxDigit.h"
#include "../IOnClickCompatible.h"

class LabParamsBase : public IOnClickCompatible
{
public:
	LabParamsBase(bool bIsReadOnly, PAGE eRetPage, Menu* pPage);
	virtual ~LabParamsBase() {};

	virtual void OnInitPageLab();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);

private:
	static bool LPOnChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext);
	static bool LPOnDateChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext);
	static bool LPOnBeforeDateActivate(WithNavigation* pEdit, bool bBecomeActive, void* pContext);

private:
	PAGE m_eRetPage;
	bool m_bIsReadOnly;
	EditBoxDigit* m_pEditTemp;
	EditBoxDigit* m_pEditTempErr;
	EditBoxDigit* m_pEditHum;
	EditBoxDigit* m_pEditHumErr;
	EditBoxDigit* m_pEditDate;

	Menu* m_pPage;
};

#endif /* LabParamsBase_H_ */

