/*
 * DisplayPage.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "GPIBConfigPage.h"

#include "../../Controls/Label.h"
#include "../../Controls/Button.h"
#include "../../../Logic/DeviceState.h"
#include "../../../UIInterface.h"
#include "../../PageManager.h"
#include <stdio.h>

const MINMAX MINMAX_GPIB_ADDR = { 0, 30, DEFAULT_GPIB_ADDR };
Validator s_validatorGPIBAddr(&MINMAX_GPIB_ADDR,	NULL, VAL_FLAG_POS_INT_NUM, 2, 0);

GPIBConfigPage::GPIBConfigPage() : BaseSettingsPage()
{
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void GPIBConfigPage::OnInitPageProtected()
{
	BaseSettingsPage::OnInitPageProtected();

	Label* pCaptionLabel = CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET, MSG_GPIB_SETTINGS_CAPTION);
	pCaptionLabel->SetSize(800, STYLE_PAGE_CAPTION_HEIGHT);
	pCaptionLabel->SetFont(FONT_CAPTION);
	pCaptionLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	uint16_t unYoffset = 150;

	CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_BUS_ADDRESS, STYLE_PAGE_KEY_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);
	m_pEditAddress = EditBox::CreateInstance<EditBoxDigit>(this, 0, STYLE_PAGE_VALUE_LABEL_XOFFSET, unYoffset, STYLE_PAGE_VALUE_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT, AL_NONE, 16);
	RegisterControl(m_pEditAddress);
	m_pEditAddress->SetDValue(g_deviceState.GetConfig().GetGPIBAddress(), 2, 0);
	m_pEditAddress->SetOnAfterValueChange(LCOnChange, this);
	m_pEditAddress->SetValidator(&s_validatorGPIBAddr);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool GPIBConfigPage::OnClick(FNBTN enumBtn, void* pContext)
{
	// Trigger on change call back to validate value
	m_pEditAddress->SetActive(false);
	m_pEditAddress->SetActive(true);

	uint8_t unNewGPIBAddr = (uint8_t)roundX(m_pEditAddress->GetDValue(), 0);

	// Updating address
	g_deviceState.GetConfig().SetGPIBAddress(unNewGPIBAddr);
	CUI_PostInit(INITSTEP_GPIB, (void*)unNewGPIBAddr);

	return BaseSettingsPage::OnClick(enumBtn, pContext);;
}
