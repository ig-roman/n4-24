/*
 * DisplayPage.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "LabParamsBase.h"

#include "../../Controls/Label.h"
#include "../../Controls/Button.h"
#include "../../../Logic/DeviceState.h"
#include "../../../UIInterface.h"
#include "../../PageManager.h"
#include <stdio.h>
#include <ctime>
#include <time.h>

#define DVAL_LEN_PX 180
#define DVAL_LEN_CHARS 6

const UNITDATA UNITS_TEMP[] =
{
	{ RANGE_X, MSG_DEGC, 1 },
	{ RANGE_UNK }
};

const MINMAX MINMAX_TEMP = { -99, 99, DEFAULT_LAB };
Validator s_validatorTemp(&MINMAX_TEMP, UNITS_TEMP, VAL_FLAG_REAL_NUM, 4, 2);

const MINMAX MINMAX_TEMP_ERR = { 0, 99, DEFAULT_LAB };
Validator s_validatorTempErr(&MINMAX_TEMP_ERR, UNITS_TEMP, VAL_FLAG_REAL_NUM, 4, 2);

const UNITDATA UNITS_HUM[] =
{
	{ RANGE_X, MSG_PERCENT, 1 },
	{ RANGE_UNK }
};

const MINMAX MINMAX_HUM = { 0, 99, DEFAULT_LAB };
Validator s_validatorHum(&MINMAX_HUM, UNITS_HUM, VAL_FLAG_REAL_NUM, 4, 2);

LabParamsBase::LabParamsBase(bool bIsReadOnly, PAGE eRetPage, Menu* pPage) :
	m_pPage(pPage),
	m_bIsReadOnly(bIsReadOnly),
	m_eRetPage(eRetPage)
{}

/** **********************************************************************
 *
 * Checks that date has strict format like 'XX.XX.XXXX' where 'X' any number
 * 
 * @param pEdit		unused
 * @param strOldVal	old value in edit box
 * @param strNewVal	new value in edit box
 * @param pContext	unused
 *
 * @return always true
 *
 *********************************************************************** */
bool LabParamsBase::LPOnDateChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext)
{
	return strOldVal.length() == strNewVal.length() && '.' == strNewVal[3] && '.' == strNewVal[6];
}

/** **********************************************************************
 *
 * Shows Save button when something changed
 * 
 * @param pEdit		unused
 * @param strOldVal	old value in edit box
 * @param strNewVal	new value in edit box
 * @param pContext	this page
 *
 * @return always true
 *
 *********************************************************************** */
bool LabParamsBase::LPOnChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext)
{
	LabParamsBase* pThis = ((LabParamsBase*)pContext);
	if (strOldVal != strNewVal && !pThis->m_bIsReadOnly)
	{
		Button* pSaveBtn = pThis->m_pPage->GetFnButton(FNBTN_SAVE);
		pSaveBtn->SetVisible(true);
	}
	return true;
}

/** **********************************************************************
 *
 * Fixes day, moths and years relation if it invalid
 * 
 * @param pEdit			edit box with date in following format: dd.mm.YYYY
 * @param bBecomeActive	unused
 * @param pContext		unused
 *
 * @return always true
 *
 *********************************************************************** */
bool LabParamsBase::LPOnBeforeDateActivate(WithNavigation* pEdit, bool bBecomeActive, void* pContext)
{
	tm time = {0};
	const char* pszDate = pEdit->GetValue();
	pszDate++;
	time.tm_mday = ParseFirstDigit(&pszDate);
	time.tm_mon  = ParseFirstDigit(&pszDate) - 1;
	time.tm_year = ParseFirstDigit(&pszDate) - 1900;

	time_t unTicks = mktime(&time);
	tm* pTimeFixed = -1 == (signed int)unTicks ? gmtime(&unTicks) : localtime(&unTicks);

	char szBuff[20];
	sprintf(szBuff, " %0.2d.%0.2d.%0.4d",pTimeFixed->tm_mday, pTimeFixed->tm_mon + 1, pTimeFixed->tm_year + 1900);
	pEdit->SetValue(szBuff);

	return true;
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void LabParamsBase::OnInitPageLab()
{
	const LAB_CONF& cfgLab = g_deviceState.GetConfig().GetLab();

	Label* pCaptionLabel = m_pPage->CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET, m_bIsReadOnly ? MSG_CAPTION_LAB_INFO : MSG_CAPTION_LAB_INFO_CHANGE );
	pCaptionLabel->SetSize(FNBTN_CONF_VERT_START_X, STYLE_PAGE_CAPTION_HEIGHT);
	pCaptionLabel->SetFont(FONT_CAPTION);
	pCaptionLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	uint16_t unYoffset = 150;
	
	// Temperature
	m_pPage->CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_LAB_TEMP, STYLE_PAGE_KEY_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);
	m_pEditTemp = EditBox::CreateInstance<EditBoxDigit>(m_pPage, 0, STYLE_PAGE_VALUE_LABEL_XOFFSET, unYoffset, DVAL_LEN_PX, DEFAULT_CONTROL_HEIGHT, AL_NONE, DVAL_LEN_CHARS);
	uint8_t unRegId = m_pPage->RegisterControl(m_pEditTemp);
	m_pEditTemp->m_unTextLimit = DVAL_LEN_CHARS;
	m_pEditTemp->SetDValue(cfgLab.nTemp / 1000.0);
	m_pEditTemp->SetOnAfterValueChange(LPOnChange, this);
	m_pEditTemp->SetWithNavigation(!m_bIsReadOnly);
	m_pEditTemp->SetValidator(&s_validatorTemp);

	if (!m_bIsReadOnly)
	{
		m_pPage->SetActiveControl(unRegId);
	}

	m_pEditTempErr = EditBox::CreateInstance<EditBoxDigit>(m_pPage, 0, 520, unYoffset, DVAL_LEN_PX, DEFAULT_CONTROL_HEIGHT, AL_NONE, DVAL_LEN_CHARS);
	m_pPage->RegisterControl(m_pEditTempErr);
	m_pEditTempErr->m_unTextLimit = DVAL_LEN_CHARS;
	m_pEditTempErr->SetWithNavigation(!m_bIsReadOnly);
	m_pEditTempErr->SetDValue(cfgLab.unTempErr / 1000.0);
	m_pEditTempErr->SetOnAfterValueChange(LPOnChange, this);
	m_pEditTempErr->SetValidator(&s_validatorTempErr);
	m_pPage->CreateLabel(510, unYoffset, MSG_PLUS_MINUS, 25, DEFAULT_CONTROL_HEIGHT);

	// Humidity
	IncOffset(unYoffset);
	m_pPage->CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_LAB_HUMID, STYLE_PAGE_KEY_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);
	m_pEditHum = EditBox::CreateInstance<EditBoxDigit>(m_pPage, 0, STYLE_PAGE_VALUE_LABEL_XOFFSET, unYoffset, DVAL_LEN_PX, DEFAULT_CONTROL_HEIGHT, AL_NONE, DVAL_LEN_CHARS);
	m_pPage->RegisterControl(m_pEditHum);
	m_pEditHum->m_unTextLimit = DVAL_LEN_CHARS;
	m_pEditHum->SetDValue(cfgLab.unHum / 1000.0);
	m_pEditHum->SetOnAfterValueChange(LPOnChange, this);
	m_pEditHum->SetWithNavigation(!m_bIsReadOnly);
	m_pEditHum->SetValidator(&s_validatorHum);

	m_pEditHumErr = EditBox::CreateInstance<EditBoxDigit>(m_pPage, 0, 520, unYoffset, DVAL_LEN_PX, DEFAULT_CONTROL_HEIGHT, AL_NONE, DVAL_LEN_CHARS);
	m_pPage->RegisterControl(m_pEditHumErr);
	m_pEditHumErr->m_unTextLimit = DVAL_LEN_CHARS;
	m_pEditHumErr->SetDValue(cfgLab.unHumErr / 1000.0);
	m_pEditHumErr->SetOnAfterValueChange(LPOnChange, this);
	m_pEditHumErr->SetWithNavigation(!m_bIsReadOnly);
	m_pPage->CreateLabel(510, unYoffset, MSG_PLUS_MINUS, 25, DEFAULT_CONTROL_HEIGHT);
	m_pEditHumErr->SetValidator(&s_validatorHum);

	// Date
	IncOffset(unYoffset);
	m_pPage->CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_LAB_DATE, STYLE_PAGE_KEY_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);
	m_pEditDate = EditBox::CreateInstance<EditBoxDigit>(m_pPage, 0, STYLE_PAGE_VALUE_LABEL_XOFFSET, unYoffset, STYLE_PAGE_VALUE_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT, AL_NONE, 11);
	m_pPage->RegisterControl(m_pEditDate);
	m_pEditDate->m_unTextLimit = m_pEditDate->m_unEditLen; // Set virtual size to same as physical
	m_pEditDate->SetWithNavigation(!m_bIsReadOnly);

	char szBuff[20];
	const char *pszD = cfgLab.szDate;
	sprintf(szBuff, " %c%c.%c%c.%s", pszD[0], pszD[1], pszD[2], pszD[3], pszD + 4);
	m_pEditDate->SetValue(szBuff);

	m_pEditDate->SetOnAfterValueChange(LPOnChange, this);
	m_pEditDate->SetOnBeforeValueChange(LPOnDateChange, this);
	m_pEditDate->SetOnBeforeActivate(LPOnBeforeDateActivate, this);

	// Protection from invalid value already stored in EEPROM
	// Should be no use cases to get this state
	LPOnBeforeDateActivate(m_pEditDate, false, this);

	m_pPage->RegisterFnButton(FNBTN_SAVE, GetLangString(MSG_SAVE), this)->SetVisible(false);
	m_pPage->RegisterFnButton(FNBTN_EXIT, GetLangString(MSG_EXIT), m_eRetPage);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool LabParamsBase::OnClick(FNBTN enumBtn, void* pContext)
{
	if (!m_bIsReadOnly)
	{
		// Validate date when user clicks save button without focus change.
		LPOnBeforeDateActivate(m_pEditDate, false, this);

		LAB_CONF labConfNew;
		char* pszDatePersistent = labConfNew.szDate;
		const char* pszDate = m_pEditDate->GetValue();
		pszDate++; // Space

		// Day
		memcpy(pszDatePersistent, pszDate, 2);
		pszDatePersistent += 2;
		pszDate += 3; // Day + dot

		// Moth
		memcpy(pszDatePersistent, pszDate, 2);
		pszDatePersistent += 2;
		pszDate += 3; // Moth + dot

		// Year + new line
		memcpy(pszDatePersistent, pszDate, 5);

		labConfNew.nTemp = (int16_t)roundX(m_pEditTemp->GetDValue() * 1000, 0);
		labConfNew.unTempErr = (uint16_t)roundX(m_pEditTempErr->GetDValue() * 1000, 0);

		labConfNew.unHum = (uint16_t)roundX(m_pEditHum->GetDValue() * 1000, 0);
		labConfNew.unHumErr = (uint16_t)roundX(m_pEditHumErr->GetDValue() * 1000, 0);

		// Updating lab data
		g_deviceState.GetConfig().SetLab(labConfNew);

		Button* pSaveBtn = m_pPage->GetFnButton(FNBTN_SAVE);
		pSaveBtn->SetVisible(false);
		m_pPage->ShowInfo(GetLangString(MSG_SAVE_DONE));
	}
	return true;
}
