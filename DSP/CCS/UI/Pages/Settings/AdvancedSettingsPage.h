/*
 * DisplayPage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef ADISPLAYPAGE_H_
#define ADISPLAYPAGE_H_

#include "AdvancedSettingsPage.h"
#include "PasswordPage.h"
#include "../IOnClickCompatible.h"

class AdvancedSettingsPage : public PasswordPage, public IOnClickCompatible
{
public:
	AdvancedSettingsPage();
	virtual ~AdvancedSettingsPage() {};

	virtual PAGE GetPageId() { return PAGE_SETTINGS_ADVANCED; };
	virtual void OnInitPageProtected();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
};

#endif /* ADISPLAYPAGE_H_ */
