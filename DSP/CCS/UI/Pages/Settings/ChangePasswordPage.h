/*
 * DisplayPage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef ChangePasswordPage_H_
#define ChangePasswordPage_H_

#include "PasswordPage.h"
#include "../../Controls/EditBoxDigit.h"
#include "../IOnClickCompatible.h"

class ChangePasswordPage : public PasswordPage, public IOnClickCompatible
{
public:
	ChangePasswordPage();
	virtual ~ChangePasswordPage() {};

	virtual PAGE GetPageId() { return PAGE_CHANGE_PSW_CONFIG; };
	virtual void OnInitPageProtected();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);

	void SetPasswordType(bool bIsConfPsw)	{ m_bIsConfPsw = bIsConfPsw; }
	void SetRetPage(PAGE eRetPage)			{ m_eRetPage = eRetPage; }

private:
	static bool CPOnChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext);

private:
	PAGE m_eRetPage;
	bool m_bIsConfPsw;
	EditBox* m_pEditPass1;
	EditBox* m_pEditPass2;
};

#endif /* ChangePasswordPage_H_ */

