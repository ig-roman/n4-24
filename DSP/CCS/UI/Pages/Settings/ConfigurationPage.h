/*
 * ConfigurationPage.h
 *
 *  Created on: Oct 30, 2013
 *      Author: Sergei Tero
 */

#ifndef CONFIGURATIONPAGE_H_
#define CONFIGURATIONPAGE_H_

#include "../Menu.h"
#include "../IOnClickCompatible.h"

class ConfigurationPage : public Menu, public IOnClickCompatible
{
public:
	ConfigurationPage();
	virtual ~ConfigurationPage() {};

	virtual PAGE GetPageId() { return PAGE_CONFIGURATION; };
	virtual void OnInitPage();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
	static void CreateSettingsPageContent(Menu* pPage);
};

#endif /* CONFIGURATIONPAGE_H_ */
