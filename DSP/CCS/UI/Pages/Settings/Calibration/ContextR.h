/*
 * ContextR.h
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#ifndef CONTEXTR_H_
#define CONTEXTR_H_

#include "ContextBase.h"

class ContextR: public ContextBase
{
public:
	ContextR();
	virtual ~ContextR() {};
	virtual void InitStingMsgs(PAGE_MSGS& pageMsgs);
	virtual const char* GetModelLabelString()		{ return m_pszModeLabel; };
	virtual MODE GetOpMode() const					{ return MODE_R; };
};

#endif /* CONTEXTR_H_ */
