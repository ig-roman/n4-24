/*
 * LinearCalibrationPage.h
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#ifndef RESISTORSCALIBRATIONPAGE_H_
#define RESISTORSCALIBRATIONPAGE_H_

#include "BaseCalibrationPage.h"

class ResistorsCalibrationPage : public BaseCalibrationPage
{
public:
	ResistorsCalibrationPage();
	virtual ~ResistorsCalibrationPage() {};

	virtual PAGE GetPageId() { return PAGE_RESISTORS_CALIBRATION; };
	virtual void OnInitPageProtected();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
	virtual void SelectStep(uint16_t unCalibrationStep);
	double GetPointDelta();

protected:
	bool SetCalibrationPoint(double dDelta);

private:
	double GetResitorValueAtPoint(uint8_t unCalibrationStep);
};

#endif /* RESISTORSCALIBRATIONPAGE_H_ */
