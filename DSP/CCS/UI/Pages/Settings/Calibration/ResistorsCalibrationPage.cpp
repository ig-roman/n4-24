/*
 * ResistorsCalibrationPage.cpp
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#include <stdio.h>
#include "ResistorsCalibrationPage.h"
#include "../../../PageManager.h"
#include "ResistorsHelpPage.h"

ResistorsCalibrationPage::ResistorsCalibrationPage() : BaseCalibrationPage()
{}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void ResistorsCalibrationPage::OnInitPageProtected()
{
	m_unCountOfPoints = GetCtx()->GetIndexData()->GetPointsCount(0);

	BaseCalibrationPage::OnInitPageProtected();

	m_pLabelSubRangeText->SetVisible(false);
	m_pLabelSubRange->SetVisible(false);

	GetFnButton(FNBTN_SENSE)->SetVisible(false);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool ResistorsCalibrationPage::OnClick(FNBTN enumBtn, void* pContext)
{
	// Updates m_bIsModified flag
	BaseCalibrationPage::OnClick(enumBtn, pContext);

	switch (enumBtn)
	{
		case FNBTN_EXIT:
		{
			if (!m_confirmPopUp.GetIsModified())
			{
				ResistorsHelpPage* pNextPage = (ResistorsHelpPage*)g_pageManager.CreatePage(PAGE_RESISTORS_HELP);
				MoveCalibrationContextTo(pNextPage);
				g_pageManager.ShowPage(pNextPage);
			}
			break;
		}
	}

	return true;
}

double ResistorsCalibrationPage::GetResitorValueAtPoint(uint8_t unCalibrationStep)
{
	WORKUNIT wuOut = {0};
	GetCtx()->GetIndexData()->GetCalibrationPoint(0, unCalibrationStep, 0, &wuOut);
	return wuOut.pBand->min;
}

/** **********************************************************************
 *
 * Performs action to change step of calibration
 * @param unCalibrationStep	new calibration point
 *
 *********************************************************************** */
void ResistorsCalibrationPage::SelectStep(uint16_t unCalibrationStep)
{
	double dResValue = GetResitorValueAtPoint((uint8_t)unCalibrationStep);
	g_deviceState.SetResistance(dResValue);

	m_ePreferedSence = unCalibrationStep < EXTBAND_R_MAX_COUNT ? STATESENSE_TWO_WIRE : STATESENSE_FOUR_WIRE;

	BaseCalibrationPage::SelectStep(unCalibrationStep);
}

/** **********************************************************************
 *
 * Validates and saves correction for calibration point
 * @param dDelta	delta correction for calibration point
 * @return true if value is valid, saved and calibration data array is updated
 *
 *********************************************************************** */
bool ResistorsCalibrationPage::SetCalibrationPoint(double dDelta)
{
	double dResValue = GetResitorValueAtPoint((uint8_t)m_nCalibrationStep);
	return BaseCalibrationPage::SetCalibrationPoint(dDelta - dResValue); // Store only correction
}

/** **********************************************************************
 *
 * @return Delta correction for selected point
 *
 *********************************************************************** */
double ResistorsCalibrationPage::GetPointDelta()
{
	double dResValue = GetResitorValueAtPoint((uint8_t)m_nCalibrationStep);
	double dRet = BaseCalibrationPage::GetPointDelta();

	return dResValue + dRet; // Resistor value + correction to show it in UI
}
