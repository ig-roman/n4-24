/*
 * ContextACI.cpp
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#include "ContextR.h"
#include "../../BasePage.h"

ContextR::ContextR() : ContextBase()
{
	m_pszModeLabel = GetLangString(MSG_R);
}

void ContextR::InitStingMsgs(PAGE_MSGS& pageMsgs)
{
	pageMsgs.unCaptionHelpMessage			= MSG_CAL_R_HELP;
	pageMsgs.unCaptionHelpMessageLin		= NULL;
	pageMsgs.unCaptionHelpMessageMinMax		= NULL;
	pageMsgs.unCaptionHelpMessageAdjust		= MSG_CAL_AJUST_R;
}
