/*
 * ContextDCU.h
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#ifndef CONTEXTDCU_H_
#define CONTEXTDCU_H_

#include "ContextBase.h"

class ContextDCU: public ContextBase
{
public:
	ContextDCU();
	virtual ~ContextDCU() {};
	virtual void InitStingMsgs(PAGE_MSGS& pageMsgs);
	virtual MODE GetOpMode() const			{ return MODE_DCU; };
};

#endif /* CONTEXTDCU_H_ */
