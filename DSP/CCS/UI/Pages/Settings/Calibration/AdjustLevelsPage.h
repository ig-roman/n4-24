/*
 * BandMinMaxLevelsPage.h
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#ifndef BANDMINMAXLEVELSPAGE_H_
#define BANDMINMAXLEVELSPAGE_H_

#include "BaseCalibrationPage.h"
#include "WithContext.h"

class AdjustLevelsPage : public BaseCalibrationPage
{
public:
	AdjustLevelsPage();
	virtual ~AdjustLevelsPage() {};

	virtual PAGE GetPageId() { return PAGE_ADJUST_LEVELS; };
	virtual void OnInitPageProtected();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
	void SelectStep(uint16_t unCalibrationStep);

private:
	bool m_bMaxValue;
	uint8_t m_unBandIdx;
	uint8_t m_unBandsCount;
	uint8_t m_unFrequencyIdx;
};

#endif /* BANDMINMAXLEVELSPAGE_H_ */
