/*
 * ResistorsHelpPage.cpp
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#include "ResistorsHelpPage.h"
#include "../../../PageManager.h"
#include "../../../Controls/Label.h"
#include "ResistorsCalibrationPage.h"
#include "CalibrationModePage.h"

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void ResistorsHelpPage::OnInitPageProtected()
{
	Label* pLabel = CreateLabel(0, 10, GetCtx()->GetMsgs()->unCaptionHelpMessage, FNBTN_CONF_VERT_START_X, 200);
	pLabel->SetFont(FONT_CAPTION);
	pLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	RegisterFnButton(FNBTN_EXIT, GetLangString(MSG_EXIT), PAGE_CALIBRATION_MODE);
	RegisterFnButton(FNBTN_NEXT, GetLangString(MSG_NEXT), this);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool ResistorsHelpPage::OnClick(FNBTN enumBtn, void* pContext)
{
	BasePage* pNextPage = NULL;
	if (FNBTN_NEXT == enumBtn)
	{
		pNextPage = g_pageManager.CreatePage(PAGE_RESISTORS_CALIBRATION);
		MoveCalibrationContextTo((ResistorsCalibrationPage*)pNextPage);
		((ResistorsCalibrationPage*)pNextPage)->GetCtx()->SetBandIdx(0);
	}
	g_pageManager.ShowPage(pNextPage);
	return true;
}
