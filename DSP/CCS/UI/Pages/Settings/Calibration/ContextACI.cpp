/*
 * ContextACI.cpp
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#include "ContextACI.h"
#include "../../BasePage.h"

const int16_t s_arrACIButtonsMap[] = 
{
	// Regular
	FNBTN_1, // 0 100mA
	FNBTN_2, // 1 1A
	FNBTN_3, // 2 2A

	// Current amplifier
	FNBTN_1, // 3 10A
	FNBTN_2, // 4 30A
};

ContextACI::ContextACI() : ContextBase()
{
	m_pszModeLabel = GetLangString(MSG_I);
	m_pnButtionsMap = s_arrACIButtonsMap;
}

void ContextACI::InitStingMsgs(PAGE_MSGS& pageMsgs)
{
	pageMsgs.unCaptionHelpMessage			= MSG_CAL_ACI_HELP;
	pageMsgs.unCaptionHelpMessageLin		= MSG_CAL_ACI_LIN_HELP;
	pageMsgs.unCaptionHelpMessageMinMax		= MSG_CAL_ACI_MIN_MAX_HELP;
	pageMsgs.unCaptionHelpMessageAdjust		= MSG_CAL_AJUST_I;
}
