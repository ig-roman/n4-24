/*
 * ContextDCU.cpp
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#include "ContextDCU.h"
#include "../../BasePage.h"

ContextDCU::ContextDCU() : ContextBase()
{
	m_pszModeLabel = GetLangString(MSG_U);
}

void ContextDCU::InitStingMsgs(PAGE_MSGS& pageMsgs)
{
	pageMsgs.unCaptionHelpMessage			= MSG_CAL_DCU_HELP;
	pageMsgs.unCaptionHelpMessageLin		= MSG_CAL_DCU_LIN_HELP;
	pageMsgs.unCaptionHelpMessageMinMax		= MSG_CAL_DCU_MIN_MAX_HELP;
	pageMsgs.unCaptionHelpMessageAdjust		= MSG_CAL_AJUST_U;
}
