/*
 * ContextACU.h
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#ifndef CONTEXTACU_H_
#define CONTEXTACU_H_

#include "ContextBase.h"

class ContextACU: public ContextBase {
public:
	ContextACU();
	virtual ~ContextACU() {};
	virtual void InitStingMsgs(PAGE_MSGS& pageMsgs);
	virtual MODE GetOpMode() const					{ return MODE_ACU; };
};

#endif /* CONTEXTACU_H_ */
