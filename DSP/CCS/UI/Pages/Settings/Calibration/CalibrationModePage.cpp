/*
 * CalibrationModePage.cpp
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#include <stdio.h>
#include "CalibrationModePage.h"
#include "../../../PageManager.h"
#include "../ChangePasswordPage.h"
#include "LabParamsPage.h"
#include "../../../Controls/Label.h"
#include "ResistorsHelpPage.h"
#include "SelectBandPage.h"

#include "ContextACI.h"
#include "ContextDCI.h"
#include "ContextDCU.h"
#include "ContextACU.h"
#include "ContextR.h"

#define DCU_BTN				FNBTN_1
#define ACU_BTN				FNBTN_2
#define DCI_EX_BTN			FNBTN_3
#define ACI_EX_BTN			FNBTN_4
#define DCI_BTN				FNBTN_5
#define ACI_BTN				FNBTN_6
#define DISABLE_ENABLE_BTN	FNBTN_8
#define LAB_PARAMS_BTN		FNBTN_9
#define CHANGE_PSW_BTN		FNBTN_10
#define R_BTN				FNBTN_11

#ifdef _WINDOWS
#pragma warning(disable:4996)
#endif

CalibrationModePage::CalibrationModePage() : CalibratePasswordPage()
{ }

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool CalibrationModePage::OnClick(FNBTN enumBtn, void* pContext)
{
	if (DISABLE_ENABLE_BTN == enumBtn)
	{
		bool bIsEnabled = !g_deviceState.GetCalibrationEnabled();
		g_deviceState.SetCalibrationEnabled(bIsEnabled);

		SyncCalibrationEnabled();
	}
	else if (LAB_PARAMS_BTN == enumBtn)
	{
		g_pageManager.ChangePage(PAGE_LAB_PARAMS_CONFIG);
	}
	else if (CHANGE_PSW_BTN == enumBtn)
	{
		ChangePasswordPage* pChangePswPage = (ChangePasswordPage*)g_pageManager.CreatePage(PAGE_CHANGE_PSW_CONFIG);
		pChangePswPage->SetRetPage(PAGE_CALIBRATION_MODE);
		pChangePswPage->SetPasswordType(false);
		g_pageManager.ShowPage(pChangePswPage);
	}
	else if (R_BTN == enumBtn)
	{
		ResistorsHelpPage* pResistorsHelpPage = (ResistorsHelpPage*)g_pageManager.CreatePage(PAGE_RESISTORS_HELP);
		pResistorsHelpPage->SetCalibrationContext(new ContextR);
		g_pageManager.ShowPage(pResistorsHelpPage);
	}
	else
	{
		ContextBase* pContextBase = NULL;
		switch (enumBtn)
		{
			case DCU_BTN:		{ pContextBase = new ContextDCU; break; }
			case ACU_BTN:		{ pContextBase = new ContextACU; break; }
			case DCI_BTN:		{ pContextBase = new ContextDCI; break; }
			case ACI_BTN:		{ pContextBase = new ContextACI; break; }
			//case DCI_EX_BTN:	{ pContextBase = new ContextDCI; ((ContextACI*)pContextBase)->SetExMode(true); break; }
			//case ACI_EX_BTN:	{ pContextBase = new ContextACI; ((ContextACI*)pContextBase)->SetExMode(true); break; }
		}

		bool bCalibrationWithShunts = false;//DCI_EX_BTN == enumBtn || ACI_EX_BTN == enumBtn;
		SelectBandPage* pSelectBandPage = (SelectBandPage*)g_pageManager.CreatePage(bCalibrationWithShunts ? PAGE_SHUNTS_HELP_EX : PAGE_SELECT_BAND);
		pSelectBandPage->SetCalibrationContext(pContextBase);
		g_pageManager.ShowPage(pSelectBandPage);
	}

	return true;
}

/** **********************************************************************
 *
 * Updates page content according to CalibrationEnabled flag in device state.
 * When calibration is disabled function hides all buttons to prevent calibration in disabled state
 *
 *********************************************************************** */
void CalibrationModePage::SyncCalibrationEnabled()
{
	bool bIsEnabled = g_deviceState.GetCalibrationEnabled();
	Button* pButton = GetFnButton(DISABLE_ENABLE_BTN);
	pButton->SetValue(GetLangString(bIsEnabled ? MSG_DISABLE : MSG_ENABLE));

	static FNBTN s_arrUiBtns[] =  { DCU_BTN, ACU_BTN, /* DCI_EX_BTN, ACI_EX_BTN, */ DCI_BTN, ACI_BTN, R_BTN };

	for (int nIdx = 0; nIdx < sizeof(s_arrUiBtns) / sizeof(FNBTN); nIdx++)
	{
		FNBTN eBtn = s_arrUiBtns[nIdx];
		Button* pButton = GetFnButton(eBtn);
		pButton->SetVisible(bIsEnabled);
	}
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void CalibrationModePage::OnInitPageProtected()
{
	Label* pCaptionLabel = CreateLabel(0, 10, MSG_CAL_SELECT_MODE, FNBTN_CONF_VERT_START_X, 130);
	pCaptionLabel->SetFont(FONT_CAPTION);
	pCaptionLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	Label* pLabelInfo = CreateLabel(0, 140, MSG_CAL_DISABLE_INFO, FNBTN_CONF_VERT_START_X , 280);
	pLabelInfo->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	CreateCustomFnButton(DCU_BTN, GetLangString(MSG_U), true);
	CreateCustomFnButton(ACU_BTN, GetLangString(MSG_U), false);
	
	//CreateCustomFnButton(DCI_EX_BTN, GetLangString(MSG_I), true, true);
	//CreateCustomFnButton(ACI_EX_BTN, GetLangString(MSG_I), false, true);
	
	CreateCustomFnButton(DCI_BTN, GetLangString(MSG_I), true);
	CreateCustomFnButton(ACI_BTN, GetLangString(MSG_I), false);

	RegisterFnButton(R_BTN, GetLangString(MSG_R), this);

	RegisterFnButton(LAB_PARAMS_BTN, GetLangString(MSG_LAB_PARAMS),	this);
	RegisterFnButton(CHANGE_PSW_BTN, GetLangString(MSG_PSW_CHANGE),	this);
	RegisterFnButton(FNBTN_EXIT, GetLangString(MSG_EXIT), PAGE_SELECT_MODE);
	Button* pBtn = RegisterFnButton(DISABLE_ENABLE_BTN, NULL, this);
	pBtn->SetVisible(true);
	SyncCalibrationEnabled();
}

void CalibrationModePage::CreateCustomFnButton(FNBTN enumFnKeyIdx, const char* pClearaLabel, bool bDC, bool bEx)
{
	char pszBuff[10];
	const char* pszDC =  bDC ? "=" : "~";
	if (bEx)
	{
		sprintf(pszBuff, "%s%s\n%s", pszDC, pClearaLabel, GetLangString(MSG_SHUNT));
	}
	else
	{
		sprintf(pszBuff, "%s%s", pszDC, pClearaLabel);
	}

	Button* pBtn = RegisterFnButton(enumFnKeyIdx, NULL, this);
	pBtn->SetValue(pszBuff, true);
	pBtn->SetVisible();
}
