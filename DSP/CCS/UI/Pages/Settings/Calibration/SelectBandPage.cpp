/*
 * BandMinMaxHelpPage.cpp
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#include "ExCalibration/ShuntsHelpPage.h"
#include "../../../PageManager.h"
#include "../../../Controls/Label.h"
#include "AdjustLevelsPage.h"
#include "CalibrationModePage.h"
#include "ExCalibration/BandMinMaxLevelsExPage.h"

#define FNBTN_BAND_UP					FNBTN_1
#define FNBTN_BAND_DOWN					FNBTN_2
#define CLEAR_BAND_BTN					FNBTN_5

const CONFIRM_DATA SelectBandPage::sm_arrConfirmData[] =
{
	{ CLEAR_BAND_BTN, MSG_CLEAR_CONFIRM },
	{ FNBTN_MAX, MSG_NONE }
};

#ifdef _WINDOWS
#pragma warning(disable:4355)
#endif

SelectBandPage::SelectBandPage() : CalibratePasswordPage(), WithContext(),
	m_pLabelSubRange(NULL),
	m_pLabelCalibrateBand(NULL),
	m_pLabelSense(NULL),
	m_pLabelSenseCaption(NULL),
	m_confirmPopUp(sm_arrConfirmData, this)
{}

#ifdef _WINDOWS
#pragma warning(default:4355)
#endif

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void SelectBandPage::OnInitPageProtected()
{
	if ((uint8_t)-1 == GetCtx()->GetBandIdx())
	{
		uint8_t unBaseBand = GetBaseCurveBandIdx(GetCtx()->GetOpMode());
		GetCtx()->SetBandIdx(unBaseBand);
	}

	uint16_t unYoffset = 10;

	Label* pLabelPleaseSelect = CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_CAL_SELECT_SUBRANGE, FNBTN_CONF_VERT_START_X - STYLE_PAGE_LEFT_OFFSET, 130);
	pLabelPleaseSelect->SetFont(FONT_CAPTION);
	pLabelPleaseSelect->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	IncOffset(unYoffset, 150);
	Label* pLabelSubRangeText = CreateLabel(50, unYoffset, MSG_SUBRANGE, 250, DEFAULT_CONTROL_HEIGHT);
	pLabelSubRangeText->SetAlignment(AL_RIGHT_JUSTIFY_TEXT);
	pLabelSubRangeText->SetFont(FONT_CAPTION);
	m_pLabelSubRange = CreateLabel(310, unYoffset, MSG_NONE, 350, DEFAULT_CONTROL_HEIGHT);
	m_pLabelSubRange->SetFont(FONT_CAPTION);

	// Sense label and button
	IncOffset(unYoffset);
	m_pLabelSenseCaption = CreateLabel(100, unYoffset, MSG_FEEDBACK, 250);
	m_pLabelSenseCaption->SetAlignment(AL_RIGHT_JUSTIFY_TEXT);
	m_pLabelSense = CreateLabel(360, unYoffset, MSG_NONE, 270);
	RegisterFnButton(FNBTN_SENSE, GetLangString(MSG_FEEDBACK), this);
	RegisterFnButton(FNBTN_DIVIDER, GetLangString(MSG_DIVIDER), this);

	IncOffset(unYoffset, 50);
	m_pLabelCalibrateBand = CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_NONE, FNBTN_CONF_VERT_START_X - STYLE_PAGE_LEFT_OFFSET, 130);
	m_pLabelCalibrateBand->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	RegisterFnButton(FNBTN_BAND_UP, GetLangString(MSG_SUBRANGE_UP), this);
	RegisterFnButton(FNBTN_BAND_DOWN, GetLangString(MSG_SUBRANGE_DOWN), this);

	RegisterFnButton(FNBTN_EXIT, GetLangString(MSG_EXIT), this);
	RegisterFnButton(FNBTN_NEXT, GetLangString(MSG_NEXT), this);

	RegisterFnButton(CLEAR_BAND_BTN, GetLangString(MSG_CLEAR), this);
	m_confirmPopUp.SetIsModified();

	UpdateUI();
}

void SelectBandPage::UpdateUI()
{
	const IndexData* pIndexData = GetCtx()->GetIndexData();
	uint8_t nBandIdx = GetCtx()->GetBandIdx();
	BaseCalibrationPage::UpdateBandText(m_pLabelSubRange, pIndexData->GetBands() + nBandIdx, GetCtx()->GetStateMode());

	bool bIsMinMax = 2 == pIndexData->GetPointsCount(nBandIdx);
	m_pLabelCalibrateBand->SetValue(GetLangString(bIsMinMax ? GetCtx()->GetMsgs()->unCaptionHelpMessageMinMax : GetCtx()->GetMsgs()->unCaptionHelpMessageLin));

	if (GetCtx()->IsSensePossible())
	{
		GetFnButton(FNBTN_SENSE)->SetVisible(true);
		GetFnButton(FNBTN_SENSE)->SetToggled(gs_calibrationManager.GetPointsManager().GetBandSense(GetCtx()->GetOpMode(), nBandIdx) == STATESENSE_TWO_WIRE ? false : true);
	}
	else
	{
		GetFnButton(FNBTN_SENSE)->SetToggled(false);
		GetFnButton(FNBTN_SENSE)->SetVisible(false);
	}
	SyncSense();
	if (GetCtx()->IsExtDividerPossible()) {
		GetFnButton(FNBTN_DIVIDER)->SetVisible(true);
		GetFnButton(FNBTN_DIVIDER)->SetToggled(g_deviceState.IsExtDividerBand(nBandIdx));
	}
	else {
		GetFnButton(FNBTN_DIVIDER)->SetVisible(false);
	}
}

/** **********************************************************************
*
* Reorder internal bands indexies to more frendly UI representation.
* Like: (1V -> 10V -> -1V -> -10V) to (-10V -> -1V -> 1V -> 10V)
*
*********************************************************************** */
int8_t SelectBandPage::ToUIBandIndex(int8_t nBandIdx, int8_t nHalfOfMaxBandIdx, bool bIsDC)
{
	int8_t nUIBandIdx = nBandIdx;
	if (bIsDC) {
		int8_t shift = (GetCtx()->GetOpMode() == MODE_DCU) ? 1 : -1;
		nUIBandIdx = nBandIdx >= nHalfOfMaxBandIdx ? nHalfOfMaxBandIdx * 2 - nBandIdx + shift: nBandIdx + nHalfOfMaxBandIdx;
	}
	return nUIBandIdx;
}

int8_t SelectBandPage::FromUIBandIndex(int8_t nUIBandIdx, int8_t nHalfOfMaxBandIdx, bool bIsDC)
{
	int8_t nBandIdx = nUIBandIdx;
	if (bIsDC) {
		int8_t shift = (GetCtx()->GetOpMode() == MODE_DCU) ? 1 : -1;
		nBandIdx = nUIBandIdx < nHalfOfMaxBandIdx ? nHalfOfMaxBandIdx * 2 - nUIBandIdx + shift : nUIBandIdx - nHalfOfMaxBandIdx;
	}
	return nBandIdx;
}

void SelectBandPage::SyncSense()
{
	if ((GetFnButton(FNBTN_DIVIDER)->GetToggled() && GetFnButton(FNBTN_DIVIDER)->GetVisible()) || 
		GetCtx()->GetOpMode() == MODE_ACI || GetCtx()->GetOpMode() == MODE_DCI) {
		m_pLabelSense->SetVisible(false);
		m_pLabelSenseCaption->SetVisible(false);
	}
	else {
		m_pLabelSense->SetVisible(true);
		m_pLabelSenseCaption->SetVisible(true);
	}
	STATESENSE enumSense = GetFnButton(FNBTN_SENSE)->GetToggled() ? STATESENSE_FOUR_WIRE : STATESENSE_TWO_WIRE;
	m_pLabelSense->SetValue(GetLangString(STATESENSE_TWO_WIRE == enumSense ? MSG_INT_FEEDBACK : MSG_EXT_FEEDBACK));
	GetCtx()->SetSenseState(enumSense);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool SelectBandPage::OnClick(FNBTN enumBtn, void* pContext)
{
	BasePage* pNextPage = NULL;
	bool bHandled = m_confirmPopUp.OnClick(enumBtn);
	if (!bHandled)
	{
		bHandled = true;
		switch (enumBtn)
		{
			case FNBTN_DIVIDER:
			{
				int8_t nBandIdx = g_deviceState.ChangeDividerBand(GetCtx()->GetBandIdx());
				if (nBandIdx < 0)
					break;
			    GetFnButton(FNBTN_DIVIDER)->SetToggled(!GetFnButton(FNBTN_DIVIDER)->GetToggled());
				GetCtx()->SetBandIdx(nBandIdx);
				ShowInfo(GetLangString(GetFnButton(FNBTN_DIVIDER)->GetToggled() ? MSG_CONNECT_EXT_DIV : MSG_DISCONNECT_EXT_DIV));
				UpdateUI();
				break;
			}
			case FNBTN_SENSE:
			{
				GetFnButton(FNBTN_SENSE)->SetToggled(!GetFnButton(FNBTN_SENSE)->GetToggled());
				SyncSense();
				break;
			}
			case CLEAR_BAND_BTN: 
			{ 
				ClearToDefault();
				ShowInfo(GetLangString(MSG_SAVE_DONE));
				break; 
			}
			case FNBTN_EXIT:
			{
				pNextPage = g_pageManager.CreatePage(PAGE_CALIBRATION_MODE);
				break;
			}
			case FNBTN_BAND_UP:
			case FNBTN_BAND_DOWN:
			{
				bool bUp = FNBTN_BAND_UP == enumBtn;
				int8_t nHalfOfMaxBandIdx = (GetCtx()->GetOpMode() == MODE_DCU) ? EXBAND_DCU_P_1000_V + 1
					: GetCtx()->GetIndexData()->GetBandCount() / 2;
				int8_t nBandIdx = GetCtx()->GetBandIdx();
				if (GetCtx()->GetOpMode() == MODE_DCU && g_deviceState.IsExtDividerBand(nBandIdx)) {
#ifdef USE_DCU_10MV
					nBandIdx = g_deviceState.ChangeDividerBand(nBandIdx);
				}
#else
					switch (nBandIdx) {
					case EXBAND_DCU_P_100_MV_E:
						nBandIdx = bUp ? EXBAND_DCU_P_100_MV_E : EXBAND_DCU_P_10_MV_E;
						break;
					case EXBAND_DCU_P_10_MV_E:
						nBandIdx = bUp ? EXBAND_DCU_P_100_MV_E : EXBAND_DCU_N_10_MV_E;
						break;
					case EXBAND_DCU_N_10_MV_E:
						nBandIdx = bUp ? EXBAND_DCU_P_10_MV_E : EXBAND_DCU_N_100_MV_E;
						break;
					case EXBAND_DCU_N_100_MV_E:
					default:
						nBandIdx = bUp ? EXBAND_DCU_N_10_MV_E : EXBAND_DCU_N_100_MV_E;
						break;
					}
					GetCtx()->SetBandIdx(nBandIdx);
				}
				else
#endif
				{
					int8_t unUIBandIdx = ToUIBandIndex(nBandIdx, nHalfOfMaxBandIdx, GetCtx()->IsDC());
					int8_t unNewUIBandIdx = unUIBandIdx + (bUp ? 1 : -1);
					if (unNewUIBandIdx >= nHalfOfMaxBandIdx * 2)
					{
						unNewUIBandIdx = nHalfOfMaxBandIdx * 2 - 1;
					}
					else if (unNewUIBandIdx < 0)
					{
						unNewUIBandIdx = 0;
					}
					GetCtx()->SetBandIdx(FromUIBandIndex(unNewUIBandIdx, nHalfOfMaxBandIdx, GetCtx()->IsDC()));
				}
				UpdateUI();
				break;
			}
			case FNBTN_NEXT:
			{
				if (GetCtx()->GetExMode())
				{
					pNextPage = g_pageManager.CreatePage(PAGE_BAND_MIN_MAX_LEVELS_EX);
					MoveCalibrationContextTo((BandMinMaxLevelsExPage*)pNextPage);
				}
				else
				{
					pNextPage = g_pageManager.CreatePage(PAGE_ADJUST_LEVELS);
					MoveCalibrationContextTo((AdjustLevelsPage*)pNextPage);
				}
				break;
			}
			default:
			{
				bHandled = false; 
			}
		}

		if (pNextPage)
		{
			g_pageManager.ShowPage(pNextPage);
		}
	}

	return bHandled;
}

void SelectBandPage::ClearToDefault()
{
	PointsManager& pointsManager = gs_calibrationManager.GetPointsManager();

	pointsManager.LoadBandPoints(GetCtx()->GetOpMode(), GetCtx()->GetBandIdx());
	pointsManager.ClearLoadedPoints();
	pointsManager.StoreBandsPoints();

	pointsManager.LoadBandPoints(MODE_UNKNOWN, (uint8_t)-1);
}
