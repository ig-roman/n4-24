/*
 * CalibrationModePage.h
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#ifndef CALIBRATIONMODEPAGE_H_
#define CALIBRATIONMODEPAGE_H_

#include "CalibratePasswordPage.h"

class CalibrationModePage : public CalibratePasswordPage, IOnClickCompatible
{
public:
	CalibrationModePage();
	virtual ~CalibrationModePage() {};

	virtual PAGE GetPageId() { return PAGE_CALIBRATION_MODE; };
	virtual void OnInitPageProtected();

	virtual bool OnClick(FNBTN enumBtn, void* pContext);

private:
	void CreateCustomFnButton(FNBTN enumFnKeyIdx, const char* pClearaLabel, bool bDC, bool bEx = false);
	void SyncCalibrationEnabled();

};

#endif /* CALIBRATIONMODEPAGE_H_ */
