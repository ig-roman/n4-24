/*
 * DisplayPage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef LabParamsPage_H_
#define LabParamsPage_H_

#include "../LabParamsBase.h"
#include "CalibratePasswordPage.h"

#ifdef _WINDOWS
#pragma warning(disable:4355)
#endif

class LabParamsPage : public CalibratePasswordPage, public LabParamsBase
{
public:
	LabParamsPage() : CalibratePasswordPage(), LabParamsBase(false, PAGE_CALIBRATION_MODE, this) {};
	virtual ~LabParamsPage() {};

	virtual void OnInitPageProtected() { OnInitPageLab(); };

	virtual PAGE GetPageId() { return PAGE_LAB_PARAMS_CONFIG; };
};

#ifdef _WINDOWS
#pragma warning(default:4355)
#endif

#endif /* LabParamsPage_H_ */

