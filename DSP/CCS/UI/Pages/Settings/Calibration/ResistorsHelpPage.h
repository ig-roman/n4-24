/*
 * ResistorsHelpPage.h
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#ifndef RESISTORSHELPPAGE_H_
#define RESISTORSHELPPAGE_H_

#include "CalibratePasswordPage.h"
#include "../../IOnClickCompatible.h"
#include "WithContext.h"

class ResistorsHelpPage : public CalibratePasswordPage, public WithContext, public IOnClickCompatible
{
public:
	ResistorsHelpPage() : CalibratePasswordPage(), WithContext() {} ;
	virtual ~ResistorsHelpPage() {};

	virtual PAGE GetPageId() { return PAGE_RESISTORS_HELP; };
	virtual void OnInitPageProtected();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
};

#endif /* RESISTORSHELPPAGE_H_ */
