/*
 * ContextBase.cpp
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#include "ContextBase.h"
#include <stdio.h>

ContextBase::ContextBase() :
	m_dAdjustedValue(0),
	m_unBandIdx((uint8_t)-1),
	m_bIsEx(false),
	m_pnButtionsMap(NULL),
	m_pIndexData(NULL)
{
	m_szModeLabelFull[0] = NULL;
	m_pageMsgs.unCaptionHelpMessage = 0;
}

PPAGE_MSGS ContextBase::GetMsgs()
{
	if (0 == m_pageMsgs.unCaptionHelpMessage)
	{
		InitStingMsgs(m_pageMsgs);
	}

	return &m_pageMsgs;
}

const char* ContextBase::GetModelLabelString()
{
	if (NULL == m_szModeLabelFull[0])
	{
		sprintf(m_szModeLabelFull, "%s%s", IsDC() ? "=" : "~", m_pszModeLabel);
	}
	return m_szModeLabelFull;
}

bool ContextBase::IsSensePossible()
{
	MODE mode = GetOpMode();
	if (MODE_R == mode)
		return true;
	if (MODE_DCU == mode)
		return g_deviceState.IsValidSenseForDCV(m_unBandIdx);
	if (MODE_ACU == mode)
		return g_deviceState.IsValidSenseForACV(m_unBandIdx);
	return false; 
};

bool ContextBase::IsExtDividerPossible()
{
	if (GetOpMode() == MODE_DCU)
		return g_deviceState.IsValidBandForDivider(m_unBandIdx);
	return false;
}
