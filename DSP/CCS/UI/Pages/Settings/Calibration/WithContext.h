/*
 * WithContext.h
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#ifndef WITHCONTEXT_H_
#define WITHCONTEXT_H_

#include "../../../../CommonTypes.h"
#include "ContextBase.h"

class WithContext
{
public:
	WithContext();
	virtual ~WithContext();
	void MoveCalibrationContextTo(WithContext* pContextPage);
	void SetCalibrationContext(ContextBase* pContextBase) { m_pContextBase = pContextBase; };
	ContextBase* GetCtx() { return m_pContextBase; };

private:
	ContextBase* m_pContextBase;
};

#endif /* WITHCONTEXT_H_ */
