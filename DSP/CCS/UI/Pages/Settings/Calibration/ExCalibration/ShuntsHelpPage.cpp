/*
 * ChooseLinOrMinMaxPage.cpp
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#include "ShuntsHelpPage.h"
#include "../../../../PageManager.h"
#include <string.h>
#include <stdio.h>

ShuntsHelpPage::ShuntsHelpPage() : SelectBandPage(),
	m_eShuntHlp(SHUNTHLP_PAGE1)
{}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool ShuntsHelpPage::OnClick(FNBTN enumBtn, void* pContext)
{
	bool bHandled = false;
	if (GetCtx()->GetExMode() && m_eShuntHlp != SHUNTHLP_INFORMED)
	{
		if (FNBTN_NEXT == enumBtn || FNBTN_PREV == enumBtn)
		{
			ShuntsHelpPage* pPage = (ShuntsHelpPage*)g_pageManager.CreatePage(PAGE_SHUNTS_HELP_EX);
			MoveCalibrationContextTo(pPage);
			pPage->m_eShuntHlp = (SHUNTHLP)(m_eShuntHlp + (FNBTN_NEXT == enumBtn ? 1 : -1));
			g_pageManager.ShowPage(pPage);
			bHandled = true;
		}
	}
	else
	{
		bHandled = SelectBandPage::OnClick(enumBtn, pContext);
	}

	return bHandled;
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void ShuntsHelpPage::OnInitPageProtected()
{
	if (GetCtx()->GetExMode() && m_eShuntHlp != SHUNTHLP_INFORMED)
	{
		Label* pLabelCaption = CreateLabel(STYLE_PAGE_LEFT_OFFSET, 10, MSG_CAL_SHUNT_HELP_CAPTION, MAX_DISPLAY_LENGTH - STYLE_PAGE_LEFT_OFFSET * 2, 100);
		pLabelCaption->SetFont(FONT_CAPTION);
		pLabelCaption->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

		Label* pLabelInfo = CreateLabel(STYLE_PAGE_LEFT_OFFSET, 100, MSG_NONE, MAX_DISPLAY_LENGTH - STYLE_PAGE_LEFT_OFFSET * 2, 320);
		pLabelInfo->SetAlignment(AL_CENTER_TEXT_VERTICALLY);

		EMSG eMsg = m_eShuntHlp == SHUNTHLP_PAGE1 ? MSG_CAL_SHUNT_HELP1 : MSG_CAL_SHUNT_HELP2;
		const char* pszTemplate = GetLangString(eMsg);
		const char* pszMode = GetLangString(GetCtx()->IsDC() ? MSG_CAL_SHUNT_MODE_DC : MSG_CAL_SHUNT_MODE_AC);
		char* pchBuff = new char[strlen(pszTemplate) + strlen(pszMode) + 5];
		sprintf(pchBuff, pszTemplate, pszMode);
		pLabelInfo->SetValue(pchBuff, true);
		delete[] pchBuff;

		// Buttons registration
		RegisterFnButton(FNBTN_EXIT, GetLangString(MSG_EXIT), PAGE_CALIBRATION_MODE);
		RegisterFnButton(FNBTN_NEXT, GetLangString(MSG_NEXT), this);
		if (m_eShuntHlp != SHUNTHLP_PAGE1)
		{
			RegisterFnButton(FNBTN_PREV, GetLangString(MSG_PREVIOUS), this);
		}
	}
	else
	{
		SelectBandPage::OnInitPageProtected();
	}
}
