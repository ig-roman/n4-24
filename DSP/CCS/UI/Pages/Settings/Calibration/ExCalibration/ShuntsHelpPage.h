/*
 * ChooseLinOrMinMaxPage.h
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#ifndef CHOOSELINORMINMAXEXPAGE_H_
#define CHOOSELINORMINMAXEXPAGE_H_

#include "../SelectBandPage.h"

typedef enum
{
	SHUNTHLP_PAGE1,
	SHUNTHLP_PAGE2,
	SHUNTHLP_INFORMED
} SHUNTHLP;

class ShuntsHelpPage : public SelectBandPage
{
public:
	ShuntsHelpPage();
	virtual ~ShuntsHelpPage() {};

	virtual PAGE GetPageId() { return PAGE_SHUNTS_HELP_EX; };
	virtual void OnInitPageProtected();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);

private:
	SHUNTHLP m_eShuntHlp;
};

#endif /* CHOOSELINORMINMAXEXPAGE_H_ */
