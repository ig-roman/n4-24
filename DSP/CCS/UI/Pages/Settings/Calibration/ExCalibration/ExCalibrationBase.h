/*
 * ExCalibrationBase.h
 *
 *  Created on: Jun 25, 2015
 *      Author: Sergei Tero
 */

#ifndef EXCALIBRATIONBASE_H_
#define EXCALIBRATIONBASE_H_

#include "../../../../Common.h"
#include "../../../../Controls/EditBoxDigit.h"
#include "../BaseCalibrationPage.h"

// N.B. Refers to s_arrShuntNames order
typedef enum
{
	SHUTNNAME_MC3050,
	SHUTNNAME_MC3050_2,
	SHUTNNAME_JA944,
	SHUTNNAME_H412MC,
	SHUTNNAME_NONE
} SHUTNNAME;

typedef struct tagSHUTN
{
	double	curent;
	double	impedance;
	int		eName;
} SHUTN, *PSHUTN;

typedef const SHUTN* PCSHUTN;

extern const char* const szMC3050;
extern const char* const szMC3050_2;
extern const char* const szJA944;
extern const char* const szH412MC;
extern const char *const s_arrShuntNames[];

extern const SHUTN s_arrShuntsDc[];
extern const SHUTN s_arrShuntsAc[];

class BaseCalibrationPage;
class ExCalibrationBase
{
public:
	ExCalibrationBase();
	virtual ~ExCalibrationBase() {};

	void OnInitPageProtectedEx(BaseCalibrationPage* pPage);
	bool OnClickEx(FNBTN enumBtn, void* pContext);
	void UpdateRefVoltage();

protected:
	void SelectStepEx();
	void UpdateControlsShuntParams();
	double GetRefVoltageEx();
	
private:
	PCSHUTN FindShunt(double dCurent, bool bIsDc) const;

private:
	Label* m_pLabelMesVoltage;
	EditBoxDigit* m_pEditMesVoltage;

	Label* m_pLabelMesRes;
	EditBoxDigit* m_pEditMesRes;

	PCSHUTN m_pShunt;

	BaseCalibrationPage* m_pPage;
	bool m_bIsPos;
};

#endif /* EXCALIBRATIONBASE_H_ */
