/*
 * ExCalibrationDefs.cpp
 *
 *  Created on: 6 ���� 2019 �.
 *      Author: Admin
 */

// N.B. This file created because TI compiler moves const struct arrays to RAM instead of ROM if they are located in ExCalibrationBase.h or ExCalibrationBase.cpp (is it a bug?)

#include "ExCalibrationBase.h"

const SHUTN s_arrShuntsDc[] =
{
	{ 0.01,		100,	SHUTNNAME_MC3050	},
	{ 0.03,		10,		SHUTNNAME_MC3050	},
	{ 0.05,		10,		SHUTNNAME_MC3050	},
	{ 0.07,		10,		SHUTNNAME_MC3050	},
	{ 0.1,		10,		SHUTNNAME_MC3050	},
	{ 0.1,		10,		SHUTNNAME_MC3050	},
	{ 0.3,		1,		SHUTNNAME_MC3050	},
	{ 0.5,		0.1,	SHUTNNAME_MC3050	},
	{ 0.7,		0.1,	SHUTNNAME_MC3050	},
	{ 0.9,		0.1,	SHUTNNAME_MC3050	},
	{ 1.0,		0.1,	SHUTNNAME_MC3050	},
	{ 1.1,		0.1,	SHUTNNAME_MC3050_2	},
	{ 1.3,		0.1,	SHUTNNAME_MC3050	},
	{ 1.5,		0.1,	SHUTNNAME_MC3050	},
	{ 1.7,		0.1,	SHUTNNAME_MC3050	},
	{ 1.9,		0.1,	SHUTNNAME_MC3050	},
	{ 2.0,		0.1,	SHUTNNAME_MC3050	},
	{ 2.1,		0.01,	SHUTNNAME_JA944		}, // 2A-30A amplifier
	{ 0,		0,		SHUTNNAME_NONE		}
};

const SHUTN s_arrShuntsAc[] =
{
	{ 0.01	,100	,SHUTNNAME_H412MC	},
	{ 0.03	,10		,SHUTNNAME_H412MC	},
	{ 0.05	,10		,SHUTNNAME_H412MC	},
	{ 0.07	,10		,SHUTNNAME_H412MC	},
	{ 0.1	,10		,SHUTNNAME_H412MC	},
	{ 0.11	,10		,SHUTNNAME_H412MC	},
	{ 0.3	,1		,SHUTNNAME_H412MC	},
	{ 0.5	,1		,SHUTNNAME_H412MC	},
	{ 0.7	,1		,SHUTNNAME_H412MC	},
	{ 0.9	,1		,SHUTNNAME_H412MC	},
	{ 1.0	,1		,SHUTNNAME_H412MC	},
	{ 1.1	,1		,SHUTNNAME_H412MC	},
	{ 1.3	,1		,SHUTNNAME_H412MC	},
	{ 1.5	,1		,SHUTNNAME_H412MC	},
	{ 1.7	,1		,SHUTNNAME_H412MC	},
	{ 1.9	,1		,SHUTNNAME_H412MC	},
	{ 2.0	,1		,SHUTNNAME_H412MC	},
	{ 2.1	,0.01	,SHUTNNAME_JA944	}, // 2A-30A amplifier
	{ 0		,0		,SHUTNNAME_NONE		}
};


