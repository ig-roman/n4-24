/*
 * BandMinMaxLevelsPage.h
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#ifndef BANDMINMAXEXLEVELSPAGE_H_
#define BANDMINMAXEXLEVELSPAGE_H_

#include "../AdjustLevelsPage.h"
#include "ExCalibrationBase.h"

class BandMinMaxLevelsExPage : public AdjustLevelsPage, public ExCalibrationBase
{
public:
	BandMinMaxLevelsExPage();
	virtual ~BandMinMaxLevelsExPage() {};

	virtual PAGE GetPageId() { return PAGE_BAND_MIN_MAX_LEVELS_EX; };
	virtual void OnInitPageProtected();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
	virtual double GetRefVoltage() { return GetRefVoltageEx(); };

protected:
	void SelectStep(uint16_t unCalibrationStep);
};

#endif /* BANDMINMAXEXLEVELSPAGE_H_ */
