/*
 * BandMinMaxLevelsPage.cpp
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#include "BandMinMaxLevelsExPage.h"

BandMinMaxLevelsExPage::BandMinMaxLevelsExPage() : AdjustLevelsPage(), ExCalibrationBase()
{}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void BandMinMaxLevelsExPage::OnInitPageProtected()
{
	// Should be before BandMinMaxLevelsPage::OnInitPageProtected() because SelectStepEx cannot work without controls
	OnInitPageProtectedEx(this);

	AdjustLevelsPage::OnInitPageProtected();

	Button* pBtnUref = GetFnButton(FNBTN_UREF);
	pBtnUref->SetVisible();
	pBtnUref->SetValue(GetLangString(MSG_SHUNT));
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool BandMinMaxLevelsExPage::OnClick(FNBTN enumBtn, void* pContext)
{
	AdjustLevelsPage::OnClick(enumBtn, pContext);
	OnClickEx(enumBtn, pContext);
	return true;
}

/** **********************************************************************
 *
 * Invokes when Calibration point has been changed
 * @param unCalibrationStep	new calibration point
 *
 *********************************************************************** */
void BandMinMaxLevelsExPage::SelectStep(uint16_t unCalibrationStep)
{
	AdjustLevelsPage::SelectStep(unCalibrationStep);

	// This function can be executed when calibration point is already defined (after BandMinMaxLevelsPage::SelectStep)
	SelectStepEx();
}
