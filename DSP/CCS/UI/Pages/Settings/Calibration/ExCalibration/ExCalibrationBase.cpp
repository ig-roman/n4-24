/*
 * ExCalibrationBase.cpp
 *
 *  Created on: Jun 25, 2015
 *      Author: Sergei Tero
 */

#include "ExCalibrationBase.h"
#include <stdio.h>

const char* const szMC3050 = "��3050T kl 0.0005";
const char* const szMC3050_2 = "��3050T kl 0.002";
const char* const szJA944 = "MC-01";
const char* const szH412MC = "�4-12��";

const char *const s_arrShuntNames[] =
{
	szMC3050,		// SHUTNNAME_MC3050
	szMC3050_2,		// SHUTNNAME_MC3050_2
	szJA944,		// SHUTNNAME_JA944
	szH412MC,		// SHUTNNAME_H412MC
	NULL			// SHUTNNAME_NONE
};

const MINMAX MINMAX_REF_VOL		= { -3, 3, 1 };
const MINMAX MINMAX_SHUNT_RES	= { 0.001, 120, 0 };

const Validator s_validatorCalVol(&MINMAX_REF_VOL,		UNITS_VOLTAGE,		VAL_FLAG_REAL_NUM);
const Validator s_validatorCalRes(&MINMAX_SHUNT_RES,	UNITS_RESISTANCE,	VAL_FLAG_REAL_POS_NUM);


/** **********************************************************************
 *
 * Invokes when shunt impedance has been changed.
 *
 * @param pEdit		edit box of shunt impedance
 * @param strOldVal	old impedance
 * @param strNewVal	new impedance
 * @param pContext	pointer to instance of ExCalibrationBase
 *
 * @return always true
 *
 *********************************************************************** */
bool OnAfterShuntChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext)
{
	ExCalibrationBase* pThis = (ExCalibrationBase*)pContext;
	pThis->UpdateRefVoltage();
	return true;
}

/** **********************************************************************
 *
 * Construction of this object
 *
 *********************************************************************** */
ExCalibrationBase::ExCalibrationBase() :
	m_pShunt(NULL),
	m_pPage(NULL),
	m_bIsPos(true)
{}

/** **********************************************************************
 *
 * Finds nearest shunt by current and current direction.
 * Shunts names and parameters are configured in s_arrShuntsAc and s_arrShuntsDc arrays
 *
 * @param dCurrent	current of calibration point must be always positive
 * @param bIsDc		direction type
 *
 * @return constant not null pointer to SHUTN structure.
 *
 *********************************************************************** */
PCSHUTN ExCalibrationBase::FindShunt(double dCurrent, bool bIsDc) const 
{
	const SHUTN* arrShunts = bIsDc ? s_arrShuntsDc : s_arrShuntsAc;

	PCSHUTN pShuntRet = NULL;
	PCSHUTN pBandPrev = &arrShunts[0];
	for (int nIdx = 0; NULL == pShuntRet; nIdx++)
	{
		PCSHUTN pBand = &arrShunts[nIdx];

		// Check that value between 2 ranges and select the nearest.
		if (pBandPrev->curent <= dCurrent && pBand->curent >= dCurrent)
		{
			double dDToPrev = dCurrent - pBandPrev->curent;
			double dDToNext = pBand->curent - dCurrent;
			pShuntRet = dDToPrev < dDToNext ? pBandPrev : pBand;
		}
		else if (0 == nIdx && pBand->curent > dCurrent)
		{
			pShuntRet = pBand;
		}
		else if (SHUTNNAME_NONE == arrShunts[nIdx + 1].eName)
		{
			// last element
			pShuntRet = pBand;
		}

		pBandPrev = pBand;
	}

	return pShuntRet;
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 * Adds 2 lines with parameters of shunt and value of reference voltage also adds shunt configuration button.
 * @param pPage	host page. It maybe BandMinMaxLevelsExPage or LinearCalibrationExPage
 *
 *********************************************************************** */
void ExCalibrationBase::OnInitPageProtectedEx(BaseCalibrationPage* pPage)
{
	m_pPage = pPage;

	m_pLabelMesRes = pPage->CreateLabel(0, 163, MSG_PEAK_VALUE, 350);
	m_pLabelMesRes->SetAlignment(AL_RIGHT_JUSTIFY_TEXT | AL_CENTER_TEXT_VERTICALLY);
	m_pEditMesRes = EditBox::CreateInstance<EditBoxDigit>(pPage, ID_CTRL_MES_RES, 360, 163, 270, DEFAULT_CONTROL_HEIGHT, AL_NONE, 9);
	m_pEditMesRes->SetWithNavigation(false);
	m_pEditMesRes->m_unEditLen = 12; // ST TODO: calculate it
	m_pEditMesRes->SetValidator(&s_validatorCalRes);
	m_pEditMesRes->SetOnAfterValueChange(::OnAfterShuntChange, this);
	pPage->RegisterControl(m_pEditMesRes);

	m_pLabelMesVoltage = pPage->CreateLabel(0, 203, MSG_PEAK_VALUE, 350);
	m_pLabelMesVoltage->SetAlignment(AL_RIGHT_JUSTIFY_TEXT | AL_CENTER_TEXT_VERTICALLY);
	m_pEditMesVoltage = EditBox::CreateInstance<EditBoxDigit>(pPage, ID_CTRL_MES_VOL, 360, 203 , 270, DEFAULT_CONTROL_HEIGHT, AL_NONE, 9);
	m_pEditMesVoltage->SetWithNavigation(false);
	m_pEditMesVoltage->SetValidator(&s_validatorCalVol);
	m_pEditMesVoltage->m_unEditLen = 12; // ST TODO: calculate it
	pPage->RegisterControl(m_pEditMesVoltage);
}

/** **********************************************************************
 *
 * Handles onClick event for all registered functional buttons
 * This handler modifies host page when FNBTN_UREF button is pressed and restores it back when exit button is pressed.
 *
 * @param dCurrent	ID of button
 * @param pContext	pointer to instance of ExCalibrationBase
 *
 * @return true if event is processed, false otherwise.
 *
 *********************************************************************** */
bool ExCalibrationBase::OnClickEx(FNBTN enumBtn, void* pContext)
{
	if (FNBTN_UREF == enumBtn)
	{
		// Always visible controls
		m_pLabelMesRes->SetVisible();
		m_pEditMesRes->SetVisible();
		m_pLabelMesVoltage->SetVisible();
		m_pEditMesVoltage->SetVisible();

		// SetWithNavigation before SetActiveControl because it is invalid behavior when controls active and without navigation
		m_pEditMesVoltage->SetWithNavigation(m_pPage->m_bRefMode);
		m_pEditMesRes->SetWithNavigation(m_pPage->m_bRefMode);

		if (m_pPage->m_bRefMode)
		{
			int16_t nRegId = m_pPage->FindCtrlRegIdByCtrlId(m_pEditMesRes->GetId());
			m_pPage->SetActiveControl(nRegId);
		}
	}

	return true;
}

/** **********************************************************************
 *
 * Invokes when Calibration point has been changed
 *
 *********************************************************************** */
void ExCalibrationBase::SelectStepEx()
{
	WORKUNIT wuValue;
	g_deviceState.GetValue(wuValue);
	m_bIsPos = IsPositiveBand(wuValue.pBand);
	PCSHUTN pShunt = FindShunt(m_bIsPos ? wuValue.value : -wuValue.value, m_pPage->GetCtx()->IsDC());

	bool bImpedanceChanged = !m_pShunt || m_pShunt->impedance != pShunt->impedance || m_pShunt->eName != pShunt->eName;
	m_pShunt = pShunt;
	UpdateControlsShuntParams();

	if (bImpedanceChanged)
	{
		m_pEditMesRes->SetDValue(m_pShunt->impedance);
		m_pEditMesRes->GetDigiBoxValidator()->OnBeforeActivateValidate(false);

		m_pEditMesVoltage->SetDValue(m_pPage->GetRefVoltage());
		m_pEditMesVoltage->GetDigiBoxValidator()->OnBeforeActivateValidate(false);
	}
}

/** **********************************************************************
 *
 * Updates resistance of shunt and reference voltage for UI and device output.
 *
 *********************************************************************** */
void ExCalibrationBase::UpdateControlsShuntParams()
{
	// Format shunt resistance
	char szShuntName[30];
	sprintf(szShuntName, "%s %s:", ::GetLangString(MSG_SHUNT), s_arrShuntNames[m_pShunt->eName]);
	m_pLabelMesRes->SetValue(szShuntName, true);

	UpdateRefVoltage();
}

/** **********************************************************************
 *
 * Updates reference voltage for UI and device output.
 *
 * @param dCurrent	ID of button
 * @param pContext	pointer to instance of ExCalibrationBase
 *
 * @return true if event is processed, false otherwise.
 *
 *********************************************************************** */
void ExCalibrationBase::UpdateRefVoltage()
{
	double dMultiplier;
	uint8_t unScale;
	uint16_t unUnitsTextId;
	int8_t nFraction;

	// Format shunt voltage
	char szRefName[30];
	double dRefVoltage = m_pPage->GetRefVoltage();
	s_validatorCalVol.Normalize(dRefVoltage, unScale, nFraction, dMultiplier, unUnitsTextId, NULL);
	sprintf(szRefName, ::GetLangString(MSG_UREF), dRefVoltage * dMultiplier, ::GetLangString(unUnitsTextId));
	m_pLabelMesVoltage->SetValue(szRefName, true);
}

/** **********************************************************************
 *
 * @return reference voltage for shunt resistance and shunt current
 *
 *********************************************************************** */
double ExCalibrationBase::GetRefVoltageEx()
{
	double dCurent = m_pPage->m_pEditValue->GetDValue();
	return m_pEditMesRes->GetDValue() * dCurent * (m_bIsPos ? 1 : -1);
}
