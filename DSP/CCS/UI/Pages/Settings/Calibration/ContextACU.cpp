/*
 * ContextACU.cpp
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#include "ContextACU.h"
#include "../../BasePage.h"

ContextACU::ContextACU() : ContextBase()
{
	m_pszModeLabel = GetLangString(MSG_U);
}

void ContextACU::InitStingMsgs(PAGE_MSGS& pageMsgs)
{
	pageMsgs.unCaptionHelpMessage			= MSG_CAL_ACU_HELP;
	pageMsgs.unCaptionHelpMessageLin		= MSG_CAL_ACU_LIN_HELP;
	pageMsgs.unCaptionHelpMessageMinMax		= MSG_CAL_ACU_MIN_MAX_HELP;
	pageMsgs.unCaptionHelpMessageAdjust		= MSG_CAL_AJUST_U;
}
