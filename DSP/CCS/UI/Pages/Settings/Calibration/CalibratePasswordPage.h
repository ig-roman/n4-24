/*
 * DisplayPage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef PSWSPAGE_H_
#define PSWSPAGE_H_

#include "../PasswordPage.h"
#include "../../../../Logic/DeviceState.h"

/** **********************************************************************
 *
 * Base implementation of all password-protected pages for calibration.
 *
 *********************************************************************** */
class CalibratePasswordPage : public PasswordPage
{
public:
	CalibratePasswordPage() : PasswordPage() {};
	virtual ~CalibratePasswordPage() {};

protected:
	virtual const char* GetPassword() const { return g_deviceState.GetConfig().GetPassword(false); };
	virtual uint16_t GetForModeTextId()		{ return MSG_FOR_CALIBR; };
	virtual PAGE GetRetPageId()				{ return PAGE_SELECT_MODE; };
};

#endif /* PSWSPAGE_H_ */
