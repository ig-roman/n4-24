/*
 * BaseCalibrationPage.h
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#ifndef BASECALIBRATIONPAGE_H_
#define BASECALIBRATIONPAGE_H_

#include "CalibratePasswordPage.h"
#include "WithContext.h"
#include "../../../Controls/Label.h"
#include "../../../Controls/EditBoxDigit.h"
#include "ExCalibration/ExCalibrationBase.h"
#include "../../../../Logic/Calibration/Definitions.h"
#include "../../../../Logic/Calibration/PointsIndexer/PointsIndexer.h"

#define ID_CTRL_VALUE				100
#define ID_CTRL_VALUE_AJUSTED		101
#define ID_CTRL_FREQUENCY_CAL		102
#define ID_CTRL_DELTA_PERCENT_CAL	103

#define ID_CTRL_MES_VOL				104
#define ID_CTRL_MES_RES				105

#define FNBTN_UREF					FNBTN_5

class BaseCalibrationPage : public CalibratePasswordPage, public WithContext, public IOnClickCompatible
{
	friend class ExCalibrationBase;
public:
	BaseCalibrationPage();
	virtual ~BaseCalibrationPage() {};
	virtual void OnInitPageProtected();
	virtual bool OnBeforeValueChange(EditBox* pEdit,string& strNewVal);
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
	virtual void OnButtonChanged(int8_t chButtonCode, bool bIsPressed);
	void SyncDelata();
	static void UpdateBandText(Label* pLabel, PCBAND pBand, STATEMODE eStateMode, const char** ppszUnitsText = NULL, double* pdMul = NULL);
	virtual void SelectStep(uint16_t unCalibrationStep);
	uint16_t GetCalibrationStep() { return m_nCalibrationStep; };
	void SetPointDelta(double newDelta) { m_pEditValueAdj->SetDValue(newDelta, m_nValueScale, m_nValueFraction, m_dValueMul); };
	virtual double GetPointDelta();
	void SaveCalibrationPoint(bool saveAll);

protected:
	virtual void UpdateControls();
	virtual void ChangeStep(bool bUp);
	virtual bool SetCalibrationPoint(double dDelta);
	virtual double GetRefVoltage();
	void ValidateApproxData(APPROXSUMERR eStatus);
	void SetModified();
	void SyncRefOutput();
	void ManageUref();
	void HideAllCtrls(bool bHide);

private:
	void OnPageExit();
	void ManageInvertSign();

protected:
	EditBoxDigit* m_pEditDeltaPercent;
	EditBoxDigit* m_pEditValue;
	EditBoxDigit* m_pEditFreq;
	EditBoxDigit* m_pEditValueAdj;
	Label* m_pUTypeLabel;
	Label* m_pLabelSubRange;
	Label* m_pLabelSubRangeText;
	Label* m_pLabelTotal;
	int16_t m_nCalibrationStep;
	uint16_t m_unStartOffset;
	uint16_t m_unCountOfPoints;
	ConfirmPopUp m_confirmPopUp;
	bool m_bIsAmpEnabled;
	Label* m_pLabelVal;
	bool m_bRefMode;

	WORKUNIT m_wuPrevCurent;
	WORKUNIT m_wuPrevFreq;
	STATESENSE m_ePreferedSence;

private:
	static const CONFIRM_DATA sm_arrConfirmData[];
	Label* m_pLabelSense;
	Button* m_pBtnPolarityPrev;
	int m_nValueScale;
	int m_nValueFraction;
	double m_dValueMul;
};

#endif /* BASECALIBRATIONPAGE_H_ */
