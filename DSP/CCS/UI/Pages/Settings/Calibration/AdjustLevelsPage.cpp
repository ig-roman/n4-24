/*
 * BandMinMaxLevelsPage.cpp
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#include "AdjustLevelsPage.h"
#include "SelectBandPage.h"
#include "../../../PageManager.h"
#include "../../../../Logic/Calibration/CalibrationManager.h"
#include "../../../../Logic/BandsManager.h"
#include <stdio.h>

#define FNBTN_LEVEL_MAX					FNBTN_1 
#define FNBTN_LEVEL_MIN					FNBTN_2

AdjustLevelsPage::AdjustLevelsPage() : BaseCalibrationPage(),
	m_bMaxValue(false),
	m_unBandIdx(0),
	m_unBandsCount(0),
	m_unFrequencyIdx(0)
{}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void AdjustLevelsPage::OnInitPageProtected()
{
	m_unCountOfPoints = gs_pointsIndexer.GetPointsCountForModeAndBand(GetCtx()->GetOpMode(), GetCtx()->GetBandIdx());

	BaseCalibrationPage::OnInitPageProtected();

	if (!GetCtx()->IsDC())
	{
		RegisterFnButton(FNBTN_LEVEL_MAX, GetLangString(MSG_MAX_POINT), this);
		RegisterFnButton(FNBTN_LEVEL_MIN, GetLangString(MSG_MIN_POINT), this);
	}
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool AdjustLevelsPage::OnClick(FNBTN enumBtn, void* pContext)
{
	// Updates m_bIsModified flag
	bool bIsProcessed = BaseCalibrationPage::OnClick(enumBtn, pContext);

	// bIsProcessed is used here to skip logic below for URef mode
	if (!bIsProcessed && 
		(FNBTN_LEVEL_MAX == enumBtn
		|| FNBTN_LEVEL_MIN == enumBtn))
	{
		bool bUp = FNBTN_LEVEL_MAX == enumBtn;

		MODE mode;
		uint8_t unFreqIdx, unPointIdx, unBandIdx;
		uint16_t nPointOffset = m_unStartOffset + m_nCalibrationStep;
		gs_pointsIndexer.FindCaibrationPointByOffset(nPointOffset, mode, unBandIdx, unFreqIdx, unPointIdx);

		uint8_t unOldValIdx = unPointIdx;
		if (bUp)
		{
			unPointIdx++;
		}
		else if (unPointIdx > 0)
		{
			unPointIdx--;
		}

		if (unOldValIdx != unPointIdx && gs_pointsIndexer.FindOffsetByPoint(mode, unBandIdx, unFreqIdx, unPointIdx, nPointOffset))
		{
			SelectStep(nPointOffset - m_unStartOffset);
		}
	}

	if (FNBTN_EXIT == enumBtn)
	{
		if (!m_confirmPopUp.GetIsModified())
		{
			SelectBandPage* pNextPage = (SelectBandPage*)g_pageManager.CreatePage(PAGE_SELECT_BAND);
			MoveCalibrationContextTo(pNextPage);
			g_pageManager.ShowPage(pNextPage);
		}
	}

	return true;
}

/** **********************************************************************
 *
 * Performs action to change step of calibration
 * @param unCalibrationStep	new calibration point
 *
 *********************************************************************** */
void AdjustLevelsPage::SelectStep(uint16_t unCalibrationStep)
{
	uint8_t unBandIdx;
	double dFreq = 0;
	uint8_t unFreqIdx = (uint8_t)-1;

	MODE mode = GetCtx()->GetOpMode();

	uint8_t unPointIdx;
	uint16_t nStartOffset = m_unStartOffset + unCalibrationStep;

	gs_pointsIndexer.FindCaibrationPointByOffset(nStartOffset, mode, unBandIdx, unFreqIdx, unPointIdx);

	
	if (!GetCtx()->IsDC())
	{	
		dFreq = GetCtx()->GetIndexData()->GetFrequency(unBandIdx, unPointIdx, unFreqIdx);
	}

	// Frequency band must be detected here because code below (SetValue) skips value validation and frequency band always default automatically.
	WORKUNIT wuFreq = { NULL, 0 }; // NULL means: use optimal frequency band
	if (!GetCtx()->IsDC())
	{
		wuFreq.value = dFreq;
		STATEERR enumRet = BandsManager::checkValue(&wuFreq, STATEMODE_U == GetCtx()->GetStateMode() ? sm_arrBandsACUFreq : sm_arrBandsACIFreq, false);
		if (STATEERR_NO_ERROR != enumRet)
		{
			CLogic_SetLastError("BandMinMaxLevelsPage::SelectStep\nCan't find provided frequency");
		}
	}

	// PCBAND pBand = GetCtx()->GetBands() + unBandIdx;
	// double dCalibrationPoint = GetBandLimit(mode, pBand, !m_bMaxValue, unFreqIdx, false);

	WORKUNIT wuValue = { 0 };
	GetCtx()->GetIndexData()->GetCalibrationPoint(unBandIdx, unPointIdx, unFreqIdx, &wuValue);

	PCBAND pBand = wuValue.pBand;
	g_deviceState.SetValue(wuValue.value, dFreq, false, pBand, wuFreq.pBand, true);

	// Optimization: Refill approximation data to use RecalculateApproxData function on point change
	// ValidateApproxData clears whole band in case of invalid data. It must be done before adjustment otherwise user can't modify point value
	APPROXSUMERR eApproxRet = gs_calibrationManager.ActivateRequestedMode(GetCtx()->GetOpMode(), pBand);
	ValidateApproxData(eApproxRet);

	// Notify user to connect or disconnect current amplifier
	bool bIsAmpBand = ::IsAmpBand(GetCtx()->GetOpMode(), unBandIdx);
	if (m_bIsAmpEnabled != bIsAmpBand)
	{
		m_bIsAmpEnabled = bIsAmpBand;
		ShowInfo(GetLangString(m_bIsAmpEnabled ? MSG_CONNECT_CURRENT_AMP : MSG_DISCONNECT_CURRENT_AMP));
	}

	BaseCalibrationPage::SelectStep(unCalibrationStep);
}

