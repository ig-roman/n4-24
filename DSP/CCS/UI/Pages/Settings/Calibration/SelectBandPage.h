/*
 * BandMinMaxHelpPage.h
 *
 *  Created on: Feb 26, 2014
 *      Author: Sergei Tero
 */

#ifndef BANDMINMAXHELPPAGE_H_
#define BANDMINMAXHELPPAGE_H_

#include "CalibratePasswordPage.h"
#include "../../IOnClickCompatible.h"
#include "WithContext.h"

class SelectBandPage : public CalibratePasswordPage, public WithContext, public IOnClickCompatible
{
public:
	SelectBandPage();
	virtual ~SelectBandPage() {};

	virtual PAGE GetPageId() { return PAGE_SELECT_BAND; };
	virtual void OnInitPageProtected();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);

private:
	void UpdateUI();
	void ClearToDefault();
	void SyncSense();
	int8_t ToUIBandIndex(int8_t nBandIdx, int8_t nHalfOfMaxBandIdx, bool bIsDC);
	int8_t FromUIBandIndex(int8_t nUIBandIdx, int8_t nHalfOfMaxBandIdx, bool bIsDC);
private:
	Label* m_pLabelSubRange;
	Label* m_pLabelCalibrateBand;
	Label* m_pLabelSense;
	Label *m_pLabelSenseCaption;
	static const CONFIRM_DATA sm_arrConfirmData[];
	ConfirmPopUp m_confirmPopUp;
};

#endif /* BANDMINMAXHELPPAGE_H_ */
