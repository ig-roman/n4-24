/*
 * ContextBase.h
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#ifndef CONTEXTBASE_H_
#define CONTEXTBASE_H_

#include "../../../../CommonTypes.h"
#include "../../../Common.h"

#include "../../../../Logic/BandsManager.h"
#include "../../../../Logic/Calibration/CalibrationManager.h"
#include "../../../../Logic/Calibration/Definitions.h"
#include "../../../../Logic/Calibration/PointsIndexer/PointsIndexer.h"

typedef struct tagPAGE_MSGS
{
	uint16_t unCaptionHelpMessage;
	uint16_t unCaptionHelpMessageLin;
	uint16_t unCaptionHelpMessageMinMax;
	uint16_t unCaptionHelpMessageAdjust;
} PAGE_MSGS, *PPAGE_MSGS;

class ContextBase
{
public:
	virtual ~ContextBase() {};
	ContextBase();

	virtual const char* GetModelLabelString();
	virtual void InitStingMsgs(PAGE_MSGS& pageMsgs)	= 0;
	PPAGE_MSGS GetMsgs();

	virtual MODE GetOpMode() const					= 0;
	bool IsDC() const								{ return MODE_ACI != GetOpMode() && MODE_ACU != GetOpMode(); };
	double GetMultiplier(const WORKUNIT& wu, uint16_t* punMesgId, bool bFrec = false)	{ return ::GetMultiplier(wu.pBand, punMesgId, bFrec ? STATEMODE_FULL_STANDBY : GetStateMode()); };
	double GetAdjustedValue()						{ return m_dAdjustedValue; };
	void SetBandIdx(uint8_t unBandIdx)				{ m_unBandIdx = unBandIdx; };
	uint8_t GetBandIdx()							{ return m_unBandIdx; };
	STATEMODE GetStateMode()						{ return ConvertMode(GetOpMode()); };
	bool IsSensePossible();
	bool IsExtDividerPossible();
	void SetSenseState(STATESENSE sense)			{ m_ePreferedSence = sense; };
	STATESENSE GetSenseState()						{ return m_ePreferedSence; };

	const int16_t* GetLinButtonsMapping()			{ return m_pnButtionsMap; };

	void SetExMode(bool bIsEx)						{ m_bIsEx = bIsEx; };
	bool GetExMode()								{ return m_bIsEx; };

	const IndexData* GetIndexData()					{ if (!m_pIndexData) m_pIndexData = &gs_pointsIndexer.GetIndexData(GetOpMode()); return m_pIndexData; };

public:
	const char* m_pszModeLabel;

protected:
	char m_szModeLabelFull[3];
	const int16_t* m_pnButtionsMap;

private:
	uint8_t m_unBandIdx;
	PAGE_MSGS m_pageMsgs;
	double m_dAdjustedValue;
	bool m_bIsEx;
	const IndexData* m_pIndexData;
	STATESENSE m_ePreferedSence;
};

#endif /* CONTEXTBASE_H_ */
