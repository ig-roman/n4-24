/*
 * ContextACI.h
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#ifndef CONTEXTACI_H_
#define CONTEXTACI_H_

#include "ContextBase.h"

class ContextACI: public ContextBase {
public:
	ContextACI();
	virtual ~ContextACI() {};
	virtual void InitStingMsgs(PAGE_MSGS& pageMsgs);
	virtual MODE GetOpMode() const					{ return MODE_ACI; };
};

#endif /* CONTEXTACI_H_ */
