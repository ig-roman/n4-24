/*
 * WithContext.cpp
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#include "WithContext.h"

WithContext::WithContext() :
	m_pContextBase(NULL)
{
	// TODO Auto-generated constructor stub
}

WithContext::~WithContext()
{
	if (m_pContextBase)
	{
		delete m_pContextBase;
		m_pContextBase = NULL;
	}
}

void WithContext::MoveCalibrationContextTo(WithContext* pContextPage)
{
	pContextPage->m_pContextBase = m_pContextBase;
	m_pContextBase = NULL;
}
