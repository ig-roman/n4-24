/*
 * BaseCalibrationPage.cpp
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#include <stdio.h>
#include <math.h>
#include "BaseCalibrationPage.h"

#define _CRT_SECURE_NO_WARNINGS

const CONFIRM_DATA BaseCalibrationPage::sm_arrConfirmData[] =
{
	{FNBTN_EXIT,	MSG_EXIT_CONFIRM},
	{FNBTN_SAVE,	MSG_SAVE_CONFIRM},
	{FNBTN_MAX,		MSG_NONE}
};

#ifdef _WINDOWS
#pragma warning(disable:4355 4996)
#endif

BaseCalibrationPage::BaseCalibrationPage() : WithContext(), CalibratePasswordPage(),
	m_pEditValue(NULL),
	m_pEditDeltaPercent(NULL),
	m_pEditFreq(NULL),
	m_pEditValueAdj(NULL),
	m_pUTypeLabel(NULL),
	m_pLabelSubRange(NULL),
	m_pLabelSubRangeText(NULL),
	m_pLabelTotal(NULL),
	m_unCountOfPoints(0),
	m_nCalibrationStep(-1),
	m_unStartOffset(0),
	m_bIsAmpEnabled(false),
	m_bRefMode(false),
	m_confirmPopUp(sm_arrConfirmData, this),
	m_pLabelSense(NULL),
	m_ePreferedSence(STATESENSE_FOUR_WIRE), // Default sense mode is external for calibration decision fromRostislav at 16.03.2016
	m_pBtnPolarityPrev(NULL)
{}

#ifdef _WINDOWS
#pragma warning(default:4355)
#endif

bool OnBeforeCalibrationChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext)
{
	BaseCalibrationPage* pThis = (BaseCalibrationPage*)pContext;
	return pThis->OnBeforeValueChange(pEdit, strNewVal);
}

bool OnAfterCalibrationChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext)
{
	BaseCalibrationPage* pThis = (BaseCalibrationPage*)pContext;
	pThis->SyncDelata();
	return true;
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void BaseCalibrationPage::OnInitPageProtected()
{
	gs_calibrationManager.Clear();
	gs_calibrationManager.ActivateRequestedMode(GetCtx()->GetOpMode(), GetCtx()->GetIndexData()->GetBands() + GetCtx()->GetBandIdx());

	uint16_t nStartOffset;
	if (gs_pointsIndexer.FindOffsetByPoint(GetCtx()->GetOpMode(), GetCtx()->GetBandIdx(), 0, 0, nStartOffset))
	{
		m_unStartOffset = nStartOffset;
	}
	else
	{
		CLogic_SetLastError("BaseCalibrationPage::OnInitPageProtected() - Can't find data offset for this mode");
	}

	STATEMODE stateMode = GetCtx()->GetStateMode();
	g_deviceState.SetOperationMode(stateMode);
	g_deviceState.SetDirecionType(GetCtx()->IsDC());

	m_pUTypeLabel = CreateLabel(0, 0, NULL, 95, 100);
	m_pUTypeLabel->SetFont(FONT_BIG_DIGITS);
	m_pUTypeLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY);
	m_pUTypeLabel->SetValue(GetCtx()->GetModelLabelString());

	uint16_t unYoffset = 3;

	m_pLabelSubRangeText = CreateLabel(100, unYoffset, MSG_SUBRANGE, 250, DEFAULT_CONTROL_HEIGHT);
	m_pLabelSubRangeText->SetAlignment(AL_RIGHT_JUSTIFY_TEXT);
	m_pLabelSubRange = CreateLabel(360, unYoffset, MSG_NONE, 300, DEFAULT_CONTROL_HEIGHT);

	IncOffset(unYoffset);
	Label* pLabelTotal = CreateLabel(100, unYoffset, MSG_CAL_POINT, 250, DEFAULT_CONTROL_HEIGHT);
	pLabelTotal->SetAlignment(AL_RIGHT_JUSTIFY_TEXT);
	m_pLabelTotal = CreateLabel(360, unYoffset, MSG_NONE, 300, DEFAULT_CONTROL_HEIGHT);

	IncOffset(unYoffset);
	m_pLabelVal = CreateLabel(100, unYoffset, MSG_NONE, 250, DEFAULT_CONTROL_HEIGHT);
	m_pLabelVal->SetValue(GetLangString(MSG_POINT_VALUE));
	m_pLabelVal->SetAlignment(AL_RIGHT_JUSTIFY_TEXT);

	m_pEditValue = new EditBoxDigit(this, ID_CTRL_VALUE, 360, unYoffset);
	m_pEditValue->SetSize(270, DEFAULT_CONTROL_HEIGHT);
	m_pEditValue->SetWithNavigation(false);
	m_pEditValue->m_unEditLen = 11; // ST TODO: calculate it
	
	RegisterControl(m_pEditValue);

	if (!GetCtx()->IsDC())
	{
		IncOffset(unYoffset);
		Label* pLabelFreqVal = CreateLabel(100, unYoffset, MSG_NONE, 250, DEFAULT_CONTROL_HEIGHT);
		pLabelFreqVal->SetValue(GetLangString(MSG_POINT_FREQ));
		pLabelFreqVal->SetAlignment(AL_RIGHT_JUSTIFY_TEXT);

		m_pEditFreq = new EditBoxDigit(this, ID_CTRL_FREQUENCY_CAL, 360, unYoffset);
		m_pEditFreq->SetSize(270, DEFAULT_CONTROL_HEIGHT);
		m_pEditFreq->SetWithNavigation(false);
		m_pEditFreq->SetUnitsText(GetCtx()->m_pszModeLabel, STYLE_UNITS_SIZE_BIG_CTRLS);
		m_pEditFreq->m_unEditLen = 12;  // ST TODO: create method
		RegisterControl(m_pEditFreq);
	}

	// Sense label
	IncOffset(unYoffset);
	if (g_deviceState.IsExtDividerBand(GetCtx()->GetBandIdx()) && GetCtx()->GetOpMode() == MODE_DCU) {
		CreateLabel(270, unYoffset, MSG_EXT_DIV, 250);
	}
	else {
		Label *pLabelFeedback = CreateLabel(100, unYoffset, MSG_NONE, 250);
		pLabelFeedback->SetAlignment(AL_RIGHT_JUSTIFY_TEXT);
		m_pLabelSense = CreateLabel(360, unYoffset, MSG_NONE, 270);
		if (GetCtx()->IsSensePossible())
		{
			pLabelFeedback->SetValue(GetLangString(MSG_FEEDBACK));
			m_ePreferedSence = GetCtx()->GetSenseState();
			m_pLabelSense->SetValue(GetLangString(m_ePreferedSence == STATESENSE_TWO_WIRE ? MSG_INT_FEEDBACK : MSG_EXT_FEEDBACK));
		}
		else
		{
			pLabelFeedback->SetValue("  ");
			m_pLabelSense->SetValue("");
		}
	}

	Label* pLabelAdjust = CreateLabel(0, 235, GetCtx()->GetMsgs()->unCaptionHelpMessageAdjust, FNBTN_CONF_VERT_START_X, DEFAULT_CONTROL_HEIGHT);
	pLabelAdjust->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	// Delta
	Label* pLabelDeltaPercent = CreateLabel(100, 375, NULL, 250);
	char szBuff[10];
	sprintf(szBuff, "%s%% =", GetLangString(MSG_DELTA_DC));
	pLabelDeltaPercent->SetValue(szBuff, true);
	pLabelDeltaPercent->SetAlignment(AL_RIGHT_JUSTIFY_TEXT | AL_CENTER_TEXT_VERTICALLY);
	m_pEditDeltaPercent = new EditBoxDigit(this, ID_CTRL_DELTA_PERCENT_CAL, 360, 375);
	m_pEditDeltaPercent->SetUnitsText("%", STYLE_UNITS_SIZE_SMALL_CTRLS);
	m_pEditDeltaPercent->SetSize(190, 50);
	m_pEditDeltaPercent->m_unEditLen = 9;
	m_pEditDeltaPercent->SetWithNavigation(false);
	RegisterControl(m_pEditDeltaPercent);

	m_pEditValueAdj = new EditBoxDigit(this, ID_CTRL_VALUE_AJUSTED, 0, 275);
	m_pEditValueAdj->SetAlignment(AL_RIGHT_JUSTIFY_TEXT);
	m_pEditValueAdj->SetFont(FONT_BIG_DIGITS);
	m_pEditValueAdj->SetSize(695, 100);
	m_pEditValueAdj->m_unEditLen = 11; // ST TODO: create method
	RegisterControl(m_pEditValueAdj);

	RegisterFnButton(FNBTN_EXIT, GetLangString(MSG_EXIT), this);
	RegisterFnButton(FNBTN_SAVE, GetLangString(MSG_SAVE), this);
	RegisterFnButton(FNBTN_PREV, GetLangString(MSG_PREVIOUS), this);
	RegisterFnButton(FNBTN_NEXT, GetLangString(MSG_NEXT), this);

	Button* pURefBtn = RegisterFnButton(FNBTN_UREF, GetLangString(MSG_UREF_BTN), this);
	pURefBtn->SetVisible(MODE_ACU == GetCtx()->GetOpMode());

	Button* pSaveBtn = GetFnButton(FNBTN_SAVE);
	pSaveBtn->SetVisible(false);

	SelectStep(0);

	SetActiveControlByCtrlId(ID_CTRL_VALUE_AJUSTED);
}

void BaseCalibrationPage::UpdateBandText(Label* pLabel, PCBAND pBand, STATEMODE eStateMode, const char** ppszUnitsText, double* pdMul)
{
	uint16_t unUnitText = 0;
	double dMul = ::GetMultiplier(pBand, &unUnitText, eStateMode);

	// Update band caption
	double dMin = roundX(pBand->min * dMul, 2);
	double dMax = roundX(pBand->max * dMul, 2);
	const char* pszUnitsText = ::GetLangString(unUnitText);
	char szBuff[40] = { 0 };
	sprintf(szBuff, "%.2f%s ... %.2f%s", dMin, pszUnitsText, dMax, pszUnitsText);
	pLabel->SetValue(szBuff, true);

	if (ppszUnitsText)
	{
		*ppszUnitsText = pszUnitsText;
	}
	if (pdMul)
	{
		*pdMul = dMul;
	}
}

void BaseCalibrationPage::UpdateControls()
{
	WORKUNIT wuValue;
	g_deviceState.GetValue(wuValue);
	PCBAND pBand = wuValue.pBand;
	const char* pszUnitsText;
	double dMul;
	UpdateBandText(m_pLabelSubRange, pBand, GetCtx()->GetStateMode(), &pszUnitsText, &dMul);

	// Update value of voltage/current calibration point
	m_pEditValue->SetDValue(wuValue.value, wuValue.pBand->scale, wuValue.pBand->fraction, dMul);
	m_pEditValue->SetUnitsText(pszUnitsText, STYLE_UNITS_SIZE_SMALL_CTRLS_UNIVERSAL);

	double dDelta = GetPointDelta();
	m_pEditValueAdj->SetUnitsText(pszUnitsText, STYLE_UNITS_SIZE_BIG_CTRLS);

	// PWM is not used for resistors calibration
	int nAdditionalFractionSize = 0;
	if (MODE_R != GetCtx()->GetOpMode())
	{
		// Calculate maximum resolution for selected band
		// Cannot use constant nMaxPWMScale = 8 because on DCI 30Amp only 20-30% of PWM is used.
		double dMaxBandVal = IsPositiveBand(pBand) ? pBand->max : -pBand->min;
		double dBandToPwm = BandToPwmMultiplier(GetCtx()->GetOpMode(), pBand->bandIdx);
		double dMaxPwmValOnBand = dMaxBandVal * dBandToPwm;
		int nMaxPWMScale = (int)log10(dMaxPwmValOnBand) + 7;
		nAdditionalFractionSize = nMaxPWMScale - pBand->scale;
	}
	m_nValueScale = pBand->scale + nAdditionalFractionSize;
	m_nValueFraction = pBand->fraction + nAdditionalFractionSize;
	m_dValueMul = dMul;
	m_pEditValueAdj->SetDValue(dDelta, m_nValueScale, m_nValueFraction, m_dValueMul);
	m_pEditValueAdj->SetOnBeforeValueChange(::OnBeforeCalibrationChange, this);
	m_pEditValueAdj->SetOnAfterValueChange(::OnAfterCalibrationChange, this);

	// Frequency
	if (!GetCtx()->IsDC())
	{
		WORKUNIT wuFreq;
		g_deviceState.GetFrequency(wuFreq);
		uint16_t unMesgIdUnit = 0;
		m_pEditFreq->SetDValue(wuFreq.value, wuFreq.pBand->scale, wuFreq.pBand->fraction, ::GetMultiplier(wuFreq.pBand, &unMesgIdUnit));
		m_pEditFreq->SetUnitsText(GetLangString(unMesgIdUnit), STYLE_UNITS_SIZE_SMALL_CTRLS_UNIVERSAL);
	}

	char szBuff[40] = { 0 };
	sprintf(szBuff, "%d %s %d", m_nCalibrationStep + 1, GetLangString(MSG_OF), m_unCountOfPoints);
	m_pLabelTotal->SetValue(szBuff, true);

	SyncDelata();
}

/** **********************************************************************
 *
 * Performs action to change step of calibration
 * @param unCalibrationStep	new calibration point
 *
 *********************************************************************** */
void BaseCalibrationPage::SelectStep(uint16_t unCalibrationStep)
{
	// Set new point value
	m_nCalibrationStep = unCalibrationStep;

	// Set preferred sense mode.
	// For some conditions it can not be set because hardware has some limitations
	if (GetCtx()->IsSensePossible())
	{
		if (GetCtx()->GetOpMode() == MODE_R)
			m_pLabelSense->SetValue(GetLangString(m_ePreferedSence == STATESENSE_TWO_WIRE ? MSG_INT_FEEDBACK : MSG_EXT_FEEDBACK));
		g_deviceState.SetSenseMode(m_ePreferedSence);
	}
	// Show new value.
	UpdateControls();
}

/** **********************************************************************
 *
 * Validates and saves correction for calibration point
 * @param dDelta	delta correction for calibration point
 * @return true if value is valid, saved and calibration data array is updated
 *
 *********************************************************************** */
bool BaseCalibrationPage::SetCalibrationPoint(double dDelta)
{
	bool bSynchronized = false;
	if (m_nCalibrationStep >= 0)
	{
		// Check that delta value has been modified.
		uint16_t unPointOffset = m_nCalibrationStep + m_unStartOffset;
		double dOldDelta = gs_calibrationManager.GetPointsManager().GetCalibrationPointCorrectionValue(unPointOffset);

		bSynchronized = dOldDelta == dDelta;
		if (!bSynchronized)
		{
			bSynchronized = gs_calibrationManager.SetCalibrationPoint(unPointOffset, dDelta);
			if (bSynchronized)
			{
				SetModified();
			}
		}
	}

	return bSynchronized;
}

void BaseCalibrationPage::ChangeStep(bool bUp)
{
	uint16_t unNewStep = m_nCalibrationStep;

	if (bUp)
	{
		unNewStep++;
		if (unNewStep >= m_unCountOfPoints)
		{
			unNewStep = 0;
		}
	}
	else
	{
		if (0 == unNewStep)
		{
			unNewStep = m_unCountOfPoints - 1;
		}
		else
		{
			unNewStep--;
		}
	}

	SelectStep(unNewStep);
}

bool BaseCalibrationPage::OnBeforeValueChange(EditBox* pEdit, string& strNewVal)
{
	EditBoxDigit* pEditDigit = (EditBoxDigit*)pEdit;
	double dDelta = EditBoxDigit::ParseDValue(strNewVal.c_str()) / pEditDigit->GetMultiplier();
	bool bRet = SetCalibrationPoint(dDelta);
	return bRet;
}

void BaseCalibrationPage::SyncDelata()
{
	// Sync Delta value
	WORKUNIT wuValue;
	g_deviceState.GetValue(wuValue);

	uint16_t unPointOffset = m_nCalibrationStep + m_unStartOffset;
	double dDelta = gs_calibrationManager.GetPointsManager().GetCalibrationPointCorrectionValue(unPointOffset);

	double dDeltaInPrecent = 0 == wuValue.value ? 0 : dDelta / wuValue.value * 100;
	m_pEditDeltaPercent->SetDValue(dDeltaInPrecent, wuValue.pBand->scale - 1, wuValue.pBand->scale - 3, 1);
}


void BaseCalibrationPage::SaveCalibrationPoint(bool saveAll)
{
	gs_calibrationManager.GetPointsManager().StoreBandsPoints(saveAll ? -1 : m_nCalibrationStep);
	if (GetCtx()->GetSenseState() != gs_calibrationManager.GetPointsManager().GetBandSense())	// Store calibration sense
		gs_calibrationManager.GetPointsManager().SetBandSense(GetCtx()->GetSenseState());
	Button* pSaveBtn = GetFnButton(FNBTN_SAVE);
	pSaveBtn->SetVisible(false);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool BaseCalibrationPage::OnClick(FNBTN enumBtn, void* pContext)
{
	bool bIsProcessed = m_confirmPopUp.OnClick(enumBtn);
	if (!bIsProcessed)
	{
		bIsProcessed = true;
		switch (enumBtn)
		{
			case FNBTN_INVERT_SIGN:
			{
				bIsProcessed = m_bRefMode && MODE_ACU == GetCtx()->GetOpMode();
				ManageInvertSign();
				break;
			}
			case FNBTN_UREF:
			{
				if (m_confirmPopUp.GetIsModified())
				{
					ShowInfo(::GetLangString(MSG_SAVE_FIRST));
				}
				else
				{
					m_bRefMode = !m_bRefMode;
					// Update device output and UI
					SyncRefOutput();
				}
				break;
			}
			case FNBTN_PREV:
			{
				ChangeStep(false);
				break;
			}
			case FNBTN_NEXT: 
			{
				ChangeStep(true);
				break;
			}
			case FNBTN_EXIT:
			{
				HideInfo();
				OnPageExit();
				break;
			}
			case FNBTN_SAVE:
			{
				SaveCalibrationPoint(true);
				HideInfo();
				break;
			}
			default:
			{
				bIsProcessed = false;
			}
		}
	}

	return bIsProcessed;
}

/** **********************************************************************
 *
 * Updates +/- button on UI and hardware state only when URef mode is active. 
 * 
 *********************************************************************** */
void  BaseCalibrationPage::ManageInvertSign()
{
	if (m_bRefMode && MODE_ACU == GetCtx()->GetOpMode())
	{
		Button* pButton = GetFnButton(FNBTN_INVERT_SIGN);

		bool bPolarity = pButton->GetToggled();
		pButton->SetToggled(!bPolarity);
		double bNewVoltage = -GetRefVoltage();

		g_deviceState.SetValue(bNewVoltage, m_wuPrevFreq.value, true, NULL, m_wuPrevFreq.pBand, false);
		m_pEditValue->SetDValue(bNewVoltage, m_pEditValue->GetScale(), m_pEditValue->GetFraction(), m_pEditValue->GetMultiplier());
	}
}

/** **********************************************************************
 *
 * Performs actions when calibration page is closing
 * - sets device output to OFF state and goes to FULL_STANDBY mode
 * - informs user that current amplifier should be disconnected
 *
 *********************************************************************** */
void BaseCalibrationPage::OnPageExit()
{
	g_deviceState.SwitchOut(false, false);
	g_deviceState.SetOperationMode(STATEMODE_FULL_STANDBY);

	if (m_bIsAmpEnabled)
	{
		ShowInfo(GetLangString(MSG_DISCONNECT_CURRENT_AMP));
	}
}

/** **********************************************************************
 *
 * Virtual function to handle or lock any events from physical keyboard
 *
 * @param chButtonCode	key code of pressed, repeated or released button.
 * @param bIsPressed	true if button is pressed or repeat event otherwise it was released
 *
 *********************************************************************** */
void BaseCalibrationPage::OnButtonChanged(int8_t chButtonCode, bool bIsPressed)
{
	// Hides info label
	if (bIsPressed)
	{
		if (BTN_OUT_ON == chButtonCode || BTN_OUT_OFF == chButtonCode)
		{
			g_deviceState.SwitchOut(BTN_OUT_ON == chButtonCode, false);
		}
	}

	// Map '-' button to FNBTN_INVERT_SIGN for URef mode only
	if (m_bRefMode && MODE_ACU == GetCtx()->GetOpMode())
	{
		if ('-' == chButtonCode)
		{
			Button* pButton = GetFnButton(FNBTN_INVERT_SIGN);
			pButton->SetPressed(bIsPressed);
			if (!bIsPressed)
			{
				ManageInvertSign();
			}
		}
	}

	// Don't leave calibration page by pressing menu buttons.
	bool bIsPageLeaveButtons = BTN_MODE_SELECT == chButtonCode || BTN_MODE_U == chButtonCode || BTN_MODE_I == chButtonCode || BTN_MODE_R == chButtonCode;
	if (bIsPageLeaveButtons && m_confirmPopUp.GetIsModified())
	{
		if (bIsPressed)
		{
			ShowInfo(::GetLangString(MSG_SAVE_FIRST));
		}
	}
	else
	{
		if (bIsPageLeaveButtons)
		{
			OnPageExit();
		}
		CalibratePasswordPage::OnButtonChanged(chButtonCode, bIsPressed);
	}
}

/** **********************************************************************
 *
 * Checks that calibration data is valid and fixes it when data is corrupted.
 * Reason of data corruptions: New firmware, eeprom update, uninitialized eeprom
 *
 * @param eStatus	return code from approximator
 * @param eMode		active operation mode to clear it
 * @param pBand		active band to clear it
 *
 *********************************************************************** */
void BaseCalibrationPage::ValidateApproxData(APPROXSUMERR eStatus)
{
	if (APPROXSUMERR_OK != eStatus)
	{
		CLogic_SetLastError("Calibration data is corrupted for selected band and has been be cleared to default state.");
		gs_calibrationManager.GetPointsManager().ClearLoadedPoints();
		SetModified();
	}
}

/** **********************************************************************
 *
 * Modifies page state to "MODIFIED" and shows Save button
 *
 *********************************************************************** */
void BaseCalibrationPage::SetModified()
{
	m_confirmPopUp.SetIsModified();
	Button* pSaveBtn = GetFnButton(FNBTN_SAVE);
	pSaveBtn->SetVisible(true);
}

/** **********************************************************************
 *
 * @return Delta correction for selected point
 *
 *********************************************************************** */
double BaseCalibrationPage::GetPointDelta()
{
	return gs_calibrationManager.GetPointsManager().GetCalibrationPointCorrectionValue(m_nCalibrationStep + m_unStartOffset);
}

/** **********************************************************************
 *
 * Synchronizes UI with device output.
 *
 *********************************************************************** */
void BaseCalibrationPage::SyncRefOutput()
{
	STATEMODE eCalStMode = GetCtx()->GetStateMode();
	if (m_bRefMode)
	{
		// Entering to voltage reference mode. Remember existing state.
		g_deviceState.GetValue(m_wuPrevCurent);
		if (GetCtx()->IsDC())
		{
			m_wuPrevFreq.pBand = NULL;
			m_wuPrevFreq.value = 0;
		}
		else
		{
			g_deviceState.GetFrequency(m_wuPrevFreq);
		}

		// Reference voltage is always voltage
		g_deviceState.SetOperationMode(STATEMODE_U);

		// For ACU mode reference voltage should be DC
		// For calibration with shunt reference voltage can be DC or AC
		bool bIsShunt = STATEMODE_I == eCalStMode;
		bool isDc = bIsShunt ? GetCtx()->IsDC() : true;

		g_deviceState.SetDirecionType(isDc);
	}
	else
	{
		// Use initial parameters from context
		g_deviceState.SetOperationMode(eCalStMode);
		g_deviceState.SetDirecionType(GetCtx()->IsDC());
	}

	if (m_bRefMode)
	{
		g_deviceState.SetValue(GetRefVoltage(), m_wuPrevFreq.value, true, NULL, m_wuPrevFreq.pBand, false);
	}
	else
	{
		g_deviceState.SetValue(m_wuPrevCurent, true, &m_wuPrevFreq, true, true);
	}

	ManageUref();
}

/** **********************************************************************
 *
 * @return reference voltage for mode. For ACI reference voltage same as calibration point value.
 *
 *********************************************************************** */
double BaseCalibrationPage::GetRefVoltage()
{
	// Available for ACU only
	return m_pEditValue->GetDValue();
}

/** **********************************************************************
 *
 * It hides or shows required UI controls when URef/Shunt button is pressed
 *
 *********************************************************************** */
void BaseCalibrationPage::ManageUref()
{
	bool bURefMode = MODE_ACU == GetCtx()->GetOpMode();
	if (bURefMode && m_bRefMode)
	{
		// Save original state of button
		// Menu always has FN buttons
		Button* pBtnPrev = GetFnButton(FNBTN_INVERT_SIGN);
		m_pBtnPolarityPrev = new Button(*pBtnPrev);
	}

	HideAllCtrls(m_bRefMode);

	if (m_bRefMode)
	{
		char szLabel[3];
		bool bDC = true;
		g_deviceState.GetDirecionType(bDC);
		sprintf(szLabel, "%s%s", bDC ? "=" : "~", GetLangString(MSG_U));
		m_pUTypeLabel->SetValue(szLabel, true);

		if (bURefMode)
		{
			// Modify invert sign button
			RegisterFnButton(FNBTN_INVERT_SIGN, GetLangString(MSG_INVERT_SIGN), this);
		}
	}
	else
	{
		if (bURefMode)
		{
			// Restore old button
			Button* pBtnPrev = GetFnButton(FNBTN_INVERT_SIGN);
			*pBtnPrev = *m_pBtnPolarityPrev;
			pBtnPrev->SetInvalid();
			delete m_pBtnPolarityPrev;
			m_pBtnPolarityPrev = NULL;
		}

		m_pUTypeLabel->SetValue(GetCtx()->GetModelLabelString());
		SetActiveControlByCtrlId(ID_CTRL_VALUE_AJUSTED);

		double dVal = GetRefVoltage();
		if (dVal < 0)
		{
			// Reference can be left with negative sign
			// Make sure that calibration point value is positive (ACU always has positive sign).
			m_pEditValue->SetDValue(-dVal, m_pEditValue->GetScale(), m_pEditValue->GetFraction(), m_pEditValue->GetMultiplier());
		}
	}

	// Save is visible when page is modified
	GetFnButton(FNBTN_SAVE)->SetVisible(!m_bRefMode && m_confirmPopUp.GetIsModified());

	// Controls which are  always visible 
	Button* pBtn = GetFnButton(FNBTN_UREF);
	pBtn->SetVisible();
	pBtn->SetToggled(m_bRefMode);

	m_pUTypeLabel->SetVisible();

	// Point value visible only for ACU mode for shuts behavior should be same
	if (bURefMode)
	{
		m_pLabelVal->SetVisible();
		m_pEditValue->SetVisible();
	}
}

/** **********************************************************************
 *
 * Hides or shows all UI components except error or info frames
 * @param bHide	if value true all controls should be hidden otherwise are shown
 *
 *********************************************************************** */
void BaseCalibrationPage::HideAllCtrls(bool bHide)
{
	uint16_t unCtrlRegId = 0;
	AlphaNumeric* pCtrl = (AlphaNumeric*)GetControlByRegId(unCtrlRegId);
	while (pCtrl)
	{
		bool bWithValue = pCtrl->GetValue() && *pCtrl->GetValue();
		if (bHide || bWithValue && !bHide && CTRL_FRAME != pCtrl->GetControlType())
		{
			pCtrl->SetVisible(!bHide);
		}

		unCtrlRegId++;
		pCtrl = (AlphaNumeric*)GetControlByRegId(unCtrlRegId);
	}
}
