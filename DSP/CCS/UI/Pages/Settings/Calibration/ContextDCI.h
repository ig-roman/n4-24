/*
 * ContextDCI.h
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#ifndef CONTEXTDCI_H_
#define CONTEXTDCI_H_

#include "ContextBase.h"

class ContextDCI: public ContextBase
{
public:
	ContextDCI();
	virtual ~ContextDCI() {};
	virtual void InitStingMsgs(PAGE_MSGS& pageMsgs);
	virtual MODE GetOpMode() const				{ return MODE_DCI; };
};

#endif /* CONTEXTDCI_H_ */
