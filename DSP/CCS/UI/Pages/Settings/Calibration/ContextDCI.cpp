/*
 * ContextDCI.cpp
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#include "ContextDCI.h"
#include "../../BasePage.h"

const int16_t m_arrDCIButtonsMap[] = 
{
	// Positive
	FNBTN_1, // 0 100mA
	FNBTN_3, // 1 1A
	FNBTN_5, // 2 2A

	// Positive amp
	FNBTN_1, // 3 10A
	FNBTN_3, // 4 30A

	// Nagative
	FNBTN_2, // 5 -100mA
	FNBTN_4, // 6 -1A
	FNBTN_6, // 7 -2A

	// Nagative amp
	FNBTN_2, // 8 -10A
	FNBTN_4, // 9 -30A
};

ContextDCI::ContextDCI() : ContextBase()
{
	m_pszModeLabel = GetLangString(MSG_I);
	m_pnButtionsMap = m_arrDCIButtonsMap;
}

void ContextDCI::InitStingMsgs(PAGE_MSGS& pageMsgs)
{
	pageMsgs.unCaptionHelpMessage			= MSG_CAL_DCI_HELP;
	pageMsgs.unCaptionHelpMessageLin		= MSG_CAL_DCI_LIN_HELP;
	pageMsgs.unCaptionHelpMessageMinMax		= MSG_CAL_DCI_MIN_MAX_HELP;
	pageMsgs.unCaptionHelpMessageAdjust		= MSG_CAL_AJUST_I;
}
