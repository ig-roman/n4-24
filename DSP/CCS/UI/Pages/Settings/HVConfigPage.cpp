/*
 * DisplayPage.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "HVConfigPage.h"

#include "../../Controls/Label.h"
#include "../../Controls/Button.h"
#include "../../../Logic/DeviceState.h"
#include "../../../UIInterface.h"
#include "../../PageManager.h"
#include <stdio.h>

extern "C" char g_HVBuzzerSuppress;

#define FNBTN_DEFAULT FNBTN_1
#define FNBTN_SUPPRESS FNBTN_2
const MINMAX MINMAX_SAF_VOL = { 30, 110, DEFAULT_HV_SAFETY_VALUE_UBOUND };
Validator s_validatorSafVol(&MINMAX_SAF_VOL, UNITS_VOLTAGE, VAL_FLAG_REAL_POS_NUM, 3, 3);

HVConfigPage::HVConfigPage() : BaseSettingsPage()
{
}

HVConfigPage::~HVConfigPage()
{
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void HVConfigPage::OnInitPageProtected()
{
	BaseSettingsPage::OnInitPageProtected();

	Label* pCaptionLabel = CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET, MSG_HV_SETTINGS);
	pCaptionLabel->SetSize(FNBTN_CONF_VERT_START_X, STYLE_PAGE_CAPTION_HEIGHT);
	pCaptionLabel->SetFont(FONT_CAPTION);
	pCaptionLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	uint16_t unYoffset = 150;

	CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_SAFETY_VOLTAGE, STYLE_PAGE_KEY_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);
	m_pEditVoltage = EditBox::CreateInstance<EditBoxDigit>(this, 0, STYLE_PAGE_VALUE_LABEL_XOFFSET, unYoffset, STYLE_PAGE_VALUE_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT, AL_NONE, 16);
	RegisterControl(m_pEditVoltage);
	m_pEditVoltage->SetDValue(g_deviceState.GetConfig().GetSafetyVoltage() + MAX_HV_THRESHOLD_THRESHOLD_VAL, 3, 3);
	m_pEditVoltage->SetOnAfterValueChange(LCOnChange, this);
	m_pEditVoltage->SetValidator(&s_validatorSafVol);

	char pszLabel[25];
	const char* pszFormat = GetLangString(MSG_HV_DEFAULT);
	sprintf(pszLabel, pszFormat, DEFAULT_HV_SAFETY_VALUE_UBOUND);
	Button* pBtnDefault = RegisterFnButton(FNBTN_DEFAULT, NULL, this);
	pBtnDefault->SetValue(pszLabel, true);
	pBtnDefault->SetVisible(true);

	Button* pBtnSuppress = RegisterFnButton(FNBTN_SUPPRESS, GetLangString(MSG_HV_SUPPRESS), this);
	pBtnSuppress->SetVisible(true);
	g_HVBuzzerSuppress = g_deviceState.GetConfig().GetHVBuzzerSuppress();
	pBtnSuppress->SetToggled(g_HVBuzzerSuppress != 0);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool HVConfigPage::OnClick(FNBTN enumBtn, void* pContext)
{
	switch (enumBtn) {
		case FNBTN_DEFAULT:
		{
			m_pEditVoltage->SetDValue(DEFAULT_HV_SAFETY_VALUE_UBOUND, 3, 3);
			GetFnButton(FNBTN_SAVE)->SetVisible(true);
		} break;
		case FNBTN_SUPPRESS:
		{
			g_HVBuzzerSuppress = !GetFnButton(FNBTN_SUPPRESS)->GetToggled();
			GetFnButton(FNBTN_SUPPRESS)->SetToggled(g_HVBuzzerSuppress != 0);
			g_deviceState.GetConfig().SetHVBuzzerSuppress(g_HVBuzzerSuppress);
		} break;
		default:
		{
			// Trigger on change call back to validate value
			m_pEditVoltage->SetActive(false);
			m_pEditVoltage->SetActive(true);

			double unNewVoltage = m_pEditVoltage->GetDValue() - MAX_HV_THRESHOLD_THRESHOLD_VAL;

			// Updating voltage
			g_deviceState.GetConfig().SetSafetyVoltage(unNewVoltage);
			return BaseSettingsPage::OnClick(enumBtn, pContext);
		}
	}

	return true;
}
