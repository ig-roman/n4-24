/*
 * DisplayPage.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "PasswordPage.h"

#include "../../Controls/Label.h"
#include "../../Controls/Button.h"
#include <string.h>
#include "../../PageManager.h"
#include "../../../Logic/DeviceState.h"

#define ID_CTRL_EDT_PASSWORD		100
#define ID_CTRL_INFO_TEXT			101
#define INVALID_PASSWORD_DELAY_TIME	250

// For all protected pages
PSWSTATE g_enumPasswordState = PSWSTATE_UNKNOWN;

PasswordPage::PasswordPage() : Menu(),
	m_pInfoLabel(NULL),
	m_pEditPassword(NULL)
{
	if (PSWSTATE_UNKNOWN == g_enumPasswordState)
	{
		g_enumPasswordState = PSWSTATE_INVALID;
	}
}

bool OnBeforePassCharChange(EditBox* pEdit, int8_t& chVal, bool nInsert, bool nDelete, void* pContext)
{
	PasswordPage* pThis = ((PasswordPage*)pContext);
	return pThis->OnBeforePassCharChange(chVal);
}

bool PasswordPage::OnBeforePassCharChange(int8_t& chVal)
{
	bool bRet = false;
	if (BTN_ENTER == chVal)
	{
		ValidatePassword(m_pEditPassword->GetValue());
	}
	else if (IsCtrlKeys(chVal))
	{
		bRet = true;
	}
	else if (IsDigitCustom(chVal, true))
	{
		Button* pBtnNext = GetFnButton(FNBTN_NEXT);
		pBtnNext->SetVisible(true);
		bRet = true;
	}

	return bRet;
}

void OnPswClickNext(FNBTN enumBtn, Button* pBtn, void* pContext)
{
	PasswordPage* pThis = ((PasswordPage*)pContext);
	int8_t chVal = BTN_ENTER;
	pThis->OnBeforePassCharChange(chVal);
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void PasswordPage::OnInitPage()
{
	Menu::OnInitPage();

	if (PSWSTATE_VALID != g_enumPasswordState)
	{
		Label* pCaptionLabel = CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET, MSG_CAPTION_ENTER_PASSWORD);
		pCaptionLabel->SetSize(MAX_DISPLAY_LENGTH, STYLE_PAGE_CAPTION_HEIGHT);
		pCaptionLabel->SetFont(FONT_CAPTION);
		pCaptionLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

		Label* pForConfigLabel = CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET + STYLE_PAGE_CAPTION_HEIGHT, GetForModeTextId());
		pForConfigLabel->SetSize(MAX_DISPLAY_LENGTH, 150);
		pForConfigLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

		Label* pEnterPswLabel = CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET + STYLE_PAGE_CAPTION_HEIGHT + 150, MSG_ENTER_PASSWORD);
		pEnterPswLabel->SetSize(MAX_DISPLAY_LENGTH / 2, 100);
		pEnterPswLabel->SetAlignment(AL_RIGHT_JUSTIFY_TEXT);

		m_pEditPassword = EditBox::CreateInstance<EditBox>(this, ID_CTRL_EDT_PASSWORD, 10 + MAX_DISPLAY_LENGTH / 2, STYLE_PAGE_CAPTION_OFFSET + STYLE_PAGE_CAPTION_HEIGHT + 150, DEFAULT_CONTROL_LENGTH, 30, AL_NONE, 10);
		m_pEditPassword->SetOnBeforeCharChange(::OnBeforePassCharChange, this);
		m_pEditPassword->SetWithNavigation(true);
		m_pEditPassword->SetColor(COLOR_BG_DEFAULT);
		m_pEditPassword->SetBackgroundColor(COLOR_FG_DEFAULT);
		m_pEditPassword->SetCursorMode(CURSOR_MODE_INSERT);
		m_pEditPassword->SetPasswordField(true);
		SetActiveControl(RegisterControl(m_pEditPassword));

		m_pInfoLabel = new Label(this, ID_CTRL_INFO_TEXT, 0, STYLE_PAGE_CAPTION_OFFSET + STYLE_PAGE_CAPTION_HEIGHT + 150 + DEFAULT_CONTROL_HEIGHT * 2);
		m_pInfoLabel->SetSize(MAX_DISPLAY_LENGTH, DEFAULT_CONTROL_HEIGHT);
		m_pInfoLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY);
		m_pInfoLabel->SetColor(COLOR_ERROR);
		m_pInfoLabel->SetValue(GetLangString(MSG_INVALID_PASSWORD));
		m_pInfoLabel->SetVisible(false);
		RegisterControl(m_pInfoLabel);

		RegisterFnButton(FNBTN_EXIT, GetLangString(MSG_EXIT),	GetRetPageId()); // ST TODO: change to conditional page
		Button* pBtnNext = RegisterFnButton(FNBTN_NEXT, GetLangString(MSG_NEXT), PAGE_EMPTY, OnPswClickNext, this);
		pBtnNext->SetVisible(false);
	}
	else
	{
		OnInitPageProtected();
	}
}

/** **********************************************************************
 *
 * Virtual function to handle or lock any events from physical keyboard
 *
 * @param chButtonCode	key code of pressed, repeated or released button.
 * @param bIsPressed	true if button is pressed or repeat event otherwise it was released
 *
 *********************************************************************** */
void PasswordPage::OnButtonChanged(int8_t chButtonCode, bool bIsPressed)
{
	// Hides info label
	if (bIsPressed)
	{
		if (m_pInfoLabel && m_pInfoLabel->GetVisible())
		{
			m_pInfoLabel->SetVisible(false);
		}
	}
	Menu::OnButtonChanged(chButtonCode, bIsPressed);
}

void PasswordPage::ValidatePassword(const char* pszPass)
{
	if (pszPass && strcmp(GetPassword(), pszPass) == 0)
	{
		g_enumPasswordState = PSWSTATE_VALID;
		g_pageManager.ChangePage(GetPageId());
	}
	else
	{
		uint32_t unDelayTime = 0;
		CheckUpdateTime(INVALID_PASSWORD_DELAY_TIME, &unDelayTime);
		while (!CheckUpdateTime(INVALID_PASSWORD_DELAY_TIME, &unDelayTime));

		m_pInfoLabel->SetVisible(true);
		m_pEditPassword->SetValue("");
		Button* pBtnNext = GetFnButton(FNBTN_NEXT);
		pBtnNext->SetVisible(false);
	}
}

void PasswordPage::ClearPassword()
{
	g_enumPasswordState = PSWSTATE_INVALID;
}

const char* PasswordPage::GetPassword() const
{ 
	return g_deviceState.GetConfig().GetPassword(true);
}
