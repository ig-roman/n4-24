/*
 * DisplayPage.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "DisplayPage.h"

#include "../../Controls/Label.h"
#include "../../Controls/Button.h"
#include "../../../Logic/DeviceState.h"
#include "../../PageManager.h"

#define FNBTN_PLUS FNBTN_11
#define FNBTN_MINUS FNBTN_10

DisplayPage::DisplayPage() : Menu(),
	m_unBacklightIntensity(0)
{}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void DisplayPage::OnInitPage()
{
	Menu::OnInitPage();

	Label* pCaptionLabel = CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET, MSG_MODE_CONFIGURATION);
	pCaptionLabel->SetSize(MAX_DISPLAY_LENGTH, STYLE_PAGE_CAPTION_HEIGHT);
	pCaptionLabel->SetFont(FONT_CAPTION);
	pCaptionLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	Label* pDescrLabel = CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET + STYLE_PAGE_CAPTION_HEIGHT, MSG_DISP_CONF_DESCTIPTION);
	pDescrLabel->SetSize(MAX_DISPLAY_LENGTH, 200);
	pDescrLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	RegisterFnButton(FNBTN_SAVE, GetLangString(MSG_SAVE),		this)->SetVisible(false);
	RegisterFnButton(FNBTN_EXIT, GetLangString(MSG_EXIT),		this);
	RegisterFnButton(FNBTN_MINUS, GetLangString(MSG_MINUS),		this);
	RegisterFnButton(FNBTN_PLUS, GetLangString(MSG_PLUS),		this);

	m_unBacklightIntensity = g_deviceState.GetConfig().GetBacklightIntensity();
}

void DisplayPage::ChangeBacklightIntensity(bool bIncrease)
{
	if (bIncrease && m_unBacklightIntensity < MAX_BACKLIGHT_INTENSITY)
	{
 		m_unBacklightIntensity++;
	}
	else if (!bIncrease && m_unBacklightIntensity > MIN_BACKLIGHT_INTENSITY)
	{
		m_unBacklightIntensity--;
	}

	GetDC()->SetBacklightIntensity(m_unBacklightIntensity);
	GetFnButton(FNBTN_SAVE)->SetVisible(true);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool DisplayPage::OnClick(FNBTN enumBtn, void* pContext)
{
	if (FNBTN_SAVE == enumBtn)
	{
		GetFnButton(FNBTN_SAVE)->SetVisible(false);
		g_deviceState.GetConfig().SetBacklightIntensity(m_unBacklightIntensity);
		ShowInfo(GetLangString(MSG_SAVE_DONE));
	}
	else if (FNBTN_EXIT == enumBtn)
	{
		if (GetFnButton(FNBTN_SAVE)->GetVisible())
		{
			// Unsaved changes.
			// Revert to old intensity
			GetDC()->SetBacklightIntensity(g_deviceState.GetConfig().GetBacklightIntensity());
		}
		g_pageManager.ChangePage(PAGE_CONFIGURATION);
	}
	else
	{
		ChangeBacklightIntensity(FNBTN_PLUS == enumBtn);
	}

	return true;
}
