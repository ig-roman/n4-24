/*
 * DisplayPage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef GPIBCONFIGPAGE_H_
#define GPIBCONFIGPAGE_H_

#include "BaseSettingsPage.h"
#include "../../Controls/EditBoxDigit.h"

class GPIBConfigPage : public BaseSettingsPage
{
public:
	GPIBConfigPage();
	virtual ~GPIBConfigPage() {};

	virtual PAGE GetPageId() { return PAGE_GPIB_CONFIG; };
	virtual void OnInitPageProtected();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
private:

private:
	EditBoxDigit* m_pEditAddress;
};

#endif /* GPIBCONFIGPAGE_H_ */
