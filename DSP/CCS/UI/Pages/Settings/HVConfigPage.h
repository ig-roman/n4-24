/*
 * DisplayPage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef HVConfigPage_H_
#define HVConfigPage_H_

#include "BaseSettingsPage.h"
#include "../../Controls/EditBoxDigit.h"

class HVConfigPage : public BaseSettingsPage
{
public:
	HVConfigPage();
	virtual ~HVConfigPage();

	virtual PAGE GetPageId() { return PAGE_HV_CONFIG; };
	virtual void OnInitPageProtected();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
private:

private:
	EditBoxDigit* m_pEditVoltage;
};

#endif /* HVConfigPage_H_ */
