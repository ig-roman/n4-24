/*
 * ConfigurationPage.cpp
 *
 *  Created on: Oct 30, 2013
 *      Author: Sergei Tero
 */

#include "ConfigurationPage.h"

#include "../../Controls/Label.h"
#include "../../Controls/Button.h"
#include "../../PageManager.h"
#include "PasswordPage.h"
#include "LabParamsInfoPage.h"
#include "../../../Logic/DeviceState.h"
#include <stdio.h>

#define OFY 32
#define FNBTN_MORE FNBTN_11
#define FNBTN_LAB_INFO FNBTN_9

ConfigurationPage::ConfigurationPage() : Menu()
{}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void ConfigurationPage::OnInitPage()
{
	Menu::OnInitPage();

	CreateSettingsPageContent(this);
	
	RegisterFnButton(FNBTN_EXIT, GetLangString(MSG_EXIT),	PAGE_SELECT_MODE);
	RegisterFnButton(FNBTN_8, GetLangString(MSG_VIEW),		PAGE_SETTINGS_DISPLAY);
	RegisterFnButton(FNBTN_LAB_INFO, GetLangString(MSG_BTN_LAB),		this);
	RegisterFnButton(FNBTN_MORE, GetLangString(MSG_MORE),				this);
}

void ConfigurationPage::CreateSettingsPageContent(Menu* pPage)
{
	const DeviceConfig& conf = g_deviceState.GetConfig();

	Label* pCaptionLabel = pPage->CreateLabel(0, 0, MSG_MODE_CONFIGURATION);
	pCaptionLabel->SetSize(FNBTN_CONF_VERT_START_X , STYLE_PAGE_CAPTION_HEIGHT);
	pCaptionLabel->SetFont(FONT_CAPTION);
	pCaptionLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	char szBuff[100];
	uint16_t unYoffset = 90;

	//sprintf(szBuff, "%s %s", ::GetLangString(MSG_SERIAL_NO), STR_SERIAL_NR);
	//pPage->CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_NONE, 325)->SetValue(szBuff, true);

	sprintf(szBuff, "%s %s", ::GetLangString(MSG_REVISION), STR_SW_VERSION);
	pPage->CreateLabel(FNBTN_CONF_VERT_START_X / 2, unYoffset,	MSG_NONE, 325)->SetValue(szBuff, true);

	pPage->CreateLabel(STYLE_PAGE_LEFT_OFFSET,		IncOffset(unYoffset, OFY),	MSG_PRESENT_SETINGS, STYLE_PAGE_KEY_LABEL_LENGTH);

	// Language
	STATELANG enumLang = conf.GetLang();
	pPage->CreateLabel(STYLE_PAGE_LEFT_OFFSET * 2,	IncOffset(unYoffset, OFY),	MSG_LANGUAGE, STYLE_PAGE_KEY_LABEL_LENGTH);
		pPage->CreateLabel(STYLE_PAGE_VALUE_LABEL_XOFFSET,	unYoffset, STATELANG_EN == enumLang ? MSG_LANG_ENG : MSG_LANG_RUS, STYLE_PAGE_VALUE_LABEL_LENGTH);

	// Power up mode
	const CTRLMODE& eCtrlMode = conf.GetCtrlModePub();
	EMSG eMsgMode = MSG_ERROR;
	switch (eCtrlMode)
	{
		case CTRLMODE_LOCAL:	eMsgMode = MSG_MODE_MANUAL; break;
		case CTRLMODE_MIXED:	eMsgMode = MSG_MODE_JOINT; break;
		case CTRLMODE_REMOTE:	eMsgMode = MSG_MODE_INTERF; break;
	}

	pPage->CreateLabel(STYLE_PAGE_LEFT_OFFSET * 2,	IncOffset(unYoffset, OFY),	MSG_POWERUP_MODE, STYLE_PAGE_KEY_LABEL_LENGTH);
		pPage->CreateLabel(STYLE_PAGE_VALUE_LABEL_XOFFSET,	unYoffset, eMsgMode, STYLE_PAGE_VALUE_LABEL_LENGTH);

	// GPIB Address
	pPage->CreateLabel(STYLE_PAGE_LEFT_OFFSET * 2,	IncOffset(unYoffset, OFY),	MSG_BUS_ADDRESS, STYLE_PAGE_KEY_LABEL_LENGTH);
	sprintf(szBuff, "%0.2d", conf.GetGPIBAddress());
	pPage->CreateLabel(STYLE_PAGE_VALUE_LABEL_XOFFSET,	unYoffset, MSG_NONE, STYLE_PAGE_VALUE_LABEL_LENGTH)->SetValue(szBuff, true);

	// Safety voltage
	pPage->CreateLabel(STYLE_PAGE_LEFT_OFFSET * 2,	IncOffset(unYoffset, OFY),	MSG_SAFETY_VOLTAGE, STYLE_PAGE_KEY_LABEL_LENGTH);
	double dSafVol = conf.GetSafetyVoltage() + MAX_HV_THRESHOLD_THRESHOLD_VAL;
	sprintf(szBuff, "%0.3f%s", roundX(dSafVol, 3), ::GetLangString(MSG_V));
	pPage->CreateLabel(STYLE_PAGE_VALUE_LABEL_XOFFSET,	unYoffset, MSG_NONE, STYLE_PAGE_VALUE_LABEL_LENGTH)->SetValue(szBuff, true);

	// RS232 Settings
	pPage->CreateLabel(STYLE_PAGE_LEFT_OFFSET,		IncOffset(unYoffset, OFY),	MSG_RS232_CONF, STYLE_PAGE_KEY_LABEL_LENGTH * 2);
	Label* pLabelRs232 = pPage->CreateLabel(STYLE_PAGE_LEFT_OFFSET * 2, IncOffset(unYoffset, OFY), MSG_NONE, STYLE_PAGE_KEY_LABEL_LENGTH * 2);
	const RS232_CONF& rs232Conf = conf.GetRs232();
	sprintf(szBuff, "%d / %s / %d", rs232Conf.unSpeed, ::GetLangString(GetRS232ParityName(rs232Conf.eParity)), rs232Conf.unStopBits);
	pLabelRs232->SetValue(szBuff, true);
	
	// IP Settings
	pPage->CreateLabel(STYLE_PAGE_LEFT_OFFSET,		IncOffset(unYoffset, OFY),	MSG_ETHERNET_CONF, STYLE_PAGE_KEY_LABEL_LENGTH * 2);
	Label* pLabelLan = pPage->CreateLabel(STYLE_PAGE_LEFT_OFFSET * 2, IncOffset(unYoffset, OFY), MSG_NONE, STYLE_PAGE_KEY_LABEL_LENGTH * 2);
	const LAN_CONF& lanConf = conf.GetLan();
	sprintf(szBuff, "%d.%d.%d.%d / %d / %d", lanConf.unIP1, lanConf.unIP2, lanConf.unIP3, lanConf.unIP4, lanConf.unMask, lanConf.unPort);
	pLabelLan->SetValue(szBuff, true);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool ConfigurationPage::OnClick(FNBTN enumBtn, void* pContext)
{
	if (FNBTN_MORE == enumBtn)
	{
		PasswordPage* pPageProtected = (PasswordPage*)g_pageManager.CreatePage(PAGE_SETTINGS_ADVANCED);
		pPageProtected->ClearPassword();
		g_pageManager.ShowPage(pPageProtected);
	}
	else if (FNBTN_LAB_INFO == enumBtn)
	{
		g_pageManager.ChangePage(PAGE_LAB_PARAMS_INFO_CONFIG);
	}

	return true;
}
