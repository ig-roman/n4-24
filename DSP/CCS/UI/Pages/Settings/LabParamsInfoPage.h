/*
 * DisplayPage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef LabParamsInfoPage_H_
#define LabParamsInfoPage_H_

#include "LabParamsBase.h"
#include "../Menu.h"

#ifdef _WINDOWS
#pragma warning(disable:4355)
#endif

class LabParamsInfoPage : public LabParamsBase, public Menu
{
public:
	LabParamsInfoPage() : LabParamsBase(true, PAGE_CONFIGURATION, this) {}
	virtual ~LabParamsInfoPage() {};
	virtual void OnInitPage() { Menu::OnInitPage(); OnInitPageLab(); };

	virtual PAGE GetPageId() { return PAGE_LAB_PARAMS_INFO_CONFIG; };
};

#ifdef _WINDOWS
#pragma warning(default:4355)
#endif

#endif /* LabParamsInfoPage_H_ */

