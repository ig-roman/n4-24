/*
 * DisplayPage.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "SelectLanguagePage.h"

#include "../../Controls/Label.h"
#include "../../Controls/Button.h"
#include "../../../Logic/DeviceState.h"
#include "../../PageManager.h"

#define FNBTN_EN FNBTN_2
#define FNBTN_RU FNBTN_1

SelectLanguagePage::SelectLanguagePage() : PasswordPage()
{
}

void OnBtnLangClicked(FNBTN enumBtn, Button* pButton, void* pContext)
{
	SelectLanguagePage* pThis = (SelectLanguagePage*)pContext;
	pThis->OnBtnLangClicked(enumBtn);
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void SelectLanguagePage::OnInitPageProtected()
{
	Label* pCaptionLabel = CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET, MSG_MODE_CONFIGURATION);
	pCaptionLabel->SetSize(FNBTN_CONF_VERT_START_X, STYLE_PAGE_CAPTION_HEIGHT);
	pCaptionLabel->SetFont(FONT_CAPTION);
	pCaptionLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	Label* pDescrLabel = CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET + STYLE_PAGE_CAPTION_HEIGHT, MSG_SEL_LANG_IS_HIGHLIGHTED);
	pDescrLabel->SetSize(FNBTN_CONF_VERT_START_X, 200);
	pDescrLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	RegisterFnButton(FNBTN_EN, GetLangString(MSG_LANG_ENG),	PAGE_EMPTY, ::OnBtnLangClicked, this);
	RegisterFnButton(FNBTN_RU, GetLangString(MSG_LANG_RUS),	PAGE_EMPTY, ::OnBtnLangClicked, this);

	RegisterFnButton(FNBTN_EXIT, GetLangString(MSG_EXIT),	PAGE_SETTINGS_ADVANCED);

	SyncButtons();
}

void SelectLanguagePage::OnBtnLangClicked(FNBTN enumBtn)
{
	g_deviceState.GetConfig().SetLang(FNBTN_EN == enumBtn ? STATELANG_EN : STATELANG_RU);
	ReloadLangStrings();
	g_pageManager.ChangePage(GetPageId());
}

void SelectLanguagePage::SyncButtons()
{
	STATELANG enumLang = g_deviceState.GetConfig().GetLang();

	GetFnButton(FNBTN_EN)->SetToggled(STATELANG_EN == enumLang);
	GetFnButton(FNBTN_RU)->SetToggled(STATELANG_RU == enumLang);
}
