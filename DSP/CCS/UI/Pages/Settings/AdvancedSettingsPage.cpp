/*
 * DisplayPage.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "AdvancedSettingsPage.h"
#include "ConfigurationPage.h"
#include "../../PageManager.h"
#include "ChangePasswordPage.h"

#include "../../Controls/Label.h"
#include "../../Controls/Button.h"

AdvancedSettingsPage::AdvancedSettingsPage() : PasswordPage()
{}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void AdvancedSettingsPage::OnInitPageProtected()
{
	ConfigurationPage::CreateSettingsPageContent(this);
	RegisterFnButton(FNBTN_1, GetLangString(MSG_SELECT_LANGUAGE),	PAGE_SETTINGS_LANGUAGE);
	RegisterFnButton(FNBTN_2, GetLangString(MSG_SAFETY_VOLTAGE),	PAGE_HV_CONFIG);
	RegisterFnButton(FNBTN_3, GetLangString(MSG_PSW_CHANGE),		this);
#ifdef WITH_FREQ_CORR
	RegisterFnButton(FNBTN_4, GetLangString(MSG_FREQ_CORR),			PAGE_FREQ_CORR);
#endif

	RegisterFnButton(FNBTN_9, GetLangString(MSG_GPIB),				PAGE_GPIB_CONFIG);
	RegisterFnButton(FNBTN_10, GetLangString(MSG_RS232),			PAGE_RS232_CONFIG);
	RegisterFnButton(FNBTN_11, GetLangString(MSG_ETHERNET),			PAGE_LAN_CONFIG);
	
	RegisterFnButton(FNBTN_EXIT, GetLangString(MSG_EXIT),			PAGE_CONFIGURATION);
}


bool AdvancedSettingsPage::OnClick(FNBTN enumBtn, void* pContext)
{
	ChangePasswordPage* pChangePswPage = (ChangePasswordPage*)g_pageManager.CreatePage(PAGE_CHANGE_PSW_CONFIG);
	pChangePswPage->SetRetPage(PAGE_SETTINGS_ADVANCED);
	pChangePswPage->SetPasswordType(true);
	g_pageManager.ShowPage(pChangePswPage);

	return true;
}
