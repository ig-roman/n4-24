/*
 * DisplayPage.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "ChangePasswordPage.h"

#include "../../Controls/Label.h"
#include "../../Controls/Button.h"
#include "../../../Logic/DeviceState.h"
#include "../../../UIInterface.h"
#include "../../PageManager.h"
#include <stdio.h>

ChangePasswordPage::ChangePasswordPage() : PasswordPage(),
	m_eRetPage(PAGE_EMPTY),
	m_bIsConfPsw(true)
{
}

bool ChangePasswordPage::CPOnChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext)
{
	if (strOldVal != strNewVal)
	{
		ChangePasswordPage* pThis = ((ChangePasswordPage*)pContext);

		const char* pszPsw1 = pThis->m_pEditPass1->GetValue();
		const char* pszPsw2 = pThis->m_pEditPass2->GetValue();
		bool bCanSave = pszPsw1 && pszPsw2 && *pszPsw1 && pszPsw2 && 0 == strcmp(pszPsw1, pszPsw2);

		Button* pSaveBtn = pThis->GetFnButton(FNBTN_SAVE);
		pSaveBtn->SetVisible(bCanSave);
	}
	return true;
}


/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void ChangePasswordPage::OnInitPageProtected()
{
	Label* pCaptionLabel = CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET, m_bIsConfPsw ? MSG_PSW_CAPTION_CONFIG : MSG_PSW_CAPTION_CALIB );
	pCaptionLabel->SetSize(FNBTN_CONF_VERT_START_X, STYLE_PAGE_CAPTION_HEIGHT);
	pCaptionLabel->SetFont(FONT_CAPTION);
	pCaptionLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	uint16_t unYoffset = 150;

	CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_PSW_NEW1, STYLE_PAGE_KEY_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);
	m_pEditPass1 = EditBox::CreateInstance<EditBox>(this, 0, STYLE_PAGE_VALUE_LABEL_XOFFSET, unYoffset, DEFAULT_CONTROL_LENGTH, 30, AL_NONE, 16);
	SetActiveControl(RegisterControl(m_pEditPass1));
	m_pEditPass1->SetCursorMode(CURSOR_MODE_INSERT);
	m_pEditPass1->m_unTextLimit = PAS_MAX_LEN;
	m_pEditPass1->SetOnAfterValueChange(CPOnChange, this);
	m_pEditPass1->SetPasswordField(true);
	m_pEditPass1->SetColor(COLOR_BG_DEFAULT);
	m_pEditPass1->SetBackgroundColor(COLOR_FG_DEFAULT);

	IncOffset(unYoffset);

	CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_PSW_NEW2, STYLE_PAGE_KEY_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);
	m_pEditPass2 = EditBox::CreateInstance<EditBox>(this, 0, STYLE_PAGE_VALUE_LABEL_XOFFSET, unYoffset, DEFAULT_CONTROL_LENGTH, 30, AL_NONE, 16);
	RegisterControl(m_pEditPass2);
	m_pEditPass2->m_unTextLimit = PAS_MAX_LEN;
	m_pEditPass2->SetCursorMode(CURSOR_MODE_INSERT);
	m_pEditPass2->SetOnAfterValueChange(CPOnChange, this);
	m_pEditPass2->SetPasswordField(true);
	m_pEditPass2->SetColor(COLOR_BG_DEFAULT);
	m_pEditPass2->SetBackgroundColor(COLOR_FG_DEFAULT);

	RegisterFnButton(FNBTN_SAVE, GetLangString(MSG_SAVE), this)->SetVisible(false);
	RegisterFnButton(FNBTN_EXIT, GetLangString(MSG_EXIT), m_eRetPage);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool ChangePasswordPage::OnClick(FNBTN enumBtn, void* pContext)
{
	// Updating password
	g_deviceState.GetConfig().SetPassword(m_pEditPass1->GetValue(), m_bIsConfPsw);
	ShowInfo(GetLangString(MSG_SAVE_DONE));

	Button* pSaveBtn = GetFnButton(FNBTN_SAVE);
	pSaveBtn->SetVisible(false);

	return true;
}
