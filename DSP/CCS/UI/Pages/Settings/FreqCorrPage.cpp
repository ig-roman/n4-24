/*
* FreqCorrPage.cpp
*
*  Created on: Apr 28, 2021
*      Author: Roman Khlopkov
*/

#include "FreqCorrPage.h"

#include "../../Controls/Label.h"
#include "../../Controls/Button.h"
#include "../../../Logic/DeviceState.h"
#include "../../../UIInterface.h"
#include "../../PageManager.h"
#include <stdio.h>

#define BASE_FREQ_VAL 1.0e+5

const MINMAX MINMAX_FREQ_VAL = { BASE_FREQ_VAL * 0.98, BASE_FREQ_VAL * 1.02, BASE_FREQ_VAL };
Validator s_validatorFreqVal(&MINMAX_FREQ_VAL, NULL, VAL_FLAG_REAL_POS_NUM, 6, 4);

FreqCorrPage::FreqCorrPage() : BaseSettingsPage()
{

}

FreqCorrPage::~FreqCorrPage()
{
#ifdef WITH_FREQ_CORR
	g_deviceState.SetFreqCorrector(g_deviceState.GetConfig().GetFreqCorr());
#endif
}

/** **********************************************************************
*
* This method is invoked once on page creation phase.
* It is responsible for creation of UI controls
*
*********************************************************************** */
void FreqCorrPage::OnInitPageProtected()
{
	BaseSettingsPage::OnInitPageProtected();

	Label* pCaptionLabel = CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET, MSG_FREQ_CORRECTION);
	pCaptionLabel->SetSize(800, STYLE_PAGE_CAPTION_HEIGHT);
	pCaptionLabel->SetFont(FONT_CAPTION);
	pCaptionLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	uint16_t unYoffset = 150;

	CreateLabel(STYLE_PAGE_LEFT_OFFSET, unYoffset, MSG_FREQ_CORR_ENTER, STYLE_PAGE_KEY_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);
	m_pEditFreqVal = EditBox::CreateInstance<EditBoxDigit>(this, 0, STYLE_PAGE_VALUE_LABEL_XOFFSET, unYoffset, STYLE_PAGE_VALUE_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT, AL_NONE, 16);
	RegisterControl(m_pEditFreqVal);
#ifdef WITH_FREQ_CORR
	double dCorr = g_deviceState.GetConfig().GetFreqCorr();
	double dFreqVal = dCorr * BASE_FREQ_VAL + BASE_FREQ_VAL;
	m_pEditFreqVal->SetDValue(dFreqVal, 4, 4);
#endif
	m_pEditFreqVal->SetOnAfterValueChange(LCOnChange, this);
	m_pEditFreqVal->SetValidator(&s_validatorFreqVal);
	g_deviceState.SetOperationMode(STATEMODE_U);
	g_deviceState.SetDirecionType(false);
	g_deviceState.SetFreqCorrector(0.0);
	g_deviceState.SetValue(1.0, BASE_FREQ_VAL, true, NULL, NULL, false);
	g_deviceState.SwitchOut(true, true);
}

/** **********************************************************************
*
* Implementation of IOnClickCompatible interface.
* Function handles OnClick events from buttons
*
* @param enumBtn	action button which is pressed
* @param pContext	optional data defined on registration phase
*
* @return return true if event is handled otherwise false
*
*********************************************************************** */
bool FreqCorrPage::OnClick(FNBTN enumBtn, void* pContext)
{
	// Trigger on change call back to validate value
	m_pEditFreqVal->SetActive(false);
	m_pEditFreqVal->SetActive(true);

	double dNewFreqVal = m_pEditFreqVal->GetDValue();
	double dNewFreqCorr = (dNewFreqVal - BASE_FREQ_VAL) / BASE_FREQ_VAL;

	// Updating correction
#ifdef WITH_FREQ_CORR
	g_deviceState.GetConfig().SetFreqCorr(dNewFreqCorr);
#endif
	g_deviceState.SetFreqCorrector(dNewFreqCorr);

	return BaseSettingsPage::OnClick(enumBtn, pContext);;
}
