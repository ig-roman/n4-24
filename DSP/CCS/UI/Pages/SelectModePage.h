/*
 * SelectMode.h
 *
 *  Created on: Oct 30, 2013
 *      Author: Sergei Tero
 */

#ifndef SELECTMODE_H_
#define SELECTMODE_H_

#include "Menu.h"


/** **********************************************************************
 *
 * Class implementing the device mode selection page.
 *
 *********************************************************************** */
class SelectModePage : public Menu 
{
public:
	SelectModePage();
	virtual ~SelectModePage() {};

	virtual PAGE GetPageId() { return PAGE_SELECT_MODE; };
	virtual void OnInitPage();

	void OnManualClick();
};

#endif /* SELECTMODE_H_ */
