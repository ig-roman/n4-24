/*
 * Menu.h
 *
 *	Contains implementation for functional buttons.
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef MENU_H_
#define MENU_H_

#include "BasePage.h"
#include "../Controls/Button.h"
#include "IOnClickCompatible.h"

#define FNBTN_CONF_MAX_VERTICALY 6

#define FNBTN_CONF_VERT_START_X 695
#define FNBTN_CONF_VERT_START_Y 5
#define FNBTN_CONF_VERT_HEIGHT 66
#define FNBTN_CONF_VERT_LENGTH 105
#define FNBTN_CONF_VERT_SPACING 2

#define FNBTN_CONF_HOR_START_X 1
#define FNBTN_CONF_HOR_START_Y 425
#define FNBTN_CONF_HOR_HEIGHT 55
#define FNBTN_CONF_HOR_LENGTH 158
#define FNBTN_CONF_HOR_SPACING 2

// Following buttons has same positions across whole application
#define FNBTN_INVERT_SIGN	FNBTN_3
#define FNBTN_DIVIDER		FNBTN_4
#define FNBTN_EXIT			FNBTN_7
#define FNBTN_SAVE			FNBTN_8
#define FNBTN_PREV			FNBTN_10
#define FNBTN_NEXT			FNBTN_11
#define FNBTN_SENSE			FNBTN_6

typedef void (* FNOnFnClick)(FNBTN enumBtn, Button* pBtn, void* pContext);

typedef struct tagFNBTN_CONF					
{
	PAGE				enumTargetPageId;
	FNOnFnClick			pfnOnClickEvent;
	void*				pContext;
	Button*				pButton;
	IOnClickCompatible*	pOnClickCompatible;
} FNBTN_CONF, *PFNBTN_CONF;

class Menu : public BasePage {
public:
	Menu();
	virtual ~Menu();

	virtual void Draw();
	virtual void OnInitPage();
	virtual void OnButtonChanged(int8_t chButtonCode, bool bIsPressed);
	Button* RegisterFnButton(FNBTN enumFnKeyIdx, const char* pszLabel, IOnClickCompatible* pOnClickCompatible, void* pContext = NULL);
	Button* RegisterFnButton(FNBTN enumFnKeyIdx, const char* pszLabel = NULL, PAGE enumPageId = PAGE_EMPTY, FNOnFnClick pfnOnClickEvent = NULL, void* pContext = NULL);

	FNBTN_CONF& GetFnButtonConf(FNBTN enumFnKeyIdx) { return m_arrFnButtonsConf[enumFnKeyIdx]; };
	Button* GetFnButton(FNBTN enumFnKeyIdx);
	virtual void SyncState(bool bForce = false) { };

private:
	FNBTN_CONF m_arrFnButtonsConf[FNBTN_MAX];

	// ST TODO: remove;
	char* arrStrings[FNBTN_MAX];
};

#endif /* MENU_H_ */
