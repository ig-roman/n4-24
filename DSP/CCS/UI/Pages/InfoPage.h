/*
 * InfoPage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef INFOPAGE_H_
#define INFOPAGE_H_

#include "Menu.h"

class InfoPage: public Menu {
public:
	InfoPage();
	virtual ~InfoPage() {};

	virtual void Draw();
};

#endif /* INFOPAGE_H_ */
