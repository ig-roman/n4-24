/*
 * Startup.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "../PageManager.h"
#include "StartupPage.h"
#include "../Controls/Label.h"
#include "../Controls/Button.h"
#include "../../Logic/DeviceState.h"
#include "../../UIInterface.h"

#define WELCOME_PAGE_MIN_INIT_TIME 300
#define ININT_STEP_CAPTION_START_Y 250
#define ININT_STEP_CAPTION_SPACE_Y 30

#define INITSTEP_DEF( eVal, msg)  msg
static const uint16_t s_arrInitNames[] = { INITSTEP_DEFS };
#undef INITSTEP_DEF

extern "C" char g_HVBuzzerSuppress;
extern unsigned char g_langIdx;

StartupPage::StartupPage()
	: Menu(),
	m_DelayTime(0),
	m_eInitStep(INITSTEP_UART),
	m_bHasError(false),
	m_BkgDelayTime(0),
	nBkgStep(MIN_BACKLIGHT_INTENSITY)
{}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void StartupPage::OnInitPage()
{
	g_langIdx = g_deviceState.GetConfig().GetLang();
	Menu::OnInitPage();

	Label* pWelcomeLabel = CreateLabel(0, 0, MSG_BRAND_NAME, MAX_DISPLAY_LENGTH, 100);
	pWelcomeLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);
	pWelcomeLabel->SetFont(FONT_CAPTION);

	Label* pDevTypeLabel = CreateLabel(0, 100, MSG_DEVICE_TYPE, MAX_DISPLAY_LENGTH, DEFAULT_CONTROL_HEIGHT);
	pDevTypeLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);
	//pDevTypeLabel->SetFont(FONT_CAPTION);

	Label* pContLabel = CreateLabel(0, 140, MSG_DEVICE_NAME, MAX_DISPLAY_LENGTH, DEFAULT_CONTROL_HEIGHT);
	pContLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);
	//pContLabel->SetFont(FONT_CAPTION);

	Label* pVer = CreateLabel(650, 439, MSG_NONE, 148, DEFAULT_CONTROL_HEIGHT);
	pVer->SetAlignment(AL_RIGHT_JUSTIFY_TEXT | AL_BOTTOM_JUSTIFY_TEXT);
	pVer->SetValue(STR_SW_VERSION);

	g_HVBuzzerSuppress = g_deviceState.GetConfig().GetHVBuzzerSuppress();
}

/** **********************************************************************
 *
 * Virtual function to handle or lock any events from physical keyboard
 *
 * @param chButtonCode	key code of pressed, repeated or released button.
 * @param bIsPressed	true if button is pressed or repeat event otherwise it was released
 *
 *********************************************************************** */
void StartupPage::OnButtonChanged(int8_t chButtonCode, bool bIsPressed)
{

	// Disables keyboard events
	if (m_bHasError && INITSTEP_END == m_eInitStep)
	{
		if (chButtonCode == BTN_ENTER)
		{
			m_bHasError = false;
			return;
		}
		Menu::OnButtonChanged(chButtonCode, bIsPressed);
	}
}

/** **********************************************************************
 *
 * The periodic function. Is invoked always.
 * Use CheckUpdateTime() function for custom period.
 *
 *********************************************************************** */
void StartupPage::Loop()
{
	const DeviceConfig& config = g_deviceState.GetConfig();

	if (!m_DelayTime)
	{
		if (!m_BkgDelayTime)
		{
			// Let 200 ms time to draw UI
			CheckUpdateTime(200, &m_BkgDelayTime);
		}
		else if (CheckUpdateTime(50, &m_BkgDelayTime))
		{
			GetDC()->SetBacklightIntensity(nBkgStep);
			if (MIN_BACKLIGHT_INTENSITY == nBkgStep)
			{
				// Enable back lights
				GetDC()->SetBacklightMode(1);
			}
			nBkgStep++;
		}

		if (nBkgStep > config.GetBacklightIntensity())
		{
			PrintInitStepCaption();
			CheckUpdateTime(WELCOME_PAGE_MIN_INIT_TIME, &m_DelayTime);
		}
	}
	else if (CheckUpdateTime(WELCOME_PAGE_MIN_INIT_TIME, &m_DelayTime))
	{
		if (!m_bHasError && INITSTEP_END == m_eInitStep)
		{
			g_deviceState.SetOperationMode(STATEMODE_U);
			BasePage::SyncState();
		}
		else
		{
			void* pCtx = NULL;
			void* pLanCfg[2];
			if (INITSTEP_XPORT == m_eInitStep)
			{
				const LAN_CONF& lanCfg = config.GetLan();
				pLanCfg[0] = (void*)&lanCfg;
				pLanCfg[1] = (void*)config.GetPassword(true);
				pCtx = (void*)pLanCfg;
			}
			else if (INITSTEP_GPIB == m_eInitStep)
			{
				uint8_t unGPIBAddr = config.GetGPIBAddress();
				pCtx = (void*)unGPIBAddr;
			}
			else if (INITSTEP_UART == m_eInitStep)
			{
				const RS232_CONF& rsConf = config.GetRs232();
				pCtx = (void*)&rsConf;
			}

			if (INITSTEP_END != m_eInitStep)
			{
				const char* pszError = CUI_PostInit(m_eInitStep, pCtx);

				Label* pTestRes = CreateLabel(STYLE_PAGE_VALUE_LABEL_XOFFSET, GetStepPos(m_eInitStep), MSG_NONE, STYLE_PAGE_VALUE_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);
				pTestRes->SetValue(NULL == pszError ? "OK": pszError);
				if (NULL != pszError)
				{
					pTestRes->SetColor(COLOR_ERROR);
					m_bHasError = true;
				}

				m_eInitStep = (INITSTEP)(m_eInitStep + 1);
			}

			if (INITSTEP_END != m_eInitStep)
			{
				PrintInitStepCaption();
			}
		}
	}

	Menu::Loop();
}

uint16_t StartupPage::GetStepPos(INITSTEP eInitStep)
{
	uint16_t unPos = ININT_STEP_CAPTION_START_Y + (eInitStep * ININT_STEP_CAPTION_SPACE_Y);
	return unPos;
}

void StartupPage::PrintInitStepCaption()
{
	Label* pTestName = CreateLabel(STYLE_PAGE_LEFT_OFFSET, GetStepPos(m_eInitStep), MSG_NONE, STYLE_PAGE_KEY_LABEL_LENGTH, DEFAULT_CONTROL_HEIGHT);
	pTestName->SetValue(GetLangString(s_arrInitNames[m_eInitStep]));
}

void StartupPage::ShowFrame(Frame* pFrame, const char* pszBodyText)
{
	// Skip any error messages. Initialization step should provide more detailed information.
}
