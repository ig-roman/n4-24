/*
 * BasePage.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "BasePage.h"
#include "../Controls/WithNavigation.h"
#include "../Controls/Label.h"
#include "../PageManager.h"
#include "../../Logic/DeviceState.h"

#define RELEASE_POSITION 0xFFFF

BasePage::BasePage() :
	m_pDC(NULL),
	m_nActiveCtrl(-1),
	m_bIsInvalid(true),
	m_nOldSelectedCtrl(-1),
	m_chPrevBtn(0x00),
	m_unOldTouchX(RELEASE_POSITION),
	m_unOldTouchY(RELEASE_POSITION),
	m_unDefBkgColor(0),
	m_bPrevOverload(false),
	m_bHasInvalidChild(true),
	m_pErrorFrame(NULL),
	m_pInfoFrame(NULL)
{}

BasePage::~BasePage()
{
	for (unsigned int i = 0; i < m_arrUIControls.size(); i++)
	{
		UIControl* pUIControl = m_arrUIControls[i];
		delete pUIControl;
	}
	m_arrUIControls.resize(0);
}

uint8_t BasePage::RegisterControl(UIControl* pUIControl)
{
	m_arrUIControls.push_back(pUIControl);
	pUIControl->SetDC(m_pDC);
	return m_arrUIControls.size() - 1;
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void BasePage::OnInitPageBase()
{
	bool bOverload = g_deviceState.GetOverload();
	m_unDefBkgColor = bOverload ? COLOR_BG_ERROR : COLOR_BG_DEFAULT;

	// Create error frames first othewise OnInitPage can't show the messages.
	CreateFrame(m_pErrorFrame, GetLangString(MSG_ERROR), true);
	CreateFrame(m_pInfoFrame, GetLangString(MSG_WARNING), false);

	OnInitPage();

	// And register frame as last controll to get the higher z index (on top)
	m_pErrorFrame->SetId(RegisterControl(m_pErrorFrame));
	m_pInfoFrame->SetId(RegisterControl(m_pInfoFrame));
}

void BasePage::HasInvalidChild()
{
	m_bHasInvalidChild = true;
}

// #define SHOW_CTRL_BOUNDS_AND_UPDATES

#ifdef SHOW_CTRL_BOUNDS_AND_UPDATES
	#include <stdlib.h>
#endif

void BasePage::Draw()
{
	if (m_pDC)
	{
		#ifdef SHOW_CTRL_BOUNDS_AND_UPDATES
			uint32_t unRandColor = rand() % 0xffffff;
		#endif

		if (m_bIsInvalid)
		{
			m_pDC->SelectViewPort(0);
			m_pDC->SetBackgroundColor(m_unDefBkgColor);
			m_pDC->EraseDisplayArea(MAX_DISPLAY_LENGTH, MAX_DISPLAY_HEIGHT);
			m_pDC->SetFont(FONT_DEFAULT);
			DrawPageBackground();
			m_bHasInvalidChild = true;
		}

		if (m_bHasInvalidChild)
		{
			// At first, mark as invalid spioiled controls around this invalid control
			for (unsigned int i = 0; i < m_arrUIControls.size(); i++)
			{
				UIControl* pUIControl = m_arrUIControls[i];
				if (pUIControl->GetInvalid())
				{
					// Draw invisible elements
					if (!pUIControl->GetVisible())
					{
						pUIControl->Draw(); // Call to remove isInvalid state.
						#ifdef SHOW_CTRL_BOUNDS_AND_UPDATES
							m_pDC->SetAbsoluteCursorPosition(0,0);
							m_pDC->SetColor(unRandColor);
							m_pDC->DrawRectangle(0, pUIControl->GetRect().position.X, pUIControl->GetRect().position.Y, pUIControl->GetRect().size.X, pUIControl->GetRect().size.Y);
						#endif
					}

					const RECT_T& r2 = pUIControl->GetRect();

					m_pDC->SelectViewPort(0);
					m_pDC->SetBackgroundColor(GetBackgroundColor());
					m_pDC->SetCursorPosition(r2.position.X, r2.position.Y);
					m_pDC->EraseDisplayArea(r2.size.X, r2.size.Y);

					// Mark visible controls as invalid if they itersect with invalid
					for (unsigned int n = 0; n < m_arrUIControls.size(); n++)
					{
						if (n != i)
						{
							UIControl* pUIControlUnder = m_arrUIControls[n];
							if (pUIControlUnder->GetVisible() && !pUIControlUnder->GetInvalid())
							{
								const RECT_T& r1 = pUIControlUnder->GetRect();

								const uint16_t r1left	= r1.position.X;
								const uint16_t r1right	= r1.position.X + r1.size.X;
								const uint16_t r1top	= r1.position.Y;
								const uint16_t r1bottom	= r1.position.Y + r1.size.Y;

								const uint16_t r2left	= r2.position.X;
								const uint16_t r2right	= r2.position.X + r2.size.X;
								const uint16_t r2top	= r2.position.Y;
								const uint16_t r2bottom	= r2.position.Y + r2.size.Y;

								if (!(	r2left		>= r1right	|| 
										r2right		<= r1left	|| 
										r2top		>= r1bottom	||
										r2bottom	<= r1top		))
								{
									// ST TODO: add recursion?
									pUIControlUnder->SetInvalid();
								}
							}
						}
					}
				}
			}

			// Draw visible controls on clear area
			for (unsigned int i = 0; i < m_arrUIControls.size(); i++)
			{
				UIControl* pUIControl = m_arrUIControls[i];
				if (pUIControl->GetVisible() && (m_bIsInvalid || pUIControl->GetInvalid()))
				{
					pUIControl->Draw();
					#ifdef SHOW_CTRL_BOUNDS_AND_UPDATES
						m_pDC->SetAbsoluteCursorPosition(0,0);
						m_pDC->SetColor(unRandColor);
						m_pDC->DrawRectangle(0, pUIControl->GetRect().position.X, pUIControl->GetRect().position.Y, pUIControl->GetRect().size.X, pUIControl->GetRect().size.Y);
					#endif
				}
			}

			m_bIsInvalid = false;
			m_bHasInvalidChild = false;
			m_pDC->CopyBuffToMain();
		}
	}
}

Label* BasePage::CreateLabel(uint16_t unX, uint16_t unY, uint16_t unMsgId, uint16_t unSizeX, uint16_t unSizeY)
{
	Label* pLabel = new Label(this, m_arrUIControls.size(), unX, unY);
	if (MSG_NONE != unMsgId)
	{
		pLabel->SetValue(GetLangString(unMsgId));
	}
	if (0 != unSizeX || 0 != unSizeY)
	{
		POINT_T pSize = pLabel->GetRect().size;
		pSize.X = unSizeX ? unSizeX : pSize.X;
		pSize.Y = unSizeY ? unSizeY : pSize.Y;
		pLabel->SetSize(pSize.X, pSize.Y);
	}
	RegisterControl(pLabel);
	return pLabel;
}

void BasePage::TouchEvent(uint16_t unTouchX, uint16_t unTouchY, char bIsTouched)
{
	// Send release event to same coordinates where was toch event. Other wise buttons may be stoked.
	uint16_t unX = bIsTouched ? unTouchX : m_unOldTouchX;
	uint16_t unY = bIsTouched ? unTouchY : m_unOldTouchY;

	if (bIsTouched)
	{
		if (m_unOldTouchX != RELEASE_POSITION)
		{
			// Some how release event is missied. On development phase it vas when user make many very shorts touches on button.
			// Emulate it
			TouchEvent(m_unOldTouchX, m_unOldTouchY, false);
		}

		m_unOldTouchX = unTouchX;
		m_unOldTouchY = unTouchY;
	}
	else
	{
		m_unOldTouchX = RELEASE_POSITION;
		m_unOldTouchY = RELEASE_POSITION;
	}

	// Iterate controls from the most z-index to to last.
	for (int i = m_arrUIControls.size() -1; i >= 0 ; i--)
	{
		UIControl* pUIControl = m_arrUIControls[i];
		if (pUIControl->GetVisible() || CTRL_BUTTON == pUIControl->GetControlType())
		{
			const RECT_T& rectCtrl = pUIControl->GetRect();
			if (rectCtrl.position.X < unX && rectCtrl.position.X + rectCtrl.size.X > unX &&
				rectCtrl.position.Y < unY && rectCtrl.position.Y + rectCtrl.size.Y > unY)
			{
				if (CTRL_BUTTON == pUIControl->GetControlType() || CTRL_FRAME == pUIControl->GetControlType())
				{
					WithNavigation* pButton = (WithNavigation*)pUIControl;
					pButton->OnButtonChanged(BTN_ENTER, bIsTouched != 0);
				}
				else
				{
					if (bIsTouched)
					{
						SetActiveControl(i);
						pUIControl->TouchEvent(unTouchX, unTouchY, bIsTouched);
					}
				}
				break;
			}
		}
	}

	// Paint button immideatelly after touch
	if (bIsTouched)
	{
		Draw();
	}
}

/** **********************************************************************
 *
 * Virtual function to handle or lock any events from physical keyboard
 *
 * @param chButtonCode	key code of pressed, repeated or released button.
 * @param bIsPressed	true if button is pressed or repeat event otherwise it was released
 *
 *********************************************************************** */
void BasePage::OnButtonChanged(int8_t chButtonCode, bool bIsPressed)
{
	if (BTN_UP_WHEEL == chButtonCode || BTN_DOWN_WHEEL == chButtonCode)
	{
		m_chPrevBtn = chButtonCode; 
	}
	else if (BTN_WHEEL_PRESSED == chButtonCode && m_chPrevBtn)
	{
		chButtonCode = m_chPrevBtn;
	}

	if (bIsPressed)
	{
		if (BTN_TAB == chButtonCode)
		{
			int8_t nNewActive = -1;
			uint8_t unLoop = 0;
			unsigned int i = m_nActiveCtrl + 1;
			do
			{
				if (i >= m_arrUIControls.size())
				{
					i = 0;
				}
				UIControl* pUIControl = m_arrUIControls[i];
				if (pUIControl->GetWithNavigation() && pUIControl->GetVisible())
				{
					nNewActive = i;
					break;
				}

				i++;
				unLoop++;

			} while(unLoop < m_arrUIControls.size());

			if (-1 != nNewActive)
			{
				SetActiveControl(nNewActive);
			}
		}
		else if (BTN_MODE_SELECT == chButtonCode)
		{
			g_deviceState.SetOperationMode(STATEMODE_FULL_STANDBY);
			SyncPage();
		}
		else if (BTN_MODE_U == chButtonCode)
		{
			g_deviceState.SetOperationMode(STATEMODE_U);
			SyncPage();
		}
		else if (BTN_MODE_I == chButtonCode)
		{
			g_deviceState.SetOperationMode(STATEMODE_I);
			SyncPage();
		}
		else if (BTN_MODE_R == chButtonCode)
		{
			g_deviceState.SetOperationMode(STATEMODE_R);
			SyncPage();
		}
	}

	if (-1 != m_nActiveCtrl)
	{
		WithNavigation* pUINavControl = (WithNavigation*)m_arrUIControls[m_nActiveCtrl];
		pUINavControl->OnButtonChanged(chButtonCode, bIsPressed);
	}

	//Draw();
}

bool BasePage::SetActiveControl(int16_t nNewActiveCtrl)
{
	bool bRet = nNewActiveCtrl == m_nActiveCtrl;
	if (nNewActiveCtrl != m_nActiveCtrl)
	{
		UIControl* pUIControl = -1 == nNewActiveCtrl ? NULL : m_arrUIControls[nNewActiveCtrl];
		// "pUIControl" is null here when selection should be removed.
		bRet = NULL == pUIControl || (pUIControl->GetWithNavigation() && pUIControl->GetVisible());
		if (bRet)
		{
			if (-1 != m_nActiveCtrl)
			{
				WithNavigation* pUINavControlOld = (WithNavigation*)m_arrUIControls[m_nActiveCtrl];
				pUINavControlOld->SetActive(false);
			}

			if (pUIControl)
			{
				WithNavigation* pUINavControlNew = (WithNavigation*)pUIControl;
				pUINavControlNew->SetActive(true);
			}

			m_nActiveCtrl = nNewActiveCtrl;
		}
	}
	return bRet;
}

void BasePage::ResetSelection()
{
	m_nOldSelectedCtrl = -1;
}

void BasePage::SaveSelection()
{
	m_nOldSelectedCtrl = m_nActiveCtrl;
}

bool BasePage::RestoreSelection()
{
	bool bRet = false;
	if (-1 != m_nOldSelectedCtrl)
	{
		bRet = SetActiveControl(m_nOldSelectedCtrl);
		m_nOldSelectedCtrl = -1;
	}
	return bRet;
}

UIControl* BasePage::FindControlByCtrlId(int16_t nCtrlId) const
{
	UIControl* pUIControlRet = NULL;
	for (unsigned int i = 0; i < m_arrUIControls.size(); i++)
	{
		UIControl* pUIControl = m_arrUIControls[i];
		if (pUIControl->GetId() == nCtrlId)
		{
			pUIControlRet = pUIControl;
			break;
		}
	}
	return pUIControlRet;
}

/** **********************************************************************
 *
 * Finds registration id of UI control by his ID.
 * @param nCtrlId public ID of UI control
 *
 *********************************************************************** */
int16_t BasePage::FindCtrlRegIdByCtrlId(int16_t nCtrlId) const
{
	int16_t nRet = -1;
	for (unsigned int i = 0; i < m_arrUIControls.size(); i++)
	{
		UIControl* pUIControl = m_arrUIControls[i];
		if (pUIControl->GetId() == nCtrlId)
		{
			nRet = i;
			break;
		}
	}
	return nRet;
}

void BasePage::SetActiveControlByCtrlId(int16_t nNewActiveCtrlId)
{
	int16_t nCtrlRegisteredIdx  = -1;

	for (unsigned int i = 0; i < m_arrUIControls.size(); i++)
	{
		UIControl* pUIControl = m_arrUIControls[i];
		if (pUIControl->GetId() == nNewActiveCtrlId && pUIControl->GetVisible())
		{
			nCtrlRegisteredIdx = i;
			break;
		}
	}

	SetActiveControl(nCtrlRegisteredIdx);
}

PAGE BasePage::GetPageAccordingToMode()
{
	PAGE page = PAGE_EMPTY;

	switch (g_deviceState.GetOperationMode())
	{
		case STATEMODE_FULL_STANDBY:
		{
			page = PAGE_SELECT_MODE;
			break;
		}
		case STATEMODE_U:
		{
			page = PAGE_MODE_U;
			break;
		}
		case STATEMODE_I:
		{
			page = PAGE_MODE_I;
			break;
		}
		case STATEMODE_R:
		{
			page = PAGE_MODE_R;
			break;
		}
	}
	return page;
}

void BasePage::SyncState(bool bForce)
{

	PAGE page = GetPageAccordingToMode();
	if (GetPageId() != page)
	{
		// Page changed from device state. Not implemented now.  should be used in auto mode.
		SyncPage();
	}
}

void BasePage::SyncPage()
{
	PAGE page = GetPageAccordingToMode();
	if (PAGE_EMPTY != page)
	{
		g_pageManager.ChangePage(page);
	}
}

uint32_t g_unUIUpdateTime = 0;
/** **********************************************************************
 *
 * The periodic function. Is invoked always.
 * Use CheckUpdateTime() function for custom period.
 *
 *********************************************************************** */
void BasePage::Loop()
{
	bool bOverload = g_deviceState.GetOverload();
	if (bOverload != m_bPrevOverload)
	{
		m_bPrevOverload = bOverload;
		m_unDefBkgColor = bOverload ? COLOR_BG_ERROR : COLOR_BG_DEFAULT;

		OverloadChange(bOverload);
		m_bIsInvalid = true;
	}

	if (g_deviceState.UnTriggerRemoteCmd())
	{
		g_tracker.Enable();
		BasePage* page = g_pageManager.NeedPageChange();
		if (page != NULL)
		{
			g_pageManager.SetNewPage(NULL);
			g_pageManager.ShowPage(page);
		}
		SyncState(true);
	}

	if (CheckUpdateTime(200, &g_unUIUpdateTime))
	{
		Draw();
	}
}

void BasePage::CreateFrame(Frame*& pFrame, const char* pszCaptionText, bool bErrorStyle)
{
	pFrame = new Frame(this, 0, 100, 50);
	pFrame->SetSize(590, 300);
	pFrame->SetWithNavigation(true);
	pFrame->SetVisible(false);
	pFrame->SetCaption(pszCaptionText);

	pFrame->SetBackgroundColor(bErrorStyle ? COLOR_BG_ERROR : COLOR_BG_INFO);
	pFrame->SetColor(bErrorStyle ? COLOR_ERROR : COLOR_INFO);
}

void BasePage::ShowFrame(Frame* pFrame, const char* pszBodyText)
{
	if (pFrame)
	{
		pFrame->SetVisible(true);
		pFrame->SetValue(pszBodyText, true);
		// Reg ID copied to control ID
		SetActiveControl(pFrame->GetId());
	}
}

/** **********************************************************************
 *
 * Shows error message on the page in the error frame
 * @param error text to show
 *
 *********************************************************************** */
void BasePage::ShowError(const char* pszError)
{
	ShowFrame(m_pErrorFrame, pszError);
}

/** **********************************************************************
 *
 * Shows Info message on the page in the frame
 * @param pszText text to show
 *
 *********************************************************************** */
void BasePage::ShowInfo(const char* pszText)
{
	ShowFrame(m_pInfoFrame, pszText);
}

/** **********************************************************************
 *
 * Returns active control
 * @return active control or NULL if control is not visible
 *
 *********************************************************************** */
UIControl* BasePage::GetActiveControl()
{
	UIControl* pCtrlRet = NULL;
	if (m_nActiveCtrl > 0)
	{
		pCtrlRet = m_arrUIControls[m_nActiveCtrl];	
		if (!pCtrlRet->GetVisible())
		{
			pCtrlRet = NULL;
		}
	}
	return pCtrlRet;
}

const char* BasePage::GetFrameText(bool bError)
{
	Frame* pFrame = bError ? m_pErrorFrame : m_pInfoFrame;
	return pFrame && pFrame->GetVisible() ? pFrame->GetValue() : NULL;
}
