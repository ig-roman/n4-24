/*
 * UILocalization.h
 *
 *	Contains localized message constants.
 *
 *  Created on: Dec 27, 2013
 *      Author: Sergei Tero
 */

#ifndef UILOCALIZATION_H_
#define UILOCALIZATION_H_

#include "../../Common.h"

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!! FILE ENCODING MUST BE CP1251 !!! 
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#define MSG_DEFS \
MSG_DEF(MSG_NONE, \
	"", \
	"") \
MSG_DEF(MSG_MODE_SELECTION, \
	"Mode Selection", \
	"����� ������") \
MSG_DEF(MSG_MODE_SELECTION_DESCR, \
	"Select required mode using softkeys", \
	"�������� ����� ������, ��������� ������� ������") \
MSG_DEF(MSG_MODE_MANUAL, \
	"MANUAL", \
	"������") \
MSG_DEF(MSG_MODE_JOINT, \
	"JOINT", \
	"����������") \
MSG_DEF(MSG_MODE_INTERF, \
	"INTERF", \
	"��") \
MSG_DEF(MSG_CONFIG, \
	"CONFIG", \
	"������") \
MSG_DEF(MSG_MODE_CALIB, \
	"CALIB", \
	"������") \
MSG_DEF(MSG_MODE_TEST, \
	"TEST", \
	"����") \
MSG_DEF(MSG_MORE, \
	"MORE", \
	"���") \
MSG_DEF(MSG_EXIT, \
	"EXIT", \
	"�����") \
MSG_DEF(MSG_VIEW, \
	"VIEW", \
	"�����") \
MSG_DEF(MSG_MODE_CONFIGURATION, \
	"Configuration", \
	"������������") \
MSG_DEF(MSG_SERIAL_NO, \
	"Ser. No", \
	"���. No") \
MSG_DEF(MSG_REVISION, \
	"Rev.", \
	"������ ��:") \
MSG_DEF(MSG_PRESENT_SETINGS, \
	"Present Settings:", \
	"������� ���������:") \
MSG_DEF(MSG_LANGUAGE, \
	"Language", \
	"����") \
MSG_DEF(MSG_POWERUP_MODE, \
	"Power-up mode", \
	"����� ���.") \
MSG_DEF(MSG_BUS_ADDRESS, \
	"Bus Address", \
	"����� � ���") \
MSG_DEF(MSG_PLUS, \
	"+", \
	"+") \
MSG_DEF(MSG_MINUS, \
	"-", \
	"-") \
MSG_DEF(MSG_DISP_CONF_DESCTIPTION, \
	"Modify screen brightness by using the �-� and �+� keys", \
	"����������� ��������� ������, ��������� ������� '+' � '-'") \
MSG_DEF(MSG_INT_FEEDBACK, \
	"LOCAL SENSE", \
	"����� ��") \
MSG_DEF(MSG_EXT_FEEDBACK, \
	"REMOTE SENSE", \
	"����� ��") \
MSG_DEF(MSG_PEAK_VALUE, \
	"PEAK", \
	"U���") \
MSG_DEF(MSG_MEAN_VALUE, \
	"MEAN", \
	"U���") \
MSG_DEF(MSG_MUL10, \
	"X10", \
	"X10") \
MSG_DEF(MSG_DEV10, \
	":10", \
	":10") \
MSG_DEF(MSG_ZERO, \
	"ZERO", \
	"����") \
MSG_DEF(MSG_FEEDBACK, \
	"FB", \
	"��") \
	MSG_DEF(MSG_DIVIDER, \
	"DIV", \
	"���") \
MSG_DEF(MSG_S_AUTO, \
	"S-Aut.", \
	"�-���") \
MSG_DEF(MSG_DELTA_DC, \
	"C", \
	"C") \
MSG_DEF(MSG_DELTA_AC, \
	"C", \
	"C") \
MSG_DEF(MSG_UV, \
	"�V", \
	"�V") \
MSG_DEF(MSG_MV, \
	"mV", \
	"mV") \
MSG_DEF(MSG_V, \
	"V", \
	"V") \
MSG_DEF(MSG_KV, \
	"kV", \
	"kV") \
MSG_DEF(MSG_HZ, \
	"Hz", \
	"Hz") \
MSG_DEF(MSG_KHZ, \
	"kHz", \
	"kHz") \
MSG_DEF(MSG_MHZ, \
	"MHz", \
	"MHz") \
MSG_DEF(MSG_GHZ, \
	"GHz", \
	"GHz") \
MSG_DEF(MSG_U, \
	"U", \
	"U") \
MSG_DEF(MSG_I, \
	"I", \
	"I") \
MSG_DEF(MSG_R, \
	"R", \
	"R") \
MSG_DEF(MSG_INVERT_SIGN, \
	"-/+", \
	"-/+") \
MSG_DEF(MSG_UA, \
	"�A", \
	"�A") \
MSG_DEF(MSG_MA, \
	"mA", \
	"mA") \
MSG_DEF(MSG_A, \
	"A", \
	"A") \
MSG_DEF(MSG_OHM, \
	"Ohm", \
	"Ohm") \
MSG_DEF(MSG_KOHM, \
	"kOhm", \
	"kOhm") \
MSG_DEF(MSG_MOHM, \
	"MOhm", \
	"MOhm") \
MSG_DEF(MSG_BRAND_NAME, \
	"TEHNOJAKS", \
	"��������") \
MSG_DEF(MSG_DEVICE_NAME, \
	"N4-24", \
	"�4-24") \
MSG_DEF(MSG_DEVICE_TYPE, \
	"Calibrator", \
	"����������") \
MSG_DEF(MSG_OUTPUT, \
	"Output", \
	"�����") \
MSG_DEF(MSG_OFF, \
	"OFF", \
	"����") \
MSG_DEF(MSG_ON, \
	"ON", \
	"���") \
MSG_DEF(MSG_HV, \
	"HV", \
	"��") \
MSG_DEF(MSG_OF, \
	"of", \
	"��") \
MSG_DEF(MSG_SAVE, \
	"SAVE", \
	"���������") \
MSG_DEF(MSG_LV, \
	"LV", \
	"��") \
MSG_DEF(MSG_LOCAL_SENSE_ONLY, \
	"LOCAL SENSE ONLY", \
	"������ ����� ��") \
MSG_DEF(MSG_GO_RANGE_UP, \
	"GO SUBRANGE UP", \
	"���. ����������� �����") \
MSG_DEF(MSG_GO_RANGE_DOWN, \
	"GO SUBRANGE DOWN", \
	"���. ����������� ����") \
MSG_DEF(MSG_OUT_OF_RANGE, \
	"OUT OF RANGE", \
	"��� ���������") \
MSG_DEF(MSG_CONFIRM_OUT_PUT, \
	"PRESS OUTPUT ON", \
	"������ ����� ���.") \
MSG_DEF(MSG_RANGE_LOWER_LIMIT, \
	"RANGE LOWER LIMIT", \
	"������ ������ ���������") \
MSG_DEF(MSG_RANGE_UPPER_LIMIT, \
	"RANGE UPPER LIMIT", \
	"������� ������ ���������") \
MSG_DEF(MSG_SELECT_LANGUAGE, \
	"Select Lang", \
	"����� �����") \
MSG_DEF(MSG_LANG_RUS, \
	"Russian", \
	"�������") \
MSG_DEF(MSG_LANG_ENG, \
	"English", \
	"����.") \
MSG_DEF(MSG_SEL_LANG_IS_HIGHLIGHTED, \
	"The current language is indicated by the highlight.", \
	"��������� ���� ������������ ������������ ��������") \
MSG_DEF(MSG_CAPTION_ENTER_PASSWORD, \
	"PASSWORD ENTRY", \
	"���� ������") \
MSG_DEF(MSG_FOR_CONFIG, \
	"for CONFIGURATION", \
	"��� ������ ������������") \
MSG_DEF(MSG_ENTER_PASSWORD, \
	"Enter password", \
	"������� ������") \
MSG_DEF(MSG_INVALID_PASSWORD, \
	"Invalid password", \
	"������ ��������") \
MSG_DEF(MSG_NEXT, \
	"NEXT", \
	"�����") \
MSG_DEF(MSG_PREVIOUS, \
	"PREVIOUS", \
	"�����") \
MSG_DEF(MSG_SUBRANGE, \
	"Subrange:", \
	"�����������:") \
MSG_DEF(MSG_CAL_POINT, \
	"Calibration point:", \
	"����� ����������:") \
MSG_DEF(MSG_CAL_SELECT_MODE, \
	"Select the mode to calibrate", \
	"�������� ����� ������ ��� ����������") \
MSG_DEF(MSG_CAL_AJUST_U, \
	"Adjust the output voltage according to gauge readings", \
	"����������� �������� ���������� �� ���������� �������") \
MSG_DEF(MSG_CAL_AJUST_I, \
	"Adjust the output current according to gauge readings", \
	"����������� �������� ��� �� ���������� �������") \
MSG_DEF(MSG_CAL_DCU_HELP, \
	"Calibration in DC voltage mode.\n" \
	"\n" \
	"Adjusting linearity for the 10V subrange and voltage boundary values for all subranges.", \
	"���������� � ������ ���������� ����������� ����.\n" \
	"\n" \
	"������������� ���������� �������������� �� ������������ 10V � �������� ������� ���������� �� ������� � ������ �������� ���� �������������.") \
MSG_DEF(MSG_CAL_ACU_HELP, \
	"Calibration in AC voltage mode.\n" \
	"\n" \
	"Adjusting linearity for 1V, 10V, 100V, 1000V subranges at the 1kHz frequency and voltage boundary values for all subranges.", \
	"���������� � ������ ���������� ����������� ����.\n" \
	"\n" \
	"������������� ���������� �������������� �� ������������� 1V, 10V, 100V, 1000V �� ������� 1kHz � �������� ������� ���������� �� ������� � ������ �������� ���� �������������.") \
MSG_DEF(MSG_CAL_DCI_HELP, \
	"Calibration in DC current mode.\n" \
	"\n" \
	"Adjusting linearity for 100mA, 1A, 2A, 10A, 30A subranges and current boundary values for all subranges.", \
	"���������� � ������ ���� ����������� ����.\n" \
	"\n" \
	"������������� ���������� �������������� �� ������������� 100mA, 1A, 2�, 10A, 30A � ������ ���� ���� �� ������� � ������ �������� ���� �������������.") \
MSG_DEF(MSG_CAL_ACI_HELP, \
	"Calibration in AC current mode.\n" \
	"\n" \
	"Adjusting linearity for 100mA, 1A, 2A, 10A, 30A subranges at the 1kHz frequency and current boundary values for all subranges.", \
	"���������� � ������ ���� ����������� ����.\n" \
	"\n" \
	"������������� ���������� �������������� �� ������������� 100mA, 1�, 2�, 10A, 30A �� ������� 1kHz � �������� ���� ���� �� ������� � ������ �������� ���� �������������.") \
MSG_DEF(MSG_CAL_DCU_LIN_HELP, \
	"Please calibrate the linearity for selected subrange.", \
	"����������� ���������� ���������� �������������� ������� �� ��������� ������������.") \
MSG_DEF(MSG_CAL_ACU_LIN_HELP, \
	"Please calibrate the linearity for selected subrange at the all frequencies", \
	"����������� ���������� ���������� �������������� ������� �� ��������� ������������ � �� ���� ��������") \
MSG_DEF(MSG_CAL_DCI_LIN_HELP, \
	"Please calibrate the linearity for selected subrange", \
	"����������� ���������� ���������� �������������� ������� �� ��������� ������������") \
MSG_DEF(MSG_CAL_ACI_LIN_HELP, \
	"Please calibrate the linearity for selected subranges at all frequencies", \
	"����������� ���������� ���������� �������������� ������� �� ��������� ������������ � �� ���� ��������") \
MSG_DEF(MSG_CAL_DCU_MIN_MAX_HELP, \
	"Please calibrate voltage boundary values for selected DC subrange", \
	"����������� ���������� ������� ���������� �� ������� � ������ ������� ���������� ������������ ����������� ����") \
MSG_DEF(MSG_CAL_ACU_MIN_MAX_HELP, \
	"Please calibrate voltage boundary values for selected AC subrange at all frequencies.", \
	"����������� ���������� ������� ���������� �� ������� � ������ ������� ���������� ������������ � �� ���� ��������") \
MSG_DEF(MSG_CAL_DCI_MIN_MAX_HELP, \
	"Please calibrate current boundary values for selected DC subranges", \
	"����������� ���������� ����� �� ������� � ������ ������� ���������� ������������ ����������� ����") \
MSG_DEF(MSG_CAL_ACI_MIN_MAX_HELP, \
	"Please calibrate current boundary values for selected AC subrange at all frequencies", \
	"����������� ���������� ����� �� ������� � ������ ������� ���������� ������������ � �� ���� ��������") \
MSG_DEF(MSG_CAL_SELECT_SUBRANGE , \
	"Select the subrange for calibration", \
	"�������� ����������� ��� ���������� �������������� �������") \
MSG_DEF(MSG_OVERLOAD , \
	"OVERLOAD", \
	"����������") \
MSG_DEF(MSG_SUBRANGE_UP, \
	"SUBRANGE UP", \
	"����. �����") \
MSG_DEF(MSG_SUBRANGE_DOWN, \
	"SUBRANGE DOWN", \
	"����. ����") \
MSG_DEF(MSG_MAX_POINT, \
	"Level up", \
	"������� �����") \
MSG_DEF(MSG_MIN_POINT, \
	"Level down", \
	"������� ����") \
MSG_DEF(MSG_POINT_FREQ, \
	"Point frequency:", \
	"�������:") \
MSG_DEF(MSG_POINT_VALUE, \
	"Point value:", \
	"��������:") \
MSG_DEF(MSG_SAVE_CONFIRM, \
	"Please press \"Save\" button again to save changes", \
	"����������, ������� ������� \"���������\" ��� ���, ����� ��������� ���������") \
MSG_DEF(MSG_SAVE_DONE, \
	"Changes have been saved successfully", \
	"��������� ���� ������� ���������") \
MSG_DEF(MSG_EXIT_CONFIRM, \
	"Please press \"Exit\" button to discard changes", \
	"����������, ������� ������� \"�����\", ����� ���������� �� ���������") \
MSG_DEF(MSG_WARNING , \
	"WARNING", \
	"��������������") \
MSG_DEF(MSG_ERROR, \
	"ERROR", \
	"������") \
MSG_DEF(MSG_DELTA_ABS, \
	"ERROR", \
	"�������") \
MSG_DEF(MSG_ERR_HIGHER_THAN_REF, \
	"The calibration voltage is higher than max reference voltage at point %d", \
	"��������������� ���������� ����������� ������������ ������� ���������� � ����� %d") \
MSG_DEF(MSG_ERR_LOWER_THAN_REF, \
	"The calibration voltage is below zero at point %d", \
	"��������������� ���������� ������ ���� � ����� %d") \
MSG_DEF(MSG_CONNECT_LOAD , \
	"CONNECT LOAD", \
	"���������� ��������!") \
MSG_DEF(MSG_CAL_R_HELP, \
	"Calibration of resistors\n" \
	"\n" \
	"Adjusting resistance values for each built-in resistor", \
	"���������� ����������\n" \
	"\n" \
	"�������� ��������������� �������� ������������� ��� ������� ����������� ���������") \
MSG_DEF(MSG_CAL_AJUST_R, \
	"Adjust the resistance value according to gauge readings", \
	"������� �������� �������������") \
MSG_DEF(MSG_MSEC, \
	"ms", \
	"ms") \
MSG_DEF(MSG_SEC, \
	"sec", \
	"sec") \
MSG_DEF(MSG_MIN, \
	"min", \
	"min") \
MSG_DEF(MSG_HOUR, \
	"h.", \
	"h.") \
MSG_DEF(MSG_AUTO_MODE_CAPTION_ARRAY, \
	"Available arrays", \
	"��������� �������") \
MSG_DEF(MSG_AUTO_MODE_CAPTION_GROUPS, \
	"Available groups", \
	"��������� ������") \
MSG_DEF(MSG_AUTO_MODE_CAPTION_GROUP, \
	"Group properties", \
	"�������� ����������, �������� � ������") \
MSG_DEF(MSG_AUTO_MODE_CAPTION_NEW_GROUP, \
	"Creation of new group", \
	"�������� ����� ������") \
MSG_DEF(MSG_AUTO_MODE_INFO_NEW_GROUP, \
	"Enter values for new group", \
	"������� �������� ���������� ��� ����� ������") \
MSG_DEF(MSG_AUTO_MODE_PROP_ARRAY_NR, \
	"Array number", \
	"����� �������") \
MSG_DEF(MSG_AUTO_MODE_PROP_F_START, \
	"Start frequency", \
	"��������� �������") \
MSG_DEF(MSG_AUTO_MODE_PROP_F_STOP, \
	"Stop frequency", \
	"�������� �������") \
MSG_DEF(MSG_AUTO_MODE_PROP_F_INC, \
	"Increment of frequency", \
	"��� ���������� �������") \
MSG_DEF(MSG_AUTO_MODE_PROP_LOG_K_IDX, \
	"k", \
	"k") \
MSG_DEF(MSG_AUTO_MODE_PROP_DELAY, \
	"Delay", \
	"����� ��������") \
MSG_DEF(MSG_AUTO_MODE_PROP_INC_FUNC, \
	"Increment function", \
	"����������� ���������� �������") \
MSG_DEF(MSG_AUTO_MODE_FUNC_LOG, \
	"LOG", \
	"���") \
MSG_DEF(MSG_AUTO_MODE_FUNC_LIN, \
	"LINEAR", \
	"���") \
MSG_DEF(MSG_ENTER_NR_OR_USE_UI_BTN, \
	"Enter number or use on screen buttons", \
	"������� ����� ��� ����������� �������") \
MSG_DEF(MSG_DELETE , \
	"DELETE", \
	"�������") \
MSG_DEF(MSG_START, \
	"START", \
	"�����") \
MSG_DEF(MSG_STOP, \
	"STOP", \
	"����") \
MSG_DEF(MSG_CREATE, \
	"CREATE", \
	"�������") \
MSG_DEF(MSG_GROUPS, \
	"GROUPS", \
	"������") \
MSG_DEF(MSG_ARRAYS, \
	"ARRAYS", \
	"�������") \
MSG_DEF(MSG_MOVE_LEFT, \
	"MOVE <", \
	"����� <") \
MSG_DEF(MSG_MOVE_RIGHT, \
	"MOVE >", \
	"����� >") \
MSG_DEF(MSG_NO_SPACE_FOR_GROUP, \
	"No free space available to save new group", \
	"��� ��������� ������ ��� ���������� ������") \
MSG_DEF(MSG_NO_ARRAY, \
	"Array is not selected!", \
	"������ �� ������!") \
MSG_DEF(MSG_NO_GROUP, \
	"Group is not selected!", \
	"������ �� �������!") \
MSG_DEF(MSG_ARRAY_NR, \
	"Array", \
	"������") \
MSG_DEF(MSG_TOTAL_GROUPS, \
	"Groups count", \
	"���-�� ����� � ������� ") \
MSG_DEF(MSG_SELECTED_GROUP, \
	"Active group", \
	"�������� ������") \
MSG_DEF(MSG_FOR_CALIBR, \
	"for CALIBRATION", \
	"��� ������ ����������") \
MSG_DEF(MSG_DELETE_ARRAY_CONFIRM, \
	"Press \"DELETE\" button again to delete all groups inside selected array", \
	"������� ������� \"�������\" ��� ��� ��� �������� ���� �����, �������� � ������") \
MSG_DEF(MSG_DELETE_GROUP_CONFIRM, \
	"Press \"DELETE\" button again to delete selected group", \
	"������� ������� \"�������\" ��� ��� ��� �������� ��������� ������") \
MSG_DEF(MSG_SHUNT, \
	"Shunt", \
	"����") \
MSG_DEF(MSG_UREF, \
	"Ushunt: %01.6f%s Umes:", \
	"U�: %01.6f%s U���:") \
MSG_DEF(MSG_SAVE_FIRST, \
	"Please save changes first", \
	"���������� ��������� ���������") \
MSG_DEF(MSG_CONNECT_CURRENT_AMP, \
	"Connect the voltage-current converter", \
	"���������� ��������������� ����������-���") \
MSG_DEF(MSG_DISCONNECT_CURRENT_AMP, \
	"Disconnect the voltage-current converter", \
	"��������� ��������������� ����������-���") \
MSG_DEF(MSG_DISABLE, \
	"DISABLE", \
	"���������") \
MSG_DEF(MSG_ENABLE, \
	"ENABLE", \
	"��������") \
MSG_DEF(MSG_CAL_DISABLE_INFO, \
	"The calibration logic can be temporarily disabled\n" \
	"by pressing the \"DISABLE\" button.\n" \
	"This setting will be in \"ENABLED\" state after device restart.", \
	"���������� ������� ����� ���� �������� ���������\n" \
	"��������� ������� \"���������\".\n" \
	"��� ����� ����� ������ \"��������\" ����� ���������� ��������� �������.") \
MSG_DEF(MSG_CAL_SHUNT_HELP_CAPTION, \
	"Calibration of current\n" \
	"using auxiliary shunts", \
	"���������� ���� ����\n" \
	"� ������� ��������������� ������") \
MSG_DEF(MSG_CAL_SHUNT_HELP1, \
	"Calibration procedure:\n" \
	"\n" \
	"- Supply the shown %s voltage Uref from the calibrator to the gauge via the 4-wire cable.\n" \
	"- Note the result of the measurement Umeas.\n" \
	"- Turn the OUTPUT of the calibrator OFF.", \
	"��������� ����������:\n" \
	"- ������� �������������� �������� �����(R�.�), ��������� ������� \"����\"\n" \
	"- ������� �� ����������� �� ������������� ������ ���������� %s\n" \
	"U� = U�� * R�.� / R�.���, ��������� ������� \"����� ���\". ��� U�� � R�.��� ����� ������� ������ �� �������� ���� ����������\n" \
	"- ���������� ��������� ��������� ��� U���") \
MSG_DEF(MSG_CAL_SHUNT_HELP2, \
	"- Connect the specified shunt to the �-� �Current output� of the calibrator.\n" \
	"- Connect the gauge in parallel to the shunt in %s voltage measurement mode.\n" \
	"- Note the result of the measurement Ushu = Umeas1..� (Rshu nom./Rshu true)\n" \
	"- Supply the current specified by the calibration point from the calibrator and follow on-screen instructions.", \
	"- ��������� ������� \"����\", ����������� ���������� � ����� ��������������� ����\n" \
	"- ������������ � ������� ����������� � � � ������ ������ I� ����, ��������� �� �������\n" \
	"- ������������ ����������� ����� ������������� ������ � ������ ��������� ���������� %s\n" \
	"- ������� �� ����������� ���, �������� ������������� ������, ��������� ������� \"����� ���\"\n" \
	"- ������������ ��� �� ��������� U��� �� ������������� �������") \
MSG_DEF(MSG_CAL_SHUNT_MODE_AC, \
	"AC", \
	"����������� ����") \
MSG_DEF(MSG_CAL_SHUNT_MODE_DC, \
	"DC", \
	"����������� ����") \
MSG_DEF(MSG_UI_LOCKED, \
	"Interface has been locked remotely", \
	"��������� �������� ������������") \
MSG_DEF(MSG_CTRL_CAPTION, \
	"Select instrument control mode", \
	"�������� ����� ������ �����������") \
MSG_DEF(MSG_CTRL_INFO, \
	"The \"REMOTE\" operation mode never stored permanently" , \
	"\"��\" ����� �� ����������� � ����������") \
MSG_DEF(MSG_ETHERNET_CONF, \
	"Ethernet (IP addr / mask / port):", \
	"��������� Ethernet (IP ����� / ����� / ����):") \
MSG_DEF(MSG_ETHERNET, \
	"Ethernet", \
	"Ethernet") \
MSG_DEF(MSG_GPIB, \
	"GPIB", \
	"GPIB") \
MSG_DEF(MSG_RS232, \
	"RS232", \
	"RS232") \
MSG_DEF(MSG_IP_ADDR, \
	"IP address", \
	"IP �����") \
MSG_DEF(MSG_MASK, \
	"Host part", \
	"����� �����") \
MSG_DEF(MSG_PORT, \
	"Port", \
	"����") \
MSG_DEF(MSG_BITS, \
	"bits", \
	"bits") \
MSG_DEF(MSG_LAN_SETTINGS_CAPTION, \
	"TCP/IP Settings", \
	"��������� TCP/IP") \
MSG_DEF(MSG_MASK_INFO, \
	"Additional settings are available via WEB interface.\n" \
	"Use port 80 and the IP address bellow to access.\n" \
	"WEB-interface uses the password for configuration.\n" \
	"Examples of the use of the host bits in the network:\n" \
	"Class A: 24 bits; Class B: 16 bits; Class C: 8 bits", \
	"�������������� ��������� �������� ����� WEB ���������.\n" \
	"����������� ���� 80 � IP ����� ��������� ����, ��� �������.\n" \
	"WEB ��������� ���������� ������ ��� ������������.\n" \
	"������� ������������� ����� ����� � �����:\n" \
	"����� A: 24 ����; ����� B: 16 ���; ����� C: 8 ���") \
MSG_DEF(MSG_RESTART_REQ, \
	"To apply changes\n" \
	"calibrator should be restarted", \
	"��� ��������� ��������,\n" \
	"���������� ���������� �������������") \
MSG_DEF(MSG_GPIB_SETTINGS_CAPTION, \
	"GPIB Settings", \
	"��������� GPIB") \
MSG_DEF(MSG_INIT_KBD, \
	"keyboard", \
	"����������") \
MSG_DEF(MSG_INIT_SERIAL, \
	"serial port", \
	"���������������� ����") \
MSG_DEF(MSG_INIT_USB, \
	"universal serial bus (USB)", \
	"USB") \
MSG_DEF(MSG_INIT_LAN, \
	"Ethernet", \
	"Ethernet") \
MSG_DEF(MSG_INIT_AARM, \
	"executive controller", \
	"�������������� ������") \
MSG_DEF(MSG_INIT_GPIB, \
	"GPIB interface", \
	"GPIB ������") \
MSG_DEF(MSG_PAR_EVEN, \
	"EVEN", \
	"������") \
MSG_DEF(MSG_PAR_NONE, \
	"NONE", \
	"���") \
MSG_DEF(MSG_PAR_ODD, \
	"ODD", \
	"��������") \
MSG_DEF(MSG_PAR_ONE, \
	"ONE", \
	"����") \
MSG_DEF(MSG_RS232_SETTINGS_CAPTION, \
	"RS-232 Settings", \
	"��������� ����������������� ����� RS-232") \
MSG_DEF(MSG_BAUD_RATE, \
	"Baud rate", \
	"�������� �����. ����") \
MSG_DEF(MSG_PARITY, \
	"Parity", \
	"��������") \
MSG_DEF(MSG_STOP_BIT, \
	"Stop bit", \
	"����-���") \
MSG_DEF(MSG_BOD, \
	"baud", \
	"baud") \
MSG_DEF(MSG_RS232_CONF, \
	"RS-232 (Baud rate / Parity / Stop bit):", \
	"��������� RS-232 (�������� / �������� / ����-���):") \
MSG_DEF(MSG_SAFETY_VOLTAGE, \
	"Safety voltage", \
	"������. ������") \
MSG_DEF(MSG_HV_SETTINGS, \
	"The safety limit of high voltage", \
	"��������� ������ ���������� ������") \
MSG_DEF(MSG_HV_DEFAULT, \
	"DEFAULT %dV", \
	"����. %dV") \
MSG_DEF(MSG_PSW_CAPTION_CALIB, \
	"Enter the new CALIBRATION password ", \
	"������� ����� ������ ��� ������ ����������") \
MSG_DEF(MSG_PSW_CAPTION_CONFIG, \
	"Enter the new CONFIGURATION password ", \
	"������� ����� ������ ��� ������ ������������") \
MSG_DEF(MSG_PSW_NEW1, \
	"New password", \
	"����� ������") \
MSG_DEF(MSG_PSW_NEW2, \
	"Confirm the password", \
	"����������� ������") \
MSG_DEF(MSG_PSW_CHANGE, \
	"CH. PSW.", \
	"���. ������") \
MSG_DEF(MSG_LAB_PARAMS, \
	"LAB. DETAILS", \
	"���. �����.") \
MSG_DEF(MSG_LAB_TEMP, \
	"Laboratory temperature", \
	"����������� � ���.") \
MSG_DEF(MSG_LAB_HUMID, \
	"Laboratory humidity", \
	"��������� � ���.") \
MSG_DEF(MSG_LAB_DATE, \
	"Calibration date", \
	"���� ����������") \
MSG_DEF(MSG_DEGC, \
	"�C", \
	"�C") \
MSG_DEF(MSG_PERCENT, \
	"%", \
	"%") \
MSG_DEF(MSG_PLUS_MINUS, \
	"�", \
	"�") \
MSG_DEF(MSG_BTN_LAB, \
	"Lab. info", \
	"���.�����.") \
MSG_DEF(MSG_CAPTION_LAB_INFO, \
	"Laboratory information", \
	"������������ ���������") \
MSG_DEF(MSG_CAPTION_LAB_INFO_CHANGE, \
	"Enter the information for the new laboratory", \
	"������� ����� ��������� �����������") \
MSG_DEF(MSG_UREF_BTN, \
	"U REF", \
	"U �����.") \
MSG_DEF(MSG_CLEAR, \
	"CLEAR", \
	"��-\n������") \
MSG_DEF(MSG_CALIBRATION_DISABLED, \
	"Calibration data DISABLED", \
	"������������� ��������� ���������") \
MSG_DEF(MSG_CLEAR_CONFIRM, \
	"Please press \"CLEAR\" button again to clear calibration data to default state", \
	"����������, ������� ������� \"��������\" ��� ���, ����� �������� ������������� ������ �� ��������� ���������") \
MSG_DEF(MSG_DISABLE_OUT, \
	"Disable output", \
	"��������� �����") \
MSG_DEF(MSG_EXT_DIV, \
	"External divider", \
	"������� ��������") \
MSG_DEF(MSG_CONNECT_EXT_DIV, \
	"Connect external divider", \
	"���������� ������� ��������") \
MSG_DEF(MSG_DISCONNECT_EXT_DIV, \
	"Disconnect external divider", \
	"��������� ������� ��������") \
MSG_DEF(MSG_2_WIRE, \
	"2-WIRE CONN", \
	"2-� ��. ����") \
MSG_DEF(MSG_4_WIRE, \
	"4-WIRE CONN", \
	"4-� ��. ����") \
MSG_DEF(MSG_WIRE, \
	"CONN", \
	"����") \
MSG_DEF(MSG_FREQ_CORR, \
	"Freq correction", \
	"����. �������") \
MSG_DEF(MSG_FREQ_CORRECTION, \
	"Frequency correction set", \
	"��������� ������������� �������") \
MSG_DEF(MSG_FREQ_CORR_ENTER, \
	"Enter frequency counter readings", \
	"������� ��������� �����������") \
MSG_DEF(MSG_HV_SUPPRESS, \
	"SOUND OFF", \
	"����. �����") \
MSG_DEF(MSG_OUT_OF_MEMORY, \
	"Out of memory!", \
	"������������ ������")

#define MSG_DEF(eVal, end, rus) eVal,
typedef enum { MSG_DEFS MSG_MAX } EMSG;
#undef MSG_DEF

void ReloadLangStrings();

#ifdef __cplusplus
extern "C" {
#endif

const char* GetLangString(uint16_t unMesgId);
EMSG GetRS232ParityName(RS232PAR ePar);

#ifdef __cplusplus
}
#endif

#endif /* UILOCALIZATION_H_ */
