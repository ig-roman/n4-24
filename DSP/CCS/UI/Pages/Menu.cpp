/*
 * Menu.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "Menu.h"
#include "../PageManager.h"
#include "../Controls/Label.h"
#include <string.h>

Menu::Menu()
	: BasePage()
{
	memset(m_arrFnButtonsConf, NULL, sizeof m_arrFnButtonsConf);
	arrStrings[0] = NULL;
}

void Menu::Draw()
{
	BasePage::Draw();
}

void OnFnBtnClicked(Button* pButton, void* pContext)
{
	Menu* pThis = (Menu*)pContext;
	FNBTN enumBtnId = (FNBTN)pButton->GetId();
	FNBTN_CONF structFnBtnConf = pThis->GetFnButtonConf(enumBtnId);

	if (structFnBtnConf.pfnOnClickEvent)
	{
		structFnBtnConf.pfnOnClickEvent(enumBtnId, pButton, structFnBtnConf.pContext);
	}

	if (PAGE_EMPTY != structFnBtnConf.enumTargetPageId)
	{
		g_pageManager.ChangePage(structFnBtnConf.enumTargetPageId);
	}

	if (structFnBtnConf.pOnClickCompatible)
	{
		structFnBtnConf.pOnClickCompatible->OnClick(enumBtnId, structFnBtnConf.pContext);
	}
}

// ST TODO: remove it
Menu::~Menu()
{
#ifdef WIN32
	if (arrStrings[0])
	{
		// page with menu
		for (int i = 0; i < FNBTN_MAX; i++)
		{
			char* pszBuff = arrStrings[i];
			delete pszBuff;
		}
	}
#endif
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void Menu::OnInitPage()
{
	uint16_t nX = FNBTN_CONF_VERT_START_X;
	uint16_t nY = FNBTN_CONF_VERT_START_Y;

	uint16_t nXIncr = 0;
	uint16_t nYIncr = FNBTN_CONF_VERT_HEIGHT + FNBTN_CONF_VERT_SPACING;

	uint16_t nDX = FNBTN_CONF_VERT_LENGTH;
	uint16_t nDY = FNBTN_CONF_VERT_HEIGHT;

	for (int i = 0; i < FNBTN_MAX; i++)
	{
		FNBTN_CONF& structFnBtnConf = m_arrFnButtonsConf[i];
		Button* pFnButton = structFnBtnConf.pButton;

		if (FNBTN_CONF_MAX_VERTICALY == i)
		{
			nX = FNBTN_CONF_HOR_START_X;
			nY = FNBTN_CONF_HOR_START_Y;
			nDX = FNBTN_CONF_HOR_LENGTH;
			nDY = FNBTN_CONF_HOR_HEIGHT;
			nXIncr = FNBTN_CONF_HOR_LENGTH + FNBTN_CONF_HOR_SPACING;
			nYIncr = 0;
		}

		if (!pFnButton)
		{
			// Not registered buttons
			pFnButton = new Button(this, i, nX, nY);
			pFnButton->SetWithNavigation(false);
			structFnBtnConf.pButton = pFnButton;
			pFnButton->SetVisible(false);
			RegisterControl(pFnButton);
		}
		else
		{
			pFnButton->SetPosition(nX, nY);
		}

#ifdef WIN32
		Label* label = new Label(this, 0, nX + (i >= FNBTN_CONF_MAX_VERTICALY ? 60 : 120), nY + (i >= FNBTN_CONF_MAX_VERTICALY ? 65 : 15));
		char* pszBuff = new char[6];
		arrStrings[i] = pszBuff;
		sprintf(pszBuff, "(F%d)", i + 2);
		label->SetValue(pszBuff);
		label->SetSize(55, DEFAULT_CONTROL_HEIGHT);
		RegisterControl(label);
#endif

		pFnButton->SetSize(nDX, nDY);

		nX += nXIncr;
		nY += nYIncr;
	}
}

Button* Menu::GetFnButton(FNBTN enumFnKeyIdx)
{
	Button* pBtnRet = NULL;
	for (int i = 0; i < FNBTN_MAX; i++)
	{
		FNBTN_CONF& structFnBtnConf = m_arrFnButtonsConf[i];
		Button* pFnButton = structFnBtnConf.pButton;
		if (pFnButton->GetId() == enumFnKeyIdx)
		{
			pBtnRet = pFnButton;
			break;
		}
	}

	return pBtnRet;
}

Button* Menu::RegisterFnButton(FNBTN enumFnKeyIdx, const char* pszLabel, IOnClickCompatible* pOnClickCompatible, void* pContext /* = NULL*/)
{
	Button* pFnButton = RegisterFnButton(enumFnKeyIdx, pszLabel, PAGE_EMPTY, NULL, pContext);
	FNBTN_CONF& structFnBtnConf = m_arrFnButtonsConf[enumFnKeyIdx];
	structFnBtnConf.pOnClickCompatible = pOnClickCompatible;
	return pFnButton;
}

Button* Menu::RegisterFnButton(FNBTN enumFnKeyIdx, const char* pszLabel /* = NULL*/, PAGE enumPageId /* = PAGE_EMPTY*/, FNOnFnClick pfnOnClickEvent /* = NULL*/, void* pContext /* = NULL*/)
{
	FNBTN_CONF& structFnBtnConf = m_arrFnButtonsConf[enumFnKeyIdx];
	Button* pFnButton = structFnBtnConf.pButton;

	if (!pFnButton)
	{
		pFnButton = new Button(this, enumFnKeyIdx, 0, 0);
		RegisterControl(pFnButton);
		structFnBtnConf.pButton = pFnButton;
	}

	pFnButton->SetWithNavigation(false);
	pFnButton->SetFont(FONT_MENU);
	pFnButton->SetValue(pszLabel);
	pFnButton->SetOnClickEvent(OnFnBtnClicked, this);
	structFnBtnConf.enumTargetPageId = enumPageId;
	structFnBtnConf.pfnOnClickEvent = pfnOnClickEvent;
	structFnBtnConf.pContext = pContext;
	structFnBtnConf.pOnClickCompatible = NULL;
	pFnButton->SetVisible(!!pszLabel && !!pszLabel[0]);

	return pFnButton;
}

/** **********************************************************************
 *
 * Virtual function to handle or lock any events from physical keyboard
 *
 * @param chButtonCode	key code of pressed, repeated or released button.
 * @param bIsPressed	true if button is pressed or repeat event otherwise it was released
 *
 *********************************************************************** */
void Menu::OnButtonChanged(int8_t chButtonCode, bool bIsPressed)
{
	int8_t unFNButtontIdx = -1;
	switch (chButtonCode)
	{
		// In windows simulator FN codes are positive but in real hardware are negative.
		case BTN_FN1:	unFNButtontIdx = 0;		break;
		case BTN_FN2:	unFNButtontIdx = 1;		break;
		case BTN_FN3:	unFNButtontIdx = 2; 	break;
		case BTN_FN4:	unFNButtontIdx = 3; 	break;
		case BTN_FN5:	unFNButtontIdx = 4; 	break;
		case BTN_FN6:	unFNButtontIdx = 5; 	break;
		case BTN_FN7:	unFNButtontIdx = 6; 	break;
		case BTN_FN8:	unFNButtontIdx = 7; 	break;
		case BTN_FN9:	unFNButtontIdx = 8; 	break;
		case BTN_FN10:	unFNButtontIdx = 9;		break;
		case BTN_FN11:	unFNButtontIdx = 10;	break;
	}

	if (-1 != unFNButtontIdx)
	{
		FNBTN_CONF& structFnBtnConf = m_arrFnButtonsConf[unFNButtontIdx];
		Button* pFnButton = structFnBtnConf.pButton;
		if (pFnButton)
		{
			pFnButton->OnButtonChanged(BTN_ENTER, bIsPressed);
		}
	}

	// Contains DRAW method.
	BasePage::OnButtonChanged(chButtonCode, bIsPressed);
}

