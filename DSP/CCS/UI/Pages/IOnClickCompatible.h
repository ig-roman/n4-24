/*
 * IOnClickCompatible.h
 *
 *  Created on: Feb 27, 2014
 *      Author: Sergei Tero
 */

#ifndef IONCLICKCOMPATIBLE_H_
#define IONCLICKCOMPATIBLE_H_

#include "../Common.h"

class IOnClickCompatible
{
public:
	virtual ~IOnClickCompatible() {};
	virtual bool OnClick(FNBTN enumBtn, void* pContext) = 0;
};

#endif /* IONCLICKCOMPATIBLE_H_ */
