/*
 * BasePage.h
 *
 *	Base page for all UI pages. Contains common functions for all pages.
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef BASEPAGE_H_
#define BASEPAGE_H_

#include "UILocalization.h"
#include "../Common.h"
#include "../DrawContext.h"
#include "../Controls/UIControl.h"
#include "../Controls/Frame.h"
#include "../../LogicInterface.h"
#include "../IParent.h"
#include "UILocalization.h"
#include <vector>

using std::vector;
typedef vector<UIControl*> TUIControlsArray;

class Label;
class BasePage : public IParent
{
public:
	BasePage();
	virtual ~BasePage();

	virtual PAGE GetPageId() = 0;
	virtual void OnInitPage() = 0;
	virtual void Loop();
	virtual void DrawPageBackground() {};
	virtual void OnButtonChanged(int8_t chButtonCode, bool bIsPressed);
	virtual void TouchEvent(uint16_t unX, uint16_t unY, char bIsTouched);
	virtual PAGE GetPageAccordingToMode();

	void OnInitPageBase();
	void Draw();

	void SetDC(DrawContext* pDC)	{ m_pDC = pDC; };
	DrawContext* GetDC()			{ return m_pDC; };

	void SetInvalid()				{ m_bIsInvalid = true; };
	UIControl* GetActiveControl();

	UIControl* FindControlByCtrlId(int16_t nCtrlId) const;
	UIControl* GetControlByRegId(uint16_t unCtrlRegId) const { return unCtrlRegId >= m_arrUIControls.size() ? NULL : m_arrUIControls[unCtrlRegId]; };
	int16_t FindCtrlRegIdByCtrlId(int16_t nCtrlId) const;

	const char* GetLangString(uint16_t unMesgId) const { return ::GetLangString(unMesgId); };
	void ReloadLangStrings() { return ::ReloadLangStrings(); };
	Label* CreateLabel(uint16_t unX, uint16_t unY, uint16_t unMsgId, uint16_t unSizeX = 0, uint16_t unSizeY = 0);

	// IParent implementation
	virtual uint32_t GetBackgroundColor() const { return m_unDefBkgColor; };
	virtual void HasInvalidChild();
	void ShowError(const char* pszError);
	void ShowInfo(const char* pszText);
	void HideInfo() { m_pInfoFrame->SetVisible(false); }
	void HideError() { m_pErrorFrame->SetVisible(false); }

	const char* GetFrameText(bool bError);

	uint8_t RegisterControl(UIControl*);
	bool SetActiveControl(int16_t nNewActiveCtrl);

protected:
	virtual void ShowFrame(Frame* pFrame, const char* pszBodyText);
	void SetActiveControlByCtrlId(int16_t nNewActiveCtrlId);
	void SaveSelection();
	bool RestoreSelection();
	void ResetSelection();
	virtual void SyncState(bool bForce = false);
	void SyncPage();
	virtual void OverloadChange(bool bOverload) {};
	void CreateFrame(Frame*& pFrame, const char* pszCaptionText, bool bErrorStyle);

private:
	char m_chPrevBtn;
	int16_t m_nActiveCtrl;
	int16_t m_nOldSelectedCtrl;
	DrawContext* m_pDC;
	TUIControlsArray m_arrUIControls;

	uint16_t m_unOldTouchX;
	uint16_t m_unOldTouchY;

	uint32_t m_unDefBkgColor;
	bool m_bPrevOverload;

	bool m_bHasInvalidChild;
	bool m_bIsInvalid;

	Frame* m_pErrorFrame;
	Frame* m_pInfoFrame;
};

typedef const struct tagCONFIRM_DATA
{
	FNBTN eActionBtn;
	EMSG eMsg;
}CONFIRM_DATA, *PCONFIRM_DATA;


class ConfirmPopUp
{
public:
	ConfirmPopUp(const PCONFIRM_DATA pConfig, BasePage* pPage) :
		m_bIsModified(false),
		m_pConfig(pConfig),
		m_pPage(pPage)
	{
		uint8_t unButtonsCount = 0;
		for (;m_pConfig[unButtonsCount].eMsg != MSG_NONE; unButtonsCount++);

		pConfirmedBtns = new bool[unButtonsCount];
		SetIsModified(false);
	}

	virtual ~ConfirmPopUp()
	{
		delete[] pConfirmedBtns;
	}

	bool OnClick(FNBTN enumBtn)
	{
		bool bEventProcessed = false;

		for (uint8_t unIdx = 0; m_pConfig[unIdx].eMsg != MSG_NONE; unIdx++)
		{
			const CONFIRM_DATA config = m_pConfig[unIdx];
			if (enumBtn == config.eActionBtn)
			{
				// This is attached button
				if (m_bIsModified && !pConfirmedBtns[unIdx])
				{
					m_pPage->ShowInfo(GetLangString(config.eMsg));
					pConfirmedBtns[unIdx] = true;
					bEventProcessed = true;
				}
				else
				{
					m_bIsModified = false;
				}
				break;
			}
		}

		return bEventProcessed;
	}

	void SetIsModified(bool bIsModified = true)
	{
		m_bIsModified = bIsModified;
		for (uint8_t unIdx = 0; m_pConfig[unIdx].eMsg != MSG_NONE; unIdx++)
		{
			pConfirmedBtns[unIdx] = false;
		}
	};

	bool GetIsModified()
	{
		return m_bIsModified;
	}

private:
	bool m_bIsModified;
	BasePage* m_pPage;
	const PCONFIRM_DATA m_pConfig;
	bool* pConfirmedBtns;
};

#endif /* BASEPAGE_H_ */
