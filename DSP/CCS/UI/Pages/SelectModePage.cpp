/*
 * SelectMode.cpp
 *
 *  Created on: Oct 30, 2013
 *      Author: Sergei Tero
 */

#include "SelectModePage.h"
#include "../Controls/Label.h"
#include "../../Logic/DeviceState.h"
#include "Settings/PasswordPage.h"
#include "../PageManager.h"

SelectModePage::SelectModePage() : Menu()
{}

void OnManualClick(FNBTN enumBtn, Button* pBtn, void* pContext)
{
	((SelectModePage*)pContext)->OnManualClick();
}

void SelectModePage::OnManualClick()
{
	g_deviceState.SetOperationMode(STATEMODE_U);
	SyncPage();
}

void OnBtnCalibrationClicked(FNBTN enumBtn, Button* pButton, void* pContext)
{
	PasswordPage* pPageProtected = (PasswordPage*)g_pageManager.CreatePage(PAGE_CALIBRATION_MODE);
	pPageProtected->ClearPassword();
	g_pageManager.ShowPage(pPageProtected);
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void SelectModePage::OnInitPage()
{
	g_deviceState.SetOperationMode(STATEMODE_FULL_STANDBY);

	Menu::OnInitPage();

	Label* pCaptionLabel = CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET, MSG_MODE_SELECTION);
	pCaptionLabel->SetSize(MAX_DISPLAY_LENGTH, STYLE_PAGE_CAPTION_HEIGHT);
	pCaptionLabel->SetFont(FONT_CAPTION);
	pCaptionLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	Label* pDescrLabel = CreateLabel(0, STYLE_PAGE_CAPTION_OFFSET + STYLE_PAGE_CAPTION_HEIGHT, MSG_MODE_SELECTION_DESCR);
	pDescrLabel->SetSize(MAX_DISPLAY_LENGTH, 200);
	pDescrLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);

	RegisterFnButton(FNBTN_7, GetLangString(MSG_MODE_MANUAL),	PAGE_EMPTY,				::OnManualClick, this);
	RegisterFnButton(FNBTN_8, GetLangString(MSG_MODE_INTERF),	PAGE_REMOTE_CTRL		);
	RegisterFnButton(FNBTN_9, GetLangString(MSG_CONFIG),		PAGE_CONFIGURATION		);
	RegisterFnButton(FNBTN_10, GetLangString(MSG_MODE_CALIB),	PAGE_EMPTY,				::OnBtnCalibrationClicked, this);
	RegisterFnButton(FNBTN_11, GetLangString(MSG_MODE_TEST),	PAGE_STARTUP			);
}
