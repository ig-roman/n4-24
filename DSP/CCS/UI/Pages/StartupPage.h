/*
 * Startup.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef STARTUP_H_
#define STARTUP_H_

#include "Menu.h"

/** **********************************************************************
 *
 * Startup page, shown upon device start.
 *
 *********************************************************************** */
class StartupPage : public Menu
{
public:
	StartupPage();
	virtual ~StartupPage() {};

	virtual PAGE GetPageId() { return PAGE_STARTUP; };
	virtual void OnInitPage();
	virtual void OnButtonChanged(int8_t chButtonCode, bool bIsPressed);
	virtual void Loop();

protected:
	virtual void ShowFrame(Frame* pFrame, const char* pszBodyText);

private:
	uint16_t GetStepPos(INITSTEP eInitStep);
	void PrintInitStepCaption();
private:
	uint32_t m_BkgDelayTime;
	uint8_t nBkgStep;
	uint32_t m_DelayTime;
	INITSTEP m_eInitStep;
	bool m_bHasError;
};

#endif /* STARTUP_H_ */
