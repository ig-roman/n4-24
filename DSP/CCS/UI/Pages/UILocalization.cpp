/*
 * UILocalization.cpp
 *
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * !!! FILE ENCODING MUST BE CP1251 !!! 
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 *  Created on: Dec 27, 2013
 *      Author: Sergei Tero
 */

#include "UILocalization.h"
#include "../../Logic/DeviceState.h"

// Creating cont array with const data in flash
#define MSG_DEF( eVal, eng, rus)  eng,
static const char *const s_arrLangEng[] = { MSG_DEFS NULL };
#undef MSG_DEF

#define MSG_DEF( eVal, eng, rus)  rus,
static const char *const s_arrLangRus[] = { MSG_DEFS NULL };
#undef MSG_DEF

unsigned char g_langIdx;

void ReloadLangStrings()
{
	GetLangString(MSG_NONE);
	g_langIdx = g_deviceState.GetConfig().GetLang();
}

const char* GetLangString(uint16_t unMesgId)
{
	const char * const * arrLang = (STATELANG_EN == g_langIdx) ? s_arrLangEng : s_arrLangRus;
	return arrLang[unMesgId];
}

EMSG GetRS232ParityName(RS232PAR ePar)
{
	EMSG eRet = MSG_ERROR;
	switch (ePar)
	{
		case RS232PAR_EVEN: eRet = MSG_PAR_EVEN; break;
		case RS232PAR_NONE: eRet = MSG_PAR_NONE; break;
		case RS232PAR_ODD: eRet = MSG_PAR_ODD; break;
		case RS232PAR_ONE: eRet = MSG_PAR_ONE; break;
		case RS232PAR_ZERO: eRet = MSG_ZERO; break;
	}
	return eRet;
}
