/*
 * BaseModePage.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef BASEFMODEPAGE_H_
#define BASEFMODEPAGE_H_

#include "BaseModePage.h"

#define FNBTN_MUL		FNBTN_1
#define FNBTN_DIV		FNBTN_2

#define FNBTN_HZ		FNBTN_3
#define FNBTN_KHZ		FNBTN_4
#define FNBTN_MHZ		FNBTN_5
#define FNBTN_HZ_OFFSET	0

#define FNBTN_UV		FNBTN_3
#define FNBTN_MV		FNBTN_4
#define FNBTN_V			FNBTN_5
#define FNBTN_KV		FNBTN_6
#define FNBTN_V_OFFSET	2

#define FNBTN_DC		FNBTN_7
#define FNBTN_AC		FNBTN_8
#define FNBTN_S_AUTO	FNBTN_9
#define FNBTN_DELTA		FNBTN_11
#define FNBTN_DELTA_ABS	FNBTN_10

#define ABS_OFF_VALUE	0

bool OnBeforeByCharInputChange(EditBoxBand* pEdit, bool bSet, bool bParentFocusChage, const char* pszVal, void* pContext);

/** **********************************************************************
 *
 * Base implementation for frequency pages.
 *
 *********************************************************************** */
class WithFrequencyPage : public BaseModePage
{
public:
	WithFrequencyPage();
	virtual ~WithFrequencyPage() {};

	virtual void OnInitPage();
	virtual void OnButtonChanged(int8_t chButtonCode, bool bIsPressed);
	virtual void ShowButtonsSet(BUTTONSET enumSet);
	void RegisterZeroBtn();
	virtual bool OnBeforeIncrChange(EditBox* pEdit, string& strOldVal, string& strNewVal);
	virtual bool OnCursorLimit(bool bRightBound);
	virtual bool OnBeforeByCharInputChange(EditBoxBand* pEdit, bool bSet, bool bParentFocusChage, const char* pszVal);
	virtual BUTTONSET GetButtonSetByCtrlId(uint8_t unCtrlId);
	virtual void UpdateValueDependedCtrls(EditBoxBand* pEdit);
	virtual bool OnClick(FNBTN enumBtn, void* pContext);

	virtual void SyncState(bool bForce = false);
	virtual void SyncValue();
	void SyncDelta();
	void SyncFreq();
	void SyncACDC(bool bForce = false);

	bool GetIsDCOperationMode() { return !m_bACU; };

protected:
	virtual int8_t GetFnBandButtonsOffset(uint8_t unCtrlId);
	virtual double GetMultiplier(WORKUNIT& wu, uint16_t* punMesgId) = 0;

private:
	STATEERR StoreValue(uint8_t unCtrlId, double dVal, PCBAND pBand, bool bUseOptimalVBand);
	void SyncAbsDelta();
	void OnACDCBtnClicked(FNBTN enumBtn);
	void OnMulDivBtnClicked(FNBTN enumBtn);
	void OnZeroBtnClicked();
	void OnInvertSignClicked();
	void OnSyncAbsDelta();
	void OnSyncDelta();

protected:
	EditBoxBand* m_pEditValue;
	EditBoxBand* m_pEditFreq;

private:
	Label* m_pLabelPeak;
	Label* m_pLabelMean;
	EditBoxDigit* m_pEditPeak;
	EditBoxDigit* m_pEditMean;

	char szModeAc[3];
	char szModeDc[3];

	Label* m_pLabelDeltaValue;
	Label* m_pLabelDeltaPercent;
	EditBoxBand* m_pEditDeltaValue;
	EditBoxBand* m_pEditDeltaPercent;
	
	Label* m_pLabelDeltaPercentAbs;
	Label* m_pLabelDeltaValueAbs;
	EditBoxDigit* m_pEditDeltaPercentAbs;
	EditBoxDigit* m_pEditDeltaValueAbs;

	bool m_bACU;
	bool m_bWheelAction;
	double m_dAbsStartValue; // 0.0 means disabled. Use ABS_OFF_VALUE constant for it.
};

#endif /* BASEFMODEPAGE_H_ */
