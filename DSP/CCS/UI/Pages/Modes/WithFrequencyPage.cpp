/*
 * BaseModePage.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "WithFrequencyPage.h"
#include "../Logic/BandsManager.h"
#include <stdio.h>
#ifdef WIN32
#include "..\..\..\..\AARM\ArmInterface.h"
#endif // WIN32


WithFrequencyPage::WithFrequencyPage() : BaseModePage(),
	m_pEditValue(NULL),
	m_pEditFreq(NULL),
	m_pEditDeltaValue(NULL),
	m_pEditDeltaPercent(NULL),
	m_pEditDeltaValueAbs(NULL),
	m_pEditDeltaPercentAbs(NULL),
	m_pLabelPeak(NULL),
	m_pEditPeak(NULL),
	m_pLabelMean(NULL),
	m_pEditMean(NULL),
	m_bACU(false),
	m_bWheelAction(false),
	m_dAbsStartValue(ABS_OFF_VALUE)
{}

bool OnBMBeforeIncrChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext)
{
	WithFrequencyPage* pThis = (WithFrequencyPage*)pContext;
	return pThis->OnBeforeIncrChange(pEdit, strOldVal, strNewVal);
}

bool OnBMAfterValueChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext)
{
	EditBoxBand* pEditB = (EditBoxBand*)pEdit;
	WithFrequencyPage* pThis = (WithFrequencyPage*)pContext;

	pThis->UpdateValueDependedCtrls(pEditB);

	return true;
}

bool OnBeforeByCharInputChange(EditBoxBand* pEdit, bool bSet, bool bParentFocusChage, const char* pszVal, void* pContext)
{
	bool bRet = true;
	WithFrequencyPage* pThis = (WithFrequencyPage*)pContext;
	if (!bParentFocusChage)
	{
		bRet = pThis->OnBeforeByCharInputChange(pEdit, bSet, bParentFocusChage, pszVal);
	}

	return bRet;
}

void WithFrequencyPage::OnACDCBtnClicked(FNBTN enumBtn)
{
	bool bDC = FNBTN_DC == enumBtn;
	g_deviceState.SetDirecionType(bDC);

	if (m_wuBeforeZero.pBand)
	{
		OnZeroBtnClicked();
	}

	SyncState();
}

int8_t WithFrequencyPage::GetFnBandButtonsOffset(uint8_t unCtrlId)
{
	int8_t nRet = -1;

	switch (unCtrlId)
	{
		case ID_CTRL_FREQUENCY:		nRet = FNBTN_HZ_OFFSET;	break;
		case ID_CTRL_VALUE:			nRet = FNBTN_V_OFFSET;	break;
		case ID_CTRL_DELTA_VALUE:	nRet = FNBTN_V_OFFSET;	break;
	}
	return nRet;
}

STATEERR WithFrequencyPage::StoreValue(uint8_t unCtrlId, double dVal, PCBAND pBand, bool bUseOptimalVBand)
{
	STATEERR stRet = STATEERR_OUT_OF_RANGE;

	switch (unCtrlId)
	{
		case ID_CTRL_FREQUENCY:		stRet = g_deviceState.SetFrequency(dVal,  bUseOptimalVBand, pBand);	break;
		case ID_CTRL_VALUE:			stRet = g_deviceState.SetValue(dVal, -1, bUseOptimalVBand, pBand);	break;
		case ID_CTRL_DELTA_VALUE:
		case ID_CTRL_DELTA_PERCENT:
		case ID_CTRL_DELTA_PERCENT_ABS:
		{
			if (ID_CTRL_DELTA_VALUE == unCtrlId)
			{
				stRet = g_deviceState.SetDelta(true, &dVal, NULL);
			}
			else
			{
				stRet = g_deviceState.SetDelta(true, NULL, &dVal);
			}

			if (STATEERR_NO_ERROR == stRet)
			{
				SyncValue();
			}
			break;
		}
	}
	return stRet;
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void WithFrequencyPage::OnInitPage()
{
	sprintf(szModeAc, "~%s", GetWorkingUnits());
	sprintf(szModeDc, "=%s", GetWorkingUnits());

	BaseModePage::OnInitPage();

	m_pLabelPeak = CreateLabel(385, 10, MSG_PEAK_VALUE, 105);
	m_pLabelPeak->SetAlignment(AL_RIGHT_JUSTIFY_TEXT | AL_CENTER_TEXT_VERTICALLY);
	m_pEditPeak = EditBox::CreateInstance<EditBoxDigit>(this, ID_CTRL_PEAK, 490, 10, 205, DEFAULT_CONTROL_HEIGHT, AL_NONE, 9);
	m_pEditPeak->SetWithNavigation(false);
	RegisterControl(m_pEditPeak);

	m_pLabelMean = CreateLabel(385, 45, MSG_MEAN_VALUE, 105);
	m_pLabelMean->SetAlignment(AL_RIGHT_JUSTIFY_TEXT | AL_CENTER_TEXT_VERTICALLY);
	m_pEditMean = EditBox::CreateInstance<EditBoxDigit>(this, ID_CTRL_MEAN, 490, 45, 205, DEFAULT_CONTROL_HEIGHT, AL_NONE, 9);
	m_pEditMean->SetWithNavigation(false);
	RegisterControl(m_pEditMean);

	EditBoxDigit* pEditValSlave = new EditBoxDigit(this, ID_CTRL_SLAVE_VALUE, 0, 0);
	m_pEditValue = EditBox::CreateInstance<EditBoxBand>(this, ID_CTRL_VALUE, 0, 125, 695, 100, AL_RIGHT_JUSTIFY_TEXT, 11);
	m_pEditValue->SetUnitsText("", STYLE_UNITS_SIZE_BIG_CTRLS); // For slave
	m_pEditValue->SetSlaveCtrl(pEditValSlave);
	m_pEditValue->SetFont(FONT_BIG_DIGITS);
	m_pEditValue->SetOnBeforeByCharInputChange(::OnBeforeByCharInputChange, this);
	m_pEditValue->SetOnBeforeValueChange(::OnBMBeforeIncrChange, this);
	m_pEditValue->SetOnAfterValueChange(::OnBMAfterValueChange, this);
	m_pEditValue->SetOnCursorLimit(::OnBMCursorLimit, this);
	m_pEditValue->SetOnBeforeActivate(::OnBeforeEditActivate, this);
	// RegisterControl() in SetActiveControl() method below the function.

	EditBoxDigit* pEditFreqSlave = new EditBoxDigit(this, ID_CTRL_SLAVE_FREQUENCY, 0, 0);
	m_pEditFreq = EditBox::CreateInstance<EditBoxBand>(this, ID_CTRL_FREQUENCY, 0, 225, 695, 100, AL_RIGHT_JUSTIFY_TEXT, 11);
	m_pEditFreq->SetUnitsText("", STYLE_UNITS_SIZE_BIG_CTRLS); // For slave
	m_pEditFreq->SetSlaveCtrl(pEditFreqSlave);
	m_pEditFreq->SetFont(FONT_BIG_DIGITS);
	m_pEditFreq->SetOnBeforeByCharInputChange(::OnBeforeByCharInputChange, this);
	m_pEditFreq->SetOnBeforeValueChange(::OnBMBeforeIncrChange, this);
	m_pEditFreq->SetOnCursorLimit(::OnBMCursorLimit, this);
	m_pEditFreq->SetOnBeforeActivate(::OnBeforeEditActivate, this);
	RegisterControl(m_pEditFreq);

	m_pLabelDeltaPercent = CreateLabel(50, 330, NULL, 80);
	m_pLabelDeltaPercent->SetValue("c =");
	m_pLabelDeltaPercent->SetFont(TAGRA_MS_20);
	m_pLabelDeltaPercent->SetAlignment(AL_RIGHT_JUSTIFY_TEXT | AL_CENTER_TEXT_VERTICALLY);
	m_pLabelDeltaPercent->SetVisible(false);
	EditBoxDigit* pEditDeltaPerSlave = new EditBoxDigit(this, ID_CTRL_SLAVE_DELTA_PERCENT, 0, 0);
	m_pEditDeltaPercent = EditBox::CreateInstance<EditBoxBand>(this, ID_CTRL_DELTA_PERCENT, 130, 330, 195, 40, AL_NONE, 9);
	m_pEditDeltaPercent->SetUnitsText("%", STYLE_UNITS_SIZE_SMALL_CTRLS);
	m_pEditDeltaPercent->SetSlaveCtrl(pEditDeltaPerSlave);
	m_pEditDeltaPercent->SetDValue(0, 7, 4);
	m_pEditDeltaPercent->SetVisible(false);
	m_pEditDeltaPercent->SetOnBeforeValueChange(::OnBMBeforeIncrChange, this);
	m_pEditDeltaPercent->SetOnBeforeByCharInputChange(::OnBeforeByCharInputChange, this);
	m_pEditDeltaPercent->SetOnBeforeActivate(::OnBeforeEditActivate, this);
	m_pEditDeltaPercent->SetWithNavigation(true);
	RegisterControl(m_pEditDeltaPercent);

	m_pLabelDeltaValue = CreateLabel(350, 330, NULL, 80);
	m_pLabelDeltaValue->SetValue("C =");
	m_pLabelDeltaValue->SetFont(TAGRA_MS_20);
	m_pLabelDeltaValue->SetAlignment(AL_RIGHT_JUSTIFY_TEXT | AL_CENTER_TEXT_VERTICALLY);
	m_pLabelDeltaValue->SetVisible(false);
	EditBoxDigit* pEditDeltaValSlave = new EditBoxDigit(this, ID_CTRL_SLAVE_DELTA_VALUE, 0, 0);
	m_pEditDeltaValue = EditBox::CreateInstance<EditBoxBand>(this, ID_CTRL_DELTA_VALUE, 430, 330, 215, 40, AL_NONE, 10);
	m_pEditDeltaValue->SetUnitsText(GetWorkingUnits(), STYLE_UNITS_SIZE_SMALL_CTRLS);
	m_pEditDeltaValue->SetSlaveCtrl(pEditDeltaValSlave);
	m_pEditDeltaValue->SetVisible(false);
	m_pEditDeltaValue->SetDValue(0, 7, 5);
	m_pEditDeltaValue->SetOnBeforeValueChange(::OnBMBeforeIncrChange, this);
	m_pEditDeltaValue->SetOnBeforeByCharInputChange(::OnBeforeByCharInputChange, this);
	m_pEditDeltaValue->SetOnBeforeActivate(::OnBeforeEditActivate, this);
	RegisterControl(m_pEditDeltaValue);

	m_pLabelDeltaPercentAbs = CreateLabel(50, 370, NULL, 80);
	m_pLabelDeltaPercentAbs->SetValue("� =");
	m_pLabelDeltaPercentAbs->SetFont(TAGRA_MS_20);
	m_pLabelDeltaPercentAbs->SetAlignment(AL_RIGHT_JUSTIFY_TEXT | AL_CENTER_TEXT_VERTICALLY);
	m_pLabelDeltaPercentAbs->SetVisible(false);
	m_pEditDeltaPercentAbs = EditBox::CreateInstance<EditBoxDigit>(this, ID_CTRL_DELTA_PERCENT, 130, 370, 195, 40, AL_NONE, 9);
	m_pEditDeltaPercentAbs->SetUnitsText("%", STYLE_UNITS_SIZE_SMALL_CTRLS);
	m_pEditDeltaPercentAbs->SetDValue(0, 7, 4);
	m_pEditDeltaPercentAbs->SetVisible(false);
	m_pEditDeltaPercentAbs->SetWithNavigation(false);
	RegisterControl(m_pEditDeltaPercentAbs);

	m_pLabelDeltaValueAbs = CreateLabel(350, 370, NULL, 80);
	m_pLabelDeltaValueAbs->SetValue("� =");
	m_pLabelDeltaValueAbs->SetFont(TAGRA_MS_20);
	m_pLabelDeltaValueAbs->SetAlignment(AL_RIGHT_JUSTIFY_TEXT | AL_CENTER_TEXT_VERTICALLY);
	m_pLabelDeltaValueAbs->SetVisible(false);
	m_pEditDeltaValueAbs = EditBox::CreateInstance<EditBoxDigit>(this, ID_CTRL_DELTA_PERCENT, 430, 370, 215, 40, AL_NONE, 10);
	m_pEditDeltaValueAbs->SetUnitsText(GetWorkingUnits(), STYLE_UNITS_SIZE_SMALL_CTRLS);
	m_pEditDeltaValueAbs->SetDValue(0, 7, 5);
	m_pEditDeltaValueAbs->SetWithNavigation(false);
	m_pEditDeltaValueAbs->SetVisible(false);
	RegisterControl(m_pEditDeltaValueAbs);

	SyncState(true);

	SetActiveControl(RegisterControl(m_pEditValue));

	// This are popup edit boxes and should be displayed over pahe controls. The last registered control has the most z index
	RegisterControl(pEditValSlave);
	RegisterControl(pEditFreqSlave);
	RegisterControl(pEditDeltaPerSlave);
	RegisterControl(pEditDeltaValSlave);
}

BUTTONSET WithFrequencyPage::GetButtonSetByCtrlId(uint8_t unCtrlId)
{
	BUTTONSET enumSet;
	switch (unCtrlId)
	{
		case ID_CTRL_VALUE:					enumSet = BUTTONSET_U_DEFAULT;		break;
		case ID_CTRL_SLAVE_VALUE:			enumSet = BUTTONSET_U_BAND;			break;

		case ID_CTRL_FREQUENCY:				enumSet = BUTTONSET_FREQ_DEFAULT;	break;
		case ID_CTRL_SLAVE_FREQUENCY:		enumSet = BUTTONSET_FREQ_BAND;		break;

		case ID_CTRL_SLAVE_DELTA_VALUE:		enumSet = BUTTONSET_U_BAND;			break;

		default: enumSet = BUTTONSET_NONE;
	}
	return enumSet;
}

void WithFrequencyPage::UpdateValueDependedCtrls(EditBoxBand* pEdit)
{
	if (m_pEditPeak->GetVisible())
	{
		double dVaverage = pEdit->GetDValue() * VAL_1_DEV_PI2SQRT2; //  = 1/(PI/(2*sqrt(2)))
		double dVpeak = pEdit->GetDValue() * VAL_SQRT2;				// = sqrt(2)
	
		uint16_t unMesgId;
		PWORKUNIT pWu = pEdit->GetUnit();
		double dMultiplier = GetMultiplier(*pWu, &unMesgId);
		m_pEditValue->SetUnitsText(GetLangString(unMesgId), STYLE_UNITS_SIZE_BIG_CTRLS);

		m_pEditPeak->SetDValue(dVpeak, pEdit->GetScale(), pEdit->GetFraction(), pEdit->GetMultiplier());
		m_pEditPeak->SetUnitsText(GetLangString(unMesgId), STYLE_UNITS_SIZE_SMALL_CTRLS);

		m_pEditMean->SetDValue(dVaverage, pEdit->GetScale(), pEdit->GetFraction(), pEdit->GetMultiplier());
		m_pEditMean->SetUnitsText(GetLangString(unMesgId), STYLE_UNITS_SIZE_SMALL_CTRLS);
	}

	// After value change Output may be disabled if voltage higher than 100
	SyncOut();
	SyncAbsDelta();
}

void WithFrequencyPage::OnMulDivBtnClicked(FNBTN enumBtn)
{
	EditBoxBand* pEdit = (EditBoxBand*)GetActiveControl();
	double dVal = pEdit->GetDValue();

	dVal *= (FNBTN_MUL == enumBtn) ? 10 : 0.1;
	
	bool bFreqCtrl = pEdit == m_pEditFreq;
	STATEERR ret = bFreqCtrl
		? g_deviceState.SetFrequency(dVal, true)
		: g_deviceState.SetValue(dVal, m_pEditFreq->GetDValue(), true);

	if (STATEERR_NO_ERROR == ret)
	{
		if (bFreqCtrl)
		{
			SyncFreq();
		}
		else
		{
			SyncValue();
			g_deviceState.ResetDelta(false);
			SyncDelta();
		}
	}
	else
	{
		ReportMessage(GetLangString(MSG_OUT_OF_RANGE));
	}
}

void WithFrequencyPage::SyncACDC(bool bForce)
{
	bool bDC = false;
	g_deviceState.GetDirecionType(bDC);

	if (m_bACU == bDC || bForce) // Changed
	{
		m_bACU = !bDC;
		m_pUTypeLabel->SetValue(m_bACU ? szModeAc : szModeDc);
		m_pEditFreq->SetVisible(m_bACU);

		ShowButtonsSet(BUTTONSET_DEFAULT);

		Button* pDcBtn = GetFnButton(FNBTN_DC);
		Button* pAcBtn = GetFnButton(FNBTN_AC);

		pDcBtn->SetToggled(!m_bACU);
		pAcBtn->SetToggled(m_bACU);

		m_pEditMean->SetVisible(!bDC);
		m_pEditPeak->SetVisible(!bDC);

		m_pLabelPeak->SetVisible(!bDC);
		m_pLabelMean->SetVisible(!bDC);

		SetActiveControl(-1); // Force activating to call activating callback.
		SetActiveControlByCtrlId(m_pEditValue->GetId());

		m_dAbsStartValue = ABS_OFF_VALUE;
		// Don't sync abs controls because they will sync later with new value see: UpdateValueDependedCtrls()
	}
}

void WithFrequencyPage::OnInvertSignClicked()
{
	if (!m_bACU)
	{
		PWORKUNIT pUnit = m_pEditValue->GetUnit();
		pUnit->value *= -1;
		pUnit->pBand = BandsManager::InvertBand(g_deviceState.GetOperationMode(), pUnit->pBand);
		StoreValue(m_pEditValue->GetId(), pUnit->value, pUnit->pBand, false);
		SyncValue();
	}
}

void WithFrequencyPage::SyncValue()
{
	GetFnButton(FNBTN_ZERO)->SetToggled(m_wuBeforeZero.pBand != 0);

	WORKUNIT voltage = {0};
	g_deviceState.GetValue(voltage);

	uint16_t unMesgId;
	double dMultiplier = GetMultiplier(voltage, &unMesgId);
	m_pEditValue->SetUnit(voltage, dMultiplier);
	m_pEditValue->SetUnitsText(GetLangString(unMesgId), STYLE_UNITS_SIZE_BIG_CTRLS);

	UpdateValueDependedCtrls(m_pEditValue);
}

void WithFrequencyPage::SyncAbsDelta()
{
	bool bDeltaEnabled = ABS_OFF_VALUE != m_dAbsStartValue;

	Button* pBtn = GetFnButton(FNBTN_DELTA_ABS);
	bool bChangedView = pBtn->GetToggled() != bDeltaEnabled;

	if (bChangedView)
	{
		m_pEditDeltaPercentAbs->SetVisible(bDeltaEnabled);
		m_pLabelDeltaPercentAbs->SetVisible(bDeltaEnabled);

		m_pEditDeltaValueAbs->SetVisible(bDeltaEnabled);
		m_pLabelDeltaValueAbs->SetVisible(bDeltaEnabled);

		pBtn->SetToggled(bDeltaEnabled);
	}

	if (bDeltaEnabled)
	{
		// Relative error
		double dDispValue = m_pEditValue->GetDValue();
		double dDeltaInPrecent = 0 == dDispValue ? 0 : (1 - dDispValue / m_dAbsStartValue) * 100;
		m_pEditDeltaPercentAbs->SetDValue(dDeltaInPrecent, m_pEditValue->GetScale() - 1, m_pEditValue->GetScale() - 3);

		// Absolute error
		double dAbsDelta = m_dAbsStartValue - dDispValue;
		m_pEditDeltaValueAbs->SetUnitsText(m_pEditValue->GetUnitsText(), STYLE_UNITS_SIZE_SMALL_CTRLS);
		m_pEditDeltaValueAbs->SetDValue(dAbsDelta, m_pEditValue->GetScale(), m_pEditValue->GetFraction(), m_pEditValue->GetMultiplier());
	}
}

void WithFrequencyPage::SyncDelta()
{
	bool bDeltaEnabled = false;

	PWORKUNIT delataPercent;
	WORKUNIT delataValue;
	g_deviceState.GetDelta(&bDeltaEnabled, &delataValue.pBand, &delataValue.value, &delataPercent);

	Button* pBtn = GetFnButton(FNBTN_DELTA);
	bool bChanged = pBtn->GetToggled() != bDeltaEnabled;
	bool bDC = true;
	g_deviceState.GetDirecionType(bDC);
	pBtn->SetValue(bDC ? GetLangString(MSG_DELTA_DC) : GetLangString(MSG_DELTA_AC));

	if (bChanged)
	{
		pBtn->SetToggled(bDeltaEnabled);

		m_pEditDeltaPercent->SetVisible(bDeltaEnabled);
		m_pLabelDeltaPercent->SetVisible(bDeltaEnabled);

		m_pEditDeltaValue->SetVisible(!m_bACU && bDeltaEnabled);
		m_pLabelDeltaValue->SetVisible(!m_bACU && bDeltaEnabled);
	}

	if (bDeltaEnabled)
	{
		m_pEditDeltaPercent->SetUnit(*delataPercent, 1);

		if (!m_bACU)
		{
			uint16_t unMesgId;
			double dMultiplier = GetMultiplier(delataValue, &unMesgId);
			m_pEditDeltaValue->SetUnit(delataValue, dMultiplier);
			m_pEditDeltaValue->SetUnitsText(GetLangString(unMesgId), STYLE_UNITS_SIZE_SMALL_CTRLS);
		}

		if (bChanged)
		{
			SaveSelection();
			SetActiveControlByCtrlId(ID_CTRL_DELTA_PERCENT);
		}
	}
	else
	{
		if (m_pEditDeltaPercent->GetActive() || m_pEditDeltaValue->GetActive())
		{
			RestoreSelection();
		}

		// Show value witout delta. ut of range becuse delta was applied and now removed. It should be updated.
		if (bChanged)
		{
			SyncValue();
		}
	}
}

/** **********************************************************************
 *
 * Shows functional buttons on this page
 * @param enumSet set of buttons to show
 *
 *********************************************************************** */
void WithFrequencyPage::ShowButtonsSet(BUTTONSET enumSet)
{
	switch (enumSet)
	{
		case BUTTONSET_DEFAULT:
		{
			RegisterFnButton(FNBTN_DC,			szModeDc,						this);
			RegisterFnButton(FNBTN_AC,			szModeAc,						this);
			
			if (m_bACU)
			{
				RegisterFnButton(FNBTN_ZERO,		NULL,						PAGE_EMPTY);
				RegisterFnButton(FNBTN_DELTA,		GetLangString(MSG_DELTA_AC),	this);
			}
			else
			{
				RegisterFnButton(FNBTN_ZERO,		GetLangString(MSG_ZERO),	this);
				RegisterFnButton(FNBTN_DELTA,		GetLangString(MSG_DELTA_DC),	this);
			}

			RegisterFnButton(FNBTN_DELTA_ABS,	GetLangString(MSG_DELTA_ABS), this);

			break;
		}
		case BUTTONSET_FREQ_DEFAULT:
		{
			RegisterFnButton(FNBTN_MUL,			GetLangString(MSG_MUL10),		this);
			RegisterFnButton(FNBTN_DIV,			GetLangString(MSG_DEV10),		this);
			break;
		}
		case BUTTONSET_FREQ_BAND:
		{
			RegisterFnButton(FNBTN_1,			NULL,							PAGE_EMPTY);
			RegisterFnButton(FNBTN_2,			NULL,							PAGE_EMPTY);
			RegisterFnButton(FNBTN_HZ,			GetLangString(MSG_HZ),			this, this);
			RegisterFnButton(FNBTN_KHZ,			GetLangString(MSG_KHZ),			this, this);
			RegisterFnButton(FNBTN_MHZ,			GetLangString(MSG_MHZ),			this, this);
			RegisterFnButton(FNBTN_6,			NULL,							PAGE_EMPTY);
			break;
		}
	}
	BaseModePage::ShowButtonsSet(enumSet);
}

void WithFrequencyPage::RegisterZeroBtn()
{
	RegisterFnButton(FNBTN_ZERO, GetLangString(MSG_ZERO), this);
}

void WithFrequencyPage::OnSyncDelta()
{
	bool bIsDeltaEnabled = false;
	if (STATEERR_NO_ERROR == g_deviceState.GetDelta(&bIsDeltaEnabled, NULL, NULL, NULL))
	{
		g_deviceState.SetDelta(!bIsDeltaEnabled, 0, 0);
	}
	SyncDelta();
}

void WithFrequencyPage::OnSyncAbsDelta()
{
	if (ABS_OFF_VALUE == m_dAbsStartValue)
	{
		m_dAbsStartValue = m_pEditValue->GetDValue();
	}
	else
	{
		m_dAbsStartValue = ABS_OFF_VALUE;
	}

	SyncAbsDelta();
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optioanl data defined on registration phase
 *
 * @return return true if event is hadled otherwise false
 *
 *********************************************************************** */
bool WithFrequencyPage::OnClick(FNBTN enumBtn, void* pContext)
{
	bool bHandled = BaseModePage::OnClick(enumBtn, pContext);
	if (!bHandled)
	{
		bHandled = true;
		switch (enumBtn)
		{
			case FNBTN_DELTA_ABS:	OnSyncAbsDelta();				break;
			case FNBTN_DELTA:		OnSyncDelta();					break;
			case FNBTN_INVERT_SIGN:	OnInvertSignClicked();			break;
			case FNBTN_MUL:
			case FNBTN_DIV:			OnMulDivBtnClicked(enumBtn);	break;
			case FNBTN_ZERO:		OnZeroBtnClicked();				break;
			case FNBTN_DC:
			case FNBTN_AC:			OnACDCBtnClicked(enumBtn);		break;
			default:				bHandled = false;				break;
		}
	}
	return bHandled;
}

void WithFrequencyPage::SyncFreq()
{
	if (m_bACU)
	{
		WORKUNIT frequency = {0};
		g_deviceState.GetFrequency(frequency);
		uint16_t unMesgIdUnit = 0;
		m_pEditFreq->SetUnit(frequency, ::GetMultiplier(frequency.pBand, &unMesgIdUnit));
		m_pEditFreq->SetUnitsText(GetLangString(unMesgIdUnit), STYLE_UNITS_SIZE_BIG_CTRLS);
	}
}
void WithFrequencyPage::SyncState(bool bForce)
{
	BaseModePage::SyncState(bForce);

	STATEMODE mode = g_deviceState.GetOperationMode();
	if (STATEMODE_U == mode || STATEMODE_I == mode)
	{
		// Value controll is used m_bZeroMode member it should be updated before
		SyncACDC(bForce);
		SyncFreq();
		SyncValue();
		SyncDelta();
	}
}

bool WithFrequencyPage::OnCursorLimit(bool bRightBound)
{
	UIControl* pCtrl = GetActiveControl();
	if (pCtrl == m_pEditFreq || pCtrl == m_pEditValue)
	{
		bool bIsFreqCtrl = m_pEditFreq == GetActiveControl();

		if (STATEERR_NO_ERROR == (bIsFreqCtrl ? g_deviceState.ChangeFrequencyBand(!bRightBound) : g_deviceState.ChangeValueBand(!bRightBound)))
		{
			if (bIsFreqCtrl)
			{
				SyncFreq();
			}
			else
			{
				SyncValue();
				SyncDelta();
			}
		}
		else
		{
			ReportMessage(GetLangString(MSG_OUT_OF_RANGE));
		}

	// ST TODO: Remove bool?
	}
	return true;
}

/** **********************************************************************
 *
 * Virtual function to handle or lock any events from physical keyboard
 *
 * @param chButtonCode	key code of pressed, repeated or released button.
 * @param bIsPressed	true if button is pressed or repeat event otherwise it was released
 *
 *********************************************************************** */
void WithFrequencyPage::OnButtonChanged(int8_t chButtonCode, bool bIsPressed)
{
	// Don't move BaseModePage::OnButtonChanged() bacause it may call OnBeforeIncrChange()
	m_bWheelAction = (BTN_WHEEL_PRESSED == chButtonCode || BTN_UP_WHEEL == chButtonCode || BTN_DOWN_WHEEL == chButtonCode) && bIsPressed;

	BaseModePage::OnButtonChanged(chButtonCode, bIsPressed);
}

uint16_t StateErrToMsgId(STATEERR val)
{
	switch (val)
	{
		case STATEERR_OUT_OF_RANGE:			return MSG_OUT_OF_RANGE;
		case STATEERR_GO_BAND_UP:			return MSG_GO_RANGE_UP;
		case STATEERR_GO_BAND_DOWN:			return MSG_GO_RANGE_DOWN;
		case STATEERR_ABSOLUTE_UPPER_LIMIT:	return MSG_RANGE_UPPER_LIMIT;
		case STATEERR_ABSOLUTE_LOWER_LIMIT:	return MSG_RANGE_LOWER_LIMIT;
	}
	return MSG_ERROR;
}

bool WithFrequencyPage::OnBeforeIncrChange(EditBox* pEdit, string& strOldVal, string& strNewVal)
{
	bool bRet = true;
	EditBoxBand* pEditB = (EditBoxBand*)pEdit;

	double dVal = EditBoxDigit::ParseDValue(strNewVal.c_str()) / pEditB->GetMultiplier();

	STATEERR ret = StoreValue(pEdit->GetId(), dVal, pEditB->GetUnit()->pBand, false);
	if (STATEERR_NO_ERROR != ret && m_bWheelAction)
	{
		// Set value in another band if action performed by wheel
		ret = StoreValue(pEdit->GetId(), dVal, NULL, true);
		if (STATEERR_NO_ERROR == ret)
		{
			SyncFreq();
			SyncValue();
			SyncDelta();
			strNewVal = pEdit->GetValue();
		}
	}

	if (STATEERR_NO_ERROR != ret)
	{
		ReportMessage(GetLangString(StateErrToMsgId(ret)));
		bRet = false;
	}

	return bRet;
}

bool WithFrequencyPage::OnBeforeByCharInputChange(EditBoxBand* pEdit, bool bSet, bool bParentFocusChage, const char* pszVal)
{
	bool bRet = true;

	if (bSet)
	{
		if ((pszVal && pszVal[0]) && pEdit == m_pEditValue)
		{
			// N.B. COPY VALUE TO SLAVE - look for this comment for linked use cases 
			// Sets to slave control value with all defined offsets (device output value)
			WORKUNIT wu;
			g_deviceState.GetOutputValue(wu);
			pEdit->GetSlaveCtrl().SetDValue(wu.value,  pEdit->GetScale(),  pEdit->GetFraction(), pEdit->GetMultiplier());
		}
		m_bMultiplyerSelectedByFnButton = false;
	}
	else
	{
		// Set value
		double dVal = EditBoxDigit::ParseDValue(pszVal);

		if (!m_bMultiplyerSelectedByFnButton)
		{
			// Use already selected band if enter button is presed
			dVal /= pEdit->GetMultiplier();
		}

		// Always set value in more precise band if "by char" action is performed
		bRet = STATEERR_NO_ERROR == StoreValue(pEdit->GetId(), dVal, NULL, true);
		if (bRet)
		{
			SyncFreq();
			SyncValue();
			SyncDelta();

			// Restore selection if delta has been changed.
			if (ID_CTRL_DELTA_PERCENT == pEdit->GetId())
			{
				RestoreSelection();
			}
			else
			{
				ResetSelection();
			}
		}
		else
		{
			ReportMessage(GetLangString(MSG_OUT_OF_RANGE));
		}
	}

	return bRet;
}

void WithFrequencyPage::OnZeroBtnClicked()
{
	if (m_wuBeforeZero.pBand)
	{
		StoreValue(m_pEditValue->GetId(), m_wuBeforeZero.value, m_wuBeforeZero.pBand, false);
		m_wuBeforeZero.pBand = NULL;
		m_wuBeforeZero.value = 0;

		m_pEditValue->SetWithNavigation(true);
		RestoreSelection();
	}
	else
	{
		WORKUNIT wuValue = {0};
		g_deviceState.GetValue(wuValue);
		STATEERR stRet = StoreValue(m_pEditValue->GetId(), 0, wuValue.pBand, false);

		if (STATEERR_NO_ERROR != stRet)
		{
			ReportMessage(GetLangString(MSG_OUT_OF_RANGE));
		}
		else
		{
			m_wuBeforeZero = wuValue;
			SaveSelection();

			if (m_pEditValue->GetActive())
			{
				SetActiveControl(-1);
			}

			m_pEditValue->SetWithNavigation(false);
			m_pEditValue->SetActive(false);

			// Absolute delta cannot be calculated for 0 value
			m_dAbsStartValue = ABS_OFF_VALUE;
			SyncAbsDelta();
		}
	}

	SyncValue();
}
