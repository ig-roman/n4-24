/*
 * UMode.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef UMODE_H_
#define UMODE_H_

#include "WithFrequencyPage.h"

class UMode : public WithFrequencyPage
{
public:
	UMode();
	virtual ~UMode() {};

	virtual PAGE GetPageId() { return PAGE_MODE_U; };
	virtual void OnInitPage();
	virtual void Loop();
	virtual void ShowButtonsSet(BUTTONSET enumSet);
	virtual void UpdateValueDependedCtrls(EditBoxBand* pEdit);
	virtual void SyncState(bool bForce = false);
	virtual void SyncOut();
	void SyncDivider();
	void OnDividerBtnClicked(FNBTN enumBtn, Button* pButton);

protected:
	virtual const char* GetWorkingUnits() { return GetLangString(MSG_U); };
	virtual double GetMultiplier(WORKUNIT& wu, uint16_t* punMesgId);

private:
	Label* m_pOutHVLabel;
	BUTTONSET m_curButtonset;
	bool m_bHV;
	uint32_t m_BlinkTime;
	bool m_bHVConfirmShown;
	bool m_bOldOutOn;
	bool m_extDivider;
};

#endif /* UMODE_H_ */
