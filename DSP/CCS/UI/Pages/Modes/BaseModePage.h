/*
 * BaseModePage.h
 *
 *  Created on: Dec 3, 2013
 *      Author: Sergei Tero
 */

#ifndef BASEMODEPAGE_H_
#define BASEMODEPAGE_H_

#include "../Menu.h"
#include "../../Controls/EditBoxBand.h"
#include "../../../Logic/DeviceState.h"
#include "../../Controls/Label.h"

#define ID_CTRL_VALUE				100
#define ID_CTRL_RESISTANCE			100 // R page only
#define ID_CTRL_FREQUENCY			101
#define ID_CTRL_OFFSET_DELTA		102
#define ID_CTRL_OFFSET_VAL			103
#define ID_CTRL_PEAK				104
#define ID_CTRL_MEAN				105
#define ID_CTRL_SENSE				106
#define ID_CTRL_DATE				107
#define ID_CTRL_TIME				108
#define ID_CTRL_DELTA_PERCENT		109
#define ID_CTRL_DELTA_VALUE			110

#define ID_CTRL_SLAVE_DELTA_PERCENT	111
#define ID_CTRL_SLAVE_DELTA_VALUE	112
#define ID_CTRL_SLAVE_VALUE			113
#define ID_CTRL_SLAVE_RESISTANCE	113 // R page only
#define ID_CTRL_SLAVE_FREQUENCY		114
#define ID_CTRL_DELTA_PERCENT_ABS	115
#define ID_CTRL_SLAVE_DELTA_PERCENT_ABS	116

#define ID_CTRL_INFO_TEXT			120

#define FNBTN_ULXX 0
#define FNBTN_MLXX 1
#define FNBTN_XXXX 2
#define FNBTN_KHXX 3
#define FNBTN_MHXX 4

#define FNBTN_ZERO		FNBTN_5

enum BUTTONSET { BUTTONSET_NONE, BUTTONSET_DEFAULT, BUTTONSET_U_DEFAULT, BUTTONSET_U_BAND, BUTTONSET_FREQ_DEFAULT, BUTTONSET_FREQ_BAND, BUTTONSET_R_BAND };
bool OnBeforeEditActivate(WithNavigation* pEdit, bool bBecomeActive, void* pContext);
bool OnBMCursorLimit(EditBox* pEdit, uint16_t unEditLen, uint16_t unCursorPos, bool bRightBound, void* pContext);

/** **********************************************************************
 *
 * Base implementation for device mode pages.
 *
 *********************************************************************** */
class BaseModePage : public Menu, public IOnClickCompatible
{
public:
	BaseModePage();
	virtual ~BaseModePage() {};

	virtual void OnInitPage();
	virtual void OnButtonChanged(int8_t chButtonCode, bool bIsPressed);
	virtual void ShowButtonsSet(BUTTONSET enumSet);
	virtual void OnBandBtnPressed(FNBTN enumBtn);
	virtual bool OnBeforeEditActivate(WithNavigation* pEdit, bool bBecomeActive);
	virtual BUTTONSET GetButtonSetByCtrlId(uint8_t unCtrlId) = 0;
	virtual bool OnCursorLimit(bool bRightBound) = 0;
	virtual void SyncState(bool bForce = false);
	virtual void SyncOut();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
	virtual void SyncSense();
	virtual void OnSenseBtnClicked(FNBTN enumBtn, Button* pButton);

protected:
	virtual int8_t GetFnBandButtonsOffset(uint8_t unCtrlId) = 0;
	virtual const char* GetWorkingUnits() = 0;
	void ReportMessage(const char* pszText, bool bPermanent = false);
	virtual void OverloadChange(bool bOverload);
	virtual void Loop();
	virtual const char* GetOverloadText() const { return GetLangString(MSG_OVERLOAD); };

protected:
	WORKUNIT m_wuBeforeZero;

	Label* m_pUTypeLabel;
	Label* m_pOutOnLabel;
	Label* m_pOutputLabel;
	Label* m_pLabelSense;

	Label* m_pInfoLabel;
	const char* m_pszPermanentMesgText;
	bool m_bMultiplyerSelectedByFnButton;

private:
	uint32_t m_unMsgTimeout;
};

#endif /* BASEMODEPAGE_H_ */
