/*
 * IMode.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef IMODE_H_
#define IMODE_H_

#include "WithFrequencyPage.h"

class IMode : public WithFrequencyPage
{
public:
	IMode() : WithFrequencyPage(), m_bIsAmpEnabled(false) {};
	virtual ~IMode() {};
	virtual PAGE GetPageId() { return PAGE_MODE_I; };
	virtual void ShowButtonsSet(BUTTONSET enumSet);

protected:
	virtual const char* GetWorkingUnits() { return GetLangString(MSG_I); };
	virtual double GetMultiplier(WORKUNIT& wu, uint16_t* punMesgId);
	virtual const char* GetOverloadText() const { return GetLangString(MSG_CONNECT_LOAD); };
	void UpdateValueDependedCtrls(EditBoxBand* pEdit);

private:
	bool m_bIsAmpEnabled;

};

#endif /* IMODE_H_ */
