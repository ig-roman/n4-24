/*
 * BaseModePage.cpp
 *
 *  Created on: Dec 3, 2013
 *      Author: Sergei Tero
 */

#include "BaseModePage.h"
#include "../../../Logic/Calibration/CalibrationManager.h"

#define TIMEOUT_MSG 5000

BaseModePage::BaseModePage() : Menu(),
	m_pInfoLabel(NULL),
	m_pUTypeLabel(NULL),
	m_pszPermanentMesgText(NULL),
	m_pOutOnLabel(NULL),
	m_pOutputLabel(NULL),
	m_bMultiplyerSelectedByFnButton(false),
	m_unMsgTimeout(0),
	m_pLabelSense(NULL)
{
	m_wuBeforeZero.pBand = NULL;
	m_wuBeforeZero.value = 0;
}

bool OnBeforeEditActivate(WithNavigation* pEdit, bool bBecomeActive, void* pContext)
{
	BaseModePage* pThis = (BaseModePage*)pContext;
	return pThis->OnBeforeEditActivate(pEdit, bBecomeActive);
}

bool OnBMCursorLimit(EditBox* pEdit, uint16_t unEditLen, uint16_t unCursorPos, bool bRightBound, void* pContext)
{
	BaseModePage* pThis = (BaseModePage*)pContext;
	return pThis->OnCursorLimit(bRightBound);
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void BaseModePage::OnInitPage()
{
	Menu::OnInitPage();

	m_pUTypeLabel = CreateLabel(0, 0, NULL, 95, 100);
	m_pUTypeLabel->SetFont(FONT_BIG_DIGITS);
	m_pUTypeLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY);

	m_pOutputLabel = CreateLabel(95, 10, MSG_OUTPUT, 115, 35);
	m_pOutputLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY);
	m_pOutOnLabel = CreateLabel(95, 45, MSG_OFF, 75, 39);
	m_pOutOnLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY);

	m_pInfoLabel = new Label(this, ID_CTRL_INFO_TEXT, 95, 85);
	m_pInfoLabel->SetSize(600, DEFAULT_CONTROL_HEIGHT);
	m_pInfoLabel->SetColor(COLOR_ERROR);

	// Triggers logic wich shows "Calibration data DISABLED"
	ReportMessage(NULL, false);

	RegisterControl(m_pInfoLabel);
}

/** **********************************************************************
 *
 * Virtual function to handle or lock any events from physical keyboard
 *
 * @param chButtonCode	key code of pressed, repeated or released button.
 * @param bIsPressed	true if button is pressed or repeat event otherwise it was released
 *
 *********************************************************************** */
void BaseModePage::OnButtonChanged(int8_t chButtonCode, bool bIsPressed)
{
	// Hides info label
	if (bIsPressed)
	{
		if (BTN_BAND_UP == chButtonCode || BTN_BAND_DOWN == chButtonCode)
		{
			OnCursorLimit(BTN_BAND_DOWN == chButtonCode);
		}
		else if (BTN_OUT_ON == chButtonCode || BTN_OUT_OFF == chButtonCode)
		{
			g_deviceState.SwitchOut(BTN_OUT_ON == chButtonCode, false);
			SyncOut();
		}
	}

	Menu::OnButtonChanged(chButtonCode, bIsPressed);
}

/** **********************************************************************
 *
 * Displays red bold information messaage on display within defined timeout. Timeout is defined in "TIMEOUT_MSG" constant
 *
 * @param pszText		message text to show
 * @param bPermanent	if true, this message will be keep after time out
 *
 *********************************************************************** */
void BaseModePage::ReportMessage(const char* pszText, bool bPermanent)
{
	m_pInfoLabel->SetValue(pszText);
	m_pInfoLabel->SetVisible(true);

	if (bPermanent)
	{
		m_pszPermanentMesgText = pszText;
	}

	// Set timeout period
	m_unMsgTimeout = 0;
	CheckUpdateTime(TIMEOUT_MSG, &m_unMsgTimeout);
}

/** **********************************************************************
 *
 * Periodically function checks timeout for information messaage.
 * When timeout is expiered ti removes information messaage or replaces it to permanent message
 *
 *********************************************************************** */
void BaseModePage::Loop()
{
	if (m_unMsgTimeout && CheckUpdateTime(TIMEOUT_MSG, &m_unMsgTimeout))
	{
		m_unMsgTimeout = 0;
		if (m_pInfoLabel)
		{
			if (m_pszPermanentMesgText)
			{
				m_pInfoLabel->SetValue(m_pszPermanentMesgText);
			}
			else if (!g_deviceState.GetCalibrationEnabled())
			{
				m_pInfoLabel->SetValue(GetLangString(MSG_CALIBRATION_DISABLED));
			}
			else if (m_pInfoLabel->GetVisible())
			{
				m_pInfoLabel->SetVisible(false);
			}
		}
	}

	if (!GetActiveControl())
	{
		SetActiveControlByCtrlId(ID_CTRL_VALUE);
	}
	Menu::Loop();
}

/** **********************************************************************
 *
 * Shows functional buttons on this page
 * @param enumSet set of buttons to show
 *
 *********************************************************************** */
void BaseModePage::ShowButtonsSet(BUTTONSET enumSet)
{
	switch (enumSet)
	{
		case BUTTONSET_NONE: break;
		case BUTTONSET_DEFAULT:
		{
			// Hidding buttons
			RegisterFnButton(FNBTN_1);
			RegisterFnButton(FNBTN_2);
			RegisterFnButton(FNBTN_3);
		}
	}
}

void BaseModePage::OnBandBtnPressed(FNBTN enumBtn)
{
	EditBoxBand* pEdtMaster = (EditBoxBand*)GetActiveControl();
	EditBoxDigit& edtSlave = pEdtMaster->GetSlaveCtrl();
	if (edtSlave.GetValue())
	{
		string strOldUserInput(edtSlave.GetValue());

		double dNewVal = edtSlave.GetDValue() * edtSlave.GetMultiplier();

		m_bMultiplyerSelectedByFnButton = true;
		double nMult = 1.0;
		switch (enumBtn - GetFnBandButtonsOffset(pEdtMaster->GetId()))
		{
			case FNBTN_MHXX:	nMult = 1.0e+6;	break;
			case FNBTN_KHXX:	nMult = 1.0e+3;	break;
			case FNBTN_XXXX:	break;
			case FNBTN_MLXX:	nMult = 1.0e-3;	break;
			case FNBTN_ULXX:	nMult = 1.0e-6;	break;
			default:
			{
				m_bMultiplyerSelectedByFnButton = false;
			}
		}

		edtSlave.SetDValue(dNewVal, edtSlave.GetScale(), edtSlave.GetFraction(), nMult);
		if (!pEdtMaster->SetByCharInput(false, false))
		{
			edtSlave.SetValue(strOldUserInput.c_str());
		}
	}
}

bool BaseModePage::OnBeforeEditActivate(WithNavigation* pEdit, bool bBecomeActive)
{
	if (bBecomeActive)
	{
		BUTTONSET enumSet = GetButtonSetByCtrlId(pEdit->GetId());
		ShowButtonsSet(enumSet);
	}
	else
	{
		ShowButtonsSet(BUTTONSET_DEFAULT);
	}
	return true; // Unused
}

void BaseModePage::SyncOut()
{
	bool bOn = g_deviceState.GetSwitchOutState();
	m_pOutOnLabel->SetValue(GetLangString(bOn ? MSG_ON : MSG_OFF));
}

void BaseModePage::SyncState(bool bForce)
{
	SyncOut();
	BasePage::SyncState(bForce);
}

void BaseModePage::OverloadChange(bool bOverload)
{
	if (bOverload)
	{
		// On 1000v Logic layer will disables output
		// Reflect it on the UI
		SyncOut();
	}

	ReportMessage(bOverload ? GetOverloadText() : NULL, true);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool BaseModePage::OnClick(FNBTN enumBtn, void* pContext)
{
	bool bHandled = pContext == this; // All "bands" buttons have this pContext
	if (bHandled) 
	{
		OnBandBtnPressed(enumBtn);
	}

	return bHandled;
}

void BaseModePage::OnSenseBtnClicked(FNBTN enumBtn, Button* pButton)
{
	STATESENSE enumSense = pButton->GetToggled() ? STATESENSE_TWO_WIRE : STATESENSE_FOUR_WIRE;
	STATEERR ret = g_deviceState.SetSenseMode(enumSense);
	if (STATEERR_NO_ERROR == ret)
	{
		SyncSense();
	}
	else
	{
		ReportMessage(GetLangString(STATEERR_DISABLE_OUT == ret ? MSG_DISABLE_OUT : MSG_LOCAL_SENSE_ONLY));
	}
}

void BaseModePage::SyncSense()
{
	// Sense is unknown when device goes to full sand by mode.
	STATESENSE sense, calibrationSense;
	if (STATEERR_NO_ERROR == g_deviceState.GetSenseMode(sense))
	{
		Button* pBtnSense = GetFnButton(FNBTN_SENSE);
		bool bIsValidSense = g_deviceState.IsValidSense(STATESENSE_FOUR_WIRE);
		STATEMODE mode = g_deviceState.GetOperationMode();

		pBtnSense->SetValue(GetLangString(mode == STATEMODE_R ? MSG_WIRE : MSG_FEEDBACK));
		pBtnSense->SetVisible(bIsValidSense);
		pBtnSense->SetToggled(STATESENSE_FOUR_WIRE == sense);

		if (mode == STATEMODE_U)
		{
			bool bIsDC;
			WORKUNIT wu;
			g_deviceState.GetDirecionType(bIsDC);
			g_deviceState.GetValue(wu);
			calibrationSense = gs_calibrationManager.GetPointsManager().GetBandSense(bIsDC ? MODE_DCU : MODE_ACU, wu.pBand->bandIdx);
		}
		else
			calibrationSense = sense;
		m_pLabelSense->SetColor(sense == calibrationSense ? COLOR_FG_DEFAULT : COLOR_RED);
		if (mode == STATEMODE_R)
			m_pLabelSense->SetValue(GetLangString(STATESENSE_TWO_WIRE == sense ? MSG_2_WIRE : MSG_4_WIRE));
		else
			m_pLabelSense->SetValue(GetLangString(STATESENSE_TWO_WIRE == sense ? MSG_INT_FEEDBACK : MSG_EXT_FEEDBACK));
		m_pLabelSense->SetVisible(!g_deviceState.GetDividerEnabled());
		m_pLabelSense->SetInvalid();
	}
}
