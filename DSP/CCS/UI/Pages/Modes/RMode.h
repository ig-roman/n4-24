/*
 * RMode.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef RMODE_H_
#define RMODE_H_

#include "BaseModePage.h"

#define FNBTN_OHM		FNBTN_3
#define FNBTN_KOHM		FNBTN_4
#define FNBTN_MOHM		FNBTN_5
#define FNBTN_R_OFFSET	0

#define FNBTN_R			FNBTN_7

class RMode: public BaseModePage
{
public:
	RMode();
	virtual ~RMode() {};
	virtual PAGE GetPageId() { return PAGE_MODE_R; };
	virtual void OnInitPage();
	virtual void ShowButtonsSet(BUTTONSET enumSet);
	virtual BUTTONSET GetButtonSetByCtrlId(uint8_t unCtrlId);
	virtual bool OnClick(FNBTN enumBtn, void* pContext);

	virtual void SyncState(bool bForce = false);
	void SyncSelectedRBtn(WORKUNIT& wu);
	void SyncValue();

	bool OnBeforeByCharInputChange(EditBoxBand* pEdit, bool bSet, bool bParentFocusChage, const char* pszVal);
	bool OnCursorLimit(bool bRightBound);

	void OnRBtnClick(uint8_t unBtnIdx);

protected:
	virtual int8_t GetFnBandButtonsOffset(uint8_t unCtrlId);
	virtual const char* GetWorkingUnits() { return GetLangString(MSG_R); };
	virtual double GetMultiplier(WORKUNIT& wu, uint16_t* punMesgId);

private:
	EditBoxBand* m_pEditValue;
	int8_t m_uOldToggledBtn;
};

#endif /* RMODE_H_ */
