/*
 * UMode.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef PLAYMODE_H_
#define PLAYMODE_H_

#include "../UMode.h"
#include "WithActionStep.h"

typedef enum { PLAYSTAT_STOPPED, PLAYSTAT_RUNS, PLAYSTAT_PAUSED } PLAYSTAT;

class PlayMode : public UMode, public WithActionStep
{
public:
	PlayMode();
	virtual ~PlayMode() {};

	virtual void Loop();
	virtual PAGE GetPageId() { return PAGE_AUTO_MODE; };
	virtual void OnInitPage();
	virtual void ShowButtonsSet(BUTTONSET enumSet);
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
	virtual void SyncSense() { /* Skip base implementation bacuse it syncronizes SENSE button but on Play page there is NEXT button instead. */}
	virtual PAGE GetPageAccordingToMode();
	void SyncStep();
	void SetAutoStart();

protected:
	bool CanIncrement(double* pdFreqValue = NULL);

private:
	double IncFreqValue(double& dFreqValue, double inc, uint8_t unFreqIncFuncIdx) const;
	bool IsStarted() const;
	void LoadGroupData();
	void StartGroup();
	void StartStep();
	void Pause();
	void StopStep();

private:
	void ActivateNextStep();

private:
	PLAYSTAT m_ePlayStat;
	double m_dFreqValue;
	bool bAutoStart;
	Label* m_pLabelSelection;
};

#endif /* PLAYMODE_H_ */
