/*
 * ArraysManager.h
 *
 *  Created on: Feb 4, 2015
 *      Author: Sergei Tero
 */

#ifndef GROUPSMANAGER_H_
#define GROUPSMANAGER_H_

#include "../../Menu.h"
#include "BaseManager.h"

class GroupsManager : public BaseManager
{
public:
	GroupsManager() : BaseManager(), m_bCanDelete(false) {};
	virtual ~GroupsManager() {};

	virtual PAGE GetPageId() { return PAGE_GROUPS_MGR; };
	virtual void OnInitPage();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
	virtual void ShowButtonsSet();

	virtual void OnUIBtnClick(uint16_t unBtnIdx, bool bFromEdit);
	virtual int16_t GetActiveBtnIdx() { return GetActiveGroupIdx(); };
	virtual int16_t GetButtonsCount() { return GetGroupsCount(); };

protected:
	virtual uint16_t GetPageCaptionId() { return MSG_AUTO_MODE_CAPTION_GROUPS; };

private:
	bool m_bCanDelete;
};

#endif /* GROUPSMANAGER_H_ */
