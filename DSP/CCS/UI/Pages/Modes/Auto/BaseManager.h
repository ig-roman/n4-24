/*
 * ArraysManager.h
 *
 *  Created on: Feb 4, 2015
 *      Author: Sergei Tero
 */

#ifndef BASEMANAGER_H_
#define BASEMANAGER_H_

#include "../../Menu.h"
#include "WithActionStep.h"
#include "../../../PageManager.h"
#include "../../../Controls/EditBoxDigit.h"

#define MAX_GROUPS_RER_PAGE_COUNT	21

class BaseManager : public Menu, public IOnClickCompatible, public WithActionStep
{
public:
	BaseManager();
	virtual ~BaseManager() {};

	virtual void OnInitPage();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
	virtual void OnUIBtnClick(uint16_t unBtnIdx, bool bFromEdit);
	virtual void ShowButtonsSet();
	virtual int16_t GetActiveBtnIdx() = 0;
	virtual int16_t GetButtonsCount() = 0;
	bool OnValueChange(int32_t nValue);

protected:
	void AddUIButon(const char* pszFormat, int16_t nButtonIdx, bool bNotEmpty);
	virtual uint16_t GetPageCaptionId() = 0;
	virtual void GetSelectionText(char* pszBuff) { GetSelectionTextBase(pszBuff); };
	void SyncButtonToggle();

private:
	uint16_t m_nUIBtnX;
	uint16_t m_nUIBtnY;
	int16_t m_uOldToggledBtn;

	Label* m_pLabelCaption;
	Label* m_pLabelSelection;

	EditBoxDigit* m_pEditSelection;
};

#endif /* BASEMANAGER_H_ */
