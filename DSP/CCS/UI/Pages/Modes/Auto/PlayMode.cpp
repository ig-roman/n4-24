/*
 * PlayMode.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "PlayMode.h"
//#include "../../Controls/WithNavigation.h"
#include "../../../PageManager.h"
#include "ArraysManager.h"
#include "GroupsManager.h"
#include "EntryConfig.h"

PlayMode::PlayMode() : UMode(), WithActionStep(),
	bAutoStart(false),
	m_pLabelSelection(NULL),
	m_dFreqValue(0),
	m_ePlayStat(PLAYSTAT_STOPPED)
{}

void PlayMode::SetAutoStart()
{
	bAutoStart = true;
}

// ST TODO: REFINE MODEL TO GET RID OF THIS PATCH
PAGE PlayMode::GetPageAccordingToMode()
{	
	if (STATEMODE_U == g_deviceState.GetOperationMode())
	{
		return PAGE_AUTO_MODE;
	}
	else
	{
		return UMode::GetPageAccordingToMode();
	}
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void PlayMode::OnInitPage()
{
	// Delta frame and this label are intercect. Create it first to get Z index lower than popups frames to delta.
	m_pLabelSelection = CreateLabel(AMODE_PAGE_LEFT_OFFSET, 398, MSG_NONE, AMODE_PAGE_MAX_CONTENT);
	m_pLabelSelection->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY);

	UMode::OnInitPage();

	Button* pBtnExit = GetFnButton(FNBTN_EXIT);
	pBtnExit->SetToggled(false);

	if (bAutoStart)
	{
		StartStep();
	}
	else
	{
		PCGROUP_DATA pGrpData = GetActiveGroup();
		m_dFreqValue = pGrpData ? pGrpData->dFreqStart : 0;
	}
	SyncStep();
}

/** **********************************************************************
 *
 * Shows functional buttons on this page
 * @param enumSet set of buttons to show
 *
 *********************************************************************** */
void PlayMode::ShowButtonsSet(BUTTONSET enumSet)
{
	WithFrequencyPage::ShowButtonsSet(enumSet);

	switch (enumSet)
	{
		case BUTTONSET_DEFAULT:
		{
			bool bHasGroup = NULL != GetActiveGroup();

			if (!IsStarted())
			{
				RegisterFnButton(FNBTN_ARRAYS,	GetLangString(MSG_ARRAYS),										this);
				RegisterFnButton(FNBTN_GROUPS,	-1 == GetActiveArrayIdx() ? NULL : GetLangString(MSG_GROUPS),	this);
				RegisterFnButton(FNBTN_CONFIG,	bHasGroup ? GetLangString(MSG_CONFIG) : NULL,					this);
			}

			if (bHasGroup)
			{
				RegisterFnButton(FNBTN_START_STOP,	IsStarted() ? GetLangString(MSG_STOP) : GetLangString(MSG_START),	this);
				RegisterFnButton(FNBTN_NEXT_ON_PLAY, PLAYSTAT_PAUSED == m_ePlayStat ? GetLangString(MSG_NEXT) : NULL,	this);	
			}
			else
			{
				RegisterFnButton(FNBTN_START_STOP,	NULL,														this);
				RegisterFnButton(FNBTN_NEXT_ON_PLAY, NULL,														this);	
			}

			RegisterFnButton(FNBTN_EXIT,		GetLangString(MSG_EXIT),										this);
			RegisterFnButton(FNBTN_8,			NULL,															PAGE_MODE_U);

			break;
		}
	}
}

double PlayMode::IncFreqValue(double& dFreqValue, double inc, uint8_t unFreqIncFuncIdx) const
{
	double dRet = dFreqValue;
	if (GROUPTYPE_LIN == (GROUPTYPE)unFreqIncFuncIdx)
	{
		dRet += inc;
	}
	else
	{
		dRet *= inc;
	}
	return dRet;
}

bool PlayMode::IsStarted() const
{
	return PLAYSTAT_RUNS == m_ePlayStat;
}

void PlayMode::LoadGroupData()
{
	PCGROUP_DATA pGrpData = GetActiveGroup();
	m_dFreqValue = pGrpData->dFreqStart;
	if (PLAYSTAT_RUNS == m_ePlayStat)
	{
		m_unNextStepTime = GetTime() + pGrpData->unDelay * 1000;
	}
}

void PlayMode::StartGroup()
{
	m_ePlayStat = PLAYSTAT_RUNS;
	LoadGroupData();
}

void PlayMode::StartStep()
{
	if (PLAYSTAT_STOPPED == m_ePlayStat)
	{
		StartGroup();
	}
	else if (PLAYSTAT_PAUSED == m_ePlayStat)
	{
		m_ePlayStat = PLAYSTAT_RUNS;
		ActivateNextStep();		
	}
}

void PlayMode::Pause()
{
	m_unNextStepTime = 0;
	m_ePlayStat = PLAYSTAT_PAUSED;
	SyncStep();
}

void PlayMode::StopStep()
{
	m_unNextStepTime = 0;
	m_ePlayStat = PLAYSTAT_STOPPED;
	SyncStep();
}

bool PlayMode::CanIncrement(double* pdFreqValue /* = NULL */)
{
	PCGROUP_DATA pGrpData = GetActiveGroup();
	double dNewFreqValue = IncFreqValue(m_dFreqValue, pGrpData->dFreqInc, pGrpData->unFreqIncFuncIdx);
	if (pdFreqValue)
	{
		*pdFreqValue = dNewFreqValue;
	}
	bool bIncPositive = GROUPTYPE_LIN == (GROUPTYPE)pGrpData->unFreqIncFuncIdx ? pGrpData->dFreqInc > 0 : pGrpData->dFreqInc >= 1;
	bool bInRange = bIncPositive
					? dNewFreqValue <= pGrpData->dFreqStop
					: dNewFreqValue >= pGrpData->dFreqStop;
	return dNewFreqValue != m_dFreqValue && bInRange;
}

void PlayMode::ActivateNextStep()
{
	PCGROUP_DATA pGrpData = GetActiveGroup();

	double dNewFreqValue;
	if (CanIncrement(&dNewFreqValue))
	{
		m_dFreqValue = dNewFreqValue;
		// Start next step if it was started before
		if (IsStarted())
		{
			m_unNextStepTime = GetTime() + pGrpData->unDelay * 1000;
		}
	}
	else
	{
		if (ActivateNextGroup())
		{
			LoadGroupData();
			// Start next step if it was started before
			//if (IsStarted())
			//{
			//	StartGroup();
			//}
		}
		else
		{
			// No more groups.
			StopStep();
			ShowButtonsSet(BUTTONSET_DEFAULT);
		}
	}
}

void PlayMode::SyncStep()
{
	PCGROUP_DATA pGrpData = GetActiveGroup();

	if (pGrpData)
	{
		m_pEditFreq->SetWithNavigation(!IsStarted());
		m_pEditValue->SetWithNavigation(!IsStarted());
		if (IsStarted())
		{
			SetActiveControl(-1);
		}

		bool bIsDCOld;
		bool bIsDCNew = 0 == m_dFreqValue;
		g_deviceState.GetDirecionType(bIsDCOld);
		if (bIsDCOld != bIsDCNew)
		{
			g_deviceState.SetDirecionType(bIsDCNew);
		}
		g_deviceState.SetFrequency(m_dFreqValue, true);
		g_deviceState.SetValue(pGrpData->dVoltage, m_dFreqValue, true);

		SyncACDC();
		Button* pDcBtn = GetFnButton(FNBTN_EXIT); // Button EXIT conflicts with FNBTN_DC
		pDcBtn->SetToggled(false);

		SyncFreq();
		SyncValue();
		SyncDelta();
	}

	char pszBuff[100] = {0};
	GetSelectionTextBase(pszBuff);
	if (*pszBuff)
	{
		m_pLabelSelection->SetValue(pszBuff, true);
	}
}

/** **********************************************************************
 *
 * The periodic function. Is invoked always.
 * Use CheckUpdateTime() function for custom period.
 *
 *********************************************************************** */
void PlayMode::Loop()
{
	if (m_unNextStepTime)
	{
		PCGROUP_DATA pGrpData = GetActiveGroup();
		if (CheckUpdateTime(m_unNextStepTime, &m_unNextStepTime)) // Use any non zero value for first arg because ActivateNextStep() recalculates m_unNextStepTime
		{
			ActivateNextStep();
			SyncStep();
		}
	}
	UMode::Loop();
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool PlayMode::OnClick(FNBTN enumBtn, void* pContext)
{
	bool bHandled = false;
	if (NULL == pContext)
	{
		bHandled = true;
		BasePage* pNewPage = NULL;
		switch (enumBtn)
		{
			case FNBTN_ARRAYS:
			{
				CEATE_PAGE_WITH_ACTION(ArraysManager, PAGE_ARRAYS_MGR, pNewPage);
				break;
			}
			case FNBTN_GROUPS:
			{
				CEATE_PAGE_WITH_ACTION(GroupsManager, PAGE_GROUPS_MGR, pNewPage);
				break;
			}
			case FNBTN_CONFIG:
			{
				CEATE_PAGE_WITH_ACTION(EntryConfig, PAGE_ENTRY_CONFIG, pNewPage);
				break;
			}
			case FNBTN_START_STOP:
			{
				if (IsStarted())
				{
					Pause();
				}
				else
				{
					StartStep();
					SyncStep();
				}
				ShowButtonsSet(BUTTONSET_DEFAULT);
				break;
			}
			case FNBTN_NEXT_ON_PLAY:
			{
				ActivateNextStep();
				SyncStep();
				break;
			}
			case FNBTN_EXIT:
			{
				StopStep();
				ResetAllCaches();
				UMode* pUMode = (UMode*)g_pageManager.CreatePage(PAGE_MODE_U);
				g_pageManager.ShowPage(pUMode);
				break;
			}
			default:
			{
				bHandled = false;
				break;
			}
		}

		if (bHandled && pNewPage)
		{
			g_pageManager.ShowPage(pNewPage);
		}
	}

	if (!bHandled)
	{
		bHandled = UMode::OnClick(enumBtn, pContext);
	}

	return bHandled;
}
