/*
 * EntryConfig.h
 *
 *  Created on: Feb 4, 2015
 *      Author: Sergei Tero
 */

#ifndef ENTRYCONFIG_H_
#define ENTRYCONFIG_H_

#include <math.h>
#include "../../Menu.h"
#include "WithActionStep.h"
#include "../BaseModePage.h"

class EntryConfig : public Menu, public IOnClickCompatible, public WithActionStep
{
public:
	EntryConfig();
	virtual ~EntryConfig() {};

	virtual PAGE GetPageId() { return PAGE_ENTRY_CONFIG; };
	virtual void OnInitPage();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
	void ShowButtonsSet();
	void SetCreateMode() { m_bCreateMode = true; };
	void OnChange(bool bSetChanged = true);

protected:
	void GetSelectionText(char* pszBuff);

private:
	 EditBoxDigit* CreatePageLine(int16_t unLabelMsgId, const Validator* pValidator, double dValue, uint16_t& unY);
	 void UpdatePageLine(Label* pLabel, EditBoxDigit* pEdit, int16_t unLabelMsgId, double dValue);
	 void SyncGroupMode(GROUPTYPE& eGroupType, bool bLoad = false);

private:
	EditBoxDigit* m_pEditArrayNr;
	EditBoxDigit* m_pEditFreqStart;
	EditBoxDigit* m_pEditFreqStop;
	EditBoxDigit* m_pEditFreqInc;
	EditBoxDigit* m_pEditTime;
	EditBoxDigit* m_pEditVoltage;

	GROUP_DATA m_grpData; // ST TODO: REMOVE IT?
	bool m_bCreateMode;

	Label* m_pLabelCaption;
	Label* m_pLabelSelection;

	GROUPTYPE m_eGroupType;

	bool m_bIsModified;
	bool m_bCanLoseChanges;
};

#endif /* ENTRYCONFIG_H_ */
