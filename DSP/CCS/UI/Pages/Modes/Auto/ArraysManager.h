/*
 * ArraysManager.h
 *
 *  Created on: Feb 4, 2015
 *      Author: Sergei Tero
 */

#ifndef ARRAYSMANAGER_H_
#define ARRAYSMANAGER_H_

#include "../../Menu.h"
#include "BaseManager.h"

class ArraysManager : public BaseManager
{
public:
	ArraysManager() : BaseManager(), m_bCanDelete(false) {};
	virtual ~ArraysManager() {};

	virtual PAGE GetPageId() { return PAGE_ARRAYS_MGR; };
	virtual void OnInitPage();
	virtual bool OnClick(FNBTN enumBtn, void* pContext);
	virtual void ShowButtonsSet();

	virtual void OnUIBtnClick(uint16_t unBtnIdx, bool bFromEdit);
	virtual int16_t GetActiveBtnIdx() { return GetActiveArrayIdx(); };
	virtual int16_t GetButtonsCount() { return MAX_ARRAYS_COUNT; };

protected:
	virtual uint16_t GetPageCaptionId() { return MSG_AUTO_MODE_CAPTION_ARRAY; };

private:
	bool m_bCanDelete;
};

#endif /* ARRAYSMANAGER_H_ */
