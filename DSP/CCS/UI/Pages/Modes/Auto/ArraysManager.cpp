/*
 * ArraysManager.cpp
 *
 *  Created on: Feb 4, 2015
 *      Author: seregia
 */

#include "ArraysManager.h"
#include "GroupsManager.h"

void ArraysManager::OnInitPage()
{
	for (int i = 0; i < MAX_ARRAYS_COUNT; i++)
	{
		int16_t nGroupsCnt = GetArrays()[i];
		if (nGroupsCnt >= 0)
		{
			AddUIButon("A%02d", i, nGroupsCnt > 0);
		}
	}

	BaseManager::OnInitPage();
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool ArraysManager::OnClick(FNBTN enumBtn, void* pContext)
{
	bool bHandled = false;
	if (NULL == pContext)
	{
		bHandled = true;
		switch (enumBtn)
		{
			case FNBTN_GROUPS:
			{
				OPEN_PAGE_WITH_ACTION(GroupsManager, PAGE_GROUPS_MGR);
				break;
			}
			case FNBTN_DELETE:
			{
				if (m_bCanDelete)
				{
					DeleteArray(GetActiveArrayIdx());
					OPEN_PAGE_WITH_ACTION(ArraysManager, PAGE_ARRAYS_MGR);
				}
				else
				{
					ShowInfo(GetLangString(MSG_DELETE_ARRAY_CONFIRM));
					m_bCanDelete = true;
				}
			}
			default:
			{
				bHandled = false;
				break;
			}
		}
	}

	if (!bHandled)
	{
		bHandled = BaseManager::OnClick(enumBtn, pContext);
	}

	return bHandled;
}

/** **********************************************************************
 *
 * Shows functional buttons on this page
 *
 *********************************************************************** */
void ArraysManager::ShowButtonsSet()
{
	BaseManager::ShowButtonsSet();

	if (-1 == GetActiveArrayIdx())
	{
		RegisterFnButton(FNBTN_GROUPS,	NULL, this);
	}
	else
	{
		RegisterFnButton(FNBTN_GROUPS,	GetLangString(MSG_GROUPS), this);
	}

	if (-1 == GetActiveArrayIdx() || 0 == GetGroupsCount())
	{
		RegisterFnButton(FNBTN_DELETE,	NULL, this);
	}
}

void ArraysManager::OnUIBtnClick(uint16_t unBtnIdx, bool bFromEdit)
{
	SetActiveArrayIdx(unBtnIdx);
	if (GetGroupsCount() > 0)
	{
		SetActiveGroupIdx(0);
	}

	if (m_bCanDelete)
	{
		// User after confirmation clicks to another array
		m_bCanDelete = false;
	}

	BaseManager::OnUIBtnClick(unBtnIdx, bFromEdit);
}
