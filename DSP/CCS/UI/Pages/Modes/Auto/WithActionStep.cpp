/*
 * WithActionStep.cpp
 *
 *  Created on: Feb 4, 2015
 *      Author: seregia
 */

#include "WithActionStep.h"
#include "../../../../LogicInterface.h"
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include "../../UILocalization.h"

#define ENTRY_FORMAT "%s: %d   "

WithActionStep::WithActionStep() :
	m_nArrayIdx(-1),
	m_nArrGroupIdx(-1),
	m_nArrGroupsCount(-1),
	m_unNextStepTime(0),
	m_arrGroupsMemCache(NULL),
	m_arrArraysCache(NULL)
{}

void WithActionStep::SetActionStep(WithActionStep* pOldPageWithStep)
{
	// Copy state of object (all members)
	m_nArrayIdx = pOldPageWithStep->m_nArrayIdx;
	m_nArrGroupIdx = pOldPageWithStep->m_nArrGroupIdx;
	m_nArrGroupsCount = pOldPageWithStep->m_nArrGroupsCount;
	m_unNextStepTime = pOldPageWithStep->m_unNextStepTime;
	m_activeGroup = pOldPageWithStep->m_activeGroup;

	// Move cache to another page
	m_arrGroupsMemCache = pOldPageWithStep->m_arrGroupsMemCache;
	pOldPageWithStep->m_arrGroupsMemCache = NULL;

	m_arrArraysCache = pOldPageWithStep->m_arrArraysCache;
	pOldPageWithStep->m_arrArraysCache = NULL;
}

void WithActionStep::SetActionStepIdx(int16_t nArrayIdx, int16_t nGroupIdx)
{
	SetActiveArrayIdx(nArrayIdx);
	SetActiveGroupIdx(nGroupIdx);
}

void WithActionStep::SetActiveGroupIdx(int16_t nGroupIdx)
{
	if (nGroupIdx >= 0 && m_nArrGroupIdx != nGroupIdx)
	{
		m_nArrGroupIdx = nGroupIdx;
	}
}

int16_t WithActionStep::GetGroupsCount()
{
	LoadGroupsData();
	return m_nArrGroupsCount;
}

void WithActionStep::SetActiveArrayIdx(int16_t nArrayIdx)
{
	if (m_nArrayIdx != nArrayIdx)
	{
		m_nArrayIdx = nArrayIdx;
		m_nArrGroupIdx = -1;
		ResetAllCaches();
	}
}

WithActionStep::~WithActionStep()
{
	ResetAllCaches();
}

void WithActionStep::ResetAllCaches()
{
	if (m_arrGroupsMemCache)
	{
		delete m_arrGroupsMemCache;
		m_arrGroupsMemCache = NULL;
	}

	if (m_arrArraysCache)
	{
		delete m_arrArraysCache;
		m_arrArraysCache = NULL;
	}

	m_nArrGroupsCount = -1;
}

bool IsGroupActive(int16_t	nArrayId)
{
	return nArrayId >= 0 && nArrayId < MAX_ARRAYS_COUNT;
}

void WithActionStep::SaveGroup(PCGROUP_DATA pGroup, int16_t nGroupIdx)
{
	CLogic_StoreBytes(GrpIdxToAddr(nGroupIdx), sizeof(GROUP_DATA), (uint8_t*)(pGroup));
}

int16_t WithActionStep::SaveActiveGroup(PCGROUP_DATA pGroup, bool bAddNew, int16_t& nGroupIdxMem)
{
	nGroupIdxMem = -1;
	if (bAddNew)
	{
		for (int16_t nAllGroupIdx = 0; nAllGroupIdx < MAX_GROUPS_COUNT; nAllGroupIdx++)
		{
			int16_t nGroupsArrayIdx = FindsArrayIdxByAddress(nAllGroupIdx);
			if (!IsGroupActive(nGroupsArrayIdx))
			{
				nGroupIdxMem = nAllGroupIdx;
				break;
			}
		}
		if (-1 == nGroupIdxMem)
		{
			CLogic_SetLastError(::GetLangString(MSG_NO_SPACE_FOR_GROUP));
		}
	}
	else
	{
		if (m_nArrGroupIdx >= 0 && m_nArrGroupIdx < m_nArrGroupsCount)
		{
			LoadGroupsData();
			nGroupIdxMem = m_arrGroupsMemCache[m_nArrGroupIdx];
		}
		else
		{
			CLogic_SetLastError("WithActionStep::SaveActiveGroup() - Group wasn't loaded for given index");
		}
	}

	if (-1 != nGroupIdxMem)
	{
		SaveGroup(pGroup, nGroupIdxMem);
		ResetAllCaches();
		LoadGroupsData();

		if (bAddNew)
		{
			m_nArrGroupIdx = -1;
			for (int16_t nAllGroupIdx = 0; nAllGroupIdx < MAX_GROUPS_COUNT; nAllGroupIdx++)
			{
				if (nGroupIdxMem == m_arrGroupsMemCache[nAllGroupIdx])
				{
					m_nArrGroupIdx = nAllGroupIdx;
					break;
				}
			}
		}
	}

	return MIN(m_nArrGroupIdx, m_nArrGroupsCount - 1);
}

bool WithActionStep::ActivateNextGroup()
{
	bool bMoved = false;

	bMoved = m_nArrGroupsCount > m_nArrGroupIdx + 1;
	if (bMoved)
	{
		SetActiveGroupIdx(m_nArrGroupIdx + 1);
	}

	return bMoved;
}

PCGROUP_DATA WithActionStep::GetActiveGroup()
{
	bool bLoaded = false;
	if (-1 != m_nArrGroupIdx && -1 != m_nArrayIdx)
	{
		LoadGroupsData();
		CLogic_RestoreBytes(GrpIdxToAddr(m_arrGroupsMemCache[m_nArrGroupIdx]), sizeof(GROUP_DATA), (uint8_t*)(&m_activeGroup));
		bLoaded = true;
	}

	return bLoaded ? &m_activeGroup : NULL;
}

uint32_t WithActionStep::GrpIdxToAddr(int16_t nGroupIdx) const
{
	return (nGroupIdx * sizeof(GROUP_DATA)) + EEPROM_ADDR_AUTO_MODES;
}

void WithActionStep::LoadGroupsData()
{
	if (!m_arrGroupsMemCache)
	{
		m_arrGroupsMemCache = new int16_t[MAX_GROUPS_COUNT];

		m_nArrGroupsCount = 0;
		for (int16_t nAllGroupIdx = 0; nAllGroupIdx < MAX_GROUPS_COUNT; nAllGroupIdx++)
		{
			int16_t nGroupArrayIdx = FindsArrayIdxByAddress(nAllGroupIdx);
			if (nGroupArrayIdx == m_nArrayIdx)
			{
				m_arrGroupsMemCache[m_nArrGroupsCount++] = nAllGroupIdx;
			}
		}
	}

}

int16_t WithActionStep::FindsArrayIdxByAddress(int16_t nAllGroupIdx) const
{
	GROUP_DATA groupPart;
	uint32_t unArrayIdxStructOffset = offsetof(GROUP_DATA, unArrayId);
	CLogic_RestoreBytes(GrpIdxToAddr(nAllGroupIdx) + unArrayIdxStructOffset, sizeof(int16_t), ((uint8_t*)(&groupPart)) + unArrayIdxStructOffset);
	return LimitArrayId(groupPart.unArrayId);
}

int16_t WithActionStep::LimitArrayId(int16_t unArrayId) const
{
	return MAX(MIN(MAX_ARRAYS_COUNT - 1, unArrayId), -1);
}

const int16_t* WithActionStep::GetArrays()
{
	if (!m_arrArraysCache)
	{
		LoadArrays();
	}

	return m_arrArraysCache;
}

void WithActionStep::DeleteArray(int16_t nArrIdx)
{
	ResetAllCaches();
	m_nArrGroupIdx = -1;
	for (int16_t nAllGroupIdx = MAX_GROUPS_COUNT - 1; nAllGroupIdx >= 0; nAllGroupIdx--)
	{
		int16_t nGroupArrayIdx = FindsArrayIdxByAddress(nAllGroupIdx);
		if (nGroupArrayIdx == nArrIdx)
		{
			DeleteGroupInt(nAllGroupIdx);
		}
  	}
}

bool WithActionStep::MoveGroup(int16_t nGrpIdx, bool bNext)
{
	bool bCanMove = bNext ? m_nArrGroupsCount > m_nArrGroupIdx + 1 : m_nArrGroupIdx > 0;
	if (bCanMove)
	{
		LoadGroupsData();
		int16_t nTargetGrpIdx = m_arrGroupsMemCache[m_nArrGroupIdx + (bNext ? 1 : -1)];
		int16_t nSourceGrpIdx = m_arrGroupsMemCache[nGrpIdx];

		SwapGroupInt(nSourceGrpIdx, nTargetGrpIdx);
	}

	return bCanMove;
}

void WithActionStep::SwapGroupInt(int16_t nGrpIdxSource, int16_t nGrpIdxTarget, bool bCopy /* = false */)
{
	// Save source group
	GROUP_DATA sourceGroupData;
	CLogic_RestoreBytes(GrpIdxToAddr(nGrpIdxSource), sizeof(GROUP_DATA), (uint8_t*)(&sourceGroupData));

	if (!bCopy)
	{
		// Write target into source place
		GROUP_DATA targetGroupData;
		CLogic_RestoreBytes(GrpIdxToAddr(nGrpIdxTarget), sizeof(GROUP_DATA), (uint8_t*)(&targetGroupData));
		SaveGroup(&targetGroupData, nGrpIdxSource);
	}

	// Write saved source into target place
	SaveGroup(&sourceGroupData, nGrpIdxTarget);
}

void WithActionStep::DeleteGroupInt(int16_t nDelGroupIdx)
{
	// Find next deleted element
	int16_t nNextDeletedIdx = nDelGroupIdx + 1;

	// Check tail for empty element
	while (MAX_GROUPS_COUNT > nNextDeletedIdx)
	{
		if (-1 == FindsArrayIdxByAddress(nNextDeletedIdx))
		{
			break;
		}
		else
		{
			nNextDeletedIdx++;
		}
	}

	// Move all active subsequent elements to old position - 1 (chain move)
	// It makes elements list without holes.
	// If elements list contains holes, new elements may insert before last element and it looks very strange.
	for (; nDelGroupIdx < nNextDeletedIdx - 1; nDelGroupIdx++)
	{
		SwapGroupInt(nDelGroupIdx + 1, nDelGroupIdx, true);
	}

	// Mark element as deleted
	GROUP_DATA groupData = {0};
	groupData.unArrayId = -1;
	SaveGroup(&groupData, nDelGroupIdx);
}

void WithActionStep::DeleteGroup(int16_t nGrpIdx)
{
	LoadGroupsData();
	int16_t nDelGroupIdx = m_arrGroupsMemCache[nGrpIdx];
	DeleteGroupInt(nDelGroupIdx);

	// All data in m_arrGroupsMemCache become invalid and should be recalculated
	ResetAllCaches();
	m_nArrGroupIdx = -1;
}

void WithActionStep::LoadArrays()
{
	if (m_arrArraysCache)
	{
		delete m_arrArraysCache;
		m_arrArraysCache = NULL;
	}

	m_arrArraysCache = new int16_t[MAX_ARRAYS_COUNT];

	memset(m_arrArraysCache, 0, MAX_ARRAYS_COUNT * sizeof(int16_t));
	for (int16_t nAllGroupIdx = 0; nAllGroupIdx < MAX_GROUPS_COUNT; nAllGroupIdx++)
	{
		int16_t nGroupArrayIdx = FindsArrayIdxByAddress(nAllGroupIdx);
		if (-1 != nGroupArrayIdx)
		{
			m_arrArraysCache[nGroupArrayIdx]++;
		}
	}
}

void WithActionStep::GetSelectionTextBase(char* pszBuff)
{
	//sprintf(pszBuff, "%s - ", "Info");
	if (-1 == m_nArrayIdx)
	{
		sprintf(pszBuff, ::GetLangString(MSG_NO_ARRAY));
	}
	else
	{
		sprintf(pszBuff, ENTRY_FORMAT, ::GetLangString(MSG_ARRAY_NR), m_nArrayIdx + 1);
		sprintf(pszBuff + strlen(pszBuff), ENTRY_FORMAT, ::GetLangString(MSG_TOTAL_GROUPS), m_nArrGroupsCount);
		if (-1 == m_nArrGroupIdx)
		{
			sprintf(pszBuff + strlen(pszBuff), ::GetLangString(MSG_NO_GROUP));
		}
		else
		{
			sprintf(pszBuff + strlen(pszBuff), ENTRY_FORMAT, ::GetLangString(MSG_SELECTED_GROUP), m_nArrGroupIdx + 1);
		}
	}
}
