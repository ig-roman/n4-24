/*
 * ArraysManager.cpp
 *
 *  Created on: Feb 4, 2015
 *      Author: seregia
 */

#include "GroupsManager.h"
#include "ArraysManager.h"
#include "EntryConfig.h"

void GroupsManager::OnInitPage()
{
	for (int i = 0; i < GetGroupsCount(); i++)
	{
		if (i < MAX_GROUPS_RER_PAGE_COUNT)
		{
			AddUIButon("G%02d", i, true);
		}
		else
		{
			break;
		}
	}

	BaseManager::OnInitPage();
}

/** **********************************************************************
 *
 * Shows functional buttons on this page
 *
 *********************************************************************** */
void GroupsManager::ShowButtonsSet()
{
	BaseManager::ShowButtonsSet();

	if (-1 == GetActiveGroupIdx())
	{
		RegisterFnButton(FNBTN_CONFIG,		NULL,							this);
		RegisterFnButton(FNBTN_PREV,		NULL,							this);
		RegisterFnButton(FNBTN_NEXT,		NULL,							this);
		RegisterFnButton(FNBTN_DELETE,		NULL,							this);
	}
	else
	{
		RegisterFnButton(FNBTN_CONFIG,		GetLangString(MSG_CONFIG),		this);
		RegisterFnButton(FNBTN_PREV,		GetLangString(MSG_MOVE_LEFT),	this);
		RegisterFnButton(FNBTN_NEXT,		GetLangString(MSG_MOVE_RIGHT),	this);
	}

	RegisterFnButton(FNBTN_ARRAYS,		GetLangString(MSG_ARRAYS),			this);
	RegisterFnButton(FNBTN_NEW,			GetLangString(MSG_CREATE),			this); 
}

void GroupsManager::OnUIBtnClick(uint16_t unBtnIdx, bool bFromEdit)
{
	if (m_bCanDelete)
	{
		// User after confirmation clicks to another group
		m_bCanDelete = false;
	}

	SetActiveGroupIdx(unBtnIdx);
	BaseManager::OnUIBtnClick(unBtnIdx, bFromEdit);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool GroupsManager::OnClick(FNBTN enumBtn, void* pContext)
{
	bool bHandled = false;
	if (NULL == pContext)
	{
		bHandled = true;
		switch (enumBtn)
		{
			case FNBTN_ARRAYS:
			{
				OPEN_PAGE_WITH_ACTION(ArraysManager, PAGE_ARRAYS_MGR);
				break;
			}
			case FNBTN_DELETE:
			{
				if (m_bCanDelete)
				{
					DeleteGroup(GetActiveGroupIdx());
					OPEN_PAGE_WITH_ACTION(GroupsManager, PAGE_GROUPS_MGR);
				}
				else
				{
					ShowInfo(GetLangString(MSG_DELETE_GROUP_CONFIRM));
					m_bCanDelete = true;
				}
				break;
			}
			case FNBTN_CONFIG:
			{
				// ADD delete logic
				OPEN_PAGE_WITH_ACTION(EntryConfig, PAGE_ENTRY_CONFIG);
				break;
			}
			case FNBTN_NEW:
			{
				EntryConfig* pNewPage = NULL;
				CEATE_PAGE_WITH_ACTION(EntryConfig, PAGE_ENTRY_CONFIG, pNewPage);
				pNewPage->SetCreateMode();
				g_pageManager.ShowPage(pNewPage);

				break;
			}
			case FNBTN_PREV:
			case FNBTN_NEXT:
			{
				if (MoveGroup(GetActiveGroupIdx(), FNBTN_NEXT == enumBtn))
				{
					SetActiveGroupIdx(GetActiveGroupIdx() + (FNBTN_NEXT == enumBtn ? 1 : -1));
					OPEN_PAGE_WITH_ACTION(GroupsManager, PAGE_GROUPS_MGR);
				}
				break;
			}
			default:
			{
				bHandled = false;
				break;
			}
		}
	}

	if (!bHandled)
	{
		bHandled = BaseManager::OnClick(enumBtn, pContext);
	}

	return bHandled;
}
