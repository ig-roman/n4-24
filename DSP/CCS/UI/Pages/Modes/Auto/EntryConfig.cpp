/*
 * EntryConfig.cpp
 *
 *  Created on: Feb 4, 2015
 *      Author: seregia
 */

#include "ArraysManager.h"
#include "GroupsManager.h"
#include "EntryConfig.h"
#include "../../../Controls/EditBoxDigit.h"
#include "PlayMode.h"
#include "../../../PageManager.h"
#include <string.h>

#define LABET_TO_EDIT_ID_OFFSET 50

const MINMAX MINMAX_ARRAYS	= { 1, MAX_ARRAYS_COUNT, 1 };
const MINMAX MINMAX_K		= { -1000, 1000, 10 };
const MINMAX MINMAX_TIME	= { 2, 86400L, 5 }; // 2s - 24h

EntryConfig::EntryConfig() : WithActionStep(), Menu(),
	m_bCreateMode(false),
	m_pLabelCaption(NULL),
	m_pLabelSelection(NULL),
	m_eGroupType(GROUPTYPE_LIN),
	m_bCanLoseChanges(false),
	m_bIsModified(false)
{}

EditBoxDigit* EntryConfig::CreatePageLine(int16_t unLabelMsgId, const Validator* pValidator, double dValue, uint16_t& unY)
{
	Label* pLabel = CreateLabel(AMODE_PAGE_LEFT_OFFSET, unY, MSG_NONE, 380);
	pLabel->SetAlignment(AL_RIGHT_JUSTIFY_TEXT);

	Label* pLabelSignEq = CreateLabel(380, unY, MSG_NONE, 30);
	pLabelSignEq->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY);
	pLabelSignEq->SetValue("=");

	EditBoxDigit* pEdit = EditBox::CreateInstance<EditBoxDigit>(this, pLabel->GetId() + LABET_TO_EDIT_ID_OFFSET, 410, unY, 270, DEFAULT_CONTROL_HEIGHT, AL_NONE, 12);
	pEdit->SetValidator(pValidator);

	UpdatePageLine(pLabel, pEdit, unLabelMsgId, dValue);

	RegisterControl(pEdit);

	unY += DEFAULT_CONTROL_HEIGHT;

	return pEdit;
}

bool ECOnChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext)
{
	if (strOldVal != strNewVal)
	{
		((EntryConfig*)pContext)->OnChange();
	}
	return true;
}

void EntryConfig::OnChange(bool bSetChanged)
{
	Button* pSaveBtn = GetFnButton(FNBTN_SAVE);
	pSaveBtn->SetVisible(bSetChanged);
	m_bIsModified = bSetChanged;
	m_bCanLoseChanges = !bSetChanged;
}

void EntryConfig::UpdatePageLine(Label* pLabel, EditBoxDigit* pEdit, int16_t unLabelMsgId, double dValue)
{
	pLabel->SetValue(GetLangString(unLabelMsgId));

	pEdit->SetDValue(dValue);
	pEdit->SetOnAfterValueChange(ECOnChange, this);
	DigiBoxValidator* pDigiValidator = pEdit->GetDigiBoxValidator();
	if (pDigiValidator)
	{
		pDigiValidator->OnBeforeActivateValidate(false);
	}
}

Validator s_validatorArrays(&MINMAX_ARRAYS,	NULL,				VAL_FLAG_POS_INT_NUM, 2, 0);
Validator s_validatorFreqLin(&MINMAX_FREQ,	UNITS_FREQUENCY,	VAL_FLAG_REAL_NUM);
Validator s_validatorFreqLog(&MINMAX_K,		NULL,				VAL_FLAG_REAL_NUM);
Validator s_validatorTime(&MINMAX_TIME,		UNITS_TIME,			VAL_FLAG_POS_INT_NUM, 3, 0);

void EntryConfig::SyncGroupMode(GROUPTYPE& eGroupType, bool bLoad /* = false */)
{
	double dValue;
	PCGROUP_DATA pGrpData = GetActiveGroup();

	if (pGrpData && bLoad)
	{
		dValue		= pGrpData->dFreqInc;
		eGroupType	= (GROUPTYPE)pGrpData->unFreqIncFuncIdx;
	}
	else
	{
		dValue		= GROUPTYPE_LIN == eGroupType ? s_validatorFreqLin.GetDefaultValue()	: s_validatorFreqLog.GetDefaultValue();
	}
	
	int16_t unLabelMsgId	= GROUPTYPE_LIN == eGroupType ? MSG_AUTO_MODE_PROP_F_INC				: MSG_AUTO_MODE_PROP_LOG_K_IDX;
	Validator* pValidator	= GROUPTYPE_LIN == eGroupType ? &s_validatorFreqLin						: &s_validatorFreqLog;

	Label* pLabel = (Label*)FindControlByCtrlId(m_pEditFreqInc->GetId() - LABET_TO_EDIT_ID_OFFSET);
	m_pEditFreqInc->SetValidator(pValidator);
	UpdatePageLine(pLabel, m_pEditFreqInc, unLabelMsgId, dValue);
}

void EntryConfig::OnInitPage()
{
	Menu::OnInitPage();

	uint16_t unYOffset = 120;

	PCGROUP_DATA pGrpData = GetActiveGroup();
	if (pGrpData)
	{
		m_grpData = *pGrpData;
	}
	else
	{
		memset(&m_grpData, 0, sizeof(GROUP_DATA));
		m_grpData.unArrayId = GetActiveArrayIdx(); // UI shows from 1
	}

	m_grpData.unArrayId++;

	m_pEditArrayNr		= CreatePageLine(MSG_AUTO_MODE_PROP_ARRAY_NR,	&s_validatorArrays,		m_grpData.unArrayId,	unYOffset);
	m_pEditFreqStart	= CreatePageLine(MSG_AUTO_MODE_PROP_F_START,	&s_validatorStdFreq,	m_grpData.dFreqStart,	unYOffset);
	m_pEditFreqStop		= CreatePageLine(MSG_AUTO_MODE_PROP_F_STOP,		&s_validatorStdFreq,	m_grpData.dFreqStop,	unYOffset);
	m_pEditFreqInc		= CreatePageLine(MSG_NONE,						NULL,					NULL,					unYOffset);
	m_pEditTime			= CreatePageLine(MSG_AUTO_MODE_PROP_DELAY,		&s_validatorTime,		m_grpData.unDelay,		unYOffset);
	m_pEditVoltage		= CreatePageLine(MSG_U,							&s_validatorStdVol,		m_grpData.dVoltage,		unYOffset);

	SyncGroupMode(m_eGroupType, true);

	ShowButtonsSet();

	if (!m_bCreateMode)
	{
		Button* pSaveBtn = GetFnButton(FNBTN_SAVE);
		pSaveBtn->SetVisible(false);
	}

	m_pLabelCaption = CreateLabel(AMODE_PAGE_LEFT_OFFSET, AMODE_PAGE_CAPTION_TOP_OFFSET, m_bCreateMode ? MSG_AUTO_MODE_CAPTION_NEW_GROUP : MSG_AUTO_MODE_CAPTION_GROUP, AMODE_PAGE_MAX_CONTENT);
	m_pLabelCaption->SetFont(FONT_CAPTION);
	m_pLabelCaption->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY);

	m_pLabelSelection = CreateLabel(AMODE_PAGE_LEFT_OFFSET, AMODE_PAGE_INFO_TOP_OFFSET, MSG_NONE, AMODE_PAGE_MAX_CONTENT);
	m_pLabelSelection->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY);
	if (m_bCreateMode)
	{
		m_pLabelSelection->SetValue(GetLangString(MSG_AUTO_MODE_INFO_NEW_GROUP));
	}
	else
	{
		char pszBuff[100] = {0};
		GetSelectionText(pszBuff);
		if (*pszBuff)
		{
			m_pLabelSelection->SetValue(pszBuff, true);
		}
	}

	// Cannot avoid OnChange(true) when validator normalizes value.
	// Set page to initial not modified state.
	OnChange(false);
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool EntryConfig::OnClick(FNBTN enumBtn, void* pContext)
{
	if (NULL == pContext)
	{
		if (FNBTN_SAVE == enumBtn)
		{
			bool bArrayHasChanged = m_grpData.unArrayId != m_pEditArrayNr->GetDValue();

			m_grpData.unArrayId				= (int16_t)m_pEditArrayNr->GetDValue() - 1; // UI shows from 1
			m_grpData.unFreqIncFuncIdx		= m_eGroupType;
			m_grpData.dFreqStart			= m_pEditFreqStart->GetDValue();
			m_grpData.dFreqStop				= m_pEditFreqStop->GetDValue();
			m_grpData.dFreqInc				= m_pEditFreqInc->GetDValue();
			m_grpData.unDelay				= (uint32_t)m_pEditTime->GetDValue();
			m_grpData.dVoltage				= m_pEditVoltage->GetDValue();

			int16_t uNewMemIdx; 
			int16_t uNewIdx = SaveActiveGroup(&m_grpData, m_bCreateMode, uNewMemIdx);
			if (-1 != uNewMemIdx)
			{
				if (-1 != uNewIdx && !bArrayHasChanged)
				{
					if (m_bCreateMode)
					{
						// Array index has been changed open ArraysManager
						OPEN_PAGE_WITH_ACTION(GroupsManager, PAGE_GROUPS_MGR);
					}
					else
					{
						SetActiveGroupIdx(uNewIdx);
						OPEN_PAGE_WITH_ACTION(EntryConfig, PAGE_ENTRY_CONFIG);
					}
				}
				else 
				{
					// Array index has been changed open ArraysManager
					OPEN_PAGE_WITH_ACTION(ArraysManager, PAGE_ARRAYS_MGR);
				}
			}
		}
		else if (FNBTN_TO_LIN == enumBtn || FNBTN_TO_LOG == enumBtn)
		{
			m_eGroupType = FNBTN_TO_LIN == enumBtn ? GROUPTYPE_LIN : GROUPTYPE_LOG;
			SyncGroupMode(m_eGroupType);
			ShowButtonsSet();
		}
		else if (FNBTN_EXIT == enumBtn && (!m_bIsModified || m_bCanLoseChanges))
		{
			OPEN_PAGE_WITH_ACTION(PlayMode, PAGE_AUTO_MODE);
		}
		else if (!m_bIsModified)
		{
			switch (enumBtn)
			{
				case FNBTN_NEXT:
				{
					int16_t unIdx = GetActiveGroupIdx();
					if (unIdx < GetGroupsCount() - 1)
					{
						SetActiveGroupIdx(unIdx + 1);
						OPEN_PAGE_WITH_ACTION(EntryConfig, PAGE_ENTRY_CONFIG);
					}
					break;
				}
				case FNBTN_PREV:
				{
					int16_t unIdx = GetActiveGroupIdx();
					if (unIdx > 0)
					{
						SetActiveGroupIdx(unIdx - 1);
						OPEN_PAGE_WITH_ACTION(EntryConfig, PAGE_ENTRY_CONFIG);
					}

					break;
				}
				case FNBTN_ARRAYS:
				{
					OPEN_PAGE_WITH_ACTION(ArraysManager, PAGE_ARRAYS_MGR);
					break;
				}
				case FNBTN_GROUPS:
				{
					OPEN_PAGE_WITH_ACTION(GroupsManager, PAGE_GROUPS_MGR);
					break;
				}
				case FNBTN_START_STOP:
				{
					PlayMode* pNewPage = NULL;
					CEATE_PAGE_WITH_ACTION(PlayMode, PAGE_AUTO_MODE, pNewPage);
					pNewPage->SetAutoStart();
					g_pageManager.ShowPage(pNewPage);
					break;
				}
			}
		}
		else
		{
			ShowInfo(GetLangString(MSG_EXIT_CONFIRM));
			m_bCanLoseChanges = true;
		}
	}
	return true;
}

void EntryConfig::ShowButtonsSet()
{
	RegisterFnButton(FNBTN_ARRAYS,		GetLangString(MSG_ARRAYS),														this);	
	RegisterFnButton(FNBTN_GROUPS,		-1 == GetActiveArrayIdx() ? NULL : GetLangString(MSG_GROUPS),					this);

	RegisterFnButton(FNBTN_TO_LIN,		GROUPTYPE_LIN != m_eGroupType ? GetLangString(MSG_AUTO_MODE_FUNC_LIN) : NULL,	this);
	RegisterFnButton(FNBTN_TO_LOG,		GROUPTYPE_LIN == m_eGroupType ? GetLangString(MSG_AUTO_MODE_FUNC_LOG) : NULL,	this);

	if (!m_bCreateMode)
	{
		RegisterFnButton(FNBTN_PREV,		GetActiveGroupIdx() > 0? GetLangString(MSG_PREVIOUS) : NULL,					this);
		RegisterFnButton(FNBTN_NEXT,		GetActiveGroupIdx() < GetGroupsCount() - 1 ? GetLangString(MSG_NEXT) : NULL,	this);
	}

	RegisterFnButton(FNBTN_START_STOP,	m_bCreateMode ? NULL : GetLangString(MSG_START),								this);

	RegisterFnButton(FNBTN_SAVE,		GetLangString(MSG_SAVE),														this);
	RegisterFnButton(FNBTN_EXIT,		GetLangString(MSG_EXIT),														this);
}

void EntryConfig::GetSelectionText(char* pszBuff)
{
	GetSelectionTextBase(pszBuff);
}
