/*
 * ArraysManager.cpp
 *
 *  Created on: Feb 4, 2015
 *      Author: seregia
 */

#include "BaseManager.h"

#include "PlayMode.h"
#include <stdio.h>

#define UIBUTTON_X_POS 35
#define UIBUTTON_X_MAX AMODE_PAGE_MAX_CONTENT
#define UIBUTTON_SIZE_X 80
#define UIBUTTON_Y_POS 210
#define UIBUTTON_SIZE_Y 50
#define UIBUTTON_SPACING 10

#define UIBUTTON_ID_OFFSET 50

#define UIBUTTON_XINC (UIBUTTON_SIZE_X + UIBUTTON_SPACING)

void BaseMgrOnClick(Button* pBtn, void* pContext)
{
	((BaseManager*)pContext)->OnUIBtnClick(pBtn->GetId() - UIBUTTON_ID_OFFSET, false);
}

BaseManager::BaseManager() : WithActionStep(), Menu(),
	m_nUIBtnX(UIBUTTON_X_POS),
	m_nUIBtnY(UIBUTTON_Y_POS),
	m_uOldToggledBtn(-1),
	m_pLabelCaption(NULL),
	m_pLabelSelection(NULL)
{}

void BaseManager::AddUIButon(const char* pszFormat, int16_t nButtonIdx, bool bNotEmpty)
{
	Button* pFnButton = new Button(this, UIBUTTON_ID_OFFSET + nButtonIdx, m_nUIBtnX, m_nUIBtnY);
	RegisterControl(pFnButton);

	pFnButton->SetPosition(m_nUIBtnX, m_nUIBtnY);
	pFnButton->SetSize(UIBUTTON_SIZE_X, UIBUTTON_SIZE_Y);

	char szBuff[10];
	sprintf(szBuff, pszFormat, nButtonIdx + 1);
	pFnButton->SetValue(szBuff, true);
	pFnButton->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);
	pFnButton->SetOnClickEvent(::BaseMgrOnClick, this);
	pFnButton->SetTextColor(bNotEmpty ? 0 : 0xB0B0B0);

	m_nUIBtnX += UIBUTTON_XINC;

	if (UIBUTTON_XINC + m_nUIBtnX >= UIBUTTON_X_MAX)
	{
		m_nUIBtnY += UIBUTTON_SIZE_Y + UIBUTTON_SPACING;
		m_nUIBtnX = UIBUTTON_X_POS;
	}
}

bool BMOnBeforeActivate(WithNavigation* pEdit, bool bBecomeActive, void* pContext)
{
	BaseManager* pBaseManager = (BaseManager*)pContext;
	EditBoxDigit* pEditDig = (EditBoxDigit*)pEdit;
	pEditDig->SetDValue(pBaseManager->GetActiveBtnIdx() + 1, 2, 0); // +1 because UI shows from 1 instead 0

	return true;
}

bool BMFNOnValueChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext)
{
	BaseManager* pBaseManager = (BaseManager*)pContext;
	int32_t nValue = (int32_t)EditBoxDigit::ParseDValue(strNewVal.c_str()) - 1;

	return pBaseManager->OnValueChange(nValue);;
}

bool BaseManager::OnValueChange(int32_t nValue)
{
	if (nValue >= 0)
	{
		int16_t nBtnsCnt = GetButtonsCount();
		if (nValue < nBtnsCnt)
		{
			// Disable validation to avoid recusion
			m_pEditSelection->SetOnBeforeValueChange(NULL, NULL);
			OnUIBtnClick(nValue, true);
			m_pEditSelection->SetOnBeforeValueChange(BMFNOnValueChange, this);
		}
		
		// Almost free tetx but limits with extremly values. 
		// This approach is used because user can enter 23 if old value is x9 and max value 25. (x = 0 or 1)
		// Otherwise strict validator declines 29 and user can't change didit x to 2.
		return nValue < 99;
	}
	
	return false;
}

void BaseManager::OnUIBtnClick(uint16_t unBtnIdx, bool bFromEdit)
{
	SyncButtonToggle();
	if (!bFromEdit)
	{
		// Sync edit box
		BMOnBeforeActivate(m_pEditSelection, false, this);
	}
}

void BaseManager::OnInitPage()
{
	m_pLabelCaption = CreateLabel(AMODE_PAGE_LEFT_OFFSET, AMODE_PAGE_CAPTION_TOP_OFFSET, GetPageCaptionId(), AMODE_PAGE_MAX_CONTENT);
	m_pLabelCaption->SetFont(FONT_CAPTION);
	m_pLabelCaption->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY);

	m_pLabelSelection = CreateLabel(AMODE_PAGE_LEFT_OFFSET, AMODE_PAGE_INFO_TOP_OFFSET, MSG_NONE, AMODE_PAGE_MAX_CONTENT);
	m_pLabelSelection->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY);

	Label* pLabel = CreateLabel(AMODE_PAGE_LEFT_OFFSET, 150, MSG_ENTER_NR_OR_USE_UI_BTN, 570);
	pLabel->SetAlignment(AL_RIGHT_JUSTIFY_TEXT);

	m_pEditSelection = EditBox::CreateInstance<EditBoxDigit>(this, 0, 580, 150, 80, DEFAULT_CONTROL_HEIGHT, AL_NONE, 10);
	BMOnBeforeActivate(m_pEditSelection, false, this);
	m_pEditSelection->SetOnBeforeValueChange(BMFNOnValueChange, this);
	m_pEditSelection->SetOnBeforeActivate(BMOnBeforeActivate, this);

	SetActiveControl(RegisterControl(m_pEditSelection));

	Menu::OnInitPage();

	ShowButtonsSet();
	SyncButtonToggle();
}

void BaseManager::SyncButtonToggle()
{
	if (-1 != m_uOldToggledBtn)
	{
		Button* pButton = (Button*)FindControlByCtrlId(UIBUTTON_ID_OFFSET + m_uOldToggledBtn);
		pButton->SetToggled(false);
	}

	int16_t nBtnIdx = GetActiveBtnIdx();
	if (-1 != nBtnIdx && nBtnIdx < MAX_GROUPS_RER_PAGE_COUNT)
	{
		Button* pButton = (Button*)FindControlByCtrlId(UIBUTTON_ID_OFFSET + nBtnIdx);
		if (pButton)
		{
			pButton->SetToggled(true);
			m_uOldToggledBtn = GetActiveBtnIdx();
		}
	}

	ShowButtonsSet();

	char pszBuff[100] = {0};
	GetSelectionText(pszBuff);
	if (*pszBuff)
	{
		m_pLabelSelection->SetValue(pszBuff, true);
	}
}

/** **********************************************************************
 *
 * Implementation of IOnClickCompatible interface.
 * Function handles OnClick events from buttons
 *
 * @param enumBtn	action button which is pressed
 * @param pContext	optional data defined on registration phase
 *
 * @return return true if event is handled otherwise false
 *
 *********************************************************************** */
bool BaseManager::OnClick(FNBTN enumBtn, void* pContext)
{
	bool bHandled = false;
	if (NULL == pContext)
	{
		bHandled = true;

		switch (enumBtn)
		{
			case FNBTN_START_STOP:
			{
				PlayMode* pNewPage = NULL;
				CEATE_PAGE_WITH_ACTION(PlayMode, PAGE_AUTO_MODE, pNewPage);
				pNewPage->SetAutoStart();
				g_pageManager.ShowPage(pNewPage);
				break;
			}
			case FNBTN_EXIT:
			{
				OPEN_PAGE_WITH_ACTION(PlayMode, PAGE_AUTO_MODE);
				break;
			}
			default:
			{
				bHandled = false;
				break;
			}
		}
	}

	return bHandled;
}

/** **********************************************************************
 *
 * Shows functional buttons on this page
 *
 *********************************************************************** */
void BaseManager::ShowButtonsSet()
{
	RegisterFnButton(FNBTN_DELETE,		GetLangString(MSG_DELETE),		this);

	if (-1 == GetActiveGroupIdx())
	{
		RegisterFnButton(FNBTN_START_STOP,	NULL,						this);
	}
	else
	{
		RegisterFnButton(FNBTN_START_STOP,	GetLangString(MSG_START),	this);
	}
	RegisterFnButton(FNBTN_EXIT,		GetLangString(MSG_EXIT),		this);
}
