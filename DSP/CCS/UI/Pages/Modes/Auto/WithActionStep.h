/*
 * WithActionStep.h
 *
 *  Created on: Feb 4, 2015
 *      Author: seregia
 */

#ifndef WITHACTIONSTEP_H_
#define WITHACTIONSTEP_H_

#include "../../../../CommonTypes.h"
#include "AutoModeCommon.h"

#define FNBTN_ARRAYS		FNBTN_1
#define FNBTN_GROUPS		FNBTN_2
#define FNBTN_CONFIG		FNBTN_3
#define FNBTN_TO_LIN		FNBTN_3
#define FNBTN_TO_LOG		FNBTN_4
#define FNBTN_START_STOP	FNBTN_5
#define FNBTN_NEXT_ON_PLAY	FNBTN_6
#define FNBTN_NEW			FNBTN_8
#define FNBTN_DELETE		FNBTN_9

// 400 leads to memory problems
#define MAX_GROUPS_COUNT	200
#define MAX_ARRAYS_COUNT	21

#define AMODE_PAGE_LEFT_OFFSET			0
#define AMODE_PAGE_CAPTION_TOP_OFFSET	5
#define AMODE_PAGE_MAX_CONTENT			695
#define AMODE_PAGE_INFO_TOP_OFFSET		45

#define CEATE_PAGE_WITH_ACTION(PAGE_CLASS, PAGE_ID, PNAME) \
	PAGE_CLASS* PNAME_macro = (PAGE_CLASS*)g_pageManager.CreatePage(PAGE_ID);\
	PNAME_macro->SetActionStep(this);\
	PNAME = PNAME_macro;

#define OPEN_PAGE_WITH_ACTION(PAGE_CLASS, PAGE_ID) \
	PAGE_CLASS* pPAGE_ID = (PAGE_CLASS*)g_pageManager.CreatePage(PAGE_ID);\
	pPAGE_ID->SetActionStep(this);\
	g_pageManager.ShowPage(pPAGE_ID);

typedef enum { GROUPTYPE_LIN, GROUPTYPE_LOG } GROUPTYPE;

typedef struct tagGROUP_DATA
{
	double		dFreqStart;
	double		dFreqStop;
	double		dFreqInc;
	int16_t		unArrayId;
	uint8_t		unFreqIncFuncIdx;
	uint32_t	unDelay;
	double		dVoltage;
} GROUP_DATA, *PGROUP_DATA;

typedef const GROUP_DATA* PCGROUP_DATA;


class WithActionStep
{
public:
	WithActionStep();
	virtual ~WithActionStep();

	void SetActionStep(WithActionStep* pOldPageWithStep );
	void SetActionStepIdx(int16_t nArrayIdx, int16_t nGroupIdx);

	int16_t SaveActiveGroup(PCGROUP_DATA pGroup, bool bAddNew,  int16_t& nGroupIdxMem);
	PCGROUP_DATA GetActiveGroup();

protected:
	bool ActivateNextGroup();
	const int16_t* GetArrays();

	int16_t GetActiveArrayIdx() { return m_nArrayIdx; };
	void SetActiveArrayIdx(int16_t nArrayIdx);

	int16_t GetActiveGroupIdx() { return m_nArrGroupIdx; };
	void SetActiveGroupIdx(int16_t nGroupIdx);
	int16_t GetGroupsCount();

	void DeleteArray(int16_t nArrIdx);
	void DeleteGroup(int16_t nGrpIdx);

	bool MoveGroup(int16_t nGrpIdx, bool bNext);

	void GetSelectionTextBase(char* pszBuff);
	void ResetAllCaches();

private:
	void LoadGroupsData();
	uint32_t GrpIdxToAddr(int16_t nGroupIdx) const;
	int16_t FindsArrayIdxByAddress(int16_t nAllGroupIdx) const;
	int16_t LimitArrayId(int16_t unArrayId) const;
	void LoadArrays();
	void SaveGroup(PCGROUP_DATA pGroup, int16_t nGroupIdx);
	void SwapGroupInt(int16_t nGrpIdxSource, int16_t nGrpIdxTarget, bool bCopy = false);
	void DeleteGroupInt(int16_t nDelGroupIdx);

protected:
	uint32_t m_unNextStepTime;

private:
	int16_t* m_arrGroupsMemCache;
	int16_t* m_arrArraysCache;
	int16_t m_nArrayIdx;
	int16_t m_nArrGroupIdx;
	int16_t m_nArrGroupsCount;
	GROUP_DATA m_activeGroup;
};

#endif /* WITHACTIONSTEP_H_ */

