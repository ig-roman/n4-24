/*
 * RMode.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "RMode.h"
#include "../../../Logic/Calibration/CalibrationManager.h"
#include <float.h>

#define RBUTTON_X_POS 50
#define RBUTTON_SIZE_X 150
#define RBUTTON_SIZE_Y 50
#define RBUTTON_SPACING 10

#define RBUTTON_ID_OFFSET 50

typedef struct tagRBTNINFO
{
	const char* label;
	double val;
} RBTNINFO, *PRBTNINFO;

// ST TODO: Calibration logic has been moved to calibrationManager. These constants should be removed.
const RBTNINFO g_arrRBtnInfo[] =
{
	{"0 Ohm",	0			},
	{"1 Ohm",	1			},
	{"10 Ohm",	10			},
	{"100 Ohm", 100			},
	{"1 kOhm",	1000		},
	{"10 kOhm",	10000		},
	{"100 kOhm",100000		},
	{"1 MOhm",	1000000		},
	{"10 MOhm",	10000000	},
	{"100 MOhm",100000000	},
	NULL
};

RMode::RMode() : BaseModePage(),
	m_uOldToggledBtn(-1)
{}

bool OnRBeforeValueChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext)
{
	return false;
}

void OnRBtnClick(Button* pBtn, void* pContext)
{
	((RMode*)pContext)->OnRBtnClick(pBtn->GetId() - RBUTTON_ID_OFFSET);
}

void RMode::OnRBtnClick(uint8_t unBtnIdx)
{
	g_deviceState.SetResistance(g_arrRBtnInfo[unBtnIdx].val);
	SyncValue();
}

/** **********************************************************************
 *
 * Adapter function for OnBeforeByCharInputChange method
 *
 *********************************************************************** */
bool OnABeforeByCharInputChange(EditBoxBand* pEdit, bool bSet, bool bParentFocusChage, const char* pszVal, void* pContext)
{
	bool bRet = true;
	RMode* pThis = (RMode*)pContext;
	if (!bParentFocusChage)
	{
		bRet = pThis->OnBeforeByCharInputChange(pEdit, bSet, bParentFocusChage, pszVal);
	}

	return bRet;
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void RMode::OnInitPage()
{
	BaseModePage::OnInitPage();

	m_pLabelSense = CreateLabel(210, 30, MSG_INT_FEEDBACK, 480);
	m_pLabelSense->SetFont(FONT_DEFAULT);
	m_pUTypeLabel->SetValue(GetLangString(MSG_R));

	EditBoxDigit* pEditEditSlave = new EditBoxDigit(this, ID_CTRL_SLAVE_RESISTANCE, 0, 0);
	m_pEditValue = EditBox::CreateInstance<EditBoxBand>(this, ID_CTRL_VALUE, 0, 125, 695, 100, AL_RIGHT_JUSTIFY_TEXT, 11);
	m_pEditValue->SetSlaveCtrl(pEditEditSlave);
	m_pEditValue->SetFont(FONT_BIG_DIGITS);
	m_pEditValue->SetOnBeforeByCharInputChange(::OnABeforeByCharInputChange, this);
	m_pEditValue->SetOnBeforeValueChange(::OnRBeforeValueChange, this);
	m_pEditValue->SetOnBeforeActivate(::OnBeforeEditActivate, this);
	m_pEditValue->SetOnCursorLimit(::OnBMCursorLimit, this);

	ShowButtonsSet(BUTTONSET_DEFAULT);

	uint16_t nX = RBUTTON_X_POS;
	uint16_t nY = 230;

	uint16_t nXIncr = RBUTTON_SIZE_X + RBUTTON_SPACING;

	for (int i = 0; g_arrRBtnInfo[i].label; i++)
	{
		Button* pFnButton = new Button(this, RBUTTON_ID_OFFSET + i, nX, nY);
		RegisterControl(pFnButton);

		pFnButton->SetPosition(nX, nY);
		pFnButton->SetSize(RBUTTON_SIZE_X, RBUTTON_SIZE_Y);
		pFnButton->SetValue(g_arrRBtnInfo[i].label);
		pFnButton->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY | AL_CENTER_TEXT_VERTICALLY);
		pFnButton->SetOnClickEvent(::OnRBtnClick, this);

		nX += nXIncr;

		if (3 == i)
		{
			nY += RBUTTON_SIZE_Y + RBUTTON_SPACING;
			nX = RBUTTON_X_POS;
		}

		if (7 == i)
		{
			nY += RBUTTON_SIZE_Y + RBUTTON_SPACING;
			nX = RBUTTON_X_POS + nXIncr;
		}
	}

	SyncState(true);
	SyncSense();

	SetActiveControl(RegisterControl(m_pEditValue));

	// This is popup edit box and should be displayed over pahe controls. The last registered control has the most z index
	RegisterControl(pEditEditSlave);
}

void RMode::SyncState(bool bForce)
{
	Button* pButtonR = GetFnButton(FNBTN_R);
	pButtonR->SetToggled();

	SyncValue();

	BaseModePage::SyncState(bForce);
}

/** **********************************************************************
 *
 * Synchronizes one of pedefined "R" buttons with device state value
 * @param wu actual device state value
 *
 *********************************************************************** */
void RMode::SyncSelectedRBtn(WORKUNIT& wu)
{	
	int8_t nNewToggledBtn = -1;
	double dBestErrorPers = DBL_MAX;
	for (int i = 0; g_arrRBtnInfo[i].label; i++)
	{
		double dDiff = g_arrRBtnInfo[i].val - wu.value;
		dDiff = dDiff < 0 ? -dDiff : dDiff;

		double dErrorPers = dDiff < 0.1 ? 0 : 100 * dDiff / wu.value;
		if (dErrorPers < dBestErrorPers)
		{
			dBestErrorPers = dErrorPers;
			nNewToggledBtn = i;
		}
	}

	if (-1 != nNewToggledBtn)
	{
		if (-1 != m_uOldToggledBtn)
		{
			Button* pButton = (Button*)FindControlByCtrlId(RBUTTON_ID_OFFSET + m_uOldToggledBtn);
			pButton->SetToggled(false);
		}

		Button* pButton = (Button*)FindControlByCtrlId(RBUTTON_ID_OFFSET + nNewToggledBtn);
		pButton->SetToggled(true);
		m_uOldToggledBtn = nNewToggledBtn;
	}
}

bool RMode::OnClick(FNBTN enumBtn, void* pContext)
{
	if (FNBTN_SENSE == enumBtn)
	{
		Button* pSeneseBtn = GetFnButton(enumBtn);
		OnSenseBtnClicked(enumBtn, pSeneseBtn);
		SyncValue();
		return true;
	}

	return BaseModePage::OnClick(enumBtn, pContext);
}
/** **********************************************************************
 *
 * Shows functional buttons on this page
 * @param enumSet set of buttons to show
 *
 *********************************************************************** */
void RMode::ShowButtonsSet(BUTTONSET enumSet)
{
	switch (enumSet)
	{
		case BUTTONSET_NONE: break;
		case BUTTONSET_DEFAULT:
		{
			RegisterFnButton(FNBTN_1);
			RegisterFnButton(FNBTN_2);
			RegisterFnButton(FNBTN_3);
			RegisterFnButton(FNBTN_4);
			RegisterFnButton(FNBTN_5);
			RegisterFnButton(FNBTN_SENSE,		GetLangString(MSG_FEEDBACK),	this, this);
			RegisterFnButton(FNBTN_R,			GetLangString(MSG_R));

			break;
		}
		case BUTTONSET_R_BAND:
		{
			RegisterFnButton(FNBTN_OHM,			GetLangString(MSG_OHM),			this, this);
			RegisterFnButton(FNBTN_KOHM,		GetLangString(MSG_KOHM),		this, this);
			RegisterFnButton(FNBTN_MOHM,		GetLangString(MSG_MOHM),		this, this);

			break;
		}
	}
}

int8_t RMode::GetFnBandButtonsOffset(uint8_t unCtrlId)
{
	int8_t nRet = -1;

	switch (unCtrlId)
	{
		case ID_CTRL_RESISTANCE:	nRet = FNBTN_R_OFFSET;	break;
	}
	return nRet;
}

// ST TODO: remove  function and use STATEMODE from dev state
double RMode::GetMultiplier(WORKUNIT& wu, uint16_t* punMesgId)
{
	return ::GetMultiplier(wu.pBand, punMesgId, STATEMODE_R);
}

BUTTONSET RMode::GetButtonSetByCtrlId(uint8_t unCtrlId)
{
	BUTTONSET enumSet;
	switch (unCtrlId)
	{
		case ID_CTRL_SLAVE_RESISTANCE:		enumSet = BUTTONSET_R_BAND;			break;
		default: enumSet = BUTTONSET_NONE;
	}
	return enumSet;
}

bool RMode::OnCursorLimit(bool bRightBound)
{
	WORKUNIT wu = {0};
	g_deviceState.GetValue(wu);

	if (wu.value < 0.5 && !bRightBound)
	{
		// Custom logic for zero resitor
		g_deviceState.SetResistance(g_arrRBtnInfo[1].val);
	}
	else
	{
		g_deviceState.SetResistance(wu.value *= bRightBound ? 0.1 : 10);
	}
	SyncValue();
	return false;
}

bool RMode::OnBeforeByCharInputChange(EditBoxBand* pEdit, bool bSet, bool bParentFocusChage, const char* pszVal)
{
	bool bRet = true;

	if (bSet)
	{
		m_bMultiplyerSelectedByFnButton = false;
	}
	else
	{
		// Set value in same band
		double dVal = EditBoxDigit::ParseDValue(pszVal);

		if (!m_bMultiplyerSelectedByFnButton)
		{
			// Use already selected band if enter button is presed
			dVal /= pEdit->GetMultiplier();
		}

		bRet = STATEERR_NO_ERROR == g_deviceState.SetResistance(dVal);

		if (bRet)
		{
			SyncState();
		}
		else
		{
			ReportMessage(GetLangString(MSG_OUT_OF_RANGE));
		}
	}

	return bRet;
}

void RMode::SyncValue()
{
	STATESENSE sense;
	g_deviceState.GetSenseMode(sense);

	WORKUNIT wu = {0};
	g_deviceState.GetValue(wu);

	uint16_t unMesgId;
	double dMultiplier = GetMultiplier(wu, &unMesgId);
	m_pEditValue->SetOnBeforeValueChange(NULL, NULL);

	wu.value = gs_calibrationManager.GetCorrectedValue(MODE_R, wu, 0, sense);
	m_pEditValue->SetUnit(wu, dMultiplier);

	m_pEditValue->SetUnitsText(GetLangString(unMesgId), STYLE_UNITS_SIZE_BIG_CTRLS);
	m_pEditValue->m_unEditLen = 9; // ST TODO: REMOVE IT
	m_pEditValue->SetOnBeforeValueChange(::OnRBeforeValueChange, this);

	SyncSelectedRBtn(wu);
}
