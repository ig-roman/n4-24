/*
 * IMode.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "IMode.h"
#include "../../Controls/Label.h"
#include "../../../Logic/Calibration/CalibrationManager.h"


// ST TODO: remove  function and use STATEMODE from dev state
double IMode::GetMultiplier(WORKUNIT& wu, uint16_t* punMesgId)
{
	return ::GetMultiplier(wu.pBand, punMesgId, STATEMODE_I);
}

/** **********************************************************************
 *
 * Shows functional buttons on this page
 * @param enumSet set of buttons to show
 *
 *********************************************************************** */
void IMode::ShowButtonsSet(BUTTONSET enumSet)
{
	switch (enumSet)
	{
		case BUTTONSET_DEFAULT:
		{
			RegisterFnButton(FNBTN_SENSE);
			RegisterFnButton(FNBTN_4);
			break;
		}
		case BUTTONSET_U_BAND:
		{
			RegisterFnButton(FNBTN_1);
			RegisterFnButton(FNBTN_2);
			RegisterFnButton(FNBTN_UV, GetLangString(MSG_UA), this, this);
			RegisterFnButton(FNBTN_MV, GetLangString(MSG_MA), this, this);
			RegisterFnButton(FNBTN_V, GetLangString(MSG_A), this, this);

			break;
		}
		case BUTTONSET_U_DEFAULT:
		{
			RegisterFnButton(FNBTN_MUL, GetLangString(MSG_MUL10), this);
			RegisterFnButton(FNBTN_DIV, GetLangString(MSG_DEV10), this);
			if (GetIsDCOperationMode())
			{
				RegisterFnButton(FNBTN_INVERT_SIGN, GetLangString(MSG_INVERT_SIGN), this);
			}

			break;
		}
	}

	WithFrequencyPage::ShowButtonsSet(enumSet);

	if (BUTTONSET_FREQ_BAND == enumSet)
	{
		// Hide Mhz key for ACI. There is 10kHz max
		RegisterFnButton(FNBTN_MHZ);
	}
}

/** **********************************************************************
 *
 * Shows functional buttons on this page
 * @param pEdit set of buttons to show
 *
 *********************************************************************** */
void IMode::UpdateValueDependedCtrls(EditBoxBand* pEdit)
{
	WithFrequencyPage::UpdateValueDependedCtrls(pEdit);
	
	// Notify user to connect or disconnect current amplifier
	PWORKUNIT pWu = pEdit->GetUnit();
	bool bIsAmpBand = ::IsAmpBand(ConvertMode(STATEMODE_I, GetIsDCOperationMode()), pWu->pBand->bandIdx);
	if (m_bIsAmpEnabled != bIsAmpBand)
	{
		m_bIsAmpEnabled = bIsAmpBand;
		ShowInfo(GetLangString(m_bIsAmpEnabled ? MSG_CONNECT_CURRENT_AMP : MSG_DISCONNECT_CURRENT_AMP));
	}
/*	if (m_bIsAmpEnabled) {
		RegisterFnButton(FNBTN_ZERO, NULL, PAGE_EMPTY);
	} */
}
