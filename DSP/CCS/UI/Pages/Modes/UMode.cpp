/*
 * UMode.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "UMode.h"
#include "../../Controls/Label.h"
#include "../../PageManager.h"
#include "Auto/PlayMode.h"

#define OUTPUT_BLINK_TIME 2000

bool OnBeforeVoltageChange(EditBox* pEdit, string& strOldVal, string& strNewVal, void* pContext);

void OnDividerBtnClicked(FNBTN enumBtn, Button* pButton, void* pContext)
{
	UMode* pThis = (UMode*)pContext;
	pThis->OnDividerBtnClicked(enumBtn, pButton);
}

void OnSenseBtnClicked(FNBTN enumBtn, Button* pButton, void* pContext)
{
	UMode* pThis = (UMode*)pContext;
	pThis->OnSenseBtnClicked(enumBtn, pButton);
}

UMode::UMode() : WithFrequencyPage(),
	m_pOutHVLabel(NULL),
	m_bHV(false),
	m_BlinkTime(0),
	m_bHVConfirmShown(false),
	m_bOldOutOn(false),
	m_extDivider(false),
	m_curButtonset(BUTTONSET_DEFAULT)
{
}

/** **********************************************************************
 *
 * This method is invoked once on page creation phase.
 * It is responsible for creation of UI controls
 *
 *********************************************************************** */
void UMode::OnInitPage()
{
	m_pLabelSense = CreateLabel(210, 30, MSG_INT_FEEDBACK, 175);
	m_pLabelSense->SetFont(FONT_DEFAULT);

	m_pOutHVLabel = CreateLabel(170, 45, MSG_LV, 40, 39);
	m_pOutHVLabel->SetAlignment(AL_CENTER_TEXT_HORIZONTALLY);

	// Contains SyncState() call and SyncState is used m_pLabelSense;
	WithFrequencyPage::OnInitPage();
}

/** **********************************************************************
 *
 * Shows functional buttons on this page
 * @param enumSet set of buttons to show
 *
 *********************************************************************** */
void UMode::ShowButtonsSet(BUTTONSET enumSet)
{
	switch (enumSet)
	{
		case BUTTONSET_DEFAULT:
		{
			RegisterFnButton(FNBTN_SENSE, GetLangString(MSG_FEEDBACK), PAGE_EMPTY, ::OnSenseBtnClicked, this);
			Button *btnDiv = RegisterFnButton(FNBTN_DIVIDER, GetLangString(MSG_DIVIDER), PAGE_EMPTY, ::OnDividerBtnClicked, this);
			btnDiv->SetVisible(g_deviceState.IsExtDividerPossible());
			btnDiv->SetToggled(g_deviceState.GetDividerEnabled());
			RegisterFnButton(FNBTN_S_AUTO,		GetLangString(MSG_S_AUTO),			PAGE_ARRAYS_MGR);
			break;
		}
		case BUTTONSET_U_BAND:
		{
			RegisterFnButton(FNBTN_1,			NULL,							PAGE_EMPTY);
			RegisterFnButton(FNBTN_2,			NULL,							PAGE_EMPTY);
			RegisterFnButton(FNBTN_UV,			GetLangString(MSG_UV),			this, this);
			RegisterFnButton(FNBTN_MV,			GetLangString(MSG_MV),			this, this);
			GetFnButton(FNBTN_MV)->SetToggled(false);
			RegisterFnButton(FNBTN_V,			GetLangString(MSG_V),			this, this);
			RegisterFnButton(FNBTN_KV,			GetLangString(MSG_KV),			this, this);
			GetFnButton(FNBTN_KV)->SetToggled(false);

			break;
		}
		case BUTTONSET_U_DEFAULT:
		{
			RegisterFnButton(FNBTN_MUL,	GetLangString(MSG_MUL10),		this);
			RegisterFnButton(FNBTN_DIV,	GetLangString(MSG_DEV10),		this);
			if (GetIsDCOperationMode())
			{
				RegisterFnButton(FNBTN_INVERT_SIGN,	GetLangString(MSG_INVERT_SIGN),	this);
			}

			break;
		}
	}
	m_curButtonset = enumSet;
	WithFrequencyPage::ShowButtonsSet(enumSet);
}

// ST TODO: remove  function and use STATEMODE from dev state
double UMode::GetMultiplier(WORKUNIT& wu, uint16_t* punMesgId)
{
	return ::GetMultiplier(wu.pBand, punMesgId, STATEMODE_U);
}

void UMode::SyncState(bool bForce)
{
	WithFrequencyPage::SyncState(bForce);
	SyncDivider();
	SyncSense();
}

void UMode::UpdateValueDependedCtrls(EditBoxBand* pEdit)
{
	WithFrequencyPage::UpdateValueDependedCtrls(pEdit);
	SyncDivider();
	SyncSense();
}

void UMode::OnDividerBtnClicked(FNBTN enumBtn, Button* pButton)
{
	m_extDivider = !g_deviceState.GetDividerEnabled();
	ShowInfo(GetLangString(m_extDivider ? MSG_CONNECT_EXT_DIV : MSG_DISCONNECT_EXT_DIV));
	pButton->SetToggled(m_extDivider);
	STATEERR ret = g_deviceState.ChangeDivider();
	SyncValue();
	g_deviceState.ResetDelta(false);
	SyncDelta();
	SyncSense();
}

void UMode::SyncDivider()
{
	if (g_deviceState.IsExtDividerPossible()) {
		Button *btnDiv = GetFnButton(FNBTN_DIVIDER);
		btnDiv->SetVisible(true);
		btnDiv->SetToggled(m_extDivider);
		if (m_extDivider != g_deviceState.GetDividerEnabled()) {
			ShowInfo(GetLangString(m_extDivider ? MSG_CONNECT_EXT_DIV : MSG_DISCONNECT_EXT_DIV));
			g_deviceState.ChangeDivider();
		}
	} else {
		GetFnButton(FNBTN_DIVIDER)->SetVisible(false);
		if (m_extDivider) {
			ShowInfo(GetLangString(MSG_DISCONNECT_EXT_DIV));
			m_extDivider = false;
		}
	}
}

void UMode::SyncOut()
{
	WithFrequencyPage::SyncOut();
	bool bIsDC;
	g_deviceState.GetDirecionType(bIsDC);
	if (bIsDC)
	{
		WORKUNIT valueRet;
		g_deviceState.GetOutputValue(valueRet);
		if (valueRet.pBand->bandIdx == EXBAND_DCU_N_1000_V || valueRet.pBand->bandIdx == EXBAND_DCU_P_1000_V)
			RegisterFnButton(FNBTN_ZERO, NULL, PAGE_EMPTY);
		else
			RegisterZeroBtn();
	}

	bool bOn = g_deviceState.GetSwitchOutState();
	bool bIsConfirmRequire = g_deviceState.GetConfirmRequire();

	STATEVOLINFO volInfo;
	g_deviceState.GetVoltageInfo(&volInfo);
	m_bHV = STATEVOLINFO_HV == volInfo;
	
	m_pOutHVLabel->SetValue(GetLangString(STATEVOLINFO_HV == volInfo ? MSG_HV : MSG_LV));
	m_pOutHVLabel->SetColor(m_bHV && bOn && !bIsConfirmRequire ? COLOR_OUT_HV : COLOR_OUT_LV);
	m_pOutHVLabel->SetInvalid(true);
	
	if (m_bOldOutOn != bOn)
	{
		if (bOn)
		{
			// Shows message when out on button is pressed.
			m_bHVConfirmShown = false;
		}
		m_bOldOutOn = bOn;
	}

	if (!m_bHV)
	{
		m_pOutputLabel->SetVisible();
		m_BlinkTime = 0;
		m_bHVConfirmShown = false;
	}

	if (!m_bHVConfirmShown && m_bHV && bIsConfirmRequire)
	{
		ReportMessage(GetLangString(MSG_CONFIRM_OUT_PUT));
		m_bHVConfirmShown = true;
	}
}

/** **********************************************************************
 *
 * The periodic function. Is invoked always.
 * Use CheckUpdateTime() function for custom period.
 *
 *********************************************************************** */
void UMode::Loop()
{
	// ST TODO: uncomment and create keyboard buffer after TIVA migration has been done
	// Keyboard works badly if display redraws always this label
/*
	if (m_bHV)
	{

		if (CheckUpdateTime(OUTPUT_BLINK_TIME, &m_BlinkTime))
		{
			STATEVOLINFO volInfo;
			g_deviceState.GetVoltageInfo(&volInfo);

			m_pOutputLabel->SetVisible(!m_pOutputLabel->GetVisible());
		}
	}
*/
	WithFrequencyPage::Loop();
}
