/* PageManager.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef PAGEMANAGER_H_
#define PAGEMANAGER_H_

#include "Common.h"
#include "DrawContext.h"
#include "Pages/BasePage.h"
#include "PageFactory.h"


/** **********************************************************************
 *
 * Class responsible for the creation, changing and deletion of UI pages,
 * also for forwarding touch and keyboard events.
 *
 *********************************************************************** */
class PageManager
{
public:
	PageManager();
	virtual ~PageManager();

	BasePage* CreatePage(PAGE);
	void OnButtonChanged(int8_t chButtonCode, bool bIsPressed);
	void TouchEvent(uint16_t unX, uint16_t unY, char bIsTouched);

	void ShowPage(BasePage* pPage);
	void ChangePage(PAGE);
	BasePage* GetActivePage() { return m_pActivePage; };
	void Loop();
	void SetNewPage(BasePage* newPage) { m_pNewPage = newPage; };
	BasePage* NeedPageChange() { return m_pNewPage; };

	void ShowError(const char* pszError) const;
	void HideFrames();

private:
	bool ValidateCtrlMode() const;
	void DeleteActivePage();

private:
	BasePage* m_pActivePage;
	BasePage* m_pNewPage;
	PageFactory m_pageFactory;
	DrawContext m_DC;
	bool m_bDCInitialized;
	BasePage* m_pOldActivePage;
};

extern PageManager g_pageManager;

#endif /* PAGEMANAGER_H_ */
