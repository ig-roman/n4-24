/*
 * DrawContext.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef DRAWCONTEXT_H_
#define DRAWCONTEXT_H_

#include "Common.h"


/** **********************************************************************
 *
 * Interface for UI drawing and manipulation.
 *
 *********************************************************************** */
class DrawContext 
{
public:
	DrawContext() {};
	virtual ~DrawContext() {};

	virtual void Init();

	virtual void EraseDisplayArea(uint16_t unDX, uint16_t unDY);
	virtual void SetBacklightMode(uint8_t unVal);
	virtual void SetBacklightIntensity(uint8_t unVal);

	virtual void CopyBuffToMain();
	virtual void SelectViewPort(uint8_t unPortNumber);
	virtual void SetAbsoluteCursorPosition(uint16_t unX, uint16_t unY); // \iCK\D0\D0
	virtual void SetCursorPosition(uint16_t unX, uint16_t unY);
	virtual void DefineViewport(uint8_t unPortNumber, uint16_t unX, uint16_t unY, uint16_t unDX, uint16_t unDY);
	virtual void InvertDisplayArea(uint16_t unDX, uint16_t unDY);

	virtual void SetTextAlignment(uint8_t unFlags);
	virtual void SetFont(uint16_t unFontId); // \iAF\D0
	virtual void WriteText(const char*); // \iDTHello World!\0
	virtual void SetColor(uint32_t unColor);
	virtual void SetBackgroundColor(uint32_t unColor);

	virtual void DrawRectangle(uint8_t unFlags, uint16_t unX, uint16_t unY, uint16_t unDX, uint16_t unDY);

	virtual void GetTextExtent(const char* pszText, uint16_t* punX, uint16_t* punY);

private:
	void WaitACK();
	void SendWord(uint16_t);
	void SendByte(uint8_t);
	void SendText(const char* pszText);
	void SendColor(uint32_t unFontId);
	void SendDisplayData(const char* pBuf, int length);
	void StartCommand();

	// Optimization
	void ResetCache();
	uint8_t m_unOldTextAlignFlags;
	uint16_t m_unOldFontId;
	uint32_t m_unOldColor;
	uint32_t m_unOldBkgColor;
};

#endif /* DRAWCONTEXT_H_ */
