/*
 * WithNavigation.h
 *
 *  Created on: Oct 29, 2013
 *      Author: Sergei Tero
 */

#ifndef IPARENT_H_
#define IPARENT_H_

#include "Common.h"

/** **********************************************************************
 *
 * Parent object for controls
 *
 *********************************************************************** */
class IParent
{
public:
	virtual uint32_t GetBackgroundColor() const = 0;
	virtual void HasInvalidChild() = 0;
};

#endif /* IPARENT_H_ */
