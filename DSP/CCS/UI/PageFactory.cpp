/*
 * PageFactory.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "../UIInterface.h"
#include "PageFactory.h"
#include "Pages/StartupPage.h"
#include "Pages/SelectModePage.h"
#include "Pages/Settings/ConfigurationPage.h"
#include "Pages/Settings/DisplayPage.h"
#include "Pages/Settings/AdvancedSettingsPage.h"
#include "Pages/Settings/SelectLanguagePage.h"
#include "Pages/Settings/RemoteCtrlPage.h"
#include "Pages/Settings/LanConfigPage.h"
#include "Pages/Settings/GPIBConfigPage.h"
#include "Pages/Settings/RS232ConfigPage.h"
#include "Pages/Settings/HVConfigPage.h"
#include "Pages/Settings/ChangePasswordPage.h"
#include "Pages/Settings/LabParamsInfoPage.h"
#include "Pages/Settings/FreqCorrPage.h"
#include "Pages/Modes/UMode.h"
#include "Pages/Modes/IMode.h"
#include "Pages/Modes/RMode.h"
#include "Pages/Settings/Calibration/LabParamsPage.h"
#include "Pages/Settings/Calibration/SelectBandPage.h"
#include "Pages/Settings/Calibration/AdjustLevelsPage.h"
#include "Pages/Settings/Calibration/CalibrationModePage.h"
#include "Pages/Settings/Calibration/ResistorsCalibrationPage.h"
#include "Pages/Settings/Calibration/ResistorsHelpPage.h"
#include "Pages/Settings/Calibration/ExCalibration/BandMinMaxLevelsExPage.h"
#include "Pages/Settings/Calibration/ExCalibration/ShuntsHelpPage.h"
#include "Pages/Modes/Auto/GroupsManager.h"
#include "Pages/Modes/Auto/ArraysManager.h"
#include "Pages/Modes/Auto/EntryConfig.h"
#include "Pages/Modes/Auto/PlayMode.h"

/** **********************************************************************
 *
 * Creates UI pages
 * @param enumPage name of page
 * @return new page. Caller must delete this page from heap.
 *
 *********************************************************************** */
BasePage* PageFactory::CreatePage(PAGE enumPage)
{
	BasePage* pRet = NULL;
	switch (enumPage)
	{
		//			 Id of page						Class name
		//----------+-------------------------------+---------------------------+
		DECLARE_PAGE(PAGE_STARTUP,					StartupPage					)
		DECLARE_PAGE(PAGE_SELECT_MODE,				SelectModePage				)
		DECLARE_PAGE(PAGE_MODE_U,					UMode						)
		DECLARE_PAGE(PAGE_MODE_I,					IMode						)
		DECLARE_PAGE(PAGE_MODE_R,					RMode						)

		// Configuration
		DECLARE_PAGE(PAGE_CONFIGURATION,			ConfigurationPage			)
		DECLARE_PAGE(PAGE_SETTINGS_DISPLAY,			DisplayPage					)
		DECLARE_PAGE(PAGE_SETTINGS_ADVANCED,		AdvancedSettingsPage		)
		DECLARE_PAGE(PAGE_SETTINGS_LANGUAGE,		SelectLanguagePage			)
		DECLARE_PAGE(PAGE_REMOTE_CTRL,				RemoteCtrlPage				)
		DECLARE_PAGE(PAGE_FREQ_CORR,				FreqCorrPage                )
		DECLARE_PAGE(PAGE_LAN_CONFIG,				LanConfigPage				)
		DECLARE_PAGE(PAGE_GPIB_CONFIG,				GPIBConfigPage				)
		DECLARE_PAGE(PAGE_RS232_CONFIG,				RS232ConfigPage				)
		DECLARE_PAGE(PAGE_HV_CONFIG,				HVConfigPage				)
		DECLARE_PAGE(PAGE_CHANGE_PSW_CONFIG,		ChangePasswordPage			)
		DECLARE_PAGE(PAGE_LAB_PARAMS_INFO_CONFIG,	LabParamsInfoPage			)

		// Calibration
		DECLARE_PAGE(PAGE_LAB_PARAMS_CONFIG,		LabParamsPage				)
		DECLARE_PAGE(PAGE_SELECT_BAND,				SelectBandPage				)
		DECLARE_PAGE(PAGE_ADJUST_LEVELS,			AdjustLevelsPage			)
		DECLARE_PAGE(PAGE_CALIBRATION_MODE,			CalibrationModePage			)
		DECLARE_PAGE(PAGE_RESISTORS_HELP,			ResistorsHelpPage			)
		DECLARE_PAGE(PAGE_RESISTORS_CALIBRATION,	ResistorsCalibrationPage	)

		DECLARE_PAGE(PAGE_BAND_MIN_MAX_LEVELS_EX,	BandMinMaxLevelsExPage		)
		DECLARE_PAGE(PAGE_SHUNTS_HELP_EX,			ShuntsHelpPage				)

		DECLARE_PAGE(PAGE_ARRAYS_MGR,				ArraysManager				)
		DECLARE_PAGE(PAGE_GROUPS_MGR,				GroupsManager				)
		DECLARE_PAGE(PAGE_ENTRY_CONFIG,				EntryConfig					)
		DECLARE_PAGE(PAGE_AUTO_MODE,				PlayMode					)
		//----------+-------------------------------+---------------------------+
	}

	if (pRet)
	{
		// Implemented to locate invalid pages.
		if (pRet->GetPageId() != enumPage)
		{
			pRet = NULL;
			CUI_SetLastError("CreatePage() - Page creation failed");
		}
	}

	return pRet;
}
