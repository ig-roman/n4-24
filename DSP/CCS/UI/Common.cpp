/*
 * Common.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#include "Common.h"
#include "Pages/UILocalization.h"
#include <math.h>

bool IsDigitCustom(int8_t chVal, bool bCharsOnly)
{
	bool bRet = chVal >= '0' && chVal <= '9';

	if (!bRet && !bCharsOnly)
	{
		bRet = '-' == chVal || '.' == chVal;
	}

	return bRet;
}

bool IsCtrlKeys(int8_t chVal)
{
	return chVal == BTN_ENTER || chVal == BTN_BACKSPACE || chVal == BTN_CLEAR; // ST TODO: add all other
}

bool IsSameBand(PCBAND pBand1, PCBAND pBand2)
{
	return pBand1 == pBand2 ||
	(
		pBand1->max				== pBand2->max &&
		pBand1->min				== pBand2->min &&
		pBand1->scale			== pBand2->scale &&
		pBand1->fraction		== pBand2->fraction
	);
}

bool IsSameInvertedBand(PCBAND pBand1, PCBAND pBand2)
{
	return pBand1 != pBand2 &&
		(
			pBand1->max == -pBand2->min &&
			pBand1->min == -pBand2->max &&
			pBand1->scale == pBand2->scale &&
			pBand1->fraction == pBand2->fraction
			);
}

double GetMultiplier(const PCBAND pBand, uint16_t* punMesgId, STATEMODE eType /* STATEMODE_NONE */)
{
	double dRet = 1;
	uint16_t unMesgId = 0;

	switch (eType)
	{
		case STATEMODE_I:
		{
			int16_t nWholePart = pBand->scale - pBand->fraction;

			if (nWholePart <= -3)
			{
				// uA Range
				dRet = 1000000;
				unMesgId = MSG_UA;
			}
			else if (nWholePart <= 0)
			{
				// mA Range
				dRet = 1000;
				unMesgId = MSG_MA;
			}
			else
			{
				unMesgId = MSG_A;
			}

			break;
		}
		case STATEMODE_U:
		{
			bool bMvRange = pBand->fraction >= pBand->scale;
			unMesgId = bMvRange ? MSG_MV : MSG_V;
			dRet = bMvRange ? 1000 : 1;
			break;
		}
		case STATEMODE_R:
		{
			if (pBand->fraction < 1)
			{
				dRet = 0.000001;
				unMesgId = MSG_MOHM;
			}
			else if (pBand->fraction < 4)
			{
				dRet = 0.001;
				unMesgId = MSG_KOHM;
			}
			else
			{
				unMesgId = MSG_OHM;
			}

			break;
		}
		default:
		{
			if (pBand->fraction < 0)
			{
				dRet = 0.000001;
				unMesgId = MSG_MHZ;
			}
			else if (pBand->fraction < 2)
			{
				dRet = 0.001;
				unMesgId = MSG_KHZ;
			}
			else
			{
				dRet = 1;
				unMesgId = MSG_HZ;
			}
			break;
		}
	}

	if (punMesgId)
	{
		*punMesgId = unMesgId;
	}

	return dRet;
}

const Validator s_validatorStdFreq(&MINMAX_FREQ,	UNITS_FREQUENCY,	VAL_FLAG_REAL_POS_NUM);
const Validator s_validatorStdVol(&MINMAX_VOL,		UNITS_VOLTAGE,		VAL_FLAG_REAL_NUM);
const Validator s_validatorStdCur(&MINMAX_CUR,		UNITS_CURRENT,		VAL_FLAG_REAL_NUM);
const Validator s_validatorStdRes(&MINMAX_RES,		UNITS_RESISTANCE,	VAL_FLAG_REAL_POS_NUM);

/** **********************************************************************
 *
 * Normalizes user enter value to C units like mega, kilo...
 *
 * @param dValRaw		value to analyze
 * @param unScale		ret value, new scale
 * @param unFraction	ret value, new fraction
 * @param dMultiplier	ret value, new multiplier
 * @param unUnitsTextId	ret value, resources ID of units (KHz, mV...)
 * @param pPrevValUnit	optional previous units value. Needed to switch bands when value is "0.0"
 *
 *********************************************************************** */
void Validator::Normalize(double& dValRaw, uint8_t& unScale, int8_t& nFraction, double& dMultiplier, uint16_t& unUnitsTextId, const UNITDATA* pPrevValUnit) const
{
	dMultiplier = 1;
	unUnitsTextId = MSG_NONE;
	if (pUnits)
	{
		const UNITDATA* pLastUnit = FindRangeUnitData(dValRaw, pPrevValUnit);
		dMultiplier = pLastUnit->dMul;
		unUnitsTextId = pLastUnit->unNameId;
	}

	if ((unFlags & VAL_FLAG_IS_INT) > 0)
	{
		nFraction = 0;
		int32_t nDispVal = (int32_t)(dValRaw * dMultiplier);
		unScale = MAX(unScaleDef, (uint8_t)log10f((float)nDispVal) + 1);
	}
	else
	{
		unScale = unScaleDef;
		nFraction = unFractionDef + (uint8_t)log10f((float)dMultiplier);
	}
}

Validator::Validator(const MINMAX* pMinMax, const UNITDATA* pUnits, uint32_t unFlags, uint8_t unScale, uint8_t unFraction) :
	pMinMax(pMinMax),
	pUnits(pUnits),
	unFlags(unFlags),
	unScaleDef(unScale),
	unFractionDef(unFraction)
{}

bool Validator::validate(double dVal) const
{
	bool bRet = pMinMax
				? dVal <= pMinMax->dMax && dVal >= pMinMax->dMin
				: true;
	return bRet;
}

double Validator::GetDefaultValue()
{
	return pMinMax->dDefault;
}

void Validator::LimitValue(double& dVal) const
{
	if (dVal > pMinMax->dMax)
	{
		dVal = pMinMax->dMax;
	}
	else if (dVal < pMinMax->dMin)
	{
		dVal = pMinMax->dMin;
	}
}

const UNITDATA* Validator::FindRangeUnitData(double dVal, const UNITDATA* m_pPrevValUnit) const
{
	const UNITDATA* pLastUnit = m_pPrevValUnit;
	if (!pLastUnit || dVal)
	{
		dVal = dVal < 0 ? -dVal : dVal;
		pLastUnit = pUnits;
		for (const UNITDATA* pUnit = pUnits; RANGE_UNK != pUnit->eRange; pUnit++)
		{
			double dProbeVal = 1 / pUnit->dMul;
			// dVal < dProbeVal
			if (0 != dVal && IsGreater(dProbeVal, dVal, 9))
			{
				break;
			}
			else if (0 == dVal && pUnit->eRange == RANGE_X)
			{
				pLastUnit = pUnit;
				break;
			}
			pLastUnit = pUnit;
		}

	}
	return pLastUnit;
}

uint16_t IncOffset(uint16_t& unYoffset, uint16_t nOffset/* = DEFAULT_CONTROL_HEIGHT */)
{
	unYoffset += nOffset; 
	return unYoffset;
}


int GetFreeMem()
{
#ifdef WIN32
	return 0;
#else
	// MEMORY MONITOR!
	int16_t unIdx = 0;
	long* pPointerPrev = NULL;
	while (true)
	{
		long* pPointer = new long();
		if (pPointer)
		{
			*pPointer = (long)pPointerPrev;
			pPointerPrev = pPointer;
			unIdx++;
		}
		else
		{
			break;
		}
	}

	long* pPointer = pPointerPrev;
	while (pPointer)
	{
		long* pPrev = (long*)*pPointer;
		delete pPointer;
		pPointer = pPrev;
	}
	return unIdx * sizeof(long);
#endif
}

