/*
 * PageFactory.h
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */
#ifndef PAGEFACTORY_H_
#define PAGEFACTORY_H_

#include "Common.h"
#include "Pages/BasePage.h"

#define DECLARE_PAGE(PAGE_ID, PAGE_CLASS)	\
case PAGE_ID:								\
{											\
	pRet = new PAGE_CLASS();				\
	break;									\
}											\

/** **********************************************************************
 *
 * Class responsible for the creation of all UI pages.
 *
 *********************************************************************** */

class PageFactory
{
public:
	PageFactory() {};
	virtual ~PageFactory() {};
	BasePage* CreatePage(PAGE);
};

#endif /* PAGEFACTORY_H_ */
