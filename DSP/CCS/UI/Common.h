/*
 * Common.h
 *
 * Contains global UI constants, functions and structures.
 *
 *  Created on: Oct 28, 2013
 *      Author: Sergei Tero
 */

#ifndef COMMON_H_
#define COMMON_H_

#include "../CommonTypes.h"

#ifdef WIN32
#include <limits>
#ifndef NAN
#define NAN std::numeric_limits<double>::quiet_NaN()
#endif //NAN
#ifndef INFINITY
#define INFINITY std::numeric_limits<double>::infinity()
#endif // INFINITY
#endif // WIN32

#define VAL_SQRT2 1.4142135623730950488016887242097				// = sqrt(2)
#define VAL_1_DEV_PI2SQRT2 0.90031631615710606955519919100672	//  = 1/(PI/(2*sqrt(2)))

#define MAX_DISPLAY_LENGTH 799
#define MAX_DISPLAY_HEIGHT 479

enum FNBTN {FNBTN_1 = 0, FNBTN_2, FNBTN_3, FNBTN_4, FNBTN_5, FNBTN_6, FNBTN_7, FNBTN_8, FNBTN_9, FNBTN_10, FNBTN_11, FNBTN_MAX};
enum PAGE {
			// Common pages
			PAGE_EMPTY, PAGE_STARTUP, PAGE_SELECT_MODE,

			// Modes pages
			PAGE_MODE_U, PAGE_MODE_I, PAGE_MODE_R,

			// Configuration pages
			PAGE_CONFIGURATION, PAGE_SETTINGS_DISPLAY, PAGE_SETTINGS_ADVANCED, PAGE_SETTINGS_LANGUAGE, PAGE_REMOTE_CTRL, PAGE_FREQ_CORR,
			PAGE_LAN_CONFIG, PAGE_GPIB_CONFIG, PAGE_RS232_CONFIG, PAGE_HV_CONFIG, PAGE_CHANGE_PSW_CONFIG, PAGE_LAB_PARAMS_INFO_CONFIG,

			// Calibration pages
			PAGE_SELECT_BAND, PAGE_ADJUST_LEVELS, PAGE_CALIBRATION_MODE,
			PAGE_RESISTORS_HELP, PAGE_RESISTORS_CALIBRATION,
			PAGE_BAND_MIN_MAX_LEVELS_EX, PAGE_SHUNTS_HELP_EX, PAGE_LAB_PARAMS_CONFIG,

			// Auto mode
			PAGE_AUTO_MODE, PAGE_ARRAYS_MGR, PAGE_GROUPS_MGR, PAGE_ENTRY_CONFIG};

enum CTRL_TYPE {CTRL_LABEL, CTRL_BUTTON, CTRL_EDITBOX, CTRL_EDITBOX_DIGIT, CTRL_EDITBOX_BAND, CTRL_FRAME};
enum FONT {FONT_FRAMDCN_13, FONT_FRAMDCN_16, TAGRA_MS_20, TAGRA_MS_48};

#define STATE_CONST_WITH_SAME_NAME

enum STATEERR		{ STATEERR_NO_ERROR, STATEERR_INVALID_MODE, STATEERR_OUT_OF_RANGE, STATEERR_GO_BAND_UP, STATEERR_GO_BAND_DOWN, STATEERR_ABSOLUTE_UPPER_LIMIT, STATEERR_ABSOLUTE_LOWER_LIMIT, STATEERR_DISABLE_OUT };
enum STATEVOLINFO	{ STATEVOLINFO_LV, STATEVOLINFO_HV };
enum STATELANG		{ STATELANG_EN, STATELANG_RU };

#define FONT_DEFAULT		FONT_FRAMDCN_13
#define FONT_MENU			FONT_FRAMDCN_13
#define FONT_CAPTION		FONT_FRAMDCN_16
#define FONT_DEFAULT_DIGITS	TAGRA_MS_20
#define FONT_BIG_DIGITS		TAGRA_MS_48

#define AL_NONE								0x00
#define AL_CENTER_TEXT_HORIZONTALLY			0x01
#define AL_CENTER_TEXT_VERTICALLY			0x02
#define AL_RIGHT_JUSTIFY_TEXT				0x04
#define AL_BOTTOM_JUSTIFY_TEXT				0x08 
#define AL_DO_NOT_WORD_WRAP_TEXT			0x10 
#define AL_ADD_HORIZONTAL_SPACE_FOR_BORDER	0x20
#define AL_ADD_VERTICAL_SPACE_FOR_BORDER	0x40
#define AL_TURN_ALIGNMENT_ON				0x80

#define DR_WITH_ROUNDED_CORNERS				0x01
#define DR_WITH_SHADOW						0x02
#define DR_BLANK_PIXELS_AROUND				0x04
#define DR_INNER_FILLED_BACKGROUND_COLOR	0x08
#define DR_INNER_DRAWN_FOREGROUND_COLOR		0x10
#define DR_FILLED_WITH_CURRENT_FILL_PATTERN	0x20
#define DR_DRAW_WITHOUT_FRAME				0x40

#define COLOR_RED							0xFE0000

#define COLOR_FG_DEFAULT					0xFFFBDA
#define COLOR_ERROR							COLOR_RED
#define COLOR_INFO							0xFF9933
#define COLOR_BG_ERROR						0x802738
#define COLOR_BG_INFO						0x993300
#define COLOR_BG_DEFAULT					0x324D6F
#define COLOR_BG_ACTIVE						COLOR_BG_DEFAULT
#define COLOR_BG_PRESSED					0x2B56A3
#define COLOR_BG_TOGGLED					COLOR_BG_PRESSED
#define COLOR_BG_DEFAULT_BTN				0x499CEC
#define COLOR_FG_DEFAULT_BTN				0x000000
#define COLOR_OUT_HV						COLOR_RED
#define COLOR_OUT_LV						COLOR_FG_DEFAULT

#define STYLE_BTN_SHADOW_DEPTH				4
#define STYLE_PAGE_CAPTION_OFFSET			30
#define STYLE_PAGE_CAPTION_HEIGHT			80
#define STYLE_PAGE_LEFT_OFFSET				10
#define STYLE_PAGE_KEY_LABEL_LENGTH			325
#define STYLE_PAGE_VALUE_LABEL_LENGTH		350
#define STYLE_PAGE_VALUE_LABEL_XOFFSET		(STYLE_PAGE_LEFT_OFFSET + STYLE_PAGE_KEY_LABEL_LENGTH + 5)
#define STYLE_UNITS_SIZE_BIG_CTRLS			180
#define STYLE_UNITS_SIZE_SMALL_CTRLS		40		// Is used for short labels: % V, Kv ... 
#define STYLE_UNITS_SIZE_SMALL_CTRLS_UNIVERSAL 80	// Is used for any labels: MOhms, min....
#define STYLE_UNITS_SPASE_CTRLS_PART		20

#define DEFAULT_CONTROL_HEIGHT				40
#define DEFAULT_CONTROL_LENGTH				150

#define VAL_FLAG_IS_INT		0x00000001
#define VAL_FLAG_HAS_POS	0x00000002
#define VAL_FLAG_HAS_NEG	0x00000004
#define VAL_FLAG_HAS_ZERO	0x00000008

#define VAL_FLAG_REAL_POS_NUM	( VAL_FLAG_HAS_POS | VAL_FLAG_HAS_ZERO )
#define VAL_FLAG_REAL_NUM		( VAL_FLAG_HAS_POS | VAL_FLAG_HAS_NEG | VAL_FLAG_HAS_ZERO )
#define VAL_FLAG_POS_INT_NUM	( VAL_FLAG_IS_INT | VAL_FLAG_HAS_POS )

#if defined(WIN32) || defined(PART_TM4C123GH6PM)
	#define BTN_BACKSPACE						8
	#define BTN_TAB								9
	#define BTN_LEFT							37
	#define BTN_RIGHT							39
	#define BTN_UP								38
	#define BTN_DOWN							40
	#define BTN_BAND_UP							33
	#define BTN_BAND_DOWN						34
	#define BTN_ENTER							32
	#define BTN_CLEAR							-64

	#define BTN_FN1								113
	#define BTN_FN2								114
	#define BTN_FN3								115
	#define BTN_FN4								116
	#define BTN_FN5								117
	#define BTN_FN6								118
	#define BTN_FN7								119
	#define BTN_FN8								120
	#define BTN_FN9								121
	#define BTN_FN10							122
	#define BTN_FN11							123

	#define BTN_MODE_I							'I'
	#define BTN_MODE_U							'U'
	#define BTN_MODE_R							'R'
	#define BTN_MODE_SELECT						'M'

	#define BTN_OUT_ON							36
	#define BTN_OUT_OFF							35

	#define BTN_UP_WHEEL						-35
	#define BTN_DOWN_WHEEL						-37
	#define BTN_WHEEL_PRESSED					-36
#elif defined(PART_TM4C123FH6PM) // Front panel with TIVA CPU
	#include "../Drivers/keyboard.h"
	#define BTN_BACKSPACE						-28
	#define BTN_TAB								-11
	#define BTN_LEFT							-10
	#define BTN_RIGHT							-12
	#define BTN_UP								-9
	#define BTN_DOWN							-13
	#define BTN_ENTER							-23
	#define BTN_BAND_UP							-39
	#define BTN_BAND_DOWN						-22
	#define BTN_CLEAR							-32

	#define BTN_FN1								-16
	#define BTN_FN2								-15
	#define BTN_FN3								-14
	#define BTN_FN4								-8
	#define BTN_FN5								-7
	#define BTN_FN6								-6

	#define BTN_FN7								-1
	#define BTN_FN8								-2
	#define BTN_FN9								-3
	#define BTN_FN10							-4
	#define BTN_FN11							-5

	#define BTN_MODE_I							-18
	#define BTN_MODE_U							-17
	#define BTN_MODE_R							-19
	#define BTN_MODE_SELECT						-24

	#define BTN_OUT_ON							-20
	#define BTN_OUT_OFF							-21

	#define BTN_UP_WHEEL						ROTARY_UP_BTN_CODE
	#define BTN_DOWN_WHEEL						ROTARY_DOWN_BTN_CODE
	#define BTN_WHEEL_PRESSED					-40
#else
	#define BTN_BACKSPACE						-7
	#define BTN_TAB								-32
	#define BTN_LEFT							-31
	#define BTN_RIGHT							-33
	#define BTN_UP								-34
	#define BTN_DOWN							-35
	#define BTN_ENTER							-11
	#define BTN_BAND_UP							-29
	#define BTN_BAND_DOWN						-17
	#define BTN_CLEAR							-8

	#define BTN_FN1								-37
	#define BTN_FN2								-38
	#define BTN_FN3								-39
	#define BTN_FN4								-40
	#define BTN_FN5								-41
	#define BTN_FN6								-42

	#define BTN_FN7								-43
	#define BTN_FN8								-44
	#define BTN_FN9								-45
	#define BTN_FN10							-46
	#define BTN_FN11							-47

	#define BTN_MODE_I							-2
	#define BTN_MODE_U							-1
	#define BTN_MODE_R							-3
	#define BTN_MODE_SELECT						-6

	#define BTN_OUT_ON							-4
	#define BTN_OUT_OFF							-5

	#define BTN_UP_WHEEL						-50
	#define BTN_DOWN_WHEEL						-51
	#define BTN_WHEEL_PRESSED					-36
#endif

typedef struct tagRECT_T
{
	POINT_T position;
	POINT_T size;
} RECT_T, *PRECT_T;

typedef struct tagCALIBRATION_POINT
{
	double		dFrequency;
	double		dInitialValue;
	double		dAdjustedValue;
	uint8_t		unBandIdx	: 7; 
	bool		bUMode		: 1;
} CALIBRATION_POINT, *PCALIBRATION_POINT;
typedef const CALIBRATION_POINT* PCCALIBRATION_POINT;

enum UNIT { UNIT_CURRENT, UNIT_VOLTAGE, UNIT_FREQUENCY, UNIT_RESISTANCE, UNIT_TIME };
enum RANGE { RANGE_u, RANGE_m, RANGE_X, RANGE_K, RANGE_M, RANGE_MIN, RANGE_HOURS, RANGE_UNK };

typedef struct tagMINMAX
{
	double dMin;
	double dMax;
	double dDefault;
} MINMAX;

typedef struct tagUNITDATA
{
	RANGE		eRange;
	uint16_t	unNameId;
	double		dMul;
} UNITDATA;

bool IsDigitCustom(int8_t chVal, bool bCharsOnly = true);
bool IsCtrlKeys(int8_t chVal);
bool IsSameBand(PCBAND pBand1, PCBAND pBand2);
bool IsSameInvertedBand(PCBAND pBand1, PCBAND pBand2);

double GetMultiplier(PCBAND pBand, uint16_t* punMesgId = NULL, STATEMODE eType = STATEMODE_FULL_STANDBY);

extern const UNITDATA UNITS_CURRENT[];
extern const UNITDATA UNITS_VOLTAGE[];
extern const UNITDATA UNITS_FREQUENCY[];
extern const UNITDATA UNITS_RESISTANCE[];
extern const UNITDATA UNITS_TIME[];

extern const MINMAX MINMAX_FREQ;
extern const MINMAX MINMAX_VOL;
extern const MINMAX MINMAX_RES;
extern const MINMAX MINMAX_CUR;

class Validator
{
public:
	Validator(const MINMAX* pMinMax, const UNITDATA* pUnits, uint32_t unFlags = VAL_FLAG_REAL_NUM, uint8_t unScale = 9, uint8_t unFraction = 6);
	bool validate(double dVal) const;
	double GetDefaultValue();
	void LimitValue(double& dVal) const;
	const UNITDATA* FindRangeUnitData(double dVal, const UNITDATA* m_pPrevValUnit) const;
	virtual void Normalize(double& dValRaw, uint8_t& unScale, int8_t& nFraction, double& dMultiplier, uint16_t& unUnitsTextId, const UNITDATA* pPrevValUnit) const;

public:
	const UNITDATA* pUnits;

private:
	const MINMAX* pMinMax;
	const uint32_t unFlags;
	const uint8_t unScaleDef;
	const uint8_t unFractionDef;
};

extern const Validator s_validatorStdFreq;
extern const Validator s_validatorStdCur;
extern const Validator s_validatorStdVol;
extern const Validator s_validatorStdRes;

uint16_t IncOffset(uint16_t& unYoffset, uint16_t = DEFAULT_CONTROL_HEIGHT);

int GetFreeMem();

#endif /* COMMON_H_ */
