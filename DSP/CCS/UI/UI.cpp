/** **********************************************************************
 *
 * C++ implementation of UI interface methods.
 *
 *********************************************************************** */

#include "../UIInterface.h"
#include "UI.hpp"
#include "../Logic/Logic.hpp"

#include <stdio.h>

#include "PageManager.h"

/** **********************************************************************
 *
 * Prepages UI on startup
 *
 *********************************************************************** */
__declspec(dllexport) void __stdcall UI_Init()
{
	g_pageManager.ChangePage(PAGE_STARTUP);
}

/** **********************************************************************
 *
 * Makes ButtonChanged call for page manager
 *
 * @param chButtonCode	key code of pressed, repeated or released button.
 * @param bIsTouched	true if it is Press event or Repeat event otherwise it is Release event
 *
 *********************************************************************** */
__declspec(dllexport) void __stdcall UI_ButtonChanged(int8_t chButtonCode, char bIsPressed)
{
#ifdef WIN32
	// ST TODO: remove char remapping
	if (-66 == chButtonCode)
	{
		chButtonCode = '.';
	}
	else if (-67 == chButtonCode)
	{
		chButtonCode = '-';
	}
#elif defined(PART_TM4C123FH6PM) // Front panel with TIVA CPU
	switch (chButtonCode)
	{
		case -36:  chButtonCode = '0'; break;
		case -33:  chButtonCode = '1'; break;
		case -34:  chButtonCode = '2'; break;
		case -35:  chButtonCode = '3'; break;
		case -29:  chButtonCode = '4'; break;
		case -30:  chButtonCode = '5'; break;
		case -31:  chButtonCode = '6'; break;
		case -25:  chButtonCode = '7'; break;
		case -26:  chButtonCode = '8'; break;
		case -27:  chButtonCode = '9'; break;
		case -37:  chButtonCode = '.'; break;
		case -38:  chButtonCode = '-'; break;
	}
#else
	switch (chButtonCode)
	{
		case -28:  chButtonCode = '0'; break;
		case -27:  chButtonCode = '1'; break;
		case -21:  chButtonCode = '2'; break;
		case -15:  chButtonCode = '3'; break;
		case -26:  chButtonCode = '4'; break;
		case -20:  chButtonCode = '5'; break;
		case -14:  chButtonCode = '6'; break;
		case -25:  chButtonCode = '7'; break;
		case -19:  chButtonCode = '8'; break;
		case -13:  chButtonCode = '9'; break;
		case -22:  chButtonCode = '.'; break;
		case -16:  chButtonCode = '-'; break;
	}
#endif
	g_pageManager.OnButtonChanged(chButtonCode, bIsPressed != 0);
}

/** **********************************************************************
 *
 * The periodic function. Is invoked always.
 *
 *********************************************************************** */
__declspec(dllexport) void __stdcall UI_Loop()
{
	g_pageManager.Loop();
}

/** **********************************************************************
 *
 * Makes TouchEvent call for page manager
 *
 * @param unX			X position of toch event
 * @param unY			Y position of toch event
 * @param bIsTouched	true if it is touch event otherwise it is release event
 *
 *********************************************************************** */
__declspec(dllexport) void __stdcall UI_TouchEvent(uint16_t unX, uint16_t unY, char bIsTouched)
{
	g_pageManager.TouchEvent(unX, unY, bIsTouched);
}

/** **********************************************************************
 *
 * Shows error message on the screen
 * @param error text to show
 *
 *********************************************************************** */
__declspec(dllexport) void __stdcall UI_ShowError(const char* pszError)
{
	g_pageManager.ShowError(pszError);
}
