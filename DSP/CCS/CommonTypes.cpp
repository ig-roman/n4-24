
#include "CommonTypes.h"
#include <stdlib.h>
#include <ctype.h>

int16_t ParseFirstDigit(const char **ppszText)
{
	int16_t nRet = -1;
	char* pszTextEnd = (char*)(*ppszText);
	if (pszTextEnd && *pszTextEnd && isdigit(*pszTextEnd))
	{
		nRet = (int16_t)strtoul(*ppszText, &pszTextEnd, 10);
		if (*pszTextEnd)
		{
			*ppszText = pszTextEnd + 1;
		}
		else
		{
			*ppszText = pszTextEnd;
		}

	}

	return nRet;
}
