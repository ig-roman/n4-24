/*
 * DeviceConfig.cpp
 *
 *  Created on: Nov 14, 2013
 *      Author: Sergei Tero
 */

#include "DeviceConfig.h"
#include "BandsManager.h"
#include <math.h>
#include <string.h>

/** **********************************************************************
 *
 * Loads valid configuration from EEPROM or uses default values on device start up.
 *
 *********************************************************************** */
void DeviceConfig::Init()
{
	if (!LoadConfig())
	{
#ifdef WIN32
		// First create 128k file
		uint8_t *dummy = new uint8_t[EEPROM_FILE_SIZE];
		memset(dummy, 0, EEPROM_FILE_SIZE);
		CLogic_StoreBytes(0, EEPROM_FILE_SIZE, dummy);
		delete[] dummy;
#endif
		// Then store initial config to it
		memset(&m_cfg, 0, sizeof(DCONF));
		strcpy(m_cfg.m_szConfPsw, DEFAULT_PASSWORD);
		strcpy(m_cfg.m_szCalPsw, DEFAULT_CALIBRATION_PASSWORD);
		m_cfg.m_enumLang = STATELANG_EN;
		m_cfg.m_enumCtrlMode = CTRLMODE_MIXED;
		m_cfg.m_unGPIBAddress = DEFAULT_GPIB_ADDR;
		m_cfg.m_dSafetyVoltage = DEFAULT_HV_SAFETY_VALUE;
		
		// Set IP to DHCP
		m_cfg.m_cfgLan.unIP1 = 0x00;
		m_cfg.m_cfgLan.unIP2 = 0x00;
		m_cfg.m_cfgLan.unIP3 = 0x00;
		m_cfg.m_cfgLan.unIP4 = 0x00;
		m_cfg.m_cfgLan.unMask = DEFAULT_SCPI_MASK;
		m_cfg.m_cfgLan.unPort = DEFAULT_SCPI_RAW_PORT;
		m_cfg.m_cfgLan.bForceUpdate = false;

		// RS232 configuration
		m_cfg.m_cfgRs232.unSpeed = DEFAULT_RS232_SPEED;
		m_cfg.m_cfgRs232.eParity = DEFAULT_RS232_PAR;
		m_cfg.m_cfgRs232.unStopBits = DEFAULT_RS232_STOP;

		// Backlights intensity
		m_cfg.m_unBacklightIntensity = ((MAX_BACKLIGHT_INTENSITY - MIN_BACKLIGHT_INTENSITY) / 2) + MIN_BACKLIGHT_INTENSITY;

		// Laboratory information
		strcpy(m_cfg.m_cfgLab.szDate, DEFAULT_LAB_DATE);
		m_cfg.m_cfgLab.unTempErr = m_cfg.m_cfgLab.unHumErr = m_cfg.m_cfgLab.unHum = m_cfg.m_cfgLab.nTemp = 0;

		// Buzzer suppress in HV mode
		m_cfg.m_bHVBuzzerSuppress = 0;

#ifdef WITH_FREQ_CORR
		// Frequency correction
		m_cfg.m_dFreqCorr = 0;
#endif

		CLogic_StoreBytes(EEPROM_ADDR_CONF, sizeof(DCONF), (uint8_t*)&m_cfg);
		SetUpdateCrc();

		// Performs XPort reconfiguration
		m_cfg.m_cfgLan.bForceUpdate = true;
	}
	else
	{
		// Force update has been set by UI
		// Remove state from EEPROM and keep it in structure
		// Then XPort will update own configuration.
		// XPort configuration interface is available within 3 seconds after device restart.
		if (m_cfg.m_cfgLan.bForceUpdate)
		{
			m_cfg.m_cfgLan.bForceUpdate = false;
			SetLan(m_cfg.m_cfgLan);	
			m_cfg.m_cfgLan.bForceUpdate = true;
		}
	}
}

/** **********************************************************************
 *
 * Updates member value in EEPROM and in configuration parameter
 *
 * @param unAddress	address in EEPROM
 * @param unVal		value of the member
 * @param unMemeber	reference to configuration parameter.
 *
 *********************************************************************** */
void DeviceConfig::SetByte(uint32_t unAddress, uint8_t unVal, uint8_t& unMemeber)
{
	unMemeber = unVal;
	CLogic_StoreByte(unAddress, unVal);
}

/** **********************************************************************
 *
 * Updates password value in configuration
 *
 * @param pszNewPsw		new password value
 * @param bIsConfPsw	if value is true function updates password for configuration otherwise for calibration
 *
 *********************************************************************** */
void DeviceConfig::SetPassword(const char* pszNewPsw, bool bIsConfPsw)
{
	char* pszTrgPassword = bIsConfPsw ? m_cfg.m_szConfPsw : m_cfg.m_szCalPsw;
	strcpy(pszTrgPassword, pszNewPsw);

	uint32_t unValStructOffset = bIsConfPsw ? offsetof(DCONF, m_szConfPsw) : offsetof(DCONF, m_szCalPsw);
	CLogic_StoreBytes(EEPROM_ADDR_CONF + unValStructOffset, PAS_MAX_LEN, (uint8_t*)pszTrgPassword);
	SetUpdateCrc();
}

/** **********************************************************************
 *
 * Updates CRC value in EEPROM for configuration data block
 *
 *********************************************************************** */
void DeviceConfig::SetUpdateCrc()
{
	uint8_t unCrc8 = countCrc8((uint8_t*)&m_cfg, sizeof(DCONF), 1);
	CLogic_StoreByte(EEPROM_ADDR_CONF_CRC, unCrc8);
}

/** **********************************************************************
 *
 * Loads configuration from EEPROM and check that data is valid
 * @return returns validation result
 *
 *********************************************************************** */
bool DeviceConfig::LoadConfig()
{
	CLogic_RestoreBytes(EEPROM_ADDR_CONF, sizeof(DCONF), (uint8_t*)&m_cfg);
	uint8_t unCrc8Must = CLogic_RestoreByte(EEPROM_ADDR_CONF_CRC);
	uint8_t unCrc8Has = countCrc8((uint8_t*)&m_cfg, sizeof(DCONF), 1);

	bool bCrcOk = unCrc8Must == unCrc8Has;
	bool bModeOk = CTRLMODE_REMOTE != GetCtrlMode();

	return bCrcOk && bModeOk;
}

/** **********************************************************************
 *
 * Public function to set device control mode.
 * Function prevent saving of REMOTE mode to EEPROM. Otherwise user can loose device control permanently.
 *
 *********************************************************************** */
void DeviceConfig::SetCtrlModePub(const CTRLMODE& enumCtrlMode, bool bTemp)
{
	if ((CTRLMODE_REMOTE == enumCtrlMode) || bTemp)
	{	// Don't save temporary lokced state to EEPROM. It need for escaping often EEPROM writes with remote calibration functions
		m_cfg.m_enumCtrlMode = enumCtrlMode;
	}
	else
	{
		SetCtrlMode(enumCtrlMode);
	}
}
