/*
 * BaseIndexData.cpp
 *
 *  Created on: Aug 22, 2016
 *      Author: User
 */

#include "IndexDataR.h"
#include "../CalibrationUtils.h"
#include "../../BandsManager.h"


IndexDataR::IndexDataR() : IndexData(MODE_R, 1, sm_arrRCurve, sm_arrBandsRsFake)
{}

int8_t IndexDataR::GetPointsCount(uint8_t unBandIdx) const 
{ 
	// Allocate point for 2W sense and 4W sense
	return EXTBAND_R_MAX_COUNT * 2;
}

void IndexDataR::GetCalibrationPoint(uint8_t unBandIdx, uint8_t unPointIdx, int8_t nFreqIdx, PWORKUNIT wuOut) const
{
	if (unPointIdx >= EXTBAND_R_MAX_COUNT)
	{
		unPointIdx -= EXTBAND_R_MAX_COUNT;
	}
	PCBAND pBand = sm_arrBandsRs + unPointIdx;
	wuOut->value = pBand->min;
	wuOut->pBand = pBand;
}
