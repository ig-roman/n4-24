/*
 * PointsIndexer.h
 *
 *  Created on: Aug 15, 2016
 *      Author: User
 */

#ifndef POINTSINDEXER_H_
#define POINTSINDEXER_H_

#include <map>
#include "../CalibrationTypes.h"
#include "IndexData.h"

typedef struct tagCALPOINT
{
	tagCALPOINT() :
	eMode(MODE_UNKNOWN),
	unBandIdx(0),
	unFreqIdx(0),
	unPointIdx(0)
	{}

	tagCALPOINT(MODE eModeA, uint8_t unBandIdxA, uint8_t unFreqIdxA, uint8_t unPointIdxA)
	{
		eMode = eModeA;
		unBandIdx = unBandIdxA;
		unFreqIdx = unFreqIdxA;
		unPointIdx = unPointIdxA;
	}

	bool operator == (const tagCALPOINT& p2) const
	{
		const tagCALPOINT& p1 = *this;

		bool bRet = false;
		if (p1.eMode == p2.eMode)
		{
			if (p1.unBandIdx == p2.unBandIdx)
			{
				if (p1.unFreqIdx == p2.unFreqIdx)
				{
					bRet = p1.unPointIdx == p2.unPointIdx;
				}
			}
		}

		return bRet;
	}

	bool operator < (const tagCALPOINT& p2) const
	{
		const tagCALPOINT& p1 = *this;

		//return memcmp(&p1, &p2, sizeof(tagCALPOINT)) < 0;

		bool bRet = false;
		if (p1.eMode == p2.eMode)
		{
			if (p1.unBandIdx == p2.unBandIdx)
			{
				if (p1.unFreqIdx == p2.unFreqIdx)
				{
					bRet = p1.unPointIdx < p2.unPointIdx;
				}
				else
				{
					bRet = p1.unFreqIdx < p2.unFreqIdx;
				}
			}
			else
			{
				bRet = p1.unBandIdx < p2.unBandIdx;
			}
		}
		else
		{
			bRet = p1.eMode < p2.eMode;
		}

		return bRet;
	}

	MODE eMode;
	uint8_t unBandIdx;
	uint8_t unFreqIdx;
	uint8_t unPointIdx;
} CALPOINT, *PCALPOINT;
typedef const CALPOINT* PCCALPOINT;
typedef std::map<CALPOINT, uint16_t> TCalPoint2IndexMap;

typedef struct tagITERATION_DATA
{
	tagITERATION_DATA() :
	bStop(false),
	unPointIdx(0)
	{}

	CALPOINT calibrationPoint;
	bool bStop;
	uint16_t unPointIdx;
} ITERATION_DATA, *PITERATION_DATA;

typedef void(*Decider)(PITERATION_DATA pIterationData, void* pData);

class PointsIndexer
{
public:
	PointsIndexer();
	virtual ~PointsIndexer();

	bool FindOffsetByPoint(MODE eMode, uint8_t unBandIdx, uint8_t unFreqIdx, uint8_t unPointIdx, uint16_t &nPointRet);
	bool FindCaibrationPointByOffset(int16_t nPointIdx, MODE &eModeRet, uint8_t &unBandIdxRet, uint8_t &unFreqIdxRet, uint8_t &unPointIdxRet);

	PCBAND GetModeBand(MODE mode, int8_t nBandIdx, bool bMinMax);
	int16_t GetCalibrBandIdx(MODE eMode, int8_t nBandIdx, bool bMinMax) const;

	const IndexData& GetIndexData(MODE mode) const;
	uint16_t GetPointsCountForModeAndBand(MODE eMode, uint8_t unBandIdx, uint16_t* punFirstPointIndex = NULL, uint16_t* punLastPointIndex = NULL);

private:
	void LoadCaibrationPointsCache(uint8_t unBandIdx);
	void IterateCaibrationPoints(PITERATION_DATA pIterationData, Decider callback, void* pData);
	void CollectIndexies(PITERATION_DATA pIterationData, const IndexData& indexData, Decider callback, void* pData);
	static void AddPointToCache(PITERATION_DATA pIterationData, void* pData);
	static void ComparePointIdx(PITERATION_DATA pIterationData, void* pData);
	void ClearLoadedCache();

private:
	MODE m_eLoadedMode;
	uint8_t m_arrLoadedBands[2];
	TCalPoint2IndexMap m_mapCalPointCache;
	CALPOINT m_prevCalPoint;
	int16_t m_nPrevIdx;
	const IndexData** m_pIndexData;
};

extern PointsIndexer gs_pointsIndexer;


typedef struct tagLOAD_CAL_POINT_ARG
{
	uint8_t unBandToLoad;
	PointsIndexer* pPintIndexer;
} LOAD_CAL_POINT_ARG, *PLOAD_CAL_POINT_ARG;

#endif /* POINTSINDEXER_H_ */
