/*
 * BaseIndexData.h
 *
 *  Created on: Aug 22, 2016
 *      Author: User
 */

#ifndef BASEINDEXDATAR_H_
#define BASEINDEXDATAR_H_

#include "IndexData.h"

class IndexDataR : public IndexData {
public:
	IndexDataR();
	virtual ~IndexDataR() {};

	virtual int8_t GetPointsCount(uint8_t unBandIdx) const;
	virtual void GetCalibrationPoint(uint8_t unBandIdx, uint8_t unPointIdx, int8_t nFreqIdx, PWORKUNIT wuOut) const;
};

#endif /* BASEINDEXDATAR_H_ */
