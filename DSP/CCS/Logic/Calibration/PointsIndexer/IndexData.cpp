/*
 * BaseIndexData.cpp
 *
 *  Created on: Aug 22, 2016
 *      Author: User
 */

#include "IndexData.h"
#include "../CalibrationUtils.h"
#include "../../BandsManager.h"

IndexData::IndexData(MODE eMode, int8_t nBandCount, PCLINEAR_CURVE pCustomCurves, PCBAND pBand)
	: m_pCustomCurves(pCustomCurves),
	  m_nBandCount(nBandCount),
	  m_eMode(eMode),
	  m_pBands(pBand)
{}

int8_t IndexData::GetBandCount() const
{
	return m_nBandCount;
}

PCLINEAR_CURVE IndexData::FindCustomCurve(uint8_t unBandIdx) const
{
	uint8_t nIdx = 0;
	while (m_pCustomCurves && m_pCustomCurves[nIdx].nBandIdx)
	{
		if (unBandIdx == m_pCustomCurves[nIdx].nBandIdx)
		{
			return m_pCustomCurves + nIdx;
		}
		++nIdx;
	}
	return NULL;
}

int8_t IndexData::GetPointsCount(uint8_t unBandIdx) const
{
	int8_t unRetCount = 2; // 2 point for min/max
	PCLINEAR_CURVE pCurve = FindCustomCurve(unBandIdx);
	if (pCurve)
	{
		for (unRetCount = 0; pCurve->arrPoints[unRetCount] != LC_END; unRetCount++);
	}

	return unRetCount;
}

/** **********************************************************************
 *
 * Counts of frequencies for defined mode and band. Only for min/max cases!
 *
 * @param unBandIdx			index of band
 * @param unFreqStopCount	in/out. To count all available frequencies value should be -1, to get value of frequency by index, index number should be defined.
 * @param pdStopFreq		out. Returns value of frequency if unFreqStopCount >= 0
 *
 *********************************************************************** */
void IndexData::GetModeFreqCountImpl(uint8_t unBandIdx, uint8_t unPointIdx, int8_t &unFreqStopCount, double *pdStopFreq) const
{
	WORKUNIT wuVal;
	GetCalibrationPoint(unBandIdx, unPointIdx, unFreqStopCount, &wuVal);
	uint8_t unFreqsCnt = GetModeMaxFreqsCount(unBandIdx);
	PCCAL_FREQ pMinMaxFreq = GetModeFreqsArr(unBandIdx);

	int8_t unFreqsCntRet = 0;
	for (uint8_t unFreqsIdx = 0; unFreqsIdx < unFreqsCnt; unFreqsIdx++)
	{
		const CAL_FREQ calFreq = pMinMaxFreq[unFreqsIdx];
		WORKUNIT wuFreq = { NULL, calFreq.dFreq};
		STATEERR ret = BandsManager::IsValidValue(&wuVal, true, &wuFreq, false, false, MODE_ACU == m_eMode);
		if (STATEERR_NO_ERROR == ret)
		{
			if (pdStopFreq && unFreqsCntRet == unFreqStopCount)
			{
				*pdStopFreq = wuFreq.value;
				break;
			}
			unFreqsCntRet++;
		}
	}

	unFreqStopCount = unFreqsCntRet;
}

/** **********************************************************************
 *
 * Returns frequency value for band and frequency point
 *
 * @param unBandIdx		index of band
 * @param unFreqIdx		index of frequency
 * @param unPointIdx	index of calibration point
 *
 * @return frequency value or -1 if frequency not available for defined point
 *
 *********************************************************************** */
double IndexData::GetFrequency(uint8_t unBandIdx, uint8_t unPointIdx, uint8_t unFreqIdx) const
{
	double dStopFreq = 0;
	int8_t unFreqStopCount = unFreqIdx;
	GetModeFreqCountImpl(unBandIdx, unPointIdx, unFreqStopCount, &dStopFreq);
	return dStopFreq;
}

/** **********************************************************************
 *
 * Returns count of frequencies for band and frequency point
 *
 * @param unBandIdx		index of band
 * @param unPointIdx	index of calibration point
 *
 * @return count of frequencies for defined mode
 *
 *********************************************************************** */
uint8_t IndexData::GetFrequencyCount(uint8_t unBandIdx, uint8_t unPointIdx) const
{
	int8_t unFreqsCntRet = (MODE_ACI == m_eMode || MODE_ACU == m_eMode) ? -1 : 1;
	if (-1 == unFreqsCntRet)
	{
		GetModeFreqCountImpl(unBandIdx, unPointIdx, unFreqsCntRet, NULL);
	}

	return unFreqsCntRet;
}

/** **********************************************************************
 *
 * Returns band for defined mode and band index
 *
 * @param eMode			operation mode
 * @param nBandIdx		index of band
 * @param unPointIdx	index of calibration point
 * @param bIMinMaxMode	if value is true function returns min or max value from system bands array relating to operation mode
 *							otherwise it returns value of calibration point from curve array relating to operation mode
 * @param wuOut			output value with information of band
 *
 *********************************************************************** */
void IndexData::GetCalibrationPoint(uint8_t unBandIdx, uint8_t unPointIdx, int8_t nFreqIdx, PWORKUNIT wuOut) const
{
	wuOut->pBand = m_pBands + unBandIdx;
	PCLINEAR_CURVE pCurve = FindCustomCurve(unBandIdx);
	if (pCurve)
	{
		double pbCurveVal = pCurve->arrPoints[unPointIdx];
		//pbCurveVal *= ::BandToPwmMultiplier(m_eMode, pCurve->nBandIdx);
		wuOut->value = IsPositiveBand(wuOut->pBand) ? pbCurveVal : -(pbCurveVal);
	}
	else
	{
		PCBAND pBand = m_pBands + unBandIdx;
		wuOut->value = GetBandCalibrationLimit(pBand, 0 == unPointIdx, nFreqIdx, false);
	}
}


/** **********************************************************************
*
* Returns count of calibration frequencies for defined mode
* N.B. It returns 1 for DC modes
* @param	operation mode
* @return Count of calibration frequencies
*
*********************************************************************** */
uint8_t IndexData::GetModeMaxFreqsCount(uint8_t unBandIdx) const
{
	return MODE_DCU == m_eMode || MODE_DCI == m_eMode || MODE_R == m_eMode
		? 1
		: CountFreqWithSameBand(unBandIdx, MODE_ACU == m_eMode ? sm_arrACUCalFreq : sm_arrACICalFreq);
}

/** **********************************************************************
 *
 * Returns array of calibration frequencies for defined mode
 * N.B. It returns array from one element of "0" frequency for DC modes
 * @param	operation mode
 * @return array of frequencies
 *
 *********************************************************************** */
PCCAL_FREQ IndexData::GetModeFreqsArr(uint8_t unBandIdx) const
{
	static const CAL_FREQ ZERO_FREQ = { 0, 0, 0.0};

	return MODE_DCU == m_eMode || MODE_DCI == m_eMode
			? &ZERO_FREQ
			: FindFirstFreqWithRequeredBand(unBandIdx, MODE_ACU == m_eMode ? sm_arrACUCalFreq : sm_arrACICalFreq);
}

uint8_t IndexData::CountFreqWithSameBand(uint8_t unBandIdx, PCCAL_FREQ arrCalFreq) const
{
	arrCalFreq = FindFirstFreqWithRequeredBand(unBandIdx, arrCalFreq);

	uint8_t nIdx = 0;
	while (arrCalFreq && arrCalFreq[nIdx].dFreq)
	{
		if (unBandIdx != arrCalFreq[nIdx].nBandIdx)
		{
			break;
		}
		++nIdx;
	}

	return nIdx;
}

PCCAL_FREQ IndexData::FindFirstFreqWithRequeredBand(uint8_t& unBandIdx, PCCAL_FREQ arrCalFreq) const
{
	PCCAL_FREQ arrCalFreqRet = NULL;
	PCLINEAR_CURVE pCurve = FindCustomCurve(unBandIdx);

	unBandIdx = pCurve ? unBandIdx : GetBaseCurveBandIdx(m_eMode);

	uint8_t nIdx = 0;
	while (arrCalFreq && arrCalFreq[nIdx].dFreq)
	{
		if (unBandIdx == arrCalFreq[nIdx].nBandIdx)
		{
			arrCalFreqRet = arrCalFreq + nIdx;
			break;
		}
		++nIdx;
	}

	return arrCalFreqRet;
}

/** **********************************************************************
*
* Find extremes provided band.
* Notes:
* For DCU minimum value is "0": calibration must be performed from zero, exclude the DCU 1000V band.
* For ACx maximum value must be not higher than value-frequency relation
*
* @param eMode			previously loaded operation mode
* @param pBand			band of to find extremes
* @param bIMinVal		when true function finds lower extreme otherwise the higher
* @param nFreqIdx		calibration index of frequency can be provided to limit output value using value-frequency relation. when value is -1 function skips such validation
* @param bAddOverlimit	when value true function extends limits to 20%
*
* @return value of extreme
*
*********************************************************************** */
double IndexData::GetBandCalibrationLimit(PCBAND pBand, bool bIMinVal, int8_t nFreqIdx, bool bAddOverlimit) const
{
	double dValRet;

	if (MODE_R == m_eMode)
	{
		dValRet = bAddOverlimit
			? MAX(pBand->min, bIMinVal ? 0 : MAX_WIRES_IMPEDANCE_VALUE)
			: pBand->min; // All resistors are declared in band.min
	}
	else
	{
		dValRet = bIMinVal
			? pBand->min
			: pBand->max;
		/*if (MODE_DCU == m_eMode && pBand->bandIdx == EXBAND_DCU_P_1000_V)
			dValRet = bIMinVal ? sm_arrDCULimitPoints1000v[0] : sm_arrDCULimitPoints1000v[1];
		if (MODE_DCU == m_eMode && pBand->bandIdx == EXBAND_DCU_N_1000_V)
			dValRet = bIMinVal ? -sm_arrDCULimitPoints1000v[1] : -sm_arrDCULimitPoints1000v[0];*/
		if (MODE_ACU == m_eMode && !bIMinVal && !bAddOverlimit)
		{
			if (EXBAND_ACU_100MV == pBand->bandIdx)
			{
				dValRet = 0.1;
			}
			else if (EXBAND_ACU_10MV == pBand->bandIdx)
			{
				dValRet = 0.01;
			}
#ifdef USE_ACU_1MV
			else if (EXBAND_ACU_1MV == pBand->bandIdx)
			{
				dValRet = 0.001;
			}
#endif
		}

		// nFreqIdx -1 when functions counts available frequencies
		if (nFreqIdx >= 0 && MODE_DCI != m_eMode && MODE_DCU != m_eMode)
		{
			PCCAL_FREQ pMinMaxFreq = GetModeFreqsArr(pBand->bandIdx);
			WORKUNIT wuFreq = { NULL, pMinMaxFreq[nFreqIdx].dFreq };
			double dMaxAvaliableVolForFreq = 0;
			STATEERR eRet = BandsManager::checkFrqVolRelation(dValRet, pBand, &wuFreq, MODE_ACU == m_eMode, &dMaxAvaliableVolForFreq);
			if (STATEERR_NO_ERROR != eRet)
			{
				// For Min/max calibration this line can move max calibration point to maximum valid value(voltage or current)
				dValRet = dMaxAvaliableVolForFreq;
			}
		}
	}

	return bAddOverlimit ? AddOverlimit(dValRet, m_eMode, pBand, bIMinVal) : dValRet;
}
