/*
 * PointsIndexer.cpp
 *
 *  Created on: Aug 15, 2016
 *      Author: User
 */

#include "PointsIndexer.h"
#include "../CalibrationUtils.h"
#include "../../BandsManager.h"
#include "IndexData.h"
#include "IndexDataR.h"
#include <stdio.h>

#define COUNT_OF_INDEX_DATA 5

PointsIndexer gs_pointsIndexer;

PointsIndexer::PointsIndexer()
	: m_nPrevIdx(0),
	m_eLoadedMode(MODE_UNKNOWN)
{
	ClearLoadedCache();

	m_pIndexData = new const IndexData*[COUNT_OF_INDEX_DATA];
	m_pIndexData[0] = new IndexDataDCU();
	m_pIndexData[1] = new IndexDataACU();
	m_pIndexData[2] = new IndexDataDCI();
	m_pIndexData[3] = new IndexDataACI();
	m_pIndexData[4] = new IndexDataR();
}

PointsIndexer::~PointsIndexer()
{
	for (size_t i = 0; i < COUNT_OF_INDEX_DATA; i++)
	{
		delete m_pIndexData[i];
	}
	delete m_pIndexData;
}
const IndexData& PointsIndexer::GetIndexData(MODE mode) const
{
	return *m_pIndexData[mode];
}


void PointsIndexer::ClearLoadedCache()
{
	m_eLoadedMode = MODE_UNKNOWN;
	m_mapCalPointCache.clear();
	for (size_t i = 0; i < sizeof(m_arrLoadedBands) / sizeof(m_arrLoadedBands[0]); i++)
	{
		m_arrLoadedBands[i] = (uint8_t)-1;
	}
}

/** **********************************************************************
 *
 * Finds calibration point index by defined calibration parameters
 *
 * @param eMode				operation mode
 * @param unBandIdx			index of band
 * @param unFreqIdx			index of frequency
 * @param unPointIdx		index of calibration point. It is required to consider value-frequency relations.
 * @param nPointRet			in/out. To count all available frequencies value should be -1, to get value of frequency by index, index number should be defined
 *
 *********************************************************************** */
bool PointsIndexer::FindOffsetByPoint(MODE eMode, uint8_t unBandIdx, uint8_t unFreqIdx, uint8_t unPointIdx, uint16_t &nPointRet)
{
	nPointRet = (uint16_t)-1;

	// The cache of points in some cases must be loaded for 2 band, because approximator uses selected and base band.
	bool bIsValidMode = eMode == m_eLoadedMode;

	bool bBandLoaded = false;
	uint8_t unEmptySlot = (uint8_t)-1;
	if (bIsValidMode)
	{
		for (int i = 0; i < (sizeof(m_arrLoadedBands) / sizeof(m_arrLoadedBands[0])); i++)
		{
			if (m_arrLoadedBands[i] == (uint8_t)-1)
			{
				unEmptySlot = i;
			}

			if (m_arrLoadedBands[i] == unBandIdx)
			{
				bBandLoaded = true;
			}
		}
	}

	bool bIsFull = ((uint8_t)-1 == unEmptySlot);

	bool bClearCache = !bIsValidMode || (!bBandLoaded && bIsFull);
	if (bClearCache)
	{
		ClearLoadedCache();
		bIsFull = bBandLoaded = false;
		unEmptySlot = 0;
	}

	bool bLoadBandCache = !bBandLoaded && !bIsFull;
	if (bLoadBandCache)
	{
		m_eLoadedMode = eMode;
		m_arrLoadedBands[unEmptySlot] = unBandIdx;
		LoadCaibrationPointsCache(unBandIdx);
	}

	CALPOINT calPoint(eMode, unBandIdx, unFreqIdx, unPointIdx);
	TCalPoint2IndexMap::const_iterator it = m_mapCalPointCache.find(calPoint);

	if (it != m_mapCalPointCache.end())
	{
		nPointRet = it->second;
	}
	else
	{
		TCalPoint2IndexMap::const_iterator it = m_mapCalPointCache.find(calPoint);
		if (it != m_mapCalPointCache.end())
		{
			nPointRet = it->second;
		}
		else
		{
			// Add invalid point to cache too.
			// It is used when approximator builds 2D array in area which are removed by frequency-value relation.
			nPointRet = (uint16_t)-1;
			m_mapCalPointCache.insert(TCalPoint2IndexMap::value_type(calPoint, nPointRet));
		}
	}

	return (uint16_t)-1 != nPointRet;
}


// FOR atltrace

// #define TRACE_POINTS

#ifdef TRACE_POINTS
	#include <atltrace.h>
	#define NOMINMAX 1
#endif

void PointsIndexer::AddPointToCache(PITERATION_DATA pIterationData, void* pData)
{
	PLOAD_CAL_POINT_ARG pFnArg = (PLOAD_CAL_POINT_ARG)pData;
	PointsIndexer* pThis = pFnArg->pPintIndexer;
	if (pThis->m_eLoadedMode == pIterationData->calibrationPoint.eMode && pIterationData->calibrationPoint.unBandIdx == pFnArg->unBandToLoad)
	{
		// Loading of cache
		pThis->m_mapCalPointCache.insert(TCalPoint2IndexMap::value_type(pIterationData->calibrationPoint, pIterationData->unPointIdx));
	}
}

void PointsIndexer::ComparePointIdx(PITERATION_DATA pIterationData, void* pData)
{
	int16_t nPointIdx = (int16_t)(unsigned long)(pData);
	if (nPointIdx == pIterationData->unPointIdx)
	{
		pIterationData->bStop = true;
	}
}

bool PointsIndexer::FindCaibrationPointByOffset(int16_t nPointIdx, MODE &eModeRet, uint8_t &unBandIdxRet, uint8_t &unFreqIdxRet, uint8_t &unPointIdxRet)
{
	bool bFound = false;

	// Cache for previous point
	// Is used for calibration of resistors when user adjusts resitor value
	if ( m_nPrevIdx == nPointIdx)
	{
		eModeRet		= m_prevCalPoint.eMode;
		unBandIdxRet	= m_prevCalPoint.unBandIdx;
		unFreqIdxRet	= m_prevCalPoint.unFreqIdx;
		unPointIdxRet	= m_prevCalPoint.unPointIdx;
		bFound = true;
	}

	if (!bFound)
	{

		ITERATION_DATA iterationData;
		IterateCaibrationPoints(&iterationData, ComparePointIdx, (void*)(nPointIdx));

		if (iterationData.bStop)
		{
			m_prevCalPoint.eMode		= eModeRet		= iterationData.calibrationPoint.eMode;
			m_prevCalPoint.unBandIdx	= unBandIdxRet	= iterationData.calibrationPoint.unBandIdx;
			m_prevCalPoint.unPointIdx	= unPointIdxRet	= iterationData.calibrationPoint.unPointIdx;
			m_prevCalPoint.unFreqIdx	= unFreqIdxRet	= iterationData.calibrationPoint.unFreqIdx;

			bFound = true;
			m_nPrevIdx = nPointIdx;
		}
	}

	return bFound;
}

void PointsIndexer::LoadCaibrationPointsCache(uint8_t unBandIdx)
{
	ITERATION_DATA iterationData;
	LOAD_CAL_POINT_ARG fnArg = { unBandIdx, this };
	IterateCaibrationPoints(&iterationData, AddPointToCache, &fnArg);
}

void PointsIndexer::IterateCaibrationPoints(PITERATION_DATA pIterationData, Decider callback, void* pData)
{
	for (int nIdx = 0; nIdx < COUNT_OF_INDEX_DATA && !pIterationData->bStop; ++nIdx)
	{
		const IndexData& indexData = *m_pIndexData[nIdx];
		CollectIndexies(pIterationData, indexData, callback, pData);
	}
}

void PointsIndexer::CollectIndexies(PITERATION_DATA pIterationData, const IndexData& indexData, Decider callback, void* pData)
{
	uint8_t unBandsCnt = indexData.GetBandCount();
	for (uint8_t unBandLoopIdx = 0; unBandLoopIdx < unBandsCnt && !pIterationData->bStop; unBandLoopIdx++)
	{
		uint8_t unPointsCnt = indexData.GetPointsCount(unBandLoopIdx);
		for (uint8_t unPointLoopIdx = 0; unPointLoopIdx < unPointsCnt && !pIterationData->bStop; unPointLoopIdx++)
		{
			uint8_t unFreqsCnt = indexData.GetFrequencyCount(unBandLoopIdx, unPointLoopIdx);
			for (uint8_t unFreqLoopIdx = 0; unFreqLoopIdx < unFreqsCnt && !pIterationData->bStop; unFreqLoopIdx++)
			{
				CALPOINT calPoint(indexData.m_eMode, unBandLoopIdx, unFreqLoopIdx, unPointLoopIdx);
				pIterationData->calibrationPoint = calPoint;
				callback(pIterationData, pData);
				pIterationData->unPointIdx++;
			}
		}
	}
}

/** **********************************************************************
 *
 * Returns system band for defined mode and calibration index of band
 *
 * @param eMode			operation mode
 * @param nBandIdx		index in bands array OR curves array
 * @param bMinMax		if value is true function returns band from system bands array relating to operation mode
 *							otherwise it returns band of calibration curve relating to operation mode
 *
 * @return band according to defined criteria it maybe retrieved from curve or it same as system index for min/max
 *
 *********************************************************************** */
PCBAND PointsIndexer::GetModeBand(MODE eMode, int8_t nCalModeBandIdx, bool bMinMax)
{
	PCBAND pBandRet = NULL;
	PCBAND arrBands = GetModeBands(eMode);
	if (bMinMax || MODE_R == eMode)
	{
		pBandRet = arrBands + nCalModeBandIdx;
	}
	else
	{
		PCLINEAR_CURVE pCruve  = GetModeCurve(eMode) + nCalModeBandIdx;
		pBandRet = arrBands + pCruve->nBandIdx;
	}
	return pBandRet;
}

/** **********************************************************************
 *
 * Returns calibration index of band for defined mode and index of system band
 *
 * @param eMode			operation mode
 * @param nBandIdx		system band index
 * @param bMinMax		if value is true function returns band from system bands array relating to operation mode
 *							otherwise it returns band of calibration curve relating to operation mode
 *
 * @return calibration index of band according to defined criteria it maybe retrieved from curve or it same as system index for min/max
 *			or -1 if nothing found (example: system band 1mA don't have linear curve)
 *
 *********************************************************************** */
int16_t PointsIndexer::GetCalibrBandIdx(MODE eMode, int8_t nSystemBandIdx, bool bMinMax) const
{
	int16_t nRet = -1;
	if (bMinMax)
	{
		// For MIN/MAX range of mode indexes same as indexes of band. Because for MinMax range all bands should be calibrated
		nRet = nSystemBandIdx;
	}
	else
	{
		PCLINEAR_CURVE arrCurves = GetModeCurve(eMode);
		int8_t nBandsCnt = GetModeBandCount(eMode, false);
		int8_t nRetIdx = 0;
		for (; nRetIdx < nBandsCnt; nRetIdx++)
		{
			PCLINEAR_CURVE pCurve = arrCurves + nRetIdx;
			if (pCurve->nBandIdx == nSystemBandIdx)
			{
				nRet = nRetIdx;
				break;
			}
		}
	}

	return nRet;
}

uint16_t PointsIndexer::GetPointsCountForModeAndBand(MODE eMode, uint8_t unBandIdx, uint16_t* punFirstPointIndex, uint16_t* punLastPointIndex)
{
	uint16_t unFirstPointIndex;
	FindOffsetByPoint(eMode, unBandIdx, 0, 0, unFirstPointIndex);

	uint16_t unLastPointIndex;
	const IndexData& indexData = GetIndexData(eMode);
	uint8_t unMaxPoints = indexData.GetPointsCount(unBandIdx) - 1;
	uint8_t unFreqsCnt = indexData.GetFrequencyCount(unBandIdx, unMaxPoints);
	gs_pointsIndexer.FindOffsetByPoint(m_eLoadedMode, unBandIdx, unFreqsCnt - 1, unMaxPoints, unLastPointIndex);

	if (punFirstPointIndex)
	{
		*punFirstPointIndex = unFirstPointIndex;
	}

	if (punLastPointIndex)
	{
		*punLastPointIndex = unLastPointIndex;
	}

	return unLastPointIndex - unFirstPointIndex + 1;
}

