/*
 * BaseIndexData.h
 *
 *  Created on: Aug 22, 2016
 *      Author: User
 */

#ifndef BASEINDEXDATA_H_
#define BASEINDEXDATA_H_

#include "../Definitions.h"

class IndexData {
public:
	IndexData(MODE eMode, int8_t nBandCount, PCLINEAR_CURVE pCustomCurves, PCBAND pBand);
	virtual ~IndexData() {};
	virtual int8_t GetPointsCount(uint8_t unBandIdx) const;
	
	int8_t GetBandCount() const;
	PCBAND GetBands() const { return m_pBands; };

	uint8_t GetFrequencyCount(uint8_t unBandIdx, uint8_t unPointIdx) const;
	double GetFrequency(uint8_t unBandIdx, uint8_t unPointIdx, uint8_t unFreqIdx) const;

	PCLINEAR_CURVE FindCustomCurve(uint8_t unBandIdx) const;
	void GetModeFreqCountImpl(uint8_t unBandIdx, uint8_t unPointIdx, int8_t &unFreqStopCount, double *pdStopFreq) const;
	virtual void GetCalibrationPoint(uint8_t unBandIdx, uint8_t unPointIdx, int8_t nFreqIdx, PWORKUNIT wuOut) const;

	double GetBandCalibrationLimit(PCBAND pBand, bool bIMinVal, int8_t nFreqIdx, bool bAddOverlimit) const;

	PCCAL_FREQ GetModeFreqsArr(uint8_t unBandIdx) const;
	uint8_t GetModeMaxFreqsCount(uint8_t unBandIdx) const;

	PCLINEAR_CURVE m_pCustomCurves;
	uint8_t m_nBandCount;
	MODE m_eMode;
	PCBAND m_pBands;

private:
	PCCAL_FREQ FindFirstFreqWithRequeredBand(uint8_t& unBandIdx, PCCAL_FREQ arrCalFreq) const;
	uint8_t CountFreqWithSameBand(uint8_t unBandIdx, PCCAL_FREQ arrCalFreq) const;
};


class IndexDataACU : public IndexData {
public:
	IndexDataACU() : IndexData(MODE_ACU, EXBAND_ACU_MAX_COUNT, sm_arrACUCurve, sm_arrBandsACU) {}
	virtual ~IndexDataACU() {};
};

class IndexDataDCU: public IndexData {
public:
	IndexDataDCU() : IndexData(MODE_DCU, EXBAND_DCU_MAX_COUNT, sm_arrDCUCurve, sm_arrBandsDCU) {}
	virtual ~IndexDataDCU() {};
};

class IndexDataACI: public IndexData {
public:
	IndexDataACI() : IndexData(MODE_ACI, EXTBAND_ACI_MAX_COUNT, sm_arrACICurve, sm_arrBandsACI) {}
	virtual ~IndexDataACI() {};
};
class IndexDataDCI: public IndexData {
public:
	IndexDataDCI() : IndexData(MODE_DCI, EXTBAND_DCI_MAX_COUNT, sm_arrDCICurve, sm_arrBandsDCI) {}
	virtual ~IndexDataDCI() {};
};

#endif /* BASEINDEXDATA_H_ */
