/*
 * CalibrationManager.cpp
 *
 *  Created on: Mar 6, 2014
 *      Author: Sergei Tero
 */

#include "CalibrationManager.h"

#include "LinearApprox.h"

#include <stdio.h>
#include <string.h>

#include "../../UI/Pages/UILocalization.h"
#include "../../LogicInterface.h"
#include "CalibrationUtils.h"
#include "PointsIndexer/PointsIndexer.h"
#include "../StateTracker.h"
#include "../BandsManager.h"

CalibrationManager gs_calibrationManager;

CalibrationManager::CalibrationManager()
	: m_pointsManager(),
	m_pCalcFreqVal2DBuff(NULL),
	m_pdValueCalibrDefs(NULL),
	m_pApproxBand(NULL),
	m_pdFreqCalibrDefs(NULL),
	m_dBandMultiplier(0),
	m_dBaseBandMultiplier(0),
	m_eLoadedMode(MODE_UNKNOWN),
	m_unValueCalibrDefsCnt(0),
	m_unFreqCalibrDefsCnt(0),
	m_bIsCurveForBand(false),
	m_unLoadedCurveStartOffset((uint16_t)-1),
	m_pszErrorText(NULL),
	m_pdLinCorrValues(NULL),
	m_pBaseCurve(NULL),
	m_pIndexData(NULL)
{}

CalibrationManager::~CalibrationManager()
{
	if (m_pszErrorText)
	{
		delete m_pszErrorText;
	}

	ClearDataForApprox();
}

/** **********************************************************************
 *
 * Clears prepared data for approximation
 *
 *********************************************************************** */
void CalibrationManager::ClearDataForApprox()
{
	if (m_pCalcFreqVal2DBuff)
	{
		delete[] m_pCalcFreqVal2DBuff;
	}

	if (m_pdValueCalibrDefs)
	{
		delete[] m_pdValueCalibrDefs;
	}

	if (m_pdLinCorrValues && !m_bIsCurveForBand)
	{
		delete[] m_pdLinCorrValues;
	}

	if (m_pdFreqCalibrDefs)
	{
		delete[] m_pdFreqCalibrDefs;
	}

	m_pdFreqCalibrDefs = m_pdValueCalibrDefs = m_pCalcFreqVal2DBuff = m_pdLinCorrValues = NULL;
	m_dBandMultiplier = m_dBaseBandMultiplier = m_unValueCalibrDefsCnt = m_unFreqCalibrDefsCnt = 0;
	m_unLoadedCurveStartOffset = (uint16_t)-1;
	m_bIsCurveForBand = false;
}

void CalibrationManager::Clear()
{
	m_eLoadedMode = MODE_UNKNOWN;
	m_pApproxBand = NULL;
	m_pIndexData = NULL;
	ClearDataForApprox();
}
/** **********************************************************************
 *
 * Fills arrays with acceptable data for approximation in specific band.
 * It also applies min/max to linear(curve) correction.
 * N.B. for negative bands function ignores sign. It required for approximator.
 *
 *
 *********************************************************************** */
void CalibrationManager::AllocateMemoryForApprox()
{
	// Find curve for this band or is uses curve from base band (for MODE_DCU it is 10v band )
	m_pBaseCurve = findCurveForBand(m_eLoadedMode, m_pApproxBand, m_bIsCurveForBand);

	m_unValueCalibrDefsCnt = GetModeCurvePointsCount(m_eLoadedMode, m_pBaseCurve->nBandIdx);

	// Create copy of definitions suitable for active band. Linearity (curve) has been calibrated on base band (10v for DCU, DCI) and should be adapted for active band.
	m_dBandMultiplier = ::BandToPwmMultiplier(m_eLoadedMode, m_pApproxBand->bandIdx);

	// For DCU base band = 10v therefore dBaseBandMultiplier is 1 but for ACI it is 100 because base band on 100ma
	m_dBaseBandMultiplier = ::BandToPwmMultiplier(m_eLoadedMode, m_pBaseCurve->nBandIdx);

	m_pdValueCalibrDefs = new double[m_unValueCalibrDefsCnt];
	for (uint8_t unPointIdx = 0; unPointIdx < m_unValueCalibrDefsCnt; unPointIdx++)
	{
		// Use curve for this band or scale up/down curve from base band (10v for DCI)
		double dCalPointValue = m_pBaseCurve->arrPoints[unPointIdx];
		m_pdValueCalibrDefs[unPointIdx] = dCalPointValue / GetCurveMultiplier();
	}

	if (m_bIsCurveForBand)
	{
		uint16_t nPointOffset;
		gs_pointsIndexer.FindOffsetByPoint(m_eLoadedMode, m_pBaseCurve->nBandIdx, 0, 0, nPointOffset);
		m_pdLinCorrValues = m_pointsManager.GetCalibrationPointInt(nPointOffset);
		m_unLoadedCurveStartOffset = m_pointsManager.GetLoadedPointsStartOffset();
	}
	else
	{
		// Take curve from the other band
		m_pdLinCorrValues = GetPointsMemory(m_eLoadedMode, m_pBaseCurve->nBandIdx, NULL, &m_unLoadedCurveStartOffset);
	}

	m_unFreqCalibrDefsCnt = m_pIndexData->GetModeMaxFreqsCount(m_pApproxBand->bandIdx);

	PCCAL_FREQ pFreqCalibrDefs = m_pIndexData->GetModeFreqsArr(m_pApproxBand->bandIdx);
	m_pdFreqCalibrDefs = new double[m_unFreqCalibrDefsCnt];
	for (uint8_t unFreqIdx = 0; unFreqIdx < m_unFreqCalibrDefsCnt; unFreqIdx++)
	{
		m_pdFreqCalibrDefs[unFreqIdx] = pFreqCalibrDefs[unFreqIdx].dFreq;
	}

	// Allocate memory for output 2D array which is always filled with positive calibration data and min/max correction is applied too.
	m_pCalcFreqVal2DBuff = new double[m_unFreqCalibrDefsCnt * m_unValueCalibrDefsCnt];
	if (m_pCalcFreqVal2DBuff == 0)
		CLogic_SetLastError(GetLangString(MSG_OUT_OF_MEMORY));

}

APPROXSUMERR CalibrationManager::RecalculateApproxData()
{
	uint16_t unErrorPointIdx = 0;
	APPROXSUMERR eRet = APPROXSUMERR_OK;
	uint8_t unBandIdx = m_pApproxBand->bandIdx;
	// Find maximum valid value for PWM
	double bPWMMaxVal = PWM_U_REF_VOL / m_dBandMultiplier; // Convert PWN voltage to band voltage

	bool bIsPosBand = IsPositiveBand(m_pApproxBand);

	for (uint8_t unPointIdx = 0; unPointIdx < m_unValueCalibrDefsCnt; unPointIdx++)
	{
		// Calculate corrections for calibration point
		// Calibration point definitions are defined as positive constants but are used for negative range too.
		double dPointVal = m_pdValueCalibrDefs[unPointIdx];

		uint8_t unSkippedFrequencyIdx = 0;
		for (uint8_t unFreqIdx = 0; unFreqIdx < m_unFreqCalibrDefsCnt; unFreqIdx++)
		{
			// Find delta min/max correction for this band
			double dMin = 0;
			double dMax = 0;

			if (!m_bIsCurveForBand)
			{
				uint16_t nMinMaxPointOffset = (uint16_t)-1;
				uint8_t unModeBandIdx = (uint8_t)gs_pointsIndexer.GetCalibrBandIdx(m_eLoadedMode, unBandIdx, true);
				if (gs_pointsIndexer.FindOffsetByPoint(m_eLoadedMode, unModeBandIdx, unFreqIdx, 0, nMinMaxPointOffset))
				{
					double dMinBak = *m_pointsManager.GetCalibrationPointInt(nMinMaxPointOffset);
					if (gs_pointsIndexer.FindOffsetByPoint(m_eLoadedMode, unModeBandIdx, unFreqIdx, 1, nMinMaxPointOffset))
					{
						dMax = *m_pointsManager.GetCalibrationPointInt(nMinMaxPointOffset);
						dMin = dMinBak;
					}
				}
			}

			double dBandMinVal = m_pIndexData->GetBandCalibrationLimit(m_pApproxBand, true, unFreqIdx, false);
			double dBandMaxVal = m_pIndexData->GetBandCalibrationLimit(m_pApproxBand, false, unFreqIdx, false);

			// Find increment factor for active min/max correction
			double dDeltaVal = dBandMaxVal - dBandMinVal;
			double dDeltaCor = dMax - dMin; // Min/max correction has negative sign for negative bands
			double dYInc = dDeltaCor / dDeltaVal;

			// Calculate min/max correction for calibration point with positive sign
			double dMinMaxCorr = bIsPosBand
				? ((dPointVal - dBandMinVal) * dYInc) + dMin
				: ((dPointVal + dBandMaxVal) * dYInc) - dMax;

			// Get linear(curve) correction for calibration point with positive sign
			double dLinCorr = 0;
			STATEERR pointCheckRet = STATEERR_NO_ERROR;
			if (MODE_ACU == m_eLoadedMode || MODE_ACI == m_eLoadedMode)
			{
				WORKUNIT wuVal;
				m_pIndexData->GetCalibrationPoint(unBandIdx, unPointIdx, unFreqIdx, &wuVal);
				PCCAL_FREQ pMinMaxFreq = m_pIndexData->GetModeFreqsArr(unBandIdx);
				WORKUNIT wuFreq = { NULL, pMinMaxFreq[unFreqIdx].dFreq };
				pointCheckRet = BandsManager::IsValidValue(&wuVal, true, &wuFreq, false, false, MODE_ACU == m_eLoadedMode);
			}

			uint8_t unBaseOrSepecifiedBand = m_bIsCurveForBand ? m_pApproxBand->bandIdx : m_pBaseCurve->nBandIdx;

			if (STATEERR_NO_ERROR == pointCheckRet)
			{
				uint16_t unPointOffset;
				gs_pointsIndexer.FindOffsetByPoint(m_eLoadedMode, unBaseOrSepecifiedBand, unFreqIdx - unSkippedFrequencyIdx, unPointIdx, unPointOffset);
				if (unPointOffset == (uint16_t)-1)
				{
					CLogic_SetLastError("Failed to find calibration point");
					return APPROXSUMERR_LOW_REF;
				}
				double dRawLinCorr = m_pdLinCorrValues[unPointOffset - m_unLoadedCurveStartOffset];
				dLinCorr = (bIsPosBand ? dRawLinCorr : -dRawLinCorr) / GetCurveMultiplier();
			}
			else
			{
				// This calibration point was skipped by frequency-value relation. Correction for such points always "0" and such points can not be reproduded
				unSkippedFrequencyIdx++;
			}

			// Applies linearity and min/max correction to the calibration point value
			// Function prepares data within positive range for approximation
			double dRes = dPointVal + dMinMaxCorr + dLinCorr;

			uint16_t unPointModeBandOffset = (unFreqIdx * m_unValueCalibrDefsCnt) + unPointIdx;
			// Sum of calibration point may be higher that band maximum value but not higher than PWM max ~ 11.45
			if (dRes > bPWMMaxVal)
			{
				eRet = APPROXSUMERR_HIGH_REF;
				dRes = bPWMMaxVal;
			}
			// Sum of calibration point may be negative.
			else if (dRes < 0)
			{
				eRet = APPROXSUMERR_LOW_REF;
				dRes = 0;
			}

			if (APPROXSUMERR_OK != eRet && unErrorPointIdx == 0)
			{
				unErrorPointIdx = unPointModeBandOffset + 1;
			}

			// Fill 2D array for aproximator
			m_pCalcFreqVal2DBuff[unPointModeBandOffset] = dRes;

			//ATLTRACE("old:%f, new:%f\n", pdCuve[unPointIdx], dRes);
		}
	}

	if (eRet != APPROXSUMERR_OK)
	{
		if (m_pszErrorText)
		{
			delete m_pszErrorText;
		}

		uint16_t unMsgId = eRet == APPROXSUMERR_LOW_REF ? MSG_ERR_LOWER_THAN_REF : MSG_ERR_HIGHER_THAN_REF;
		const char * pszErrorText = ::GetLangString(unMsgId);
		m_pszErrorText = new char[strlen(pszErrorText) + 4]; // 3 it is point number (���) + endline
		sprintf(m_pszErrorText, pszErrorText, unErrorPointIdx);
		CLogic_SetLastError(m_pszErrorText);
	}
	return eRet;
}

/** **********************************************************************
 *
 * Applies correction to the outputu value
 *
 * @param eMode		operation mode
 * @param wuValue	operation value (voltage or curent) and band of value
 * @param dFreq		operation frequency
 * @param sense		sense for resistors only
 *
 * @return output value with correction
 *
 *********************************************************************** */
double CalibrationManager::GetCorrectedValue(MODE eMode, WORKUNIT& wuValue, double dFreq, STATESENSE sense /* = STATESENSE_UNKNOWN*/)
{
	double dRet = wuValue.value;
	if (MODE_UNKNOWN == eMode)
	{	
		CLogic_SetLastError("CalibrationManager::GetCorrectedValue()\nIs called for invalid mode");
	}
	else
	{
		ActivateRequestedMode(eMode, wuValue.pBand);

		if (MODE_R == eMode)
		{
			uint16_t unPointOffset = wuValue.pBand->bandIdx + m_pointsManager.GetLoadedPointsStartOffset() + (sense == STATESENSE_FOUR_WIRE ? EXTBAND_R_MAX_COUNT : 0);
			dRet += m_pointsManager.GetCalibrationPointCorrectionValue(unPointOffset);
		}
		else
		{
			if (MODE_ACI == eMode || MODE_ACU == eMode)
			{
				// Perhaps this function fails when unFreqsCnt < 3 but it is not problem because count of "freqs" more than 3 for AC
				// quadraticApproxPiecewise2DCalc(&dRet, m_pCalcFreqVal2DBuff,
				linearApproxPiecewise2DCalc(&dRet, m_pCalcFreqVal2DBuff,
					dFreq, m_pdFreqCalibrDefs, m_unFreqCalibrDefsCnt,
					dRet, m_pdValueCalibrDefs, m_unValueCalibrDefsCnt,
					m_unValueCalibrDefsCnt);
			}
			else
			{
				// Approximator works with positive values only
				if (!IsPositiveBand(wuValue.pBand))
				{
					dRet = -dRet;
				}

				//quadraticApproxPiecewise1DCalc(&dRet, m_pCalcFreqVal2DBuff, wuValue.value, m_pdValueCalibrDefs, m_unValueCalibrDefsCnt);
				linearApproxPiecewise1DCalc(&dRet, m_pCalcFreqVal2DBuff, dRet, m_pdValueCalibrDefs, m_unValueCalibrDefsCnt);

				if (!IsPositiveBand(wuValue.pBand))
				{
					dRet = -dRet;
				}
			}

			// ATLTRACE("%f\t%f\n",wuValue.value, dRet);
		}
	}

	return dRet;
}

APPROXSUMERR CalibrationManager::ActivateRequestedMode(MODE eMode, PCBAND pBand)
{
	APPROXSUMERR eRet = APPROXSUMERR_OK;
		 
	bool bIsModeChanged = eMode != m_eLoadedMode;
	bool bIsBandChanged = MODE_R != eMode && (bIsModeChanged || m_pApproxBand->bandIdx != pBand->bandIdx);

	m_eLoadedMode = eMode;
	m_pApproxBand = pBand;

	if (bIsModeChanged || bIsBandChanged)
	{
		m_pointsManager.LoadBandPoints(eMode, MODE_R == eMode ? 0 : pBand->bandIdx);
		m_pIndexData = &gs_pointsIndexer.GetIndexData(m_eLoadedMode);
	}

	if (bIsBandChanged && MODE_R != eMode)
	{
		ClearDataForApprox();
		AllocateMemoryForApprox();
		eRet = RecalculateApproxData();
	}

	return eRet;
}

/** **********************************************************************
*
* Function evaluates multiplier to convert linearity(curve) to loaded band.
* Example linearity configuration is done for 10v band but is should be applied to the 1000v band with same proportion 1v (10v band) = 100v (1KV band)
*
*********************************************************************** */
double CalibrationManager::GetCurveMultiplier() const
{
	return m_bIsCurveForBand ? 1 : (m_dBandMultiplier / m_dBaseBandMultiplier);
}

/** **********************************************************************
*
* Sets calibration point value to internal array.
* N.B. Calibration points must be previously loaded with same mode by LoadBandPoints() function.
*
* @param unPointOffset		calibration point index from beginning of calibration data
* @param dDeltaValue		new calibration point delta value
*
* @return true if index of point and value are valid
*
*********************************************************************** */
bool CalibrationManager::SetCalibrationPoint(uint16_t unPointOffset, double dDeltaValue)
{
	bool bRet = MODE_R == m_eLoadedMode;

	if (!bRet)
	{
		double* dpPoint = m_pointsManager.GetCalibrationPointInt(unPointOffset);
		double dPrevState = *dpPoint;
		*dpPoint = dDeltaValue;
		bRet = APPROXSUMERR_OK == RecalculateApproxData();
		*dpPoint = dPrevState;
	}

	if (bRet)
	{
		bRet = m_pointsManager.SetCalibrationPointCorrectionValue(unPointOffset, dDeltaValue);
		if (bRet)
		{
			g_tracker.SetModified(STATECH_VALUE);
		}
	}

	return bRet;
}
