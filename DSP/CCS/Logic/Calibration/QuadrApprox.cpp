/*
 * File:    quadrApprox.c
 * Title:   
 * Created: Fri Feb 15 13:32:31 2008
 *
 */
#include "QuadrApprox.h"

typedef long double qa_t;

/* Calculate quadratic function ax2 + bx + c = y
 */
static qa_t calculateQuadraticFunction(qa_t x, qa_t a, qa_t b, qa_t c)
{
    return (a * x * x) + (b * x) + c;
}

/*
    Calculate determinant of 3X3 matrix:

    11 12 13
    21 22 23
    31 32 33
 */
static qa_t calculateDeterminantOf3X3Matrix(qa_t (*m)[3])
{
qa_t x112233;
qa_t x132132;
qa_t x122331;

qa_t mx132231;
qa_t mx112332;
qa_t mx122133;

    x112233 = m[0][0] * m[1][1] * m[2][2];
    x132132 = m[0][2] * m[1][0] * m[2][1];
    x122331 = m[0][1] * m[1][2] * m[2][0];

    mx132231 = m[0][2] * m[1][1] * m[2][0];
    mx112332 = m[0][0] * m[1][2] * m[2][1];
    mx122133 = m[0][1] * m[1][0] * m[2][2];

    return (x112233 + x132132 + x122331 - mx132231 - mx112332 - mx122133);
}

/*
    Find soulution for linear system Ax = b, where A is 3X3 matrix, x and b are 3X1 matrixes

    a11*x + a12*y + a13*z = b1
    a21*x + a22*y + a23*z = b2
    a31*x + a32*y + a33*z = b2
 */
static void solveLinearSystemOf3X3(qa_t (*A)[3], qa_t *b, qa_t *x0, qa_t *x1, qa_t *x2)
{
qa_t d;
qa_t dx0;
qa_t dx1;
qa_t dx2;
qa_t Ai[3][3];
int i, j;

    d = calculateDeterminantOf3X3Matrix(A);

    for(i = 0; i < 3; i++) {
        for(j = 0; j < 3; j++) {
            Ai[i][j] = A[i][j];
        }
    }
    for(i = 0; i < 3; i++) {
        Ai[i][0] = b[i];
    }
    dx0 = calculateDeterminantOf3X3Matrix(Ai);

    for(i = 0; i < 3; i++) {
        for(j = 0; j < 3; j++) {
            Ai[i][j] = A[i][j];
        }
    }
    for(i = 0; i < 3; i++) {
        Ai[i][1] = b[i];
    }
    dx1 = calculateDeterminantOf3X3Matrix(Ai);

    for(i = 0; i < 3; i++) {
        for(j = 0; j < 3; j++) {
            Ai[i][j] = A[i][j];
        }
    }
    for(i = 0; i < 3; i++) {
        Ai[i][2] = b[i];
    }
    dx2 = calculateDeterminantOf3X3Matrix(Ai);

    *x0 = dx0 / d;
    *x1 = dx1 / d;
    *x2 = dx2 / d;
}

void quadraticApproxCalc(double *y, double y0, double y1, double y2, double x, double x0, double x1, double x2)
{
qa_t s;
qa_t a, b, c;
qa_t A[3][3];
qa_t B[3];

    s = 0.0;

    A[0][0] = (qa_t)x0 * (qa_t)x0;
    A[0][1] = (qa_t)x0;
    A[0][2] = (qa_t)1.0;
    A[1][0] = (qa_t)x1 * (qa_t)x1;
    A[1][1] = (qa_t)x1;
    A[1][2] = (qa_t)1.0;
    A[2][0] = (qa_t)x2 * (qa_t)x2;
    A[2][1] = (qa_t)x2;
    A[2][2] = (qa_t)1.0;
    B[0]    = (qa_t)y0;
    B[1]    = (qa_t)y1;
    B[2]    = (qa_t)y2;

    solveLinearSystemOf3X3(A, &B[0], &a, &b, &c);

    s = calculateQuadraticFunction(x, a, b, c);

    *y = (double)s;
}

int quadraticApproxFindPointsStart(const double *tbl, int tblSize, double in)
{
int i;
int len;

    len = tblSize - 2;

    for(i = 1; i < len; i++) {
        if(in < tbl[i]) {
            break;
        }
    }

    return i - 1;
}

void quadraticApproxPiecewise1DCalc(double *y, const double *yTbl, double x, const double *xTbl, int size)
{
int startPoint;
double y0, y1, y2, x0, x1, x2;

    if(size <= 0) {
        *y = x;
    } else {

        startPoint = quadraticApproxFindPointsStart(xTbl, size, x);
        y0 = yTbl[startPoint];
        y1 = yTbl[startPoint + 1];
        y2 = yTbl[startPoint + 2];
        x0 = xTbl[startPoint];
        x1 = xTbl[startPoint + 1];
        x2 = xTbl[startPoint + 2];

        quadraticApproxCalc(y, y0, y1, y2, x, x0, x1, x2);
    }
}

void quadraticApproxPiecewise2DCalc(double *z, const double *zTbl, double y, const double *yTbl, int yLen, double x, const double *xTbl, int xLen, int xDim)
{
int iY = 0, iX = 0;
double x0, x1, x2, y0, y1, y2, t0, t1, t2, z0, z1, z2, zOut;

    if((xLen <= 0) || (yLen <= 0)) {
        *z = x;
    } else {
        iX = quadraticApproxFindPointsStart(xTbl, xLen, x);
        iY = quadraticApproxFindPointsStart(yTbl, yLen, y);

        x0 = xTbl[iX];
        x1 = xTbl[iX + 1];
        x2 = xTbl[iX + 2];

        y0 = yTbl[iY];
        y1 = yTbl[iY + 1];
        y2 = yTbl[iY + 2];

        t0 = zTbl[(iY * xDim) + iX];
        t1 = zTbl[((iY + 1) * xDim) + iX];
        t2 = zTbl[((iY + 2) * xDim) + iX];

        quadraticApproxCalc(&z0, t0, t1, t2, y, y0, y1, y2);

        t0 = zTbl[(iY * xDim) + iX + 1];
        t1 = zTbl[((iY + 1) * xDim) + iX + 1];
        t2 = zTbl[((iY + 2) * xDim) + iX + 1];

        quadraticApproxCalc(&z1, t0, t1, t2, y, y0, y1, y2);

        t0 = zTbl[(iY * xDim) + iX + 2];
        t1 = zTbl[((iY + 1) * xDim) + iX + 2];
        t2 = zTbl[((iY + 2) * xDim) + iX + 2];

        quadraticApproxCalc(&z2, t0, t1, t2, y, y0, y1, y2);

        quadraticApproxCalc(&zOut, z0, z1, z2, x, x0, x1, x2);

        *z = zOut;
    }
}
