/*
 * PointsManager.cpp
 *
 *  Created on: Aug 15, 2016
 *      Author: User
 */

#include "PointsManager.h"
#include "CalibrationUtils.h"
#include "PointsIndexer/PointsIndexer.h"
#include "../BandsManager.h"
#include "../../LogicInterface.h"
#include "../../UI/Pages/UILocalization.h"
#include <string.h>

PointsManager::PointsManager() :
	m_eLoadedMode(MODE_UNKNOWN),
	m_unLoadedStartOffset(0),
	m_unLoadedPointsCount(0),
	m_pPointsBuff(NULL),
	m_pszErrorText(NULL),
	m_pIndexData(NULL),
	m_unBandIdx(NULL)
{}

PointsManager::~PointsManager()
{
	ClearPointsBuffer();

	if (m_pszErrorText)
	{
		delete m_pszErrorText;
		m_pszErrorText = NULL;
	}
}

void PointsManager::ClearLoadedPoints()
{
	if (m_pPointsBuff)
	{
		int16_t nStart = m_unLoadedStartOffset;
		while (nStart < m_unLoadedPointsCount + m_unLoadedStartOffset)
		{
			*(GetCalibrationPointInt(nStart)) = 0;
			nStart++;
		}
	}
	else
	{
		CLogic_SetLastError("PointsManager::ClearLoadedPoints()\nIs called for invalid mode");
	}
}

double* PointsManager::GetCalibrationPointInt(int16_t nPointIdx) const
{
	double* dRet = NULL;

	uint16_t unOffset = nPointIdx - m_unLoadedStartOffset;
	if (unOffset >= m_unLoadedPointsCount)
	{
		CLogic_SetLastError("PointsManager::GetLoadedCalPointDelta()\nAddress of point out of loaded buffer bounds");
	}
	else
	{
		dRet = m_pPointsBuff + unOffset;
	}

	return dRet;
}


void PointsManager::ClearPointsBuffer()
{
	if (m_pPointsBuff)
	{
		delete[] m_pPointsBuff;
		m_pPointsBuff = NULL;
	}
	m_unLoadedStartOffset = 0;
	m_unLoadedPointsCount = 0;
}
/** **********************************************************************
 *
 * Loads calibration sense for defined mode and band from EEPROM.
 *
 *********************************************************************** */
STATESENSE PointsManager::GetBandSense(MODE eMode, uint8_t unBandIdx)
{
	uint8_t unData, unShift, i, unSize;
	uint32_t unAddr;
	const int *Flags;
	if (eMode == MODE_UNKNOWN)
		eMode = m_eLoadedMode;
	if (unBandIdx == (uint8_t)-1)
		unBandIdx = m_unBandIdx;
	if ((MODE_DCU != eMode) && (MODE_ACU != eMode))
		return STATESENSE_TWO_WIRE;
	if (MODE_DCU == eMode)
	{
		unAddr = EEPROM_ADDR_FLAGS_DC;
		Flags = &sm_DC_FLAGS[0];
		unSize = sizeof(sm_DC_FLAGS) / sizeof(int);
	}
	else
	{
		unAddr = EEPROM_ADDR_FLAGS_AC;
		Flags = &sm_AC_FLAGS[0];
		unSize = sizeof(sm_AC_FLAGS) / sizeof(int);
	}
 	for (i = 0; i < unSize; i++)
	{
		if (Flags[i] == unBandIdx)
		{
			unShift = i;
			break;
		}

	}
	if (i == unSize)
		return STATESENSE_TWO_WIRE;
	if (unShift >= 8) {
		unShift -= 8;
		unAddr++;
	}
	CLogic_RestoreBytes(unAddr, 1, &unData);
	if ((unData >> unShift) & 1)
		return STATESENSE_FOUR_WIRE;
	return STATESENSE_TWO_WIRE;
}
/** **********************************************************************
 *
 * Stores calibration sense for defined mode and band to EEPROM.
 *
 *********************************************************************** */
void PointsManager::SetBandSense(STATESENSE sense, MODE eMode, uint8_t unBandIdx)
{
	uint8_t unData, unShift, i, unSize;
	uint32_t unAddr;
	const int *Flags;
	if (eMode == MODE_UNKNOWN)
		eMode = m_eLoadedMode;
	if (unBandIdx == (uint8_t)-1)
		unBandIdx = m_unBandIdx;
	if ((MODE_DCU != eMode) && (MODE_ACU != eMode))
		return;
	if (MODE_DCU == eMode)
	{
		unAddr = EEPROM_ADDR_FLAGS_DC;
		Flags = &sm_DC_FLAGS[0];
		unSize = sizeof(sm_DC_FLAGS) / sizeof(int);
	}
	else
	{
		unAddr = EEPROM_ADDR_FLAGS_AC;
		Flags = &sm_AC_FLAGS[0];
		unSize = sizeof(sm_AC_FLAGS) / sizeof(int);
	}
	for (i = 0; i < unSize; i++)
	{
		if (Flags[i] == unBandIdx)
		{
			unShift = i;
			break;
		}
	}
	if (i == unSize)
		return;
	if (unShift >= 8) {
		unShift -= 8;
		unAddr++;
	}
	CLogic_RestoreBytes(unAddr, 1, &unData);
	if (sense == STATESENSE_FOUR_WIRE)
		unData |= 1 << unShift;
	else
		unData &= ~(1 << unShift);
	CLogic_StoreBytes(unAddr, 1, &unData);
}
/** **********************************************************************
 *
 * Loads calibration point for defined mode and band to internal buffer from EEPROM.
 * Functions GetCalibrationPointCorrectionValue() and SetCalibrationPoint() provide
 * access to single calibration point.
 *
 * @param eMode		operation mode to load calibration points
 *
 *********************************************************************** */
void PointsManager::LoadBandPoints(MODE eMode, uint8_t unBandIdx)
{
	m_eLoadedMode = eMode;
	m_unBandIdx = unBandIdx;
	m_pIndexData = &gs_pointsIndexer.GetIndexData(m_eLoadedMode);
	ClearPointsBuffer();

	m_pPointsBuff = GetPointsMemory(eMode, unBandIdx, &m_unLoadedPointsCount, &m_unLoadedStartOffset);
}

/** **********************************************************************
 *
 * Stores calibration points for defined mode to EEPROM.
 * N.B. calibration points must be previously loaded with same mode
 * by LoadBandPoints() function.
 *
 *********************************************************************** */
void PointsManager::StoreBandsPoints(int nCalibrationStep)
{
	if (m_pPointsBuff)
	{
		GetEepromAddress(m_unLoadedStartOffset, 0);	 // Load first band point address
		uint16_t nFreqsCount = m_pIndexData->GetModeMaxFreqsCount(m_unBandIdx);
		if (nCalibrationStep == -1)
		{
			for (int i = 0; i < m_unLoadedPointsCount; i++)
				CLogic_StoreBytes(GetEepromAddress(m_unLoadedStartOffset + i, nFreqsCount), sizeof(double), (uint8_t*)(&m_pPointsBuff[i]));
		}
		else	
			CLogic_StoreBytes(GetEepromAddress(m_unLoadedStartOffset + nCalibrationStep, nFreqsCount), sizeof(double), (uint8_t*)(&m_pPointsBuff[nCalibrationStep]));
	}
	else
	{
		CLogic_SetLastError("PointsManager::StoreBandsPoints()\nIs called for invalid mode");
	}
}

/** **********************************************************************
 *
 * Sets calibration point value to internal array.
 * N.B. Calibration points must be previously loaded with same mode by LoadBandPoints() function.
 *
 * @param eMode				previously loaded operation mode
 * @param unPointOffset		calibration point index from beginning of calibration data
 * @param dDeltaValue		new calibration point delta value
 *
 * @return true if index of point and value are valid
 *
 *********************************************************************** */
bool PointsManager::SetCalibrationPointCorrectionValue(uint16_t unPointOffset, double dDeltaValue)
{
	bool bRet = ValidatePointValue(unPointOffset, dDeltaValue);
	if (bRet)
	{
		double* dpPoint = GetCalibrationPointInt(unPointOffset);
		*dpPoint = dDeltaValue;
	}

	return bRet;
}

/** **********************************************************************
 *
 * Returns calibration point value from internal array.
 * N.B. Internal array of calibration points must be previously loaded
 * with same mode by LoadBandPoints() function.
 *
 * @param unPointOffset		calibration point index from beginning of calibration data
 *
 * @return Calibration point delta value
 *
 *********************************************************************** */
double PointsManager::GetCalibrationPointCorrectionValue(uint16_t unPointOffset)
{
	double dRet = 0;
	if (m_pPointsBuff)
	{
		double *pdRet = GetCalibrationPointInt(unPointOffset);
		if (pdRet)
		{
			dRet = *pdRet;
			if (!ValidatePointValue(unPointOffset, dRet))
			{
				CLogic_SetLastError("PointsManager::GetCalibrationPoint()\nThis point has invalid value and has been cleared to 0");
				dRet = 0;
				*pdRet = 0;
			}
		}
	}
	else
	{
		CLogic_SetLastError("PointsManager::GetCalibrationPoint()\nIs called for invalid mode");
	}
	return dRet;
}

void PointsManager::GetCalibrationPoint(uint16_t unPointOffset, PWORKUNIT wuOut)
{
	if (unPointOffset - m_unLoadedStartOffset >= m_unLoadedPointsCount )
	{
		CLogic_SetLastError("PointsManager::GetCalibrationPoint()\nAddress of point out of buffer bounds");
	}
	else
	{
		MODE mode;
		uint8_t unFreqIdx, unPointIdx, unBandIdx;

		// Finds detailed information about calibration point
		gs_pointsIndexer.FindCaibrationPointByOffset(unPointOffset, mode, unBandIdx, unFreqIdx, unPointIdx);
		m_pIndexData->GetCalibrationPoint(unBandIdx, unPointIdx, unFreqIdx, wuOut);
	}
}

bool PointsManager::ValidatePointValue(uint16_t unPointOffset, double dDeltaValue)
{
	WORKUNIT wuVal;
	bool bRet = MODE_R != m_eLoadedMode;

	// Don't validate U and I modes because it uses more complex validation logic which is implemented in approximator.
	if (!bRet)
	{
		GetCalibrationPoint(unPointOffset, &wuVal);
		PCBAND pBand = wuVal.pBand;
		double dNewValue = wuVal.value + dDeltaValue;

		double dBandMin = m_pIndexData->GetBandCalibrationLimit(pBand, true, -1, true);
		if (dNewValue >= dBandMin)
		{
			double dBandMax = m_pIndexData->GetBandCalibrationLimit(pBand, false, -1, true);
			bRet = dBandMax >= dNewValue;
		}
	}

	return bRet;
}

double* GetPointsMemory(MODE eMode, uint8_t unBandIdx, uint16_t* punLoadedPointsCount, uint16_t* punLoadedStartOffset)
{
	uint16_t unLoadedStartOffset = 0, unLoadedPointsCount = 0, nFreqsCount;

	const IndexData& indexData = gs_pointsIndexer.GetIndexData(eMode);

	double* pPointsBuff = NULL;

	if (MODE_UNKNOWN != eMode)
	{
		unLoadedPointsCount = gs_pointsIndexer.GetPointsCountForModeAndBand(eMode, unBandIdx, &unLoadedStartOffset);

		pPointsBuff = new double[unLoadedPointsCount];
		if (!pPointsBuff) {
			CLogic_SetLastError(GetLangString(MSG_OUT_OF_MEMORY));
			return NULL;
		}
		memset(pPointsBuff, NULL, unLoadedPointsCount * sizeof(double));

		GetEepromAddress(unLoadedStartOffset, 0);	 // Load first band point address
		if (eMode == MODE_R)
			nFreqsCount = 1;
		else
			nFreqsCount = indexData.GetModeMaxFreqsCount(unBandIdx);
		for (int i = 0; i < unLoadedPointsCount; i++)
			CLogic_RestoreBytes(GetEepromAddress(unLoadedStartOffset + i, nFreqsCount), sizeof(double), (uint8_t*)(&pPointsBuff[i]));

		for (uint16_t unIdx = 0; unIdx < unLoadedPointsCount; unIdx++)
		{
			double* pDTestVal = pPointsBuff + unIdx;
			if (!ValidateCalibrationValueLimits(*pDTestVal))
			{
				*pDTestVal = 0;
			}
		}
	}

	if (punLoadedPointsCount) *punLoadedPointsCount = unLoadedPointsCount;
	if (punLoadedStartOffset) *punLoadedStartOffset = unLoadedStartOffset;

	return pPointsBuff;
}
