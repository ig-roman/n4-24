/*
 * PointsManager.h
 *
 *  Created on: Aug 15, 2016
 *      Author: User
 */

#ifndef POINTSMANAGER_H_
#define POINTSMANAGER_H_

#include "CalibrationTypes.h"
#include "../../Common.h"
#include "PointsIndexer/IndexData.h"

class PointsManager 
{
public:
	PointsManager();
	virtual ~PointsManager();

	void StoreBandsPoints(int nCalibrationStep = -1);
	void LoadBandPoints(MODE unModeIdx, uint8_t unBandIdx);

	void ClearLoadedPoints();
	double* GetCalibrationPointInt(int16_t nPointIdx) const;

	STATESENSE GetBandSense(MODE eMode = MODE_UNKNOWN, uint8_t unBandIdx = (uint8_t)-1);
	void SetBandSense(STATESENSE sense, MODE eMode = MODE_UNKNOWN, uint8_t unBandIdx = (uint8_t)-1);

	double GetCalibrationPointCorrectionValue(uint16_t unPointOffset);
	void GetCalibrationPoint(uint16_t unPointOffset, PWORKUNIT wuOut);
	bool SetCalibrationPointCorrectionValue(uint16_t unPointOffset, double dPointValue);
	bool ValidatePointValue(uint16_t unPointOffset, double dDeltaValue);

	uint16_t GetLoadedPointsStartOffset() { return m_unLoadedStartOffset; };
	void ClearPointsBuffer();

private:
	char * m_pszErrorText;
	MODE m_eLoadedMode;
	const IndexData* m_pIndexData;
	uint16_t m_unLoadedStartOffset;
	uint16_t m_unLoadedPointsCount;
	double* m_pPointsBuff;
	uint8_t m_unBandIdx;
};

const int sm_DC_FLAGS[] = 
{
	EXBAND_DCU_P_1_V,
	EXBAND_DCU_P_10_V,
	EXBAND_DCU_P_100_V,
	EXBAND_DCU_P_1000_V,
	EXBAND_DCU_N_1_V,
	EXBAND_DCU_N_10_V,
	EXBAND_DCU_N_100_V,
	EXBAND_DCU_N_1000_V,
	EXBAND_DCU_P_100_MV,
	EXBAND_DCU_N_100_MV
};

const int sm_AC_FLAGS[] = 
{
	EXBAND_ACU_1V,
	EXBAND_ACU_10V,
	EXBAND_ACU_100V,
	EXBAND_ACU_1000V
};

double* GetPointsMemory(MODE eMode, uint8_t unBandIdx, uint16_t* punLoadedPointsCount = NULL, uint16_t* punLoadedStartOffset = NULL);

#endif /* POINTSMANAGER_H_ */
