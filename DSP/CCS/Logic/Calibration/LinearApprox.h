/*
 * File:    linearApprox.h
 * Title:   
 * Created: Wed Apr 18 15:12:06 2007
 *
 */

#ifndef LINEARAPPROX_H
#define LINEARAPPROX_H

#ifdef __cplusplus
extern "C" {
#endif

void linearApproxCalc(double *y, double y0, double y1, double x, double x0, double x1);

void linearApproxPiecewise1DCalc(double *y, const double *yTbl, double x, const double *xTbl, int size);
void linearApproxPiecewise2DCalc(double *z, const double *zTbl, double y, const double *yTbl, int yLen, double x, const double *xTbl, int xLen, int xDim);

#ifdef __cplusplus
}
#endif

#endif /* #ifndef LINEARAPPROX_H */
