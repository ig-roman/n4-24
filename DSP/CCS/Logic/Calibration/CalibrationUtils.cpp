/*
 * CalibrationUtils.cpp
 *
 *  Created on: Aug 15, 2016
 *      Author: User
 */

#include "../../LogicInterface.h"
#include "../BandsManager.h"
#include "PointsIndexer/PointsIndexer.h"
#include "CalibrationUtils.h"
#include "Definitions.h"
#include <math.h>

STATEMODE ConvertMode(MODE eMode)
{
	STATEMODE modeRet = STATEMODE_UNKNOWN;

	if (MODE_ACI == eMode || MODE_DCI == eMode)
	{
		modeRet = STATEMODE_I;
	}
	else if (MODE_ACU == eMode || MODE_DCU == eMode)
	{
		modeRet = STATEMODE_U;
	}
	else if (MODE_R)
	{
		modeRet = STATEMODE_R;
	}

	return modeRet;
}

/** **********************************************************************
 *
 * Converts "device state mode" to "operation mode".
 *
 * @param eMode	device state mode
 * @param bDc	AC/DC flag is required to build "operation mode"
 *
 * @param Returns operation mode
 *
 *********************************************************************** */
MODE ConvertMode(STATEMODE eMode, bool bDc)
{
	MODE modeRet = MODE_UNKNOWN;

	if (STATEMODE_U == eMode && bDc)
	{
		modeRet = MODE_DCU;
	}
	else if (STATEMODE_U == eMode && !bDc)
	{
		modeRet = MODE_ACU;
	}
	else if (STATEMODE_I == eMode && bDc)
	{
		modeRet = MODE_DCI;
	}
	else if (STATEMODE_I == eMode && !bDc)
	{
		modeRet = MODE_ACI;
	}
	else if (STATEMODE_R == eMode)
	{
		modeRet = MODE_R;
	}

	return modeRet;
}

bool ValidateCalibrationValueLimits(double dVal)
{
	return NAN != dVal && INFINITY != dVal && dVal < MAX_CAL_VALUE && dVal > -MAX_CAL_VALUE;
}
/** **********************************************************************
 *
 * Returns calibration curve for defined mode
 * @param eMode			operation mode
 * @return calibration curve which is contains bands with calibration points.
 *
 *********************************************************************** */
PCLINEAR_CURVE GetModeCurve(MODE eMode)
{
	PCLINEAR_CURVE pCruve = NULL;
	switch (eMode)
	{
		case MODE_DCU: pCruve = sm_arrDCUCurve; break; // DCU
		case MODE_DCI: pCruve = sm_arrDCICurve; break; // DCI
		case MODE_ACU: pCruve = sm_arrACUCurve; break; // ACU
		case MODE_ACI: pCruve = sm_arrACICurve; break; // ACI
	}
	return pCruve;
}

/** **********************************************************************
 *
 * Returns bands array of system bands for defined mode
 * @param eMode			operation mode
 * @return pointer to bands array
 *
 *********************************************************************** */
PCBAND GetModeBands(MODE mode)
{
	PCBAND pBand = NULL;

	switch (mode)
	{
		case MODE_DCU:	pBand = sm_arrBandsDCU;	break; // DCU
		case MODE_DCI:	pBand = sm_arrBandsDCI;	break; // DCI
		case MODE_ACU:	pBand = sm_arrBandsACU;	break; // ACU
		case MODE_ACI:	pBand = sm_arrBandsACI;	break; // ACI
		case MODE_R:	pBand = sm_arrBandsRs;	break; // R
	}

	return pBand;
}

/** **********************************************************************
 *
 * Returns count of points in curve for defined mode and curve
 *
 * @param eMode			operation mode
 * @param unBandIdx	index of band
 *
 * @return count of points in curve
 *
 *********************************************************************** */
int8_t GetModeCurvePointsCount(MODE mode, uint8_t unBandIdx)
{
	if (MODE_R == mode)
	{
		return EXTBAND_R_MAX_COUNT;
	}
	uint16_t unRet = (uint16_t)-1;
	switch (mode)
	{
		case MODE_DCU:
		{
			switch (unBandIdx)
			{
				case EXBAND_DCU_P_100_V:
				case EXBAND_DCU_N_100_V:
					unRet = sizeof(sm_arrDCUPoints100v);
					break;
				case EXBAND_DCU_P_1000_V:
				case EXBAND_DCU_N_1000_V:
					unRet = sizeof(sm_arrDCUPoints1000v);
					break;
				default:
					unRet = sizeof(sm_arrDCUPoints10v);
					break;
			}
		} break;
		case MODE_ACU:
		{
			switch (unBandIdx)
			{
				case EXBAND_ACU_10V:
					unRet = sizeof(sm_arrACUPoints10v);
					break;
				case EXBAND_ACU_100V:
					unRet = sizeof(sm_arrACUPoints100v);
					break;
				case EXBAND_ACU_1000V:
					unRet = sizeof(sm_arrACUPoints1000v);
					break;
				default:
					unRet = sizeof(sm_arrACUPoints1v);
					break;
			}
		} break;
		case MODE_DCI:
		{
			switch (unBandIdx)
			{
				case EXTBAND_DCI_P_1A:
				case EXTBAND_DCI_N_1A:
					unRet = sizeof(sm_arrDCIPoints1A);
					break;
				case EXTBAND_DCI_P_2A:
				case EXTBAND_DCI_N_2A:
					unRet = sizeof(sm_arrDCIPoints2A);
					break;
				case EXTBAND_DCI_P_10A_AMP:
				case EXTBAND_DCI_N_10A_AMP:
					unRet = sizeof(sm_arrDCIPoints10A);
					break;
				case EXTBAND_DCI_P_30A_AMP:
				case EXTBAND_DCI_N_30A_AMP:
					unRet = sizeof(sm_arrDCIPoints30A);
					break;
				default:
					unRet = sizeof(sm_arrDCIPoints100mA);
					break;
			}
		} break;
		case MODE_ACI:
		{
			switch (unBandIdx)
			{
				case EXTBAND_ACI_1A:
					unRet = sizeof(sm_arrACIPoints1A);
					break;
				case EXTBAND_ACI_2A:
					unRet = sizeof(sm_arrACIPoints2A);
					break;
				case EXTBAND_ACI_10A_AMP:
					unRet = sizeof(sm_arrACIPoints10A);
					break;
				case EXTBAND_ACI_30A_AMP:
					unRet = sizeof(sm_arrACIPoints30A);
					break;
				default:
					unRet = sizeof(sm_arrACIPoints100mA);
					break;
			}
		} break;
		default:
			break;
	}

	if ((uint16_t)-1 == unRet)
	{
		CLogic_SetLastError("GetModeCurvePointsCount()\nFailed to find count of calibration points");
	}

	return unRet / sizeof(double) - 1;
}

// Find curve for this band or is uses curve from base band (for MODE_DCU it is 10v band )
PCLINEAR_CURVE findCurveForBand(MODE eMode, PCBAND pBand, bool& bIsCurveForBand)
{
	int8_t nBaseCurveIdx = GetBaseCurveBandIdx(eMode, ::IsPositiveBand(pBand));
	PCLINEAR_CURVE pRet = NULL;
	for (PCLINEAR_CURVE pCurve = GetModeCurve(eMode); pCurve->nBandIdx; pCurve++)
	{
		bIsCurveForBand = pCurve->nBandIdx == pBand->bandIdx;
		if (bIsCurveForBand || pCurve->nBandIdx == nBaseCurveIdx)
		{   // Found base curve or curve for band
			pRet = pCurve;
			if (bIsCurveForBand)
			{   // Found curve for band
				break;
			}
		}
	}
	return pRet;
}
/** **********************************************************************
 *
 * Finds base band for provided band. Base band means that no deviders between PWMs and device output. For example 1v on PWMs = 1v on device output.
 *
 * @param eMode		operation mode
 * @param pBand		pBand
 *
 * @return returns index of base band
 *
 *********************************************************************** */
uint8_t GetBaseCurveBandIdx(MODE eMode, bool bIsBandPos)
{
	uint8_t unRet = 0;
	for (uint8_t unIdx = 0; sm_arrBaseCurve[unIdx].eMode != MODE_UNKNOWN; unIdx++)
	{
		const BASE_CURVE pBaseCurve = sm_arrBaseCurve[unIdx];
		if (eMode == pBaseCurve.eMode &&  bIsBandPos == pBaseCurve.bIsPositive)
		{
			unRet = pBaseCurve.nBandIdx;
			break;
		}
	}
	return unRet;
}

/** **********************************************************************
*
* Returns count of band for defined mode and calibration mode
*
* @param eMode			operation mode
* @param bMinMax		if value is true function returns counts of system bands relating to operation mode
*							otherwise it returns counts of bands in calibration curve relating to operation mode
*
* @return Count of points in defined mode
*
*********************************************************************** */
int8_t GetModeBandCount(MODE eMode, bool bMinMax)
{
	int8_t unRet = 0;

	if (bMinMax)
	{
		switch (eMode)
		{
		case MODE_DCU: unRet = EXBAND_DCU_MAX_COUNT;	break; // DCU
		case MODE_DCI: unRet = EXTBAND_DCI_MAX_COUNT;	break; // DCI
		case MODE_ACU: unRet = EXBAND_ACU_MAX_COUNT;	break; // ACU
		case MODE_ACI: unRet = EXTBAND_ACI_MAX_COUNT;	break; // ACI
		}
	}
	else if (MODE_R == eMode)
	{
		// No Min/Max points for R mode
		unRet = bMinMax ? 0 : 1;
	}
	else
	{
		switch (eMode)
		{
		case MODE_DCU: unRet = sizeof(sm_arrDCUCurve); break; // DCU
		case MODE_DCI: unRet = sizeof(sm_arrDCICurve); break; // DCI
		case MODE_ACU: unRet = sizeof(sm_arrACUCurve); break; // ACU
		case MODE_ACI: unRet = sizeof(sm_arrACICurve); break; // ACI
		}
		unRet = unRet / sizeof(LINEAR_CURVE)-1;
	}

	return unRet;
}

double ApplyOverlimitPart(double dInitial, bool bAdd)
{
	return bAdd
				? dInitial * MAX_DELTA_OVERLIMIT
				: dInitial - (dInitial * (MAX_DELTA_OVERLIMIT - 1));
}

double AddOverlimit(double dInitial, MODE eMode, PCBAND pBand,  bool bIMinVal)
{
	double dValRet;
	bool bIsPosBand = IsPositiveBand(pBand);
	if (bIMinVal == bIsPosBand) // Should be inverted for negative bands
	{
		// All other modes
		dValRet = ApplyOverlimitPart(dInitial, false);
	}
	else
	{
		// Calibration point may be higher that band maximum value no more than 20%
		dValRet = ApplyOverlimitPart(dInitial, true);


		if (MODE_DCI == eMode || MODE_DCU == eMode || MODE_ACI == eMode || MODE_ACU == eMode)
		{
			// Calibration point may be higher that band maximum value but not more than PWM max ~ 11.45
			double bPWMMaxVal = (bIsPosBand ? PWM_U_REF_VOL : -PWM_U_REF_VOL) / ::BandToPwmMultiplier(eMode, pBand->bandIdx); // Converts PWM voltage range 0 - 11.45 to band range

			// Selects the absolute lowest value for range
			dValRet = bIsPosBand ? MIN(bPWMMaxVal, dValRet) : MAX(bPWMMaxVal, dValRet);
		}
	}
	return dValRet;
}

uint32_t GetEepromAddress(uint16_t unOffset, uint16_t nBandFreqsCount)
{
	static MODE eMode;
	static uint8_t unBandIdx;
	static uint16_t unBandStartOffset;
	uint8_t unPointIdx = 0, unFreqIdx = 0;
	uint32_t unAddr, unPointsCount, unFreqsCount;


	if (nBandFreqsCount == 0)
	{	// Now it called only once per GetPointsMemory or StoreBandPoints function.
		gs_pointsIndexer.FindCaibrationPointByOffset(unOffset, eMode, unBandIdx, unFreqIdx, unPointIdx);
		unBandStartOffset = unOffset;
	}
	
	switch (eMode)
	{
	case MODE_DCU:
		unAddr = EEPROM_ADDR_DCU_START;
		unPointsCount = EEPROM_SIZE_DCU_POINTS;
		unFreqsCount = 1;
		break;
	case MODE_ACU:
		unAddr = EEPROM_ADDR_ACU_START;
		unPointsCount = EEPROM_SIZE_ACU_POINTS;
		unFreqsCount = EEPROM_SIZE_ACU_FREQS;
		break;
	case MODE_DCI:
		unAddr = EEPROM_ADDR_DCI_START;
		unPointsCount = EEPROM_SIZE_DCI_POINTS;
		unFreqsCount = 1;
		break;
	case MODE_ACI:
		unAddr = EEPROM_ADDR_ACI_START;
		unPointsCount = EEPROM_SIZE_ACI_POINTS;
		unFreqsCount = EEPROM_SIZE_ACI_FREQS;
		break;
	case MODE_R:
		unAddr = EEPROM_ADDR_R_START;
		unPointsCount = 1;
		unFreqsCount = 1;
		break;
	default:
		return (uint32_t)-1;
	}

	if (nBandFreqsCount)
	{
		unPointIdx = (unOffset - unBandStartOffset) / nBandFreqsCount;
		unFreqIdx = (unOffset - unBandStartOffset) % nBandFreqsCount;
	}
	unAddr += (unBandIdx * unPointsCount * unFreqsCount + unPointIdx * unFreqsCount + unFreqIdx) * sizeof(double);
	return unAddr;
}

uint32_t GetEepromBytesCount(uint16_t unLoadedPointsCount)		{ return unLoadedPointsCount * sizeof(double); }
