/*
 * File:    quadrApprox.h
 * Title:   
 * Created: Fri Feb 15 13:32:39 2008
 *
 */

#ifndef QUADRAPPROX_H
#define QUADRAPPROX_H

void quadraticApproxCalc(double *y, double y0, double y1, double y2, double x, double x0, double x1, double x2);

void quadraticApproxPiecewise1DCalc(double *y, const double *yTbl, double x, const double *xTbl, int size);
void quadraticApproxPiecewise2DCalc(double *z, const double *zTbl, double y, const double *yTbl, int yLen, double x, const double *xTbl, int xLen, int xDim);

#endif /* #ifndef QUADRAPPROX_H */
