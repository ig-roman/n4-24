/*
	* File:    linearApprox.c
	* Title:   
	* Created: Wed Apr 18 15:11:59 2007
	*
	*/
#include "LinearApprox.h"

typedef long double la_t;

/*
 *	Linear approximation by formula:
 *
 *	y = y0 + ((x - x0) / (x1 - x0)) * (y1 - y0)
 *
 *	x - input
 *	y - output
 *
 */
void linearApproxCalc(double *y, double y0, double y1, double x, double x0, double x1)
{
	la_t diff1 = 0.0;
	la_t diff2 = 0.0;
	la_t diff3 = 0.0;
	la_t k = 0.0;
	la_t s = 0.0;

	diff1 = ((la_t)x - (la_t)x0);
	diff2 = ((la_t)x1 - (la_t)x0);

	if (diff2)
	{
		diff3 = (la_t)y1 - (la_t)y0;
		k = (diff1 / diff2);
		s = (la_t)y0 + (k * diff3);
	}
	else
	{
		s = (la_t)y1;
	}

	*y = (double)s;
}

static int linearApproxFindPointsStart(const double *tbl, int tblSize, double in)
{
	int i;
	int len;

	len = tblSize - 1;
	for (i = 1; i < len; i++)
	{
		if (in < tbl[i])
		{
			break;
		}
	}

	return i - 1;
}

/*
Piecewise 2D linear approximation:
*/
void linearApproxPiecewise2DCalc(double *z, const double *zTbl, double y, const double *yTbl, int yLen, double x, const double *xTbl, int xLen, int xDim)
{
	int iY = 0, iX = 0;
	double x0, x1, y0, y1, t0, t1, z0, z1, zOut;

	if((xLen <= 0) || (yLen <= 0))
	{
		*z = x;
	}
	else
	{
		iX = linearApproxFindPointsStart(xTbl, xLen, x);
		iY = linearApproxFindPointsStart(yTbl, yLen, y);

		x0 = xTbl[iX];
		x1 = xTbl[iX + 1];

		y0 = yTbl[iY];
		y1 = yTbl[iY + 1];

		t0 = zTbl[(iY * xDim) + iX];
		t1 = zTbl[((iY + 1) * xDim) + iX];

		linearApproxCalc(&z0, t0, t1, y, y0, y1);

		t0 = zTbl[(iY * xDim) + iX + 1];
		t1 = zTbl[((iY + 1) * xDim) + iX + 1];

		linearApproxCalc(&z1, t0, t1, y, y0, y1);

		linearApproxCalc(&zOut, z0, z1, x, x0, x1);

		*z = zOut;
	}
}

void linearApproxPiecewise1DCalc(double *y, const double *yTbl, double x, const double *xTbl, int size)
{
	int iX = 0;
	double x0, x1, y0, y1, yOut;

	if(size <= 0)
	{
		*y = x;
	}
	else
	{
		iX = linearApproxFindPointsStart(xTbl, size, x);

		x0 = xTbl[iX];
		x1 = xTbl[iX + 1];

		y0 = yTbl[iX];
		y1 = yTbl[iX + 1];

		linearApproxCalc(&yOut, y0, y1, x, x0, x1);

		*y = yOut;
	}
}
