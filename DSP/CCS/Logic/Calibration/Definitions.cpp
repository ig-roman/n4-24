/*
 * Definitions.h
 *
 *  Created on: Aug 15, 2016
 *      Author: User
 */

#include "Definitions.h"

const double sm_arrRPoints[] = {
	R_VAL_0R,
	R_VAL_1R,
	R_VAL_10R,
	R_VAL_100R,
	R_VAL_1K,
	R_VAL_10K,
	R_VAL_100K,
	R_VAL_1M,
	R_VAL_10M,
	R_VAL_100M,
	LC_END };

const LINEAR_CURVE sm_arrRCurve[] =
{
	{ EXTBAND_R_0R, sm_arrRPoints }
};
