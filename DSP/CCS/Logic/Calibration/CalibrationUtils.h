/*
 * CalibtationUtils.h
 *
 *  Created on: Aug 15, 2016
 *      Author: User
 */

#ifndef CALIBTATIONUTILS_H_
#define CALIBTATIONUTILS_H_

#include "CalibrationTypes.h"

bool ValidateCalibrationValueLimits(double dVal);
double AddOverlimit(double dInitial, MODE eMode, PCBAND pBand,  bool bIMinVal);

MODE ConvertMode(STATEMODE mode, bool bDc);
STATEMODE ConvertMode(MODE eMode);

PCBAND GetModeBands(MODE mode);
int8_t GetModeBandCount(MODE mode, bool bMinMax);

PCLINEAR_CURVE findCurveForBand(MODE eMode, PCBAND pBand, bool& bIsCurveForBand);


uint8_t GetBaseCurveBandIdx(MODE eMode, bool bIsBandPos = true);
PCLINEAR_CURVE GetModeCurve(MODE mode);
int8_t GetModeCurvePointsCount(MODE mode, uint8_t unBandIdx);

uint32_t GetEepromAddress(uint16_t unLoadedStartOffset, uint16_t nBandFreqsCount);
uint32_t GetEepromBytesCount(uint16_t unLoadedPointsCount);


#endif /* CALIBTATIONUTILS_H_ */
