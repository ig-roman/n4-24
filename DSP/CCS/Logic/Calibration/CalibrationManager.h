/*
 * CalibrationManager.h
 *
 *  Created on: Mar 6, 2014
 *      Author: Sergei Tero
 */

#ifndef CALIBRATIONMANAGER_H_
#define CALIBRATIONMANAGER_H_

//#include "../../../../CommonTypes.h"
#include "../../Common.h"
#include "PointsManager.h"
#include "CalibrationUtils.h"

/** **********************************************************************
 *
 * Is responsible for device calibration.
 *	It organizes calibration data in memory, loads/stores it from EEPROM
 *	and applies correction to device output.
 *
 *********************************************************************** */
class CalibrationManager
{
public:
	CalibrationManager();
	virtual ~CalibrationManager();

	APPROXSUMERR ActivateRequestedMode(MODE eMode, PCBAND pBand);
	double GetCorrectedValue(MODE eMode, WORKUNIT& wuValue, double dFreq, STATESENSE sense = STATESENSE_UNKNOWN);

	bool SetCalibrationPoint(uint16_t unPointOffset, double dDeltaValue);

	void AllocateMemoryForApprox();
	APPROXSUMERR RecalculateApproxData();

	PointsManager& GetPointsManager() { return m_pointsManager; };
	void Clear();

private:
	void ClearDataForApprox();
	double GetCurveMultiplier() const;

	PointsManager m_pointsManager;
	double* m_pCalcFreqVal2DBuff;
	double* m_pdValueCalibrDefs;
	PCBAND m_pApproxBand;
	double* m_pdFreqCalibrDefs;
	double m_dBandMultiplier;
	double m_dBaseBandMultiplier;
	MODE m_eLoadedMode;
	uint8_t m_unValueCalibrDefsCnt;
	bool m_bIsCurveForBand;
	uint8_t m_unFreqCalibrDefsCnt;
	char * m_pszErrorText;
	double* m_pdLinCorrValues;
	uint16_t m_unLoadedCurveStartOffset;
	PCLINEAR_CURVE m_pBaseCurve;
	const IndexData* m_pIndexData;
};

extern CalibrationManager gs_calibrationManager;

#endif /* CALIBRATIONMANAGER_H_ */
