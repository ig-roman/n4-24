/*
 * CalibrationTypes.h
 *
 *  Created on: Aug 15, 2016
 *      Author: User
 */

#ifndef CALIBRATIONTYPES_H_
#define CALIBRATIONTYPES_H_

#include "../../Common.h"

typedef enum { APPROXSUMERR_OK, APPROXSUMERR_LOW_REF, APPROXSUMERR_HIGH_REF } APPROXSUMERR;

typedef struct tagLINEAR_CURVE
{
	uint8_t			nBandIdx;
	const double*	arrPoints;
} LINEAR_CURVE, *PLINEAR_CURVE;
typedef const LINEAR_CURVE* PCLINEAR_CURVE;

typedef struct tagCAL_FREQ
{
	uint8_t	nBandIdx;
	uint8_t	nFreqBandIdx;
	double	dFreq;
} CAL_FREQ, *PCAL_FREQ;
typedef const CAL_FREQ* PCCAL_FREQ;

typedef struct tagBASE_CURVE
{
	MODE	eMode;
	uint8_t	nBandIdx;
	bool bIsPositive;
} BASE_CURVE, *PBASE_CURVE;
typedef const BASE_CURVE* PCBASE_CURVE;

#endif /* CALIBRATIONTYPES_H_ */
