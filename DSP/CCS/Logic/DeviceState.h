/*
 * DeviceState.h
 *
 *  Created on: Nov 14, 2013
 *      Author: Sergei Tero
 */

#ifndef DEVICESTATE_H_
#define DEVICESTATE_H_

#include "../UI/Common.h"
#include "StateTracker.h"
#include "DeviceConfig.h"

#define DEFAULT_VOLTAGE_DCU					10
#define DEFAULT_VOLTAGE_ACU					1
#define DEFAULT_CURRENT						0.01
#define DEFAULT_FREQENCY					1000
#define DEFAULT_RESISTANCE					1000000

/** **********************************************************************
 *
 * Contains the device state. The state is always maintained valid. Any changes
 * to dependencies are tracked and adjusted as necessary.
 *
 *********************************************************************** */
class DeviceState
{
public:
	DeviceState();
	virtual ~DeviceState();

	STATEERR GetDirecionType(bool& bDC) const;
	STATEERR SetDirecionType(bool bDC);

	STATEMODE GetOperationMode() const;
	void SetOperationMode(STATEMODE enumVal);

	STATEERR GetValue(WORKUNIT& voltage) const;
	
	STATEERR SetValue(double dVoltage, double dFrequency, bool bUseOptimalVBand, PCBAND pBandVol = NULL, PCBAND pBandFreq = NULL, bool bSkipValidation = false);
	STATEERR SetValue(WORKUNIT& voltage, bool bInSameBandVol, PWORKUNIT pFrequency, bool bInSameBandFreq, bool bSkipValidation = false);

	STATEERR GetFrequency(WORKUNIT& frequency) const;

	STATEERR SetFrequency(double dFrequency, bool bUseOptimalVBand, PCBAND pBandFreq = NULL);
	STATEERR SetFrequency(WORKUNIT& frequency, bool bInSameBand);

	STATEERR SetResistance(double dValue);

	STATEERR ChangeFrequencyBand(bool bUp);
	STATEERR ChangeValueBand(bool bUp);
	STATEERR ChangeDivider();
	int8_t ChangeDividerBand(int8_t oldIdx);

	STATEERR GetDelta(bool* pbOn, PCBAND* ppBandValue, double* pdValue, PWORKUNIT* ppPercent);
	STATEERR SetDelta(bool bOn, const double* pdValue, const double* pdPercent, bool bSkipValidation = false);

	STATEERR GetSenseMode(STATESENSE& sense) const;
	STATEERR SetSenseMode(STATESENSE sense);

	STATEERR GetZeroMode(bool& bOn) const;
	STATEERR SetZeroMode(bool bOn);

	STATEERR GetOutputValue(WORKUNIT& value) const;
	STATEERR GetCorrectedValue(WORKUNIT& value) const;

	STATEERR GetVoltageInfo(STATEVOLINFO* pInfo) const;
	bool GetSwitchOutState() const;
	STATEERR SwitchOut(bool bOn, bool bForce);

	STATEOUT GetOutState() const				{ return m_bOutOn ? STATEOUT_ON : STATEOUT_OFF; };
	bool GetConfirmRequire() const				{ return m_bHVConfirmRequire;};

	void SetOverload(bool bOverload);
	bool GetOverload() const					{ return m_bOverload; };

	void TriggerRemoteCmd()						{ m_bWasRemoteCmd = true; };
	bool UnTriggerRemoteCmd()					{ bool bWasRemoteCmd = m_bWasRemoteCmd; m_bWasRemoteCmd = false; return bWasRemoteCmd; };

	// Temporary disables or enables calibration logic of device
	bool GetCalibrationEnabled() const			{ return m_bIsCalibrationEnabled; };
	void SetCalibrationEnabled(bool bIsEnabled)	{ m_bIsCalibrationEnabled = bIsEnabled; };

	void Init();
	void Reset();

	DeviceConfig& GetConfig() 					{ return m_deviceConfig; };
	bool IsExtDividerBand(uint8_t nBandIdx);
	bool IsValidBandForDivider(uint8_t nBandIdx);
	bool IsExtDividerPossible();
	bool GetDividerEnabled();
	bool IsValidSenseForDCV(uint8_t bandIdx);
	bool IsValidSenseForACV(uint8_t bandIdx);
	bool IsValidSense(STATESENSE sense);

	STATEERR GetAbsoluteDeviceLimit(double& dLimitValue, bool bIsMax) const;
	void ResetDelta(bool bDisable);
	STATEERR SetFreqCorrector(double corr);
	double GetFreqCorrector() { return m_dFreqCorrector; }
	
private:
	STATEERR ValidateDelta(const double* pdDelataValue, const double* pdPercent, double dInitalValue, WORKUNIT* pFrequency, bool bInSameBandFreq, PCBAND* ppOutBand, double& dOutResultValue, bool bSkipValidation = false);
	void SyncDeltaBand();
	void ResetValue();
	bool ManageHV(double dVoltage, PCBAND pBand);
	void UpdateValueDependencies();
	STATELANG LoadLanguage();
	CTRLMODE LoadCtrlMode();
	STATEERR SwitchOut(bool bOn, bool bFromUser, bool bForce);

private:
	WORKUNIT m_wuInitial; // The initial value displayed on the screen
	double m_dWithDeltaValue; // Works with wuInitial BAND
	WORKUNIT m_wuOutputValue; // The resulting output value of the device with its own separate band, used in case switching to the HV mode was not allowed

	WORKUNIT m_wuFrequency;

	WORKUNIT m_wuDeltaPercent;
	PCBAND m_pBandOldInitial;
	double m_dDeltaValue;		// Shares BAND with wuInitial

	bool m_bDC;
	bool m_bIsDeltaEnabled;
	bool m_bZeroMode;
	bool m_bOutOn;
	bool m_bHVRange;
	bool m_bHVConfirmRequire;

	STATEMODE m_enumMode;
	STATESENSE m_enumSense;

	bool m_bOverload;
	bool m_bIsCalibrationEnabled;
	bool m_bWasRemoteCmd;

	DeviceConfig m_deviceConfig;

	uint32_t m_unOverloadLockTime;
	double m_dFreqCorrector;
};

// ST TODO: Add to C++ interface and remove UI direct calls
extern DeviceState g_deviceState;


#endif /* DEVICESTATE_H_ */
