/*
	* File:    fmt.c
	* Title:   
	* Created: Wed Jan 24 13:59:30 2007
	*
	*/
#include "inst.h"
#include "inst_err.h"
#include "fmt.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


const char *FMT_Coupling[FMT_COUPLING_COUNT] = { "DC", "AC" };
const char *FMT_Polarity[FMT_POLARITY_COUNT] = { "NORMal", "INVerted", "NORM", "INV", "+", "-"};
const char *FMT_calibType[FMT_CALIB_TYPE_COUNT] = { "NONE", "DCU", "ACU", "DCI", "ACI" };

int FMT_isCharacterData(char *cdata, const char *s)
{
char *cStart;
char *cStop;

	/* Ignore leading whitespaces */
	while(isspace(*cdata))
		++cdata;
	cStart = cdata;

	/* Ignore whitespaces at the end if any */
	while(*cdata) {
		if(isspace(*cdata) == 0) {
			++cdata;
		} else {
			break;
		}
	}
	cStop = cdata;

	if(*cdata) {
		/* There should be nothing after whitespaces */
		while(isspace(*cdata))
			++cdata;

		if(*cdata) {
			return INST_ERR_CHARACTER_DATA_ERROR;
		}
	}

	*cStop = 0;

	if(INST_strcmp(cStart, s) != 0) {
		return INST_ERR_CHARACTER_DATA_ERROR;
	}

	return 0;
}
int FMT_getCharacterData(char *cdata, const char *const *s, int count, unsigned int *nr)
{
int i;
int err;

	for(i = 0, err = INST_ERR_CHARACTER_DATA_ERROR; i < count; i++) {
		err = FMT_isCharacterData(cdata, s[i]);
		if(err == 0) {
			*nr = i;
			break;
		}
	}

	return err;
}

int FMT_quotedStrintgToString(char *qs, char **s)
{
char mark;
char *end;

	/* Ignore leading white spaces */
	while(isspace(*qs))
		++qs;

	/* Get quoted string type, "string" or 'string' */
	if(*qs == '\"') {
		mark = '\"';
	} else if(*qs == '\'') {
		mark = '\'';
	} else {
		return INST_ERR_STRING_DATA_ERROR;
	}

	/* Get string end */
	end = strchr(++qs, mark);
	if(end) {
		/* Set output string */
		s[0] = qs;
		*end = 0;

		/* Ignore preseding white spaces */
		end++;
		while(isspace(*end))
			++end;
		/* Theres shouldn't be nothing more */
		if(*end) {
			return INST_ERR_STRING_DATA_ERROR;
		}
	} else {
		return INST_ERR_STRING_DATA_ERROR;
	}

	return INST_ERR_NO;
}

const char *FMT_FreqSuff[FMT_FREQ_SUFF_COUNT] = {
"",
"HZ",
"KHZ",
"MHZ",
"GHZ"};
const double FMT_FreqMult[FMT_FREQ_SUFF_COUNT] = {
1.0,
1.0,
1.0e+3,
1.0e+6,
1.0e+9};

const char *FMT_VoltSuff[FMT_VOLT_SUFF_COUNT] = {
"",
"V",
"MV",
"UV",
"KV"
};
const double FMT_VoltMult[FMT_VOLT_SUFF_COUNT] = {
1.0,
1.0,
1.0e-3,
1.0e-6,
1.0e+3,
};

const char *FMT_ImpSuff[FMT_IMP_SUFF_COUNT] = {
"",
"OHM",
"KOHM",
"MOHM"};
const double FMT_ImpMult[FMT_IMP_SUFF_COUNT] = {
1.0,
1.0,
1.0e+3,
1.0e+6};

const char *FMT_CurSuff[FMT_CUR_SUFF_COUNT] = {
"",
"A",
"MA",
"UA"};
const double FMT_CurMult[FMT_CUR_SUFF_COUNT] = {
1.0,
1.0,
1.0e-3,
1.0e-6
};

int FMT_getNumericValue(char *buf, const char *const *suffixes, const double *mult, int suffixesCount, double *value, double min, double max)
{
unsigned int nr;
char *endp[1];
double tmp;

	tmp = (double)strtod(buf, endp);

	if(*(endp[0])) {
		if(FMT_getCharacterData(endp[0], suffixes, suffixesCount, &nr)) {
			return INST_ERR_SUFFIX_ERROR;
		}
	} else {
		nr = 0;
	}

	tmp *= mult[nr];

	if((tmp >= min) && (tmp <= max)) {
		*value = tmp;
	} else {
		return INST_ERR_DATA_OUT_OF_RANGE;
	}

	return INST_ERR_NO;
}
#define FMT_BOOLEAN_COUNT 4
const char *FMT_Boolean[FMT_BOOLEAN_COUNT] = {
"OFF",
"ON",
"0",
"1"};

int FMT_getBoolean(char *buf, bool& state)
{
	unsigned int nr;
	if(FMT_getCharacterData(buf, FMT_Boolean, FMT_BOOLEAN_COUNT, &nr))
	{
		return INST_ERR_DATA_TYPE_ERROR;
	}

	state = (nr == 1) || (nr == 3);

	return INST_ERR_NO;
}

int FMT_getIsOdd(char *buf, const char *const *s, int count, bool& bIsEven)
{
	unsigned int nr;
	if(FMT_getCharacterData(buf, s, count, &nr))
	{
		return INST_ERR_DATA_TYPE_ERROR;
	}

	bIsEven = nr % 2 == 0;

	return INST_ERR_NO;
}

int FMT_sprintf(char *s, const char *fmt, ...)
{
va_list ap;
int ret;

    va_start(ap, fmt);
    ret = vsprintf(s, fmt, ap);
    va_end(ap);

    return ret;
}

int FMT_getInteger(char *buf, int *val)
{
	*val = strtol(buf, NULL, 10);

	return INST_ERR_NO;
}
