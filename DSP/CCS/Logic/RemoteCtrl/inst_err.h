/*
 * File:    inst_err.h
 * Title:   
 * Created: Tue Aug 14 11:31:40 2007
 *
 */

#ifndef INST_ERR_H
#define INST_ERR_H

#ifdef __cplusplus
extern "C" {
#endif

#define INST_ERR_NO                           0
#define INST_ERR_DATA_TYPE_ERROR              -104
#define INST_ERR_MISSING_PARAMETER            -109
#define INST_ERR_COMMAND_HEADER_ERROR         -110
#define INST_ERR_SUFFIX_ERROR                 -130
#define INST_ERR_CHARACTER_DATA_ERROR         -140
#define INST_ERR_STRING_DATA_ERROR            -150
#define INST_ERR_SETTINGS_CONFLICT            -221
#define INST_ERR_DATA_OUT_OF_RANGE            -222
#define INST_ERR_ILLEGAL_PARAMETER_VALUE      -224
#define INST_ERR_OUT_OF_MEMORY                -291
#define INST_ERR_QUEUE_OVERFLOW               -350

#ifdef __cplusplus
}
#endif

#endif /* #ifndef INST_ERR_H */
