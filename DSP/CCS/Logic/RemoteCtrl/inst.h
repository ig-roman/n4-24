/*
	* File:    inst.h
	* Title:   
	* Created: Tue Feb 06 16:13:22 2007
	*
	*/

#ifndef INST_H
#define INST_H

#include "../../Common.h"
#include "inst_err.h"
#include "../DeviceState.h"

#define INST_KEYWORD_MAX_SIZE           20
#define INST_MAX_QUERY_SIZE             30
#define INST_PARSER_ARGV_SIZE           18
#define INST_ERR_QUEUE_SIZE             20
#define INST_DISP_TEXT_SIZE             20

#define INST_OUTPUT_FMT_NUMERIC_FLOAT "%f"
#define INST_OUTPUT_FMT_BOOLEAN       "%d"
#define INST_OUTPUT_FMT_INTEGER       "%d"
#define INST_OUTPUT_FMT_FREQUENCY     "%e"
#define INST_OUTPUT_FMT_VOLTAGE       "%.8e"
#define INST_OUTPUT_FMT_CURENT        "%.8e"
#define INST_OUTPUT_FMT_STRING        "%s"

void INST_errorEnqueue(int err);
int INST_errorDequeue(void);
void INST_errorQueueCls(void);
int INST_executeCommand(int cmd, int argc, char **argv, char *outb);

int INST_strcmp(char *cs, const char *ct);
int INST_parser(char *input, char *output);

void INST_execute(char *input, char *output);

int idnQ(int argc, char **argv, char *outb);
int rst(int argc, char **argv, char *outb);
int cls(int argc, char **argv, char *outb);
int opcQ(int argc, char **argv, char *outb);

int systemErrorNextQ(int argc, char **argv, char *outb);
int systemVersionQ(int argc, char **argv, char *outb);
int systemKlock(int argc, char **argv, char *outb);
int systemKlockQ(int argc, char **argv, char *outb);
int systemStatusQ(int argc, char **argv, char *outb);
int systemLocal(int argc, char **argv, char *outb);
int systemRemote(int argc, char **argv, char *outb);

int sourceFrequency(int argc, char **argv, char *outb);
int sourceFrequencyQ(int argc, char **argv, char *outb);
int sourceVoltage(int argc, char **argv, char *outb);
int sourceVoltageQ(int argc, char **argv, char *outb);

int sourceCurrent(int argc, char **argv, char *outb);
int sourceCurrentQ(int argc, char **argv, char *outb);

int sourceCorrectionState(int argc, char **argv, char *outb);
int sourceCorrectionStateQ(int argc, char **argv, char *outb);
int sourceCorrectionOFFSetQ(int argc, char **argv, char *outb);

int sourceModeQ(int argc, char **argv, char *outb);
int sourceBandQ(int argc, char **argv, char *outb);
int sourceBand(int argc, char **argv, char *outb);
int sourceBandMinimumQ(int argc, char **argv, char *outb);
int sourceBandMaximumQ(int argc, char **argv, char *outb);

int outputCoupling(int argc, char **argv, char *outb);
int outputCouplingQ(int argc, char **argv, char *outb);
int outputImpedance(int argc, char **argv, char *outb);
int outputImpedanceQ(int argc, char **argv, char *outb);
int outputPolarity(int argc, char **argv, char *outb);
int outputPolarityQ(int argc, char **argv, char *outb);
int outputState(int argc, char **argv, char *outb);
int outputStateQ(int argc, char **argv, char *outb);
int outputSense(int argc, char **argv, char *outb);
int outputSenseQ(int argc, char **argv, char *outb);

int calibrationSecureState(int argc, char **argv, char *outb);
int calibrationSecureStateQ(int argc, char **argv, char *outb);
int calibrationState(int argc, char **argv, char *outb);
int calibrationStateQ(int argc, char **argv, char *outb);
int calibrationType(int argc, char **argv, char *outb);
int calibrationTypeQ(int argc, char **argv, char *outb);
int calibrationSense(int argc, char **argv, char *outb);
int calibrationSenseQ(int argc, char **argv, char *outb);
int calibrationBandsCountQ(int argc, char **argv, char *outb);
int calibrationBandsBaseQ(int argc, char **argv, char *outb);
int calibrationBand(int argc, char **argv, char *outb);
int calibrationBandQ(int argc, char **argv, char *outb);
int calibrationBandLevelsQ(int argc, char **argv, char *outb);
int calibrationBandFreqsQ(int argc, char **argv, char *outb);
int calibrationBandMaxQ(int argc, char **argv, char *outb);
int calibrationPoint(int argc, char **argv, char *outb);
int calibrationPointQ(int argc, char **argv, char *outb);
int calibrationLevelQ(int argc, char **argv, char *outb);
int calibrationFrequencyQ(int argc, char **argv, char *outb);
int calibrationValue(int argc, char **argv, char *outb);
int calibrationValueQ(int argc, char **argv, char *outb);
int calibrateQ(int argc, char **argv, char *outb);

// Indexes starts from 2 to keep compatibility with H5-5 upload software

// From H5-5
// #define MEM_TYPE_INT16   0
// #define MEM_TYPE_INT32   1
// #define MEM_TYPE_FLOAT32 2

#define MEM_TYPE_DOUBLE 2
#define MEM_TYPE_INT8   3

int eepromWrite(int argc, char **argv, char *outb);
int eepromReadQ(int argc, char **argv, char *outb);

#define SEC_CODE_DEF  0
#define SEC_CODE_MIN  -999999
#define SEC_CODE_MAX   999999

int sourceVoltageMaximumQ(int argc, char **argv, char *outb);
int sourceVoltageMinimumQ(int argc, char **argv, char *outb);

typedef int (*FNSysh)(int argc, char **argv, char *outb);
extern const FNSysh s_arrSyshFn[];

#endif /* #ifndef INST_H */
