/*
 * File:    inst_parser.c
 * Title:   
 * Created: Wed Feb 07 11:40:21 2007
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "inst.h"
#include "sysh.h"

#ifdef _WINDOWS
#pragma warning(disable:4996)
#endif

static char *INST_ParserArgv[INST_PARSER_ARGV_SIZE];

static void stringToUpper(char *s)
{
int c;

    while(*s) {
        c = toupper((int)*s);
        *s++ = (char)c;
    }
}

static unsigned int getKeywordCount(const char *s)
{
unsigned int counter;
unsigned int i, size;

    counter = 0;
    size = strlen(s);
    for(i = 0; i < size; i++) {
        if(s[i] == ':') {
            counter++;
        }
    }
    return counter;
}

static const char *cpyShortKeyword(char *s, const char *r)
{
	uint8_t unLeft = INST_KEYWORD_MAX_SIZE - 1;
    while (*r && unLeft)
    {
        if(isupper(*r) || (*r == '*'))
        {
            *s++ = *r++;
        }
        else
        {
            break;
        }

        unLeft--;
    }

    /* For keywords with numeric suffix */
    while (isalpha(*r))
    {
        r++;
    }

    while (unLeft && isdigit(*r))
    {
        *s++ = *r++;
        unLeft--;
    }

    *s = 0;

    return r;
}

static const char *cpyLongKeyword(char *s, const char *r)
{
	uint8_t unLeft = INST_KEYWORD_MAX_SIZE - 1;
    while (*r && unLeft--)
    {
        if(*r != ':')
        {
            *s++ = *r++;
        }
        else
        {
            break;
        }
    }
    *s = 0;

    return r + 1;
}

int INST_strcmp(char *cs, const char *ct)
{
unsigned int qmBits;
char keywordEndBuf[2];
const char *ptr;
int keywordCount;
char keywordBuf[INST_KEYWORD_MAX_SIZE];
char sourceKeywordBuf[INST_KEYWORD_MAX_SIZE];

    // Count of keywords should be the same
    keywordCount = getKeywordCount(ct);
    if(keywordCount != getKeywordCount(cs)) {
        return -1;
    }

    // Keywords with ?
    if(ct[strlen(ct) - 1] == '?') {
        qmBits = 0x1;
    } else {
        qmBits = 0x0;
    }
    if(cs[strlen(cs) - 1] == '?') {
        qmBits |= 0x2;
    }
    switch(qmBits) {
        default:
        case 0x00:
            keywordEndBuf[0] = 0;
            break;
        case 0x01:
        case 0x02:
            return -1;
        case 0x03:
            keywordEndBuf[0] = '?';
            keywordEndBuf[1] = 0;
            break;
    }

    do {

        // If short matches ...
        ptr = cpyShortKeyword(keywordBuf, ct);
        cs = (char *)cpyLongKeyword(sourceKeywordBuf, cs);
        if(!keywordCount) {
            strcat(keywordBuf, keywordEndBuf);
        }

        stringToUpper(keywordBuf);
        stringToUpper(sourceKeywordBuf);
        if(strcmp(sourceKeywordBuf, keywordBuf) != 0) {
            // no, try luck with long one
            ptr = cpyLongKeyword(keywordBuf, ct);
            stringToUpper(keywordBuf);
            if(strcmp(sourceKeywordBuf, keywordBuf) != 0) {
                return -1;
            }
        }

        ptr = cpyLongKeyword(keywordBuf, ct);
        ct = ptr;

    } while (--keywordCount >= 0);

    return 0;
}

int INST_parser(char *input, char *output)
{
int i;
int argc;
char **argv = INST_ParserArgv;
char *ptr;
int CmdStatus = INST_ERR_COMMAND_HEADER_ERROR;
char *NextCmd;
char *RootPtr = NULL;
int AsteriskFlag = 0;
int FirstQueryFlag;
char *CmdOutput;

    if(!*input) {
        // No command to execute
        return INST_ERR_COMMAND_HEADER_ERROR;
    }

    if (strlen(input) >= INST_INPUT_BUFFER_MAX_SIZE - 1)
    {
    	return INST_ERR_OUT_OF_MEMORY;
    }

    // First ':' is ignored
    if(*input == ':') {
        input++;
    }

    output[0] = 0;
    CmdOutput = output;
    FirstQueryFlag = 0;

    while(1) {

        // Get command and its parameters
        NextCmd = strchr(input, ';');
        if(NextCmd) {
            // There are more commands to parse, zero terminat current command
            *NextCmd++ = 0;
        }

        // Get command
        ptr = strtok(input, " ");

        // Parse command
        CmdStatus = INST_ERR_COMMAND_HEADER_ERROR;
        for(i = 0; SystemHeaders[i]; i++)
		{
            if(INST_strcmp(ptr, SystemHeaders[i]) == 0) {
                argc = 0;
                argv[0] = NULL;
                // Get command parameters
                ptr = strtok(NULL, ",");
                while(ptr != NULL) {
                    if(argc >= INST_PARSER_ARGV_SIZE) {
                        return INST_ERR_OUT_OF_MEMORY;
                    }
                    argv[argc++] = ptr;
                    ptr = strtok(NULL, ",");
                }

                // Is there a room in output
                if(strlen(output) > (INST_OUTPUT_BUFFER_MAX_SIZE - INST_MAX_QUERY_SIZE)) {
                    return INST_ERR_OUT_OF_MEMORY;
                }

                // Execute command
                CmdStatus = INST_executeCommand(i, argc, argv, CmdOutput);
                if(CmdStatus) {
                    // The command returned a error
                    return CmdStatus;
                }
                
                if(*CmdOutput) {

                    if(FirstQueryFlag) {
                        // queries are seperated with ';'
                        output[strlen(output)] = ';';
                    } else {
                        FirstQueryFlag = 1;
                    }
                    // By default next command wont have response, so zero terminate output
                    output[strlen(output) + 1] = 0;
                    CmdOutput = &output[strlen(output) + 1];
                }

                // Command found and executed successfully
                break;
            }
        }

        if(CmdStatus) {
            // Parsed command wasnt found
            break;
        } else if(NextCmd) { // If there are more commands to parse
            
            // All commands that begin with '*' dont give root and they dont change the root
            if(*NextCmd != '*') {
                
                if(*NextCmd == ':') {
                    // Next command resets the root, ignore the first ':' of next command
                    NextCmd++;
                    RootPtr = NULL;
                    AsteriskFlag = 0;
                } else if(AsteriskFlag) {
                    // As '*' preserved the root, append root to next command if possible
                    AsteriskFlag = 0;
                    if(RootPtr) {
                        strcat(RootPtr, NextCmd);
                        NextCmd = RootPtr;
                    }
                } else {
                    // Next command is without root
                    RootPtr = strrchr(input, ':'); // Test for root
                    if(RootPtr) {
                        *(++RootPtr) = 0;          // Get root from current command
                        RootPtr = input;           
                        strcat(RootPtr, NextCmd);
                        NextCmd = RootPtr;         // Add up root and next command
                    }
                }
            } else { // Parse '*' command

                // Preserve root if possible
                RootPtr = strrchr(input, ':');
                if(RootPtr) {
                    *(++RootPtr) = 0;
                    RootPtr = input;
                }
                AsteriskFlag = 1;
            }

            input = NextCmd;
        } else {
            // All commands are parsed and executed
            return INST_ERR_NO;
        }
    }

    return CmdStatus;
}
