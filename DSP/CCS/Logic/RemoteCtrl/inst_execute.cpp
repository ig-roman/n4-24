/*
 * File:    inst_execute.c
 * Title:   
 * Created: Fri Aug 17 09:08:44 2007
 *
 */
#include "inst.h"

void INST_execute(char *input, char *output)
{
	CTRLMODE eMode = g_deviceState.GetConfig().GetCtrlModePub();
	if (CTRLMODE_REMOTE == eMode || CTRLMODE_MIXED == eMode)
	{
		g_tracker.Disable();
		int err = INST_parser(input, output);
		if (err == INST_ERR_NO)
		{
			g_deviceState.TriggerRemoteCmd();
		}
		else
		{
			g_tracker.Enable();
			INST_errorEnqueue(err);
		}
	}

}
