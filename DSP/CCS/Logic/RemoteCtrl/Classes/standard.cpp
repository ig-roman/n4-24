/*
 * File:    idn.c
 * Title:   
 * Created: Wed Feb 07 09:46:55 2007
 *
 */
#include <stdio.h>
#include "../inst.h"
#include "../../DeviceState.h"

#ifdef _WINDOWS
#pragma warning(disable:4996)
#endif

const char *s_pszIdnFmt = "\"%s,%s,%s\"";

int idnQ(int argc, char **argv, char *outb)
{
    sprintf(outb, s_pszIdnFmt, STR_ORG_NAME, STR_PRODUCT_NAME, STR_SW_VERSION);
    return INST_ERR_NO;
}

int rst(int argc, char **argv, char *outb)
{
	g_deviceState.SetCalibrationEnabled(true);
	g_deviceState.SetOperationMode(STATEMODE_U);
    return INST_ERR_NO;
}

int cls(int argc, char **argv, char *outb)
{
    INST_errorQueueCls();

    return INST_ERR_NO;
}

int opcQ(int argc, char **argv, char *outb)
{	// it's the dummy & should never be called
    return INST_ERR_NO;
}
