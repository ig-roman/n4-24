/*
* File:    memory.c
* Title:   
* Created: Thu Sep 13 15:06:24 2007
*
*/
#include "../inst.h"
#include "../fmt.h"
#include "../../../LogicInterface.h"

int eepromWrite(int argc, char **argv, char *outb)
{
	if(argc < 3)
	{
		return INST_ERR_MISSING_PARAMETER;
	}

	uint32_t addr = (uint32_t)strtoul(argv[0], NULL, 10);
	uint16_t type = (uint16_t)strtoul(argv[1], NULL, 10);

	if(addr > EEPROM_ADDR_END)
	{
	    return INST_ERR_SETTINGS_CONFLICT;
	}

	switch(type)
	{
		default:
		{
			return INST_ERR_ILLEGAL_PARAMETER_VALUE;
		}
		case MEM_TYPE_INT8:
		{
			uint8_t data8 = (uint8_t)strtol(argv[2], NULL, 10);
			CLogic_StoreByte(addr, data8);
			break;
		}
		case MEM_TYPE_DOUBLE:
		{
			double dVal = strtod(argv[2], NULL);
			double* pdVal = &dVal;
			CLogic_StoreBytes(addr, sizeof(double), (uint8_t*)(pdVal));
			break;
		}
	}

	return INST_ERR_NO;
}

int eepromReadQ(int argc, char **argv, char *outb)
{
	if(argc < 2)
	{
		return INST_ERR_MISSING_PARAMETER;
	}

	uint32_t addr = (uint32_t)strtoul(argv[0], NULL, 10);
	uint16_t type = (uint16_t)strtoul(argv[1], NULL, 10);

	if(addr > EEPROM_ADDR_END)
	{
	    return INST_ERR_SETTINGS_CONFLICT;
	}

	switch(type)
	{
		default:
		{
			return INST_ERR_ILLEGAL_PARAMETER_VALUE;
		}
		case MEM_TYPE_INT8:
		{
			uint8_t data8 = CLogic_RestoreByte(addr);
			FMT_sprintf(outb, "%d", (int)data8);
			break;
		}
		case MEM_TYPE_DOUBLE:
		{
			double dVal = 0;
			double*pdVal = &dVal;

			CLogic_RestoreBytes(addr, sizeof(double), (uint8_t*)(pdVal));
			FMT_sprintf(outb, "%e", dVal);

			break;
		}
	}

	return INST_ERR_NO;
}
