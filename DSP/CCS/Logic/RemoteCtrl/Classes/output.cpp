/*
 * File:    idn.c
 * Title:   
 * Created: Wed Feb 07 09:46:55 2007
 *
 */
#include "../inst.h"
#include "../fmt.h"
#include "../../../UI/PageManager.h"
#include "../../BandsManager.h"
#include <math.h>

extern const char *FMT_ImpSuff[FMT_IMP_SUFF_COUNT];
extern const double FMT_ImpMult[FMT_IMP_SUFF_COUNT];

int outputCoupling(int argc, char **argv, char *outb)
{
	int err = INST_ERR_SETTINGS_CONFLICT;

	if (argc < 1)
	{
		err = INST_ERR_MISSING_PARAMETER;
	}
	else
	{
		bool bIsDC;
		err = FMT_getIsOdd(argv[0], FMT_Coupling, FMT_COUPLING_COUNT, bIsDC);
		if (err == INST_ERR_NO)
		{
			if (STATEERR_NO_ERROR == g_deviceState.SetDirecionType(bIsDC))
			{
				err = INST_ERR_NO;
			}
		}
	}

	return err;
}

int outputCouplingQ(int argc, char **argv, char *outb)
{
	int err = INST_ERR_SETTINGS_CONFLICT;

	bool bIsDC;
	if (STATEERR_NO_ERROR == g_deviceState.GetDirecionType(bIsDC))
	{
		FMT_sprintf(outb, INST_OUTPUT_FMT_STRING, bIsDC ? FMT_Coupling[0] : FMT_Coupling[1]);
		err = INST_ERR_NO;
	}

	return err;
}

int outputImpedance(int argc, char **argv, char *outb)
{
	int err = INST_ERR_SETTINGS_CONFLICT;
	if (argc < 1)
	{
		err = INST_ERR_MISSING_PARAMETER;
	}
	else
	{
		double impedance;
		err = FMT_getNumericValue(argv[0], FMT_ImpSuff, FMT_ImpMult, FMT_IMP_SUFF_COUNT, &impedance, MIN_IMP_VALUE, MAX_IMP_VALUE);
		if (INST_ERR_NO == err)
		{
			if (STATEMODE_R != g_deviceState.GetOperationMode())
			{
				g_deviceState.SetOperationMode(STATEMODE_R);
			}

			if (STATEERR_NO_ERROR != g_deviceState.SetResistance(impedance))
			{
				err = INST_ERR_DATA_OUT_OF_RANGE;
			}
		}
	}

	return err;
}

int outputImpedanceQ(int argc, char **argv, char *outb)
{
	int err = INST_ERR_SETTINGS_CONFLICT;

	STATEMODE eMode = g_deviceState.GetOperationMode();
	if (STATEMODE_R == eMode)
	{
		WORKUNIT valueRet;
		if (STATEERR_NO_ERROR == g_deviceState.GetCorrectedValue(valueRet))
		{
			FMT_sprintf(outb, INST_OUTPUT_FMT_CURENT, valueRet.value);
			err = INST_ERR_NO;
		}
	}

	return err;
}

int outputPolarity(int argc, char **argv, char *outb)
{
	int err = INST_ERR_SETTINGS_CONFLICT;
	if (argc < 1)
	{
		err = INST_ERR_MISSING_PARAMETER;
	}
	else
	{
		bool bIsPos;
		err = FMT_getIsOdd(argv[0], FMT_Polarity, FMT_POLARITY_COUNT, bIsPos);
		if (err == INST_ERR_NO)
		{
			err = INST_ERR_SETTINGS_CONFLICT;
			STATEMODE eMode = g_deviceState.GetOperationMode();
			if (STATEMODE_I != eMode || STATEMODE_U != eMode)
			{
				bool bIsDC;
				if (STATEERR_NO_ERROR == g_deviceState.GetDirecionType(bIsDC) && bIsDC)
				{
					WORKUNIT wuValue;
					if (STATEERR_NO_ERROR == g_deviceState.GetValue(wuValue))
					{
						if (bIsPos != (wuValue.value >= 0))
						{
							wuValue.value = -wuValue.value;
							wuValue.pBand = BandsManager::InvertBand(eMode, wuValue.pBand);
							if (STATEERR_NO_ERROR == g_deviceState.SetValue(wuValue, true, NULL, true))
							{
								err = INST_ERR_NO;
							}
						}
						else
						{
							err = INST_ERR_NO;
						}
					}
				}
			}
		}
	}

	return err;
}

int outputPolarityQ(int argc, char **argv, char *outb)
{
	int err = INST_ERR_SETTINGS_CONFLICT;

	STATEMODE eMode = g_deviceState.GetOperationMode();
	if (STATEMODE_I != eMode || STATEMODE_U != eMode)
	{
		bool bIsDC;
		if (STATEERR_NO_ERROR == g_deviceState.GetDirecionType(bIsDC) && bIsDC)
		{
			WORKUNIT wuValue;
			if (STATEERR_NO_ERROR == g_deviceState.GetValue(wuValue))
			{
				FMT_sprintf(outb, INST_OUTPUT_FMT_STRING, wuValue.value >= 0 ? FMT_Polarity[0] : FMT_Polarity[1]);
				err = INST_ERR_NO;
			}
		}
	}

	return err;
}

int outputState(int argc, char **argv, char *outb)
{
	int err = INST_ERR_SETTINGS_CONFLICT;
	if (argc < 1)
	{
		err = INST_ERR_MISSING_PARAMETER;
	}
	else
	{
		bool state;
		err = FMT_getBoolean(argv[0], state);
		if(err == INST_ERR_NO)
		{
			g_deviceState.SwitchOut(state, true);
		}
	}

	return err;
}

int outputStateQ(int argc, char **argv, char *outb)
{
	if (g_deviceState.GetConfirmRequire())
		strcpy(outb, "-1");
	else
		FMT_sprintf(outb, INST_OUTPUT_FMT_BOOLEAN, g_deviceState.GetSwitchOutState());
	return INST_ERR_NO;
}

int outputSense(int argc, char **argv, char *outb)
{
	int err = INST_ERR_SETTINGS_CONFLICT;
	if (argc < 1)
		return INST_ERR_MISSING_PARAMETER;
	bool state;
	err = FMT_getBoolean(argv[0], state);
	if (err == INST_ERR_NO)
		g_deviceState.SetSenseMode(state ? STATESENSE_FOUR_WIRE : STATESENSE_TWO_WIRE);
	return err;
}

int outputSenseQ(int argc, char **argv, char *outb)
{
	STATESENSE sense;
	int err = g_deviceState.GetSenseMode(sense);
	if (err == INST_ERR_NO)
		FMT_sprintf(outb, INST_OUTPUT_FMT_BOOLEAN, sense == STATESENSE_FOUR_WIRE);
	return err;
}
