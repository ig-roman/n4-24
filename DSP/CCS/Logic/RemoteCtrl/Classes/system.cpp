/*
 * File:    system.c
 * Title:   
 * Created: Wed Feb 07 16:56:15 2007
 *
 */
#include "../inst.h"
#include "../fmt.h"

/* SYSTem:LOCal */
int systemLocal(int argc, char **argv, char *outb)
{
	g_deviceState.GetConfig().SetCtrlModePub(CTRLMODE_LOCAL, false);
	return INST_ERR_NO;
}

/* SYSTem:REMote */
int systemRemote(int argc, char **argv, char *outb)
{
	g_deviceState.GetConfig().SetCtrlModePub(CTRLMODE_MIXED, false);
	return INST_ERR_NO;
}

/* SYSTem:KLOCk */
int systemKlock(int argc, char **argv, char *outb)
{
	int err = INST_ERR_SETTINGS_CONFLICT;

	if (argc < 1)
	{
		err = INST_ERR_MISSING_PARAMETER;
	}
	else
	{
		bool state;
		err = FMT_getBoolean(argv[0], state);
		if(err == INST_ERR_NO)
		{
			g_deviceState.GetConfig().SetCtrlModePub(state ? CTRLMODE_REMOTE : CTRLMODE_MIXED, false);
		}
	}

	return err;
}

int systemKlockQ(int argc, char **argv, char *outb)
{
	bool bUILocked = CTRLMODE_REMOTE == g_deviceState.GetConfig().GetCtrlModePub();
	FMT_sprintf(outb, INST_OUTPUT_FMT_BOOLEAN, bUILocked);
	return INST_ERR_NO;
}

/* SYSTem:VERSion? */
static const char *SystemVersion = "1990.0";
int systemVersionQ(int argc, char **argv, char *outb)
{
    strcpy(outb, SystemVersion);

    return INST_ERR_NO;
}

/* SYSTem:ERRor:NEXT? */
int systemErrorNextQ(int argc, char **argv, char *outb)
{
	int err = INST_errorDequeue();
    switch(err) 
	{
        default:
            FMT_sprintf(outb, "%d, \"UNKNOWN\"", (int)err);
            break;
        case INST_ERR_NO:
            strcpy(outb, "0, \"No error\"");
            break;
        case INST_ERR_QUEUE_OVERFLOW:
            strcpy(outb, "-350, \"Queue overflow\"");
            break;
        case INST_ERR_DATA_TYPE_ERROR:
            strcpy(outb, "-104, \"Data type error\"");
            break;
        case INST_ERR_MISSING_PARAMETER:
            strcpy(outb, "-109, \"Missing parameter\"");
            break;
        case INST_ERR_COMMAND_HEADER_ERROR:
            strcpy(outb, "-110, \"Command header error\"");
            break;
        case INST_ERR_SUFFIX_ERROR:
            strcpy(outb, "-130, \"Suffix error\"");
            break;
        case INST_ERR_CHARACTER_DATA_ERROR:
            strcpy(outb, "-140, \"Character data error\"");
            break;
        case INST_ERR_STRING_DATA_ERROR:
            strcpy(outb, "-150, \"String data error\"");
            break;
        case INST_ERR_SETTINGS_CONFLICT:
            strcpy(outb, "-221, \"Settings conflict\"");
            break;
        case INST_ERR_DATA_OUT_OF_RANGE:
            strcpy(outb, "-222, \"Data out of range\"");
            break;
        case INST_ERR_ILLEGAL_PARAMETER_VALUE:
            strcpy(outb, "-224, \"Illegal parameter value\"");
            break;
        case INST_ERR_OUT_OF_MEMORY:
            strcpy(outb, "-291, \"Out of memory\"");
            break;
    }

    return INST_ERR_NO;
}

int systemStatusQ(int argc, char **argv, char *outb)
{
	int err;
	char modeStr[4];
	err = sourceModeQ(0, NULL, modeStr);
	if (err != INST_ERR_NO)
		return err;
	WORKUNIT valueRet;
	if (STATEERR_NO_ERROR != g_deviceState.GetOutputValue(valueRet))
		return INST_ERR_SETTINGS_CONFLICT;
	double lev = valueRet.value;
	double freq = 0;
	if (STATEERR_NO_ERROR == g_deviceState.GetFrequency(valueRet))
		freq = valueRet.value;
	FMT_sprintf(outb, "MODE "INST_OUTPUT_FMT_STRING";"
					  "LEV "INST_OUTPUT_FMT_VOLTAGE";"
					  "FREQ "INST_OUTPUT_FMT_FREQUENCY";"
					  "OUT "INST_OUTPUT_FMT_BOOLEAN,
					  modeStr, lev, freq, 
					  g_deviceState.GetConfirmRequire() ? -1 : g_deviceState.GetSwitchOutState());
	return INST_ERR_NO;
}
