/*
* File:    source.c
* Title:   
* Created: Mon Sep 03 09:44:54 2007
*
*/
#include "../inst.h"
#include "../fmt.h"
#include "../../../UI/PageManager.h"
#include "../Logic/BandsManager.h"
#include <math.h>

extern const char *FMT_TimeSuff[FMT_TIME_SUFF_COUNT];
extern const double FMT_TimeMult[FMT_TIME_SUFF_COUNT];
extern const char *FMT_FreqSuff[FMT_FREQ_SUFF_COUNT];
extern const double FMT_FreqMult[FMT_FREQ_SUFF_COUNT];
extern const char *FMT_VoltSuff[FMT_VOLT_SUFF_COUNT];
extern const double FMT_VoltMult[FMT_VOLT_SUFF_COUNT];

extern const char *FMT_CurSuff[FMT_CUR_SUFF_COUNT];
extern const double FMT_CurMult[FMT_CUR_SUFF_COUNT];

static bool isFixedBand = false;

int sourceVoltageAbsoluteDeviceLimit(int argc, char **argv, char *outb, bool bIsMax)
{
	STATEMODE eMode = g_deviceState.GetOperationMode();

	int nErrorRet = INST_ERR_SETTINGS_CONFLICT;
	if (STATEMODE_U == eMode)
	{
		double dLimitValue;
		if (STATEERR_NO_ERROR == g_deviceState.GetAbsoluteDeviceLimit(dLimitValue, bIsMax))
		{
			FMT_sprintf(outb, INST_OUTPUT_FMT_VOLTAGE, dLimitValue);
			nErrorRet = INST_ERR_NO;
		}
	}

	return nErrorRet;
}

int sourceVoltageMaximumQ(int argc, char **argv, char *outb)
{
	return sourceVoltageAbsoluteDeviceLimit(argc, argv, outb, true);
}

int sourceVoltageMinimumQ(int argc, char **argv, char *outb)
{
	return sourceVoltageAbsoluteDeviceLimit(argc, argv, outb, false);
}

int sourceCorrectionOFFSetQ(int argc, char **argv, char *outb)
{
	int err = INST_ERR_SETTINGS_CONFLICT;

	STATEMODE eMode = g_deviceState.GetOperationMode();
	if (STATEMODE_U == eMode || STATEMODE_I == eMode)
	{
		WORKUNIT valueCor;
		WORKUNIT value;
		if (STATEERR_NO_ERROR == g_deviceState.GetCorrectedValue(valueCor) 
			&& STATEERR_NO_ERROR == g_deviceState.GetOutputValue(value))
		{
			FMT_sprintf(outb, INST_OUTPUT_FMT_FREQUENCY, valueCor.value - value.value);
			err = INST_ERR_NO;
		}
	}

	return err;
}

int sourceCorrectionState(int argc, char **argv, char *outb)
{
	int err = INST_ERR_SETTINGS_CONFLICT;

	if (argc < 1)
	{
		err = INST_ERR_MISSING_PARAMETER;
	}
	else
	{
		bool state;
		err = FMT_getBoolean(argv[0], state);
		if(err == INST_ERR_NO)
		{
			g_deviceState.SetCalibrationEnabled(state);
		}
	}

	return err;
}

int sourceCorrectionStateQ(int argc, char **argv, char *outb)
{
	FMT_sprintf(outb, INST_OUTPUT_FMT_BOOLEAN, g_deviceState.GetCalibrationEnabled());
	return INST_ERR_NO;
}

/* SOURce:FREQuency */
int sourceFrequency(int argc, char **argv, char *outb)
{
	int err = INST_ERR_SETTINGS_CONFLICT;
	if (argc < 1)
	{
		err = INST_ERR_MISSING_PARAMETER;
	}
	else
	{
		double frequency;
		err = FMT_getNumericValue(argv[0], FMT_FreqSuff, FMT_FreqMult, FMT_FREQ_SUFF_COUNT, &frequency, MIN_FREQUENCY_VALUE, MAX_FREQUENCY_VALUE);
		if (INST_ERR_NO == err)
		{
			if (STATEERR_NO_ERROR != g_deviceState.SetFrequency(frequency, true))
			{
				err = INST_ERR_SETTINGS_CONFLICT;
			}
		}
	}

	return err;
}

int sourceFrequencyQ(int argc, char **argv, char *outb)
{
	int err = INST_ERR_SETTINGS_CONFLICT;

	WORKUNIT valueRet;
	if (STATEERR_NO_ERROR == g_deviceState.GetFrequency(valueRet))
	{
		FMT_sprintf(outb, INST_OUTPUT_FMT_FREQUENCY, valueRet.value);
		err = INST_ERR_NO;
	}

	return err;
}

/* SOURce:VOLTage */
int sourceVoltage(int argc, char **argv, char *outb)
{
	if (argc < 1)
		return INST_ERR_MISSING_PARAMETER;

	double voltage;
	int err = FMT_getNumericValue(argv[0], FMT_VoltSuff, FMT_VoltMult, FMT_VOLT_SUFF_COUNT, &voltage, MIN_VOLTAGE_DC_VALUE, MAX_VOLTAGE_DC_VALUE);
	if (INST_ERR_NO != err)
		return err;
	if (STATEMODE_U != g_deviceState.GetOperationMode())
	{
		g_deviceState.SetOperationMode(STATEMODE_U);
		isFixedBand = false;
	}
	if (STATEERR_NO_ERROR != g_deviceState.SetValue(voltage, -1, !isFixedBand))
		return INST_ERR_SETTINGS_CONFLICT;
	return INST_ERR_NO;
}

int sourceVoltageQ(int argc, char **argv, char *outb)
{
	int err = INST_ERR_SETTINGS_CONFLICT;

	STATEMODE eMode = g_deviceState.GetOperationMode();
	if (STATEMODE_U == eMode)
	{
		WORKUNIT valueRet;
		if (STATEERR_NO_ERROR == g_deviceState.GetOutputValue(valueRet))
		{
			FMT_sprintf(outb, INST_OUTPUT_FMT_VOLTAGE, valueRet.value);
			err = INST_ERR_NO;
		}
	}

	return err;
}

int sourceCurrent(int argc, char **argv, char *outb)
{
	if (argc < 1)
		return INST_ERR_MISSING_PARAMETER;

	double curent;
	int err = FMT_getNumericValue(argv[0], FMT_CurSuff, FMT_CurMult, FMT_CUR_SUFF_COUNT, &curent, MIN_CURENT_VALUE, MAX_CURENT_VALUE);
	if (INST_ERR_NO != err)
		return err;
	if (STATEMODE_I != g_deviceState.GetOperationMode())
	{
		g_deviceState.SetOperationMode(STATEMODE_I);
		isFixedBand = false;
	}

	if (STATEERR_NO_ERROR != g_deviceState.SetValue(curent, -1, !isFixedBand))
		return INST_ERR_SETTINGS_CONFLICT;

	return INST_ERR_NO;
}

int sourceCurrentQ(int argc, char **argv, char *outb)
{
	int err = INST_ERR_SETTINGS_CONFLICT;

	STATEMODE eMode = g_deviceState.GetOperationMode();
	if (STATEMODE_I == eMode)
	{
		WORKUNIT valueRet;
		if (STATEERR_NO_ERROR == g_deviceState.GetOutputValue(valueRet))
		{
			FMT_sprintf(outb, INST_OUTPUT_FMT_CURENT, valueRet.value);
			err = INST_ERR_NO;
		}
	}

	return err;
}

int sourceModeQ(int argc, char **argv, char *outb)
{
	STATEMODE eMode = g_deviceState.GetOperationMode();
	bool bIsDC;
	g_deviceState.GetDirecionType(bIsDC);
	switch(eMode)
	{
	case STATEMODE_U:
		if (bIsDC) 
			strcpy(outb, "DCU");
		else
			strcpy(outb, "ACU");
		break;
	case STATEMODE_I:
		if (bIsDC) 
			strcpy(outb, "DCI");
		else
			strcpy(outb, "ACI");
		break;
	case STATEMODE_R:
			strcpy(outb, "IMP");
		break;
	default:
		return INST_ERR_SETTINGS_CONFLICT;
	}
	return INST_ERR_NO;
}

int sourceBandQ(int argc, char **argv, char *outb)
{
	WORKUNIT valueRet;
	if (STATEERR_NO_ERROR != g_deviceState.GetOutputValue(valueRet))
		return INST_ERR_SETTINGS_CONFLICT;
	FMT_sprintf(outb, INST_OUTPUT_FMT_INTEGER, valueRet.pBand->bandIdx + 1);
	return INST_ERR_NO;
}

int sourceBand(int argc, char **argv, char *outb)
{
	int bandIdx;
	if (argc < 1)
		return INST_ERR_MISSING_PARAMETER;
	if (!strcmp(argv[0], "DEF") || !strcmp(argv[0], "def"))
	{
		isFixedBand = false;
	}
	else
	{
		bandIdx = atoi(argv[0]);
		if (!bandIdx)
			return INST_ERR_ILLEGAL_PARAMETER_VALUE;
		isFixedBand = true;
	}
	WORKUNIT value;
	if (STATEERR_NO_ERROR != g_deviceState.GetOutputValue(value))
		return INST_ERR_SETTINGS_CONFLICT;
	if (!isFixedBand)
	{
		value.pBand = NULL;
		g_deviceState.SetValue(value, false, NULL, false, false);
		return INST_ERR_NO;
	}
	STATEMODE eMode = g_deviceState.GetOperationMode();
	if (eMode == STATEMODE_R)
		return INST_ERR_SETTINGS_CONFLICT;
	bool bIsDC;
	g_deviceState.GetDirecionType(bIsDC);
	PCBAND arrBands = BandsManager::GetBandsArray(bIsDC, eMode == STATEMODE_U);
	if (bandIdx > BandsManager::GetBandsCount(arrBands))
		return INST_ERR_DATA_OUT_OF_RANGE;
	value.pBand = &arrBands[bandIdx - 1];
	if (g_deviceState.SetValue(value, true, NULL, false, false) != STATEERR_NO_ERROR)
	{
		value.value = value.pBand->min;
		if (g_deviceState.SetValue(value, true, NULL, false, false) != STATEERR_NO_ERROR)
		{
			return INST_ERR_SETTINGS_CONFLICT;
		}
	}
	return INST_ERR_NO;
}

int sourceBandMinimumQ(int argc, char **argv, char *outb)
{
	WORKUNIT valueRet;
	if (STATEERR_NO_ERROR != g_deviceState.GetOutputValue(valueRet))
		return INST_ERR_SETTINGS_CONFLICT;
	FMT_sprintf(outb, INST_OUTPUT_FMT_VOLTAGE, valueRet.pBand->min);
	return INST_ERR_NO;
}

int sourceBandMaximumQ(int argc, char **argv, char *outb)
{
	WORKUNIT valueRet;
	if (STATEERR_NO_ERROR != g_deviceState.GetOutputValue(valueRet))
		return INST_ERR_SETTINGS_CONFLICT;
	FMT_sprintf(outb, INST_OUTPUT_FMT_VOLTAGE, valueRet.pBand->max);
	return INST_ERR_NO;
}
