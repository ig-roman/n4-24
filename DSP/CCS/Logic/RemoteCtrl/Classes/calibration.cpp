/*
 * File:    calibration.cpp
 * Title:   
 * Created: Fry Apr 12 11:28:15 2019
 *
 */
#include "../inst.h"
#include "../fmt.h"
#include "../../../UI/Pages/Settings/PasswordPage.h"
#include "../../../UI/Pages/Settings/Calibration/AdjustLevelsPage.h"
#include "../../../UI/Pages/Settings/Calibration/ResistorsCalibrationPage.h"
#include "../../../UI/Pages/Settings/Calibration/ContextBase.h"
#include "../../../UI/Pages/Settings/Calibration/ContextDCU.h"
#include "../../../UI/Pages/Settings/Calibration/ContextDCI.h"
#include "../../../UI/Pages/Settings/Calibration/ContextACU.h"
#include "../../../UI/Pages/Settings/Calibration/ContextACI.h"
#include "../../../UI/Pages/Settings/Calibration/ContextR.h"
#include "../../../UI/Pages/Settings/Calibration/WithContext.h"
#include "../../../UI/PageManager.h"

#define DEFAULT_PAGE PAGE_MODE_U

extern PSWSTATE g_enumPasswordState;
static CALIB_TYPE s_eCalibType = CALIB_TYPE_NONE;
static bool s_bIsCalibrating = false;

int getCalibrationFreqsCount(int nPointIdx)
{
	ContextBase *ctx = ((AdjustLevelsPage *)g_pageManager.GetActivePage())->GetCtx();
	return ctx->GetIndexData()->GetFrequencyCount(ctx->GetBandIdx(), nPointIdx);
}

int getCalibrationLevelsCount()
{
	ContextBase *ctx = ((AdjustLevelsPage *)g_pageManager.GetActivePage())->GetCtx();
	return ctx->GetIndexData()->GetPointsCount(ctx->GetBandIdx());
}

void getCalibrationPoint(int *levPointIdx, int *freqPointIdx)
{
	uint16_t nCalibrationStep, nStartOffset;
	MODE eMode;
	uint8_t unBandIdx, unPointIdx, unFreqIdx;

	ContextBase *ctx = ((AdjustLevelsPage *)g_pageManager.GetActivePage())->GetCtx();
	nCalibrationStep = ((AdjustLevelsPage *)g_pageManager.GetActivePage())->GetCalibrationStep();
	gs_pointsIndexer.FindOffsetByPoint(ctx->GetOpMode(), ctx->GetBandIdx(), 0, 0, nStartOffset);
	gs_pointsIndexer.FindCaibrationPointByOffset(nStartOffset + nCalibrationStep, eMode, unBandIdx, unFreqIdx, unPointIdx);
	*levPointIdx = unPointIdx;
	*freqPointIdx = unFreqIdx;
}

int calibrationSecureState(int argc, char **argv, char *outb)
{
	int err;
	bool state;
	char *pszPass;

	if (argc < 1)
		return INST_ERR_MISSING_PARAMETER;
	err = FMT_getBoolean(argv[0], state);
	if (err != INST_ERR_NO)
		return err;
	if (!state)
	{
		if (argc < 2)
			return INST_ERR_MISSING_PARAMETER;
		pszPass = argv[1];
		if (pszPass && strcmp(g_deviceState.GetConfig().GetPassword(false), pszPass) == 0)
			g_enumPasswordState = PSWSTATE_VALID;
		else
			return INST_ERR_ILLEGAL_PARAMETER_VALUE;
	}
	else
		g_enumPasswordState = PSWSTATE_INVALID;
	return INST_ERR_NO;
}

int calibrationSecureStateQ(int argc, char **argv, char *outb)
{
	FMT_sprintf(outb, INST_OUTPUT_FMT_BOOLEAN, g_enumPasswordState != PSWSTATE_VALID);
	return INST_ERR_NO;
}

int calibrationState(int argc, char **argv, char *outb)
{
	bool state;
	PAGE enumPage;
	ContextBase* pContextBase;
	int err;

	if (g_enumPasswordState != PSWSTATE_VALID)
		return INST_ERR_SETTINGS_CONFLICT;
	if (argc < 1)
		return INST_ERR_MISSING_PARAMETER;
	err = FMT_getBoolean(argv[0], state);
	if (err != INST_ERR_NO)
		return err;
	if (state)
	{
		switch (s_eCalibType)
		{
		case CALIB_TYPE_DCU:
			enumPage = PAGE_ADJUST_LEVELS;
			pContextBase = new ContextDCU;
			break;
		case CALIB_TYPE_ACU:
			enumPage = PAGE_ADJUST_LEVELS;
			pContextBase = new ContextACU;
			break;
		case CALIB_TYPE_DCI:
			enumPage = PAGE_ADJUST_LEVELS;
			pContextBase = new ContextDCI;
			break;
		case CALIB_TYPE_ACI:
			enumPage = PAGE_ADJUST_LEVELS;
			pContextBase = new ContextACI;
			break;
		default:
			enumPage = DEFAULT_PAGE;
			break;
		}
	}
	else
		enumPage = DEFAULT_PAGE;
	if ((s_eCalibType == CALIB_TYPE_DCU || s_eCalibType == CALIB_TYPE_ACU) &&
		g_deviceState.GetOperationMode() != STATEMODE_U)
	{
		g_deviceState.SetOperationMode(STATEMODE_U);
		g_deviceState.TriggerRemoteCmd();
		g_pageManager.Loop();
	}
	if ((s_eCalibType == CALIB_TYPE_DCI || s_eCalibType == CALIB_TYPE_ACI) &&
		g_deviceState.GetOperationMode() != STATEMODE_I)
	{
		g_deviceState.SetOperationMode(STATEMODE_I);
		g_deviceState.TriggerRemoteCmd();
		g_pageManager.Loop();
	}
	BasePage *pPage = g_pageManager.CreatePage(enumPage);
	if (enumPage != DEFAULT_PAGE)
	{
		pContextBase->SetBandIdx(0);
		pContextBase->SetSenseState(STATESENSE_TWO_WIRE);
		s_bIsCalibrating = true;
		g_deviceState.GetConfig().SetCtrlModePub(CTRLMODE_REMOTE, true);
	}
	else
	{
		s_bIsCalibrating = false;
		g_deviceState.GetConfig().SetCtrlModePub(CTRLMODE_MIXED, true);
		g_deviceState.SetOperationMode(STATEMODE_U);
	}
	if (enumPage == PAGE_ADJUST_LEVELS)
		((AdjustLevelsPage *)pPage)->SetCalibrationContext(pContextBase);
	g_pageManager.SetNewPage(pPage);
	return INST_ERR_NO;
}

int calibrationStateQ(int argc, char **argv, char *outb)
{
	FMT_sprintf(outb, INST_OUTPUT_FMT_BOOLEAN, s_bIsCalibrating);
	return INST_ERR_NO;
}

int calibrationType(int argc, char **argv, char *outb)
{
	if (argc < 1)
		return INST_ERR_MISSING_PARAMETER;
	if (s_bIsCalibrating)
		return INST_ERR_SETTINGS_CONFLICT;
	if (FMT_getCharacterData(argv[0], FMT_calibType, FMT_CALIB_TYPE_COUNT, (unsigned int *)&s_eCalibType))
		return INST_ERR_ILLEGAL_PARAMETER_VALUE;
	return INST_ERR_NO;
}

int calibrationTypeQ(int argc, char **argv, char *outb)
{
	strcpy(outb, FMT_calibType[s_eCalibType]);
	return INST_ERR_NO;
}

int calibrationSense(int argc, char **argv, char *outb)
{
	int err;
	bool state;

	if (argc < 1)
		return INST_ERR_MISSING_PARAMETER;
	if (err = FMT_getBoolean(argv[0], state))
		return err;
	if (s_bIsCalibrating)
	{
		AdjustLevelsPage *activePage = (AdjustLevelsPage *)g_pageManager.GetActivePage();
		ContextBase *ctx = activePage->GetCtx();
		if (ctx->IsSensePossible())
		{
			ctx->SetSenseState(state ? STATESENSE_FOUR_WIRE : STATESENSE_TWO_WIRE);
			AdjustLevelsPage *newPage = (AdjustLevelsPage *)g_pageManager.CreatePage(PAGE_ADJUST_LEVELS);
			activePage->MoveCalibrationContextTo(newPage);
			g_pageManager.SetNewPage(newPage);
			return INST_ERR_NO;
		}
	}
	return INST_ERR_SETTINGS_CONFLICT;
}

int calibrationSenseQ(int argc, char **argv, char *outb)
{
	bool state = false;

	if (s_bIsCalibrating)
		state = ((AdjustLevelsPage *)g_pageManager.GetActivePage())->GetCtx()->GetSenseState() == STATESENSE_FOUR_WIRE;
	FMT_sprintf(outb, INST_OUTPUT_FMT_BOOLEAN, state);
	return INST_ERR_NO;
}

int calibrationBandsCountQ(int argc, char **argv, char *outb)
{
	int count = 0;

	if (s_bIsCalibrating)
		count = ((AdjustLevelsPage *)g_pageManager.GetActivePage())->GetCtx()->GetIndexData()->GetBandCount();
	FMT_sprintf(outb, INST_OUTPUT_FMT_INTEGER, count);
	return INST_ERR_NO;
}

int calibrationBandsBaseQ(int argc, char **argv, char *outb)
{
	int idx = 0, err;
	bool isPositive;

	if (argc < 1)
		isPositive = true;
	else if (err = FMT_getBoolean(argv[0], isPositive))
		return err;
	if (s_bIsCalibrating)
		idx = GetBaseCurveBandIdx(((AdjustLevelsPage *)g_pageManager.GetActivePage())->GetCtx()->GetOpMode(), isPositive) + 1;
	FMT_sprintf(outb, INST_OUTPUT_FMT_INTEGER, idx);
	return INST_ERR_NO;
}

int calibrationBand(int argc, char **argv, char *outb)
{
	int idx, err;

	if (argc < 1)
		return INST_ERR_MISSING_PARAMETER;
	if (err = FMT_getInteger(argv[0], &idx))
		return err;
	if (s_bIsCalibrating)
	{
		AdjustLevelsPage *activePage = (AdjustLevelsPage *)g_pageManager.GetActivePage();
		ContextBase *ctx = activePage->GetCtx();
		if (idx > ctx->GetIndexData()->GetBandCount())
			return INST_ERR_DATA_OUT_OF_RANGE;
		ctx->SetBandIdx(idx - 1);
		if (ctx->IsSensePossible())
			ctx->SetSenseState(gs_calibrationManager.GetPointsManager().GetBandSense(ctx->GetOpMode(), idx - 1));
		AdjustLevelsPage *newPage = (AdjustLevelsPage *)g_pageManager.CreatePage(PAGE_ADJUST_LEVELS);
		activePage->MoveCalibrationContextTo(newPage);
		g_pageManager.SetNewPage(newPage);
		return INST_ERR_NO;
	}
	return INST_ERR_SETTINGS_CONFLICT;
}

int calibrationBandQ(int argc, char **argv, char *outb)
{
	int idx = 0;

	if (s_bIsCalibrating)
		idx = ((AdjustLevelsPage *)g_pageManager.GetActivePage())->GetCtx()->GetBandIdx() + 1;
	FMT_sprintf(outb, INST_OUTPUT_FMT_INTEGER, idx);
	return INST_ERR_NO;
}

int calibrationBandLevelsQ(int argc, char **argv, char *outb)
{
	int count = 0;

	if (s_bIsCalibrating)
		count = getCalibrationLevelsCount();
	FMT_sprintf(outb, INST_OUTPUT_FMT_INTEGER, count);
	return INST_ERR_NO;
}

int calibrationBandFreqsQ(int argc, char **argv, char *outb)
{
	int count = 0, levelIdx = 0, err;

	if (s_bIsCalibrating)
	{
		if (argc >= 1)
		{
			if (err = FMT_getInteger(argv[0], &levelIdx))
				return err;
		}
		count = getCalibrationFreqsCount(levelIdx - 1);
	}
	FMT_sprintf(outb, INST_OUTPUT_FMT_INTEGER, count);
	return INST_ERR_NO;
}

int calibrationBandMaxQ(int argc, char **argv, char *outb)
{
	double max = 0;

	if (s_bIsCalibrating)
	{
		ContextBase *ctx = ((AdjustLevelsPage *)g_pageManager.GetActivePage())->GetCtx();
		max = ctx->GetIndexData()->GetBandCalibrationLimit(ctx->GetIndexData()->m_pBands + ctx->GetBandIdx(), false, 0, false);
	}
	FMT_sprintf(outb, "%f", max);
	return INST_ERR_NO;
}

int calibrationPoint(int argc, char **argv, char *outb)
{
	int nPointIdx, nFreqIdx, err, nFreqsCount;
	uint16_t nStartPoint, nCurrentPoint;

	if (argc < 2)
		return INST_ERR_MISSING_PARAMETER;
	if (!s_bIsCalibrating)
		return INST_ERR_SETTINGS_CONFLICT;

	if (err = FMT_getInteger(argv[0], &nPointIdx))
		return err;
	if (err = FMT_getInteger(argv[1], &nFreqIdx))
		return err;

	if (nPointIdx > getCalibrationLevelsCount())
		return INST_ERR_DATA_OUT_OF_RANGE;
	nFreqsCount = getCalibrationFreqsCount(nPointIdx - 1);
	if (nFreqIdx > nFreqsCount)
		return INST_ERR_DATA_OUT_OF_RANGE;
	ContextBase *ctx = ((AdjustLevelsPage *)g_pageManager.GetActivePage())->GetCtx();
	gs_pointsIndexer.FindOffsetByPoint(ctx->GetOpMode(), ctx->GetBandIdx(), 0, 0, nStartPoint);
	gs_pointsIndexer.FindOffsetByPoint(ctx->GetOpMode(), ctx->GetBandIdx(), nFreqIdx - 1, nPointIdx - 1, nCurrentPoint);
	((AdjustLevelsPage *)g_pageManager.GetActivePage())->SelectStep(nCurrentPoint - nStartPoint);

	return INST_ERR_NO;
}

int calibrationPointQ(int argc, char **argv, char *outb)
{
	int nPointIdx = 0, nFreqIdx = 0;

	if (s_bIsCalibrating)
		getCalibrationPoint(&nPointIdx, &nFreqIdx);
	FMT_sprintf(outb, "%d,%d", nPointIdx + 1, nFreqIdx + 1);

	return INST_ERR_NO;
}

int calibrationLevelQ(int argc, char **argv, char *outb)
{
	double level = 0.0;
	int nPointIdx = 0, nFreqIdx = 0;

	if (s_bIsCalibrating)
	{
		getCalibrationPoint(&nPointIdx, &nFreqIdx);
		WORKUNIT wuValue = { 0 };
		ContextBase *ctx = ((AdjustLevelsPage *)g_pageManager.GetActivePage())->GetCtx();
		ctx->GetIndexData()->GetCalibrationPoint(ctx->GetBandIdx(), nPointIdx, nFreqIdx, &wuValue);
		level = wuValue.value;
	}
	FMT_sprintf(outb, INST_OUTPUT_FMT_VOLTAGE, level);
	return INST_ERR_NO;
}

int calibrationFrequencyQ(int argc, char **argv, char *outb)
{
	double freq = 0.0;
	int nPointIdx = 0, nFreqIdx = 0;

	if (s_bIsCalibrating)
	{
		getCalibrationPoint(&nPointIdx, &nFreqIdx);
		ContextBase *ctx = ((AdjustLevelsPage *)g_pageManager.GetActivePage())->GetCtx();
		freq = ctx->GetIndexData()->GetFrequency(ctx->GetBandIdx(), nPointIdx, nFreqIdx);
	}
	FMT_sprintf(outb, INST_OUTPUT_FMT_VOLTAGE, freq);
	return INST_ERR_NO;
}

int calibrationValue(int argc, char **argv, char *outb)
{
	if (argc < 1)
		return INST_ERR_MISSING_PARAMETER;
	if (!s_bIsCalibrating)
		return INST_ERR_SETTINGS_CONFLICT;

	double val = strtod(argv[0], NULL);
	((AdjustLevelsPage *)g_pageManager.GetActivePage())->SetPointDelta(val);

	return INST_ERR_NO;
}

int calibrationValueQ(int argc, char **argv, char *outb)
{
	if (!s_bIsCalibrating)
		return INST_ERR_SETTINGS_CONFLICT;

	double val = ((AdjustLevelsPage *)g_pageManager.GetActivePage())->GetPointDelta();
	FMT_sprintf(outb, INST_OUTPUT_FMT_VOLTAGE, val);
	return INST_ERR_NO;
}

int calibrateQ(int argc, char **argv, char *outb)
{
	if (!s_bIsCalibrating)
	{
		strcpy(outb, "1");
		return INST_ERR_SETTINGS_CONFLICT;
	}
	((AdjustLevelsPage *)g_pageManager.GetActivePage())->SaveCalibrationPoint(false);
	((AdjustLevelsPage *)g_pageManager.GetActivePage())->HideError();
	strcpy(outb, "0");
	return INST_ERR_NO;
}
