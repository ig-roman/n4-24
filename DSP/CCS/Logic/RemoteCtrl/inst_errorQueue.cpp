/*
 * File:    inst_errorQueue.c
 * Title:   
 * Created: Fri Aug 17 09:09:01 2007
 *
 */
#include "inst.h"

static int ErrorQueue[INST_ERR_QUEUE_SIZE];
static int ErrorQueueCount = 0;

void INST_errorQueueCls(void)
{
    ErrorQueueCount = 0;
}
void INST_errorEnqueue(int err)
{
    if(ErrorQueueCount >= INST_ERR_QUEUE_SIZE - 1) {
        ErrorQueue[INST_ERR_QUEUE_SIZE - 1] = INST_ERR_QUEUE_OVERFLOW;
        ErrorQueueCount = INST_ERR_QUEUE_SIZE;
    } else {
       ErrorQueue[ErrorQueueCount++] = err;
    }
}
int INST_errorDequeue(void)
{
int err, i;

    if(ErrorQueueCount) {
        err = ErrorQueue[0];
        for(i = 0; i < ErrorQueueCount; i++) {
            ErrorQueue[i] = ErrorQueue[i + 1];
        }
        ErrorQueueCount--;   
    } else {
        err = INST_ERR_NO;
    }

    return err;
}

