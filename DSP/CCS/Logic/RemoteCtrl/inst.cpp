/*
	* File:    inst.c
	* Title:   
	* Created: Tue Feb 06 16:13:27 2007
	*
	*/
#include "inst.h"
#include "sysh.h"
#include <string.h>
#include "../../UI/PageManager.h"

extern int AMP_stby;

#define REMCMD_DEF( fn, string_value)  fn,
extern const FNSysh s_arrSyshFn[] = { REMCMDS_DEF };
#undef REMCMD_DEF

static uint32_t delayTimer = 0;

int INST_executeCommand(int cmd, int argc, char **argv, char *outb)
{
	if (!cmd) // *OPC?
	{
		if (GetTime() < delayTimer)
			strcpy(outb, "0");
		else
			strcpy(outb, "1");
		return INST_ERR_NO;
	}
	while (GetTime() < delayTimer);
	delayTimer = GetTime();// + 1000; // ms
	g_pageManager.HideFrames();
	return s_arrSyshFn[cmd]( argc, argv, outb);
}
