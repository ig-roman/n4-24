/*
 * File:    fmt.h
 * Title:   
 * Created: Wed Jan 24 13:59:32 2007
 *
 */

#ifndef FMT_H
#define FMT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#ifdef __cplusplus
extern "C" {
#endif

#define FMT_UNIT_VOLTAGE_100V           1
#define FMT_UNIT_VOLTAGE_10V            2
#define FMT_UNIT_VOLTAGE_1V             3
#define FMT_UNIT_VOLTAGE_100MV          4
#define FMT_UNIT_VOLTAGE_10MV           5
#define FMT_UNIT_VOLTAGE_1MV            6
#define FMT_UNIT_VOLTAGE_100UV          7
#define FMT_UNIT_VOLTAGE_10UV           8
#define FMT_UNIT_VOLTAGE_1UV            9
#define FMT_UNIT_VOLTAGE_10DB           20
#define FMT_UNIT_VOLTAGE_1DB            21
#define FMT_UNIT_FREQUENCY_GHZ          30
#define FMT_UNIT_FREQUENCY_100MHZ       31
#define FMT_UNIT_FREQUENCY_10MHZ        32
#define FMT_UNIT_FREQUENCY_1MHZ         33
#define FMT_UNIT_FREQUENCY_100KHZ       34
#define FMT_UNIT_FREQUENCY_10KHZ        35
#define FMT_UNIT_FREQUENCY_1KHZ         36
#define FMT_UNIT_FREQUENCY_100HZ        37
#define FMT_UNIT_FREQUENCY_10HZ         38

#define FMT_UNIT_FREQUENCY_MAX    FMT_UNIT_FREQUENCY_GHZ
#define FMT_UNIT_FREQUENCY_MIN    FMT_UNIT_FREQUENCY_10HZ
#define FMT_UNIT_VOLTAGE_MAX      FMT_UNIT_VOLTAGE_100V
#define FMT_UNIT_VOLTAGE_MIN      FMT_UNIT_VOLTAGE_1UV
#define FMT_UNIT_VOLTAGE_DB_MAX   FMT_UNIT_VOLTAGE_10DB
#define FMT_UNIT_VOLTAGE_DB_MIN   FMT_UNIT_VOLTAGE_1DB

/* 6 digits */
#define FMT_DISP_VOLTAGE_SIZE           6

/* xxx.xxx => 100V */
#define FMT_VOLTAGE_DP_100V             2
#define FMT_VOLTAGE_MULT_100V           (double)1.0e+3     
/* xx.xxxx => 10V */
#define FMT_VOLTAGE_DP_10V              1
#define FMT_VOLTAGE_MULT_10V            (double)1.0e+4
/* x.xxxxx => V */
#define FMT_VOLTAGE_DP_1V               0
#define FMT_VOLTAGE_MULT_1V             (double)1.0e+5
/* xxx.xxx => 100mV */
#define FMT_VOLTAGE_DP_100MV            2
#define FMT_VOLTAGE_MULT_100MV          (double)1.0e+6     
/* xx.xxxx => 10mV */
#define FMT_VOLTAGE_DP_10MV             1
#define FMT_VOLTAGE_MULT_10MV           (double)1.0e+7
/* x.xxxxx => 1mV */
#define FMT_VOLTAGE_DP_1MV              0
#define FMT_VOLTAGE_MULT_1MV            (double)1.0e+8
/* xxx.xxx => 100uV */
#define FMT_VOLTAGE_DP_100UV            2
#define FMT_VOLTAGE_MULT_100UV          (double)1.0e+9
/* xx.xxxx => 10uV */
#define FMT_VOLTAGE_DP_10UV             1
#define FMT_VOLTAGE_MULT_10UV           (double)1.0e+10
/* x.xxxxx => 1uV */
#define FMT_VOLTAGE_DP_1UV              0
#define FMT_VOLTAGE_MULT_1UV            (double)1.0e+11
/* -xx.xxx => 10dB */
#define FMT_VOLTAGE_DP_10DB             2
#define FMT_VOLTAGE_MULT_10DB           (double)1.0e+3
/* -x.xxxx => 1dB */
#define FMT_VOLTAGE_DP_1DB              1
#define FMT_VOLTAGE_MULT_1DB            (double)1.0e+4

/* 7 digits */
#define FMT_DISP_FREQUENCY_SIZE         7
/* x.xxxxxx => GHz */
#define FMT_FREQUENCY_DP_GHZ            0
#define FMT_FREQUENCY_MULT_GHZ          (double)1.0e-3
/* xxx.xxxx => 100MHz */
#define FMT_FREQUENCY_DP_100MHZ         2
#define FMT_FREQUENCY_MULT_100MHZ       (double)1.0e-2
/* xx.xxxxx => 10MHz */
#define FMT_FREQUENCY_DP_10MHZ          1
#define FMT_FREQUENCY_MULT_10MHZ        (double)1.0e-1
/* x.xxxxxx => 1MHz */
#define FMT_FREQUENCY_DP_1MHZ           0
#define FMT_FREQUENCY_MULT_1MHZ         (double)1.0e-0
/* xxx.xxxx => 100kHz */
#define FMT_FREQUENCY_DP_100KHZ         2
#define FMT_FREQUENCY_MULT_100KHZ       (double)1.0e+1
/* xx.xxxxx => 10kHz */
#define FMT_FREQUENCY_DP_10KHZ          1
#define FMT_FREQUENCY_MULT_10KHZ        (double)1.0e+2
/* x.xxxxxx => 1kHz */
#define FMT_FREQUENCY_DP_1KHZ           0
#define FMT_FREQUENCY_MULT_1KHZ         (double)1.0e+3
/* xxx.xxxx => 100Hz */
#define FMT_FREQUENCY_DP_100HZ          2
#define FMT_FREQUENCY_MULT_100HZ        (double)1.0e+4
/* xx.xxxxx => 10Hz */
#define FMT_FREQUENCY_DP_10HZ           1
#define FMT_FREQUENCY_MULT_10HZ         (double)1.0e+5

#define FMT_UNIT_FREQUENCY_POS_DEF      0
#define FMT_FREQUENCY_FMT_HZ            "%07ld"
#define FMT_VOLTAGE_FMT_V               "%06ld"
#define FMT_VOLTAGE_FMT_DB              "%06ld"
#define FMT_UNIT_VOLTAGE_POS_DEF        0
#define FMT_UNIT_VOLTAGE_DB_POS_DEF     1

#define FMT_DISP_BUF_MAX_SIZE FMT_DISP_FREQUENCY_SIZE

double FMT_dispBufferToFloat(char *buf, unsigned int unit);
void FMT_floatToDispBuffer(double voltage, char *buf, unsigned int unit);
unsigned int FMT_voltageToUnit(double volt);
unsigned int FMT_dbToUnit(double db);
unsigned int FMT_frequencyToUnit(double freq);
#define FMT_dispVoltageToFloat   FMT_dispBufferToFloat
#define FMT_floatToDispVoltage   FMT_floatToDispBuffer
#define FMT_dispFrequencyToFloat FMT_dispBufferToFloat
#define FMT_floatToDispFrequency FMT_floatToDispBuffer

void FMT_unitToDispPosGain(unsigned int unit, unsigned int pos, long *step);

double FMT_dbmToVoltage(double dBm);
double FMT_voltageToDBm(double voltage);

/* Match character data items s to character data cdata. Returs SCPI error code.
   If no errors argument nr will be set to match number */
int FMT_getCharacterData(char *cdata, const char *const *s, int count, unsigned int *nr);

/* Convert quoted string to normal string. Returns SCPI error code. */
int FMT_quotedStrintgToString(char *qs, char **s);

/* Get numeric value with unit */
#define FMT_FREQ_SUFF_COUNT 5
#define FMT_VOLT_SUFF_COUNT 5
#define FMT_CUR_SUFF_COUNT 4
#define FMT_IMP_SUFF_COUNT 4
#define FMT_TIME_SUFF_COUNT 3

extern const char *FMT_FreqSuff[FMT_FREQ_SUFF_COUNT];
extern const double FMT_FreqMult[FMT_FREQ_SUFF_COUNT];
extern const double FMT_VoltMult[FMT_VOLT_SUFF_COUNT];
extern const double FMT_CurMult[FMT_CUR_SUFF_COUNT];
extern const double FMT_ImpMult[FMT_IMP_SUFF_COUNT];
int FMT_getNumericValue(char *buf, const char *const *suffixes, const double *mult, int suffixesCount, double *value, double min, double max);

/* Get boolean value 0|1|OFF|ON */
int FMT_getBoolean(char *buf, bool& state);

#define FMT_COUPLING_COUNT 2
extern const char *FMT_Coupling[FMT_COUPLING_COUNT];

#define FMT_POLARITY_COUNT 6
extern const char *FMT_Polarity[FMT_POLARITY_COUNT];

#define FMT_CALIB_TYPE_COUNT 5
typedef enum {		// According to FMT_calibType[]
	CALIB_TYPE_NONE,
	CALIB_TYPE_DCU,
	CALIB_TYPE_ACU,
	CALIB_TYPE_DCI,
	CALIB_TYPE_ACI
} CALIB_TYPE;
extern const char *FMT_calibType[FMT_CALIB_TYPE_COUNT];

int FMT_getIsOdd(char *buf, const char *const *s, int count, bool& bIsEven);
int FMT_getInteger(char *buf, int *val);

int FMT_sprintf(char *s, const char *fmt, ...);
int FMT_snprintf(char *s, size_t n, const char *fmt, ...);

#ifdef __cplusplus
}
#endif

#endif /* #ifndef FMT_H */
