/*
	* File:    inst.h
	* Title:   
	* Created: Tue Feb 06 16:13:22 2007
	*
	*/

#ifndef SYSH_H
#define SYSH_H

#include "../../UI/Common.h"

// Compile time safe definition of constant array with constant array values which is located in flash area with associated function pointers array and they also located in flash area
// '*OPC?' always should be first! 
#define REMCMDS_DEF \
	REMCMD_DEF( opcQ								, "*OPC?") \
	REMCMD_DEF( idnQ								, "*IDN?") \
	REMCMD_DEF( rst									, "*RST") \
	REMCMD_DEF( cls									, "*CLS") \
	\
	REMCMD_DEF( systemErrorNextQ					, "SYSTem:ERRor?") \
	REMCMD_DEF( systemErrorNextQ					, "SYSTem:ERRor:NEXT?") \
	REMCMD_DEF( systemVersionQ						, "SYSTem:VERSion?") \
	\
	REMCMD_DEF( systemLocal							, "SYSTem:LOCal") \
	REMCMD_DEF( systemRemote						, "SYSTem:REMote") \
	REMCMD_DEF( systemKlock							, "SYSTem:KLOCk") \
	REMCMD_DEF( systemKlockQ						, "SYSTem:KLOCk?") \
	REMCMD_DEF( systemStatusQ						, "SYSTem:STATus?") \
	\
	REMCMD_DEF( sourceFrequency						, "FREQuency") \
	REMCMD_DEF( sourceFrequency						, "FREQuency:CW") \
	REMCMD_DEF( sourceFrequency						, "SOURce:FREQuency") \
	REMCMD_DEF( sourceFrequency						, "SOURce:FREQuency:CW") \
	REMCMD_DEF( sourceFrequencyQ					, "FREQuency?") \
	REMCMD_DEF( sourceFrequencyQ					, "FREQuency:CW?") \
	REMCMD_DEF( sourceFrequencyQ					, "SOURce:FREQuency?") \
	REMCMD_DEF( sourceFrequencyQ					, "SOURce:FREQuency:CW?") \
	\
	REMCMD_DEF( sourceVoltage						, "VOLTage") \
	REMCMD_DEF( sourceVoltage						, "VOLTage:LEVel") \
	REMCMD_DEF( sourceVoltage						, "SOURce:VOLTage") \
	REMCMD_DEF( sourceVoltage						, "SOURce:VOLTage:LEVel") \
	REMCMD_DEF( sourceVoltageQ						, "VOLTage?") \
	REMCMD_DEF( sourceVoltageQ						, "VOLTage:LEVel?") \
	REMCMD_DEF( sourceVoltageQ						, "SOURce:VOLTage?") \
	REMCMD_DEF( sourceVoltageQ						, "SOURce:VOLTage:LEVel?") \
	\
	REMCMD_DEF( sourceCurrent						, "CURRent") \
	REMCMD_DEF( sourceCurrent						, "CURRent:LEVel") \
	REMCMD_DEF( sourceCurrent						, "SOURce:CURRent") \
	REMCMD_DEF( sourceCurrent						, "SOURce:CURRent:LEVel") \
	REMCMD_DEF( sourceCurrentQ						, "CURRent?") \
	REMCMD_DEF( sourceCurrentQ						, "CURRent:LEVel?") \
	REMCMD_DEF( sourceCurrentQ						, "SOURce:CURRent?") \
	REMCMD_DEF( sourceCurrentQ						, "SOURce:CURRent:LEVel?") \
	\
	REMCMD_DEF( sourceCorrectionOFFSetQ				, "CORRection:OFFSet?") \
	REMCMD_DEF( sourceCorrectionOFFSetQ				, "SOURce:CORRection:OFFSet?") \
	\
	REMCMD_DEF( sourceCorrectionState				, "CORRection") \
	REMCMD_DEF( sourceCorrectionState				, "CORRection:STATe") \
	REMCMD_DEF( sourceCorrectionState				, "SOURce:CORRection") \
	REMCMD_DEF( sourceCorrectionState				, "SOURce:CORRection:STATe") \
	\
	REMCMD_DEF( sourceCorrectionStateQ				, "CORRection?") \
	REMCMD_DEF( sourceCorrectionStateQ				, "SOURce:CORRection?") \
	REMCMD_DEF( sourceCorrectionStateQ				, "CORRection:STATe?") \
	REMCMD_DEF( sourceCorrectionStateQ				, "SOURce:CORRection:STATe?") \
	\
	REMCMD_DEF( sourceVoltageMaximumQ				, "VOLTage:MAXimum?") \
	REMCMD_DEF( sourceVoltageMaximumQ				, "SOURce:VOLTage:MAXimum?") \
	REMCMD_DEF( sourceVoltageMinimumQ				, "VOLTage:MINimum?") \
	REMCMD_DEF( sourceVoltageMinimumQ				, "SOURce:VOLTage:MINimum?") \
	\
	REMCMD_DEF( sourceModeQ							, "SOURce:MODE?") \
	REMCMD_DEF( sourceBandQ							, "SOURce:BAND?") \
	REMCMD_DEF( sourceBandQ							, "BAND?") \
	REMCMD_DEF( sourceBand							, "SOURce:BAND") \
	REMCMD_DEF( sourceBand							, "BAND") \
	REMCMD_DEF( sourceBandMinimumQ					, "SOURce:BAND:MINimum?") \
	REMCMD_DEF( sourceBandMinimumQ					, "BAND:MINimum?") \
	REMCMD_DEF( sourceBandMaximumQ					, "SOURce:BAND:MAXimum?") \
	REMCMD_DEF( sourceBandMaximumQ					, "BAND:MAXimum?") \
	\
	REMCMD_DEF( outputCoupling						, "OUTPut:COUPling") \
	REMCMD_DEF( outputCouplingQ						, "OUTPut:COUPling?") \
	REMCMD_DEF( outputImpedance						, "OUTPut:IMPedance") \
	REMCMD_DEF( outputImpedanceQ					, "OUTPut:IMPedance?") \
	REMCMD_DEF( outputPolarity						, "OUTPut:POLarity") \
	REMCMD_DEF( outputPolarityQ						, "OUTPut:POLarity?") \
	REMCMD_DEF( outputState							, "OUTPut") \
	REMCMD_DEF( outputState							, "OUTPut:STATe") \
	REMCMD_DEF( outputStateQ						, "OUTPut?") \
	REMCMD_DEF( outputStateQ						, "OUTPut:STATe?") \
	REMCMD_DEF(	outputSense							, "OUTPut:SENSe") \
	REMCMD_DEF(	outputSenseQ						, "OUTPut:SENSe?") \
	\
	REMCMD_DEF( calibrationSecureState				, "CALibration:SECure:STATe")\
	REMCMD_DEF( calibrationSecureStateQ				, "CALibration:SECure:STATe?")\
	REMCMD_DEF( calibrationState					, "CALibration:STATe")\
	REMCMD_DEF( calibrationStateQ					, "CALibration:STATe?")\
	REMCMD_DEF( calibrationType						, "CALibration:TYPE")\
	REMCMD_DEF( calibrationTypeQ					, "CALibration:TYPE?")\
	REMCMD_DEF( calibrationSense					, "CALibration:SENSe")\
	REMCMD_DEF( calibrationSenseQ					, "CALibration:SENSe?")\
	REMCMD_DEF( calibrationBandsCountQ				, "CALibration:BANDs:COUNt?")\
	REMCMD_DEF( calibrationBandsBaseQ				, "CALibration:BANDs:BASE?")\
	REMCMD_DEF( calibrationBand						, "CALibration:BAND")\
	REMCMD_DEF( calibrationBandQ					, "CALibration:BAND?")\
	REMCMD_DEF( calibrationBandLevelsQ				, "CALibration:BAND:LEVels?")\
	REMCMD_DEF( calibrationBandFreqsQ				, "CALibration:BAND:FREQuencies?")\
	REMCMD_DEF( calibrationBandMaxQ					, "CALibration:BAND:MAXimum?")\
	REMCMD_DEF( calibrationPoint					, "CALibration:POINt")\
	REMCMD_DEF( calibrationPointQ					, "CALibration:POINt?")\
	REMCMD_DEF( calibrationLevelQ					, "CALibration:LEVel?")\
	REMCMD_DEF( calibrationFrequencyQ				, "CALibration:FREQuency?")\
	REMCMD_DEF( calibrationValue					, "CALibration:VALue")\
	REMCMD_DEF( calibrationValueQ					, "CALibration:VALue?")\
	REMCMD_DEF( calibrateQ							, "CALibrate?")\
	\
	REMCMD_DEF( eepromWrite							, "Eeprom:Write") \
	REMCMD_DEF( eepromReadQ							, "Eeprom?") \
	REMCMD_DEF( eepromReadQ							, "Eeprom:Read?") \
	\
	REMCMD_DEF( rst									, NULL) // Always should at end of definition

// Creating of enum

extern const char * const SystemHeaders[];

#endif /* #ifndef INST_H */
