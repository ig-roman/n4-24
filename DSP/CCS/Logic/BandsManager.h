/*
 * BandsManager.h
 *
 *  Created on: Nov 14, 2013
 *      Author: Sergei Tero
 */

#ifndef BANDSMANAGER_H_
#define BANDSMANAGER_H_

#include "DeviceState.h"
#include "../UI/Common.h"

#define MAX_DELTA_PERCENT 10

/** **********************************************************************
 *
 * Provides a number of functions to check that a value is within a given
 * band.
 *
 *********************************************************************** */
class BandsManager
{
public:
	BandsManager();
	virtual ~BandsManager() {};
	// ST TODO: Remove static?
	static STATEERR IsValidValue(WORKUNIT* pVoltage, bool bInSameBandVol, WORKUNIT* pFrequency, bool bInSameBandFreq, bool bDC, bool bVoltage);
	static STATEERR ChangeFrequencyBand(WORKUNIT* pVoltage, WORKUNIT* pFrequency, bool bUp, bool bVoltage);
	static STATEERR ChangeVoltageBand(WORKUNIT* pVoltage, WORKUNIT* pFrequency, bool bUp, bool bDC, bool bVoltage);
	static STATEERR IsValidResitance(WORKUNIT* value);
	static STATEERR checkFrqVolRelation(double dValue, PCBAND pValBand, WORKUNIT* frequency, bool bVoltage, double* pdMaxAvaliableVolForFreq = NULL);
	static STATEERR checkValue(WORKUNIT* pVal, const BAND arrBands[], bool bInSameBand, uint8_t* pBandIdx = NULL);
	static PCBAND GetBandsArray(bool bDC, bool bVoltage);
	static uint8_t GetBandsCount(PCBAND pBandArr);
	static PCBAND InvertBand(STATEMODE mode, PCBAND invBand);
	static PCFREQ_VOL_RELATION GetFreqResponse(bool bVoltage, PCBAND pValBand);
	static bool ValueInBand(double dVal, const BAND& band);
};

#endif /* BANDSMANAGER_H_ */
