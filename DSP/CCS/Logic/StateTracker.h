/*
 * StateTracker.h
 *
 *  Created on: Apr 3, 2014
 *      Author: Sergei Tero
 */

#ifndef STATETRACKER_H_
#define STATETRACKER_H_

#include "../Common.h"

typedef enum 
{
	STATECH_OPMODE		= 0x00000001,
	STATECH_DIR_TYPE	= 0x00000002,
	STATECH_VALUE		= 0x00000004,
	STATECH_FREQ		= 0x00000008,
	STATECH_SENSE		= 0x00000010,
	STATECH_OUT			= 0x00000020,
	STATECH_CORR		= 0x00000040,
	STATECH_ALL			= 0xFFFFFFFF
}
STATECH;

class StateTracker
{
public:
	StateTracker();
	virtual ~StateTracker() {};

	void SetModified(uint32_t lStatusBit, bool bSet = true);
	void Disable() { m_bEnabled = false; };
	void Enable() { m_bEnabled = true; };
	bool GetModified(uint32_t lStatusBit) { return m_bEnabled ? (m_lStatusBits & lStatusBit) > 0 : false; };

private:
	bool m_bEnabled;
	uint32_t m_lStatusBits;
};

extern StateTracker g_tracker;

#endif /* STATETRACKER_H_ */
