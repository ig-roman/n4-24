/*
 * StateProvider.cpp
 *
 *  Created on: Apr 4, 2014
 *      Author: Sergei Tero
 */

#include "StateProvider.h"
#include "../LogicInterface.h"
#include "StateTracker.h"
#include "DeviceState.h"
#include <string.h>
#include <stdio.h>

#define SHOW_AARM_ERRORS

StateProvider::StateProvider() 
	: m_unACKTimeout(0),
	m_bSyncProblem(false),
	m_pszErrorText(NULL),
	m_bIsInitialized(false),
	m_unPingDeadTime(0)
{
	m_unVerCrc = countCrc8((const uint8_t*)STR_SW_VERSION, strlen(STR_SW_VERSION), 0);
}

const char* StateProvider::PostInit()
{
	// When test is repeated, execution part (AARM) must be acknowledged again
	m_bSyncProblem = false;

	// Enable all logic in this class
	m_bIsInitialized = true;

	// Clear last error. It is used as return value
	m_pszErrorText = NULL;

	// Synchronize state later because ping signal checks MUC version
	g_tracker.SetModified(STATECH_ALL, false);

	// Executive MCU always sends ping signals. Wait for one of them.
	m_unPingDeadTime = GetNewPingDeadTime();
	uint32_t unPingDeadTime = m_unPingDeadTime;
	while (!m_bSyncProblem && unPingDeadTime == m_unPingDeadTime)
	{
		// Simulate program loop
		CLogic_ManageSerial();
		Loop();
	}

	if (!m_bSyncProblem)
	{
		// Send state synchronization command to executive MCU (AARM)
		g_tracker.SetModified(STATECH_ALL, true);
		uint32_t unACKTimeout = m_unACKTimeout;
		Loop();

		while (unACKTimeout == m_unACKTimeout)
		{
			// Simulate program loop
			CLogic_ManageSerial();
			Loop();
		}
	}

	m_bIsInitialized = !m_bSyncProblem;

	return m_pszErrorText;
}

void StateProvider::SetLastErrorInt(const char* pszErrorText)
{
	m_pszErrorText = pszErrorText;
	CLogic_SetLastError(pszErrorText);
}

/** **********************************************************************
 *
 * Sends low level command to AARM
 *
 * @param pBuf		buffer with data
 * @param length	length of buffer
 *
 *********************************************************************** */
void StateProvider::SendLLCommand(const char* pBuf, int length)
{
	if (m_bIsInitialized)
	{
		if (m_unACKTimeout != 0)
		{
			SetLastErrorInt("Can't send command because already in progress.");
		}
		else
		{
			CLogic_SendControlData(pBuf, length);
			CheckUpdateTime(AARM_TIMEOUT_ACK, &m_unACKTimeout);
		}
	}
}

/** **********************************************************************
 *
 * @return new ded time for ping signal
 *
 *********************************************************************** */
uint32_t StateProvider::GetNewPingDeadTime()
{
	// ACV 1000v may take ~ 4sec
	return GetTime() + AARM_PING_TIME * 3;
}

/** **********************************************************************
 *
 * Recieves data from AARM.
 * It may be ACK, NACK, PING or OVERLOAD signals.
 *
 * @param pBuf		buffer with data
 * @param length	length of buffer
 *
 *********************************************************************** */
void StateProvider::HandleControlData(const char* pBuf, int length)
{
	if (m_bIsInitialized)
	{
		bool bIsValidVersion = true;
		if (1 == length)
		{
			if (m_bSyncProblem = NACK == pBuf[0])
			{
				// Communication problems here. Send complete data to AARM next time.
				SetLastErrorInt("NACK has been received from AARM");
			}
		}
		else if (AARM_STATUS_RESPONSE_LENGTH == length)
		{
			uint16_t unBuffIdx = 0;
			STRCMD cmd = (STRCMD)pBuf[unBuffIdx++];
			char byValue = 0;
			if (STRCMD_START == cmd)
			{
				cmd = (STRCMD)pBuf[unBuffIdx++];
				if (STRCMD_STATUS == cmd)
				{
					if (ParseByteCmd(byValue, cmd, unBuffIdx, length, pBuf))
					{
						if (STRCMD_VER_CRC == cmd)
						{
							bIsValidVersion = (uint8_t)byValue == m_unVerCrc;
							m_bSyncProblem = !bIsValidVersion;
							if (bIsValidVersion)
							{
								if (ParseByteCmd(byValue, cmd, unBuffIdx, length, pBuf))
								{
									if (STRCMD_ERROR == cmd)
									{
										if (byValue != AERROR_NO_ERROR)
										{
											sprintf(m_chLastAARMError, "AARM ERROR: %d", (uint8_t)byValue);
											CLogic_SetLastError(m_chLastAARMError);
										}

										if (ParseByteCmd(byValue, cmd, unBuffIdx, length, pBuf))
										{
											if (STRCMD_OVERLOAD == cmd)
											{
												g_deviceState.SetOverload(byValue != 0);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if (!bIsValidVersion)
		{
			SetLastErrorInt("AARM Invalid version");
		}
		m_unACKTimeout = 0;
		m_unPingDeadTime = GetNewPingDeadTime();
	}
}

/** **********************************************************************
 *
 * The periodic function. Is invoked always.
 * Use CheckUpdateTime() function for custom period.
 *
 *********************************************************************** */
void StateProvider::Loop()
{
	if (m_bIsInitialized)
	{
		// Check Ping dead time
		if (!m_bSyncProblem && CheckUpdateTime(AARM_PING_TIME, &m_unPingDeadTime))
		{
			// No ping signal
			// Communication problems here. Send complete data to AARM next time.
			#ifdef SHOW_AARM_ERRORS
				m_bSyncProblem = true;
				SetLastErrorInt("No ping signal");
			#endif
		}

		if (m_unACKTimeout != 0)
		{
			// Waiting ACK
			if (CheckUpdateTime(AARM_TIMEOUT_ACK, &m_unACKTimeout))
			{
				// No ACK or NACK codes from AARM
				// Communication problems here. Send complete data to AARM next time.
				#ifdef SHOW_AARM_ERRORS
					m_bSyncProblem = true;
					m_unACKTimeout = 0;
					SetLastErrorInt("No acknowledge code");
				#endif
			}
		}
		else
		{
			// Check modified values in tracker.
			if (g_tracker.GetModified(STATECH_ALL))
			{
				// Send modifications to AARM
				SendControlData();
				g_tracker.SetModified(STATECH_ALL, false);
#ifndef LAUNCHPAD
				// Set ACK timeout
				CheckUpdateTime(AARM_TIMEOUT_ACK, &m_unACKTimeout);
#endif
			}
		}
	}
}

#define AARM_MAX_CMD_LEN 64
static char AARM_cmdBuff[AARM_MAX_CMD_LEN];

/** **********************************************************************
 *
 * Sends modified values to AARM
 *
 *********************************************************************** */
void StateProvider::SendControlData()
{
	if (m_bSyncProblem)
	{
		// Allways send complete packege if communcication layer was unstable
		g_tracker.SetModified(STATECH_ALL, true);
	}

	uint16_t unLength = 0;

	AARM_cmdBuff[unLength++] = STRCMD_START;

	if (g_tracker.GetModified(STATECH_OPMODE))
	{
		FormatByteCmd((char)g_deviceState.GetOperationMode(), STRCMD_MODE, unLength, AARM_cmdBuff);
	}

	if (g_tracker.GetModified(STATECH_OUT))
	{
		FormatByteCmd((char)g_deviceState.GetOutState(),	STRCMD_OUT,	unLength, AARM_cmdBuff);
	}

	if (g_tracker.GetModified(STATECH_SENSE))
	{
		STATESENSE sense;
		if (STATEERR_NO_ERROR == g_deviceState.GetSenseMode(sense))
		{
			FormatByteCmd((char)sense,	STRCMD_SENSE, unLength, AARM_cmdBuff);
		}
	}

	if (g_tracker.GetModified(STATECH_VALUE))
	{
		WORKUNIT wuVal;
		if (STATEERR_NO_ERROR == g_deviceState.GetCorrectedValue(wuVal))
		{
			FormatDoubleCmd(wuVal.value, STRCMD_VALUE, unLength, AARM_cmdBuff);
			FormatByteCmd((char)wuVal.pBand->bandIdx,	STRCMD_BAND, unLength, AARM_cmdBuff);
		}
	}

	if (g_tracker.GetModified(STATECH_FREQ | STATECH_DIR_TYPE))
	{
		WORKUNIT wuFreq;
		if (STATEERR_NO_ERROR == g_deviceState.GetFrequency(wuFreq))
		{
			FormatDoubleCmd(wuFreq.value, STRCMD_FREQUENCY, unLength, AARM_cmdBuff);
			FormatByteCmd((char)wuFreq.pBand->bandIdx,	STRCMD_FREQ_BAND,	unLength, AARM_cmdBuff);
		}
		else
		{
			FormatDoubleCmd(0, STRCMD_FREQUENCY, unLength, AARM_cmdBuff);
			FormatByteCmd(0, STRCMD_FREQ_BAND, unLength, AARM_cmdBuff);
		}
	}

	if (g_tracker.GetModified(STATECH_CORR))
	{
		FormatDoubleCmd(g_deviceState.GetFreqCorrector(), STRCMD_CORR, unLength, AARM_cmdBuff);
	}
	

	AARM_cmdBuff[unLength++] = STRCMD_END;

	if (AARM_MAX_CMD_LEN < unLength)
	{
		SetLastErrorInt("StateProvider - Buffer overflow");
	}

	CLogic_SendControlData(AARM_cmdBuff, unLength);
}

void StateProvider::FormatByteCmd(char byValue, STRCMD cmd, uint16_t& unLength, char* chBuff) const
{
	chBuff[unLength++] = cmd;
	chBuff[unLength++] = byValue;
}

bool StateProvider::ParseByteCmd(char& byValue, STRCMD& cmd, uint16_t& unBuffIdx, const uint16_t& unBuffLen, const char* chBuff) const
{
	bool bRet = unBuffLen >= 2;
	if (bRet)
	{
		cmd = (STRCMD)chBuff[unBuffIdx++];
		byValue = chBuff[unBuffIdx++];
	}
	return bRet;
}

void StateProvider::FormatDoubleCmd(double dVal, STRCMD cmd, uint16_t& unLength, char* chBuff) const
{
	chBuff[unLength++] = cmd;

	char* pMemPointer = (char*)&dVal;
	memcpy(chBuff + unLength, pMemPointer, sizeof(double));
	unLength += sizeof(double);
}

