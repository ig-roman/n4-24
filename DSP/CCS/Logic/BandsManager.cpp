/*
 * BandsManager.cpp
 *
 *  Created on: Nov 14, 2013
 *      Author: Sergei Tero
 */


#include "BandsManager.h"
#include "../UI/Pages/BasePage.h"

BandsManager::BandsManager()
{}

STATEERR GetNexBand(WORKUNIT* pVal, const BAND arrBands[], PCBAND* ppBand, bool bUp);

STATEERR BandsManager::IsValidResitance(WORKUNIT* value)
{
	// ST TODO: change argument to double
	STATEERR enumRet = STATEERR_OUT_OF_RANGE;

	PCBAND pBandPrev = &sm_arrBandsRs[0];
	for (int nIdx = 0; sm_arrBandsRs[nIdx].scale; nIdx++)
	{
		PCBAND pBand = &sm_arrBandsRs[nIdx];

		// Check that value between 2 ranges and select the nearest.
		if (pBandPrev->min <= value->value && pBand->min >= value->value)
		{
			double dDToPrev = value->value - pBandPrev->min;
			double dDToNext = pBand->min - value->value;
			value->pBand = dDToPrev < dDToNext ? pBandPrev : pBand;
			value->value = value->pBand->min;
			enumRet = STATEERR_NO_ERROR;
			break;
		}
		else if (0 == nIdx &&  pBand->min > value->value)
		{
			value->pBand = pBand;
			value->value = value->pBand->min;
			enumRet = STATEERR_NO_ERROR;
			break;
		}

		pBandPrev = pBand;
	}

	// Value greather than covered by ranges
	// Use last range
	if (STATEERR_OUT_OF_RANGE == enumRet)
	{
		const BAND& band = sm_arrBandsRs[EXTBAND_R_MAX_COUNT - 1];
		value->pBand = &band;
		value->value = value->pBand->min;
		enumRet = STATEERR_NO_ERROR;
	}

	// ST TODO: change return value to double
	return enumRet;
}

void maxMinInBands(double* pdMax, double* pdMin, const BAND arrBands[], uint8_t* pBandIdxMax, uint8_t* pBandIdxMin)
{
	for (int nIdx = 0; arrBands[nIdx].scale; nIdx++)
	{
		const BAND& band = arrBands[nIdx];

		if (!nIdx || (*pdMax < band.max))
		{
			*pdMax = band.max;
			*pBandIdxMax = nIdx;
		}

		if (!nIdx || (*pdMin > band.min))
		{
			*pdMin = band.min;
			*pBandIdxMin = nIdx;
		}
	}
}

bool BandsManager::ValueInBand(double dVal, const BAND& band)
{
	// Same as val <= max && val >= min but works with doubles.
	// 0 == dVal required to get zero on fixed negative band or positive 
	// IsPositiveBand skips bands which are defenetly invalid. 
	// For example +1000.000 - 0000.000 is invalid for -0.0000001 but without this check after round opration 0000.000 >= -0.0000001 is true.
	return (0 == dVal || IsPositiveBand(&band) == (dVal > 0)) && (!IsGreater(dVal, band.max, band.fraction) && !IsGreater(band.min, dVal, band.fraction));
}

/** **********************************************************************
 *
 * Checks whether a value matches a given band.
 * OR
 * Finds the best-matching band for the given value.
 * The best-matching band is determined as follows: if the value is already in
 * the provided band and the value matches the band, then that band is selected.
 * Otherwise, the most precise band is selected.
 *
 * @param pVal			the provided value and band. The band is also used as one of the outputs.
 * @param arrBands		array of allowed bands
 * @param bInSameBand	the value must match the provided band. If true, the band will not be changed.
 *
 * @param pBandIdx		the index of the found band
 *
 * @return				If bInSameBand is true, the function may return the following errors:
 * 							STATEERR_OUT_OF_RANGE - the value is completely out of the band
 * 							STATEERR_GO_BAND_UP, STATEERR_GO_BAND_DOWN - the value has been found in the band above or below, correspondingly
 * 							STATEERR_ABSOLUTE_UPPER_LIMIT, STATEERR_ABSOLUTE_LOWER_LIMIT - the value is greater/less than the absolute max/min of the device
 * 						Otherwise, errors STATEERR_OUT_OF_RANGE, STATEERR_ABSOLUTE_UPPER_LIMIT, STATEERR_ABSOLUTE_LOWER_LIMIT may be returned.
 *
 * 						If a band is found, STATEERR_NO_ERROR is returned.
 *
 *********************************************************************** */
STATEERR BandsManager::checkValue(WORKUNIT* pVal, const BAND arrBands[], bool bInSameBand, uint8_t* pBandIdx)
{
	STATEERR enumRet = STATEERR_OUT_OF_RANGE;
	PCBAND pBestBand = NULL;
	if (bInSameBand && pVal->pBand) {
		if (ValueInBand(pVal->value, arrBands[pVal->pBand->bandIdx])) {
			if (pBandIdx)
				*pBandIdx = pVal->pBand->bandIdx;
			return STATEERR_NO_ERROR;
		}
		else {
			// Value doesn't valid for this band, report error.

			// Calculate band switching indepened of value sign.
			bool bGoUpBand = (pVal->value >= 0 && pVal->value > pVal->pBand->max) ||
							 (pVal->value < 0 && pVal->value <= pVal->pBand->max);

			// Report "out of range" or "swith band to...".
			WORKUNIT wuTest = { pVal->pBand, pVal->value };

			uint8_t unNextBand = 0xFF;
			enumRet = checkValue(&wuTest, arrBands, false, &unNextBand);
			if (STATEERR_NO_ERROR == enumRet)
			{
				// Value valid for anoter band but is not for this band.
				if (bGoUpBand && unNextBand == pVal->pBand->bandIdx + 1 ||
					!bGoUpBand && unNextBand == pVal->pBand->bandIdx - 1)
				{
					// Next or pevious band is valid report it.
					enumRet = bGoUpBand ? STATEERR_GO_BAND_UP : STATEERR_GO_BAND_DOWN;
				}
				else
				{
					// Value valid for some band but not for next and pevious
					enumRet = STATEERR_OUT_OF_RANGE;
				}
			}
		}
	}
	else {
		for (int nIdx = 0; arrBands[nIdx].scale; nIdx++)
		{
			const BAND& band = arrBands[nIdx];
			bool bIsBandEqual = pVal->pBand && IsSameBand(&band, pVal->pBand);
			bool bIsBandEqualSignIngnoring = pVal->pBand && IsSameInvertedBand(&band, pVal->pBand);
			if (!bInSameBand || bIsBandEqualSignIngnoring || bIsBandEqual)
			{
				if (ValueInBand(pVal->value, band))
				{
					if (!pBestBand)
					{
						// Remember first lower band
						pBestBand = &band;
						if (pBandIdx)
						{
							*pBandIdx = nIdx;
						}
					}

					// Don't stop immediately next band may be equal with existing
					// or stop immediately if no preferred band is found
					if (bIsBandEqual || bIsBandEqualSignIngnoring || !pVal->pBand)
					{
						// Found identical band or negative
						pBestBand = &band;
						if (pBandIdx)
						{
							*pBandIdx = nIdx;
						}
						break;
					}
				}
			}
		}
	}

	if (pBestBand)
	{
		enumRet = STATEERR_NO_ERROR;
		pVal->pBand = pBestBand;
		uint8_t bandIdx = pVal->pBand->bandIdx;
		if (arrBands == sm_arrBandsDCU && g_deviceState.GetDividerEnabled() && g_deviceState.IsValidBandForDivider(bandIdx)) {
			bandIdx = g_deviceState.ChangeDividerBand(bandIdx);
			if (ValueInBand(pVal->value, arrBands[EXBAND_DCU_P_10_MV_E]))
				bandIdx = EXBAND_DCU_P_10_MV_E;
			if (ValueInBand(pVal->value, arrBands[EXBAND_DCU_N_10_MV_E]))
				bandIdx = EXBAND_DCU_N_10_MV_E;
			pVal->pBand = &arrBands[bandIdx];
			if (pBandIdx)
				*pBandIdx = bandIdx;
		}
	}

	if (STATEERR_NO_ERROR != enumRet)
	{
		uint8_t unBandIdxMax, unBandIdxMin;
		double dAbsoluteMax, dAbsoluteMin;

		maxMinInBands(&dAbsoluteMax, &dAbsoluteMin, arrBands, &unBandIdxMax, &unBandIdxMin);

		if (IsGreater(pVal->value, dAbsoluteMax, arrBands[unBandIdxMax].fraction))
		{
			enumRet = STATEERR_ABSOLUTE_UPPER_LIMIT;
		}
		else if (IsGreater(dAbsoluteMin, pVal->value, arrBands[unBandIdxMin].fraction))
		{
			enumRet = STATEERR_ABSOLUTE_LOWER_LIMIT;
		}
	}

	return enumRet;
}

/** **********************************************************************
 *
 * Checks the given value's relation to a given frequency.
 *
 * @param dValue					the value to compare
 * @param pValBand					Band of value. Because each band has own frequency response
 * @param frequency					the frequency to compare
 * @param bVoltage					if true, then the function uses the frequency-voltage relation array
 * 									otherwise, the function uses the frequency-current relation array
 * @param pdMaxAvaliableVolForFreq	returns maximum value for matched frequencies. Returns 0 when no frequency response for provided band.
 *
 * @return				STATEERR_OUT_OF_RANGE or STATEERR_NO_ERROR on success
 *
 *********************************************************************** */
STATEERR BandsManager::checkFrqVolRelation(double dValue, PCBAND pValBand, WORKUNIT* frequency, bool bVoltage, double* pdMaxAvaliableVolForFreq)
{
	PCFREQ_VOL_RELATION pRelationArr = GetFreqResponse(bVoltage, pValBand);
	STATEERR enumRet = pRelationArr ? STATEERR_OUT_OF_RANGE : STATEERR_NO_ERROR;

	double dMaxAvaliableVolForFreq = 0;
	if (STATEERR_NO_ERROR != enumRet)
	{
		for (int nIdx = 0; pRelationArr[nIdx].voltage; nIdx++)
		{
			const FREQ_VOL_RELATION& freqVolStruct = pRelationArr[nIdx];
			int8_t	fraction = frequency->pBand ? frequency->pBand->fraction : 0;
			double dFreq = frequency->value;

			// Check that frequency matches
			// dRoundedFreq <= freqVolStruct.freqMax && dRoundedFreq >= freqVolStruct.freqMin
			if (!IsGreater(dFreq, freqVolStruct.freqMax, fraction) && !IsGreater(freqVolStruct.freqMin, dFreq, fraction))
			{
				// Check that value matches
				dMaxAvaliableVolForFreq = MAX(dMaxAvaliableVolForFreq, freqVolStruct.voltage);
				//if (freqVolStruct.voltage >= roundX(dValue, 0))
				if (!IsGreater(dValue, freqVolStruct.voltage, pValBand->fraction))
				{
					enumRet = STATEERR_NO_ERROR;
					break;
				}
			}
		}
	}

	if (pdMaxAvaliableVolForFreq)
	{
		*pdMaxAvaliableVolForFreq = dMaxAvaliableVolForFreq;
	}

	return enumRet;
}

/** **********************************************************************
 *
 * Finds frequency response for provided band and operation mode.
 *
 * @param bVoltage					operation mode if value true mode is ACV otherwise ACI
 * @param pValBand					Band of value. Because each band has own frequency response
 *
 * @return	NULL if all frequencies are available or frequency response for provided band.
 *
 *********************************************************************** */
PCFREQ_VOL_RELATION BandsManager::GetFreqResponse(bool bVoltage, PCBAND pValBand)
{
	PCFREQ_VOL_RELATION pRetFreqRel = NULL;
	if (bVoltage && EXBAND_ACU_1000V == pValBand->bandIdx)
	{
		pRetFreqRel = sm_arrFreqVolRelation1000VBandOnly;
	}
	/*else if (!bVoltage &&
			(EXTBAND_ACI_10A_AMP == pValBand->bandIdx || EXTBAND_ACI_30A_AMP == pValBand->bandIdx))
	{
		pRetFreqRel = sm_arrFreqCurRelation;
	}*/

	return pRetFreqRel;
}

PCBAND BandsManager::GetBandsArray(bool bDC, bool bVoltage)
{
	PCBAND pArrRet = NULL;
	if		(bDC && bVoltage)	{ pArrRet = sm_arrBandsDCU; }
	else if	(bDC && !bVoltage)	{ pArrRet = sm_arrBandsDCI; }
	else if	(!bDC && bVoltage)	{ pArrRet = sm_arrBandsACU; }
	else if	(!bDC && !bVoltage)	{ pArrRet = sm_arrBandsACI; }
	return pArrRet;
}

uint8_t BandsManager::GetBandsCount(PCBAND pBandArr)
{
	for (int nIdx = 0; ; nIdx++)
	{
		if (!pBandArr[nIdx].scale)
			return nIdx;
	}
}

/** **********************************************************************
 *
 * Base function to check that the provided value (current or voltage) corresponds
 * to the band and frequency provided.
 *
 * @param pValue			the voltage or current to check
 * @param bInSameBandVol	the value must match the provided voltage/current band. If true, the band will not be changed.
 * @param pFrequency		the frequency of the current/voltage
 * @param bInSameBandFreq	the frequency must match the provided frequency band. If true, the band will not be changed.
 * @param bDC				if true, the frequency will be ignored
 * @param bVoltage			if true, then the function uses the frequency-voltage relation array
 * 							otherwise, the function uses the frequency-current relation array
 *
 * @return					STATEERR_OUT_OF_RANGE or STATEERR_NO_ERROR on success
 *
 *********************************************************************** */
STATEERR BandsManager::IsValidValue(WORKUNIT* pValue, bool bInSameBandVol, WORKUNIT* pFrequency, bool bInSameBandFreq, bool bDC, bool bVoltage)
{
	STATEERR enumRet = STATEERR_OUT_OF_RANGE;
	if (bDC)
	{
		enumRet = checkValue(pValue, GetBandsArray(true, bVoltage), bInSameBandVol);
	}
	else
	{
		WORKUNIT volCopy = { pValue->pBand, pValue->value };
		enumRet = checkValue(&volCopy, GetBandsArray(false, bVoltage), bInSameBandVol);
		if (STATEERR_NO_ERROR == enumRet)
		{
			WORKUNIT freqCopy = { pFrequency->pBand, pFrequency->value };
			enumRet = checkValue(&freqCopy, bVoltage ? sm_arrBandsACUFreq : sm_arrBandsACIFreq, bInSameBandFreq);
			if (STATEERR_NO_ERROR == enumRet)
			{
				// volCopy has updated and valid band when function detects band automatically
				enumRet = checkFrqVolRelation(volCopy.value, volCopy.pBand, &freqCopy, bVoltage);
				if (STATEERR_NO_ERROR == enumRet)
				{
					pValue->pBand = volCopy.pBand;
					pFrequency->pBand = freqCopy.pBand;
				}
			}
		}
	}

	return enumRet;
}

PCBAND BandsManager::InvertBand(STATEMODE mode, PCBAND invBand)
{
	PCBAND arrBands = GetBandsArray(true, STATEMODE_U == mode);
	if (mode == STATEMODE_U && g_deviceState.IsExtDividerBand(invBand->bandIdx))
	{
		switch (invBand->bandIdx)
		{
		case EXBAND_DCU_P_10_MV_E:
			return &arrBands[EXBAND_DCU_N_10_MV_E];
		case EXBAND_DCU_N_10_MV_E:
			return &arrBands[EXBAND_DCU_P_10_MV_E];
		case EXBAND_DCU_P_100_MV_E:
			return &arrBands[EXBAND_DCU_N_100_MV_E];
		case EXBAND_DCU_N_100_MV_E:
			return &arrBands[EXBAND_DCU_P_100_MV_E];
		}
	}
	for (int nIdx = 0; arrBands[nIdx].scale; nIdx++)
		if (IsSameInvertedBand(&arrBands[nIdx], invBand))
			return &arrBands[nIdx];
	return NULL;
}

STATEERR GetNexBand(WORKUNIT* pVal, const BAND arrBands[], PCBAND* ppBand, bool bUp)
{
	STATEERR enumRet = STATEERR_OUT_OF_RANGE;

	uint8_t unBandIdx = NULL;
	enumRet = BandsManager::checkValue(pVal, arrBands, true, &unBandIdx);
	if (STATEERR_NO_ERROR == enumRet)
	{
		if (bUp && sizeof(arrBands) / sizeof(*arrBands) - 2 == unBandIdx)
		{
			enumRet = STATEERR_OUT_OF_RANGE;
		}
		else if (!bUp && 0 == unBandIdx)
		{
			enumRet = STATEERR_OUT_OF_RANGE;
		}
		else
		{
			unBandIdx += bUp ? 1 : -1;
			*ppBand = &arrBands[unBandIdx];
		}
	}
	return enumRet;
}

/** **********************************************************************
 *
 * Changes the band to the next or the previous, only in case the value exists
 * in both the current band and the next/previous one.
 *
 * @param pValue			the voltage or current
 * @param pFrequency		the frequency of the current/voltage, return value
 * @param bUp				if true, we switch to the less precise band, otherwise to the more precise one
 * @param bVoltage			if true, then the function uses the voltage bands array
 * 							otherwise, the function uses the current bands array
 *
 * @return					STATEERR_NO_ERROR on success
 *
 *********************************************************************** */
STATEERR BandsManager::ChangeFrequencyBand(WORKUNIT* pValue, WORKUNIT* pFrequency, bool bUp, bool bVoltage)
{
	STATEERR enumRet = STATEERR_OUT_OF_RANGE;
	if (pFrequency && pFrequency->value > 0)
	{
		// Get next band if possible
		PCBAND pBand = NULL;
		enumRet = GetNexBand(pFrequency, bVoltage ? sm_arrBandsACUFreq : sm_arrBandsACIFreq, &pBand, bUp);
		if (STATEERR_NO_ERROR == enumRet)
		{
			// Check that band and new value are acceptable
			WORKUNIT newFreq = { pBand, pFrequency->value };
			enumRet = BandsManager::IsValidValue(pValue, true, &newFreq, true, false, bVoltage);
			if (STATEERR_NO_ERROR == enumRet)
			{
				// Return new band
				pFrequency->pBand = pBand;
			}
		}
	}
	return enumRet;
}

/** **********************************************************************
 *
 * Changes the voltage band to the next or the previous, only in case the value exists
 * in both the current band and the next/previous one.
 *
 * @param pValue			the voltage or current, return value
 * @param pFrequency		the frequency of the current/voltage
 * @param bUp				if true, we switch to the less precise band, otherwise to the more precise one
 * @param bVoltage			if true, then the function uses the voltage bands array
 * 							otherwise, the function uses the current bands array
 *
 * @return					STATEERR_NO_ERROR on success
 *
 *********************************************************************** */
STATEERR BandsManager::ChangeVoltageBand(WORKUNIT* pVoltage, WORKUNIT* pFrequency, bool bUp, bool bDC, bool bVoltage)
{
	STATEERR enumRet = STATEERR_OUT_OF_RANGE;

	// Get next band if possible
	PCBAND pBand = NULL;
	enumRet = GetNexBand(pVoltage, GetBandsArray(bDC, bVoltage), &pBand, bUp);
	if (STATEERR_NO_ERROR == enumRet)
	{
		// Check that band and new value are acceptable
		WORKUNIT newVoltage = { pBand, pVoltage->value };
		enumRet = BandsManager::IsValidValue(&newVoltage, true, pFrequency, true, bDC,bVoltage);
		if (STATEERR_NO_ERROR == enumRet)
		{
			// Return new band
			pVoltage->pBand = pBand;
		}
	}

	return enumRet;
}
