/*
 * StateProvider.h
 *
 *  Created on: Apr 4, 2014
 *      Author: Sergei Tero
 */

#ifndef STATEPROVIDER_H_
#define STATEPROVIDER_H_

#include "../Common.h"
class StateProvider {
public:
	StateProvider();
	virtual ~StateProvider() {};

	const char* PostInit();
	void Loop();
	void HandleControlData(const char* pBuf, int length);
	void SendLLCommand(const char* pBuf, int length);

private:
	void FormatByteCmd(char byValue, STRCMD cmd, uint16_t& unLength, char* chBuff) const;
	void FormatDoubleCmd(double dVal, STRCMD cmd, uint16_t& unLength, char* chBuff) const;

	bool ParseByteCmd(char& byValue, STRCMD& cmd, uint16_t& unBuffIdx, const uint16_t& unBuffLen, const char* chBuff) const;
	void SendControlData();
	uint32_t GetNewPingDeadTime();
	void SetLastErrorInt(const char* pszErrorText);

private:
	uint32_t m_unACKTimeout;
	uint32_t m_unPingDeadTime;
	bool m_bSyncProblem;
	char m_chLastAARMError[20];
	uint8_t m_unVerCrc;
	const char* m_pszErrorText;
	bool m_bIsInitialized;
};

#endif /* STATEPROVIDER_H_ */
