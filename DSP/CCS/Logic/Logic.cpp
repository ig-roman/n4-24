#include "../LogicInterface.h"
#include "Logic.hpp"
#include "DeviceState.h"
#include "StateProvider.h"
#include "RemoteCtrl/inst.h"
#include <string.h>

StateProvider s_stateProvider;

__declspec(dllexport) bool __stdcall  Logic_SwitchOut(char bOn, char bHV, char bHVConfirmRequire, char bForce)
{
	return CLogic_SwitchOut(bOn, bHV, bHVConfirmRequire, bForce);
}

__declspec(dllexport) void __stdcall Logic_Init()
{
	g_deviceState.Init();
}

__declspec(dllexport) const char* __stdcall Logic_PostInit()
{
	return s_stateProvider.PostInit();
}

/** **********************************************************************
 *
 * The periodic function. Is invoked always.
 * Use CheckUpdateTime() function for custom period.
 *
 *********************************************************************** */
__declspec(dllexport) void __stdcall Logic_Loop()
{
	s_stateProvider.Loop();
}

__declspec(dllexport) void __stdcall Logic_HandleControlData(const char* pBuf, int length)
{
	s_stateProvider.HandleControlData(pBuf, length);
}

__declspec(dllexport) void __stdcall Logic_HandleRemoteCtrlData(const char* pSingleLineCmd, int length)
{
	char pszOutBuff[INST_OUTPUT_BUFFER_MAX_SIZE + 2];
	char* pszInpBuff = new char[length + 1];

	memcpy(pszInpBuff, pSingleLineCmd, length);

	pszInpBuff[length] = NULL;
	*pszOutBuff = NULL;

	INST_execute(pszInpBuff, pszOutBuff);
	delete[] pszInpBuff;

	if (*pszOutBuff)
	{
		const char arrEndLineCRLF[] = "\r\n";	// Finished with ZERO!
		strcat(pszOutBuff, arrEndLineCRLF);
		CLogic_SendRemoteCtrlData(pszOutBuff, strlen(pszOutBuff));
	}
}

__declspec(dllexport) void __stdcall Logic_SendLLCommand(const char* pBuf, int length)
{
	s_stateProvider.SendLLCommand(pBuf, length);
}
