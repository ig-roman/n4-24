/*
 * StateTracker.cpp
 *
 *  Created on: Apr 3, 2014
 *      Author: Sergei Tero
 */

#include "StateTracker.h"

StateTracker g_tracker;

StateTracker::StateTracker() :
	m_lStatusBits(STATECH_ALL),
	m_bEnabled(true)
{}

void StateTracker::SetModified(uint32_t lStatusBit, bool bSet)
{
	m_lStatusBits = bSet ? (m_lStatusBits | lStatusBit) : (m_lStatusBits & ~lStatusBit);
}
