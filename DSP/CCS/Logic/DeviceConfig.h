/*
 * DeviceConfig.h
 *
 *  Created on: Nov 14, 2013
 *      Author: Sergei Tero
 */

#ifndef DEVICECONFIG_H_
#define DEVICECONFIG_H_

#include "../UI/Common.h"
#include "../LogicInterface.h"
#include <stddef.h>

typedef struct DCONFtag
{
	STATELANG m_enumLang;
	CTRLMODE m_enumCtrlMode;
	LAN_CONF m_cfgLan;
	LAB_CONF m_cfgLab;
	RS232_CONF m_cfgRs232;
	double m_dSafetyVoltage;
	uint8_t m_unGPIBAddress;
	uint8_t m_unBacklightIntensity;
	char m_szConfPsw[PAS_MAX_LEN + 1];
	char m_szCalPsw[PAS_MAX_LEN + 1];
	uint8_t m_bHVBuzzerSuppress;
#ifdef WITH_FREQ_CORR
	double m_dFreqCorr;
#endif
} DCONF, *PDCONF;

#define DECLARE_GET_SET(TYPE, PREF, VAL)							\
void Set##VAL(const TYPE& PREF##VAL)								\
{																	\
	m_cfg.m_##PREF##VAL = PREF##VAL;								\
	uint32_t unValStructOffset = offsetof(DCONF, m_##PREF##VAL);	\
	CLogic_StoreBytes(EEPROM_ADDR_CONF + unValStructOffset, sizeof(TYPE), (uint8_t*)(&m_cfg.m_##PREF##VAL)); \
	SetUpdateCrc();													\
};																	\
const TYPE& Get##VAL() const										\
{																	\
	return m_cfg.m_##PREF##VAL;										\
};																	\

/** **********************************************************************
 *
 * Contains the device state. The state is always maintained valid. Any changes
 * to dependencies are tracked and adjusted as necessary.
 *
 *********************************************************************** */
class DeviceConfig
{
public:
	DeviceConfig() {};
	virtual ~DeviceConfig() {};

	DECLARE_GET_SET(STATELANG, enum, Lang)
	DECLARE_GET_SET(LAN_CONF, cfg, Lan)
	DECLARE_GET_SET(LAB_CONF, cfg, Lab)
	DECLARE_GET_SET(RS232_CONF, cfg, Rs232)
	DECLARE_GET_SET(double, d, SafetyVoltage)
	DECLARE_GET_SET(uint8_t, un, GPIBAddress)
	DECLARE_GET_SET(uint8_t, un, BacklightIntensity)
	DECLARE_GET_SET(uint8_t, b, HVBuzzerSuppress)
#ifdef WITH_FREQ_CORR
	DECLARE_GET_SET(double, d, FreqCorr)
#endif

	// Don't save CTRLMODE_REMOTE to the EEPROM and never read it back otherwise user can not control the device.
	void SetCtrlModePub(const CTRLMODE& enumCtrlMode, bool bTemp);
	const CTRLMODE& GetCtrlModePub() const { return GetCtrlMode(); };

	void SetPassword(const char* pszNewPsw, bool bIsConfPsw);
	const char* GetPassword(bool bIsConfPsw) const	{ return bIsConfPsw ? m_cfg.m_szConfPsw : m_cfg.m_szCalPsw; }

	void Init();
private:
	DECLARE_GET_SET(CTRLMODE, enum, CtrlMode)

	bool LoadConfig();
	void SetByte(uint32_t unAddress, uint8_t unVal, uint8_t& unMemeber);
	void SetUpdateCrc();
private:
	DCONF m_cfg;
};


#endif /* DEVICECONFIG_H_ */

