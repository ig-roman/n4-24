/*
 * DeviceState.cpp
 *
 *  Created on: Nov 14, 2013
 *      Author: Sergei Tero
 */

#include "DeviceState.h"
#include "BandsManager.h"
#include <math.h>
#include "../LogicInterface.h"
#include "Calibration/CalibrationManager.h"

DeviceState g_deviceState;

DeviceState::DeviceState()
		: m_enumMode(STATEMODE_U),
		m_unOverloadLockTime(0),
		m_bIsCalibrationEnabled(true), // This value must be saved in device RAM memory only and restored when device is powered OFF.
		m_dFreqCorrector(0)
{
	m_wuDeltaPercent.pBand = NULL;
}

DeviceState::~DeviceState()
{
	if (m_wuDeltaPercent.pBand)
	{
		delete m_wuDeltaPercent.pBand;
	}
}

void DeviceState::Init()
{
	m_bOverload = false;
	m_deviceConfig.Init();
	Reset();
}

/** **********************************************************************
 *
 * Returns whether the mode is AC or DC.
 * @param bDC	return value, true if DC, otherwise false
 * @return	STATEERR_NO_ERROR if the mode is one of STATEMODE_U or STATEMODE_I.
 * 			Otherwise STATEERR_INVALID_MODE.
 *
 *********************************************************************** */
STATEERR DeviceState::GetDirecionType(bool& bDC) const
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if (STATEMODE_U == m_enumMode || STATEMODE_I == m_enumMode) 
	{
		bDC = m_bDC;
		enumRet = STATEERR_NO_ERROR;
	}

	return enumRet;
}

/** **********************************************************************
 *
 * Changes the mode to AC or DC. Will reset some parameters to default values.
 *
 * @param bDC	mode to set, true if DC, false if AC
 *
 * @return	STATEERR_NO_ERROR if the mode is one of STATEMODE_U or STATEMODE_I.
 * 			Otherwise STATEERR_INVALID_MODE.
 *
 *********************************************************************** */
STATEERR DeviceState::SetDirecionType(bool bDC)
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if (STATEMODE_U == m_enumMode || STATEMODE_I == m_enumMode) 
	{
		m_bDC = bDC;
		ResetDelta(true);
		ResetValue();
		SwitchOut(false, false);
		enumRet = STATEERR_NO_ERROR;

		g_tracker.SetModified(STATECH_DIR_TYPE);
	}

	return enumRet;
}

STATEERR DeviceState::GetAbsoluteDeviceLimit(double& dLimitValue, bool bIsMax) const
{
	switch (m_enumMode)
	{
		case STATEMODE_U:
			if (bIsMax)
				dLimitValue = m_bDC ? MAX_VOLTAGE_DC_VALUE : MAX_VOLTAGE_AC_VALUE;
			else
				dLimitValue = m_bDC ? MIN_VOLTAGE_DC_VALUE : MIN_VOLTAGE_AC_VALUE;
			return STATEERR_NO_ERROR;
		case STATEMODE_I:
			if (bIsMax)
				dLimitValue = m_bDC ? MAX_CURENT_VALUE : MAX_CURENT_AC_VALUE;
			else
				dLimitValue = m_bDC ? MIN_CURENT_VALUE : MIN_CURENT_AC_VALUE;
			return STATEERR_NO_ERROR;
		case STATEMODE_R:
			if (bIsMax)
				dLimitValue = MAX_IMP_VALUE;
			else
				dLimitValue = MIN_IMP_VALUE;
			return STATEERR_NO_ERROR;
		default:
			return STATEERR_INVALID_MODE;
	}
}

/** **********************************************************************
 *
 * @return the current device mode, one of STATEMODE_NONE, STATEMODE_U, STATEMODE_I, STATEMODE_R
 *
 *********************************************************************** */
STATEMODE DeviceState::GetOperationMode() const
{
	return m_enumMode;
}

/** **********************************************************************
 *
 * Changes the operation mode. Will reset some parameters to default values.
 *
 * @param enumVal	one of STATEMODE_NONE, STATEMODE_U, STATEMODE_I, STATEMODE_R, STATEMODE_OTHER
 *
 *********************************************************************** */
void DeviceState::SetOperationMode(STATEMODE enumVal)
{
	m_enumMode = enumVal;
	Reset();
}

/** **********************************************************************
 *
 * Resets value of device
 *
 *********************************************************************** */
void DeviceState::ResetValue()
{
	if (STATEMODE_U == m_enumMode)
	{
		SetValue(m_bDC ? DEFAULT_VOLTAGE_DCU : DEFAULT_VOLTAGE_ACU, m_bDC ? 0 : DEFAULT_FREQENCY, true);

		// Select more acurate band if possibe
		ChangeFrequencyBand(false);
		ChangeValueBand(false);
	}
	else if (STATEMODE_I == m_enumMode)
	{
		SetValue(DEFAULT_CURRENT, DEFAULT_FREQENCY, true);

		// Select more acurate band if possibe
		ChangeFrequencyBand(false);
		ChangeValueBand(false);
	}

	SetResistance(DEFAULT_RESISTANCE);
}

/** **********************************************************************
 *
 * Resets device completely
 *
 *********************************************************************** */
void DeviceState::Reset()
{
	m_bHVRange = false;
	m_bHVConfirmRequire = false;
	m_dWithDeltaValue = 0;
	SwitchOut(false, false, true);
	m_bDC = true;

	// SetValue() checks delta it should be reseted before
	ResetDelta(true);
	ResetValue();
	m_enumSense = STATESENSE_TWO_WIRE;

	SetZeroMode(false);
#ifdef WITH_FREQ_CORR
	SetFreqCorrector(m_deviceConfig.GetFreqCorr());
#endif
	g_tracker.SetModified(STATECH_ALL);
	gs_calibrationManager.Clear();
}

/** **********************************************************************
 *
 * Resets device delta
 *
 *********************************************************************** */
void DeviceState::ResetDelta(bool bDisable)
{
	if (bDisable)
	{
		m_bIsDeltaEnabled = false;
		m_pBandOldInitial = NULL;
	}
	m_dDeltaValue = 0;
	m_wuDeltaPercent.value = 0;
}

/** **********************************************************************
 *
 * Updates the delta band, expressed in percent.
 *
 *********************************************************************** */
void DeviceState::SyncDeltaBand()
{
	if (STATEMODE_U == m_enumMode || STATEMODE_I == m_enumMode)
	{
		if (m_pBandOldInitial != m_wuInitial.pBand)
		{
			// Create band for percents
			PBAND pNewBand = (PBAND)m_wuDeltaPercent.pBand;
			if (!pNewBand)
			{
				pNewBand = new BAND(*m_wuInitial.pBand);
				m_wuDeltaPercent.pBand = pNewBand;
			}
			pNewBand->scale = m_wuInitial.pBand->scale - 1;
			pNewBand->fraction = pNewBand->scale - 2;

			m_pBandOldInitial = m_wuInitial.pBand;
		}
	}
}

/** **********************************************************************
 *
 * Returns the frequency value
 *
 * @param frequency the return value and band
 *
 * @return	STATEERR_INVALID_MODE or STATEERR_NO_ERROR on success
 *
 *********************************************************************** */
STATEERR DeviceState::GetFrequency(WORKUNIT& frequency) const
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if ((STATEMODE_U == m_enumMode || STATEMODE_I == m_enumMode) && !m_bDC)
	{
		enumRet = STATEERR_NO_ERROR;
		frequency.value = m_wuFrequency.value;
		frequency.pBand = m_wuFrequency.pBand;
	}

	return enumRet;
}

/** **********************************************************************
 *
 * Sets the device frequency value
 *
 * @param dFrequency		new frequency
 * @param bUseOptimalVBand	don't keep existing frequency band allways finds optimal
 * @param pBandFreqRaw		band of frequency is used to limit value to this band
 *
 * @return STATEERR_NO_ERROR on success
 *
 *********************************************************************** */
STATEERR DeviceState::SetFrequency(double dFrequency, bool bUseOptimalVBand, PCBAND pBandFreqRaw /*= NULL*/)
{
	PCBAND pBandFreq = pBandFreqRaw
						? pBandFreqRaw
						: (bUseOptimalVBand ? NULL : m_wuFrequency.pBand);

	WORKUNIT frequency = { pBandFreq, dFrequency };
	return SetFrequency(frequency, !!pBandFreqRaw);
}

/** **********************************************************************
 *
 * Sets the frequency value
 *
 * @param frequency 	the frequency value to set, also the return value
 * @param bInSameBand	if true, the frequency will be in the same band and the
 * 						band will not be changed
 *
 * @return				STATEERR_NO_ERROR on success
 *
 *********************************************************************** */
STATEERR DeviceState::SetFrequency(WORKUNIT& frequency, bool bInSameBand)
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if ((STATEMODE_U == m_enumMode || STATEMODE_I == m_enumMode) && !m_bDC)
	{
		enumRet = BandsManager::IsValidValue(&m_wuInitial, true, &frequency, bInSameBand, m_bDC, STATEMODE_U == m_enumMode);
		if (STATEERR_NO_ERROR == enumRet)
		{
			m_wuFrequency.value = frequency.value;
			m_wuFrequency.pBand = frequency.pBand;

			SetZeroMode(false);

			g_tracker.SetModified(STATECH_FREQ);

			// Value maybe changed too because of calibration logic.
			// Same display value with different frequencies may produce different PWM values with same device output value.
			g_tracker.SetModified(STATECH_VALUE);
		}
	}

	return enumRet;
}

STATEERR DeviceState::SetResistance(double dValue)
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if (STATEMODE_R == m_enumMode)
	{
		WORKUNIT value = { m_wuInitial.pBand, dValue };
		enumRet = BandsManager::IsValidResitance(&value);
		if (STATEERR_NO_ERROR == enumRet)
		{
			// UI values 
			m_wuInitial.value = value.value;
			m_wuInitial.pBand = value.pBand;

			// Device output value 
			m_wuOutputValue.value = value.value;
			m_wuOutputValue.pBand = value.pBand;

			g_tracker.SetModified(STATECH_VALUE);
		}
	}

	return enumRet;
}

STATEERR DeviceState::GetValue(WORKUNIT& valueRet) const
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if (STATEMODE_U == m_enumMode || STATEMODE_I == m_enumMode || STATEMODE_R == m_enumMode)
	{
		enumRet = STATEERR_NO_ERROR;
		valueRet.value = m_wuInitial.value;
		valueRet.pBand = m_wuInitial.pBand;
	}

	return enumRet;
}

STATEERR DeviceState::SetValue(double dVoltage, double dFrequency, bool bUseOptimalVBand, PCBAND pBandVolRaw, PCBAND pBandFreqRaw, bool bSkipValidation /* = false */)
{
	PCBAND pBandFreq = pBandFreqRaw ? pBandFreqRaw : m_wuFrequency.pBand;
	WORKUNIT frequency = { pBandFreq, dFrequency };

	PCBAND pBandVol = pBandVolRaw 
						? pBandVolRaw
						: (bUseOptimalVBand ? NULL : m_wuInitial.pBand);
	WORKUNIT voltage = { pBandVol, dVoltage };

	return SetValue(voltage, !bUseOptimalVBand, dFrequency < 0 ? NULL : &frequency, !!pBandFreqRaw, bSkipValidation);
}

/** **********************************************************************
 *
 * Sets the voltage or current, depending on the operation mode
 *
 * @param value		 		the value to set, also the return value
 * @param bInSameBandVol	if true, the value will be in the same band and the
 * 							band will not be changed
 * @param pFrequency		the frequency of the value to set, also the return value
 * @param bInSameBandFreq	if true, the frequency will be in the same band and the
 * 							band will not be changed
 *
 * @return					STATEERR_NO_ERROR on success
 *
 *********************************************************************** */
STATEERR DeviceState::SetValue(WORKUNIT& value, bool bInSameBandVol, PWORKUNIT pFrequency, bool bInSameBandFreq, bool bSkipValidation /* = false */)
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if (STATEMODE_U == m_enumMode || STATEMODE_I == m_enumMode)
	{
		WORKUNIT targetValue = { value.pBand, value.value };

		PCBAND pBandDeltaAutoDetected = value.pBand;
		PWORKUNIT pNewFreq = pFrequency ? pFrequency : &m_wuFrequency; // Use old frequency in case when only value should be changed.
		enumRet = m_bIsDeltaEnabled ? ValidateDelta(NULL, NULL, value.value, pNewFreq, bInSameBandFreq, &pBandDeltaAutoDetected, targetValue.value, bSkipValidation) : STATEERR_NO_ERROR;
		if (STATEERR_NO_ERROR == enumRet)
		{
			enumRet = bSkipValidation ? STATEERR_NO_ERROR : BandsManager::IsValidValue(&targetValue, bInSameBandVol, pNewFreq, bInSameBandFreq, m_bDC, STATEMODE_U == m_enumMode);
			if (STATEERR_NO_ERROR == enumRet)
			{
				if (m_bIsDeltaEnabled)
				{
					targetValue.pBand = pBandDeltaAutoDetected;
				}

				if (pFrequency)
				{
					m_wuFrequency.value = pFrequency->value;
					m_wuFrequency.pBand = pFrequency->pBand;
					g_tracker.SetModified(STATECH_FREQ);
				}

				m_wuInitial.value = value.value;
				m_wuInitial.pBand = targetValue.pBand;

				m_dWithDeltaValue = targetValue.value;

				// Uses m_dWithDeltaValue and targetValue.pBand
				UpdateValueDependencies();

				SetZeroMode(false);
			}
		}
	}

	return enumRet;
}

/** **********************************************************************
 *
 * Coordinates switching to the high-voltage mode and back.
 *
 * @param dVoltage			the new voltage value
 * @param pBand				the band, needed for precise comparison
 *
 * @return					true if the output value can be changed
 *
 *********************************************************************** */
bool DeviceState::ManageHV(double dVoltage, PCBAND pBand)
{
	if (STATEMODE_U == m_enumMode)
	{
		double dSafVol = GetConfig().GetSafetyVoltage();
		double dProbValue = m_bDC ? dVoltage : dVoltage * VAL_SQRT2;
		dProbValue = fabs(dProbValue);
		if (m_bHVRange)
		{
			// dProbValue < HV
			if (IsGreater(dSafVol - MAX_HV_THRESHOLD_THRESHOLD_VAL, dProbValue, pBand->fraction))
			{
				// Value become lower than HW + threshold
				m_bHVRange = false;

				// Sets led blinking Off and buzzer off
				m_bHVConfirmRequire = false;
				SwitchOut(m_bOutOn, false, false);
			}
		}
		else
		{
			// dProbValue >= HV
			if (!IsGreater(dSafVol + MAX_HV_THRESHOLD_THRESHOLD_VAL, dProbValue, pBand->fraction))
			{
				m_bHVRange = true;

				if (!m_bHVConfirmRequire)
				{
					if (m_bOutOn)
					{
						// Sets led blinking on to tequest users confirmation
						m_bHVConfirmRequire = true;
						SwitchOut(m_bOutOn, false, false);
					}
				}
			}
			else if (m_bHVConfirmRequire)
			{
				// Value become lower than HW
				// Sets led blinking Off
				m_bHVConfirmRequire = false;
				SwitchOut(m_bOutOn, false, false);
			}
		}
	}

	return !m_bOutOn || STATEMODE_U != m_enumMode || !m_bHVConfirmRequire;
}

/** **********************************************************************
 *
 * Changes the frequency band to the next or the previous, only in case the value exists
 * in both the current band and the next/previous one.
 *
 * @param bUp				if true, we switch to the less precise band, otherwise to the more precise one
 *
 * @return					STATEERR_NO_ERROR on success
 *
 *********************************************************************** */
STATEERR DeviceState::ChangeFrequencyBand(bool bUp)
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if ( STATEMODE_U == m_enumMode || STATEMODE_I == m_enumMode)
	{
		enumRet = BandsManager::ChangeFrequencyBand(&m_wuInitial, &m_wuFrequency, bUp, STATEMODE_U == m_enumMode);
		SetZeroMode(false); // ST TODO: Should be moved under if below?

		if (STATEERR_NO_ERROR == enumRet)
		{
			g_tracker.SetModified(STATECH_FREQ);
		}
	}

	return enumRet;
}

/** **********************************************************************
 *
 * Changes the voltage/current band to the next or the previous, only in case the value exists
 * in both the current band and the next/previous one.
 *
 * @param bUp				if true, we switch to the less precise band, otherwise to the more precise one
 *
 * @return					STATEERR_NO_ERROR on success
 *
 *********************************************************************** */
STATEERR DeviceState::ChangeValueBand(bool bUp)
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if ( STATEMODE_U == m_enumMode || STATEMODE_I == m_enumMode)
	{
		WORKUNIT targetValue = { m_wuInitial.pBand, m_dWithDeltaValue };
		enumRet = BandsManager::ChangeVoltageBand(&targetValue, &m_wuFrequency, bUp, m_bDC, STATEMODE_U == m_enumMode);
		if ( STATEERR_NO_ERROR == enumRet)
		{
			// ST TODO: Check that m_wuOutputValue.pBand should be updated?
			m_wuInitial.pBand = targetValue.pBand;
			SetZeroMode(false);

			g_tracker.SetModified(STATECH_VALUE);
		}
	}
	return enumRet;
}

int8_t DeviceState::ChangeDividerBand(int8_t oldIdx)
{
	switch (oldIdx) {
#ifdef USE_DCU_10_MV		
	case EXBAND_DCU_P_10_MV:
		return EXBAND_DCU_P_10_MV_E;
#endif
	case EXBAND_DCU_P_10_MV_E:
#ifdef USE_DCU_10_MV
		return EXBAND_DCU_P_10_MV;
#else
		return EXBAND_DCU_P_100_MV;
#endif
	case EXBAND_DCU_P_100_MV:
		return EXBAND_DCU_P_100_MV_E;
	case EXBAND_DCU_P_100_MV_E:
		return EXBAND_DCU_P_100_MV;
#ifdef USE_DCU_10_MV
	case EXBAND_DCU_N_10_MV:
		return EXBAND_DCU_N_10_MV_E;
#endif
	case EXBAND_DCU_N_10_MV_E:
#ifdef USE_DCU_10_MV
		return EXBAND_DCU_N_10_MV;
#else
		return EXBAND_DCU_N_100_MV;
#endif
	case EXBAND_DCU_N_100_MV:
		return EXBAND_DCU_N_100_MV_E;
	case EXBAND_DCU_N_100_MV_E:
		return EXBAND_DCU_N_100_MV;
	default:
		return -1;
	}
}

STATEERR DeviceState::ChangeDivider()
{
	if (!IsExtDividerPossible())
		return STATEERR_INVALID_MODE;
	int newIdx = ChangeDividerBand(m_wuInitial.pBand->bandIdx);
	PCBAND bandsArray = BandsManager::GetBandsArray(true, true);
	m_wuInitial.pBand = &bandsArray[newIdx];
	m_wuOutputValue.pBand = m_wuInitial.pBand;
	if (BandsManager::ValueInBand(m_wuInitial.value, *m_wuInitial.pBand))
		ChangeValueBand(false);
	else
		g_tracker.SetModified(STATECH_VALUE);
	return STATEERR_NO_ERROR;
}

/** **********************************************************************
 *
 * Checks the output value of the device taking into account the given delta.
 *
 * @param pdDelataValue		the new delta value, may be NULL, in which case the current value is used
 * @param pdPercent			the new delta value, expressed in relative percent, may be NULL, in which case the current value is used
 * @param dInitalValue		the initial value that the user sees on screen
 * @param ppOutBand			the new band which fits the delta and the voltage/current
 * @param dOutResultValue	the resulting output value of the device
 *
 * @return					STATEERR_NO_ERROR on success
 *
 *********************************************************************** */
STATEERR DeviceState::ValidateDelta(const double* pdDelataValue, const double* pdPercent, double dInitalValue, WORKUNIT* pFrequency, bool bInSameBandFreq, PCBAND* ppOutBand, double& dOutResultValue, bool bSkipValidation /* = false */)
{
	STATEERR enumRet = STATEERR_NO_ERROR;

	double dDelataValue	= pdDelataValue	? *pdDelataValue	: m_dDeltaValue;
	double dPercent		= pdPercent		? *pdPercent		: m_wuDeltaPercent.value;
	double dLimit;
	if (*ppOutBand)
		dLimit = IsPositiveBand(*ppOutBand) ? (*ppOutBand)->max : -(*ppOutBand)->min;
	else
		dLimit = MAX_VOLTAGE_DC_VALUE;
	dLimit = dLimit * MAX_DELTA_PERCENT / 110.0;

	if (fabs(dPercent) > MAX_DELTA_PERCENT || fabs(dDelataValue) > dLimit)
		return STATEERR_OUT_OF_RANGE;

	// y = (1 + m/100)*x + c
	double dDeltaRelVal = (1 + dPercent / 100) * dInitalValue;
	double dResultValue = dDeltaRelVal + dDelataValue;

	WORKUNIT targetValue = { *ppOutBand, dResultValue};
	enumRet = bSkipValidation ? STATEERR_NO_ERROR : BandsManager::IsValidValue(&targetValue, false, pFrequency, bInSameBandFreq, m_bDC, STATEMODE_U == m_enumMode);
	if (STATEERR_NO_ERROR == enumRet)
	{
		// User can apply 1kV offset to 1uv. Prevent this case.
		bool bZerroBeforeRound = 0.0 == dInitalValue || -0.0 == dInitalValue;
		double dRounded = roundX(dInitalValue, targetValue.pBand->fraction);
		bool bZerroAfrerRound = 0.0 == dRounded || -0.0 == dRounded;

		// Check that initial value is valid for new band
		enumRet = bZerroBeforeRound == bZerroAfrerRound ? STATEERR_NO_ERROR : STATEERR_OUT_OF_RANGE;
		if (STATEERR_NO_ERROR == enumRet)
		{
			*ppOutBand = targetValue.pBand;
			dOutResultValue = dResultValue;
		}
	}
	return enumRet;
}

/** **********************************************************************
 *
 * Sets the new delta value and the new delta value in percent.
 *
 * @param bOn				turn on/off the delta mode
 * @param pdDelataValue		the new delta value, may be NULL, in which case the current value is used
 * @param pdPercent			the new delta value, expressed in relative percent, may be NULL, in which case the current value is used
 *
 * @return					STATEERR_NO_ERROR on success
 *
 *********************************************************************** */
STATEERR DeviceState::SetDelta(bool bOn, const double* pdDelataValue, const double* pdPercent, bool bSkipValidation /* = false */)
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if ( STATEMODE_U == m_enumMode || STATEMODE_I == m_enumMode)
	{
		enumRet = STATEERR_NO_ERROR;
		
		if (bOn)
		{
			enumRet = ValidateDelta(pdDelataValue, pdPercent, m_wuInitial.value, &m_wuFrequency, true, &m_wuInitial.pBand, m_dWithDeltaValue, bSkipValidation);
			if (STATEERR_NO_ERROR == enumRet)
			{
				UpdateValueDependencies();
				m_bIsDeltaEnabled = true;

				if (pdDelataValue)
				{
					m_dDeltaValue = *pdDelataValue;
				}

				if (pdPercent)
				{
					m_wuDeltaPercent.value = *pdPercent;
				}

				SetZeroMode(false);
			}
		}
		else
		{
			if (m_bIsDeltaEnabled)
			{
				m_wuInitial.value = m_dWithDeltaValue;
				ResetDelta(true);
			}
		}
	}
	return enumRet;
}

/** **********************************************************************
 *
 * Returns the current delta value
 *
 * @param pbOn				optional return value, whether the delta mode is on/off
 * @param ppBandValue		optional return value, the current delta value band
 * @param pdValue			optional return value, the current delta value
 * @param ppPercent			optional return value, the current delta value, expressed in percent
 *
 * @return					STATEERR_NO_ERROR on success
 *
 *********************************************************************** */
STATEERR DeviceState::GetDelta(bool* pbOn, PCBAND* ppBandValue, double* pdValue, PWORKUNIT* ppPercent)
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if ( STATEMODE_U == m_enumMode || STATEMODE_I == m_enumMode)
	{
		enumRet = STATEERR_NO_ERROR;
		SyncDeltaBand();
		if (pbOn)			{ *pbOn = m_bIsDeltaEnabled; }
		if (ppBandValue)	{ *ppBandValue = m_bIsDeltaEnabled ? m_wuInitial.pBand : NULL; }
		if (pdValue)		{ *pdValue = m_bIsDeltaEnabled ? m_dDeltaValue : NULL; }
		if (ppPercent)		{ *ppPercent = m_bIsDeltaEnabled ? &m_wuDeltaPercent : NULL; }
	}

	return enumRet;
}

void DeviceState::UpdateValueDependencies()
{
	if (!IsValidSense(m_enumSense))
	{
		m_enumSense = STATESENSE_TWO_WIRE; // Should be allways valid
		g_tracker.SetModified(STATECH_SENSE);
	}

	if (ManageHV(m_dWithDeltaValue, m_wuInitial.pBand))
	{
		// Output value can be changed.
		m_wuOutputValue.pBand = m_wuInitial.pBand;
		m_wuOutputValue.value = m_dWithDeltaValue;
		g_tracker.SetModified(STATECH_VALUE);
	}
}

bool DeviceState::IsExtDividerBand(uint8_t nBandIdx)
{
	switch (nBandIdx) {
	case EXBAND_DCU_P_10_MV_E:
	case EXBAND_DCU_N_10_MV_E:
	case EXBAND_DCU_P_100_MV_E:
	case EXBAND_DCU_N_100_MV_E:
		return true;
	default:
		return false;
	}
}

bool DeviceState::IsValidBandForDivider(uint8_t nBandIdx)
{
	switch (nBandIdx) {
#ifdef USE_DCU_10_MV
	case EXBAND_DCU_P_10_MV:
	case EXBAND_DCU_N_10_MV:
#endif
	case EXBAND_DCU_P_100_MV:
	case EXBAND_DCU_N_100_MV:
	case EXBAND_DCU_P_10_MV_E:
	case EXBAND_DCU_N_10_MV_E:
	case EXBAND_DCU_P_100_MV_E:
	case EXBAND_DCU_N_100_MV_E:
		return true;
	default:
		return false;
	}
}

bool DeviceState::IsExtDividerPossible()
{
	if ((STATEMODE_U == m_enumMode) && m_bDC) {
		return IsValidBandForDivider(m_wuInitial.pBand->bandIdx);
	}
	return false;
}

bool DeviceState::GetDividerEnabled()
{
	if (!m_wuInitial.pBand)
		return false;
	if ((STATEMODE_U == m_enumMode) && m_bDC &&
		IsExtDividerBand(m_wuInitial.pBand->bandIdx))
		return true;
	return false;
}

bool DeviceState::IsValidSenseForDCV(uint8_t bandIdx)
{
	return IsExtDividerBand(bandIdx);
}

bool DeviceState::IsValidSenseForACV(uint8_t bandIdx)
{
	switch (bandIdx)
	{
		case EXBAND_ACU_100MV:
		case EXBAND_ACU_10MV:
#ifdef USE_ACU_1MV
		case EXBAND_ACU_1MV:
#endif
			return false;
		default:
			return true;
	}
}

/** **********************************************************************
 *
 * Checks whether the given sense may be used.
 *
 * @param sense		the type of sense
 *
 * @return			true if the sense can be applied
 *
 *********************************************************************** */
bool DeviceState::IsValidSense(STATESENSE sense)
{
	if (STATESENSE_TWO_WIRE == sense)
		return true;
	if (STATESENSE_FOUR_WIRE == sense)
	{
		if (STATEMODE_U == m_enumMode) {
			return m_bDC
				? IsValidSenseForDCV(m_wuInitial.pBand->bandIdx)
				: IsValidSenseForACV(m_wuInitial.pBand->bandIdx);
		}
		else 
		{
			// For resistors and current
			// TODO: Current is filtered out but logically should be evaluated by this method.
			return true;
		}
	}
	return false;
}

STATEERR DeviceState::GetSenseMode(STATESENSE& sense) const
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if (STATEMODE_U == m_enumMode || STATEMODE_R == m_enumMode)
	{
		enumRet = STATEERR_NO_ERROR;
		sense = m_enumSense;
	}
	return enumRet;
}

STATEERR DeviceState::SetSenseMode(STATESENSE sense)
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if (STATEMODE_U == m_enumMode || STATEMODE_R == m_enumMode)
	{
		if (STATEMODE_U == m_enumMode && STATEOUT_ON == GetOutState())
		{
			enumRet = STATEERR_DISABLE_OUT;
		}
		else
		{
			if (!IsValidSense(sense))
			{
				// Works from coaxial cabel. Remote sense impossible.
			}
			else
			{
				enumRet = STATEERR_NO_ERROR;
				m_enumSense = sense;
				g_tracker.SetModified(STATECH_SENSE);
			}
		}
	}
	return enumRet;
}

STATEERR DeviceState::GetZeroMode(bool& bOn) const
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if ((STATEMODE_U == m_enumMode || STATEMODE_I == m_enumMode) && m_bDC)
	{
		enumRet = STATEERR_NO_ERROR;
		bOn = m_bZeroMode;
	}
	return enumRet;
}

STATEERR DeviceState::SetZeroMode(bool bOn)
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if ((STATEMODE_U == m_enumMode || STATEMODE_I == m_enumMode) && m_bDC)
	{
		enumRet = STATEERR_NO_ERROR;
		m_bZeroMode = bOn;
		g_tracker.SetModified(STATECH_VALUE);
	}
	return enumRet;
}

/** **********************************************************************
 *
 * Provides device output value with correction.
 * Correction depends on calibration data for selected mode, band and frequency.
 *
 * @param value	structure with band and corrected value to fill it
 *
 * @return			STATEERR_NO_ERROR on success otherwise STATEERR_INVALID_MODE
 *
 *********************************************************************** */
STATEERR DeviceState::GetCorrectedValue(WORKUNIT& value) const
{
	STATEERR enumRet = STATEMODE_U == m_enumMode || STATEMODE_I == m_enumMode || STATEMODE_R == m_enumMode 
					? STATEERR_NO_ERROR
					: STATEERR_INVALID_MODE;

	if (STATEERR_NO_ERROR == enumRet)
	{
		enumRet = GetOutputValue(value);
		if (STATEERR_NO_ERROR == enumRet && m_bIsCalibrationEnabled)
		{	
			if (m_bZeroMode)
			{
				value.value = 0.0;
			}

			MODE eMode = ConvertMode(m_enumMode, m_bDC);
			value.value = gs_calibrationManager.GetCorrectedValue(eMode, value, m_wuFrequency.value);
		}
	}

	return enumRet;
}

/** **********************************************************************
 *
 * Provides device output value.
 * Output value consists delta correction.
 * Also it is used to hold voltage on previous state when "high voltage confirmation" is active but is not confirmed by user.
 *
 * @param value	structure with band and value to fill it
 *
 * @return			STATEERR_NO_ERROR on success otherwise STATEERR_INVALID_MODE
 *
 *********************************************************************** */
STATEERR DeviceState::GetOutputValue(WORKUNIT& value) const
{
	STATEERR enumRet = STATEMODE_U == m_enumMode || STATEMODE_I == m_enumMode || STATEMODE_R == m_enumMode 
						? STATEERR_NO_ERROR
						: STATEERR_INVALID_MODE;

	if (STATEERR_NO_ERROR == enumRet)
	{
		value = m_wuOutputValue;
	}

	return enumRet;
}

bool DeviceState::GetSwitchOutState() const
{
	return m_bOutOn;
}

/** **********************************************************************
 *
 * Stores overload signal for UI and disables output.
 * Output disable command may be suppressed for AARM because of AARM allready has "output disabled" state in this case
 *
 * @param bOverload		new overload signal
 *
 *********************************************************************** */
#define OVERLOAD_ON_DELAY_TIME BEEP_TIME_HV_BEFORE * 2
void DeviceState::SetOverload(bool bOverload)
{
	if (m_unOverloadLockTime && CheckUpdateTime(OVERLOAD_ON_DELAY_TIME, &m_unOverloadLockTime))
	{
		// Clear overload delay time
		m_unOverloadLockTime = 0;
	}

	if (!m_unOverloadLockTime && !m_bHVConfirmRequire)
	{
		if (bOverload)
		{
			if (STATEMODE_U == m_enumMode)
			{
				uint8_t bandIdx = m_wuOutputValue.pBand->bandIdx;
				if ((m_bDC && (bandIdx == EXBAND_DCU_N_1000_V || bandIdx == EXBAND_DCU_P_1000_V)) || !m_bDC && bandIdx == EXBAND_ACU_1000V)
				{
					SwitchOut(false, false, false);
				}
			}
		}

		m_bOverload = bOverload;
	}
	else
	{
		m_bOverload = false;
	}
}

/** **********************************************************************
 *
 * Switches the output of the device.
 *
 * @param bOn			switches output on/off
 * @param bFromUser		whether the action is performed by the user or automatically
 * @param bForce		From Remote interface
 *
 * @return				STATEERR_NO_ERROR on success
 *
 *********************************************************************** */
STATEERR DeviceState::SwitchOut(bool bOn, bool bFromUser, bool bForce)
{
	bool bOutStateOld = m_bOutOn;

	if (m_bOutOn != bOn)
	{
		SetOverload(false); // N.B. depends on m_bHVConfirmRequire and m_unOverloadLockTime
		if (!bForce && bOn && m_bHVRange && !m_bHVConfirmRequire && bFromUser)
		{
			// User want to enable HV output. Set out led blinking and and waiting for user presses output on button again.
			m_bHVConfirmRequire = true;
		}
		else
		{
			m_bOutOn = bOn;
			m_bHVConfirmRequire = false;
			if (m_bOutOn)
			{
				CheckUpdateTime(OVERLOAD_ON_DELAY_TIME, &m_unOverloadLockTime);
			}
		}
	}
	else if (!m_bOutOn && m_bHVConfirmRequire)
	{
		// Decline confirmation
		m_bHVConfirmRequire = false;
	}

	if (bForce || (m_bHVRange && bOn && m_bOutOn && m_bHVConfirmRequire && bFromUser))
	{
		// User has psessed output button again. The refore device must apply holded value to output
		m_bHVConfirmRequire = false;

		// Sync display and device holded values
		m_wuOutputValue.pBand = m_wuInitial.pBand;
		m_wuOutputValue.value = m_dWithDeltaValue;

		// Sync output value with real executive part
		g_tracker.SetModified(STATECH_VALUE);
	}

	// Sync front panel led and buzzer with internal device state
	bool bCancel = CLogic_SwitchOut((char)m_bOutOn, (char)m_bHVRange, (char)m_bHVConfirmRequire, (char)bForce);

	if (bCancel)
	{
		// User press any key while device prepares to reproduce high voltage. Emergency STOP.
		m_bOutOn = false;
	}

	if (bOutStateOld != m_bOutOn)
	{
		// Sync output realay state with real executive part
		g_tracker.SetModified(STATECH_OUT);
	}

	return STATEERR_NO_ERROR;
}

STATEERR DeviceState::SwitchOut(bool bOn, bool bForce)
{
	return SwitchOut(bOn, true, bForce);
}

STATEERR DeviceState::GetVoltageInfo(STATEVOLINFO* pInfo) const
{
	STATEERR enumRet = STATEERR_INVALID_MODE;
	if ( STATEMODE_U == m_enumMode)
	{
		enumRet = STATEERR_NO_ERROR;
		*pInfo = m_bHVRange ? STATEVOLINFO_HV : STATEVOLINFO_LV;
	}
	return enumRet;
}

STATEERR DeviceState::SetFreqCorrector(double corr)
{
	if (corr < -2.0e-2 || corr > 5.0e-2)
		return STATEERR_OUT_OF_RANGE;
	m_dFreqCorrector = corr;
	g_tracker.SetModified(STATECH_CORR);
	return STATEERR_NO_ERROR;
}
