#include "UI.h"
#include "UIInterface.h"
#include "../LogicInterface.h"
#include "Drivers/keyboard.h"
#include "Drivers/iLcdSpi.h"
#include "SerialCmdParser.h"
#include "Common.h"
#include "../Drivers/xPort.h"
#include "../Drivers/uart.h"
#include "../Drivers/panel.h"
#include "../Drivers/xPort.h"
#include "../Drivers/usb/usb_dev_serial.h"
#include "../Drivers/gpib.h"

Vector gs_vecDispCmdBuff;

void CUI_StartDisplayData(char chStartSeq)
{
	vector_clear(&gs_vecDispCmdBuff);
	vector_append(&gs_vecDispCmdBuff, chStartSeq);
}

void CUI_SendDisplayData(const char* pBuf, int length)
{
	int c = 0;
	for (c = 0; c < length; c++)
	{
		char chByte = pBuf[c];
		vector_append(&gs_vecDispCmdBuff, chByte);
		if (0xAA == chByte)
		{
			vector_append(&gs_vecDispCmdBuff, chByte);
		}
	}
}

char CUI_ReadDisplayData(char* pBuf, unsigned int* pnBuffDataLen)
{
	return SCP_ReadPortData(PORTNAME_DISP, pBuf, pnBuffDataLen);
}

#define LCD_ACK_MAX_WAIT_TIME_OUT 400 // 200 not enough to write very long text (cobration with shunts)
uint32_t s_unWaitACKTimeOut = 0;

char CUI_WaitDisplayACK()
{
	RouteAndSendData(PORTNAME_DISP, (char*)gs_vecDispCmdBuff.data , gs_vecDispCmdBuff.size);
	vector_free(&gs_vecDispCmdBuff);
#ifndef LAUNCHPAD
	gs_UIFeedbackData.UIACKCode = ACKDISP_WAIT;

	// Set maximum data receive timeout
	SetUpdateTime(LCD_ACK_MAX_WAIT_TIME_OUT, &s_unWaitACKTimeOut);
	while (ACKDISP_WAIT == gs_UIFeedbackData.UIACKCode)
	{
		ManageSerial();
		if (CheckUpdateTimeForLoop(LCD_ACK_MAX_WAIT_TIME_OUT, &s_unWaitACKTimeOut))
		{
			// ACK time out.
			gs_UIFeedbackData.UIACKCode = ACKDISP_FAILED;
			ReportError("CUI_WaitDisplayACK() - ACK wait timeout");
			break;
		}
	}
	char bRet = ACKDISP_OK == gs_UIFeedbackData.UIACKCode;
	gs_UIFeedbackData.UIACKCode = ACKDISP_NONE;

	return bRet;
#else
	return true;
#endif
}

/** **********************************************************************
 *
 * Provides error message from UI part
 * @param error text to log
 *
 *********************************************************************** */
__declspec(dllexport) void __stdcall CUI_SetLastError(char* pszText)
{
	ReportError(pszText);
}

void CUI_GetTextExtent(uint16_t* punX, uint16_t* punY)
{
	gs_UIFeedbackData.eTextExtened = ACKDISP_WAIT;

	// Set maximum data receive timeout
	SetUpdateTime(LCD_ACK_MAX_WAIT_TIME_OUT, &s_unWaitACKTimeOut);
	while (ACKDISP_WAIT == gs_UIFeedbackData.eTextExtened)
	{
		ManageSerial();
		if (CheckUpdateTimeForLoop(LCD_ACK_MAX_WAIT_TIME_OUT, &s_unWaitACKTimeOut))
		{
			// ACK time out.
			gs_UIFeedbackData.eTextExtened = ACKDISP_FAILED;
			break;
		}
	}
	char bRet = ACKDISP_OK == gs_UIFeedbackData.eTextExtened;
	gs_UIFeedbackData.eTextExtened = ACKDISP_NONE;

	if (bRet)
	{
		*punX = gs_UIFeedbackData.UITextSize.X;
		*punY = gs_UIFeedbackData.UITextSize.Y;
	}
	else
	{
		ReportError("GetTextExtent() - returns false");
	}
}

void ManageUI()
{
	int8_t nKeyCode = 0;
	uint8_t	bPressed = 0;

	if (ManageKeyboard(&nKeyCode, &bPressed))
	{
		if (bPressed)
		{
			KeyBeep();
		}
		UI_ButtonChanged(nKeyCode, bPressed);
	}

	if (gs_UIFeedbackData.pUITouchPoint || gs_UIFeedbackData.pUIReleasePoint)
	{
		bPressed = NULL != gs_UIFeedbackData.pUITouchPoint;

		if (bPressed)
		{
			KeyBeep();
		}

		POINT_T** ppPoint = (bPressed ? &gs_UIFeedbackData.pUITouchPoint : &gs_UIFeedbackData.pUIReleasePoint);
		UI_TouchEvent((*ppPoint)->X, (*ppPoint)->Y, bPressed);
		*ppPoint = NULL;
	}

	if (gs_UIFeedbackData.unKeyCodePush || gs_UIFeedbackData.unKeyCodeRelease)
	{
		bPressed = NULL != gs_UIFeedbackData.unKeyCodePush;
		int8_t* punCode = bPressed ? &gs_UIFeedbackData.unKeyCodePush : &gs_UIFeedbackData.unKeyCodeRelease;

		UI_ButtonChanged(*punCode, bPressed);

		*punCode = 0;
	}

	UI_Loop();
}

__declspec(dllexport) const char* __stdcall CUI_PostInit(INITSTEP eInitStep, void* pCtx)
{
	const char* pszRet = "Invalid post initialization step";
	if (INITSTEP_XPORT == eInitStep)
	{
		void** pParams = (void**)pCtx;
		PLAN_CONF pConf = (PLAN_CONF)(pParams[0]);
		const char* pszPass = (const char*)(pParams[1]);
		pszRet = XPORT_PostInit(pConf, pszPass);
	}
	else if (INITSTEP_GPIB == eInitStep)
	{
#ifndef LAUNCHPAD
		pszRet = GPIB_PostInit((unsigned int)pCtx);
#endif
	}
	else if (INITSTEP_UART == eInitStep)
	{
		pszRet = UART_PostInit((PRS232_CONF)pCtx);
	}
	else if (INITSTEP_USB == eInitStep)
	{
		pszRet = USB_PostInit();
	}
	else if (INITSTEP_KBD == eInitStep)
	{
		pszRet = PANEL_PostInit();
	}
	else if (INITSTEP_AARM == eInitStep)
	{
		pszRet = Logic_PostInit();
	}
	else
	{
		ASSERT(!pszRet);
	}
	return pszRet;
}
