//*****************************************************************************
//
// usbmode.c - Functions related to dual mode USB device/host operation.
//
// Copyright (c) 2008-2014 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
// 
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
// 
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
// 
// This is part of revision 2.1.0.12573 of the Tiva USB Library.
//
//*****************************************************************************

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_usb.h"
#include "driverlib/debug.h"
#include "driverlib/interrupt.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/usb.h"
#include "driverlib/rtos_bindings.h"
#include "usblib/usblib.h"
#include "usblib/usblibpriv.h"
#include "usblib/device/usbdevice.h"
#include "usblib/usblibpriv.h"

//*****************************************************************************
//
//! \addtogroup general_usblib_api
//! @{
//
//*****************************************************************************

//*****************************************************************************
//
// The following label defines interrupts that we will always pass to the host
// interrupt handler even if we are in dual mode and not yet sure of which
// mode we are operating in.
//
//*****************************************************************************
#define USB_HOST_INTS           (USB_INTCTRL_VBUS_ERR)

//*****************************************************************************
//
// Global variable indicating which mode of operation the application has
// requested.
//
//*****************************************************************************
volatile tUSBMode g_iUSBMode = eUSBModeNone;

//*****************************************************************************
//
// The default and the current polling rate for the USB OTG library.
//
//*****************************************************************************
volatile uint32_t g_ui32PollRate;

//*****************************************************************************
//
// The current time remaining in milliseconds before checking the cable
// connection.
//
//*****************************************************************************
volatile uint32_t g_ui32WaitTicks = 0;

//*****************************************************************************
//
// Global variable holding a pointer to the callback function which will be
// called when the USB mode changes between device and host.
//
//*****************************************************************************
static tUSBModeCallback g_pfnUSBModeCallback;

//*****************************************************************************
//
//! Allows dual mode application to switch between USB device and host modes
//! and provides a method to force the controller into the desired mode.
//!
//! \param ui32Index specifies the USB controller whose mode of operation is to
//! be set.  This parameter must be set to 0.
//! \param iUSBMode indicates the mode that the application wishes to operate
//! in.  Valid values are \b eUSBModeDevice to operate as a USB device and
//! \b eUSBModeHost to operate as a USB host.
//! \param pfnCallback is a pointer to a function which the USB library will
//! call each time the mode is changed to indicate the new operating mode.  In
//! cases where \e iUSBMode is set to either \b eUSBModeDevice or
//! \b eUSBModeHost, the callback will be made immediately to allow the
//! application to perform any host or device specific initialization.
//!
//! This function allows a USB application that can operate in host
//! or device mode to indicate to the USB stack the mode that it wishes to
//! use.  The caller is responsible for cleaning up the interface and removing
//! itself from the bus prior to making this call and reconfiguring afterwards.
//! The \e pfnCallback function can be a NULL(0) value to indicate that no
//! notification is required.
//!
//! For successful dual mode mode operation, an application must register
//! USB0DualModeIntHandler() as the interrupt handler for the USB0 interrupt.
//! This handler is responsible for steering interrupts to the device or host
//! stack depending upon the chosen mode.  Devices which do not require dual
//! mode capability should register either \e USB0DeviceIntHandler() or
//! \e USB0HostIntHandler() instead.  Registering \e USB0DualModeIntHandler()
//! for a single mode application will result in an application binary larger
//! than required since library functions for both USB operating modes will be
//! included even though only one mode is required.
//!
//! Single mode applications (those offering exclusively USB device or USB
//! host functionality) are only required to call this function if they need to
//! force the mode of the controller to Host or Device mode.  This is usually
//! in the event that the application needs to reused the USBVBUS and/or USBID
//! pins as GPIOs.
//!
//! \return None.
//
//*****************************************************************************
void
USBStackModeSet(uint32_t ui32Index, tUSBMode iUSBMode,
                tUSBModeCallback pfnCallback)
{
    //
    // Check the arguments.
    //
    ASSERT(ui32Index == 0);

    //
    // Remember the mode so that we can steer the interrupts appropriately.
    //
    g_iUSBMode = iUSBMode;

    //
    // Remember the callback pointer.
    //
    g_pfnUSBModeCallback = pfnCallback;

    //
    // If we are being asked to be either a host or device, we will not be
    // trying to auto-detect the mode so make the callback immediately.
    //
    if((iUSBMode == eUSBModeDevice) || (iUSBMode == eUSBModeHost))
    {
        //
        // Make sure that a callback was provided.
        //
        if(g_pfnUSBModeCallback)
        {
            g_pfnUSBModeCallback(0, iUSBMode);
        }
    }
}

//*****************************************************************************
//
// Close the Doxygen group general_usblib_api.
//! @}
//
//*****************************************************************************

//*****************************************************************************
//
//! This call sets the USB OTG controllers poll rate when checking for the mode
//! of the controller.
//!
//! \param ui32Index specifies which USB controller to set the polling rate.
//! \param ui32PollRate is the rate in milliseconds to poll for changes in the
//! controller mode.
//!
//! This function is called to set the USB OTG libraries polling rate when
//! checking the status of the cable.  The \e ui32PollRate value used sets the
//! rate in milliseconds that the USB OTG library will poll the cable to see
//! if the controller should enter host mode.  This value has no effect on
//! device detection rate as the controller will detect being connected to a
//! host controller automatically.  The \e ui32PollRate can be set to 0 to
//! disable polling.  The USB OTG library can still function with the polling
//! rate set to zero, however it will fail to detect host mode properly when no
//! device is present at the end of the USB OTG B side of the cable.
//!
//! \note This function should only be called on devices that support OTG
//! functionality.
//!
//! \return None.
//
//*****************************************************************************
void
USBOTGPollRate(uint32_t ui32Index, uint32_t ui32PollRate)
{
    //
    // Save the timeout.
    //
    g_ui32PollRate = ui32PollRate;
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
