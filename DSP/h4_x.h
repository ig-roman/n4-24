
// h4_x.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// Ch4_xApp:
// See h4_x.cpp for the implementation of this class
//

class Ch4_xApp : public CWinApp
{
public:
	Ch4_xApp();

// Overrides
public:
	virtual BOOL InitInstance();
	PVOID m_pdlg;

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern Ch4_xApp theApp;