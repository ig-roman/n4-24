#include "TopCommon.h"
#include <math.h>

#include <stdio.h>
#include <stdlib.h>


const char * STR_ORG_NAME = ID_ORG;
const char * STR_PRODUCT_NAME = ID_PRODUCT;
const char * STR_SERIAL_NR = ID_SERIAL;
const char * STR_SW_VERSION = VER_VERSION_STR;
const char * STR_ERROR = "ERROR";

const BAND sm_arrBandsDCU[] =
{
	//								Min		Max
#ifdef USE_DCU_10_MV
	{ EXBAND_DCU_P_10_MV ,	8,	9,	0,		0.011	},
#endif
	{ EXBAND_DCU_P_100_MV,	8,	8,	0,		0.11	},
	{ EXBAND_DCU_P_1_V   ,	8,	7,	0,		1.1		},
	{ EXBAND_DCU_P_10_V  ,	8,	6,	0,		11		},
	{ EXBAND_DCU_P_100_V ,	8,	5,	0,		110		},
	{ EXBAND_DCU_P_1000_V,	8,	4,	20,		MAX_VOLTAGE_DC_VALUE	},
	{ EXBAND_DCU_P_10_MV_E, 8,  9,  0,      0.011   },
	{ EXBAND_DCU_P_100_MV_E, 8, 8,  0,      0.11    },

#ifdef USE_DCU_10_MV
	{ EXBAND_DCU_N_10_MV ,	8,	9,	-0.011,	0		},
#endif
	{ EXBAND_DCU_N_100_MV,	8,	8,	-0.11,	0		},
	{ EXBAND_DCU_N_1_V   ,	8,	7,	-1.1,	0		},
	{ EXBAND_DCU_N_10_V  ,	8,	6,	-11,	0		},
	{ EXBAND_DCU_N_100_V ,	8,	5,	-110,	0		},
	{ EXBAND_DCU_N_1000_V,	8,	4,	MIN_VOLTAGE_DC_VALUE,	-20		},
	{ EXBAND_DCU_N_10_MV_E, 8,  9,  -0.011, 0       },
	{ EXBAND_DCU_N_100_MV_E, 8, 8,  -0.11,  0       },
	{ NULL,					0,	0,	0,		0		}
};

const BAND sm_arrBandsACU[] =
{
#ifdef USE_ACU_1MV
	{ EXBAND_ACU_1MV,	5,	7,	MIN_VOLTAGE_AC_VALUE, 0.0011	},
	{ EXBAND_ACU_10MV,	6,	7,	0.0009,	0.011	},
#else
	{ EXBAND_ACU_10MV,	6,	7,	MIN_VOLTAGE_AC_VALUE,	0.011	},
#endif
	{ EXBAND_ACU_100MV,	7,	7,	0.009,	0.11	},
	{ EXBAND_ACU_1V,	7,	6,	0.09,	1.1		},
	{ EXBAND_ACU_10V,	7,	5,	0.9,	11		},
	{ EXBAND_ACU_100V,	7,	4,	9,		110		},
	{ EXBAND_ACU_1000V,	7,	3,	90,		MAX_VOLTAGE_AC_VALUE },
	{ NULL,				0,	0,	0,		0		}
};

const BAND sm_arrBandsACUFreq[] =
{
	{ EXTBAND_ACU_FREQ_100,		6,	3,	9.8,	100		},
	{ EXTBAND_ACU_FREQ_1K,		6,	2,	100,	1000	},
	{ EXTBAND_ACU_FREQ_10K,		6,	1,	1000,	10000	},
	{ EXTBAND_ACU_FREQ_100K,	6,	0,	10000,	100000	},
	{ EXTBAND_ACU_FREQ_1M,		6,	-1,	100000,	MAX_FREQUENCY_VALUE	},
	{ NULL,						0,	0,	0,		0		}
};

// Dependency: sm_arrDCIBandsToLLB, s_arrDCIBandsToPWM
const BAND sm_arrBandsDCI[] =
{
	//								Min				Max
	{ EXTBAND_DCI_P_10UA,	7,	11,	0,				0.000011	},
	{ EXTBAND_DCI_P_100UA,	8,	11,	0,				0.00011		},
	{ EXTBAND_DCI_P_1MA,	8,	10,	0,				0.0011		},
	{ EXTBAND_DCI_P_10MA,	8,	9,	0,				0.011		},
	{ EXTBAND_DCI_P_100MA,	8,	8,	0,				0.11		},
	{ EXTBAND_DCI_P_1A,		8,	7,	0,				1.1			},
	{ EXTBAND_DCI_P_2A,		8,	6,	0,				2.1			},
	{ EXTBAND_DCI_P_10A_AMP, 8, 6,	0,	    		11			},
	{ EXTBAND_DCI_P_30A_AMP,8,	5,	0,	    		MAX_CURENT_VALUE },

	{ EXTBAND_DCI_N_10UA,	7,	11,	-0.000011,	0			},
	{ EXTBAND_DCI_N_100UA,	8,	11,	-0.00011,	0			},
	{ EXTBAND_DCI_N_1MA,	8,	10,	-0.0011,	0			},
	{ EXTBAND_DCI_N_10MA,	8,	9,	-0.011,		0			},
	{ EXTBAND_DCI_N_100MA,	8,	8,	-0.11,		0			},
	{ EXTBAND_DCI_N_1A,		8,	7,	-1.1,		0			},
	{ EXTBAND_DCI_N_2A,		8,	6,	-2.1,		0			},
	{ EXTBAND_DCI_N_10A_AMP,8,	6,	-11,		0    		},
	{ EXTBAND_DCI_N_30A_AMP,8,	5,	MIN_CURENT_VALUE,	   0},
	{ NULL,					0,	0,	0,			0			}
};

// Dependency: sm_arrACIBandsToLLB, sm_arrBandsACI
const BAND sm_arrBandsACI[] =
{
	{ EXTBAND_ACI_100UA,    6,  9,  MIN_CURENT_AC_VALUE,    0.00011 },
	{ EXTBAND_ACI_1MA,		7,	9,	0.00009,	0.0011		},
	{ EXTBAND_ACI_10MA,		7,	8,	0.0009,		0.011		},
	{ EXTBAND_ACI_100MA,	7,	7,	0.009,		0.11		},
	{ EXTBAND_ACI_1A,		7,	6,	0.09,		1.1			},
	{ EXTBAND_ACI_2A,		7,	5,	0.9,		2.1			},
	{ EXTBAND_ACI_10A_AMP,	7,	5,	1.9,		11			},	// 11 = sm_arrBandsACU[EXBAND_ACU_10V].max can't get it from array because linker moves whole array to RAM instead FLASH
	{ EXTBAND_ACI_30A_AMP,	7,	4,	9,	MAX_CURENT_AC_VALUE	},	// 9 = sm_arrBandsACU[EXBAND_ACU_100V].min can't get it from array because linker moves whole array to RAM instead FLASH
	{ NULL,						0,	0,	0,		0			}
};

const BAND sm_arrBandsACIFreq[] =
{
	{ EXTBAND_ACI_FREQ_100,		6,	3,	9.8,	100		},
	{ EXTBAND_ACI_FREQ_1K,		6,	2,	100,	1000	},
	{ EXTBAND_ACI_FREQ_10K5,	6,	1,	1000,	10000	},
	{ NULL,						0,	0,	0,		0		}
};

const FREQ_VOL_RELATION sm_arrFreqCurRelation[] =
{
	// Make sure that frequency goes from min to max sequentially and without gaps.
	// I	Min Freq	Max freq
	{ 20,	9.8,		30		},
	{ 30,	30,			5000	},
	{ 20,	5000,		10500	},
	NULL
};

const FREQ_VOL_RELATION sm_arrFreqVolRelation1000VBandOnly[] =
{
	// Make sure that frequency goes from min to max sequentially and without gaps.
	// U					Min Freq	Max freq
	{ 400,					10,			20		},
	{ 750,					20,			30		},
	{ MAX_VOLTAGE_AC_VALUE,	30,			70000	},
	{ 750,					70000,		MAX_FREQUENCY_VALUE_1000V_BAND },
	NULL
};

const BAND sm_arrBandsRs[] =
{
	{ EXTBAND_R_0R,		7,	6,	R_VAL_0R	},
	{ EXTBAND_R_1R,		7,	6,	R_VAL_1R	},
	{ EXTBAND_R_10R,	7,	5,	R_VAL_10R	},
	{ EXTBAND_R_100R,	7,	4,	R_VAL_100R	},
	{ EXTBAND_R_1K,		7,	3,	R_VAL_1K	},
	{ EXTBAND_R_10K,	7,	2,	R_VAL_10K	},
	{ EXTBAND_R_100K,	7,	1,	R_VAL_100K	},
	{ EXTBAND_R_1M,		7,	0,	R_VAL_1M	},
	{ EXTBAND_R_10M,	7,	-1,	R_VAL_10M	},
	{ EXTBAND_R_100M,	7,	-2,	R_VAL_100M	},
	{ NULL,				0,	0,	0			}
};

// Dependency: sm_arrBandsACI
const BANDTOPWM s_arrACIBandsToPWM[] =
{
	{ EXTBAND_ACI_100UA,	100000	},
	{ EXTBAND_ACI_1MA,		10000	},
	{ EXTBAND_ACI_10MA,		1000	},
	{ EXTBAND_ACI_100MA,	100		},
	{ EXTBAND_ACI_1A,		10		},
	{ EXTBAND_ACI_2A,		5		},
	{ EXTBAND_ACI_10A_AMP,	1		},
	{ EXTBAND_ACI_30A_AMP,	0.1		}
};

const BANDTOPWM s_arrDCIBandsToPWM[] =
{
	{ EXTBAND_DCI_P_10UA,	100000	},
	{ EXTBAND_DCI_P_100UA,	100000	},
	{ EXTBAND_DCI_P_1MA,	10000	},
	{ EXTBAND_DCI_P_10MA,	1000	},
	{ EXTBAND_DCI_P_100MA,	100		},
	{ EXTBAND_DCI_P_1A,		10		},
	{ EXTBAND_DCI_P_2A,		5		},
	{ EXTBAND_DCI_P_10A_AMP,1		},
	{ EXTBAND_DCI_P_30A_AMP,0.1		},

	{ EXTBAND_DCI_N_10UA,	100000	},
	{ EXTBAND_DCI_N_100UA,	100000	},
	{ EXTBAND_DCI_N_1MA,	10000	},
	{ EXTBAND_DCI_N_10MA,	1000	},
	{ EXTBAND_DCI_N_100MA,	100		},
	{ EXTBAND_DCI_N_1A,		10		},
	{ EXTBAND_DCI_N_2A,		5		},
	{ EXTBAND_DCI_N_10A_AMP,1		},
	{ EXTBAND_DCI_N_30A_AMP,0.1		}
};

const BANDTOPWM s_arrDCUBandsToPWM[] =
{
#ifdef USE_DCU_10_MV
	{ EXBAND_DCU_P_10_MV,	1000	},
#endif
	{ EXBAND_DCU_P_100_MV,	100		},
	{ EXBAND_DCU_P_1_V,		10		},
	{ EXBAND_DCU_P_10_V,	1		},
	{ EXBAND_DCU_P_100_V,	0.1		},
	{ EXBAND_DCU_P_1000_V,	0.01	},
	{ EXBAND_DCU_P_10_MV_E, 1000    },
	{ EXBAND_DCU_P_100_MV_E,100     },

#ifdef USE_DCU_10_MV
	{ EXBAND_DCU_N_10_MV,	1000	},
#endif
	{ EXBAND_DCU_N_100_MV,	100		},
	{ EXBAND_DCU_N_1_V,		10		},
	{ EXBAND_DCU_N_10_V,	1		},
	{ EXBAND_DCU_N_100_V,	0.1		},
	{ EXBAND_DCU_N_1000_V,	0.01	},
	{ EXBAND_DCU_N_10_MV_E, 1000    },
	{ EXBAND_DCU_N_100_MV_E,100     },
	NULL
};

const BANDTOPWM s_arrACUBandsToPWM[] =
{
#ifdef USE_ACU_1MV
	{ EXBAND_ACU_1MV,	10000	},
#endif
	{ EXBAND_ACU_10MV,	1000	},
	{ EXBAND_ACU_100MV,	100		},
	{ EXBAND_ACU_1V,	10		},
	{ EXBAND_ACU_10V,	1		},
	{ EXBAND_ACU_100V,	0.1		},
	{ EXBAND_ACU_1000V,	0.01	},
	NULL
};

double BandToPwmMultiplier(MODE eMode, uint8_t bandIdx)
{
	const BANDTOPWM* arrBandData = NULL;
	switch (eMode)
	{
		case MODE_ACI: arrBandData = s_arrACIBandsToPWM; break;
		case MODE_DCI: arrBandData = s_arrDCIBandsToPWM; break;
		case MODE_ACU: arrBandData = s_arrACUBandsToPWM; break;
		case MODE_DCU: arrBandData = s_arrDCUBandsToPWM; break;
	}

	return arrBandData[bandIdx].multiplier;
}

#define MAX_POW10_TABLE 13
const double s_dPow10Arr[MAX_POW10_TABLE * 2 + 1] =
{
	1e-13, 1e-12,	1e-11,	1e-10,	1e-9,	1e-8,	1e-7,	1e-6,	1e-5,	1e-4,	1e-3,	1e-2,	1e-1,
	1,
	1e1,	1e2,	1e3,	1e4,	1e5,	1e6,	1e7,	1e8,	1e9,	1e10,	1e11,	1e12,	1e13
};

double Pow10Fast(int8_t nFraction)
{
	// Can't compile AARM with this ASSERT because out of flash memory
	// ASSERT(nFraction <= MAX_POW10_TABLE && nFraction >= -MAX_POW10_TABLE);
	double dRet = s_dPow10Arr[nFraction + MAX_POW10_TABLE];
	return dRet;
}

double roundX(double x, int8_t nFraction)
{
	double dScale = Pow10Fast(nFraction);
	double dTruncated, dRoundedFraction;
	double dFraction = modf(x * dScale, &dTruncated);
	modf(2.0 * dFraction, &dRoundedFraction);
	return (dTruncated + dRoundedFraction) / dScale;
}

/** **********************************************************************
 *
 * Counts CRC-8 checksum without pre-generated table (slower).
 *
 * @param pData			data to count
 * @param unLen			length of buffer
 * @param unPolynomial	initial polynomial
 *
 * @return CRC value using x^8 + x^2 + x + 1 polynomial.
 *
 *********************************************************************** */
uint8_t countCrc8(const uint8_t *pData, uint16_t unLen, uint8_t unPolynomial)
{
	uint16_t unCrcRet = unPolynomial;
	int i = 0, j = 0;

	for (j = unLen; j > 0; j--, pData++)
	{
		unCrcRet ^= (*pData << 8);
		for (i = 8; i > 0; i--)
		{
			if (unCrcRet & 0x8000)
			{
				unCrcRet ^= (0x1070 << 3);
			}
			unCrcRet <<= 1;
		}
	}

	return (uint8_t)(unCrcRet >> 8);
}

/** **********************************************************************
 *
 * Periodically returns true for defined period.
 *
 * @param unPeriod			constant value of period in milliseconds
 * @param punTriggerTime	new time in future for triggering. If punTriggerTime is 0 function sets new time and returns true.
 *
 * @return true if punTriggerTime equals or less than actual time
 *
 *********************************************************************** */
bool CheckUpdateTime(uint32_t unPeriod, uint32_t* punTriggerTime)
{
	uint32_t unNewTime = GetTime();

	if (0 == *punTriggerTime)
	{
		// Initialization or first call
		*punTriggerTime = unNewTime + unPeriod;
		return true;
	}
	// Skips events if system very busy.
	while ((unNewTime >= *punTriggerTime) && unPeriod)
	{
		// Use old value
		*punTriggerTime = *punTriggerTime + unPeriod;
		return true;
	}
	return false;
}

/* Fuction to wait any delay based on CheckUpdateTime */

void delay_ms(uint32_t value)
{
	uint32_t unDelayTime = 0;
	CheckUpdateTime(value, &unDelayTime);
	while (!CheckUpdateTime(value, &unDelayTime));
}

/** **********************************************************************
 *
 * Checks that given band has positive values
 * @param pBand		band to check
 * @return true if band has posivitive values range otherwise negative.
 *
 *********************************************************************** */
bool IsPositiveBand(PCBAND pBand)
{
	return pBand->max >= 0 &&  pBand->min >= 0;
}

/** **********************************************************************
 *
 * Checks that given band index dedicated to work with external amplifier
 * @param eMode		operation mode
 * @param nBandIdx	band index to check
 * @return true if band index dedicated to work with external amplifier otherwise false
 *
 *********************************************************************** */
bool IsAmpBand(MODE eMode, uint8_t nBandIdx)
{
	bool bRet = false;

	if (eMode == MODE_ACI)
	{
		bRet = nBandIdx == EXTBAND_ACI_10A_AMP || nBandIdx == EXTBAND_ACI_30A_AMP;
	}
	else if (eMode == MODE_DCI)
	{
		bRet = nBandIdx == EXTBAND_DCI_P_10A_AMP
			|| nBandIdx == EXTBAND_DCI_P_30A_AMP
			|| nBandIdx == EXTBAND_DCI_N_10A_AMP
			|| nBandIdx == EXTBAND_DCI_N_30A_AMP;
	}

	return bRet;
}

bool IsGreater(double d1, double d2, int8_t nFraction)
{
	double dEpsilon = Pow10Fast(-(nFraction + 1)); // ST TODO: Create array
	double dAbsDelta = d1 - d2;
	bool bRet = dAbsDelta > dEpsilon;
	return bRet;
}

bool IsEqual(double d1, double d2, int8_t nFraction)
{
	double dEpsilon = pow((double)0.1, nFraction);
	double dAbsDelta = fabs(d1 - d2);
	return dAbsDelta < dEpsilon;
}

void vector_init(Vector *vector)
{
	// initialize size and capacity
	vector->size = 0;
	vector->capacity = VECTOR_INITIAL_CAPACITY;

	// allocate memory for vector->data
	vector->data = (char*)malloc(sizeof(char) * vector->capacity);
}

void vector_append(Vector *vector, char value)
{
	// make sure there's room to expand into
	vector_double_capacity_if_full(vector);

	if (vector->data)
	{
		// append the value and increment vector->size
		vector->data[vector->size++] = value;
	}
	else
	{
		//SetLastError("vector_get() - No buffer");
	}
}

char vector_get(Vector *vector, int index)
{
	if (vector->data)
	{
		if (index < vector->size || index >= 0)
		{
			return vector->data[index];
		}
		else
		{
			//SetLastError("vector_get() - Index out of bounds");
		}
	}
	else
	{
		//SetLastError("vector_get() - No buffer");
	}

	return 0;
}

void vector_double_capacity_if_full(Vector *vector)
{
	if (0 == vector->capacity)
	{
		vector_init(vector);
	}
	else if (vector->size >= vector->capacity)
	{
		// double vector->capacity and resize the allocated memory accordingly
		vector->capacity *= 2;
		vector->data = (char*)realloc(vector->data, sizeof(char) * vector->capacity);
		if (!vector->data)
		{
			// SetLastError("vector_get() - Out of memory");
		}
	}
}

void vector_free(Vector *vector)
{
	free(vector->data);
	vector->capacity = 0;
	vector->data = 0;
	vector_clear(vector);
}

void vector_clear(Vector *vector)
{
	vector->size = 0;
}

void __error__(char *pcFilename, uint32_t ui32Line)
{
	AsserReporter("false", pcFilename, ui32Line);
}

void AsserReporter(char *pszExpr, char *pcFilename, uint32_t ui32Line)
{
	char pszBuff[128];
	sprintf(pszBuff, "ASSERT\n%s\n%s(%d)\n", pszExpr, pcFilename, ui32Line);
	ReportError(pszBuff);
}

char *AARM_errorStr(AERROR code)
{
	switch (code)
	{
	case AERROR_NO_ERROR:
		return "No error";
	case AERROR_UNKNOWN:
	default:
		return "Unknown error";
	case AERROR_INCOMPLETE_TEXAS_CMD:
		return "Incomplete Texas command";
	case AERROR_UNKNOWN_TEXAS_CMD:
		return "Unknown Texas command";
	case AERROR_UNKNOWN_LLCMD:
		return "Unknown low-level command";
	case AERROR_INVALID_LLCMD:
		return "Invalid low-level command";
	case AERROR_OVERLIMIT_PWM:
		return "PWM voltage exceeded";
	case AERROR_FREQ_OUT_OF_RANGE:
		return "Frequency out-of-range";
	case AERROR_INVALID_RESPONSE_LENGH:
		return "Invalid response length";
	}
}
