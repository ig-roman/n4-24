/*
 * File:    circ_buf.h
 * Title:
 * Created: Tue Sep 05 10:13:45 2006
 *
 */

#ifndef CIRC_BUF_H
#define CIRC_BUF_H

#include "TopCommon.h"

typedef struct CIRC_BUF
{
    int mask;
    int writeIndex;
    int readIndex;
    char *mem;
} CIRC_BUF;

#define CIRC_BUF_DEF {0, 0, 0, 0}

void CIRC_BUF_init(CIRC_BUF *buf, char *mem, int size);
void CIRC_BUF_put(CIRC_BUF *buf, const char *mem, int count);
int CIRC_BUF_get(CIRC_BUF *buf, char *mem, int count, bool bMoveReadIdx);
bool CIRC_BUF_ISEMPTY(CIRC_BUF *buf);

#endif /* #ifndef CIRC_BUF_H */
