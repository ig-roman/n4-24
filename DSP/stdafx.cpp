
// stdafx.cpp : source file that includes just the standard includes
// h4_x.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"


void ToBinStr(char* pszBuff, unsigned char data)
{
	pszBuff[0] = 'b';
	for (int n = 0; n < 8; n++)
	{
		unsigned char unMask = 1 << (7 - n);
		pszBuff[n + 1] = (unMask & data) > 0 ? '1': '0';
	}
	pszBuff[9] = NULL; 
}

extern HANDLE handlePortArm = NULL;  