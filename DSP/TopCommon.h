#ifndef TCOMMONC_H_
#define TCOMMONC_H_

#define STRINGIZE2(s) #s
#define TOSTR(s) STRINGIZE2(s)


#define VERSION_MAJOR		2
#define VERSION_MINOR		1
#define VERSION_REVISION	4
// This line break intentionally for git otherwise it always conflicts with previous line. Probably he decides that changes are related to each other
#define VERSION_BUILD		9

// Versions example
// 1.2.0.1 instead of 1.2-a1 alpha version
// 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
// 1.2.2.3 instead of 1.2-rc3 (release candidate)
// 1.2.3.0 instead of 1.2-r (commercial distribution)
// 1.2.3.5 instead of 1.2-r5 (commercial distribution with many bug fixes)
//
// VERSION-REVISION	4 is reserved for the current hardware revision
// VERSION-REVISION	2 is reserved for special build with DCV output ampliefer

#define ID_ORG			"TEHNOYAKS"
#define ID_PRODUCT		"N4-24"
#define ID_SERIAL		"0"
#define VER_VERSION			VERSION_MAJOR, VERSION_MINOR, VERSION_REVISION, VERSION_BUILD
#define VER_VERSION_STR		TOSTR(VERSION_MAJOR)        \
							"." TOSTR(VERSION_MINOR)    \
							"." TOSTR(VERSION_REVISION) \
							"." TOSTR(VERSION_BUILD)    \

#if defined(_TMS320C28X)
	#include "CCS/HAL/tistdtypes.h"
#else
	typedef unsigned int        Uint32;
	typedef unsigned short      Uint16;
	typedef unsigned char       Uint8;

	typedef signed int          Int32;
	typedef signed short        Int16;
	typedef signed char         Int8;
#endif

#if defined(PART_TM4C123GH6PM) || defined(PART_TM4C123FH6PM)
	#include <stdbool.h>
	#include <stdint.h>
#else
	typedef Int8	int8_t;
	typedef Int16	int16_t;
	typedef Int32	int32_t;

	typedef Uint8	uint8_t;
	typedef Uint16	uint16_t;
	typedef Uint32	uint32_t;

#ifndef __cplusplus
	#define true 1
	#define false 0
	#define bool char
#endif

#endif

#ifndef NULL
  #define NULL  0
#endif /* NULL */

#define DEBUG
// For VS compiler
#define _CRT_SECURE_NO_WARNINGS

#define PWM_MAX_VAL 60069UL
#define PWM_U_REF_VOL 11.485
#define PWM_R1 2047.0
#define PWM_R2 1.0
#define PWM_RS (PWM_R1 + PWM_R2)
#define PWM_CLOCK_FREQ 100

#define SPI_CLOCK_FREQ 100000 // Harry said that frequency should be 100khz

#define AARM_TIMEOUT_ACK 500
#define AARM_PING_TIME 4000

// Low level program comands
#define LLPGR_CS_MASK			0x0F
#define LLPGR_CMD_MASK			0xE0

#define LLPGR_CS_AND_ADDR_CMD	0
#define LLPGR_PWM_CMD			0x40
#define LLPGR_DELAY_CMD			0x80
#define LLPROG_END_CMD			0xC0

// Maximum buffer size for low level programms
#define LLPROG_BUFFER_SIZE		1024

#define PORT_SWITCH_TIME_OUT_MS	5000
#define PORT_MAX_MESSAGE_LENGTH	63

// Max incoming remote command
#define INST_INPUT_BUFFER_MAX_SIZE 256
#define INST_OUTPUT_BUFFER_MAX_SIZE 64

#define ACK						0x06
#define NACK					0x15

typedef enum { MODE_DCU, MODE_ACU, MODE_DCI, MODE_ACI, MODE_R, MODE_UNKNOWN } MODE;
typedef enum { PORTNAME_TEXAS, PORTNAME_DISP, PORTNAME_AARM, PORTNAME_REAR_COM, PORTNAME_XPORT, PORTNAME_GPIB, PORTNAME_USB, PORTNAME_END} PORTNAME;
typedef enum { STATEMODE_FULL_STANDBY, STATEMODE_U, STATEMODE_I, STATEMODE_R, STATEMODE_UNKNOWN } STATEMODE;
typedef enum { STATESENSE_TWO_WIRE, STATESENSE_FOUR_WIRE, STATESENSE_UNKNOWN } STATESENSE;
typedef enum { STATEOUT_OFF, STATEOUT_ON, STATEOUT_UNKNOWN } STATEOUT;
typedef enum { CTRLMODE_MIXED, CTRLMODE_LOCAL, CTRLMODE_REMOTE } CTRLMODE;

// This structure defines how to band looks like in this device
// Examples (X is decimal number):
// 		EXBAND_DCU_P_1000_V:	Scale 7, fraction 3 then number is going to be XXXX.XXX
// 		EXBAND_DCU_P_10_MV:		Scale 7, fraction 8 then number is going to be 0.0XXXXXXX or XX.XXXXXmV
// 		EXBAND_ACU_1MV:			Scale 4, fraction 6	then number is going to be 0.00XXXX or X.XXXmV
typedef struct tagBAND
{
	uint8_t	bandIdx;	// Band index is unique within operation mode (ACV, DCV or R) and starts from 0.
	uint8_t	scale;		// Defines how precise number is or how many decimal numbers exist (decimal + fractional) for band
	int8_t	fraction;	// Defines how many zeros go after comma (radix point). Probably better name could be fractional part length. https://en.wikipedia.org/wiki/Fractional_part
	double	min;		// Minimal allowed value for band (inclusive)
	double	max;		// Maximal allowed value for band (inclusive)
} BAND, *PBAND;

typedef const BAND* PCBAND;

typedef struct tagWORKUNIT
{
	PCBAND		pBand;
	double		value;
} WORKUNIT, *PWORKUNIT;

typedef const WORKUNIT* PCWORKUNIT;

#define AARM_STATUS_RESPONSE_LENGTH 9
typedef enum
{
	STRCMD_START		= 'S',
	STRCMD_END			= 'E',

	// Commands from Texas to AARM
	STRCMD_CUSTOM		= 'c',

	STRCMD_MODE			= 'm',
	STRCMD_VALUE		= 'v',
	STRCMD_FREQUENCY	= 'f',
	STRCMD_BAND			= 'b',
	STRCMD_FREQ_BAND	= 'i',
	STRCMD_SENSE		= 's',
	STRCMD_OUT			= 'o',
	STRCMD_CORR			= 'e',
	
	// Commands from AARM to Texas
	STRCMD_STATUS		= 'l',
	STRCMD_ERROR		= 'r',
	STRCMD_OVERLOAD		= 'a',
	STRCMD_VER_CRC		= 'k'
} STRCMD;


typedef enum
{
	AERROR_NO_ERROR,
	AERROR_UNKNOWN,
	AERROR_INCOMPLETE_TEXAS_CMD,
	AERROR_UNKNOWN_TEXAS_CMD,
	AERROR_UNKNOWN_LLCMD,
	AERROR_INVALID_LLCMD,
	AERROR_OVERLIMIT_PWM,
	AERROR_FREQ_OUT_OF_RANGE,
	AERROR_INVALID_RESPONSE_LENGH
} AERROR;

//#define USE_DCU_10_MV

typedef enum
{
#ifdef USE_DCU_10_MV
	EXBAND_DCU_P_10_MV,
#endif
	EXBAND_DCU_P_100_MV,
	EXBAND_DCU_P_1_V,
	EXBAND_DCU_P_10_V,
	EXBAND_DCU_P_100_V,
	EXBAND_DCU_P_1000_V,
	EXBAND_DCU_P_10_MV_E,
	EXBAND_DCU_P_100_MV_E,

#ifdef USE_DCU_10_MV
	EXBAND_DCU_N_10_MV,
#endif
	EXBAND_DCU_N_100_MV,
	EXBAND_DCU_N_1_V,
	EXBAND_DCU_N_10_V,
	EXBAND_DCU_N_100_V,
	EXBAND_DCU_N_1000_V,
	EXBAND_DCU_N_10_MV_E,
	EXBAND_DCU_N_100_MV_E,
	EXBAND_DCU_MAX_COUNT
}
EXBAND_DCU; // ST TODO: FIX ENUM NAME

#ifdef USE_DCU_10_MV
#define EXBAND_DCU_N_LOWEST EXBAND_DCU_N_10_MV
#else
#define EXBAND_DCU_N_LOWEST EXBAND_DCU_N_100_MV
#endif

//#define USE_ACU_1MV

typedef enum
{
#ifdef USE_ACU_1MV
	EXBAND_ACU_1MV = 0,
	EXBAND_ACU_10MV,
#else
	EXBAND_ACU_10MV = 0,
#endif
	EXBAND_ACU_100MV,
	EXBAND_ACU_1V,
	EXBAND_ACU_10V,
	EXBAND_ACU_100V,
	EXBAND_ACU_1000V,
	EXBAND_ACU_MAX_COUNT
}
EXBAND_ACU; // ST TODO: FIX ENUM NAME

#define R_VAL_0R		MIN_IMP_VALUE
#define R_VAL_1R		1.000000
#define R_VAL_10R		10.00000
#define R_VAL_100R		100.0000
#define R_VAL_1K		1000.000
#define R_VAL_10K		10000.00
#define R_VAL_100K		100000.0
#define R_VAL_1M		1000000.0
#define R_VAL_10M		10000000.0
#define R_VAL_100M		MAX_IMP_VALUE

typedef enum
{
	EXTBAND_R_0R,
	EXTBAND_R_1R,
	EXTBAND_R_10R,
	EXTBAND_R_100R,
	EXTBAND_R_1K,
	EXTBAND_R_10K,
	EXTBAND_R_100K,
	EXTBAND_R_1M,
	EXTBAND_R_10M,
	EXTBAND_R_100M,
	EXTBAND_R_MAX_COUNT
} EXBAND_R;

typedef enum
{
	EXTBAND_ACU_FREQ_100,
	EXTBAND_ACU_FREQ_1K,
	EXTBAND_ACU_FREQ_10K,
	EXTBAND_ACU_FREQ_100K,
	EXTBAND_ACU_FREQ_1M
} EXTBAND_ACU_FREQ;

typedef enum
{
	EXTBAND_ACI_100UA,
	EXTBAND_ACI_1MA,
	EXTBAND_ACI_10MA,
	EXTBAND_ACI_100MA,
	EXTBAND_ACI_1A,
	EXTBAND_ACI_2A,
	EXTBAND_ACI_10A_AMP,
	EXTBAND_ACI_30A_AMP,
	EXTBAND_ACI_MAX_COUNT
} EXTBAND_ACI;

typedef enum
{
	EXTBAND_DCI_P_10UA,
	EXTBAND_DCI_P_100UA,
	EXTBAND_DCI_P_1MA,
	EXTBAND_DCI_P_10MA,
	EXTBAND_DCI_P_100MA,
	EXTBAND_DCI_P_1A,
	EXTBAND_DCI_P_2A,
	EXTBAND_DCI_P_10A_AMP,
	EXTBAND_DCI_P_30A_AMP,

	EXTBAND_DCI_N_10UA,
	EXTBAND_DCI_N_100UA,
	EXTBAND_DCI_N_1MA,
	EXTBAND_DCI_N_10MA,
	EXTBAND_DCI_N_100MA,
	EXTBAND_DCI_N_1A,
	EXTBAND_DCI_N_2A,
	EXTBAND_DCI_N_10A_AMP,
	EXTBAND_DCI_N_30A_AMP,
	EXTBAND_DCI_MAX_COUNT
} EXTBAND_DCI;

typedef enum
{
	EXTBAND_ACI_FREQ_100,
	EXTBAND_ACI_FREQ_1K,	
	EXTBAND_ACI_FREQ_10K5
} EXTBAND_ACI_FREQ;

// ST TODO: MOVE TO SEPARATE FILE!!!
typedef struct tagFREQ_VOL_RELATION
{
	double		voltage;
	double		freqMin;
	double		freqMax;

} FREQ_VOL_RELATION, *PFREQ_VOL_RELATION;

typedef const FREQ_VOL_RELATION* PCFREQ_VOL_RELATION;

typedef struct
{
	uint8_t	bandIdx;
	double	multiplier;
} BANDTOPWM;

extern const BANDTOPWM s_arrACIBandsToPWM[];
extern const BANDTOPWM s_arrDCIBandsToPWM[];
extern const BANDTOPWM s_arrDCUBandsToPWM[];
extern const BANDTOPWM s_arrACUBandsToPWM[];

double BandToPwmMultiplier(MODE eMode, uint8_t bandIdx);

#define MAX_FREQUENCY_VALUE_1000V_BAND 100000.0

#define MAX_FREQUENCY_VALUE		1050000.0
#define MIN_FREQUENCY_VALUE		(-MAX_FREQUENCY_VALUE)

#define MAX_VOLTAGE_AC_VALUE	1050.0
#ifdef USE_ACU_1MV
#define MIN_VOLTAGE_AC_VALUE	0.00009
#else
#define MIN_VOLTAGE_AC_VALUE	9.0e-4
#endif

#define MAX_VOLTAGE_DC_VALUE	1050.0
#define MIN_VOLTAGE_DC_VALUE	(-MAX_VOLTAGE_DC_VALUE)

#define MAX_CURENT_VALUE		21.0
#define MIN_CURENT_VALUE		(-MAX_CURENT_VALUE)

#define MAX_CURENT_AC_VALUE		MAX_CURENT_VALUE
#define MIN_CURENT_AC_VALUE		0.000009

#define MAX_IMP_VALUE			100000000.0
#define MIN_IMP_VALUE			0.0

extern const BAND sm_arrBandsACI[];
extern const BAND sm_arrBandsDCI[];
extern const BAND sm_arrBandsACU[];
extern const BAND sm_arrBandsDCU[];
extern const BAND sm_arrBandsRs[];
extern const FREQ_VOL_RELATION sm_arrFreqCurRelation[];
extern const FREQ_VOL_RELATION sm_arrFreqVolRelation1000VBandOnly[];

bool IsPositiveBand(PCBAND pBand);
bool IsAmpBand(MODE eMode, uint8_t nBandIdx);

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))
#define ABS(X) (((X) > 0) ? (X) : -(X))

// ST TODO: MOVE TO SEPARATE FILE!!!
/*
Binary constant generator macro
By Tom Torfs - donated to the public domain

All macro's evaluate to compile-time constants
*/

/* turn a numeric literal into a hex constant
(avoids problems with leading zeroes)
8-bit constants max value 0x11111111, always fits in unsigned long
*/
#define HEX__(n) 0x##n##LU

/* 8-bit conversion function,  MSB first */
#define B8MSF__(x) \
 ((x&0x0000000FLU)?1:0) \
 +((x&0x000000F0LU)?2:0) \
 +((x&0x00000F00LU)?4:0) \
 +((x&0x0000F000LU)?8:0) \
 +((x&0x000F0000LU)?16:0) \
 +((x&0x00F00000LU)?32:0) \
 +((x&0x0F000000LU)?64:0) \
 +((x&0xF0000000LU)?128:0)

/* for upto 8-bit binary constants */
#define B8MSF(d) ((unsigned char)B8MSF__(HEX__(d)))

/* 8-bit conversion function, MSB last */
#define B8MSL__(x) \
 ((x&0xF0000000LU)?1:0) \
+((x&0x0F000000LU)?2:0) \
+((x&0x00F00000LU)?4:0) \
+((x&0x000F0000LU)?8:0) \
+((x&0x0000F000LU)?16:0) \
+((x&0x00000F00LU)?32:0) \
+((x&0x000000F0LU)?64:0) \
+((x&0x0000000FLU)?128:0)

/* *** user macros *** */

/* for upto 8-bit binary constants */
#define B8(d) ((unsigned char)B8MSL__(HEX__(d)))

/* for upto 16-bit binary constants, MSB last */
#define B16(dlsb, dmsb) (((unsigned short)B8(dmsb)<< 8) \
+ B8(dlsb))

/* for upto 32-bit binary constants, MSB last */
#define B32(dlsb, db3, db2, dmsb) (((unsigned long)B8(dmsb)<<24) \
+ ((unsigned long)B8(db2)<<16) \
+ ((unsigned long)B8(db3)<< 8)\
+ B8(dlsb))

/* Sample usage:
B8(01010101) = 85
B16(10101010,01010101) = 43605
B32(10000000,11111111,10101010,01010101) = 2164238933
*/


double roundX(double x, int8_t nFraction);
double Pow10Fast(int8_t nFraction);

#ifdef __cplusplus
extern "C" {
#endif

extern const char * STR_ORG_NAME;
extern const char * STR_PRODUCT_NAME;
extern const char * STR_SERIAL_NR;
extern const char * STR_SW_VERSION;
extern const char * STR_ERROR;

// Should be externalized to C because used from AARM interface
extern const BAND sm_arrBandsACUFreq[];
extern const BAND sm_arrBandsACIFreq[];

uint32_t GetTime();
uint8_t countCrc8(const uint8_t *pData, uint16_t unLen, uint8_t unPolynomial);
bool CheckUpdateTime(uint32_t unPeriod, uint32_t* punTriggerTime);

bool IsGreater(double d1, double d2, int8_t nFraction);
bool IsEqual(double d1, double d2, int8_t nFraction);

#define VECTOR_INITIAL_CAPACITY 50

// Define a vector type
typedef struct {
  int size;      // slots used so far
  int capacity;  // total available slots
  char* data;     // array of integers we're storing
} Vector;

void vector_append(Vector *vector, char value);
void vector_clear(Vector *vector);
char vector_get(Vector *vector, char index);
void vector_double_capacity_if_full(Vector *vector);
void vector_free(Vector *vector);

extern void ReportError(const char *err);

extern void __error__(char *pcFilename, uint32_t ui32Line);
extern void AsserReporter(char *pszExpr, char *pcFilename, uint32_t ui32Line);

#ifdef ASSERT
#undef ASSERT
#endif

#define ASSERT(expr) do                                                       \
                     {                                                        \
                         if(!(expr))                                          \
                         {                                                    \
                        	 AsserReporter(#expr,__FILE__, __LINE__);                   \
                         }                                                    \
                     }                                                        \
                     while(0)


#ifdef __cplusplus
}
#endif


#endif /*TCOMMONC_H_*/
