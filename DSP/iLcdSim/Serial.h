#pragma once

typedef UINT(*FN_HAS_DATA)(char * pData, int& nLength);

class CSerial
{
public:
	CSerial(void);
	virtual ~CSerial(void);
	void DisConnect();
	void Connect();
	void GetData(char ** ppData, int& nLength);
	void SendData(const char* pBuf, int length);

	HANDLE handlePortArm;
	FN_HAS_DATA m_fnHasData;
	DWORD dwReadLen;

private:
	void ReceiveChars();
	void ProcessErrorMessage(char*);

private:
	OVERLAPPED s_ovRead;
	OVERLAPPED s_ovCom;
	char comBuff[1000];
	HANDLE m_hThread;
	CRITICAL_SECTION m_cs;
};

