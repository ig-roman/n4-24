
// iLcdSimDlg.cpp : implementation file
//

#include "stdafx.h"
#include "iLcdSim.h"
#include "iLcdSimDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CiLcdSimDlg* s_pThis = NULL;
UINT RXData(char *pData, int& nLength)
{
	ATLTRACE("%d - ", nLength);
	for (int n = 0; n < nLength; n++)
	{
		ATLTRACE("%c", pData[n]);
	}
	
	if (nLength == 9)
	{
		int n = 0;
	}
	int nParsed = 0;
	EnterCriticalSection(&s_pThis->m_cs);
	nParsed = s_pThis->disp.HandleDisplayData(pData, nLength);
	LeaveCriticalSection(&s_pThis->m_cs);

	ATLTRACE("(parsed-%d) \n", nParsed);

	return nParsed;
}

// CiLcdSimDlg dialog

CiLcdSimDlg::CiLcdSimDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CiLcdSimDlg::IDD, pParent),
		m_bIsInvalid(false),
		m_pCDC(NULL)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	InitializeCriticalSection(&m_cs);
	disp.m_pParent = this;
	s_pThis = this;
	serial.m_fnHasData = RXData;
}

CiLcdSimDlg::~CiLcdSimDlg()
{
	serial.DisConnect();
	DeleteCriticalSection(&m_cs);
}

CDC* CiLcdSimDlg::GetDc()
{
	return m_pCDC;
}

void CiLcdSimDlg::SetInvalid()
{
	m_bIsInvalid = true;
}

void CiLcdSimDlg::SendDispRetData(const char * buff, int length)
{
	ATLTRACE(" (ACK-%d)(RetLen-%d)", buff[0] == 0x06, length);
	serial.SendData(buff, length);
}

void CiLcdSimDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CiLcdSimDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_SYSKEYDOWN()
	ON_WM_SYSKEYUP()
END_MESSAGE_MAP()

// CiLcdSimDlg message handlers

BOOL CiLcdSimDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	serial.Connect();

	::SetTimer(GetSafeHwnd(), 1, 200, NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CiLcdSimDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CRect rect;
	GetClientRect(&rect);

	if (IsIconic())
	{
		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
	
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		static bool bFirstTime = true;
		if (bFirstTime)
		{
			bFirstTime = false;
			CDC* CDC = GetDC();
			HDC hDC= CDC->GetSafeHdc();
			HDC m_hMemDC = CreateCompatibleDC(hDC);
			m_pCDC = CDC::FromHandle(m_hMemDC);

			m_hMemBmp = CreateCompatibleBitmap(hDC, rect.Width(), rect.Height());
			ReleaseDC(CDC);

			HBITMAP hOldBmp = (HBITMAP)SelectObject(m_hMemDC, m_hMemBmp);
			DeleteObject(hOldBmp);

			CBrush br(0xFFFFFF);
			m_pCDC->FillRect(rect, &br);
		}

		CRect rect;
		GetClientRect(&rect);
		dc.BitBlt(rect.left, rect.top, 870, rect.Height(), m_pCDC, 0, 0, SRCCOPY);
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CiLcdSimDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CiLcdSimDlg::OnTimer(UINT_PTR nIDEvent)
{
	EnterCriticalSection(&m_cs);
	if (m_bIsInvalid)
	{
		m_bIsInvalid = false;

		Invalidate();
		UpdateWindow();
	}
	LeaveCriticalSection(&m_cs);

	__super::OnTimer(nIDEvent);
}


void CiLcdSimDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	disp.HandleTouchEvent(point, true);
	__super::OnLButtonDown(nFlags, point);
}


void CiLcdSimDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	disp.HandleTouchEvent(point, false);
	__super::OnLButtonUp(nFlags, point);
}

UINT s_chPressedBefore = 0;
void CiLcdSimDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	ButtonChanged(nChar, true);
	s_chPressedBefore = nChar;

	__super::OnKeyDown(nChar, nRepCnt, nFlags);
}


void CiLcdSimDlg::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (!s_chPressedBefore)
	{
		ButtonChanged(nChar, true);
	}
	s_chPressedBefore = 0;
	ButtonChanged(nChar, false);
	__super::OnKeyUp(nChar, nRepCnt, nFlags);
}


void CiLcdSimDlg::OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	__super::OnSysKeyDown(nChar, nRepCnt, nFlags);
}


void CiLcdSimDlg::OnSysKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default
	ButtonChanged(nChar, true);
	ButtonChanged(nChar, false);
	__super::OnSysKeyUp(nChar, nRepCnt, nFlags);
}

void CiLcdSimDlg::ButtonChanged(char nChar, bool bPressed)
{
	char nBuffDataLen[3];

	// 4B, FF, 0016, 0007, 06 - Push event (K key_char ev_coord_x ev_coord_y [ACK])
	// 6B, FF, 0016, 0007, 06 - Release event (k key_char ev_coord_x ev_coord_y [ACK])
	nBuffDataLen[0] = bPressed ? (char)0x4B : (char)0x6B;
	nBuffDataLen[1] = nChar;
	nBuffDataLen[2] = 0x06;

	serial.SendData(nBuffDataLen, sizeof(nBuffDataLen));
}