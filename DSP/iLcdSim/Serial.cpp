#include "stdafx.h"
#include "Serial.h"

DWORD WINAPI ThreadFun(LPVOID lpParam) 
{
	CSerial* pThis = (CSerial*)lpParam;
	for (;;)
	{
		if (pThis->handlePortArm)
		{
			char * pData;
			int nLength = 0;
			pThis->GetData(&pData, nLength);
			if (nLength > 0)
			{
				UINT unParsed = pThis->m_fnHasData(pData, nLength);
				if (unParsed)
				{
					::memmove(pData, pData + unParsed, nLength - unParsed);
					pThis->dwReadLen -= unParsed;
				}
				else if (nLength > 500)
				{
					// Some invalid data in buffer
					pThis->dwReadLen = 0;
				}
			}
		}
		else
		{
			// Waitng for connect action
			Sleep(10);
		}
	}
}

CSerial::CSerial(void) :
	handlePortArm(NULL),
	dwReadLen(0)
{
	ZeroMemory(&s_ovRead, sizeof(s_ovRead));
	ZeroMemory(&s_ovCom, sizeof(s_ovCom));

	s_ovRead.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	ResetEvent(s_ovRead.hEvent);

	s_ovCom.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	ResetEvent(s_ovCom.hEvent);

	InitializeCriticalSection(&m_cs);

	DWORD dwID = 0;
	m_hThread = CreateThread(NULL, 0, ThreadFun, this, 0, &dwID);
}

CSerial::~CSerial(void)
{
	DisConnect();
}

void CSerial::ProcessErrorMessage(char* msg)
{
	::MessageBoxA(NULL, msg, "Error", 0);
}

void CSerial::GetData(char ** ppData, int& nLength)
{
	if (handlePortArm)
	{
		DWORD dwEvtMask = EV_RXCHAR | EV_ERR;

		WaitCommEvent(handlePortArm, &dwEvtMask, &s_ovCom);

		DWORD dwRet = WaitForSingleObject(s_ovCom.hEvent, INFINITE);
		if (dwRet != WAIT_TIMEOUT)
		{
			if (dwEvtMask & EV_ERR)
			{
				DWORD dwErrors = 0;
				::ClearCommError(handlePortArm, &dwErrors, NULL);
			}
			else if (dwEvtMask & EV_RXCHAR)
			{
				EnterCriticalSection(&m_cs);
				ReceiveChars();
				LeaveCriticalSection(&m_cs);
			}
		}
	}


	//{
		nLength = dwReadLen;
		*ppData = comBuff;
		//dwReadLen = 0;
	//}
}

void CSerial::ReceiveChars()
{
	COMSTAT	cs = {0};
	DWORD	dwErrors = 0;

	
	// Find out, how many chars do we have there
	::ClearCommError(handlePortArm, &dwErrors, &cs);
	
	


	CHAR bufRecv[128];
	ZeroMemory(bufRecv, sizeof(bufRecv));

	DWORD dwLeft = cs.cbInQue;
	while (dwLeft)
	{
		DWORD dwToRead = __min(128, dwLeft);
		DWORD dwBytesRead = 0;
		if (!::ReadFile(handlePortArm, bufRecv, dwToRead, &dwBytesRead, &s_ovRead))
		{
			DWORD dwErr = ::GetLastError();
			if (dwErr == ERROR_IO_PENDING)
			{
				if (!::GetOverlappedResult(handlePortArm, &s_ovRead, &dwBytesRead, TRUE))
				{
					ProcessErrorMessage("GetOverlappedResults() in ReadFile()");
				}
			}
			else
			{
				// Error
				ProcessErrorMessage("ReadFile()");
				dwLeft = 0;
			}
		}

		if (dwBytesRead)
		{
			for (int n = 0; n < dwBytesRead; n++)
			{
				comBuff[dwReadLen++] = bufRecv[n];
			}
			dwLeft -= dwBytesRead;
		}
	}


}

void CSerial::DisConnect()
{
	if (handlePortArm)
	{
		CloseHandle(handlePortArm);
		handlePortArm = NULL;
	}
}

void CSerial::Connect()
{
	DCB config;
	handlePortArm = CreateFile(_T("\\\\.\\COM12"),  // Specify port device: default "COM1"
	GENERIC_READ | GENERIC_WRITE,       // Specify mode that open device.
	0,                                  // the devide isn't shared.
	NULL,                               // the object gets a default security.
	OPEN_EXISTING,                      // Specify which action to take on file. 
	FILE_FLAG_OVERLAPPED,
	NULL);                              // default.

	if (GetCommState(handlePortArm, &config) != 0)
	{
		config.BaudRate = 115200;

		if (SetCommState(handlePortArm, &config) == 0)
		{
			CloseHandle(handlePortArm);
			handlePortArm = NULL;
			ProcessErrorMessage("Error: SetCommState() returns false");
		}
		else
		{
			if (!SetCommMask(handlePortArm, EV_RXCHAR | EV_ERR))
			{
				// Handle the error. 
				ProcessErrorMessage("SetCommMask failed with error.\n");
				return;
			}
		}

	}
	else
	{
		CloseHandle(handlePortArm);
		handlePortArm = NULL;
		ProcessErrorMessage("Error: GetCommState() returns false");
	}
}

void CSerial::SendData(const char* pBuf, int length)
{
	// Add package CRC
	const char* pNewBuff = pBuf;
	// For physical USB or serial port
	if (handlePortArm)
	{
		DWORD dwWritelen = (DWORD)length;
		char bRet = 0;
		OVERLAPPED osWrite = {0};
		osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
		if (WriteFile(handlePortArm,   // handle to file to write to
			pNewBuff,              // pointer to data to write to file
			length,              // number of bytes to write
			&dwWritelen, &osWrite) == 0)      // pointer to number of bytes written
		{
			 if (GetLastError() == ERROR_IO_PENDING)
			 { 
				DWORD dwRes = WaitForSingleObject(osWrite.hEvent, INFINITE);
				if (WAIT_OBJECT_0 == dwRes)
				{
					bRet = GetOverlappedResult(handlePortArm, &osWrite, &dwWritelen, FALSE);
				}
			}
		}

		if (!bRet)
		{
			CloseHandle(handlePortArm);
			handlePortArm = NULL;
			ProcessErrorMessage("Error: WriteFile() returns false");
		}
		CloseHandle(osWrite.hEvent);
	}
}
