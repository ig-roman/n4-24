
// iLcdSimDlg.h : header file
//

#pragma once

#include "../CiLcd.h"
#include "Serial.h"

// CiLcdSimDlg dialog
class CiLcdSimDlg : public CDialog, public IiLcdParent
{
// Construction
public:
	CiLcdSimDlg(CWnd* pParent = NULL);
	virtual ~CiLcdSimDlg();

// Dialog Data
	enum { IDD = IDD_ILCDSIM_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	 CDC* GetDc();
	virtual void SetInvalid();
	virtual void SendDispRetData(const char * buff, int length);

// Implementation
public:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	CDC*	m_pCDC;
	CRITICAL_SECTION m_cs;
	CiLcd	disp;
	HBITMAP m_hMemBmp;
	bool m_bIsInvalid;
	CSerial serial;

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSysKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);

private:
	void ButtonChanged(char nChar, bool bPressed);
};
