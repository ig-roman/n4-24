#include "CCS/LogicInterface.h"
#include "CCS/UIInterface.h"
#include "AARM/ArmInterface.h"
#include <windows.h>
#include <string.h>
#include <malloc.h>
#include "CommonCUI.h"
#include "Header.h"

char g_HVBuzzerSuppress = 0;

void DisplayError(LPTSTR lpszFunction)
{ 
	LPVOID lpMsgBuf, lpDisplayBuf;
	DWORD dw = GetLastError(); 

	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, dw, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL );

	lpDisplayBuf = (LPVOID)LocalAlloc( LMEM_ZEROINIT, 
							( lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) // account for format string
							* sizeof(TCHAR) );

	wsprintf((LPTSTR)lpDisplayBuf, TEXT("%s\nFailed with error code %d as follows:\n%s"), lpszFunction, dw, lpMsgBuf);

	MessageBox(NULL, (LPCTSTR)lpDisplayBuf, NULL, 0);

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
}

__declspec(dllexport) void __stdcall CLogic_StoreBytes(uint32_t unStartAddress, uint32_t unLength, const uint8_t* byData)
{
	HANDLE hEepromFile = CreateFile(L"eeprom.bin", 
		GENERIC_WRITE,						// Specify mode that open device.
		FILE_SHARE_READ,					// the devide mode for others
		NULL,								// the object gets a default security.
		OPEN_ALWAYS,						// Specify which action to take on file. 
		FILE_FLAG_OVERLAPPED,
		NULL);

	if (INVALID_HANDLE_VALUE == hEepromFile)
	{
		DisplayError(TEXT("Error: CLogic_StoreBytes() can't create file \"eeprom.bin\""));
	}
	else
	{
		DWORD dwcount = 0;
		OVERLAPPED ovelapf;
		memset(&ovelapf, 0, sizeof(ovelapf));
		ovelapf.Offset = unStartAddress;
		ovelapf.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

		if (!WriteFile(hEepromFile, byData, unLength, &dwcount, &ovelapf))
		{
			if (GetLastError() == ERROR_IO_PENDING)
			{ 
				DWORD dwRes = WaitForSingleObject(ovelapf.hEvent, INFINITE);
				if (WAIT_OBJECT_0 == dwRes)
				{
					if (!GetOverlappedResult(hEepromFile, &ovelapf, &dwcount, FALSE))
					{
						DisplayError(TEXT("Error: CLogic_StoreBytes() can't write data file \"eeprom.bin\""));
					}
				}
			}
			else
			{
				DisplayError(TEXT("Error: CLogic_StoreBytes() can't start writing file \"eeprom.bin\""));
			}
		}

		CloseHandle(hEepromFile);
		CloseHandle(ovelapf.hEvent);
	}
}

__declspec(dllexport) void __stdcall CLogic_RestoreBytes(uint32_t unStartAddress, uint32_t unLength, uint8_t* pbyData)
{
	HANDLE hEepromFile = CreateFile(L"eeprom.bin", 
		GENERIC_READ,						// Specify mode that open device.
		FILE_SHARE_READ,					// the devide mode for others
		NULL,								// the object gets a default security.
		OPEN_EXISTING,						// Specify which action to take on file. 
		FILE_FLAG_OVERLAPPED,
		NULL);

	if (INVALID_HANDLE_VALUE == hEepromFile)
	{
		// DisplayError(TEXT("Error: CLogic_RestoreBytes() can't open file \"eeprom.bin\""));
	}
	else
	{
		DWORD dwcount = 0;
		OVERLAPPED ovelapf;
		memset(&ovelapf, 0, sizeof(ovelapf));
		ovelapf.Offset = unStartAddress;
		ovelapf.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

		//ovelapf.Offset = unStartAddress;
		if (!ReadFile(hEepromFile, pbyData, unLength, &dwcount, &ovelapf))
		{
			DWORD dw = GetLastError(); 
			if (dw == ERROR_IO_PENDING)
			{ 
				DWORD dwRes = WaitForSingleObject(ovelapf.hEvent, INFINITE);
				if (WAIT_OBJECT_0 == dwRes)
				{
					if (!GetOverlappedResult(hEepromFile, &ovelapf, &dwcount, FALSE))
					{
						dw = GetLastError(); 
						if (ERROR_HANDLE_EOF != dw)
						{
							DisplayError(TEXT("Error: CLogic_RestoreBytes() can't read data file \"eeprom.bin\""));
						}
						else
						{
							// It doesn't problem because code handles errors when eeprom is empty.
						}
					}
				}
			}
			else
			{
				DisplayError(TEXT("Error: CLogic_RestoreBytes() can't start reading file \"eeprom.bin\""));
			}
		}

		CloseHandle(hEepromFile);
		CloseHandle(ovelapf.hEvent);
	}
}

__declspec(dllexport) void __stdcall CLogic_StoreByte(uint32_t unAddress, uint8_t byData)
{
	CLogic_StoreBytes(unAddress, 1, &byData);
}

__declspec(dllexport) uint8_t __stdcall CLogic_RestoreByte(uint32_t unAddress)
{
	char chData = 0;
	CLogic_RestoreBytes(unAddress, 1, &chData);
	return chData;
}

char g_bHVConfirmRequire = 0;
void SetOnLedBlinking(bool bOn)
{
	g_bHVConfirmRequire = bOn;
}

__declspec(dllexport) bool __stdcall CLogic_SwitchOut(char bOn, char bHV, char bHVConfirmRequire, char bForce)
{
	bool bCancel = false;
	size_t i;
	if (bOn)
	{
		if (bHVConfirmRequire)
		{
			SetOnLedBlinking(true);
		}
		else
		{
			if (g_bHVConfirmRequire)
			{
				if (bHV)
				{
					for(i = 0; i < 10; i++)
					{
						if(!g_HVBuzzerSuppress)
							Beep(1000,100);
						Sleep(250);
					}
				}
				SetOnLedBlinking(false);
			}
		}
	}
	else if (bHVConfirmRequire)
	{
		SetOnLedBlinking(true);
	}
	else
	{
		SetOnLedBlinking(false);
	}

	return bCancel;
}

uint32_t GetTime()
{
	return GetTickCount();
}

__declspec(dllexport) void __stdcall CLogic_SendRemoteCtrlData(const char* pBuf, int length)
{}

__declspec(dllexport) void __stdcall CLogic_SendControlData(const char* pBuf, int length)
{
	uint8_t unCrc = 0; 
	char* pNewBuff = (char*)(malloc((length + 1) * sizeof(char)));

	memcpy(pNewBuff, pBuf, length);

	// Add package CRC
	unCrc = countCrc8((const uint8_t*)pBuf, length, 0);
	unCrc = countCrc8((const uint8_t*)STR_SW_VERSION, strlen(STR_SW_VERSION), unCrc);
	pNewBuff[length] = unCrc;
	length++;

	// For physical USB or serial port
	if (handlePortArm)
	{
		DWORD dwWritelen = (DWORD)length;
		char bRet = 0;
		OVERLAPPED osWrite = {0};
		osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
		if (WriteFile(handlePortArm,   // handle to file to write to
			pNewBuff,              // pointer to data to write to file
			length,              // number of bytes to write
			&dwWritelen, &osWrite) == 0)      // pointer to number of bytes written
		{
			 if (GetLastError() == ERROR_IO_PENDING)
			 { 
				DWORD dwRes = WaitForSingleObject(osWrite.hEvent, INFINITE);
				if (WAIT_OBJECT_0 == dwRes)
				{
					bRet = GetOverlappedResult(handlePortArm, &osWrite, &dwWritelen, FALSE);
				}
			}
		}

		if (!bRet)
		{
			CloseHandle(handlePortArm);
			handlePortArm = NULL;
			MessageBoxA(NULL, "Error: WriteFile() returns false", NULL, 0);
		}
		CloseHandle(osWrite.hEvent);
	}

	// For UI emulation only. Shuld be executed with delay.
	CUI_HandleCommandStringDlg(pNewBuff, length);

	free(pNewBuff);
}

/** **********************************************************************
 *
 * Shows error message on the screen
 * @param error text to show
 *
 *********************************************************************** */
__declspec(dllexport) void __stdcall CLogic_SetLastError(const char* pszError)
{
	UI_ShowError(pszError);
}
