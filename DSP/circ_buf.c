/*
 * File:    circ_buf.c
 * Title:
 * Created: Tue Sep 05 10:13:49 2006
 *
 */
#include "circ_buf.h"

void CIRC_BUF_init(CIRC_BUF *buf, char *mem, int size)
{
    buf->mask = size - 1;
    buf->writeIndex = 0;
    buf->readIndex = 0;
    buf->mem = mem;
}

void CIRC_BUF_put(CIRC_BUF *buf, const char *mem, int count)
{
	int i = 0;
    for(; i < count; i++)
	{
        buf->mem[buf->writeIndex++] = *mem++;
        buf->writeIndex &= buf->mask;
	
		if(buf->writeIndex == buf->readIndex)
		{
			buf->readIndex++;
			buf->readIndex &= buf->mask;

			ReportError("CIRC_BUF_put() - Buffer overload");
		}
	}
}

/** **********************************************************************
 *
 * Checks that circular buffer has some data
 * @param pCircBuffer	circular buffer
 * @return	returns true if buffer is empty
 *
 *********************************************************************** */
bool CIRC_BUF_ISEMPTY(CIRC_BUF *buf)
{
	return buf->readIndex == buf->writeIndex;
}

/** **********************************************************************
 *
 * Reads data from circular buffer and moves or copies them into provided buffer.
 *
 * @param pCircBuffer	circular buffer
 * @param pOutMemBuff	output buffer to fill
 * @param nCount		count bytes to copy/move from circular buffer to pOutMemBuff
 * @param bMove			if value true function moves data otherwise copies
 *
 * @return	returns count of copied/moved data
 *
 *********************************************************************** */
int CIRC_BUF_get(CIRC_BUF *pCircBuffer, char *pOutMemBuff, int nCount, bool bMove)
{
	int nGetCount = 0;
    if(nCount <= (pCircBuffer->mask + 1))
	{
		int readIndex = pCircBuffer->readIndex;
		for(; nGetCount < nCount && readIndex != pCircBuffer->writeIndex; nGetCount++)
		{
			if (pOutMemBuff)
			{
				*pOutMemBuff = pCircBuffer->mem[readIndex];
				pOutMemBuff++;
			}
			
			readIndex++;
			readIndex &= pCircBuffer->mask;
		}
		
		if (nGetCount && bMove)
		{
			pCircBuffer->readIndex = readIndex;
		}
	}
	
    return nGetCount;
}
