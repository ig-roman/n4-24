
// Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "h4_x.h"
#include "Dlg.h"
#include "Header.h"
#include "afxdialogex.h"
#include "CCS/UIInterface.h"
#include "CCS/LogicInterface.h"
#include "AARM/ArmInterface.h"
#include "CommonCUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CDlg dialog
DWORD WINAPI ThreadFun(LPVOID lpParam);
void CheckControlData();

OVERLAPPED s_ovRead = {0};

DWORD g_dUIThreadID;
DRAW_EVENT_TYPE g_eDrawEvt;
unsigned long g_unPwm = 0;
unsigned long g_unDelay = 0;
unsigned char g_nByteOfCSReg = 0;
unsigned char g_nActiveCSRegs = 0;
unsigned char g_byCSRegsbit[15][5] = {};
char g_szSPITextBuff[100] = { 0 };
uint32_t g_unLastLogTime = GetTickCount();
HANDLE	m_hLoopEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);

CDlg::CDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDlg::IDD, pParent),
	m_pCDC(NULL),
	m_hMemBmp(NULL),
	m_bIsInvalid(false),
	m_nCmdLen(0)
{
	s_ovRead.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwID = 0;
	m_hThread = CreateThread(NULL, 0, ThreadFun, this, 0, &dwID);

	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	
	hFontArmRegs = CreateFont(13,0,0,0,FW_DONTCARE,FALSE,FALSE,FALSE,DEFAULT_CHARSET,OUT_OUTLINE_PRECIS,
						CLIP_DEFAULT_PRECIS,CLEARTYPE_QUALITY, VARIABLE_PITCH,TEXT("Courier New"));

	InitializeCriticalSection(&m_cs);

	disp.m_pParent = this;

	m_hDrawEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
}

void CDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_SYSKEYDOWN()
	ON_WM_SYSKEYUP()
	ON_WM_TIMER()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()


// CDlg message handlers

__declspec(dllexport) bool __stdcall CARM_DelayMsDlg(unsigned long unDelay, bool bSkipLoop, bool bCheckOverload)
{
	CDlg* pdlg = (CDlg*)theApp.m_pdlg;
	return pdlg->CARM_DelayMsDlg(unDelay, bSkipLoop, bCheckOverload);
}

__declspec(dllexport) void __stdcall CARM_SetPWMValueDlg(unsigned long unPwm)
{
	CDlg* pdlg = (CDlg*)theApp.m_pdlg;
	pdlg->CARM_SetPWMValueDlg(unPwm);
}

__declspec(dllexport) void __stdcall CARM_SPI_StartDlg(unsigned char unAddr)
{
	CDlg* pdlg = (CDlg*)theApp.m_pdlg;
	pdlg->CARM_SPI_StartDlg(unAddr);
}

__declspec(dllexport) void __stdcall CARM_SPI_SendDlg(char byByte)
{
	CDlg* pdlg = (CDlg*)theApp.m_pdlg;
	pdlg->CARM_SPI_SendDlg(byByte);
}

__declspec(dllexport) void __stdcall CARM_SPI_StopDlg()
{
	CDlg* pdlg = (CDlg*)theApp.m_pdlg;
	pdlg->CARM_SPI_StopDlg();
}

__declspec(dllexport) void __stdcall CUI_SendDisplayDataDlg(const char* pBuf, int length)
{
	CDlg* pdlg = (CDlg*)theApp.m_pdlg;
	pdlg->CUI_SendDisplayDataDlg(pBuf, length);
}

__declspec(dllexport) char __stdcall CUI_WaitDisplayACKDlg()
{
	CDlg* pdlg = (CDlg*)theApp.m_pdlg;
	return pdlg->CUI_WaitDisplayACKDlg();
}

__declspec(dllexport) void __stdcall CUI_GetTextExtentDlg(PUSHORT punX, PUSHORT punY)
{
	CDlg* pdlg = (CDlg*)theApp.m_pdlg;
	pdlg->CUI_GetTextExtentDlg(punX, punY);
}

__declspec(dllexport) void __stdcall CUI_HandleCommandStringDlg(const char* pBuf, unsigned int length)
{
	CDlg* pdlg = (CDlg*)theApp.m_pdlg;
	pdlg->CUI_HandleCommandStringDlg(pBuf, length);
}

__declspec(dllexport) void __stdcall CLogic_ManageSerial()
{
	//MSG Msg;
	//while (::PeekMessage(&Msg, 0, 0, 0, PM_REMOVE))
	//{
	//	TranslateMessage(&Msg);
	//	DispatchMessage(&Msg);
	//}

	if (!handlePortArm)
	{
		ARM_Loop();
	}
	CheckControlData();
}

CDC* CDlg::GetDc()
{
	return m_pCDC;
}

void CDlg::SetInvalid()
{
	m_bIsInvalid = true;
}

void CDlg::CUI_GetTextExtentDlg(PUSHORT punX, PUSHORT punY)
{
	if (lastTextSize.cx > 0 && lastTextSize.cx > 0)
	{
		*punX = (USHORT)lastTextSize.cx;
		*punY = (USHORT)lastTextSize.cy;
		lastTextSize.cx = -1;
	}
}

void CDlg::SendDispRetData(const char * buff, int length)
{
	if (1 == length)
	{
		m_bAckOk = ACK == buff[0];
	}
	else if (5 == length)
	{
		// Text Extend
		// Buffer for string [ACK][00][51][00][10][ACK]
		// [00][51] - X
		// [00][10] - Y

		lastTextSize.cx = ((ULONG)buff[0]) << 8 | (UCHAR)buff[1];
		lastTextSize.cy = ((ULONG)buff[2]) << 8 | (UCHAR)buff[3];
	}
	else if (7 == length)
	{
		// Touch Event 
		// 4B, FF, 0016, 0007, 06 - Push event (K key_char ev_coord_x ev_coord_y [ACK])
		// 6B, FF, 0016, 0007, 06 - Release event (k key_char ev_coord_x ev_coord_y [ACK])
		bool bPressed = 0x4B == (UCHAR)buff[0];

		ULONG x = ((ULONG)buff[2]) << 8 | (UCHAR)buff[3];
		ULONG y = ((ULONG)buff[4]) << 8 | (UCHAR)buff[5];

		UI_TouchEvent((uint16_t)x, (uint16_t)y, bPressed);
	}
}

void CDlg::CUI_HandleCommandStringDlg(const char* pBuf, unsigned int length)
{
	memcpy(m_buffCmd, pBuf, length);
	m_nCmdLen = length;
	SetEvent(m_hLoopEvent);
}

void CDlg::CUI_SendDisplayDataDlg(const char* pBuf, int length)
{
	for (int n = 0; n < length; n++)
	{
		arrDisCmd.push_back(pBuf[n]);
	}
}

char CDlg::CUI_WaitDisplayACKDlg()
{
	disp.HandleDisplayData(&arrDisCmd[0], arrDisCmd.size());
	arrDisCmd.clear();
	return m_bAckOk;
}

BOOL CDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	::SetTimer(GetSafeHwnd(), 1, 10, NULL);

	m_pConsole = new ConsoleDlg();
	m_pConsole->Create(IDD_CONSOLE, this);
	m_pConsole->ShowWindow(SW_SHOW);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
static bool bFirstTime = true;
void CDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CRect rect;
	GetClientRect(&rect);
	if (IsIconic())
	{
		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		if (bFirstTime)
		{
			g_dUIThreadID = GetCurrentThreadId();
			CDC* CDC = GetDC();
			HDC hDC= CDC->GetSafeHdc();
			HDC m_hMemDC = CreateCompatibleDC(hDC);
			m_pCDC = CDC::FromHandle(m_hMemDC);

			m_hMemBmp = CreateCompatibleBitmap(hDC, rect.Width(), rect.Height());
			ReleaseDC(CDC);

			HBITMAP hOldBmp = (HBITMAP)SelectObject(m_hMemDC, m_hMemBmp);
			DeleteObject(hOldBmp);

			CBrush br(0xFFFFFF);
			m_pCDC->FillRect(rect, &br);

			ARM_Init();
			Logic_Init();
			UI_Init();
			bFirstTime = false;
		}
		dc.BitBlt(rect.left, rect.top, rect.Width()/*870*/, rect.Height(), m_pCDC, 0, 0, SRCCOPY);
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

unsigned long long lModeButtonsCounter = 0;
unsigned long long lCounter = 0;
UINT s_chPressedBefore = 0;
void CDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (nRepCnt > 10)
	{
		int n =0;
	}
	if ('T' == nChar || 'S' == nChar)
	{
		//while (lCounter < 80400L || 'S' == nChar)
		while (true)
		{
			char unKeyOldCode = rand() % 255;

			//if ('I' == unKeyOldCode || 'U' == unKeyOldCode || 'R' == unKeyOldCode)
			if ('M' == unKeyOldCode || 'I' == unKeyOldCode || 'U' == unKeyOldCode || 'R' == unKeyOldCode)
			{
				if (false)
				{
					lModeButtonsCounter++;
					if (lModeButtonsCounter > 1000000)
					{
						UI_ButtonChanged(unKeyOldCode, true);
						UI_ButtonChanged(unKeyOldCode, false);
						lCounter++;
						lModeButtonsCounter = 0;
					}
				}
			}
			else
			{
				UI_ButtonChanged(unKeyOldCode, true);
				UI_ButtonChanged(unKeyOldCode, false);
				lCounter++;
			}
			if ('S' == nChar)
			{
				break;
			}
			UI_Loop();
		}
	}

	UI_ButtonChanged(nChar, true);
	s_chPressedBefore = nChar;
	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CDlg::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (s_chPressedBefore)
	{
		s_chPressedBefore = 0;
	}
	else
	{
		UI_ButtonChanged(nChar, true);
	}
	UI_ButtonChanged(nChar, false);
	
	CDialog::OnKeyUp(nChar, nRepCnt, nFlags);
}

void CDlg::OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	//UI_ButtonChanged(nChar, true);
	//UI_ButtonChanged(nChar, false);
	CDialog::OnSysKeyDown(nChar, nRepCnt, nFlags);
}

void CDlg::OnSysKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	UI_ButtonChanged(nChar, true);
	UI_ButtonChanged(nChar, false);
	CDialog::OnSysKeyUp(nChar, nRepCnt, nFlags);
}

DWORD WINAPI ThreadFun(LPVOID lpParam) 
{
	CDlg* pThis = (CDlg*)lpParam;
	for (;;)
	{
		DWORD dwRet = WaitForSingleObject(m_hLoopEvent, INFINITE);
		if (dwRet != WAIT_TIMEOUT)
		{
			// Event
			ResetEvent(m_hLoopEvent);
			if (pThis->m_nCmdLen != 0)
			{
				char buffCmd[LLPROG_BUFFER_SIZE];
				unsigned int nCmdLen;

				EnterCriticalSection(&pThis->m_cs);
				nCmdLen = pThis->m_nCmdLen;
				::memcpy(buffCmd, pThis->m_buffCmd, pThis->m_nCmdLen);
				LeaveCriticalSection(&pThis->m_cs);

				ARM_HandleCommandString(buffCmd, nCmdLen);

				EnterCriticalSection(&pThis->m_cs);
				if (pThis->m_nCmdLen && nCmdLen == pThis->m_nCmdLen && 0 == ::memcmp(buffCmd, pThis->m_buffCmd, nCmdLen))
				{
					pThis->m_nCmdLen = 0;
				}
				LeaveCriticalSection(&pThis->m_cs);
			}
			ARM_Loop();
			//Sleep(2000);
		}
	}
}

DWORD dwReadLen = 0;
char comBuff[1000] = {0};
bool bWaitingOnRead = false;

void ProcessErrorMessage(char* ErrorText)
{
	MessageBoxA(NULL, ErrorText, NULL, 0);
}

void CheckControlData()
{
	if (handlePortArm)
	{
		if (bWaitingOnRead)
		{
			if (WaitForSingleObject(s_ovRead.hEvent, 0) == WAIT_OBJECT_0)
			{
				if (!::GetOverlappedResult(handlePortArm, &s_ovRead, &dwReadLen, FALSE))
				{
					ProcessErrorMessage("GetOverlappedResults() in ReadFile()");
				}
				bWaitingOnRead = false;
			}
		}
		else
		{
			if (!::ReadFile(handlePortArm, comBuff, 1000, &dwReadLen, &s_ovRead))
			{
				if (::GetLastError() == ERROR_IO_PENDING)
				{
					bWaitingOnRead = true;
				}
				else
				{	// Error
					ProcessErrorMessage("ReadFile()");
					dwReadLen = 0;
				}
			}
		}
	}
	else if (bWaitingOnRead)
	{
		bWaitingOnRead = false;
		ResetEvent(s_ovRead.hEvent);
	}

	if (dwReadLen > 0)
	{
		Logic_HandleControlData(comBuff, dwReadLen);
		dwReadLen = 0;
	}
}

void CDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (!bFirstTime)
	{
		UI_Loop();
		Logic_Loop();
		SetEvent(m_hLoopEvent);
		CheckControlData();

		if (WaitForSingleObject(m_hDrawEvent, 0) == WAIT_OBJECT_0)
		{
			switch (g_eDrawEvt)
			{
			case DRAW_EVT_DELAY:
				char szBuff[50];
				sprintf(szBuff, "Delay %dms", g_unDelay);
				LogStr(szBuff);
				break;
			case DRAW_EVT_PWM:
				DrawPWM();
				break;
			case DRAW_EVT_CS:
				DrawCS();
				break;
			}
			ResetEvent(m_hDrawEvent);
			if (!m_bIsInvalid)
			{
				CRect rect(880, 0, 1200, 532);
				InvalidateRect(rect);
				UpdateWindow();
			}
		}

		if (m_bIsInvalid)
		{
			m_bIsInvalid = false;
			Invalidate();
			UpdateWindow();
		}
	}
	CDialog::OnTimer(nIDEvent);
}

CDlg::~CDlg()
{
	DeleteDC(m_pCDC->GetSafeHdc());
	DeleteObject(m_hMemBmp);

	if (m_pConsole)
	{
		delete m_pConsole;
	}

	::SuspendThread(m_hThread);
	::CloseHandle(m_hThread);
	::CloseHandle(m_hLoopEvent);
	::CloseHandle(m_hDrawEvent);

	CloseHandle(s_ovRead.hEvent);
	DeleteCriticalSection(&m_cs);
}

void CDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	disp.HandleTouchEvent(point, true);
	CDialog::OnLButtonDown(nFlags, point);
}

void CDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	disp.HandleTouchEvent(point, false);
	CDialog::OnLButtonUp(nFlags, point);
}

short GetRegSz(unsigned char unCSReg)
{
	switch (unCSReg) {
	case 2: return 3;
	case 3: return 2;
	case 4: return 5;
	default: return 1;
	}
}

void CDlg::LogStr(char* pszText)
{
	bool bLogSpace = g_unLastLogTime + 500 < GetTickCount();
	g_unLastLogTime = GetTickCount();

	if (bLogSpace)
	{
		LogStr("");
	}

	CDC& dcs = *m_pCDC;

	dcs.SetBkColor(0xF8F8F8);
	dcs.SetTextColor(0);

	dcs.SelectObject(hFontArmRegs);

	CRect rectClient(880, 520, 1070, 532);

	CRect updRect;
	CRect ScrollRect(880, 0, 1070, 532);
	dcs.ScrollDC(0, -13, ScrollRect, ScrollRect, NULL, &updRect);

	CBrush br(0xF8F8F8);
	dcs.FillRect(rectClient, &br);
	USES_CONVERSION;
	dcs.DrawText(CA2CT(pszText), -1, rectClient, 0);

}

void CDlg::CARM_SPI_StartDlg(unsigned char unAddr)
{
	if (g_dUIThreadID != GetCurrentThreadId())
	{
		while (WaitForSingleObject(m_hDrawEvent, 0) == WAIT_OBJECT_0);
	}
	sprintf(g_szSPITextBuff, "CS %02d ", (unsigned char)unAddr);

	g_nByteOfCSReg = 0;
	g_nActiveCSRegs = unAddr;
	m_pConsole->Log_SPI_Start(unAddr);
}

void CDlg::CARM_SPI_SendDlg(char byByte)
{
	bool bShortReg = GetRegSz(g_nActiveCSRegs) == 1 || GetRegSz(g_nActiveCSRegs) == 2;

	char szBuff[10];
	if (bShortReg)
	{
		ToBinStr(szBuff, byByte);
	}
	else
	{
		sprintf(szBuff, "0x%02x", (unsigned char)byByte);
	}
	strcat(g_szSPITextBuff, szBuff);
	strcat(g_szSPITextBuff, " ");

	g_byCSRegsbit[g_nActiveCSRegs][g_nByteOfCSReg] = byByte;
	g_nByteOfCSReg++;

	m_pConsole->Log_SPI_Send(byByte);
}

void CDlg::DrawCS()
{
	char szBuff[200];

	LogStr(g_szSPITextBuff);

	CDC& dcs = *m_pCDC;
	dcs.SetBkColor(0xFFFFFF);
	dcs.SetTextColor(0);
	dcs.SelectObject(hFontArmRegs);

	CRect rectClient(1080, 20, 1200, 30);
	USES_CONVERSION;
	for (int n = 0; n < 15; n++)
	{
		rectClient.OffsetRect(0, 10);

		sprintf(szBuff, "CS%02d", n);
		dcs.DrawText(CA2CT(szBuff), -1, rectClient, 0);

		rectClient.OffsetRect(40, 0);

		short sz = GetRegSz(n);
		if (sz == 1)
		{
			ToBinStr(szBuff, g_byCSRegsbit[n][0]);
			dcs.DrawText(CA2CT(szBuff), -1, rectClient, 0);
		}
		else
		{
			dcs.DrawText(_T(""), -1,  rectClient, 0);
			rectClient.OffsetRect(0, 5);

			for (short b = 0; b < sz; b++)
			{
				ToBinStr(szBuff, g_byCSRegsbit[n][b]);

				dcs.DrawText(CA2CT(szBuff), -1, rectClient, 0);
				rectClient.OffsetRect(0, b == sz - 1 ? 5 : 10);
			}
		}

		rectClient.OffsetRect(-40, 0);
	}
}

void CDlg::DrawPWM()
{
	CDC& dcs = *m_pCDC;

	dcs.SetBkColor(0xFFFFFF);
	dcs.SetTextColor(0);

	dcs.SelectObject(hFontArmRegs);

	char szBuff[50];

	uint16_t unPwm1 = g_unPwm >> 16;
	uint16_t unPwm2 = g_unPwm & 0xffff;
	double dPwmVal = (unPwm1 * PWM_R1 + unPwm2 * PWM_R2) / (PWM_RS * PWM_MAX_VAL) * PWM_U_REF_VOL;;
	sprintf(szBuff, "PWM: %02.8fv ", dPwmVal);
	CRect rectClient(1080, 10, 1300, 20);
	dcs.DrawText(CA2CT(szBuff), -1, rectClient, 0);
	LogStr(szBuff);
	sprintf(szBuff, "%05d,%05d", unPwm1, unPwm2);
	rectClient.SetRect(1080, 20, 1300, 30);
	dcs.DrawText(CA2CT(szBuff), -1, rectClient, 0);
	LogStr(szBuff);
}

void CDlg::CARM_SPI_StopDlg()
{
	if (g_dUIThreadID == GetCurrentThreadId())
	{
		DrawCS();
	}
	else
	{
		g_eDrawEvt = DRAW_EVT_CS;
		SetEvent(m_hDrawEvent);
	}
	m_pConsole->Log_SPI_Stop();
}

bool CDlg::CARM_DelayMsDlg(unsigned long unDelay, bool bSkipLoop, bool bCheckOverload)
{
	bool bRet = true;
	if (g_dUIThreadID == GetCurrentThreadId())
	{
		g_unDelay = unDelay;
		char szBuff[50];
		sprintf(szBuff, "Delay %dms", g_unDelay);
		LogStr(szBuff);
	}
	else
	{
		while (WaitForSingleObject(m_hDrawEvent, 0) == WAIT_OBJECT_0);
		g_unDelay = unDelay;
		g_eDrawEvt = DRAW_EVT_DELAY;
		SetEvent(m_hDrawEvent);
	}

	UINT unDelayDiv10 = unDelay / 10;
	m_pConsole->Log_DelayMs(unDelayDiv10);

	int nCnt = 0;
	for (; nCnt < (int)(unDelayDiv10 && bRet); nCnt++)
	{
		Sleep(10);
		if (!bSkipLoop)
		{
			if (m_nCmdLen != 0)
			{
				char buffCmd[100];
				unsigned int nCmdLen;

				EnterCriticalSection(&m_cs);
				nCmdLen = m_nCmdLen;
				::memcpy(buffCmd, m_buffCmd, m_nCmdLen);
				LeaveCriticalSection(&m_cs);

				ARM_HandleCommandString(buffCmd, nCmdLen);

				EnterCriticalSection(&m_cs);
				if (m_nCmdLen && nCmdLen == m_nCmdLen && 0 == ::memcmp(buffCmd, m_buffCmd, nCmdLen))
				{
					m_nCmdLen = 0;
				}
				LeaveCriticalSection(&m_cs);
			}

			if (bCheckOverload)
			{
				UCHAR nState = (UCHAR)GetKeyState(VK_SHIFT);
				if ((nState & 0x80) > 0)
				{
					bRet = false;
				}
			}
		}
	}
	UINT unLess10Ms = unDelay - unDelayDiv10 * 10;
	if (unLess10Ms && bRet)
	{
		Sleep(unLess10Ms);
	}

	return bRet;
}

void CDlg::CARM_SetPWMValueDlg(unsigned long unPwm)
{
	USES_CONVERSION;
	if (g_dUIThreadID == GetCurrentThreadId())
	{
		g_unPwm = unPwm;
		DrawPWM();
	}
	else
	{
		while (WaitForSingleObject(m_hDrawEvent, 0) == WAIT_OBJECT_0);
		g_unPwm = unPwm;
		g_eDrawEvt = DRAW_EVT_PWM;
		SetEvent(m_hDrawEvent);
	}
	m_pConsole->Log_SetPWMValue(unPwm);
}

BOOL CDlg::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
}
