
// Dlg.h : header file
//

#pragma once

#ifdef __cplusplus

#include "ConsoleDlg.h"
#include "CiLcd.h"
#include "IiLcdParent.h"
#include "TopCommon.h"
// CDlg dialog
class CDlg : public CDialog, public IiLcdParent
{
// Construction
public:
	CDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CDlg();
	
// Dialog Data
	enum { IDD = IDD_H4_X_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSysKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);

	char CUI_WaitDisplayACKDlg();
	void CUI_SendDisplayDataDlg(const char* pBuf, int length);
	void CUI_GetTextExtentDlg(PUSHORT punX, PUSHORT punY);

	void CUI_HandleCommandStringDlg(const char* pBuf, unsigned int length);

	void CARM_SPI_StartDlg(unsigned char unAddr);
	void CARM_SPI_SendDlg(char byByte);
	void CARM_SPI_StopDlg();
	void CARM_SetPWMValueDlg(unsigned long unPwm);
	bool CARM_DelayMsDlg(unsigned long unDelay, bool bSkipLoop, bool bCheckOverload);

	unsigned int m_nCmdLen;
	char m_buffCmd[LLPROG_BUFFER_SIZE];

	CRITICAL_SECTION m_cs;

	virtual CDC* GetDc();
	virtual void SetInvalid();
	virtual void SendDispRetData(const char * buff, int length);

private:
	void DrawCS();
	void DrawPWM();
	void LogStr(char*);
	HFONT hFontArmRegs;
	CDC*	m_pCDC;
	CiLcd	disp;
	HBITMAP m_hMemBmp;
	bool m_bIsInvalid;
	CSize lastTextSize;

	HANDLE m_hThread;
	bool m_bAckOk;
	TCmdArray arrDisCmd;
	HANDLE m_hDrawEvent;
	
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);

private:
	ConsoleDlg* m_pConsole;
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};

typedef enum {
	DRAW_EVT_DELAY,
	DRAW_EVT_PWM,
	DRAW_EVT_CS
} DRAW_EVENT_TYPE;

#endif