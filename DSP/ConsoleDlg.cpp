// ConsoleDlg.cpp : implementation file
//

#include "stdafx.h"
#include "H4_X.h"
#include "ConsoleDlg.h"
#include "afxdialogex.h"

#include "resource.h"

#include "AARM/ArmInterface.h"
#include "CCS/LogicInterface.h"
#include "CommonCUI.h"
#include "CCS/Logic/RemoteCtrl/inst.h"

#include "TopCommon.h"
#include <string>
#include <sstream>
#include <iostream>
#include <bitset>

// ConsoleDlg dialog

IMPLEMENT_DYNAMIC(ConsoleDlg, CDialog)

ConsoleDlg::ConsoleDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ConsoleDlg::IDD, pParent),
	m_nSPLength(0),
	m_unSPAddr(0)
{
}

ConsoleDlg::~ConsoleDlg()
{
}

void ConsoleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(ConsoleDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_SEND, &ConsoleDlg::OnBnClickedBtnSend)
	ON_BN_CLICKED(IDC_BTN_CONNECT, &ConsoleDlg::OnBnClickedBtnConnect)
END_MESSAGE_MAP()

// ConsoleDlg message handlers

static const char UILLCMD_PWM_CH = 'P';
#define UILLCMD_PWM			"PWM"

static const char UILLCMD_WAIT_CH = 'W';
#define UILLCMD_WAIT		"WAIT"

static const char UILLCMD_CS_CH = 'C';
#define UILLCMD_CS			"CS"

#define UILLCMD_BYTE_DEC	'd'
#define UILLCMD_BYTE_HEX	'x'
#define UILLCMD_BYTE_BIN	'b'

#define UILLCMD_IGNORE_CHARS " \r\n"

#define MAX_UILLCMD_LEN 2000

bool MoveIdx(int& nIdx , int nCount)
{
	nIdx += nCount;
	return nIdx < MAX_UILLCMD_LEN;
}

bool IsHex(char chTest)
{
	return (chTest >= 'a' && chTest <= 'f') || (chTest >= 'A' && chTest <= 'F') || (chTest >= '0' && chTest <= '9');
}

bool IsBin(char chTest)
{
	return ('0' == chTest || '1' == chTest);
}

bool Read(const char* pszUiData, int& nStringCursor, char*& pszLLCommand, int nSizeInBytes = 1)
{
	bool bRet = false;
	while (pszUiData[nStringCursor] && strchr(UILLCMD_IGNORE_CHARS, pszUiData[nStringCursor]) && MoveIdx(nStringCursor, 1));

	switch (pszUiData[nStringCursor])
	{
		case UILLCMD_BYTE_DEC:
		{
			if (MoveIdx(nStringCursor, 1))
			{
				int nEndOfDigits = nStringCursor;
				while (isdigit(pszUiData[nEndOfDigits]) && MoveIdx(nEndOfDigits, 1));
				int nLenOfInt = nEndOfDigits - nStringCursor;
				if (0 < nLenOfInt)
				{
					std::string strInteger(pszUiData + nStringCursor, nEndOfDigits - nStringCursor);
					int nVal = atoi(strInteger.c_str());
					long lMaxVal = (long)powf(2, (float)(8 * nSizeInBytes));
					if (nVal >= 0 && nVal <= lMaxVal)
					{
						if (2 == nSizeInBytes)
						{
							*((short*)pszLLCommand) = (short)nVal;
							bRet = true;
						}
						else if (1 == nSizeInBytes)
						{
							*pszLLCommand = (char)nVal;
							bRet = true;
						}
						
						if (bRet)
						{
							pszLLCommand += nSizeInBytes;
							nStringCursor += nLenOfInt;
						}
					}
				}
			}
			break;
		}
		case UILLCMD_BYTE_HEX:
		{
			if (MoveIdx(nStringCursor, 1))
			{
				int nEndOfDigits = nStringCursor;
				while (IsHex(pszUiData[nEndOfDigits]) && MoveIdx(nEndOfDigits, 1));
				int nLenOfHex = nEndOfDigits - nStringCursor;
				if (nLenOfHex > 0 && nLenOfHex <= 2)
				{
					std::string strHex(pszUiData + nStringCursor, nEndOfDigits - nStringCursor);
					std::stringstream ss;
					ss << std::hex << strHex;

					int nHexVal = 0;
					ss >> nHexVal;
					*pszLLCommand = nHexVal;
					pszLLCommand++;
					nStringCursor += nLenOfHex;

					bRet = true;
				}
			}
			break;
		}
		case UILLCMD_BYTE_BIN:
		{
			if (MoveIdx(nStringCursor, 1))
			{
				int nEndOfDigits = nStringCursor;
				while (IsBin(pszUiData[nEndOfDigits]) && MoveIdx(nEndOfDigits, 1));
				int nLenOfBin = nEndOfDigits - nStringCursor;
				if (8 == nLenOfBin)
				{
					std::string strBin(pszUiData + nStringCursor, nEndOfDigits - nStringCursor);
					//std::reverse(strBin.begin(), strBin.end());
					std::bitset<8> bs(strBin);
					*pszLLCommand = (char)bs.to_ulong();
					pszLLCommand++;
					bRet = true;
				}
				nStringCursor += nLenOfBin;
			}
			break;
		}
	}

	return bRet;
}

void ConsoleDlg::OnBnClickedBtnSend()
{
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_INPUT);
	TCHAR szBuffT[MAX_UILLCMD_LEN] = { 0 };

	int nStartChar;
	int nEndChar;
	pEdit->GetSel(nStartChar, nEndChar);
	GetDlgItemText(IDC_EDIT_INPUT, szBuffT, MAX_UILLCMD_LEN);
	int nUiStrLen = _tcslen(szBuffT);

	if (nStartChar != nEndChar)
	{
		szBuffT[nEndChar] = NULL;
		nUiStrLen = nEndChar - nStartChar;
		memmove(szBuffT, szBuffT + nStartChar, (nUiStrLen + 1) * sizeof(TCHAR));
	}

	USES_CONVERSION;
	const char* szBuff = T2A((LPCTSTR)szBuffT);

	if (!handlePortArm)
	{	// Execute remote commands if not connected to AARM
		char pszOutBuff[INST_OUTPUT_BUFFER_MAX_SIZE + 2];
		INST_execute((char *)szBuff, pszOutBuff);
		if (*pszOutBuff)
			AppendText(pszOutBuff);
		else
			AppendText("Nothing");
		systemErrorNextQ(0, NULL, pszOutBuff);
		AppendText(pszOutBuff);
		AppendText(" ");
		return;
	}

	char arrLLCommand[LLPROG_BUFFER_SIZE * 2];
	char* pszLLCommand = arrLLCommand;

	pszLLCommand[0] = STRCMD_START;
	pszLLCommand[1] = STRCMD_CUSTOM;
	pszLLCommand += 2;

	bool bValidFormat = true;
	int nIdx = 0;
	for (; nIdx < nUiStrLen && bValidFormat; nIdx++)
	{
		if (!strchr(UILLCMD_IGNORE_CHARS, szBuff[nIdx]))
		{
			switch (szBuff[nIdx])
			{
				case UILLCMD_PWM_CH:
				{
					bValidFormat = MoveIdx(nIdx, strlen(UILLCMD_PWM));
					if (bValidFormat)
					{
						*pszLLCommand = LLPGR_PWM_CMD;
						pszLLCommand++;

						bValidFormat = bValidFormat && Read(szBuff, nIdx, pszLLCommand, 2);
						bValidFormat = bValidFormat && Read(szBuff, nIdx, pszLLCommand, 2);
						if (bValidFormat)
						{
							PUSHORT unPwm1 = (PUSHORT)(pszLLCommand - 4);
							PUSHORT unPwm2 = (PUSHORT)(pszLLCommand - 2);

							// Validate PWM max value for device
							bValidFormat = (*unPwm1 < PWM_MAX_VAL) && (*unPwm2 < PWM_MAX_VAL);
							if (bValidFormat)
							{
								std::swap<USHORT>(*unPwm1, *unPwm2);
							}
						}
					}
					break;
				}
				case UILLCMD_CS_CH:
				{
					bValidFormat = MoveIdx(nIdx, strlen(UILLCMD_CS));
					if (bValidFormat)
					{
						bValidFormat = Read(szBuff, nIdx, pszLLCommand);
						if (bValidFormat)
						{
							// Check CS address it should be 0 - 15
							bValidFormat = (UCHAR)(*(pszLLCommand - 1)) <= LLPGR_CS_MASK;
							if (bValidFormat)
							{
								*(pszLLCommand - 1) |= LLPGR_CS_AND_ADDR_CMD;

								bValidFormat = Read(szBuff, nIdx, pszLLCommand);
								if (bValidFormat)
								{
									int nDataSize = (UCHAR)(*(pszLLCommand - 1));
									for (;nDataSize > 0 && bValidFormat; nDataSize--)
									{
										bValidFormat = Read(szBuff, nIdx, pszLLCommand);
									}
								}
							}
						}
					}
					break;
				}
				case UILLCMD_WAIT_CH:
				{
					bValidFormat = MoveIdx(nIdx, strlen(UILLCMD_WAIT));
					if (bValidFormat)
					{
						*pszLLCommand = (char)LLPGR_DELAY_CMD;
						pszLLCommand++;
						bValidFormat = Read(szBuff, nIdx, pszLLCommand);
					}
					break;
				}
				default:
				{
					bValidFormat = false;
				}
			}
		}

		if (pszLLCommand - arrLLCommand > LLPROG_BUFFER_SIZE)
		{
			char szErrorBuff[2000] = {0};
			sprintf(szErrorBuff, "ERROR command to long. Maximum size is: %d, actual size is: '%d'", LLPROG_BUFFER_SIZE, pszLLCommand - arrLLCommand);
			AppendText(szErrorBuff);
		}
	}

	if (bValidFormat)
	{
		*pszLLCommand = (char)LLPROG_END_CMD;
		pszLLCommand++;

		Logic_SendLLCommand(arrLLCommand, pszLLCommand - arrLLCommand);
	}
	else
	{
		char szErrorBuff[2000] = {0};
		sprintf(szErrorBuff, "ERROR at char %d unable to parse:'%s'", nIdx, szBuff + nIdx);

		AppendText(szErrorBuff);
	}
}

void ConsoleDlg::AppendText(LPCSTR pszText)
{
	USES_CONVERSION;
	CString strLine;
	// add CR/LF to text
	strLine.Format(_T("%s\r\n"), A2T(pszText));
	CEdit* pEdit  = (CEdit*)GetDlgItem(IDC_EDIT_HISTORY);
	int nLength = pEdit->GetWindowTextLength();
	pEdit->SetSel(nLength, nLength);
	pEdit->ReplaceSel(strLine);
}

void ConsoleDlg::Log_DelayMs(unsigned long unDelay)
{
	char szBuff[50];
	sprintf(szBuff, "WAIT d%03d", unDelay);
	AppendText(szBuff);
}

void ConsoleDlg::Log_SetPWMValue(unsigned long unPwm)
{
	char szBuff[50];

	//sprintf(szBuff, "PWM x%02x x%02x x%02x x%02x", (UCHAR)(unPwm >> 24), (UCHAR)(unPwm >> 16), (UCHAR)(unPwm >> 8), (UCHAR)(unPwm));
	sprintf(szBuff, "PWM d%04d d%04d", unPwm >> 16, unPwm & 0xffff);

	AppendText(szBuff);
}

void ConsoleDlg::Log_SPI_Stop()
{
	char szBuff[2000] = {0};

	sprintf(szBuff, "CS d%02d d%02d ", m_unSPAddr, m_nSPLength);

	int nLen = strlen(szBuff);

	for (int nDatIdx = 0; nDatIdx < m_nSPLength; nDatIdx++)
	{
		ToBinStr(szBuff + nLen, (UCHAR)m_arrSpiData[nDatIdx]);
		nLen += 9;
		szBuff[nLen] = ' ';
		nLen++;
	}
	szBuff[nLen] = NULL;

	AppendText(szBuff);
}

void ConsoleDlg::Log_SPI_Send(char byByte)
{
	m_arrSpiData[m_nSPLength] = byByte;
	m_nSPLength++;
}

void ConsoleDlg::Log_SPI_Start(unsigned char unAddr)
{
	m_unSPAddr = unAddr;
	m_nSPLength = 0;
}

void ConsoleDlg::OnBnClickedBtnConnect()
{
	if (handlePortArm)
	{
		CloseHandle(handlePortArm);
		handlePortArm = NULL;
	}
	else
	{
		DCB config;
		TCHAR szBuff[20];
		_tcscpy(szBuff, _T("\\\\.\\COM"));
		GetDlgItem(IDC_EDT_COM_NR)->GetWindowText(szBuff + 7, _countof(szBuff) - 3);
		handlePortArm = CreateFile(szBuff,  // Specify port device: default "COM1"
		GENERIC_READ | GENERIC_WRITE,       // Specify mode that open device.
		0,                                  // the devide isn't shared.
		NULL,                               // the object gets a default security.
		OPEN_EXISTING,                      // Specify which action to take on file. 
		FILE_FLAG_OVERLAPPED,
		NULL);                              // default.

		if (GetCommState(handlePortArm, &config) != 0)
		{
			config.BaudRate = CBR_115200;
			config.ByteSize = 8;
			config.Parity = NOPARITY;
			config.StopBits = ONESTOPBIT;
			config.fRtsControl = 0;
			config.fDtrControl = 1;

			if (SetCommState(handlePortArm, &config) == 0)
			{
				CloseHandle(handlePortArm);
				handlePortArm = NULL;
				MessageBox(_T("Error: SetCommState() returns false"));
			}

			COMMTIMEOUTS CommTimeOuts = { 1, 0, 0, 0, 0 };
			SetCommTimeouts(handlePortArm, &CommTimeOuts);
		}
		else
		{
			CloseHandle(handlePortArm);
			handlePortArm = NULL;
			MessageBox(_T("Error: GetCommState() returns false"));
		}
	}

	GetDlgItem(IDC_BTN_CONNECT)->SetWindowText(handlePortArm ? _T("Disconnect") : _T("Connect"));
}

BOOL ConsoleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetDlgItem(IDC_EDT_COM_NR)->SetWindowText(_T("6"));
	return true;
}
