#include "stdafx.h"
#include "CiLcd.h"

CiLcd::CiLcd(void) : 
	m_pParent(0),
	rect(0,0,200,20),
	port(0,0,20,20),
	color(0xFFFFFF),
	textColor(0x0),
	unAlFlags(0),
	hFontActive(0)
{
	hFont1 = CreateFont(36,11,0,0,FW_DONTCARE,FALSE,FALSE,FALSE,DEFAULT_CHARSET,OUT_OUTLINE_PRECIS,
					CLIP_DEFAULT_PRECIS,CLEARTYPE_QUALITY, VARIABLE_PITCH,TEXT("Impact"));

	hFont2 = CreateFont(100,45,0,0,FW_BOLD,FALSE,FALSE,FALSE,DEFAULT_CHARSET,OUT_OUTLINE_PRECIS,
						CLIP_DEFAULT_PRECIS,CLEARTYPE_QUALITY, FIXED_PITCH,TEXT("Courier New") );

	hFont3 = CreateFont(40,17,0,0,FW_BOLD,FALSE,FALSE,FALSE,DEFAULT_CHARSET,OUT_OUTLINE_PRECIS,
						CLIP_DEFAULT_PRECIS,CLEARTYPE_QUALITY, FIXED_PITCH,TEXT("Courier New") );
}

CiLcd::~CiLcd(void)
{
	DeleteObject(hFont1);
	DeleteObject(hFont2);
	DeleteObject(hFont3);
}

void CiLcd::SendTextExtent(CSize& sizeStr)
{
	unsigned char nBuffDataLen[5];
	// Buffer for string [ACK][00][51][00][10][ACK]
	// [00][51] - X
	// [00][10] - Y

	nBuffDataLen[4] = 0x06;

	nBuffDataLen[0] = (unsigned char)(sizeStr.cx >> 8);
	nBuffDataLen[1] = (unsigned char)(sizeStr.cx);

	nBuffDataLen[2] = (unsigned char)(sizeStr.cy >> 8);
	nBuffDataLen[3] = (unsigned char)(sizeStr.cy);

	m_pParent->SendDispRetData((char*)nBuffDataLen, sizeof(nBuffDataLen));
}

void CiLcd::HandleTouchEvent(CPoint point, bool bPressed)
{
	unsigned char nBuffDataLen[7];

	// 4B, FF, 0016, 0007, 06 - Push event (K key_char ev_coord_x ev_coord_y [ACK])
	// 6B, FF, 0016, 0007, 06 - Release event (k key_char ev_coord_x ev_coord_y [ACK])
	nBuffDataLen[0] = bPressed ? 0x4B : 0x6B;
	nBuffDataLen[1] = 0xFF;
	nBuffDataLen[6] = 0x06;

	nBuffDataLen[2] = (unsigned char)(point.x >> 8);
	nBuffDataLen[3] = (unsigned char)(point.x);

	nBuffDataLen[4] = (unsigned char)(point.y >> 8);
	nBuffDataLen[5] = (unsigned char)(point.y);

	m_pParent->SendDispRetData((char*)nBuffDataLen, sizeof(nBuffDataLen));
}

// This is a new DrawText that will draw your text centered horizontally and vertically with word breaks
void CiLcd::DrawTextEEX(CDC* pdc, const LPCWSTR pszText, CRect& rcRect, UINT unAlFlags)
{
	CRect rcSavedRect = rcRect;
	CRect rcNewRect = rcRect;
	CRgn rgn;

	rgn.CreateRectRgn(rcRect.left, rcRect.top, rcRect.right, rcRect.bottom);

	// Call DrawText with DT_WORDBREAK | DT_CALCRECT flags, this will adjust the rectagle size
	// after breaking it into lines but without drawing
	pdc->DrawText(pszText, &rcSavedRect, unAlFlags | DT_WORDBREAK | DT_CALCRECT);

	// Modify the caption rectagle according to the new coordinates
	rcNewRect.top = rcRect.top + (rcRect.Height() - rcSavedRect.Height()) / 2;
	//rcNewRect.bottom = rcRect.top + rcSavedRect.Height();

	HRGN hrgnOld = NULL;
	::GetClipRgn(pdc->m_hDC, hrgnOld);

	pdc->SelectClipRgn(&rgn, RGN_COPY);

	// Draw the caption
	pdc->DrawText(pszText, &rcNewRect, unAlFlags | DT_WORDBREAK);

	::SelectClipRgn(pdc->m_hDC, hrgnOld);

	DeleteObject(rgn);

}
UINT CiLcd::HandleDisplayData(const char* pBuf, int length)
{
	USES_CONVERSION;
	UINT unTotalParsedLen = 0;
	while (true)
	{
		CDC* pDcs =  m_pParent->GetDc();
		if (pDcs)
		{
			TCmdArray arrCmd;
			CDC& dcs = *pDcs;
			char chAck = 0x06;
			UINT unParsedLen = 0;
			for (unsigned int n = unTotalParsedLen; n < (unsigned int)length; n++)
			{
				if (n <= unTotalParsedLen || (char)0xAA != pBuf[n] || (char)0xAA != pBuf[n - 1]) // Removes extra 0xAA char when it is escaped.
				{
					if (n > unTotalParsedLen && (char)0xAA == pBuf[n] && (n + 1  == length || (char)0xAA != pBuf[n + 1]) )
					{
						// Next command
						break;
					}
					arrCmd.push_back(pBuf[n]);
				}
				else
				{
					unParsedLen++;
				}
			}

			unParsedLen += arrCmd.size();
			if (!unParsedLen)
			{
				break;
			}

			dcs.SetBkColor(color);
			dcs.SetTextColor(textColor);

			if (arrCmd.size() >= 5 && 'C' == arrCmd[1] && '?' == arrCmd[2] && 'T' == arrCmd[3] && NULL == arrCmd[arrCmd.size() - 1])
			{
				// Get Text Extent
				dcs.SelectObject(hFontActive);

				LPCSTR pszText = &arrCmd[4];

				m_pParent->SendDispRetData(&chAck, sizeof(chAck));

				WCHAR buff[5000];
				MultiByteToWideChar(1251, 0, pszText, -1, buff, 5000);
				SendTextExtent(dcs.GetTextExtent(buff, strlen(pszText)));
				chAck = 0;
			}
			else if (arrCmd.size() == 8 && 'C' == arrCmd[1] && 'T' == arrCmd[2])
			{
				//  AL_CENTER_TEXT_HORIZONTALLY			0x01
				//  AL_CENTER_TEXT_VERTICALLY			0x02
				//  AL_RIGHT_JUSTIFY_TEXT				0x04
				//  AL_BOTTOM_JUSTIFY_TEXT				0x08 
				//  AL_DO_NOT_WORD_WRAP_TEXT			0x10 
				//  AL_ADD_HORIZONTAL_SPACE_FOR_BORDER	0x20
				//  AL_ADD_VERTICAL_SPACE_FOR_BORDER	0x40
				//  AL_TURN_ALIGNMENT_ON				0x80

				unsigned char unFlags = arrCmd[3];
				if ((unFlags & AL_RIGHT_JUSTIFY_TEXT) > 0)
				{
					unAlFlags |= DT_RIGHT;
				}
				else
				{
					unAlFlags &= ~DT_RIGHT;
				}

				if ((unFlags & AL_CENTER_TEXT_HORIZONTALLY) > 0)
				{
					unAlFlags |= DT_CENTER;
				}
				if ((unFlags & AL_CENTER_TEXT_VERTICALLY) > 0)
				{
					unAlFlags |= DT_VCENTER;
				}
				if ((unFlags & AL_BOTTOM_JUSTIFY_TEXT) > 0)
				{
					unAlFlags |= DT_BOTTOM;
				}
			}
			else if (arrCmd.size() >= 3 &&  'D' == arrCmd[1] && 'T' == arrCmd[2] && NULL == arrCmd[arrCmd.size() - 1])
			{
				dcs.SelectObject(hFontActive);

				LPCSTR pszText = &arrCmd[3];

				WCHAR buff[5000];
				MultiByteToWideChar(1251, 0, pszText, -1, buff, 5000);

				unsigned int uiLen = strlen(pszText);
				for (int i = 0; i < uiLen; i++) {
					if (pszText[i] == '�')
						buff[i] = L'\x394';
					if (pszText[i] == '�')
						buff[i] = L'\x3B4';
				}
					
				CSize& s = dcs.GetTextExtent(buff, uiLen);

				CRect rectClient(port.top + rect.top, port.left + rect.left, port.top + rect.top + port.right, port.left + rect.left + port.bottom);

				if ((unAlFlags & DT_VCENTER) > 0)
				{
					DrawTextEEX(&dcs, buff, rectClient, unAlFlags);
				}
				else
				{
					dcs.DrawText(buff, -1, rectClient, unAlFlags);
				}

				//dcs.MoveTo(rectClient.left, rectClient.top);
				//dcs.LineTo(rectClient.right, rectClient.bottom);
				rect.top += s.cx;
			}
			else if (arrCmd.size() == 7 &&  'C' == arrCmd[1] && 'K' == arrCmd[2])
			{
				WORD X = ((WORD)arrCmd[3] << 8) + (unsigned char)arrCmd[4];
				WORD Y = ((WORD)arrCmd[5] << 8) + (unsigned char)arrCmd[6];
					
				rect.top = X;
				rect.left = Y;
				//dcs.MoveTo(port.top + rect.top, port.left + rect.left);
				//dcs.LineTo(100,100);
			}

			// Define view port
			else if (arrCmd.size() == 10 && 'C' == arrCmd[1] && 'V' == arrCmd[2] && 'D' == arrCmd[3])
			{	
				WORD X = ((WORD)arrCmd[6] << 8) + (unsigned char)arrCmd[7];
				WORD Y = ((WORD)arrCmd[8] << 8) + (unsigned char)arrCmd[9];

				port.bottom = Y;
				port.right = X;

				port.top = rect.top;
				port.left = rect.left;
				rect.top = 0;
				rect.left = 0;

				color = 0xFFFFFF;
				textColor = 0x00;
				hFontActive = hFont1;
				unAlFlags = 0;
			}
			else if (arrCmd.size() == 5 && 'C' == arrCmd[1] && 'V' == arrCmd[2] && 'S' == arrCmd[3])
			{	
				if (0 == arrCmd[4])
				{
					port.top = 0;
					port.left = 0;
				}
			}
			else if (arrCmd.size() >= 4 && 'A' == arrCmd[1] && 'C' == arrCmd[2] && 'F' == arrCmd[3])
			{	
				textColor = (RGB((unsigned char)arrCmd[4],(unsigned char)arrCmd[5],(unsigned char)arrCmd[6]));
			}
			else if (arrCmd.size() == 7 && 'A' == arrCmd[1] && 'C' == arrCmd[2] && 'B' == arrCmd[3])
			{	
				color = (RGB((unsigned char)arrCmd[4],(unsigned char)arrCmd[5],(unsigned char)arrCmd[6]));
			}
			else if (arrCmd.size() >= 3 && 'D' == arrCmd[1] && 'E' == arrCmd[2])
			{	
				dcs.FillSolidRect(0,0, 800,480, color);
			}
			else if (arrCmd.size() >= 7 && 'D' == arrCmd[1] && 'e' == arrCmd[2])
			{	
				WORD DX = ((WORD)arrCmd[3] << 8) + (unsigned char)arrCmd[4];
				WORD DY = ((WORD)arrCmd[5] << 8) + (unsigned char)arrCmd[6];

				dcs.FillSolidRect(rect.top + port.top, rect.left + port.left, DX, DY, color);
			}
			else if (arrCmd.size() >= 3 && 'D' == arrCmd[1] && 'i' == arrCmd[2])
			{
				WORD DX = ((WORD)arrCmd[3] << 8) + (unsigned char)arrCmd[4];
				WORD DY = ((WORD)arrCmd[5] << 8) + (unsigned char)arrCmd[6];

				CRect rectClient(port.top + rect.top, port.left + rect.left, port.top + rect.top + DX,  port.left + rect.left + DY);

				dcs.InvertRect(rectClient);
			}
			else if (arrCmd.size() >= 3 && 'D' == arrCmd[1] && 'R' == arrCmd[2])
			{
				unsigned char unFlags = arrCmd[3];
				WORD DX = ((WORD)arrCmd[4] << 8) + (unsigned char)arrCmd[5];
				WORD DY = ((WORD)arrCmd[6] << 8) + (unsigned char)arrCmd[7];

				CRect rectClient(port.top + rect.top, port.left + rect.left, port.top + rect.top + DX,  port.left + rect.left + DY);
				CBrush br(color);

				if ((unFlags & 0x08) > 0)
				{
					dcs.FillRect(rectClient, &br);
				}

				dcs.Draw3dRect(rect.top + port.top, rect.left + port.left, DX, DY, textColor, textColor);
			}
			else if (arrCmd.size() >= 3 && 'A' == arrCmd[1] && 'F' == arrCmd[2])
			{
				WORD font = ((WORD)arrCmd[3] << 8) + (unsigned char)arrCmd[4];

				if (3 == font)
				{
					hFontActive = hFont2;
				}
				else if (2 == font)
				{
					hFontActive = hFont3;
				}
				else
				{
					hFontActive = hFont1;
				}
			}
			// Unused but known comands
			else if (arrCmd.size() == 4 && 'C' == arrCmd[1] && 'F' == arrCmd[2]) {}
			else if (arrCmd.size() == 8 && 'C' == arrCmd[1] && 'T' == arrCmd[2]) {}
			else if (arrCmd.size() == 5 && 'M' == arrCmd[1] && 'S' == arrCmd[2] && 'S' == arrCmd[3]) {}
			else if (arrCmd.size() == 2 && '!' == arrCmd[1]) {}
			else if (arrCmd.size() == 5 && 'T' == arrCmd[1] && 'W' == arrCmd[2]) {}
			else if (arrCmd.size() == 5 && 'T' == arrCmd[1] && 'H' == arrCmd[2]) {}
			else if (arrCmd.size() == 5 && 'T' == arrCmd[1] && 'A' == arrCmd[2]) {}
			else if (arrCmd.size() == 4 && 'T' == arrCmd[1] && 'K' == arrCmd[2]) {}
			else if (arrCmd.size() == 4 && 'T' == arrCmd[1] && 'O' == arrCmd[2]) {}
			else if (arrCmd.size() == 4 && 'M' == arrCmd[1] && 'D' == arrCmd[2]) {}
			else if (arrCmd.size() == 4 && 'M' == arrCmd[1] && 'V' == arrCmd[2]) {}

			else
			{
				break;
			}

			m_pParent->SetInvalid();
			unTotalParsedLen += unParsedLen;

			if (chAck)
			{
				m_pParent->SendDispRetData(&chAck, sizeof(chAck));
			}

			GdiFlush(); // Required when works from another thread
		}
	}

	return unTotalParsedLen;
}