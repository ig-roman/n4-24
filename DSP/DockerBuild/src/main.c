#include "../../CCS/LogicInterface.h"
#include "../../CCS/UIInterface.h"
#include "../../AARM/ArmInterface.h"
#include <string.h>
#include <malloc.h>

//#include "Common.h"
//#include "UI.h"
//#include "UIInterface.h"
//#include "LogicInterface.h"
//#include "SerialCmdParser.h"

void CUI_StartDisplayData(char chStartSeq) {}
void CUI_SendDisplayData(const char* pBuf, int length) {}
char CUI_ReadDisplayData(char* pBuf, unsigned int* pnBuffDataLen) { return 0; }
char CUI_WaitDisplayACK() { return 0; }
void CUI_GetTextExtent(uint16_t* punX, uint16_t* punY) {}
void CUI_SetLastError(char* e) {}
const char* CUI_PostInit(INITSTEP eInitStep, void* pCtx) { return NULL; }

void CLogic_ManageSerial() {}
void CLogic_StoreBytes(uint32_t unStartAddress, uint32_t unLength, const uint8_t* byData) {}
void CLogic_RestoreBytes(uint32_t unStartAddress, uint32_t unLength, uint8_t* pbyData) {}
void CLogic_StoreByte(uint32_t unAddress, uint8_t byData) {}
uint8_t CLogic_RestoreByte(uint32_t unAddress) {}
bool CLogic_SwitchOut(char bOn, char bHV, char bHVConfirmRequire, char bForce) {return true;}
uint32_t GetTime(){return 0;}
void CLogic_SendRemoteCtrlData(const char* pBuf, int length){}
void CLogic_SendControlData(const char* pBuf, int length){}
void CLogic_SetLastError(const char* pszError){}

void ReportError(const char *err){};

void CARM_LLProg_Status(char bStart) {}
char CARM_GetOverloadStatus() {}
AERROR CARM_ClearLastError() {}
void CARM_SendCommandString(const char* pBuf, unsigned int length) {}
void CARM_SetLastError(AERROR unError) {}
void CARM_SPI_Start(unsigned char unAddr)  {}
void CARM_SPI_Send(char byByte) {}
void CARM_SPI_Stop() {}
void CARM_SetPWMValue(unsigned long unPwm) {}
bool CARM_DelayMs(unsigned long unDelay, bool bSkipLoop, bool bCheckOverload) {}

int main(void)
{
	printf("Starting\n");

	Logic_Init();	// C++ logic
	UI_Init();      // UI logic

	while (true)
	{
		Logic_Loop();
		ARM_Loop();
	}
}