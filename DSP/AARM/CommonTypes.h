#ifndef COMMONCTARM_H_
#define COMMONCTARM_H_

#include "../TopCommon.h"
#include <string.h>

typedef enum 		{ PROG_CUSTOM, PROG_INIT, PROG_FULLSTBY, PROG_DC10MV100V, PROG_DC1000V, PROG_AC10V, PROG_AC100V, PROG_AC1000V, PROG_ACI, PROG_DCI, PROG_R, PROG_UNKNOWN } PROG; // See m_arrProgsPool

#define UNKNOWN_DOUBLE 100000000.0
#define UNKNOWN_VALUE UNKNOWN_DOUBLE
#define UNKNOWN_FREQUENCY UNKNOWN_DOUBLE
#define UNKNOWN_BAND 0xFF

typedef enum 		{ PRGPHASE_UNKNOWN, PRGPHASE_LOAD, PRGPHASE_ON, PRGPHASE_OFF, PRGPHASE_UNLOAD } PRGPHASE;

class COMMAND
{
public:
	STATEMODE		mode;
	STATESENSE		sense;
	double			value;
	double			frequency;
	uint8_t			bandIdx;
	uint8_t			bandIdxFreq;
	STATEOUT		outState;
	uint8_t*		pCustomComand;
	uint8_t			unCustomComandLen;

	COMMAND() : pCustomComand(NULL)
	{
		ResetCommand();
	}

	COMMAND(const COMMAND &command) : pCustomComand(NULL)
	{
		mode 				= command.mode;
		sense				= command.sense;
		value				= command.value;
		frequency			= command.frequency;
		bandIdx				= command.bandIdx;
		bandIdxFreq			= command.bandIdxFreq;
		outState			= command.outState;

		AssignCustomCommand(command.pCustomComand, command.unCustomComandLen);
	}

	virtual ~COMMAND()
	{
		AssignCustomCommand(NULL, 0);
	}

	void ResetCommand()
	{
		bandIdx				= UNKNOWN_BAND;
		bandIdxFreq			= UNKNOWN_BAND;
		frequency			= UNKNOWN_FREQUENCY;
		mode				= STATEMODE_UNKNOWN;
		outState			= STATEOUT_UNKNOWN;
		sense				= STATESENSE_UNKNOWN;
		value				= UNKNOWN_VALUE;

		AssignCustomCommand(NULL, 0);
	}

	void AssignCustomCommand(const uint8_t* pNewCustomCommand, uint8_t unNewCustomComandLen)
	{
		unCustomComandLen = unNewCustomComandLen;
		if (NULL != pCustomComand)
		{
			delete pCustomComand;
			pCustomComand = NULL;
		}

		if (pNewCustomCommand && unNewCustomComandLen > 0)
		{
			pCustomComand = new uint8_t[unNewCustomComandLen];
			memcpy(pCustomComand, pNewCustomCommand, unNewCustomComandLen);
		}
	}

	void CompleteCommand(COMMAND* pCommand) const
	{
		if (STATEMODE_UNKNOWN	!= mode)			pCommand->mode			= mode;
		if (STATESENSE_UNKNOWN	!= sense)			pCommand->sense			= sense;
		if (UNKNOWN_VALUE		!= value)			pCommand->value			= value;
		if (UNKNOWN_FREQUENCY	!= frequency)		pCommand->frequency		= frequency;
		if (UNKNOWN_BAND		!= bandIdx)			pCommand->bandIdx		= bandIdx;
		if (UNKNOWN_BAND		!= bandIdxFreq)		pCommand->bandIdxFreq	= bandIdxFreq;
		if (STATEOUT_UNKNOWN	!= outState)		pCommand->outState		= outState;

		// Intentionally force copy of pCustomComand should be done. Beacuse it disables cache for custom commands.
		pCommand->AssignCustomCommand(pCustomComand, unCustomComandLen);
	}

};

typedef COMMAND* PCOMMAND;
typedef const COMMAND* PCCOMMAND;

#endif