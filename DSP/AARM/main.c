#include "main.h"

#include <string.h>
#include "drivers/board/board.h"

#include "ArmInterface.h"
#include "drivers/pwmc/pwmc.h"
#include "drivers/spi/spi.h"
#include "drivers/usart/dbgu.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////

//#define N55TESTBOARD

///////////////////////////////////////////////////////////////////////////////////////////////////////////
PORTNAME gs_lastActivePort = PORTNAME_TEXAS;
uint32_t g_unRunLedOffTime = 0;
AERROR s_unError = AERROR_NO_ERROR;

USART_Conf Usart0Conf = USART0_CONF_DEFAULT;
USART_Handle Usart0;

#define USART0_RX_BUF_SIZE 512
char Usart0RxBuf[USART0_RX_BUF_SIZE] = {0};
#define USART0_TX_BUF_SIZE 512
char Usart0TxBuf[USART0_TX_BUF_SIZE] = {0};

void Usart0IrqHandler(void)
{
    USART_irqHandler(Usart0);
}

/** **********************************************************************
 *
 * Is invoked when system has an error.
 * Enables red led on AARM controller and stores error code to report it in future
 *
 * @param error code to report
 *
 *********************************************************************** */
void SetLastAARMError(AERROR unError)
{
	s_unError = unError;
	ERROR_LED_ON;
}

/** **********************************************************************
 *
 * Returns last error and clears it
 * @retgurn last error
 *
 *********************************************************************** */
AERROR CARM_ClearLastError()
{
	AERROR unError = s_unError;
	s_unError = AERROR_NO_ERROR;
	return unError;
}

/** **********************************************************************
 *
 * Is invoked when c++ part of system has an error.
 * @param error code to report
 *
 *********************************************************************** */
void CARM_SetLastError(AERROR unError)
{
	SetLastAARMError(unError);
}

/** **********************************************************************
 *
 * Is invoked when shared part of system has an error (circular buffer).
 * Enables red led on AARM controller
 *
 * @param error text to log
 *
 *********************************************************************** */
void ReportError(const char *err)
{
	SetLastAARMError(AERROR_UNKNOWN);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Init code for USB by Atmel
#define USB_MSG_SIZE 				100
#define USART0_BUFFER_SIZE			1024 // ST TODO: change to 100?

struct _AT91S_CDC 	pCDC;
char USART0_Buffer[USART0_BUFFER_SIZE];

//*----------------------------------------------------------------------------
//* \fn    AT91F_USB_Open
//* \brief This function Open the USB device
//*----------------------------------------------------------------------------
void AT91F_USB_Open(void)
{
    // Set the PLL USB Divider
    AT91C_BASE_CKGR->CKGR_PLLR |= AT91C_CKGR_USBDIV_1 ;

    // Specific Chip USB Initialisation
    // Enables the 48MHz USB clock UDPCK and System Peripheral USB Clock
    AT91C_BASE_PMC->PMC_SCER = AT91C_PMC_UDP;
    AT91C_BASE_PMC->PMC_PCER = (1 << AT91C_ID_UDP);
#ifdef N55TESTBOARD
    // Enable UDP PullUp (USB_DP_PUP) : enable & Clear of the corresponding PIO
    // Set in PIO mode and Configure in Output
    AT91F_PIO_CfgOutput(AT91C_BASE_PIOA,AT91C_PIO_PA16);
    // Clear for set the Pul up resistor
    AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PIO_PA16);
#endif
    // CDC Open by structure initialization
    AT91F_CDC_Open(&pCDC, AT91C_BASE_UDP);
}

bool TIMER_delay(unsigned long ms, bool bSkipLoop, bool bCheckOverload)
{
	unsigned int i = *AT91C_TC0_SR; // clears the AT91C_TC_CPCS bit
	bool bRet = true;
	
    *AT91C_TC0_CCR = AT91C_TC_SWTRG;
    for (i=0; i < ms && bRet; i++)
	{
        while (bRet && !((*AT91C_TC0_SR) & AT91C_TC_CPCS));
		if (!bSkipLoop)
		{
			Loop();
		}
		bRet = !bCheckOverload || !CARM_GetOverloadStatus();
	}
	
	return bRet;
}

char CARM_GetOverloadStatus()
{
	return OVERLOAD_GET != 0;
}

void CARM_LLProg_Status(char bStart)
{
	if (bStart)
	{
		g_unRunLedOffTime = 0;
		RUN_LED_ON;
	}
	else
	{
		g_unRunLedOffTime = GetTime() + MIN_RUN_LED_BLINK_TIME;
	}
}

void Abort_Handler()
{
	// Critical error.
	while(1)
	{
		// Fast led bliking
	 	for (unsigned long lDelay = 0; lDelay < FATAL_LOOP_BLINK_DELAY; lDelay++)
		{
			if (lDelay > (FATAL_LOOP_BLINK_DELAY / 2))
			{
				ERROR_LED_ON;
				RUN_LED_ON;
			}
			else
			{
				ERROR_LED_OFF;
				RUN_LED_OFF;
			}
		}
	}
}

/** **********************************************************************
 *
 * Starts transission to SPI port.
 *
 * @param unAddr address of SPI line (CS)
 *
 *********************************************************************** */
void CARM_SPI_Start(unsigned char unAddr)	{ SPI_Start(unAddr - 1); } // -1 because Q0 of decoder connected to CS1 line

/** **********************************************************************
 *
 * Sends data to SPI port
 *
 * @param byByte data to be transfered
 *
 *********************************************************************** */
void CARM_SPI_Send(char byByte)				{ SPI_Swap(byByte); }

/** **********************************************************************
 *
 * Stops SPI port
 *
 *********************************************************************** */
void CARM_SPI_Stop()						{ SPI_Stop(); }

/** **********************************************************************
 *
 * Changes PWM value
 * @param unPwm new value of PWM modules
 *
 *********************************************************************** */
void CARM_SetPWMValue(unsigned long unPwm)
{
	unsigned short pwm1 = unPwm >> 16;
	unsigned short pwm2 = unPwm;
	PWM_Change(AT91C_PWMC_CHID0, pwm1);
	PWM_Change(AT91C_PWMC_CHID1, pwm2);
}

/** **********************************************************************
 *
 * Waits defined time.
 *
 * @param unDelay			delay time in ms.
 * @param bSkipLoop			when value is ture fuction pervents any actions in main loop it means that MCU don't read any commands from serial port
 * @param bCheckOverload	checks overload signal and stop waiting when overload is detected.
 *
 * @return false when bCheckOverload is true and overload is detected and loop has been terminated
 *
 *********************************************************************** */
bool CARM_DelayMs(unsigned long unDelay, bool bSkipLoop, bool bCheckOverload)
{
	return TIMER_delay(unDelay, bSkipLoop, bCheckOverload);
}

/** **********************************************************************
 *
 * Extends reset time. (resets wachdog timer)
 *
 *********************************************************************** */
void KikDog()
{
	static char sbIsSet = 0;
	if (sbIsSet)
	{
		AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, AT91C_PIO_PA25);
	}
	else
	{
		AT91F_PIO_SetOutput(AT91C_BASE_PIOA, AT91C_PIO_PA25);
	}
	sbIsSet = !sbIsSet;
}

/** **********************************************************************
 *
 * Main function executes once on startup
 *
 *********************************************************************** */
int main()
{
    BOARD_init();

    // Set up USART0
    Usart0Conf.handler = Usart0IrqHandler;
    Usart0Conf.rxBufMem = &Usart0RxBuf[0];
    Usart0Conf.rxBufMemSize = USART0_RX_BUF_SIZE;
    Usart0Conf.txBufMem = &Usart0TxBuf[0];
    Usart0Conf.txBufMemSize = USART0_TX_BUF_SIZE;
    Usart0 = USART_conf(&Usart0Conf);
	
    // HW watchdog Set in PIO mode and Configure in Output
    // N.B! PA25 is used by UART pin AT91C_PA25_CTS1 terefore this line shold be after uart configuration
    AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, AT91C_PIO_PA25);
	
    PWM_Init(AT91C_PWMC_CHID0, AT91C_PA0_PWM0);
    PWM_Init(AT91C_PWMC_CHID1, AT91C_PA1_PWM1);
	
    SPI_Init();
    ARM_Init();
    
    ERROR_LED_INIT;
    RUN_LED_INIT;
    OVERLOAD_INIT;
#ifdef N55TESTBOARD
    AT91F_PIO_CfgInput(AT91C_BASE_PIOA, AT91C_PIO_PA0 | AT91C_PIO_PA1 | AT91C_PIO_PA2 | AT91C_PIO_PA3 | AT91C_PIO_PA3 | AT91C_PIO_PA5 | AT91C_PIO_PA6 | AT91C_PIO_PA13 | AT91C_PIO_PA14 | AT91C_PIO_PA17 | AT91C_PIO_PA18 | AT91C_PIO_PA19 | AT91C_PIO_PA20 | AT91C_PIO_PA23 | AT91C_PIO_PA25);
#endif 
    // Init USB device
    AT91F_USB_Open();
    while(1)
    {
        // Execution logic of pendings commands.
        ARM_Loop();
		
        // Handele infinity loop.
        Loop();
    }
}

/** **********************************************************************
 *
 * Main loop function handles all system events
 *
 *********************************************************************** */
void Loop()
{
	// Clear the busy led after some time
	if (g_unRunLedOffTime && CheckUpdateTime(MIN_RUN_LED_BLINK_TIME, &g_unRunLedOffTime))
	{
		g_unRunLedOffTime = 0;
		RUN_LED_OFF;
	}
	
	// Clear the error led if error code has been sent to Texas
	if (AERROR_NO_ERROR == s_unError)
	{
		ERROR_LED_OFF;
	}
	
	KikDog();
	if (pCDC.IsConfigured(&pCDC))
	{
		char arrUsbData[USB_MSG_SIZE];
		unsigned int unUsbLength = pCDC.Read(&pCDC, arrUsbData, USB_MSG_SIZE);
		if(unUsbLength)
		{
			gs_lastActivePort = PORTNAME_END;
			ARM_HandleCommandString(arrUsbData, unUsbLength);
		}
	}
	
	char nByte;
	int nLen = 0;
	if(USART_pollChar(Usart0, &nByte))
	{
		do
		{
			USART0_Buffer[nLen] = nByte;
			nLen++;
			if (!USART_pollChar(Usart0, &nByte))
			{
				uint16_t unRetryCount = 50; // ST TODO: Send pakage len from texas? Found by experemental. 20 leads to communications errors, 25 works stable, 50 to be shure that all bytes recieved in packege.
				while (!USART_pollChar(Usart0, &nByte) && unRetryCount != 0)
				{
					unRetryCount--;
				}
				if (unRetryCount == 0)
				{
					break;
				}
			}
		}
		while (1);
		gs_lastActivePort = PORTNAME_TEXAS;
		ARM_HandleCommandString(USART0_Buffer, nLen);
	 }
}

/** **********************************************************************
 *
 * Sends command string to Texas CPU or PC with emulation
 *
 * @param pBuf		data to send
 * @param length	length of data
 *
 *********************************************************************** */
void CARM_SendCommandString(const char* pBuf, unsigned int length)
{
	if (PORTNAME_END == gs_lastActivePort && pCDC.IsConfigured(&pCDC))
	{
		pCDC.Write(&pCDC, pBuf, length);
	}
	else
	{
		for (int n = 0; n < length; n++)
		{
			USART_putChar(Usart0, pBuf[n]);
		}
	}
}
