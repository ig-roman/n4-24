
#ifdef __cplusplus
extern "C" {
#endif

#if !defined(WIN32)
	#define __declspec(dllexport)
	#define __stdcall
#endif

#include "../TopCommon.h"

// From C to C++ layer
__declspec(dllexport) void __stdcall ARM_Init();
__declspec(dllexport) void __stdcall ARM_Loop();
__declspec(dllexport) void __stdcall ARM_HandleCommandString(const char* pBuf, unsigned int length);

// From C++ to C layer
__declspec(dllexport) void __stdcall CARM_SendCommandString(const char* pBuf, unsigned int length);
__declspec(dllexport) bool __stdcall CARM_GetOverloadStatus();
__declspec(dllexport) void __stdcall CARM_SetLastError(AERROR unError);
__declspec(dllexport) AERROR __stdcall CARM_ClearLastError();

__declspec(dllexport) void __stdcall CARM_SPI_Start(unsigned char unAddr);
__declspec(dllexport) void __stdcall CARM_SPI_Send(char byByte);
__declspec(dllexport) void __stdcall CARM_SPI_Stop();

__declspec(dllexport) void __stdcall CARM_SetPWMValue(unsigned long unPwm);
__declspec(dllexport) bool __stdcall CARM_DelayMs(unsigned long unDelay, bool bSkipLoop, bool bCheckOverload);

__declspec(dllexport) void __stdcall CARM_LLProg_Status(char bStart);

#ifdef __cplusplus
}
#endif
