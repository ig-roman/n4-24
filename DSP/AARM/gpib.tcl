set sourceFile	"GPIB.bin"

proc UnlockSector { first_lockregion last_lockregion } {
    global   target
    set dummy_err 0

    # Mailbox is 32 word long (add variable here if you need read/write more data)
    set appletAddrCmd       [expr $GENERIC::appletMailboxAddr]
    set appletAddrArgv0     [expr $GENERIC::appletMailboxAddr + 0x08]

    # Unlock all sectors involved in the write    
    for {set i $first_lockregion} {$i <= $last_lockregion } {incr i} {
        # Write the Cmd op code in the argument area
        if {[catch {TCL_Write_Int $target(handle) $GENERIC::appletCmd(unlock) $appletAddrCmd} dummy_err] } {
            error "Error Writing Applet command ($dummy_err)"
        }   
        
        # Write the page number in the argument area
        if {[catch {TCL_Write_Int $target(handle) $i $appletAddrArgv0} dummy_err] } {
            error "Error Writing Applet page \n$dummy_err"
        }
        # Launch the applet Jumping to the appletAddr
        if {[catch {set result [GENERIC::Run $GENERIC::appletCmd(unlock)]} dummy_err]} {
            error "Applet unlock command has not been launched ($dummy_err)"
        }
        puts "-I- Sector $i unlocked"
    }

    return 1
}


proc LockSector { first_lockregion last_lockregion } {
    global   target
    set dummy_err 0

    # Mailbox is 32 word long (add variable here if you need read/write more data)
    set appletAddrCmd       [expr $GENERIC::appletMailboxAddr]
    set appletAddrArgv0     [expr $GENERIC::appletMailboxAddr + 0x08]

    # Lock all sectors involved in the write
    for {set i $first_lockregion} {$i <= $last_lockregion } {incr i}  {
        # Write the Cmd op code in the argument area
        if {[catch {TCL_Write_Int $target(handle) $GENERIC::appletCmd(lock) $appletAddrCmd} dummy_err] } {
            error "Error Writing Applet command ($dummy_err)"
        }   
        # Write the page number in the argument area
        if {[catch {TCL_Write_Int $target(handle) $i $appletAddrArgv0} dummy_err] } {
            error "Error Writing Applet page \n$dummy_err"
        }
        # Launch the applet Jumping to the appletAddr
        if {[catch {set result [GENERIC::Run $GENERIC::appletCmd(lock)]} dummy_err]} {
            error "Applet unlock command has not been launched ($dummy_err)"
        }
        puts "-I- Sector $i locked"
    }

    return 1
}

puts "-I- === Initialize the access ==="
FLASH::Init

UnlockSector 0 [expr $FLASH::flashNumbersLockBits - 1]

puts "-I- === Erase all the flash blocks and test the erasing ==="
FLASH::EraseAll

puts "-I- === Load the example image ==="
send_file {Flash} "$sourceFile" 0x00100000 0

LockSector 0 [expr $FLASH::flashNumbersLockBits - 1]

puts "-I- === Compare the example image ==="
compare_file  {Flash} "$sourceFile" 0x00100000 0


puts "-I------------------------------------------------------"
puts "-I- Flash script file correctly executed !"
puts "-I------------------------------------------------------"


