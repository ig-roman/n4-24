#include "../board/board.h"
#include "pwmc.h"
#include "../../../TopCommon.h"

#define MAX_HW_DIVISORS 11

/** **********************************************************************
 *
 * Remaps software (from API) PWM channel to hardware (to registers)
 * @param unSwCh		software channel AT91C_PWMC_CHID0 or AT91C_PWMC_CHID1
 * @return 				return hardware channel
 *
 *********************************************************************** */
unsigned int MapPwmHwChannel(unsigned int unSwCh)
{
	switch(unSwCh)
	{
		case AT91C_PWMC_CHID0: return 0; break;
		case AT91C_PWMC_CHID1: return 1; break;
	}
	return 0;
}

/** **********************************************************************
 *
 * Finds a prescaler and divisor value to generate the desired frequency of PWM
 * @param frequency		desired frequency of PWM
 * @return 		        prescaler + divisor value for PWMC_MR register
 *
 *********************************************************************** */
unsigned short PWM_GetClockConfiguration(unsigned int frequency)
{
	unsigned short unDivisor = 0;
	unsigned long prescaler = (AT91B_MCK / 2 / (1 << unDivisor)) / frequency;
	while (prescaler > 255 && unDivisor < MAX_HW_DIVISORS)
	{
		unDivisor++;
		prescaler = (AT91B_MCK / 2 / (1 << unDivisor)) / frequency;
	}
	
	return prescaler | (unDivisor << 8);
}

/** **********************************************************************
 *
 * Initializes PWM in micro controller
 * @param unPwmSwChanel	software channel AT91C_PWMC_CHID0 or AT91C_PWMC_CHID1 only
 * @param unPwmPin		oputput pin for PWN
 * @return 			none
 *
 *********************************************************************** */
void PWM_Init(unsigned int unPwmSwChanel, unsigned int unPwmPin)
{
	// Configure PWM clocks
        AT91C_BASE_PWMC->PWMC_MR = 0x201; // MCK / 4
        //PWM_GetClockConfiguration(PWM_CLOCK_FREQ * PWM_MAX_VAL);
	
 	//Configure Peripherials for PWM outputs
	AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA, unPwmPin, 0); // mux function A

	AT91F_PWMC_CfgPMC();
	
	// Don't use the AT91F_PWMC_CH1_CfgPIO(); function because it conflicts with SPI by AT91C_PIO_PA23 pin	
	AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA, unPwmPin, 0);

	AT91F_PWMC_InterruptDisable(AT91C_BASE_PWMC, unPwmSwChanel);  //Disable PWM Interrupt
    AT91F_PWMC_CfgChannel(AT91C_BASE_PWMC, MapPwmHwChannel(unPwmSwChanel), (AT91C_PWMC_CPRE_MCKA | AT91C_PWMC_CALG | AT91C_PWMC_CPD | AT91C_PWMC_CPOL), PWM_MAX_VAL, 1);
    AT91F_PWMC_StartChannel(AT91C_BASE_PWMC, unPwmSwChanel);      //Enable channel
}

/** **********************************************************************
 *
 * Changes duty cycle of PWM
 *
 * @param unPwmSwChanel	software channel AT91C_PWMC_CHID0 or AT91C_PWMC_CHID1 only
 * @param unNewDuty		new duty cycle for selected PWM. To define 100% value should be equal PWM_MAX_VAL
 *
 *********************************************************************** */
void PWM_Change(unsigned int unPwmSwChanel, unsigned int unNewDuty)
{
	unsigned int unHwChanel = MapPwmHwChannel(unPwmSwChanel);
	unsigned int unPwmPin = unHwChanel ? AT91C_PA1_PWM1 : AT91C_PA0_PWM0;

	// PWM controller can't adjust via update register when Duty = 0, 1 or MAX
	//
	// Implemented workarounds:
	// 1. when value is 0 or MAX function softly disables PWM controller and reconfigures pin as output then sets required value. 0 = 0, 1 = MAX
	bool bIsPwmActive = (AT91F_PWMC_GetStatus(AT91C_BASE_PWMC) & unPwmSwChanel) != 0;
	if (0 == unNewDuty || PWM_MAX_VAL == unNewDuty)
	{
		// Define output value when pin is not configured
		if (PWM_MAX_VAL == unNewDuty)
		{
			AT91F_PIO_SetOutput(AT91C_BASE_PIOA, unPwmPin);
		}
		else
		{
			AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, unPwmPin);
		}
		
		// Atomic switch to defined value
		AT91F_PIO_CfgOutput(AT91C_BASE_PIOA, unPwmPin);
		
		if (bIsPwmActive)
		{
			AT91F_PWMC_StopChannel(AT91C_BASE_PWMC, unPwmSwChanel);
		}
	}
	else
	{
		AT91C_BASE_PWMC->PWMC_CH[unHwChanel].PWMC_CMR &= ~AT91C_PWMC_CPD;
		AT91F_PWMC_UpdateChannel(AT91C_BASE_PWMC, unHwChanel, unNewDuty);
		
		if (!bIsPwmActive)
		{
			AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, unPwmPin);
		  	AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA, unPwmPin, 0);
			AT91F_PWMC_StartChannel(AT91C_BASE_PWMC, unPwmSwChanel);
		}
	}
}
