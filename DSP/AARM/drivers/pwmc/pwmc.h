#ifndef PWMC_H
#define PWMC_H

void PWM_Init(unsigned int unPwmSwChanel, unsigned int unPwmPin);
void PWM_Change(unsigned int unPwmSwChanel, unsigned int unNewDuty);

#endif
