/*
 * File:    usart.c
 * Title:
 * Created: Wed Sep 06 10:11:07 2006
 *
 */
#include <string.h>
#include "../board/board.h"
#include "../../../TopCommon.h"

bool TIMER_delay(unsigned long unDelay, bool bSkipLoop, bool bCheckOverload);

int USART_collectString(USART_Handle usart, char *s)
{
    char c;
    int result = 0;

    while (USART_pollChar(usart, &c) != 0)
    {
        if ((c == '\n') || (c == '\r'))
        {
            result = strlen(s);
            break;
        }
        else
        {
            result = strlen(s);
            s[result] = c;
            s[result+1] = '\0';
            result = 0;
        }
    }
    return result;
}

void USART_getString(USART_Handle usart, char *s, int len)
{
    int curLn = 0;

    s[0] = 0;
    TIMER_delay(5, true, false);
    while (USART_collectString(usart, s) == 0)
    {
        if (curLn == strlen(s))
        {
            s[curLn] = '\0';
            break;
        }
        else
            curLn = strlen(s);
        if (curLn >= len)
        {
            s[len] = '\0';
            break;
        }
        TIMER_delay(5, true, false);
    }
}

void USART_putString(USART_Handle usart, char *s)
{
char c;

    AT91F_US_DisableIt(usart->base, AT91C_US_TXRDY);

    while(c = *s) {
         if(c == '\n') {
             c = 13;
             CIRC_BUF_put(&(usart->txBuf), &c, 1);
             c = 10;
             CIRC_BUF_put(&(usart->txBuf), &c, 1);
         } else {
             CIRC_BUF_put(&(usart->txBuf), &c, 1);
         }
         s++;
    }

    AT91F_US_EnableIt(usart->base, AT91C_US_TXRDY);
}

void USART_putChar(USART_Handle usart, char c)
{
    AT91F_US_DisableIt(usart->base, AT91C_US_TXRDY);

    CIRC_BUF_put(&(usart->txBuf), &c, 1);

    AT91F_US_EnableIt(usart->base, AT91C_US_TXRDY);
}

char USART_getChar(USART_Handle usart)
{
char c = 0;
int i = 0;

    while(USART_pollChar(usart, &c) == 0) {
        if(i++ > 1000000) {
            c = 0;
            break;
        }
    }

    return c;
}

int USART_pollChar(USART_Handle usart, char *c)
{
int status;

    AT91F_US_DisableIt(usart->base, AT91C_US_RXRDY);

    if(CIRC_BUF_get(&(usart->rxBuf), c, 1, true)) {
        status = 1;
    } else {
        status = 0;
    }

    AT91F_US_EnableIt(usart->base, AT91C_US_RXRDY);

    return status;
}

void USART_irqHandler(USART_Handle usart)
{
char c;
unsigned int status;

    // get Usart status register
    status = usart->base->US_CSR;

    if(status & AT91C_US_RXRDY) {
        // Get byte
        c = AT91F_US_GetChar(usart->base);
        CIRC_BUF_put(&(usart->rxBuf), &c, 1);
    }

    if(status & AT91C_US_TXRDY) {
        if(CIRC_BUF_get(&(usart->txBuf), &c, 1, true)) {
            AT91F_US_PutChar(usart->base, c);
        } else {
            AT91F_US_DisableIt(usart->base, AT91C_US_TXRDY);
        }
    }

    if((status & AT91C_US_PARE) || (status & AT91C_US_FRAME) || (status & AT91C_US_OVRE)) {
        // Reset the satus bit
        usart->base->US_CR = AT91C_US_RSTSTA;
        // clear US_RXRDY
        AT91F_US_GetChar(usart->base);
    }

    if(status & AT91C_US_TIMEOUT) {
        usart->base->US_CR = AT91C_US_STTTO;
    }
}

USART_Handle USART_conf(USART_Conf *conf)
{
    if(conf->handler == 0) {
        return 0;
    }

    if (conf->id == AT91C_ID_US0) {
        AT91F_US0_CfgPMC();
        AT91F_US0_CfgPIO();
    } else if (conf->id == AT91C_ID_US1)
    {
        AT91F_US1_CfgPMC();
        AT91F_US1_CfgPIO();
    }
    else
    {
        return 0;
    }

    // First, enable the clock of the USART
    AT91F_PMC_EnablePeriphClock(AT91C_BASE_PMC, 1 << conf->id);

    // Usart Configure
    AT91F_US_Configure(conf->usart.base, conf->mainClock, conf->mode, conf->baudRate, 0);

    // Enable usart
    conf->usart.base->US_CR = AT91C_US_RXEN | AT91C_US_TXEN;

    // Enable USART IT error and RXRDY
    AT91F_US_EnableIt(conf->usart.base,
                      AT91C_US_TIMEOUT | AT91C_US_FRAME | AT91C_US_OVRE | AT91C_US_PARE |
                      AT91C_US_RXRDY);

    // open Usart  interrupt
    AT91F_AIC_ConfigureIt(AT91C_BASE_AIC, conf->id, conf->interruptLevel, AT91C_AIC_SRCTYPE_INT_HIGH_LEVEL, conf->handler);
    AT91F_AIC_EnableIt(AT91C_BASE_AIC, conf->id);

    CIRC_BUF_init(&(conf->usart.rxBuf), conf->rxBufMem, conf->rxBufMemSize);
    CIRC_BUF_init(&(conf->usart.txBuf), conf->txBufMem, conf->txBufMemSize);

    return &(conf->usart);
}
