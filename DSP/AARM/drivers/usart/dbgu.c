/*
 * File:    dbgu.c
 * Title:
 * Created: Mon Sep 04 14:55:05 2006
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "../board/board.h"
#include "../../../circ_buf.h"
#include "../../../TopCommon.h"

#define DBGU_PRINTF_ENA     // Enable DBGU_printf() usage

#define DEF_DBGU_CR (AT91C_US_RXEN | AT91C_US_TXEN)
#define DEF_DBGU_MR (AT91C_US_PAR_NONE | AT91C_US_CHMODE_NORMAL)

/*
 * BRGR
 * CD         - Baud Rate Clock
 * 0          - disabled
 * 1          - MCK
 * 2 .. 65535 - (MCK / (CD * 16))
 */
// ST TODO: check
#define DBGU_BAUDRATE 115200 // 9600
#define DEF_DBGU_BRGR (AT91B_MCK / (DBGU_BAUDRATE * 16))

#define DBGU_RX_MEM_SIZE 512
CIRC_BUF DbguRxBuf;
char DbguRxBufMem[DBGU_RX_MEM_SIZE];

#define DBGU_TX_MEM_SIZE 512
CIRC_BUF DbguTxBuf;
char DbguTxBufMem[DBGU_TX_MEM_SIZE];

#define DBGU_BUF_SIZE  DBGU_TX_MEM_SIZE
char DBGU_Buf[DBGU_BUF_SIZE];

void DBGU_init(void)
{
    AT91F_DBGU_CfgPIO();
    AT91F_DBGU_CfgPMC();
    AT91C_BASE_DBGU->DBGU_CR = AT91C_US_RSTSTA;

    AT91C_BASE_DBGU->DBGU_CR = DEF_DBGU_CR;
    AT91C_BASE_DBGU->DBGU_MR = DEF_DBGU_MR;

    AT91C_BASE_DBGU->DBGU_BRGR = DEF_DBGU_BRGR;

    AT91C_BASE_DBGU->DBGU_IER = AT91C_US_RXRDY;

    CIRC_BUF_init(&DbguRxBuf, &DbguRxBufMem[0], DBGU_RX_MEM_SIZE);
    CIRC_BUF_init(&DbguTxBuf, &DbguTxBufMem[0], DBGU_TX_MEM_SIZE);

    //AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA, AT91C_PIO_PA9 | AT91C_PIO_PA10, 0);
}

int DBGU_printf(char *fmt, ...)
{
    int ret = 1;
#ifdef DBGU_PRINTF_ENA
    va_list ap;

    va_start(ap, fmt);

    ret = vsnprintf(DBGU_Buf, DBGU_BUF_SIZE, fmt, ap);
    DBGU_putString(DBGU_Buf);
    va_end(ap);
#endif

    return ret;

}

int AMP_printf(char *fmt, ...)
{
int ret;
va_list ap;

    va_start(ap, fmt);

    ret = vsnprintf(DBGU_Buf, DBGU_BUF_SIZE, fmt, ap);
    DBGU_putString(DBGU_Buf);
    va_end(ap);

    return ret;
}


void DBGU_putString(char *s)
{
char c;

    AT91C_BASE_DBGU->DBGU_IDR = AT91C_US_TXRDY;

    while(c = *s) {
         if(c == '\n') {
             c = 13;
             CIRC_BUF_put(&DbguTxBuf, &c, 1);
             c = 10;
             CIRC_BUF_put(&DbguTxBuf, &c, 1);
         } else {
             CIRC_BUF_put(&DbguTxBuf, &c, 1);
         }
         s++;
    }

    AT91C_BASE_DBGU->DBGU_IER = AT91C_US_TXRDY;
}

void DBGU_putChar(char c)
{
    AT91C_BASE_DBGU->DBGU_IDR = AT91C_US_TXRDY;

    CIRC_BUF_put(&DbguTxBuf, &c, 1);

    AT91C_BASE_DBGU->DBGU_IER = AT91C_US_TXRDY;
}

int DBGU_pollChar(char *c)
{
int status;

    AT91C_BASE_DBGU->DBGU_IDR = AT91C_US_RXRDY;

    if(CIRC_BUF_get(&DbguRxBuf, c, 1, true)) {
        status = 1;
    } else {
        status = 0;
    }

    AT91C_BASE_DBGU->DBGU_IER = AT91C_US_RXRDY;

    return status;
}

void DBGU_rxRdyIrq(void)
{
char c;

    c = AT91C_BASE_DBGU->DBGU_RHR;
    CIRC_BUF_put(&DbguRxBuf, &c, 1);
}

void DBGU_txRdyIrq(void)
{
char c;

    if(CIRC_BUF_get(&DbguTxBuf, &c, 1, true)) {
        AT91C_BASE_DBGU->DBGU_THR = c;
    } else {
        AT91C_BASE_DBGU->DBGU_IDR = AT91C_US_TXRDY;
    }
}
