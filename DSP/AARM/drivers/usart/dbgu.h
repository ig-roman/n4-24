/*
 * File:    dbgu.h
 * Title:
 * Created: Mon Sep 04 14:55:20 2006
 *
 */

#ifndef DBGU_H
#define DBGU_H

void DBGU_rxRdyIrq(void);
void DBGU_txRdyIrq(void);

void DBGU_init(void);

void DBGU_putChar(char c);
int DBGU_pollChar(char *c);
void DBGU_putString(char *s);
int DBGU_printf(char *fmt, ...);
int AMP_printf(char *fmt, ...);

#endif /* #ifndef DBGU_H */
