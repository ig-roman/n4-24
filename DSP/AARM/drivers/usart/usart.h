/*
 * File:    usart.h
 * Title:
 * Created: Wed Sep 06 10:11:03 2006
 *
 */

#ifndef USART_H
#define USART_H

#include "../../../circ_buf.h"

typedef struct USART {
    AT91PS_USART base;
    CIRC_BUF rxBuf;
    CIRC_BUF txBuf;
} USART, *USART_Handle;

typedef struct USART_Conf {

    USART usart;
    unsigned int id;
    unsigned int mainClock;
    unsigned int mode;
    unsigned int baudRate;
    unsigned int interruptLevel;
    void (*handler)();
    char *rxBufMem;
    int rxBufMemSize;
    char *txBufMem;
    int txBufMemSize;
} USART_Conf;

// #define AT91C_ID_US0    ((unsigned int)  6) // USART 0
// #define AT91C_ID_US1    ((unsigned int)  7) // USART 1

#define USART0_CONF_DEFAULT { \
{AT91C_BASE_US0, CIRC_BUF_DEF}, \
AT91C_ID_US0, \
AT91B_MCK, \
AT91C_US_ASYNC_MODE, \
115200, \
7, \
0, \
0, \
0, \
0, \
0}

USART_Handle USART_conf(USART_Conf *conf);
void USART_irqHandler(USART_Handle usart);

void USART_getString(USART_Handle usart, char *s, int len);
void USART_putString(USART_Handle usart, char *s);
void USART_putChar(USART_Handle usart, char c);
int USART_pollChar(USART_Handle usart, char *c);
char USART_getChar(USART_Handle usart);

#endif /* #ifndef USART_H */
