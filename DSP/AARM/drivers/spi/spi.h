#ifndef SPI_H
#define SPI_H

#include "../../../TopCommon.h"

void SPI_Init();

void SPI_Start(uint8_t unCSAddr);
uint8_t SPI_Swap(uint8_t unData);
void SPI_Stop();

#endif
