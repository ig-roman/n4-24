#include "../board/board.h"
#include "spi.h"

#define CS_LINE_ALL ((uint32_t) AT91C_PIO_PA17 | AT91C_PIO_PA18 | AT91C_PIO_PA19 | AT91C_PIO_PA20)
#define CS_LINE_EN ((uint32_t) AT91C_PIO_PA23) // Inverted

/** **********************************************************************
 *
 * Configures SPI clock and CS
 *
 *********************************************************************** */
void AT91F_SPI_CfgSPI()
{
	AT91F_SPI_Close(AT91C_BASE_SPI);
	AT91F_SPI_Reset(AT91C_BASE_SPI);
	
	// Configure SPI in Master Mode
	AT91F_SPI_CfgMode(AT91C_BASE_SPI, AT91C_SPI_MSTR | AT91C_SPI_PS_VARIABLE);
	
	// Configure SPI CS
	AT91F_SPI_CfgCs(AT91C_BASE_SPI, 0, ((AT91C_SPI_SCBR & (((AT91B_MCK / 2) / SPI_CLOCK_FREQ) << 8)) | AT91C_SPI_NCPHA | AT91C_SPI_CSAAT ));
	
	AT91F_SPI_Enable(AT91C_BASE_SPI);
	
	AT91F_PDC_Open(AT91C_BASE_PDC_SPI);	
	AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1 << AT91C_ID_PIOA );
	AT91F_PIO_Enable(AT91C_BASE_PIOA,(unsigned int) AT91C_PA11_NPCS0);
}

/** **********************************************************************
 *
 * SPI Initialization
 *
 *********************************************************************** */
void SPI_Init()
{
	AT91PS_PIO pPio = AT91C_BASE_PIOA;
 	// Don't use the AT91F_SPI_CfgPIO() function because it spoils configuration of debug port (AT91C_PA9_NPCS1, AT91C_PA10_NPCS2)
  	// Configure PIO controllers to periph mode
	AT91F_PIO_CfgPeriph(pPio, // PIO controller base address
		((unsigned int) AT91C_PA13_MOSI | AT91C_PA12_MISO | AT91C_PA14_SPCK), // Peripheral A. Modify SPI lines only
		0 ); // Peripheral B
	
	AT91F_SPI_CfgPMC();
	AT91F_SPI_CfgSPI();
	
	// Custom CS lines
	AT91F_PIO_CfgOutput(pPio, CS_LINE_ALL);
	AT91F_PIO_CfgOutput(pPio, CS_LINE_EN);
	SPI_Stop();
}

/** **********************************************************************
 *
 * Starts transmission to SPI port.
 * @param unCSAddr address of SPI line (CS)
 *
 *********************************************************************** */
void SPI_Start(uint8_t unCSAddr)
{
	AT91PS_PIO pPio = AT91C_BASE_PIOA;
	
	// Prepare CS line address
	unCSAddr &= 0x0F; // Limit to 15
	uint32_t unBitsToEnableInPort = ((uint32_t)unCSAddr) << 17; // First SC pins starts from pin AT91C_PIO_PA17	
	AT91F_PIO_SetOutput(pPio, unBitsToEnableInPort);
	
	// Enable signal on selected CS line
	AT91F_PIO_ClearOutput(pPio, CS_LINE_EN); // Inverted
}

/** **********************************************************************
 *
 * Swaps byte via SPI bus
 * @param byByte data to be transfered
 * @return recieved byte from SPI
 *
 *********************************************************************** */
uint8_t SPI_Swap(uint8_t unData)
{
    AT91PS_SPI pSPI = AT91C_BASE_SPI;
	
    unsigned int x = pSPI->SPI_RDR;
    while(!(pSPI->SPI_SR & AT91C_SPI_TDRE));
    pSPI->SPI_TDR = (unData & 0xFFFF);
    while((pSPI->SPI_SR & !AT91C_SPI_TXEMPTY));
    while(!(pSPI->SPI_SR & AT91C_SPI_RDRF));
	
    return((uint8_t)pSPI->SPI_RDR);
}


/** **********************************************************************
 *
 * Stops bytes transmission to SPI port and removes signals from CS lines
 *
 *********************************************************************** */
void SPI_Stop()
{
	AT91PS_PIO pPio = AT91C_BASE_PIOA;
	
	// Remove CS line signal
	AT91F_PIO_SetOutput(pPio, CS_LINE_EN); // Inverted
	
	// Change to default addres (CS1 or 000b). (Is it needed?)
	AT91F_PIO_ClearOutput(pPio, CS_LINE_ALL);
}
