/*
 * File:    board_init.c
 * Title:
 * Created: Mon Sep 04 10:18:07 2006
 *
 */
#include "board.h"

#define SYS_INTERRUPT_LEVEL 7

unsigned int DefIrqCount = 0;
unsigned int DefFiqCount = 0;
unsigned int DefDefaultCount = 0;
unsigned int DefSpuriousCount = 0;

unsigned int DbguRxIrqCount = 0;
unsigned int DbguTxIrqCount = 0;

unsigned int SysIrqCount = 0;

void DefIrqHandler(void)
{
    DefIrqCount++;
}
void DefFiqHandler(void)
{
    DefFiqCount++;
}
void DefDefaultHandler(void)
{
    DefDefaultCount++;
}
void DefSpuriousHandler(void)
{
    DefSpuriousCount++;
}

void SysIrqHandler(void)
{
unsigned int dbguCsr;

    SysIrqCount++;

    dbguCsr = AT91C_BASE_DBGU->DBGU_CSR;
    if(dbguCsr & AT91C_US_RXRDY) {
        DbguRxIrqCount++;
        DBGU_rxRdyIrq();
    }
    if(dbguCsr & AT91C_US_TXRDY) {
        DbguTxIrqCount++;
        DBGU_txRdyIrq();
    }
}

void BOARD_init(void)
{
    // Set default handlers
#ifdef NDEBUG
    AT91F_AIC_Open(AT91C_BASE_AIC, DefIrqHandler, DefFiqHandler, DefDefaultHandler, DefSpuriousHandler, 0);
#else
    AT91F_AIC_Open(AT91C_BASE_AIC, DefIrqHandler, DefFiqHandler, DefDefaultHandler, DefSpuriousHandler, 1);
#endif

    // Enable the clock of the PIOA
    AT91F_PIOA_CfgPMC();

    AT91F_AIC_ConfigureIt(AT91C_BASE_AIC, AT91C_ID_SYS, SYS_INTERRUPT_LEVEL, AT91C_AIC_SRCTYPE_INT_HIGH_LEVEL, SysIrqHandler);
    AT91F_AIC_EnableIt(AT91C_BASE_AIC, AT91C_ID_SYS);

    TIMER_init();
    DBGU_init();
}
/** **********************************************************************
 *
 * Is called every 1ms
 *
 *********************************************************************** */
volatile unsigned long long gs_timer = 0;
void Timer1_IrqHandler()
{
	gs_timer++;
	volatile int i = *AT91C_TC1_SR;       // Clears the AT91C_TC_CPCS bit
	i = i; // Avoid unused wrning
}

/** **********************************************************************
 *
 * @return time in ms from CPU start
 *
 *********************************************************************** */
uint32_t GetTime()
{
	unsigned long long ulTimeRet = gs_timer;
	
	while (ulTimeRet != gs_timer)
	{
		ulTimeRet = gs_timer;
	}
	
	return ulTimeRet;
}

/** **********************************************************************
 *
 * Common logic for timer initialization. It can be configured with IDQ handler or witout
 *
 * @param timerId		arg interrupt number to initialize
 * @param pTimer		base timer structure
 * @param newHandler	IRQ handler
 *
 *********************************************************************** */
void TIMER_init2(unsigned int timerId, AT91PS_TC pTimer, void (*newHandler)() )
{
	if (newHandler)
	{
		// Disable the interrupt first
		AT91C_BASE_AIC->AIC_IDCR = 1 << timerId;
		
		// Clear interrupt
		AT91C_BASE_AIC->AIC_ICCR = 1 << timerId;
	}
	
	AT91F_PMC_EnablePeriphClock(AT91C_BASE_PMC, ((unsigned int) 1 << timerId));
	pTimer->TC_CMR = AT91C_TC_CLKS_TIMER_DIV2_CLOCK | AT91C_TC_CPCTRG;
	pTimer->TC_RC = 6007;   // 1 ms @ 96.109 MHz
	
	// TC Channel Control Register
	pTimer->TC_CCR = AT91C_TC_CLKEN | AT91C_TC_SWTRG;

	if (newHandler)
	{
		//* Open Timer 0 interrupt
		pTimer->TC_IER = AT91C_TC_CPCS;  //  IRQ enable CPC

		// Advanced Interrupt Controller (AIC).
		AT91F_AIC_ConfigureIt(AT91C_BASE_AIC, timerId, AT91C_AIC_PRIOR_HIGHEST, AT91C_AIC_SRCTYPE_INT_HIGH_LEVEL, newHandler); //  AIC_SMR, AIC_ICCR, AIC_SVR, AIC_IDCR
		AT91F_AIC_EnableIt(AT91C_BASE_AIC, timerId); // AIC_IECR
	}
}

/** **********************************************************************
 *
 * Initializing system timers TCO and TC1
 *
 *********************************************************************** */
void TIMER_init()
{
	TIMER_init2(AT91C_ID_TC0, AT91C_BASE_TC0, 0);
	TIMER_init2(AT91C_ID_TC1, AT91C_BASE_TC1, Timer1_IrqHandler);
}
