/*
 * File:    board.h
 * Title:
 * Created: Mon Sep 04 10:18:15 2006
 *
 */

#ifndef BOARD_H
#define BOARD_H

#include "../../include/AT91SAM7S64.h"
#define __inline inline
#include "../../include/lib_AT91SAM7S64.h"

#include "../usart/dbgu.h"
#include "../usart/usart.h"
#include "../usb/cdc_enumerate.h"

/* Hardware selection */
#include "board_AT91SAM7S64_BK3_78.h"

void BOARD_init(void);
void TIMER_init();
uint32_t GetTime();

#endif /* #ifndef BOARD_H */
