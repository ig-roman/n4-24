/*
 * CommandParser.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "CommandParser.h"
#include <string.h>

#include "../CommonArmCpp.h"

CommandParser g_commandParser;
extern double g_dFreqCorrector;

const uint8_t* CommandParser::FindCustomCommand(const char* pszCommand, uint16_t unLength) const
{
	return (unLength >= 4 && STRCMD_START == pszCommand[0] && STRCMD_CUSTOM == pszCommand[1]) ? (const uint8_t*)(pszCommand + 2) : NULL;
}

/** **********************************************************************
 *
 * Parses binnary command.
 * Command defenition:
 *	S � start of command
 *	m � mode selection (1 byte: 0 stand by, 1 voltage, 2 current, 3 resistance)
 *	v � value (8 bytes: double)
 *	f � frequency (8 bytes: double: 0 means as DC)
 *	b � band (1 byte)
 *	i � band of frequency (1 byte: band index)
 *	s � sense mode (1 byte: 0 - 2wires, 1 - 4wires)
 *	o � output (1 byte: 0 means switched off otherwise switched on)
 *	v � overload (1 byte: 0 means no overload otherwise overload mode, sends by analogue part only)
 *	c � custom low level program string
 *	E � end of command
 *	All commands with capital characters are mandatory
 *
 * @param pszCommand	new command buffer
 * @param unLength		length of command
 *
 * @return Returns command if it can be parsed or null if it contains invalid format
 *
 *********************************************************************** */
PCCOMMAND CommandParser::ParseCommand(const char* pszCommand, uint16_t* punLength)
{
	uint16_t unCharIdx = 0;
	uint16_t unLength = *punLength;
	bool bValidCmd = STRCMD_START == pszCommand[unCharIdx] && unLength >= 3;

	if (bValidCmd)
	{
		m_command.ResetCommand();

		const uint8_t* pCustomCommand = g_commandParser.FindCustomCommand(pszCommand, unLength);
		if (pCustomCommand)
		{
			// Command caching isn't supported for custom commands
			m_command.AssignCustomCommand(pCustomCommand, unLength - 3); // 3 = sart byte, end byte and CRC byte.
			unCharIdx = unLength - 1;
		}
		else
		{
			unCharIdx++;

			while (bValidCmd && unCharIdx < unLength)
			{
  				if (STRCMD_END == pszCommand[unCharIdx])
				{
				  	unCharIdx++;
				 	break;
				}

				switch (pszCommand[unCharIdx])
				{
					case STRCMD_VALUE:		{ bValidCmd = ParseDoubleCmd(m_command.value,				pszCommand, unLength, unCharIdx); break; }
					case STRCMD_FREQUENCY:	{ bValidCmd = ParseDoubleCmd(m_command.frequency,			pszCommand, unLength, unCharIdx); break; }
					case STRCMD_MODE:		{ bValidCmd = ParseByteCmd(	(char&)m_command.mode,			pszCommand, unLength, unCharIdx); break; }
					case STRCMD_BAND:		{ bValidCmd = ParseByteCmd(	(char&)m_command.bandIdx,		pszCommand, unLength, unCharIdx); break; }
					case STRCMD_FREQ_BAND:	{ bValidCmd = ParseByteCmd(	(char&)m_command.bandIdxFreq,	pszCommand, unLength, unCharIdx); break; }
					case STRCMD_SENSE:		{ bValidCmd = ParseByteCmd(	(char&)m_command.sense,			pszCommand, unLength, unCharIdx); break; }
					case STRCMD_OUT:		{ bValidCmd = ParseByteCmd(	(char&)m_command.outState,		pszCommand, unLength, unCharIdx); break; }
					case STRCMD_CORR: {
						double dCorr;
						bValidCmd = ParseDoubleCmd(dCorr, pszCommand, unLength, unCharIdx);
						if (bValidCmd) {
							g_dFreqCorrector = dCorr;
						}
						break;
					}
					default:				{ bValidCmd = false; }
				}
				
				unCharIdx++;
			}
		}
	}

	if (bValidCmd)
	{
		// Check command CRC
		uint8_t unCrc = countCrc8((const uint8_t*)pszCommand, unCharIdx, 0);
		unCrc = countCrc8((const uint8_t*)STR_SW_VERSION, strlen(STR_SW_VERSION), unCrc);
		bValidCmd = unCrc == (uint8_t)pszCommand[unCharIdx];
		unCharIdx++;
	}
			
	*punLength = unLength - unCharIdx;
	
	return bValidCmd ? &m_command : NULL;
}

bool CommandParser::ParseByteCmd(char& byRet, const char* pszCommand, uint16_t unLength, uint16_t& unCharIdx)
{
	bool bRet = unLength - unCharIdx >= 2;

	if (bRet)
	{
		unCharIdx++;
		byRet = pszCommand[unCharIdx];
	}

	return bRet;
}

bool CommandParser::ParseDoubleCmd(double& dRet, const char* pszCommand, uint16_t unLength, uint16_t& unCharIdx)
{
	bool bRet = unLength - unCharIdx >= sizeof(double) + 1;

	if (bRet)
	{
		char* pMemPointer = (char*)&dRet;
		memcpy(pMemPointer, pszCommand + 1 + unCharIdx, sizeof(double));
		unCharIdx += sizeof(double);
	}

	return bRet;
}
