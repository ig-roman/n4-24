/*
 * CommandGenerator.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef COMMANDGENERATOR_H_
#define COMMANDGENERATOR_H_

#include "../../CommonTypes.h"

class CommandGenerator
{
public:
	CommandGenerator() {};
	virtual ~CommandGenerator() {};

	void CreateStatusCommand(bool bOverloaded, uint8_t unLastError, const char*& pCommandBuff, unsigned int& unLength);
	void CreateACKCommand(bool bRet, const char*& pCommandBuff, unsigned int& unLength);

private:
	char m_buff[5];
};

extern CommandGenerator g_commandGen;

#endif /* COMMANDGENERATOR_H_ */
