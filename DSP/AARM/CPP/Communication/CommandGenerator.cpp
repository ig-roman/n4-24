/*
 * CommandGenerator.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "CommandGenerator.h"
#include "../../ArmInterface.h"
#include "../CommonArmCpp.h"

CommandGenerator g_commandGen;

/** **********************************************************************
 *
 * Creates overload command
 *
 * @param bOverloaded		status of overload pin
 * @param unLastError		last error stored in memory
 * @param pCommandBuff		returns pointer to buffer
 * @param unLength			length of command
 *
 *********************************************************************** */
void CommandGenerator::CreateStatusCommand(bool bOverloaded, uint8_t unLastError,  const char*& pCommandBuff, unsigned int& unLength)
{
	uint8_t unVerCrc = countCrc8((const uint8_t*)STR_SW_VERSION, strlen(STR_SW_VERSION), 0);
	
    // ST TODO: don't use internal buffer
	unLength = 0;
	m_buff[unLength++] = (char)STRCMD_START;
	m_buff[unLength++] = (char)STRCMD_STATUS;
	m_buff[unLength++] = (char)STRCMD_VER_CRC;
	m_buff[unLength++] = (char)unVerCrc;
	m_buff[unLength++] = (char)STRCMD_ERROR;
  	m_buff[unLength++] = (char)unLastError;
	m_buff[unLength++] = (char)STRCMD_OVERLOAD;
	m_buff[unLength++] = (char)bOverloaded;
	m_buff[unLength++] = (char)STRCMD_END;

	if (unLength != AARM_STATUS_RESPONSE_LENGTH)
	{
	  	// Indicate error by error LED
		CARM_SetLastError(AERROR_INVALID_RESPONSE_LENGH);
	}
	
	pCommandBuff = m_buff;
}

/** **********************************************************************
 *
 * Creates overload command
 *
 * @param bRet				status acknowledge code
 * @param pCommandBuff		returns pointer to buffer
 * @param unLength			length of command
 *
 *********************************************************************** */
void CommandGenerator::CreateACKCommand(bool bRet, const char*& pCommandBuff, unsigned int& unLength)
{
    // ST TODO: optimize and don't use internal buffer
	unLength = 0;
	m_buff[unLength++] = (char)(bRet ? ACK : NACK);
	pCommandBuff = m_buff;
}