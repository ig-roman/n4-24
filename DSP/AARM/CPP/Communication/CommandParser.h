/*
 * CommandParser.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef COMMANDPARSER_H_
#define COMMANDPARSER_H_

#include "../../CommonTypes.h"

class CommandParser
{
public:
	CommandParser() {};
	virtual ~CommandParser() {};

	PCCOMMAND ParseCommand(const char* pszCommand, uint16_t* punLength);
	const uint8_t* FindCustomCommand(const char* pszCommand, uint16_t unLength) const;

private:
	bool ParseByteCmd(char& byRet, const char* pszCommand, uint16_t unLength, uint16_t& unCharIdx);
	bool ParseDoubleCmd(double& dRet, const char* pszCommand, uint16_t unLength, uint16_t& unCharIdx);

private:
	COMMAND m_command;
};

extern CommandParser g_commandParser;

#endif /* COMMANDPARSER_H_ */
