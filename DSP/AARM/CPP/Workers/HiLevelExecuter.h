/*
 * HiLevelExecuter.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef HILEVELEXECUTER_H_
#define HILEVELEXECUTER_H_

#include "../../CommonTypes.h"
#include "LowLevelExecuter.h"

/** **********************************************************************
 *
 * Executes hi-level commands from front panel
 *
 *********************************************************************** */
class HiLevelExecuter
{
public:
	HiLevelExecuter();
	virtual ~HiLevelExecuter();

	void ExecuteCommand(PCCOMMAND pCommand);
	void Initialize();
	PCCOMMAND GetLastCommand() { return &m_lastCommand; }

private:
	void SetActiveProg(LowLevelProgram* pNewTempProg, bool bSkipUload);
	LowLevelProgram* GetProgByCommand(PCCOMMAND pCompleteCmd);

private:
	LowLevelExecuter	m_lowLevelExecuter;
	LowLevelProgram*	m_pActiveProg;
	COMMAND m_command, m_lastCommand;
};

// N.B. order should be same as enum PROG
extern LowLevelProgram* m_arrProgsPool[];
extern HiLevelExecuter g_hiLevelExecuter;

#endif /* HILEVELEXECUTER_H_ */
