/*
 * HiLevelExecuter.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "HiLevelExecuter.h"
#include "../LowLevelPrograms/LLProgInit.h"
#include "../LowLevelPrograms/LLProgFullStby.h"
#include "../LowLevelPrograms/LLProgCustom.h"
#include "../LowLevelPrograms/DCV/LLProgDC10mV100V.h"
#include "../LowLevelPrograms/DCV/LLProgDC1000V.h"
#include "../LowLevelPrograms/ACV/LLProgAC10V.h"
#include "../LowLevelPrograms/ACV/LLProgAC100V.h"
#include "../LowLevelPrograms/ACV/LLProgAC1000V.h"
#include "../LowLevelPrograms/ACI/LLProgAcCurrents.h"
#include "../LowLevelPrograms/DCI/LLProgDcCurrents.h"
#include "../LowLevelPrograms/R/LLProgOhms.h"

HiLevelExecuter g_hiLevelExecuter;

// N.B. order should be same as enum PROG
LowLevelProgram* m_arrProgsPool[PROG_UNKNOWN + 1] = { NULL };

HiLevelExecuter::HiLevelExecuter() :
	m_pActiveProg(NULL),
	m_command(),
	m_lastCommand()
{
	// Initialization of programs pool
	// // N.B. order should be same as in PROG enum;
	m_arrProgsPool[PROG_CUSTOM]		= new LLProgCustom();
	m_arrProgsPool[PROG_INIT]		= new LLProgInit();
	m_arrProgsPool[PROG_FULLSTBY]	= new LLProgFullStby();

	m_arrProgsPool[PROG_DC10MV100V]	= new LLProgDC10mV100V();
	m_arrProgsPool[PROG_DC1000V]	= new LLProgDC1000V();

	m_arrProgsPool[PROG_AC10V]		= new LLProgAC10V();
	m_arrProgsPool[PROG_AC100V]		= new LLProgAC100V();
	m_arrProgsPool[PROG_AC1000V]	= new LLProgAC1000V();

	m_arrProgsPool[PROG_ACI]		= new LLProgAcCurrents();
	m_arrProgsPool[PROG_DCI]		= new LLProgDcCurrents();

	m_arrProgsPool[PROG_R]			= new LLProgOhms();
}

HiLevelExecuter::~HiLevelExecuter()
{
	for (uint8_t nProgIdx = 0; NULL != m_arrProgsPool[nProgIdx]; nProgIdx++)
	{
		LowLevelProgram* pProbeProg = m_arrProgsPool[nProgIdx];
		delete pProbeProg;
	}
}

/** **********************************************************************
 *
 * Finds low level program in programs pool according to new command
 *
 * @param pCompleteCmd	new valid command from front panel or emulation software
 *
 *********************************************************************** */
LowLevelProgram* HiLevelExecuter::GetProgByCommand(PCCOMMAND pCompleteCmd)
{
	LowLevelProgram* pTempProgRet = NULL;
	if (m_pActiveProg->ApplyCommand(pCompleteCmd, true))
	{
		pTempProgRet = m_pActiveProg;
	}

	for (uint8_t nProgIdx = 0; NULL == pTempProgRet && NULL != m_arrProgsPool[nProgIdx]; nProgIdx++)
	{
		LowLevelProgram* pProbeProg = m_arrProgsPool[nProgIdx];
		if (m_pActiveProg->GetProgId() != pProbeProg->GetProgId())
		{
			if (pProbeProg->ApplyCommand(pCompleteCmd, true))
			{
				pTempProgRet = pProbeProg;
			}
		}
	}

	return pTempProgRet;
}

/** **********************************************************************
 *
 * Executes new hi-level command from front panel
 *
 * @param pCommand	new valid command from front panel or emulation software
 *
 *********************************************************************** */
void HiLevelExecuter::ExecuteCommand(PCCOMMAND pCommand)
{
	memcpy(&m_command, pCommand, sizeof(COMMAND));
	LowLevelProgram* pNewProg = GetProgByCommand(pCommand);

	// Custom program is used ONLY FOR DEVICE DEVELOPMENT. INVALID COMMANDS MAY DO PERMANETS DAMAGE OF HARWARE.
	// It should be executed without modification of active state (no unload of previous program)
	// 2015.05.14 Rein requests: safe unloading of custom program. It means than first hardware initialization step should be performed when custom program unloads.
	bool bCustom = PROG_CUSTOM == pNewProg->GetProgId();

	if (pNewProg != m_pActiveProg)
	{
		// New command requires another hardware configuration.
		// Reset all previous state caches
		pNewProg->Reset();
	}

	// Copies required fields from new command
	pNewProg->ApplyCommand(pCommand, false);

	// Performs safe update of hardware configuration. I.e. Unloads old low level program and loads new one.
	SetActiveProg(pNewProg, bCustom);
	memcpy(&m_lastCommand, pCommand, sizeof(COMMAND));
}

/** **********************************************************************
 *
 * Is called when controller is ready.
 * Loads default values to all device registers to avoid random state there.
 *
 *********************************************************************** */
void HiLevelExecuter::Initialize()
{
	LowLevelProgram* pInitProg = m_arrProgsPool[PROG_INIT];
	SetActiveProg(pInitProg, true);
}

/** **********************************************************************
 *
 * Performs update of hardware configuration according to new selected programm.
 * Function may:
 *	1) for same low level programm, syncronizes valid small harware changes
 *	2) for another low level programm, unloads old instance and load new one
 *
 * @param pNewProg		new low level program to synchronize with hardware state.
 * @param bSkipUload	WARNING when value true it skips uload phase of active program! Only for device DEVELOPMENT
 *
 *********************************************************************** */
void HiLevelExecuter::SetActiveProg(LowLevelProgram* pNewProg, bool bSkipUload)
{
	bool bIsSameProg = NULL != m_pActiveProg && pNewProg->GetProgId() == m_pActiveProg->GetProgId();

	if (!bIsSameProg)
	{
		if (!bSkipUload)
		{
			m_pActiveProg->Load(false);
			m_lowLevelExecuter.UpdateHWState(m_pActiveProg);
		}

		m_pActiveProg = pNewProg;
		m_pActiveProg->Load(true);
	}

	m_lowLevelExecuter.UpdateHWState(m_pActiveProg);
}
