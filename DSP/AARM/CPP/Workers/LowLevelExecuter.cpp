/*
 * LowLevelExecuter.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "LowLevelExecuter.h"
#include "../CommonArmCpp.h"
#include "../../ArmInterface.h"
#include <string.h>

/** **********************************************************************
 *
 * Parses low level programm and sends new data to harware.
 * At the end of execution function informs low level program that all data were updated.
 *
 * @param pProgramm		low level programm with modified state
 *
 *********************************************************************** */
void LowLevelExecuter::UpdateHWState(LowLevelProgram* pProgramm) const
{
	const uint8_t* pData = pProgramm->GetLowLevelSequence();
	
	const uint8_t* pDataInit = pData;
	while (!IsLLProgCmd(*pData, LLPROG_END_CMD))
	{
		// Chip select and data transfer
		if (IsLLProgCmd(*pData, LLPGR_CS_AND_ADDR_CMD))
		{
			uint8_t unCSAddr = *pData & LLPGR_CS_MASK;
			pData++;

			int16_t unSPIDataLen = *pData;
			pData++;

			CARM_SPI_Start(unCSAddr);
			for (; unSPIDataLen > 0; unSPIDataLen--)
			{
				CARM_SPI_Send(*pData);
				pData++;
			}
			CARM_SPI_Stop();
		}
		// Puls-width modulators change command
		else if (IsLLProgCmd(*pData, LLPGR_PWM_CMD))
		{
			pData++;

			uint32_t unPWMValue;
			memcpy(&unPWMValue, pData, sizeof(uint32_t));
			CARM_SetPWMValue(unPWMValue);

			// Doesn't work on ARM
			// uint32_t* punPWMValue = (uint32_t*)pData;
			// CARM_SetPWMValue(*punPWMValue);
			pData += 4;
		}
		// Delay command
		else if (IsLLProgCmd(*pData, LLPGR_DELAY_CMD))
		{
			pData++;
			uint16_t unDelayMs = *pData * 10;
			CARM_DelayMs(unDelayMs, false, false);
			pData++;
		}
		else
		{
			CARM_SetLastError(AERROR_UNKNOWN_LLCMD);
			break;
		}
		
		if (pData - pDataInit > LLPROG_BUFFER_SIZE)
		{
			CARM_SetLastError(AERROR_INVALID_LLCMD);
		}
	}

	// Clears modifications mask in low level programm
	pProgramm->CommitLowLevelSequence();
}
