/*
 * LowLevelExecuter.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef LOWLEVELEXECUTER_H_
#define LOWLEVELEXECUTER_H_

#include "../LowLevelPrograms/LowLevelProgram.h"

/** **********************************************************************
 *
 * Executes low level program as one atomic operation
 *
 *********************************************************************** */
class LowLevelExecuter
{
public:
	LowLevelExecuter() {};
	virtual ~LowLevelExecuter() {};

	void UpdateHWState(LowLevelProgram* pProgramm) const;
};

#endif /* LOWLEVELEXECUTER_H_ */
