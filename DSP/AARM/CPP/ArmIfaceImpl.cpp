/** **********************************************************************
 *
 * C++ implementation of methods for analogue MCU.
 *
 *********************************************************************** */

#include "../ArmInterface.h"
#include "Communication/CommandParser.h"
#include "Communication/CommandGenerator.h"
#include "Workers/HiLevelExecuter.h"
#include "../CommonTypes.h"

static COMMAND s_command;
static volatile bool s_bHasNewCommand = false;

__declspec(dllexport) void __stdcall ARM_Init()
{
	g_hiLevelExecuter.Initialize();
}

/** **********************************************************************
 *
 * Modifies high level command before execution.
 * First find suitable frequency band for given frequncy
 * Further modification is required because current amplifier is used voltage as input and changes output current in proportion -1V = +1A
 *
 * @param cmd	low level program with modified state
 *
 *********************************************************************** */
void PreModifyCommand(COMMAND& cmd)
{
	bool bIsDc = 0 == cmd.frequency;
	int nIdx;
	if (!bIsDc)
	{
		if (STATEMODE_I == cmd.mode)
		{
			PCBAND pBand = sm_arrBandsACIFreq + cmd.bandIdxFreq;
			if (IsGreater(pBand->min, cmd.frequency, pBand->fraction) ||
				IsGreater(cmd.frequency, pBand->max, pBand->fraction))
			{
				for (nIdx = 0; 0 != sm_arrBandsACIFreq[nIdx].min; nIdx++)
				{
					pBand = sm_arrBandsACIFreq + nIdx;
					if (pBand->min <= cmd.frequency && pBand->max >= cmd.frequency)
					{
						cmd.bandIdxFreq = nIdx;
						break;
					}
				}
			}
			if (cmd.frequency < pBand->min)
				cmd.frequency = pBand->min;
			if (cmd.frequency > pBand->max)
				cmd.frequency = pBand->max;
		}
		if (STATEMODE_U == cmd.mode)
		{
			PCBAND pBand = sm_arrBandsACUFreq + cmd.bandIdxFreq;
			if (IsGreater(pBand->min, cmd.frequency, pBand->fraction) ||
				IsGreater(cmd.frequency, pBand->max, pBand->fraction))
			{
				for (nIdx = 0; 0 != sm_arrBandsACUFreq[nIdx].min; nIdx++)
				{
					pBand = sm_arrBandsACUFreq + nIdx;
					if (pBand->min <= cmd.frequency && pBand->max >= cmd.frequency)
					{
						cmd.bandIdxFreq = nIdx;
						break;
					}
				}
			}
			if (cmd.frequency < pBand->min)
				cmd.frequency = pBand->min;
			if (cmd.frequency > pBand->max)
				cmd.frequency = pBand->max;
		}
	}
	if (STATEMODE_I == cmd.mode)
	{
		bool bIsCurentAmp = true;

		if (bIsDc)
		{
			switch (cmd.bandIdx)
			{
				// Bands are not inverted because proportion +1V = +1A
				case EXTBAND_DCI_N_10A_AMP: cmd.bandIdx = EXBAND_DCU_N_10_V; break;
				case EXTBAND_DCI_N_30A_AMP: cmd.bandIdx = EXBAND_DCU_N_100_V; break;
				case EXTBAND_DCI_P_10A_AMP: cmd.bandIdx = EXBAND_DCU_P_10_V; break;
				case EXTBAND_DCI_P_30A_AMP: cmd.bandIdx = EXBAND_DCU_P_100_V; break;
				default: bIsCurentAmp = false;
			}
		}
		else
		{
			switch (cmd.bandIdx)
			{
				case EXTBAND_ACI_10A_AMP: cmd.bandIdx = EXBAND_ACU_10V; break;
				case EXTBAND_ACI_30A_AMP: cmd.bandIdx = EXBAND_ACU_100V; break;
				default: bIsCurentAmp = false;
			}
			switch (cmd.bandIdxFreq)
			{
				case EXTBAND_ACI_FREQ_100: cmd.bandIdxFreq = EXTBAND_ACU_FREQ_100; break;
				case EXTBAND_ACI_FREQ_1K: cmd.bandIdxFreq = EXTBAND_ACU_FREQ_1K; break;
				case EXTBAND_ACI_FREQ_10K5: cmd.bandIdxFreq = EXTBAND_ACU_FREQ_10K; break;
				default: break;
			}

		}

		// Use implicit logic because comparison of cmd.bandIdx and bandIdxPrevious may not work because bands indexes are unique per single mode
		if (bIsCurentAmp)
		{
			cmd.mode = STATEMODE_U;
			if (bIsDc)
			{
				// Update value because proportion -1V = +1A
				cmd.value = -cmd.value;
			}
		}
	}
}

double s_getBandMaxVal(uint8_t mode, uint8_t bandIdx)
{
	if (mode == STATEMODE_U) {
		switch (bandIdx)
		{
#ifdef USE_DCU_10_MV
		case EXBAND_DCU_P_10_MV:
#endif
		case EXBAND_DCU_P_10_MV_E:
			return 0.01;
		case EXBAND_DCU_P_100_MV:
		case EXBAND_DCU_P_100_MV_E:
			return 0.1;
		case EXBAND_DCU_P_1_V:
			return 1;
		case EXBAND_DCU_P_10_V:
			return 10;
		case EXBAND_DCU_P_100_V:
			return 100;
		case EXBAND_DCU_P_1000_V:
			return 1000;
#ifdef USE_DCU_10_MV
		case EXBAND_DCU_N_10_MV:
#endif
		case EXBAND_DCU_N_10_MV_E:
			return -0.01;
		case EXBAND_DCU_N_100_MV:
		case EXBAND_DCU_N_100_MV_E:
			return -0.1;
		case EXBAND_DCU_N_1_V:
			return -1;
		case EXBAND_DCU_N_10_V:
			return -10;
		case EXBAND_DCU_N_100_V:
			return -100;
		case EXBAND_DCU_N_1000_V:
			return -1000;
		}

	}
	return 0;
}

void ExecuteLLCommandSafe(COMMAND & cmdToRun)
{
	// Copy command before execution because it may change in TIMER_delay_noLoop function
	COMMAND cmdCopy(cmdToRun);
	PCCOMMAND lastCmd = g_hiLevelExecuter.GetLastCommand();
	if (cmdToRun.mode == lastCmd->mode && cmdToRun.mode == STATEMODE_U && !cmdToRun.frequency &&
		((cmdToRun.bandIdx >= EXBAND_DCU_N_LOWEST && lastCmd->bandIdx < EXBAND_DCU_N_LOWEST) ||
		(cmdToRun.bandIdx < EXBAND_DCU_N_LOWEST && lastCmd->bandIdx >= EXBAND_DCU_N_LOWEST)))
	{
		const int inverseDelay = 100; // ms
		cmdCopy.bandIdx = lastCmd->bandIdx;
		cmdCopy.value = s_getBandMaxVal(lastCmd->mode, lastCmd->bandIdx) / 1.0e6;
		g_hiLevelExecuter.ExecuteCommand(&cmdCopy);
		CARM_DelayMs(inverseDelay, false, false);
		cmdCopy.bandIdx = cmdToRun.bandIdx;
		cmdCopy.value = -cmdCopy.value;
		g_hiLevelExecuter.ExecuteCommand(&cmdCopy);
		CARM_DelayMs(inverseDelay, false, false);
		cmdCopy.value = cmdToRun.value;
	}
	PreModifyCommand(cmdCopy);
	g_hiLevelExecuter.ExecuteCommand(&cmdCopy);
}

/** **********************************************************************
 *
 * The periodic function. Is invoked always.
 * Use CheckUpdateTime() function for custom period.
 *
 *********************************************************************** */
__declspec(dllexport) void __stdcall ARM_Loop()
{
	if (s_bHasNewCommand)
	{
	  	CARM_LLProg_Status(1);
		s_bHasNewCommand = false;
		ExecuteLLCommandSafe(s_command);
		CARM_LLProg_Status(0);
	}

	static bool sbOverloaded = false;
	static uint32_t s_unTriggerTime = 0;
	
	bool bOverloaded = CARM_GetOverloadStatus();
	bool bOverloadChanged = bOverloaded != sbOverloaded;
	
	if (CheckUpdateTime(AARM_PING_TIME, &s_unTriggerTime) || bOverloadChanged)
	{
		CARM_LLProg_Status(1);

		// Simulate output off command when overload signal is detected IMEDIATELLY
		if (bOverloadChanged && bOverloaded)
		{
			// It should work only for ACU 1000v
			if (	STATEMODE_U			== s_command.mode
				 && EXBAND_ACU_1000V	== s_command.bandIdx
				 && STATEOUT_ON			== s_command.outState)
			{
				COMMAND cmd;
				cmd.outState = STATEOUT_OFF;
				cmd.CompleteCommand(&s_command);
				
				ExecuteLLCommandSafe(s_command);
				// Front panel should disable output.
				// After that state will be synchronized
			}
		}
		
		sbOverloaded = bOverloaded;

		const char* pCommandBuff = 0;
		unsigned int unLength = 0;
		g_commandGen.CreateStatusCommand(bOverloaded, CARM_ClearLastError(), pCommandBuff, unLength);
		CARM_SendCommandString(pCommandBuff, unLength);
		CARM_LLProg_Status(0);
	}
}

__declspec(dllexport) void __stdcall ARM_HandleCommandString(const char* pszCommand, unsigned int length)
{
	uint16_t unCmdLen = length;
	bool bRet = true;
	while (unCmdLen > 0 && bRet)
	{
		PCCOMMAND pCommand = g_commandParser.ParseCommand(pszCommand + (length - unCmdLen), &unCmdLen);
		if (pCommand)
		{
			// Very important step!!!
			// Every the new separate command is applied to existing device state.
			// In some cases it may be problem. For example:
			// - Impossible to change just device mode(DCI->DCV) because all bands indexes become invalid.
			pCommand->CompleteCommand(&s_command);

			// Validate mandatory fields
			if (UNKNOWN_VALUE != s_command.value
				&&	UNKNOWN_BAND != s_command.bandIdx
				&&	STATEMODE_UNKNOWN != s_command.mode
				&&	STATEOUT_UNKNOWN != s_command.outState)
			{
				// Command is stored in m_command and ARM_Loop() will process it.
				// Next command caching isn't supported for custom commands
				s_bHasNewCommand = true;
			}
			else
			{
				CARM_SetLastError(AERROR_INCOMPLETE_TEXAS_CMD);
				bRet = false;
			}
		}
		else
		{
			CARM_SetLastError(AERROR_UNKNOWN_TEXAS_CMD);
			bRet = false;
		}
		
		const char* pCommandBuff = 0;
		unsigned int unLength = 0;
		g_commandGen.CreateACKCommand(bRet, pCommandBuff, unLength);
		CARM_SendCommandString(pCommandBuff, unLength);
	}
}
