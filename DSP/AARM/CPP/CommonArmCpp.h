#ifndef COMMONCPPTARM_H_
#define COMMONCPPTARM_H_

#include "../CommonTypes.h"

bool IsLLProgCmd(uint8_t byByte, uint8_t byCmd);
uint8_t swap_endian(uint8_t u);

#endif