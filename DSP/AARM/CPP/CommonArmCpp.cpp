
#include "CommonArmCpp.h"

bool IsLLProgCmd(uint8_t byByte, uint8_t byCmd)
{
	return byCmd == (byByte & LLPGR_CMD_MASK);
}

uint8_t swap_endian(uint8_t source)
{
	uint8_t unRet = 0;
	for(uint8_t i = 0; i < 8; i++)
	{
		unRet |= ((source >> i) & 1) << (7 - i);
	}
	
    return unRet;
}