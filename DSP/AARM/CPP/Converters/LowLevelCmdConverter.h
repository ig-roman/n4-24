/*
 * LowLevelCmdConverter.h
 *
 *  Created on: Apr 1, 2014
 *      Author: seregia
 */

#ifndef LOWLEVELCMDCONVERTER_H_
#define LOWLEVELCMDCONVERTER_H_

class LowLevelCmdConverter {
public:
	LowLevelCmdConverter();
	virtual ~LowLevelCmdConverter() {};
};

#endif /* LOWLEVELCMDCONVERTER_H_ */
