/*
 * LLProgFullStby.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef LLPROGFULLSTBY_H_
#define LLPROGFULLSTBY_H_

#include "LowLevelProgram.h"

class LLProgFullStby : public LowLevelProgram
{
public:
	LLProgFullStby() : LowLevelProgram() {};
	virtual ~LLProgFullStby() {};

	virtual PROG GetProgId() { return PROG_FULLSTBY; };
	virtual bool ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly);

	virtual const uint8_t* GetLowLevelSequence() const;
};

#endif /* LLPROGFULLSTBY_H_ */
