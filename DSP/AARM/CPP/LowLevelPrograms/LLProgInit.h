/*
 * LLProgFullStby.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef LLPROGINIT_H_
#define LLPROGINIT_H_

#include "LowLevelProgram.h"

class LLProgInit : public LowLevelProgram
{
public:
	LLProgInit() : LowLevelProgram() {};
	virtual ~LLProgInit() {};

	virtual PROG GetProgId() { return PROG_INIT; };
	virtual bool ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly);
	virtual const uint8_t* GetLowLevelSequence() const;
};

#endif /* LLPROGINIT_H_ */
