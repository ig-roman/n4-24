/*
 * LLProgFullStby.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef LLPROGCUSTOM_H_
#define LLPROGCUSTOM_H_

#include "LowLevelProgram.h"

class LLProgCustom : public LowLevelProgram
{
public:
	LLProgCustom() : LowLevelProgram() {};
	virtual ~LLProgCustom() {};

	virtual PROG GetProgId()												{ return PROG_CUSTOM; };
	virtual const uint8_t* GetLowLevelSequence() const;
	virtual bool ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly)	{ return false; }

	virtual bool ApplyCommand(PCCOMMAND pCommand, bool bTestOnly);
};

#endif /* LLPROGCUSTOM_H_ */
