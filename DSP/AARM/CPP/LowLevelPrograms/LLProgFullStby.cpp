/*
 * LLProgFullStby.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "LLProgFullStby.h"

/** **********************************************************************
 *
 * Checks that requested values from new command are suited to this object or not.
 * When values are suited this function invokes again and object copies required values
 * and becomes completely synchronized with front panel.
 * Hardware state is updated by LowLevelExecuter it gets value from this object throw GetLowLevelSequence() function.
 *
 * @param pCommand	completed new command with values to test them or update internal state of this object
 * @param bTestOnly	when value true function must decide that this command chan be handled or not
 *					when value false it means that function returned true before and must update internal state of this object
 *
 *********************************************************************** */
bool LLProgFullStby::ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly)
{
	return STATEMODE_FULL_STANDBY == pCommand->mode;
}

const uint8_t* LLProgFullStby::GetLowLevelSequence() const
{
	g_unLenSeqBuff = 1;
	g_lowLevelSeqBuff[0] = LLPROG_END_CMD;
	return g_lowLevelSeqBuff;
}