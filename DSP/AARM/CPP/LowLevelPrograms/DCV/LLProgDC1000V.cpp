/*
 * LLProgDC1000V.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "LLProgDC1000V.h"

#define SOFT_PWM_MAX_STEP_SIZE 50
#define SOFT_PWM_STEP_DELAY   100

/** **********************************************************************
 *
 * Checks that requested values from new command are suited to this object or not.
 * When values are suited this function invokes again and object copies required values
 * and becomes completely synchronized with front panel.
 * Hardware state is updated by LowLevelExecuter it gets value from this object throw GetLowLevelSequence() function.
 *
 * @param pCommand	completed new command with values to test them or update internal state of this object
 * @param bTestOnly	when value true function must decide that this command chan be handled or not
 *					when value false it means that function returned true before and must update internal state of this object
 *
 *********************************************************************** */
bool LLProgDC1000V::ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly)
{
	bool bRet = STATEMODE_U == pCommand->mode
				&& 0 == pCommand->frequency
				&&	(pCommand->bandIdx == EXBAND_DCU_N_1000_V || pCommand->bandIdx == EXBAND_DCU_P_1000_V);

	if (bRet && !bTestOnly)
	{
		m_dPrevVoltage = m_command.outState ? m_command.value : HW_STBY_PWM;
		ApplyCommandForce(pCommand);
	}

	return bRet;
}

const uint8_t* LLProgDC1000V::GetLowLevelSequence() const
{
	g_unLenSeqBuff = 0;

	if (!GetModified(GetModified(PROG_STATUS_UNLOAD)))
	{
	  	if (STATEOUT_ON == m_command.outState && GetModified(PROG_STATUS_POLARITY))
		{
			FormatPwmSeq(HW_STBY_PWM);
			FormatWaitSeq(PROG_STATUS_POLARITY, 1000);
		}
		
		FormatByteSeq(PROG_STATUS_LOAD | PROG_STATUS_POLARITY, 5, m_command.bandIdx < EXBAND_DCU_N_LOWEST ? CS05_PW2_DC_PLUS : CS05_PW2_DC_MINUS);
		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_POLARITY, 500);

		FormatByteSeq(PROG_STATUS_LOAD, 12, CS12_TRANS1KV_DC);
		FormatWaitSeq(PROG_STATUS_LOAD, 10);

		FormatByteSeq(PROG_STATUS_LOAD, 11, CS11_AMP1KV_DC);
		FormatWaitSeq(PROG_STATUS_LOAD, 10);

		FormatBandSeq(PROG_STATUS_LOAD | PROG_STATUS_BAND | PROG_STATUS_SENSE, m_arrDCBandsToLLB, m_command.bandIdx, m_command.sense, 8);
		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_BAND | PROG_STATUS_SENSE, 10);

		// After output on PWM should be valid only after output relay is closed
		uint32_t lOutBit = PROG_STATUS_OUT | PROG_STATUS_BAND | PROG_STATUS_SENSE;
		if (STATEOUT_ON == m_command.outState)
		{
			FormatWaitSeq(lOutBit, 200);
			FormatDWordSeq(lOutBit, 2, STATESENSE_TWO_WIRE == m_command.sense ?
				CS02_OUT_MAIN_CAL & ~(CS02_OUT_U_CTRL | CS02_SENSE_CTRL)
				: CS02_OUT_MAIN_CAL & ~CS02_OUT_U_CTRL);
			
			FormatByteSeq(lOutBit, 10, CS10_PW450V_READY);
			FormatWaitSeq(lOutBit, 100);
	
			FormatByteSeq(lOutBit, 10, CS10_PW450V_VOLTUP);
			FormatWaitSeq(lOutBit, 100);
			
			if (GetModified(lOutBit | PROG_STATUS_VALUE))
				FormatStepByStepPwmSeq(m_dPrevVoltage, m_command.value);
		}
		else if (GetModified(lOutBit))
		{
			// When output is off keep PWM low
			FormatPwmSeq(HW_STBY_PWM);
			FormatByteSeq(PROG_STATUS_LOAD, 10, CS10_PW450V_STBY);
			FormatWaitSeq(PROG_STATUS_OUT, 100);
			FormatDWordSeq(PROG_STATUS_OUT, 2, CS02_OUT_STBY);
		}
	}
	else
	{
		// Set PWM to 0 
		FormatPwmSeq(HW_STBY_PWM);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 200);

		FormatByteSeq(PROG_STATUS_UNLOAD, 10, CS10_PW450V_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 100);
		
		// Disconnect relay when no power on relay
		FormatDWordSeq(PROG_STATUS_UNLOAD, 2, CS02_OUT_STBY);

		FormatByteSeq(PROG_STATUS_UNLOAD, 11, CS11_AMP1KV_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 12, CS12_TRANS1KV_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 8, CS08_DCVOL_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 5, CS05_PW2_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 500);
	}

	g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPROG_END_CMD;
	g_unLenSeqBuff++;

	return g_lowLevelSeqBuff;
}

void LLProgDC1000V::FormatStepByStepPwmSeq(double dOldVoltage, double dNewVoltage) const
{
	double dVolDiff = dNewVoltage - dOldVoltage;
	double dStepsCnt = ABS(dVolDiff / SOFT_PWM_MAX_STEP_SIZE);
	int16_t nStepsCnt = (int16_t)dStepsCnt;

	double dLoopVol = dOldVoltage;
	double dPeciceIncStepVol = dVolDiff / (nStepsCnt + 1);
	for (int16_t nStep = 0; nStep < nStepsCnt; nStep++)
	{
		dLoopVol += dPeciceIncStepVol;
		FormatBandPwmSeq(PROG_STATUS_ALL, MODE_DCU, m_command.bandIdx, dLoopVol);
		FormatWaitSeq(PROG_STATUS_ALL, SOFT_PWM_STEP_DELAY);
	}
	FormatBandPwmSeq(PROG_STATUS_ALL, MODE_DCU, m_command.bandIdx, dNewVoltage);
}