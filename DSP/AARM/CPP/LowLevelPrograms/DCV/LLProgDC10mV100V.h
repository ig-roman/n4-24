/*
 * LLProgDC10mV100V.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef LLPROGDC10MV100V_H_
#define LLPROGDC10MV100V_H_

#include "../LowLevelProgram.h"

class LLProgDC10mV100V : public LowLevelProgram
{
public:
	LLProgDC10mV100V() : LowLevelProgram() {};
	virtual ~LLProgDC10mV100V() {};

	virtual PROG GetProgId() { return PROG_DC10MV100V; };
	virtual bool ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly);

	virtual const uint8_t* GetLowLevelSequence() const;

protected:
	void ApplyCommandForce(PCCOMMAND pCommand);

protected:
	static const BANDTOLLB m_arrDCBandsToLLB[];
};

#endif /* LLPROGDC10MV100V_H_ */
