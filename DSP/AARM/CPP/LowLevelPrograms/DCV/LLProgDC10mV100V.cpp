/*
 * LLProgDC10mV100V.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "LLProgDC10mV100V.h"

const BANDTOLLB LLProgDC10mV100V::m_arrDCBandsToLLB[] =
{
#ifdef USE_DCU_10_MV
	{ EXBAND_DCU_P_10_MV , CS08_DCVOL_10MV_2W	, CS08_DCVOL_10MV_4W	},
#endif
	{ EXBAND_DCU_P_100_MV, CS08_DCVOL_100MV_2W	, CS08_DCVOL_100MV_4W	},
	{ EXBAND_DCU_P_1_V   , CS08_DCVOL_1V_2W		, CS08_DCVOL_1V_4W		},
	{ EXBAND_DCU_P_10_V  , CS08_DCVOL_10V_2W	, CS08_DCVOL_10V_4W		},
	{ EXBAND_DCU_P_100_V , CS08_DCVOL_100V_2W	, CS08_DCVOL_100V_4W	},
	{ EXBAND_DCU_P_1000_V, CS08_DCVOL_1000V_2W	, CS08_DCVOL_1000V_4W	},
	{ EXBAND_DCU_P_10_MV_E, CS08_DCVOL_1V_4W    , ILLEGAL_STATE         },
	{ EXBAND_DCU_P_100_MV_E, CS08_DCVOL_10V_4W  , ILLEGAL_STATE         },
#ifdef USE_DCU_10_MV
	{ EXBAND_DCU_N_10_MV , CS08_DCVOL_10MV_2W	, CS08_DCVOL_10MV_4W	},
#endif
	{ EXBAND_DCU_N_100_MV, CS08_DCVOL_100MV_2W	, CS08_DCVOL_100MV_4W	},
	{ EXBAND_DCU_N_1_V   , CS08_DCVOL_1V_2W		, CS08_DCVOL_1V_4W		},
	{ EXBAND_DCU_N_10_V  , CS08_DCVOL_10V_2W	, CS08_DCVOL_10V_4W		},
	{ EXBAND_DCU_N_100_V , CS08_DCVOL_100V_2W	, CS08_DCVOL_100V_4W	},
	{ EXBAND_DCU_N_1000_V, CS08_DCVOL_1000V_2W	, CS08_DCVOL_1000V_4W	},
	{ EXBAND_DCU_N_10_MV_E, CS08_DCVOL_1V_4W    , ILLEGAL_STATE			},
	{ EXBAND_DCU_N_100_MV_E, CS08_DCVOL_10V_4W  , ILLEGAL_STATE			},
	NULL
};

/** **********************************************************************
 *
 * Checks that requested values from new command are suited to this object or not.
 * When values are suited this function invokes again and object copies required values
 * and becomes completely synchronized with front panel.
 * Hardware state is updated by LowLevelExecuter it gets value from this object throw GetLowLevelSequence() function.
 *
 * @param pCommand	completed new command with values to test them or update internal state of this object
 * @param bTestOnly	when value true function must decide that this command chan be handled or not
 *					when value false it means that function returned true before and must update internal state of this object
 *
 *********************************************************************** */
bool LLProgDC10mV100V::ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly)
{
        int bandIdx = pCommand->bandIdx; 
        bool bRet = STATEMODE_U == pCommand->mode &&
					0 == pCommand->frequency &&
					bandIdx != EXBAND_DCU_N_1000_V &&
					bandIdx != EXBAND_DCU_P_1000_V;
    
        if (bRet && !bTestOnly)
        {
                ApplyCommandForce(pCommand);
        }
    
        return bRet;
}

/** **********************************************************************
 *
 * Applies required values from new command to internal state of this object.
 * After this step object becomes completely synchronized with front panel.
 * Hardware state is updated by LowLevelExecuter it gets value from this object throw GetLowLevelSequence() function.
 *
 * @param pCommand	completed new command with values to update internal state of this object
 *
 *********************************************************************** */
void LLProgDC10mV100V::ApplyCommandForce(PCCOMMAND pCommand)
{
	if (pCommand->sense != m_command.sense)
	{
		m_command.sense = pCommand->sense;
		SetModified(PROG_STATUS_SENSE);
	}

	if (m_command.bandIdx < EXBAND_DCU_N_LOWEST != pCommand->bandIdx < EXBAND_DCU_N_LOWEST)
	{
		// New value will be asigned later in LowLevelProgram::ApplyCommand()
		SetModified(PROG_STATUS_POLARITY);
	}
}

const uint8_t* LLProgDC10mV100V::GetLowLevelSequence() const
{
	g_unLenSeqBuff = 0;

	if (!GetModified(PROG_STATUS_UNLOAD))
	{
		if (GetModified(PROG_STATUS_LOAD | PROG_STATUS_OUT) && STATEOUT_OFF == m_command.outState)
		{
			FormatPwmSeq(HW_STBY_PWM);
			FormatWaitSeq(PROG_STATUS_ALL, 10);
		}
	
		FormatByteSeq(PROG_STATUS_LOAD | PROG_STATUS_POLARITY, 5, m_command.bandIdx < EXBAND_DCU_N_LOWEST ? CS05_PW2_DC_PLUS : CS05_PW2_DC_MINUS);
		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_POLARITY, 10);

		FormatBandSeq(PROG_STATUS_LOAD | PROG_STATUS_BAND | PROG_STATUS_SENSE, m_arrDCBandsToLLB, m_command.bandIdx, m_command.sense, 8);
		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_BAND | PROG_STATUS_SENSE, 10);

		FormatByteSeq(PROG_STATUS_LOAD, 9, CS09_PW_190V);
		FormatWaitSeq(PROG_STATUS_LOAD, 100);

		FormatDWordSeq(PROG_STATUS_LOAD | PROG_STATUS_OUT | PROG_STATUS_SENSE, 2, STATEOUT_ON == m_command.outState ?
			(STATESENSE_TWO_WIRE == m_command.sense ? CS02_OUT_MAIN_CAL & ~(CS02_OUT_U_CTRL | CS02_SENSE_CTRL) : CS02_OUT_MAIN_CAL & ~CS02_OUT_U_CTRL)
			: CS02_OUT_STBY);
		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_OUT, 10);

		if (STATEOUT_OFF != m_command.outState)
			FormatBandPwmSeq(PROG_STATUS_LOAD | PROG_STATUS_VALUE | PROG_STATUS_BAND | PROG_STATUS_OUT, MODE_DCU, m_command.bandIdx, m_command.value);

	}
	else
	{
		FormatBaseUnload();

		FormatByteSeq(PROG_STATUS_UNLOAD, 9, CS09_PW_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 8, CS08_DCVOL_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 5, CS05_PW2_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);
	}

	g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPROG_END_CMD;
	g_unLenSeqBuff++;

	return g_lowLevelSeqBuff;
}
