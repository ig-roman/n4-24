/*
 * LLProgDC1000V.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef LLPROGDC1000V_H_
#define LLPROGDC1000V_H_

#include "LLProgDC10mV100V.h"

class LLProgDC1000V : public LLProgDC10mV100V
{
public:
	LLProgDC1000V() : LLProgDC10mV100V() {};
	virtual ~LLProgDC1000V() {};
	bool ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly);

	virtual PROG GetProgId() { return PROG_DC1000V; };

protected:
	virtual const uint8_t* GetLowLevelSequence() const;
private:
	void FormatStepByStepPwmSeq(double dOldVoltage, double dNewVoltage) const;
	double m_dPrevVoltage;
};

#endif /* LLPROGDC1000V_H_ */
