/*
 * LLProgOhms.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef LLPROGOHMS_H_
#define LLPROGOHMS_H_

#include "../LowLevelProgram.h"

class LLProgOhms : public LowLevelProgram
{
public:
	LLProgOhms() : LowLevelProgram() {};
	virtual ~LLProgOhms() {};

	virtual const uint8_t* GetLowLevelSequence() const;
	virtual PROG GetProgId() { return PROG_R; };

protected:
	virtual bool ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly);
};

#endif /* LLPROGOHMS_H_ */
