/*
 * LLProgOhms.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "LLProgOhms.h"

typedef struct
{
	uint8_t		bandIdx;
	uint16_t	reg2w;
	uint16_t	reg4w;
} BANDTORREG;

const BANDTORREG sm_arrRBandToLLB[] =
{
	{ EXTBAND_R_0R		, CS02_OUT_2W_0R	, CS02_OUT_4W_0R	},
	{ EXTBAND_R_1R		, CS02_OUT_2W_1R	, CS02_OUT_4W_1R	},
	{ EXTBAND_R_10R		, CS02_OUT_2W_10R	, CS02_OUT_4W_10R	},
	{ EXTBAND_R_100R	, CS02_OUT_2W_100R	, CS02_OUT_4W_100R	},
	{ EXTBAND_R_1K		, CS02_OUT_2W_1KR	, CS02_OUT_4W_1KR	},
	{ EXTBAND_R_10K		, CS02_OUT_2W_10KR	, CS02_OUT_4W_10KR	},
	{ EXTBAND_R_100K	, CS02_OUT_2W_100KR	, CS02_OUT_4W_100KR	},
	{ EXTBAND_R_1M		, CS02_OUT_2W_1MR	, CS02_OUT_4W_1MR	},
	{ EXTBAND_R_10M		, CS02_OUT_2W_10MR	, CS02_OUT_4W_10MR	},
	{ EXTBAND_R_100M	, CS02_OUT_2W_100MR	, CS02_OUT_4W_100MR	}
};

/** **********************************************************************
 *
 * Checks that requested values from new command are suited to this object or not.
 * When values are suited this function invokes again and object copies required values
 * and becomes completely synchronized with front panel.
 * Hardware state is updated by LowLevelExecuter it gets value from this object throw GetLowLevelSequence() function.
 *
 * @param pCommand	completed new command with values to test them or update internal state of this object
 * @param bTestOnly	when value true function must decide that this command chan be handled or not
 *					when value false it means that function returned true before and must update internal state of this object
 *
 *********************************************************************** */
bool LLProgOhms::ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly)
{
	bool bRet = STATEMODE_R == pCommand->mode;

	if (bRet && !bTestOnly && pCommand->sense != m_command.sense)
	{
		m_command.sense = pCommand->sense;
		SetModified(PROG_STATUS_SENSE);
	}

	return bRet;
}

const uint8_t* LLProgOhms::GetLowLevelSequence() const
{
	static uint8_t s_currentOut;
	g_unLenSeqBuff = 0;

	if (!GetModified(PROG_STATUS_UNLOAD))
	{
		FormatByteSeq(PROG_STATUS_LOAD, 9, CS09_PW_190V);
		FormatWaitSeq(PROG_STATUS_LOAD, 100);

		if (GetModified(PROG_STATUS_OUT))
			 s_currentOut = STATEOUT_ON == m_command.outState;

		if (GetModified(PROG_STATUS_OUT | PROG_STATUS_BAND | PROG_STATUS_SENSE))
		{
			const BANDTORREG rbandToLLB = sm_arrRBandToLLB[m_command.bandIdx];
			uint32_t unScData = STATESENSE_TWO_WIRE == m_command.sense ? (rbandToLLB.reg2w << 8) & ~CS02_SENSE_CTRL : (rbandToLLB.reg4w << 8) | CS02_SENSE_CTRL;
			if (s_currentOut)
				unScData &= ~(CS02_STBY_CTRL | CS02_OUT_IR_CTRL);
			else
				unScData |= (CS02_STBY_CTRL | CS02_OUT_IR_CTRL);
			unScData |= CS02_OUT_U_CTRL;
			FormatDWordSeq(PROG_STATUS_OUT | PROG_STATUS_BAND | PROG_STATUS_SENSE, 2, unScData);
		}
	}
	else
	{
		FormatDWordSeq(PROG_STATUS_UNLOAD, 2, CS02_OUT_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 100);
		FormatByteSeq(PROG_STATUS_UNLOAD, 9, CS09_PW_STBY);
	}

	g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPROG_END_CMD;
	g_unLenSeqBuff++;

	return g_lowLevelSeqBuff;
}
