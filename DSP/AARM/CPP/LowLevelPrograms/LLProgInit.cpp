/*
 * LLProgFullStby.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "LLProgInit.h"

/** **********************************************************************
 *
 * Checks that requested values from new command are suited to this object or not.
 * When values are suited this function invokes again and object copies required values
 * and becomes completely synchronized with front panel.
 * Hardware state is updated by LowLevelExecuter it gets value from this object throw GetLowLevelSequence() function.
 *
 * @param pCommand	completed new command with values to test them or update internal state of this object
 * @param bTestOnly	when value true function must decide that this command chan be handled or not
 *					when value false it means that function returned true before and must update internal state of this object
 *
 *********************************************************************** */
bool LLProgInit::ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly)
{
	return false;
}

const uint8_t* LLProgInit::GetLowLevelSequence() const
{
	g_unLenSeqBuff = 0;

	if (GetModified(PROG_STATUS_LOAD))
	{
		FormatWHInitSequence(PROG_STATUS_LOAD);
	}

	g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPROG_END_CMD;
	g_unLenSeqBuff++;

	return g_lowLevelSeqBuff;
}