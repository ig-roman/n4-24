/*
 * LLProgAC10V.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef LLPROGAC10V_H_
#define LLPROGAC10V_H_

#include "../LLProgACBase.h"

class LLProgAC10V : public LLProgACBase
{
public:
	LLProgAC10V() : LLProgACBase() {};
	virtual ~LLProgAC10V() {};

	virtual const uint8_t* GetLowLevelSequence() const;
	virtual PROG GetProgId() { return PROG_AC10V; };

protected:
	virtual bool ApplyCommandInternalAC(PCCOMMAND pCommand);
};

#endif /* LLPROGAC10V_H_ */
