/*
 * LLProgAC100V.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "LLProgAC100V.h"

const BANDTOLLB sm_arrAmpFreqToLLB[] =
{
	{ POWERAMP100RANGE_UNKNOWN,		CS07_AC100V_STBY,		CS07_AC100V_STBY		},
	{ POWERAMP100RANGE_10HZ_100KHZ,	CS07_AC100V_100KHZ_2W,	CS07_AC100V_100KHZ_4W	},
	{ POWERAMP100RANGE_100KHZ_1MHZ,	CS07_AC100V_1MHZ_2W,	CS07_AC100V_1MHZ_4W		}
};

LLProgAC100V::LLProgAC100V() : LLProgACBase(),
	m_eAmpFreqRange(POWERAMP100RANGE_UNKNOWN)
{}

/** **********************************************************************
 *
 * Checks that requested values from new command are suited to this object or not.
 * @param pCommand	completed new command with values to test them or update internal state of this object
 *
 *********************************************************************** */
bool LLProgAC100V::ApplyCommandInternalAC(PCCOMMAND pCommand)
{
	return STATEMODE_U == pCommand->mode && EXBAND_ACU_100V == pCommand->bandIdx;
}

/** **********************************************************************
 *
 * Applies required values from new command to internal state of this object.
 * After this step object becomes completely synchronized with front panel.
 * Hardware state is updated by LowLevelExecuter it gets value from this object throw GetLowLevelSequence() function.
 *
 * @param pCommand	completed new command with values to update internal state of this object
 *
 *********************************************************************** */
void LLProgAC100V::ApplyCommandForce(PCCOMMAND pCommand)
{
	POWERAMP100RANGE eNewFreqRange = EXTBAND_ACU_FREQ_1M == pCommand->bandIdxFreq ? POWERAMP100RANGE_100KHZ_1MHZ : POWERAMP100RANGE_10HZ_100KHZ;
	if (m_eAmpFreqRange != eNewFreqRange)
	{
		m_eAmpFreqRange = eNewFreqRange;
		SetModified(PROG_STATUS_POWER100_FREQ_RANGE);
	}

	LLProgACBase::ApplyCommandForce(pCommand);
}

const uint8_t* LLProgAC100V::GetLowLevelSequence() const
{
	g_unLenSeqBuff = 0;

	if (!GetModified(PROG_STATUS_UNLOAD))
	{
		if (GetModified(PROG_STATUS_LOAD | PROG_STATUS_OUT) && STATEOUT_OFF == m_command.outState)
		{
			FormatPwmSeq(HW_STBY_PWM_AC);
			FormatWaitSeq(PROG_STATUS_ALL, 10);
		}

		FormatByteSeq(PROG_STATUS_LOAD | PROG_STATUS_OUT, 5, STATEOUT_ON == m_command.outState ? CS05_PW2_AC : CS05_PW2_STBY);
		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_OUT, 10);

		FormatByteSeq(PROG_STATUS_LOAD | PROG_STATUS_POWER100_FREQ_RANGE, 9, POWERAMP100RANGE_10HZ_100KHZ == m_eAmpFreqRange ? CS09_PW_190V : CS09_PW_75V);
		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_POWER100_FREQ_RANGE, 100);

		// For invisibility frame
		FormatByteSeq(PROG_STATUS_FREQ_BAND, 7, CS07_AC100V_STBY);
		FormatWaitSeq(PROG_STATUS_FREQ_BAND, 10);

		FormatFreqSeq(PROG_STATUS_LOAD | PROG_STATUS_FREQ | PROG_STATUS_FREQ_BAND, m_command.frequency, sm_arrBandsACUFreq, m_command.bandIdxFreq);
		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_FREQ | PROG_STATUS_FREQ_BAND, 10);

		// For invisibility frame
		FormatWaitSeq(PROG_STATUS_FREQ_BAND, INVISIBILITY_FRAME_LENGTH);

		FormatBandSeq(PROG_STATUS_LOAD | PROG_STATUS_POWER100_FREQ_RANGE | PROG_STATUS_SENSE | PROG_STATUS_FREQ_BAND, sm_arrAmpFreqToLLB, m_eAmpFreqRange, m_command.sense, 7);
		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_POWER100_FREQ_RANGE | PROG_STATUS_SENSE | PROG_STATUS_FREQ_BAND, 10);

		FormatDWordSeq(PROG_STATUS_LOAD | PROG_STATUS_OUT | PROG_STATUS_SENSE, 2, STATEOUT_ON == m_command.outState ?
			(STATESENSE_TWO_WIRE == m_command.sense ? CS02_OUT_MAIN_CAL & ~(CS02_OUT_U_CTRL | CS02_SENSE_CTRL) : CS02_OUT_MAIN_CAL & ~CS02_OUT_U_CTRL)
			: CS02_OUT_STBY);
		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_OUT, 10);

		if (STATEOUT_OFF != m_command.outState)
			FormatBandPwmSeq(PROG_STATUS_LOAD | PROG_STATUS_VALUE | PROG_STATUS_BAND | PROG_STATUS_OUT, MODE_ACU, m_command.bandIdx, m_command.value);
	}
	else
	{
		FormatBaseUnload();
		
		FormatFreqSeq(PROG_STATUS_UNLOAD, HW_STBY_FREQ, sm_arrBandsACUFreq, EXTBAND_ACU_FREQ_1K);

		FormatByteSeq(PROG_STATUS_UNLOAD, 9, CS09_PW_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 7, CS07_AC100V_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 5, CS05_PW2_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);
	}

	g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPROG_END_CMD;
	g_unLenSeqBuff++;

	return g_lowLevelSeqBuff;
}