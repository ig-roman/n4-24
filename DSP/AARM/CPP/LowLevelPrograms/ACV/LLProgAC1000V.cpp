/*
 * LLProgAC1000V.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "LLProgAC1000V.h"

// Rein requests that "Soft PWM change" should work every time for any level and sinle step is voltage / SOFT_PWM_STEP_COUNT
//#define USE_STRANGE_STEPPER_APPROACH_REQUESTED_BY_REIN
#define SOFT_PWM2_STEP_COUNT	20
// (in volts) Anyway it very strange when voltage difference is very small to devide it to SOFT_PWM2_STEP_COUNT.
// I have introduced the SOFT_PWM2_THRESHOLD parameter to skip "Soft PWM change" logic for very small levels
#define SOFT_PWM2_THRESHOLD		1.0
// Low-high frequency discriminator
#define SOFT_PWM2_LOW_HIGH_FREQ_DISCRIMINATOR	30
// Delay in ms after each "Soft PWM2 change" on high frequencies > 30hz. Max value 2550 ms
#define SOFT_PWM2_STEP_DELAY					100
// Delay in ms after each "Soft PWM2 change" on low frequencies <= 30hz. Max value 2550 ms
#define SOFT_PWM2_LOW_FREQ_STEP_DELAY			500

// "Soft PWM change" works when voltage difference more than this value
#define SOFT_PWM_MAX_STEP_SIZE	50
// Delay in ms after each "Soft PWM change" max value 2550 ms
#define SOFT_PWM_STEP_DELAY		100
// Is used as previous value when program starts in first time
#define SOFT_PWM_INITIAL_VOL	10 //(sm_arrBandsACU[EXBAND_ACU_1000V].min)

LLProgAC1000V::LLProgAC1000V()  : LLProgACBase(),
		m_eAmpFreqRange(POWERAMPRANGE_UNKNOWN),
		m_dPrevVoltage(SOFT_PWM_INITIAL_VOL)
{}

/** **********************************************************************
 *
 * Checks that requested values from new command are suited to this object or not.
 * @param pCommand	completed new command with values to test them or update internal state of this object
 *
 *********************************************************************** */
bool LLProgAC1000V::ApplyCommandInternalAC(PCCOMMAND pCommand)
{
	return STATEMODE_U == pCommand->mode && EXBAND_ACU_1000V == pCommand->bandIdx;
}

void LLProgAC1000V::Reset()
{
	m_dPrevVoltage = SOFT_PWM_INITIAL_VOL;
	m_eAmpFreqRange = POWERAMPRANGE_UNKNOWN;
	LLProgACBase::Reset();
}

POWERAMPRANGE GetPowerAmpFreqRange(double dFreq)
{
	POWERAMPRANGE eFreqRange = POWERAMPRANGE_UNKNOWN;
	if (dFreq < 5000)
	{
		eFreqRange = POWERAMPRANGE_30HZ_5KHZ;
	}
	else if (dFreq >= 5000 && dFreq < 20000)
	{
		eFreqRange = POWERAMPRANGE_5KHZ_20KHZ;
	}
	else if (dFreq >= 20000)
	{
		eFreqRange = POWERAMPRANGE_20KHZ_100KHZ;
	}

	return eFreqRange;
}

/** **********************************************************************
 *
 * Applies required values from new command to internal state of this object.
 * After this step object becomes completely synchronized with front panel.
 * Hardware state is updated by LowLevelExecuter it gets value from this object throw GetLowLevelSequence() function.
 *
 * @param pCommand	completed new command with values to update internal state of this object
 *
 *********************************************************************** */
void LLProgAC1000V::ApplyCommandForce(PCCOMMAND pCommand)
{
	POWERAMPRANGE eNewFreqRange = GetPowerAmpFreqRange(pCommand->frequency);
	if (m_eAmpFreqRange != eNewFreqRange)
	{
		m_eAmpFreqRange = eNewFreqRange;
		SetModified(PROG_STATUS_POWER_FREQ_RANGE);
	}

	// Hold PWM on the lower value when output is disabled
	// It requires to use "Step by step" increasing when output become active
	if (!pCommand->outState)
	{
		// Output has been changed set modification flag to update hardware
		if (m_command.outState != pCommand->outState)
		{
			SetModified(PROG_STATUS_VALUE);

			// Requires to decrease voltage from old value
			if (UNKNOWN_VALUE != m_command.value)
			{
				m_dPrevVoltage = m_command.value;
			}
		}

		SetModified(PROG_STATUS_SKIP_VALUE_CHANGE);
		m_command.value = SOFT_PWM_INITIAL_VOL;
	}
	else
	{
		SetModified(PROG_STATUS_SKIP_VALUE_CHANGE, false);

		// Remember old voltage before it is changed in function LowLevelProgram::ApplyCommand().
		if (m_command.value != pCommand->value && UNKNOWN_VALUE != m_command.value)
		{
			m_dPrevVoltage = m_command.value;
		}
	}

	LLProgACBase::ApplyCommandForce(pCommand);
}

void LLProgAC1000V::FormatTransformerSeq(const uint32_t unCheckFlags, POWERAMPRANGE eAmpFreqRange, STATESENSE sense) const
{
	if (GetModified(unCheckFlags))
	{
		uint8_t unTransRegVal = CS12_TRANS1KV_STBY;
		switch (eAmpFreqRange)
		{
			case POWERAMPRANGE_30HZ_5KHZ	: unTransRegVal = STATESENSE_TWO_WIRE == sense ? CS12_TRANS1KV_30HZ_5KHZ_2W		: CS12_TRANS1KV_30HZ_5KHZ_4W; break;
			case POWERAMPRANGE_5KHZ_20KHZ	: unTransRegVal = STATESENSE_TWO_WIRE == sense ? CS12_TRANS1KV_5KHZ_20KHZ_2W	: CS12_TRANS1KV_5KHZ_20KHZ_4W; break;
			case POWERAMPRANGE_20KHZ_100KHZ	: unTransRegVal = STATESENSE_TWO_WIRE == sense ? CS12_TRANS1KV_20KHZ_100KHZ_2W	: CS12_TRANS1KV_20KHZ_100KHZ_4W; break;
		}
		
		FormatByteSeq(unCheckFlags, 12, unTransRegVal);
	}
}

void LLProgAC1000V::FormatAmplifierSeq(const uint32_t unCheckFlags, POWERAMPRANGE eAmpFreqRange) const
{
	if (GetModified(unCheckFlags))
	{
		uint8_t unTransRegVal = CS11_AMP1KV_STBY;
		switch (eAmpFreqRange)
		{
			case POWERAMPRANGE_30HZ_5KHZ	: unTransRegVal = CS11_AMP1KV_30HZ_5KHZ; break;
			case POWERAMPRANGE_5KHZ_20KHZ	: unTransRegVal = CS11_AMP1KV_5KHZ_20KHZ; break;
			case POWERAMPRANGE_20KHZ_100KHZ	: unTransRegVal = CS11_AMP1KV_20KHZ_100KHZ; break;
		}
		
		FormatByteSeq(unCheckFlags, 11, unTransRegVal);
		FormatWaitSeq(unCheckFlags, 10);
	}
}


void LLProgAC1000V::FormatOutSwitchSeq(bool bOutputOn, double dNewFreq) const
{
	if (bOutputOn)
	{
		FormatONOutSwitchSeq(SOFT_PWM_INITIAL_VOL, m_command.value, dNewFreq);
	}
	else
	{
		FormatOFFOutSwitchSeq(m_dPrevVoltage, SOFT_PWM_INITIAL_VOL, dNewFreq);
	}
}

/** **********************************************************************
* Softly applies high voltage to the output
*********************************************************************** */
void LLProgAC1000V::FormatONOutSwitchSeq(double dOldVoltage, double dNewVoltage, double dNewFreq) const
{
	// This bit resets overload trigger
	FormatByteSeq(PROG_STATUS_ALL, 10, CS10_PW450V_READY);
	FormatWaitSeq(PROG_STATUS_ALL, 10);

	FormatByteSeq(PROG_STATUS_ALL, 10, CS10_PW450V_VOLTUP);
	FormatWaitSeq(PROG_STATUS_ALL, 100);

	// Configure output relay CS2. Works when operator goes from band 100v to band 1000v with enabled output before
	// CS 2 output relay must be closed with minimal voltage, otherwise protection disables output again
	FormatDWordSeq(PROG_STATUS_ALL, 2, STATESENSE_TWO_WIRE == m_command.sense ?
		CS02_OUT_MAIN_CAL & ~(CS02_OUT_U_CTRL | CS02_SENSE_CTRL)
		: CS02_OUT_MAIN_CAL & ~CS02_OUT_U_CTRL);
	FormatWaitSeq(PROG_STATUS_ALL, 10);

	// CS 11 + delay 10 ms
	FormatAmplifierSeq(PROG_STATUS_ALL, m_eAmpFreqRange);
	FormatWaitSeq(PROG_STATUS_ALL, 10);

	// Increase voltage
	FormatStepByStepPwmSeq(dOldVoltage, dNewVoltage, dNewFreq);
}

/** **********************************************************************
* Softly removes high voltage from output and disconnects output relay or applies voltage to output
*********************************************************************** */
void LLProgAC1000V::FormatOFFOutSwitchSeq(double dOldVoltage, double dNewVoltage, double dNewFreq) const
{
	// Decrease voltage
	FormatStepByStepPwmSeq(dOldVoltage, dNewVoltage, dNewFreq);
	
	// In overload and emergency cases amplifier should be disabled immediately to remove voltage from output as fast as possible.
	//FormatPwmSeq(HW_STBY_PWM);

	// This bit resets overload trigger
	FormatByteSeq(PROG_STATUS_ALL, 10, CS10_PW450V_STBY);

	FormatByteSeq(PROG_STATUS_ALL, 11, CS11_AMP1KV_STBY);

	// Configure output relay CS2. Works when operator goes from band 100v to band 1000v with enabled output before
	// CS 2 output relay must be updated in last step because
	// - it saves output relay. relay may break when it opens on 1KV
	FormatDWordSeq(PROG_STATUS_ALL, 2, CS02_OUT_STBY);
}

void LLProgAC1000V::PreLoad() const
{
	FormatByteSeq(PROG_STATUS_LOAD, 5, CS05_PW2_AC);
	FormatWaitSeq(PROG_STATUS_LOAD, 500);
	
	// CS 12
	FormatTransformerSeq(PROG_STATUS_LOAD, m_eAmpFreqRange, m_command.sense);
	FormatWaitSeq(PROG_STATUS_LOAD, 10);
	
	FormatFreqSeq(PROG_STATUS_LOAD, m_command.frequency, sm_arrBandsACUFreq, m_command.bandIdxFreq);
	FormatWaitSeq(PROG_STATUS_LOAD, 10);
}

void LLProgAC1000V::SoftChangeFrequencyBand() const
{
	if (STATEOUT_ON == m_command.outState)
	{
		// Remove high voltage from output when frequency band is changing.
		FormatOFFOutSwitchSeq(m_command.value, SOFT_PWM_INITIAL_VOL, m_command.frequency);
		FormatWaitSeq(PROG_STATUS_ALL, 1000);
	}

	// CS 12
	FormatTransformerSeq(PROG_STATUS_ALL, m_eAmpFreqRange, m_command.sense);
	FormatWaitSeq(PROG_STATUS_ALL, 500);

	FormatFreqSeq(PROG_STATUS_ALL, m_command.frequency, sm_arrBandsACUFreq, m_command.bandIdxFreq);
	FormatWaitSeq(PROG_STATUS_ALL, 10);

	if (STATEOUT_ON == m_command.outState)
	{
		FormatONOutSwitchSeq(SOFT_PWM_INITIAL_VOL, m_command.value, m_command.frequency);
	}
}

const uint8_t* LLProgAC1000V::GetLowLevelSequence() const
{
	g_unLenSeqBuff = 0;

	if (!GetModified(PROG_STATUS_UNLOAD))
	{
	  	if (GetModified(PROG_STATUS_LOAD))
		{
		  	// Program can be loaded with enabled output relay or not.
	  		PreLoad();
			FormatOutSwitchSeq(STATEOUT_ON == m_command.outState, m_command.frequency);
		}
		else
		{
			bool bIsValueAlredyChanged = false;
			if (GetModified(PROG_STATUS_POWER_FREQ_RANGE))
			{
				SoftChangeFrequencyBand();
				bIsValueAlredyChanged = true;
			}
			else
			{
				FormatFreqSeq(PROG_STATUS_FREQ | PROG_STATUS_FREQ_BAND, m_command.frequency, sm_arrBandsACUFreq, m_command.bandIdxFreq);
				FormatWaitSeq(PROG_STATUS_FREQ | PROG_STATUS_FREQ_BAND, 10);
			}

			if (GetModified(PROG_STATUS_SENSE))
			{
				// CS 12
				FormatTransformerSeq(PROG_STATUS_ALL, m_eAmpFreqRange, m_command.sense);
				FormatWaitSeq(PROG_STATUS_ALL, 10);
			}
			
			if (GetModified(PROG_STATUS_OUT))
			{
				// Overload or output on/off command. PWM should be changed according to required state
				FormatOutSwitchSeq(STATEOUT_ON == m_command.outState, m_command.frequency);
				bIsValueAlredyChanged = true;
			}
			
			if (GetModified(PROG_STATUS_VALUE) && !bIsValueAlredyChanged)
			{
				// Regular voltage change and out remains same
				FormatStepByStepPwmSeq(m_dPrevVoltage, m_command.value, m_command.frequency);
			}
		}
	}
	else
	{
		FormatOFFOutSwitchSeq(m_command.value, SOFT_PWM_INITIAL_VOL, m_command.frequency);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);
		
		FormatFreqSeq(PROG_STATUS_UNLOAD, HW_STBY_FREQ, sm_arrBandsACUFreq, EXTBAND_ACU_FREQ_1K);

		// This bit resets overload trigger when programm exits
		FormatByteSeq(PROG_STATUS_UNLOAD, 10, CS10_PW450V_READY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);
		FormatByteSeq(PROG_STATUS_UNLOAD, 10, CS10_PW450V_VOLTUP);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 10, CS10_PW450V_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 200);

		FormatByteSeq(PROG_STATUS_UNLOAD, 12, CS12_TRANS1KV_STBY);

		FormatByteSeq(PROG_STATUS_UNLOAD, 5, CS05_PW2_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 500);
	}

	g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPROG_END_CMD;
	g_unLenSeqBuff++;

	return g_lowLevelSeqBuff;
}

/** **********************************************************************
 *
 * Formats low level commands to get required PWM (voltage) for ACU 1000V band.
 * This band requires additional intermediate steps to grow up voltage from 100V to 1000V
 * To configure behavior of steps see SOFT_PWM_MAX_STEP_SIZE and SOFT_PWM_STEP_DELAY defines
 *
 * @param dOldVoltage	previous voltage
 * @param dValue		required voltage on device output.
 *
 *********************************************************************** */
void LLProgAC1000V::FormatStepByStepPwmSeq(double dOldVoltage, double dNewVoltage, double dNewFreq) const
{


	double dVolDiff = dNewVoltage - dOldVoltage;
	uint32_t unDelay = dNewFreq > SOFT_PWM2_LOW_HIGH_FREQ_DISCRIMINATOR 
		? SOFT_PWM2_STEP_DELAY
		: SOFT_PWM2_LOW_FREQ_STEP_DELAY;

#ifdef USE_STRANGE_STEPPER_APPROACH_REQUESTED_BY_REIN
  	if (ABS(dVolDiff) > SOFT_PWM2_THRESHOLD )
	{
		double dLoopVol = dOldVoltage;
		double dPeciceIncStepVol = dVolDiff / SOFT_PWM2_STEP_COUNT;
		for (int16_t nStep = 0; nStep < SOFT_PWM2_STEP_COUNT; nStep++)
		{
			dLoopVol += dPeciceIncStepVol;
			FormatBandPwmSeq(PROG_STATUS_ALL, MODE_ACU, EXBAND_ACU_1000V, dLoopVol);
			FormatWaitSeq(PROG_STATUS_ALL, unDelay);
		}
	}
	else
	{
		FormatBandPwmSeq(PROG_STATUS_ALL, MODE_ACU, EXBAND_ACU_1000V, dNewVoltage);
		FormatWaitSeq(PROG_STATUS_ALL, unDelay);
	}
#else
	double dStepsCnt = ABS(dVolDiff / SOFT_PWM_MAX_STEP_SIZE);
	int16_t nStepsCnt = (int16_t)dStepsCnt;

	double dLoopVol = dOldVoltage;
	double dPeciceIncStepVol = dVolDiff / (nStepsCnt + 1);
	for (int16_t nStep = 0; nStep < nStepsCnt; nStep++)
	{
		dLoopVol += dPeciceIncStepVol;
		FormatBandPwmSeq(PROG_STATUS_ALL, MODE_ACU, EXBAND_ACU_1000V, dLoopVol);
		FormatWaitSeq(PROG_STATUS_ALL, unDelay);
	}
	FormatBandPwmSeq(PROG_STATUS_ALL, MODE_ACU, EXBAND_ACU_1000V, dNewVoltage);
#endif
	// N.B. This is constant function its just formats low level command!
	// m_dPrevVoltage will be update when new command arrived with new voltage value.
	// It gets previous state from internal structure and logic of it located in function: LProgAC1000V::ApplyCommandForce(PCCOMMAND pCommand)
}
		