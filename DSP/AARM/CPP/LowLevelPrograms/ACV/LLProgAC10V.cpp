/*
 * LLProgAC10V.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "LLProgAC10V.h"

/** **********************************************************************
 *
 * Checks that requested values from new command are suited to this object or not.
 * When values are suited this function invokes again and object copies required values
 * and becomes completely synchronized with front panel.
 * Hardware state is updated by LowLevelExecuter it gets value from this object throw GetLowLevelSequence() function.
 *
 * @param pCommand	completed new command with values to test them or update internal state of this object
 * @param bTestOnly	when value true function must decide that this command chan be handled or not
 *					when value false it means that function returned true before and must update internal state of this object
 *
 *********************************************************************** */
bool LLProgAC10V::ApplyCommandInternalAC(PCCOMMAND pCommand)
{
#ifdef USE_ACU_1MV
	int lowestBandIdx = EXBAND_ACU_1MV;
#else
	int lowestBandIdx = EXBAND_ACU_10MV;
#endif
    int bandIdx = pCommand->bandIdx;
    return STATEMODE_U == pCommand->mode && bandIdx <= EXBAND_ACU_10V && bandIdx >= lowestBandIdx;
}

const uint8_t* LLProgAC10V::GetLowLevelSequence() const
{
	g_unLenSeqBuff = 0;

	if (!GetModified(PROG_STATUS_UNLOAD))
	{
		if (GetModified(PROG_STATUS_BAND)) {
			FormatPwmSeq(HW_STBY_PWM_AC);
			FormatWaitSeq(PROG_STATUS_BAND, 100);
		} 
		else if (GetModified(PROG_STATUS_LOAD | PROG_STATUS_OUT) && STATEOUT_OFF == m_command.outState)
		{
			FormatPwmSeq(HW_STBY_PWM_AC);
			FormatWaitSeq(PROG_STATUS_ALL, 10);
		}


		FormatByteSeq(PROG_STATUS_LOAD | PROG_STATUS_OUT, 5, STATEOUT_ON == m_command.outState ? CS05_PW2_AC : CS05_PW2_STBY);
		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_OUT, 10);

		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_BAND, 100);
		FormatByteSeq(PROG_STATUS_LOAD | PROG_STATUS_BAND, 9, m_command.bandIdx == EXBAND_ACU_10V ? CS09_PW_75V : CS09_PW_STBY);

		// For invisibility frame
		FormatByteSeq(PROG_STATUS_FREQ_BAND, 6, CS06_AC10V_STBY);
		FormatWaitSeq(PROG_STATUS_FREQ_BAND, 10);

		FormatFreqSeq(PROG_STATUS_LOAD | PROG_STATUS_FREQ | PROG_STATUS_FREQ_BAND, m_command.frequency, sm_arrBandsACUFreq, m_command.bandIdxFreq);
		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_FREQ | PROG_STATUS_FREQ_BAND, 10);

		// For invisibility frame
		FormatWaitSeq(PROG_STATUS_FREQ_BAND, INVISIBILITY_FRAME_LENGTH);

		if (STATEOUT_ON == m_command.outState)
		{
			FormatBandSeq(PROG_STATUS_LOAD | PROG_STATUS_BAND | PROG_STATUS_SENSE | PROG_STATUS_OUT | PROG_STATUS_FREQ_BAND, sm_arrBandsToLLB, m_command.bandIdx, m_command.sense, 6);
			FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_BAND | PROG_STATUS_SENSE | PROG_STATUS_OUT, 10);
		}
		else
		{
			// Disconnect low voltage output because CS02_OUT_STBY cannot disconnect it
			FormatByteSeq(PROG_STATUS_LOAD | PROG_STATUS_OUT, 6, CS06_AC10V_STBY);
			FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_OUT, 10);
		}
		
		FormatDWordSeq(PROG_STATUS_LOAD | PROG_STATUS_OUT | PROG_STATUS_SENSE, 2, STATEOUT_ON == m_command.outState ?
			(STATESENSE_TWO_WIRE == m_command.sense ? CS02_OUT_MAIN_CAL & ~(CS02_OUT_U_CTRL | CS02_SENSE_CTRL) : CS02_OUT_MAIN_CAL & ~CS02_OUT_U_CTRL) 
			: CS02_OUT_STBY);
		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_OUT, 10);

		if (STATEOUT_OFF != m_command.outState)
			FormatBandPwmSeq(PROG_STATUS_LOAD | PROG_STATUS_VALUE | PROG_STATUS_BAND | PROG_STATUS_OUT, MODE_ACU, m_command.bandIdx, m_command.value);
	}
	else
	{
		FormatBaseUnload();
		
		FormatFreqSeq(PROG_STATUS_UNLOAD, HW_STBY_FREQ, sm_arrBandsACUFreq, EXTBAND_ACU_FREQ_1K);
		FormatByteSeq(PROG_STATUS_UNLOAD, 9, CS09_PW_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 6, CS06_AC10V_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 5, CS05_PW2_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);
	}

	g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPROG_END_CMD;
	g_unLenSeqBuff++;

	return g_lowLevelSeqBuff;
}
