/*
 * LLProgAC100V.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef LLPROGAC100V_H_
#define LLPROGAC100V_H_

#include "../LLProgACBase.h"

typedef enum { POWERAMP100RANGE_UNKNOWN, POWERAMP100RANGE_10HZ_100KHZ, POWERAMP100RANGE_100KHZ_1MHZ } POWERAMP100RANGE;

class LLProgAC100V  : public LLProgACBase
{
public:
	LLProgAC100V();
	virtual ~LLProgAC100V() {};

	virtual const uint8_t* GetLowLevelSequence() const;
	virtual PROG GetProgId() { return PROG_AC100V; };

protected:
	virtual bool ApplyCommandInternalAC(PCCOMMAND pCommand);
	virtual void ApplyCommandForce(PCCOMMAND pCommand);

private:
	POWERAMP100RANGE m_eAmpFreqRange;
};

#endif /* LLPROGAC100V_H_ */
