/*
 * LLProgAC1000V.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef LLPROGAC1000V_H_
#define LLPROGAC1000V_H_

#include "../LLProgACBase.h"

typedef enum { POWERAMPRANGE_UNKNOWN, POWERAMPRANGE_30HZ_5KHZ, POWERAMPRANGE_5KHZ_20KHZ, POWERAMPRANGE_20KHZ_100KHZ } POWERAMPRANGE;

class LLProgAC1000V  : public LLProgACBase
{
public:
	LLProgAC1000V();
	virtual ~LLProgAC1000V() {};

	virtual const uint8_t* GetLowLevelSequence() const;
	virtual PROG GetProgId() { return PROG_AC1000V; };
	virtual void Reset();

protected:
	virtual void ApplyCommandForce(PCCOMMAND pCommand);
	virtual bool ApplyCommandInternalAC(PCCOMMAND pCommand);

private:
	void FormatTransformerSeq(const uint32_t unCheckFlags, POWERAMPRANGE eNewFreqRange, STATESENSE sense) const;
	void FormatAmplifierSeq(const uint32_t unCheckFlags, POWERAMPRANGE eNewFreqRange) const;
	void PreLoad() const;
	void SoftChangeFrequencyBand() const;

	void FormatOutSwitchSeq(bool bTurnOn, double dNewFreq) const;
	void FormatONOutSwitchSeq(double dOldVoltage, double dNewVoltage, double dNewFreq) const;
	void FormatOFFOutSwitchSeq(double dOldVoltage, double dNewVoltage, double dNewFreq) const;
	void FormatStepByStepPwmSeq(double dOldVoltage, double dNewVoltage, double dNewFreq) const;

private:
	POWERAMPRANGE m_eAmpFreqRange;
	double m_dPrevVoltage;
};

#endif /* LLPROGAC1000V_H_ */
