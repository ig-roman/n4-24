/*
 * LLProgAcCurrents.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "LLProgAcCurrents.h"

// Dependency: sm_arrBandsACI
const IBANDTOLLB sm_arrACIBandsToLLB[] =
{
	{ EXTBAND_ACI_100UA,	CS01_CUR_100UA	},
	{ EXTBAND_ACI_1MA,		CS01_CUR_1MA	},
	{ EXTBAND_ACI_10MA,		CS01_CUR_10MA	},
	{ EXTBAND_ACI_100MA,	CS01_CUR_100MA	},
	{ EXTBAND_ACI_1A,		CS01_CUR_1A		},
	{ EXTBAND_ACI_2A,		CS01_CUR_2A		},
	{ EXTBAND_ACI_10A_AMP,	ILLEGAL_STATE	},	// Current amplifier uses voltage to operate with current
	{ EXTBAND_ACI_30A_AMP,	ILLEGAL_STATE	}	// Current amplifier uses voltage to operate with current
};

bool LLProgAcCurrents::ApplyCommandInternalAC(PCCOMMAND pCommand)
{
	return STATEMODE_I == pCommand->mode && 0 < pCommand->frequency;
}

static uint8_t s_currentBand, s_currentOut;

const uint8_t* LLProgAcCurrents::GetLowLevelSequence() const
{

	g_unLenSeqBuff = 0;

	if (!GetModified(PROG_STATUS_UNLOAD))
	{
		FormatByteSeq(PROG_STATUS_LOAD, 5, CS05_PW2_AC);
		FormatWaitSeq(PROG_STATUS_LOAD, 10);

		FormatByteSeq(PROG_STATUS_LOAD, 6, CS06_AC10V_10V_4W);
		FormatByteSeq(PROG_STATUS_LOAD, 9, CS09_PW_75V);

		// For invisibility frame
		FormatByteSeq(PROG_STATUS_FREQ_BAND, 1, s_currentBand);
		FormatWaitSeq(PROG_STATUS_FREQ_BAND, 10);
		
		FormatFreqSeq(PROG_STATUS_LOAD | PROG_STATUS_FREQ | PROG_STATUS_FREQ_BAND, m_command.frequency, sm_arrBandsACIFreq, m_command.bandIdxFreq);
		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_FREQ | PROG_STATUS_FREQ_BAND, 10);

		// For invisibility frame
		FormatWaitSeq(PROG_STATUS_FREQ_BAND, INVISIBILITY_FRAME_LENGTH);

		FormatCurrentBandSeq(PROG_STATUS_LOAD | PROG_STATUS_BAND, sm_arrACIBandsToLLB, m_command.bandIdx);
		if (GetModified(PROG_STATUS_LOAD | PROG_STATUS_BAND)) {
			s_currentBand = g_lowLevelSeqBuff[g_unLenSeqBuff - 1];
			if (!s_currentOut)
				g_lowLevelSeqBuff[g_unLenSeqBuff - 1] = CS01_CUR_STBY;
		}
		FormatWaitSeq(PROG_STATUS_LOAD | PROG_STATUS_FREQ_BAND, 100);

		FormatBandPwmSeq(PROG_STATUS_LOAD | PROG_STATUS_VALUE | PROG_STATUS_BAND, MODE_ACI, m_command.bandIdx, m_command.value);
		FormatWaitSeq(PROG_STATUS_LOAD, 500);

		if (GetModified(PROG_STATUS_OUT)) {
			s_currentOut = STATEOUT_ON == m_command.outState;
		}
		FormatDWordSeq(PROG_STATUS_OUT, 2, s_currentOut ? CS02_OUT_MAIN_I : CS02_OUT_STBY);
		FormatByteSeq(PROG_STATUS_OUT | PROG_STATUS_FREQ_BAND, 1, s_currentOut ? s_currentBand : CS01_CUR_STBY);
	}
	else
	{
		FormatByteSeq(PROG_STATUS_UNLOAD, 1, CS01_CUR_STBY);

		FormatBaseUnload();
		
		// ACU may be used here.
		FormatFreqSeq(PROG_STATUS_UNLOAD, HW_STBY_FREQ, sm_arrBandsACUFreq, EXTBAND_ACU_FREQ_1K);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 9, CS09_PW_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 6, CS06_AC10V_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 5, CS05_PW2_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 1, CS01_CUR_STBY);
	}

	g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPROG_END_CMD;
	g_unLenSeqBuff++;

	return g_lowLevelSeqBuff;
}
