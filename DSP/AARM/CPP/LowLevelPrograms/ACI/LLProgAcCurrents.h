/*
 * LLProgAcCurrents.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef LLPROGACCURRENTS_H_
#define LLPROGACCURRENTS_H_

#include "../LLProgACBase.h"

class LLProgAcCurrents : public LLProgACBase
{
public:
	LLProgAcCurrents() : LLProgACBase() {};
	virtual ~LLProgAcCurrents() {};

	virtual const uint8_t* GetLowLevelSequence() const;
	virtual PROG GetProgId() { return PROG_ACI; };

protected:
	virtual bool ApplyCommandInternalAC(PCCOMMAND pCommand);
};

#endif /* LLPROGACCURRENTS_H_ */
