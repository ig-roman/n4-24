/*
 * LLProgCustom.cpp
 *
 *  Created on: May 14, 2015
 *      Author: Sergei Tero
 */

#include "LLProgCustom.h"

bool LLProgCustom::ApplyCommand(PCCOMMAND pCommand, bool bTestOnly)
{
	bool bRet = pCommand->unCustomComandLen > 0;

	if (bRet && !bTestOnly)
	{
		m_command.AssignCustomCommand(pCommand->pCustomComand, pCommand->unCustomComandLen);
	}

	return bRet;
}

const uint8_t* LLProgCustom::GetLowLevelSequence() const
{
	bool bUnload = GetModified(PROG_STATUS_UNLOAD);
	if (bUnload)
	{
		g_unLenSeqBuff = 0;

		FormatWHInitSequence(PROG_STATUS_UNLOAD);

		g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPROG_END_CMD;
		g_unLenSeqBuff++;
	}

	return bUnload ? g_lowLevelSeqBuff : m_command.pCustomComand;
}