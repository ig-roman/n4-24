/*
 * LLProgACBase.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "LLProgACBase.h"

const BANDTOLLB LLProgACBase::sm_arrBandsToLLB[] =
{
#ifdef USE_ACU_1MV
	{ EXBAND_ACU_1MV,	CS06_AC10V_1MV_2W,		CS06_AC10V_STBY		},
#endif
	{ EXBAND_ACU_10MV,	CS06_AC10V_10MV_2W,		CS06_AC10V_STBY		},
	{ EXBAND_ACU_100MV,	CS06_AC10V_100MV_2W,	CS06_AC10V_STBY		},
	{ EXBAND_ACU_1V,	CS06_AC10V_1V_2W,		CS06_AC10V_1V_4W	},
	{ EXBAND_ACU_10V,	CS06_AC10V_10V_2W,		CS06_AC10V_10V_4W	},
	{ EXBAND_ACU_100V,	0,						0					},
	{ EXBAND_ACU_1000V,	0,						0					},
	NULL
};

/** **********************************************************************
 *
 * Applies required values from new command to internal state of this object.
 * After this step object becomes completely synchronized with front panel.
 * Hardware state is updated by LowLevelExecuter it gets value from this object throw GetLowLevelSequence() function.
 *
 * @param pCommand	completed new command with values to update internal state of this object
 *
 *********************************************************************** */
void LLProgACBase::ApplyCommandForce(PCCOMMAND pCommand)
{
	if (pCommand->sense != m_command.sense)
	{
		m_command.sense = pCommand->sense;
		SetModified(PROG_STATUS_SENSE);
	}

	if (pCommand->frequency != m_command.frequency)
	{
		m_command.frequency = pCommand->frequency;
		SetModified(PROG_STATUS_FREQ);
	}

	if (pCommand->bandIdxFreq != m_command.bandIdxFreq)
	{
		m_command.bandIdxFreq = pCommand->bandIdxFreq;
		SetModified(PROG_STATUS_FREQ_BAND);
	}
}

/** **********************************************************************
 *
 * Checks that requested values from new command are suited to this object or not.
 * When values are suited this function invokes again and object copies required values
 * and becomes completely synchronized with front panel.
 * Hardware state is updated by LowLevelExecuter it gets value from this object throw GetLowLevelSequence() function.
 *
 * @param pCommand	completed new command with values to test them or update internal state of this object
 * @param bTestOnly	when value true function must decide that this command chan be handled or not
 *					when value false it means that function returned true before and must update internal state of this object
 *
 *********************************************************************** */
bool LLProgACBase::ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly)
{
	bool bRet = 0 < pCommand->frequency && ApplyCommandInternalAC(pCommand);

	if (bRet && !bTestOnly)
	{
		ApplyCommandForce(pCommand);
	}

	return bRet;
}
