/*
 * LLProgDcCurrents.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef LLPROGDCCURRENTS_H_
#define LLPROGDCCURRENTS_H_

#include "../LowLevelProgram.h"

class LLProgDcCurrents : public LowLevelProgram
{
public:
	LLProgDcCurrents() : LowLevelProgram() {};
	virtual ~LLProgDcCurrents() {};

	virtual const uint8_t* GetLowLevelSequence() const;
	virtual PROG GetProgId() { return PROG_DCI; };

protected:
	virtual bool ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly);
};

#endif /* LLPROGDCCURRENTS_H_ */
