/*
 * LLProgDcCurrents.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "LLProgDcCurrents.h"

// Dependency: sm_arrBandsDCI
const IBANDTOLLB sm_arrDCIBandsToLLB[] =
{
	{ EXTBAND_DCI_P_10UA,		CS01_CUR_100UA	},
	{ EXTBAND_DCI_P_100UA,		CS01_CUR_100UA	},
	{ EXTBAND_DCI_P_1MA,		CS01_CUR_1MA	},
	{ EXTBAND_DCI_P_10MA,		CS01_CUR_10MA	},
	{ EXTBAND_DCI_P_100MA,		CS01_CUR_100MA	},
	{ EXTBAND_DCI_P_1A,			CS01_CUR_1A		},
	{ EXTBAND_DCI_P_2A,			CS01_CUR_2A		},
	{ EXTBAND_DCI_P_10A_AMP,	ILLEGAL_STATE	},	// Current amplifier uses voltage to operate with current
	{ EXTBAND_DCI_P_30A_AMP,	ILLEGAL_STATE	},	// Current amplifier uses voltage to operate with current

	{ EXTBAND_DCI_N_10UA,		CS01_CUR_100UA	},
	{ EXTBAND_DCI_N_100UA,		CS01_CUR_100UA	},
	{ EXTBAND_DCI_N_1MA,		CS01_CUR_1MA	},
	{ EXTBAND_DCI_N_10MA,		CS01_CUR_10MA	},
	{ EXTBAND_DCI_N_100MA,		CS01_CUR_100MA	},
	{ EXTBAND_DCI_N_1A,			CS01_CUR_1A		},
	{ EXTBAND_DCI_N_2A,			CS01_CUR_2A		},
	{ EXTBAND_DCI_N_10A_AMP,	ILLEGAL_STATE	},	// Current amplifier uses voltage to operate with current
	{ EXTBAND_DCI_N_30A_AMP,	ILLEGAL_STATE	}	// Current amplifier uses voltage to operate with current
};

/** **********************************************************************
 *
 * Checks that requested values from new command are suited to this object or not.
 * When values are suited this function invokes again and object copies required values
 * and becomes completely synchronized with front panel.
 * Hardware state is updated by LowLevelExecuter it gets value from this object throw GetLowLevelSequence() function.
 *
 * @param pCommand	completed new command with values to test them or update internal state of this object
 * @param bTestOnly	when value true function must decide that this command chan be handled or not
 *					when value false it means that function returned true before and must update internal state of this object
 *
 *********************************************************************** */
bool LLProgDcCurrents::ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly)
{
	bool bRet = STATEMODE_I == pCommand->mode
		&& 0 == pCommand->frequency;

	if (bRet && !bTestOnly)
	{
		if (m_command.value > 0 != pCommand->value > 0)
		{
			// New value will be asigned later in LowLevelProgram::ApplyCommand()
			SetModified(PROG_STATUS_POLARITY);
		}
	}

	return bRet;
}

static uint8_t s_currentBand, s_currentOut;

const uint8_t* LLProgDcCurrents::GetLowLevelSequence() const
{

	g_unLenSeqBuff = 0;

	if (!GetModified(PROG_STATUS_UNLOAD))
	{
		if (GetModified(PROG_STATUS_LOAD | PROG_STATUS_POLARITY))
		{
			bool bIsPositive = IsPositiveBand(sm_arrBandsDCI + m_command.bandIdx);
			
			// Note from REIN: For currents sign must be inverted
			FormatByteSeq(PROG_STATUS_ALL, 5, !bIsPositive ? CS05_PW2_DC_PLUS : CS05_PW2_DC_MINUS);
			FormatWaitSeq(PROG_STATUS_ALL, 10);
		}

		FormatByteSeq(PROG_STATUS_LOAD, 8, CS08_DCVOL_10V_4W);
		FormatWaitSeq(PROG_STATUS_LOAD, 10);

		FormatCurrentBandSeq(PROG_STATUS_LOAD | PROG_STATUS_BAND, sm_arrDCIBandsToLLB, m_command.bandIdx);
		if (GetModified(PROG_STATUS_LOAD | PROG_STATUS_BAND)) {
			s_currentBand = g_lowLevelSeqBuff[g_unLenSeqBuff - 1];
			if (!s_currentOut)
				g_lowLevelSeqBuff[g_unLenSeqBuff - 1] = CS01_CUR_STBY;
		}
		FormatWaitSeq(PROG_STATUS_LOAD, 10);

		FormatBandPwmSeq(PROG_STATUS_LOAD | PROG_STATUS_VALUE | PROG_STATUS_BAND, MODE_DCI, m_command.bandIdx, m_command.value);
		FormatWaitSeq(PROG_STATUS_LOAD, 500);

		if (GetModified(PROG_STATUS_OUT)) {
			s_currentOut = STATEOUT_ON == m_command.outState;
		}
		FormatDWordSeq(PROG_STATUS_OUT, 2, s_currentOut ? CS02_OUT_MAIN_I : CS02_OUT_STBY);
		FormatByteSeq(PROG_STATUS_OUT, 1, s_currentOut ? s_currentBand : CS01_CUR_STBY);
	}
	else
	{
		FormatByteSeq(PROG_STATUS_UNLOAD, 1, s_currentBand | CS01_CUR_CTRL);

		FormatBaseUnload();

		FormatByteSeq(PROG_STATUS_UNLOAD, 1, CS01_CUR_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 8, CS08_DCVOL_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);

		FormatByteSeq(PROG_STATUS_UNLOAD, 5, CS05_PW2_STBY);
		FormatWaitSeq(PROG_STATUS_UNLOAD, 10);
	}

	g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPROG_END_CMD;
	g_unLenSeqBuff++;

	return g_lowLevelSeqBuff;
}

