/*
 * LowLevelProgram.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#include "LowLevelProgram.h"
#include "../CommonArmCpp.h"
#include "../../ArmInterface.h"
#include "../Workers/HiLevelExecuter.h"

#define INT_SCALE 100000

uint16_t g_unLenSeqBuff = 1;
uint8_t g_lowLevelSeqBuff[LLPROG_BUFFER_SIZE];
double g_dFreqCorrector;

/** **********************************************************************
 *
 * Informs this programm that it should be prepared for load or unload
 *
 * @param bLoad		if true programm should be prepared for loading othervise for unloading
 *
 *********************************************************************** */
void LowLevelProgram::Load(bool bLoad)
{
	// ST TODO: Set only PROG_STATUS_LOAD flag here and add this flag to almost every line in GetLowLevelSequence function
	// or set all flags here and remove from GetLowLevelSequence but it isn't clear.

	// Sets all falags modified instead of unload for load function call and load for unload function call
	m_lStatusBits = PROG_STATUS_ALL & ~(bLoad ? PROG_STATUS_UNLOAD : PROG_STATUS_LOAD);
}

/** **********************************************************************
 *
 * Tests or applies new command to this low level program.
 *
 * @param pCommand		new command with new data
 * @param bTestOnly		if true function skips syncronization phase
 *							othervise new values from acceptable command applies to internal state
 *
 * @return Returns true if command's values is compatible with this low-level program othervise false.
 *
 *********************************************************************** */
bool LowLevelProgram::ApplyCommand(PCCOMMAND pCommand, bool bTestOnly)
{
	bool bRet = 0 == pCommand->unCustomComandLen && ApplyCommandInternal(pCommand, bTestOnly);

	if (bRet && !bTestOnly)
	{
		// PROG_STATUS_SKIP_VALUE_CHANGE may be define in ACV1000V to hold PWM on the lowest value when output is off
		if (!GetModified(PROG_STATUS_SKIP_VALUE_CHANGE) && m_command.value != pCommand->value)
		{
			m_command.value = pCommand->value;
			SetModified(PROG_STATUS_VALUE);
		}

		if (pCommand->bandIdx != m_command.bandIdx)
		{
			m_command.bandIdx = pCommand->bandIdx;
			SetModified(PROG_STATUS_BAND);
		}

		if (m_command.outState != pCommand->outState)
		{
			m_command.outState = pCommand->outState;
			SetModified(PROG_STATUS_OUT);
		}
	}

	return bRet;
}

/** **********************************************************************
 *
 * Resets all cached data
 *
 *********************************************************************** */
void LowLevelProgram::Reset()
{
	m_command.ResetCommand();
}

/** **********************************************************************
 *
 * Formats SPI data to transfer 8bits
 *
 * @param unCheckFlags	modification flags
 * @param unAddress		CS address from 1 to 15
 * @param unData		data to send
 *
 *********************************************************************** */
void LowLevelProgram::FormatByteSeq(uint32_t unCheckFlags, uint8_t unAddress, uint8_t byData) const
{
	if (GetModified(unCheckFlags))
	{
		g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPGR_CS_AND_ADDR_CMD | (unAddress & LLPGR_CS_MASK);
		g_unLenSeqBuff++;

		g_lowLevelSeqBuff[g_unLenSeqBuff] = 1;
		g_unLenSeqBuff++;

		g_lowLevelSeqBuff[g_unLenSeqBuff] = byData;
		g_unLenSeqBuff++;
	}
}

/** **********************************************************************
 *
 * Formats SPI data to transfer 16bits
 *
 * @param unCheckFlags	modification flags
 * @param unAddress		CS address from 1 to 15
 * @param unData		data to send
 *
 *********************************************************************** */
void LowLevelProgram::FormatWordSeq(uint32_t unCheckFlags, uint8_t unAddress, uint16_t unData) const
{
	if (GetModified(unCheckFlags))
	{
		// Command and chip select
		g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPGR_CS_AND_ADDR_CMD | (unAddress & LLPGR_CS_MASK);
		g_unLenSeqBuff++;

		// Length of data
		g_lowLevelSeqBuff[g_unLenSeqBuff] = 2;
		g_unLenSeqBuff++;
		
		g_lowLevelSeqBuff[g_unLenSeqBuff]		= (uint8_t)(unData >> 8);
		g_lowLevelSeqBuff[g_unLenSeqBuff + 1]	= (uint8_t)unData;
		
		// Doesn't work on ARM
		// *((uint16_t*)(g_lowLevelSeqBuff + g_unLenSeqBuff)) = unData;

		g_unLenSeqBuff += 2;
	}
}

void LowLevelProgram::FormatDWordSeq(uint32_t unCheckFlags, uint8_t unAddress, uint32_t unData) const
{
	if (GetModified(unCheckFlags))
	{
		// Command and chip select
		g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPGR_CS_AND_ADDR_CMD | (unAddress & LLPGR_CS_MASK);
		g_unLenSeqBuff++;

		// Length of data
		g_lowLevelSeqBuff[g_unLenSeqBuff] = 3;
		g_unLenSeqBuff++;

		g_lowLevelSeqBuff[g_unLenSeqBuff] = (uint8_t)(unData >> 16);
		g_lowLevelSeqBuff[g_unLenSeqBuff + 1] = (uint8_t)(unData >> 8);
		g_lowLevelSeqBuff[g_unLenSeqBuff + 2] = (uint8_t)unData;

		g_unLenSeqBuff += 3;
	}
}

/** **********************************************************************
 *
 * Formats delay command
 *
 * @param unCheckFlags	modification flags
 * @param unDelayMs		delay in ms
 *
 *********************************************************************** */
void LowLevelProgram::FormatWaitSeq(uint32_t unCheckFlags, uint32_t unDelayMs) const
{
	if (GetModified(unCheckFlags))
	{
		g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPGR_DELAY_CMD;
		g_unLenSeqBuff++;

		g_lowLevelSeqBuff[g_unLenSeqBuff] = unDelayMs / 10;
		g_unLenSeqBuff++;
	}
}

/** **********************************************************************
 *
 * Formats SPI data for selected band
 *
 * @param unCheckFlags	modification flags
 * @param pBandsToLLB	mapping of hi-level bands to low level
 * @param unBandIdx		hi-level band index
 * @param enumSense		type of sense
 * @param unCS			low level bands register
 *
 *********************************************************************** */
void LowLevelProgram::FormatBandSeq(uint32_t unCheckFlags, const BANDTOLLB* pBandsToLLB, uint8_t unBandIdx, STATESENSE enumSense, uint8_t unCS) const
{
	if (GetModified(unCheckFlags))
	{
		bool b2wire = STATESENSE_TWO_WIRE == enumSense;
		uint8_t byData = b2wire ? pBandsToLLB[unBandIdx].band2w : pBandsToLLB[unBandIdx].band4w;
		FormatByteSeq(unCheckFlags, unCS, byData);

	}
}

/** **********************************************************************
 *
 * Normalizes value and formats data for 2 pulse-width modulators (PWMs).
 * Normalization means that almost for every band, PWM value shold be from 0.98v - 10v.
 *
 * @param unCheckFlags	modification flags
 * @param pBandsToLLB	mapping of hi-level bands to 10v band
 * @param unBandIdx		selected hi-level band index
 * @param dValue		required voltage/current on device output.
 *
 *********************************************************************** */
void LowLevelProgram::FormatBandPwmSeq(uint32_t unCheckFlags, MODE eMode, uint8_t unBandIdx, double dValue) const
{
	if (GetModified(unCheckFlags))
	{
		double dTo10Vband = dValue * ::BandToPwmMultiplier(eMode, unBandIdx);
		FormatPwmSeq(dTo10Vband);
	}
}

/** **********************************************************************
 *
 * Formats data for 2 pulse-width modulators (PWMs).
 * Both PWMs works on 100Hz and can be devided up to PWM_TICKS_COUNT parts.
 * The first PWM is corse second is fine. When both PWMs equeal 100% (PWM_TICKS_COUNT) voltage after PWM filter is PWM_U_REF_VOL.
 *
 * For more information about PWM see "KALIBRAATORI JUHTIMISPROGRAMM VER2 23.03.14.sch.pdf" document
 *
 * @param dValue	required references voltage after PWM filters. It may be positive or negative but output voltage is always positive.
 *
 *********************************************************************** */
void LowLevelProgram::FormatPwmSeq(double dValue) const
{
	if (dValue < 0)
	{
		dValue = -dValue;
	}

	if (PWM_U_REF_VOL < dValue)
	{
		CARM_SetLastError(AERROR_OVERLIMIT_PWM);
		dValue = PWM_U_REF_VOL;
	}

	g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPGR_PWM_CMD;
	g_unLenSeqBuff++;
	
	uint16_t scale = (uint16_t)((float)PWM_MAX_VAL / PWM_RS);
	double dRefPortion = dValue / PWM_U_REF_VOL;
	double  dPwm1Portion = (dRefPortion * PWM_RS - 1) / PWM_R1;
	uint16_t unPwm1 = dPwm1Portion < 0 ? 0 : ((uint16_t)(dPwm1Portion * PWM_MAX_VAL / scale)) * scale + scale;
	double dPwm2Portion = dRefPortion * PWM_RS / PWM_R2 - PWM_R1 * (double)unPwm1 / (PWM_MAX_VAL * PWM_R2);
	uint16_t unPwm2 = (uint16_t)(dPwm2Portion * PWM_MAX_VAL + 0.5);
	
	g_lowLevelSeqBuff[g_unLenSeqBuff]		= (uint8_t)(unPwm2);
	g_lowLevelSeqBuff[g_unLenSeqBuff + 1]	= (uint8_t)(unPwm2 >> 8);
	g_lowLevelSeqBuff[g_unLenSeqBuff + 2]	= (uint8_t)(unPwm1);
	g_lowLevelSeqBuff[g_unLenSeqBuff + 3]	= (uint8_t)(unPwm1 >> 8);

	// Doesn't work on ARM
	//*((uint32_t*)(g_lowLevelSeqBuff + g_unLenSeqBuff)) = (((uint32_t)unPwm1) << 16) + unPwm2;
	g_unLenSeqBuff += 4;
}

/** **********************************************************************
 *
 * Common part of code for all unload programms.
 *
 *********************************************************************** */
void LowLevelProgram::FormatBaseUnload() const
{
	FormatDWordSeq(PROG_STATUS_UNLOAD, 2, CS02_OUT_STBY);
	FormatWaitSeq(PROG_STATUS_UNLOAD, 100);
	FormatPwmSeq(GetStandByPwmValue());
	FormatWaitSeq(PROG_STATUS_UNLOAD, 200);
}

/** **********************************************************************
 *
 * Formats SPI data for currents (CS01)
 *
 * @param unCheckFlags	modification flags
 * @param pBandsToLLB	array with mapping values
 * @param unBandIdx		hi-level band to remap
 *
 *********************************************************************** */
void LowLevelProgram::FormatCurrentBandSeq(uint32_t unCheckFlags, const IBANDTOLLB* pBandsToLLB, uint8_t unBandIdx) const
{
	if (GetModified(unCheckFlags))
	{
		uint8_t unRegVal = pBandsToLLB[unBandIdx].band;
		FormatByteSeq(unCheckFlags, 1, unRegVal);
	}
}

/** **********************************************************************
 *
 * Formats SPI data for 2 generators corse (CS03) and fine(CS04).
 * For formula description see "KALIBRAATORI JUHTIMISPROGRAMM VER2 23.03.14.sch.pdf" document
 *
 * @param unCheckFlags	modification flags
 * @param dFreq			new frequency value
 * @param pBands		hi-level frequency bands array for ACI or ACU
 * @param bandIdxFreq	required hi-level band index
 *
 *********************************************************************** */
void LowLevelProgram::FormatFreqSeq(uint32_t unCheckFlags, double dFreq, PCBAND pBands, uint8_t bandIdxFreq) const
{
	if (!GetModified(unCheckFlags))
		return;
  	PCBAND pBand = pBands + bandIdxFreq;
	// dFreq < band.min || dFreq > band.max
	if (IsGreater(pBand->min, dFreq, pBand->fraction) || IsGreater(dFreq, pBand->max, pBand->fraction))
	{
		CARM_SetLastError(AERROR_FREQ_OUT_OF_RANGE);
		return;
	}

	double curFreq = g_hiLevelExecuter.GetLastCommand()->frequency;
	const double freqMultiplier = 10.0;
	if (curFreq > 0 && curFreq != UNKNOWN_FREQUENCY && dFreq > curFreq)
	{
		while (dFreq / curFreq > freqMultiplier)
		{
			curFreq *= freqMultiplier;
			if (IsGreater(pBand->min, curFreq, pBand->fraction) || IsGreater(curFreq, pBand->max, pBand->fraction))
			{
				for (int nIdx = 0; 0 != pBands[nIdx].min; nIdx++)
				{
					pBand = pBands + nIdx;
					if (pBand->min <= curFreq && pBand->max >= curFreq)
						break;
				}
			}
			FormatFreqSeqRegs(curFreq, pBand);
			FormatWaitSeq(PROG_STATUS_ALL, 10);
		}
	}
	FormatFreqSeqRegs(dFreq, pBands + bandIdxFreq);
}

void LowLevelProgram::FormatFreqSeqRegs(double dFreq, PCBAND pBand) const
{
	int8_t uNPow = pBand->scale - pBand->fraction - 2;
	double dMantissa = (dFreq - g_dFreqCorrector * dFreq / (1 + g_dFreqCorrector)) / Pow10Fast(uNPow); // Sould be from 0.98 to 10.5
	
	// CORSE generator (RC oscillator)
	// Extract single numbers from value. For example: 2.589999 means K = 2, L = 5, M = 8
	// Don't use floating point here bacase it always leads to rounding problem for K,L,M
	uint32_t unMantCoarse = dMantissa >= 1 ? (uint32_t)roundX(dMantissa * INT_SCALE, 0) : INT_SCALE;
	uint8_t unK = (uint8_t)(unMantCoarse / INT_SCALE);
	uint8_t unM = (uint8_t)(unMantCoarse % 10000 / 1000);
	uint8_t unL = (uint8_t)(((unMantCoarse - unM) % INT_SCALE) / 10000);
			
	// Prescaler value
	uint16_t unRegVal = (uint16_t)(sm_arrFreqCoarsePow[uNPow - 1]) << 13;
	
	// Resistors value
	unRegVal |= (uint16_t)(sm_arrFreqCoarseData[unK - 1].K) << 1;	// K range is 1 to 10
	unRegVal |= (uint16_t)(sm_arrFreqCoarseData[unL].LM) << 5;		// L range is 0 to 9
	unRegVal |= (uint16_t)(sm_arrFreqCoarseData[unM].LM) << 9;		// M range is 0 to 9
	FormatWordSeq(PROG_STATUS_ALL, 3, unRegVal);
	
	// FINE generator (digial synthesizer)
	uint32_t unDpahse = (uint32_t)(0x100000000L * (dMantissa / 100.0));
	
	// Command and chip select 4
	g_lowLevelSeqBuff[g_unLenSeqBuff] = LLPGR_CS_AND_ADDR_CMD | (4 & LLPGR_CS_MASK);
	g_unLenSeqBuff++;
	
	// Length of data 5 bytes
	g_lowLevelSeqBuff[g_unLenSeqBuff] = 5;
	g_unLenSeqBuff++;
			
	// For AD9850 all bytes and bits order should be inverted for details see AD9850.pdf datasheet
	// The first most significant byte always 0 W39 - W32
	g_lowLevelSeqBuff[g_unLenSeqBuff + 4] = 0;
	
	g_lowLevelSeqBuff[g_unLenSeqBuff + 3] = swap_endian((uint8_t)(unDpahse >> 24));
	g_lowLevelSeqBuff[g_unLenSeqBuff + 2] = swap_endian((uint8_t)(unDpahse >> 16));
	g_lowLevelSeqBuff[g_unLenSeqBuff + 1] = swap_endian((uint8_t)(unDpahse >> 8));
	g_lowLevelSeqBuff[g_unLenSeqBuff + 0] = swap_endian((uint8_t)unDpahse);
	
	g_unLenSeqBuff +=5;
}

/** **********************************************************************
 *
 * Creates program to initialize device from cold start
 * Writes to all registers defaults states
 *
 * @param unCheckFlags	modification flags
 *
 *********************************************************************** */
void LowLevelProgram::FormatWHInitSequence(uint32_t unCheckFlags) const
{
	FormatByteSeq(unCheckFlags, 5, CS05_PW2_STBY);
	FormatByteSeq(unCheckFlags, 10, CS10_PW450V_STBY);
	FormatByteSeq(unCheckFlags, 9, CS09_PW_STBY);
	FormatByteSeq(unCheckFlags, 1, CS01_CUR_STBY);
	FormatDWordSeq(unCheckFlags, 2, CS02_OUT_STBY);
	FormatByteSeq(unCheckFlags, 11, CS11_AMP1KV_STBY);
	FormatByteSeq(unCheckFlags, 12, CS12_TRANS1KV_STBY);
	FormatByteSeq(unCheckFlags, 8, CS08_DCVOL_STBY);
	FormatByteSeq(unCheckFlags, 6, CS06_AC10V_STBY);
	FormatWordSeq(unCheckFlags, 3, CS03_FCOARSE_STBY);

	FormatFreqSeq(unCheckFlags, HW_STBY_FREQ, sm_arrBandsACUFreq, EXTBAND_ACU_FREQ_1K);
	FormatPwmSeq(GetStandByPwmValue());

	FormatByteSeq(unCheckFlags, 7, CS07_AC100V_STBY);
}