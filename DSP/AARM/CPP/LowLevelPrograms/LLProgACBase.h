/*
 * LLProgACBase.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef LLProgACBase_H_
#define LLProgACBase_H_

#include "LowLevelProgram.h"

class LLProgACBase : public LowLevelProgram
{
public:
	LLProgACBase() : LowLevelProgram() {};
	virtual ~LLProgACBase() {};

protected:
	virtual bool ApplyCommandInternalAC(PCCOMMAND pCommand) = 0;
	virtual bool ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly);
	virtual void ApplyCommandForce(PCCOMMAND pCommand);
	virtual double GetStandByPwmValue() const { return HW_STBY_PWM_AC; };

protected:
	static const BANDTOLLB sm_arrBandsToLLB[];
};

#endif /* LLProgACBase_H_ */
