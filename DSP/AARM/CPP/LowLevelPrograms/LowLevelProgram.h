/*
 * LowLevelProgram.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Sergei Tero
 */

#ifndef LOWLEVELPROGRAM_H_
#define LOWLEVELPROGRAM_H_

#include "../../CommonTypes.h"
#include "../CommonArmCpp.h"

// For more information about tables see KALIBRAATORI JUHTIMISPROGRAMM VER2 23.03.14.sch.pdf document

// CS01 (Table 5)
#define CS01_CUR_STBY					B8(11010000)			// STS26 STBY
#define CS01_CUR_100UA					B8(10000000)			// STS27 100uA DC/AC
#define CS01_CUR_1MA					B8(00000001)			// STS28 1mA DC/AC
#define CS01_CUR_10MA					B8(00000010)			// STS29 10mA DC/AC
#define CS01_CUR_100MA					B8(00000100)			// STS30 100mA DC/AC
#define CS01_CUR_1A						B8(00101000)			// STS31 1A DC/AC
#define CS01_CUR_2A						B8(00001000)			// STS32 2A DC/AC

#define CS01_CUR_CTRL					0x8

// CS02 (Table 6)
#define CS02_OUT_STBY					B32(01110000, 10111001, 00000000, 0)	// STS39 STBY
#define CS02_OUT_MAIN_CAL				CS02_OUT_STBY
#define CS02_OUT_MAIN_I					B32(01100000, 10110001, 00000000, 0)	// STS40 MAIN CAL.
#define CS02_OUT_HVAC					B16(10011000, 01000001) // STS44 AC HV (1000 V)

#define CS02_OUT_2W_0R					B16(00101000, 01000001)	// 2Wire sense 0 Ohms
#define CS02_OUT_2W_1R					B16(00101000, 01000010)	// 2Wire sense 1 Ohms
#define CS02_OUT_2W_10R					B16(00101000, 01000100)	// 2Wire sense 10 Ohms
#define CS02_OUT_2W_100R				B16(00101000, 01001000)	// 2Wire sense 100 Ohms
#define CS02_OUT_2W_1KR					B16(00101000, 01010000)	// 2Wire sense 1K Ohms
#define CS02_OUT_2W_10KR				B16(00101000, 01100000)	// 2Wire sense 10K Ohms
#define CS02_OUT_2W_100KR				B16(00101000, 10000000)	// 2Wire sense 100K Ohms
#define CS02_OUT_2W_1MR					B16(00101001, 00000000)	// 2Wire sense 1M Ohms
#define CS02_OUT_2W_10MR				B16(00101010, 00000000)	// 2Wire sense 10M Ohms
#define CS02_OUT_2W_100MR				B16(00101100, 00000000)	// 2Wire sense 100M Ohms

#define CS02_OUT_4W_0R					B16(10101000, 01000001)	// 4Wire sense 0 Ohms
#define CS02_OUT_4W_1R					B16(10101000, 01000010)	// 4Wire sense 1 Ohms
#define CS02_OUT_4W_10R					B16(10101000, 01000100)	// 4Wire sense 10 Ohms
#define CS02_OUT_4W_100R				B16(10101000, 01001000)	// 4Wire sense 100 Ohms
#define CS02_OUT_4W_1KR					B16(10101000, 01010000)	// 4Wire sense 1K Ohms
#define CS02_OUT_4W_10KR				B16(10101000, 01100000)	// 4Wire sense 10K Ohms
#define CS02_OUT_4W_100KR				B16(10101000, 10000000)	// 4Wire sense 100K Ohms
#define CS02_OUT_4W_1MR					B16(10101001, 00000000)	// 4Wire sense 1M Ohms
#define CS02_OUT_4W_10MR				B16(10101010, 00000000)	// 4Wire sense 10M Ohms
#define CS02_OUT_4W_100MR				B16(10101100, 00000000)	// 4Wire sense 100M Ohms

#define CS02_STBY_CTRL					0x800
#define CS02_SENSE_CTRL					0x2
#define CS02_OUT_U_CTRL					0x4
#define CS02_OUT_IR_CTRL				0x8

// CS03 (Table 12)
#define CS03_FCOARSE_STBY				B16(00110010, 01010011)	// STS74 STBY(625Hz)

// CS05 (Table 11)
#define CS05_PW2_STBY					B8(00000000)			// STS70 STBY
#define CS05_PW2_AC						B8(11000000)			// STS71 AC
#define CS05_PW2_DC_PLUS				B8(10010000)			// STS72 DC+ If U >= 0
#define CS05_PW2_DC_MINUS				B8(10100000)			// STS73 DC- If U < 0

// CS06 (Table 3)
#define CS06_AC10V_STBY					B8(00000000)			// STS13 STBY
#define CS06_AC10V_1MV_2W				B8(00111001)			// STS14 0,001V 2W
#define CS06_AC10V_10MV_2W				B8(00110101)			// STS15 0,01V 2W
#define CS06_AC10V_100MV_2W				B8(00110011)			// STS16 0,1V 2W
#define CS06_AC10V_1V_2W				B8(10110000)			// STS17 1V 2W
#define CS06_AC10V_10V_2W				B8(11010000)			// STS18 10V 2W
#define CS06_AC10V_1V_4W				B8(10100000)			// STS19 1V 4W
#define CS06_AC10V_10V_4W				B8(11000000)			// STS20 10V 4W

// CS07 (Table 4)
#define CS07_AC100V_STBY				B8(00000000)			// STS21 STBY
#define CS07_AC100V_100KHZ_2W			B8(01110000)			// STS22 100V 10Hz - 100kHz 2 wire sense
#define CS07_AC100V_1MHZ_2W				B8(11010000)			// STS23 100V 100kHz - 1MHz 2 wire sense
#define CS07_AC100V_100KHZ_4W			B8(01100000)			// STS24 100V 10Hz - 100kHz 4 wire sense
#define CS07_AC100V_1MHZ_4W				B8(11000000)			// STS25 100V 100kHz - 1MHz 4 wire sense

// CS08 (Table 1)
#define CS08_DCVOL_STBY					B8(00100000)			// STS0 STBY
#define CS08_DCVOL_10MV_2W				B8(00100011)			// STS1 0,01V 2W
#define CS08_DCVOL_100MV_2W				B8(01000011)			// STS2 0,1V 2W
#define CS08_DCVOL_1V_2W				B8(10000011)			// STS3 1V 2W
#define CS08_DCVOL_10V_2W				B8(00010011)			// STS4 10V 2W
#define CS08_DCVOL_100V_2W				B8(00011011)			// STS5 100V 2W
#define CS08_DCVOL_1000V_2W				B8(00010111)			// STS6 1000V 2W
#define CS08_DCVOL_10MV_4W				B8(00100010)			// STS7 0,01V 4W
#define CS08_DCVOL_100MV_4W				B8(01000010)			// STS8 0,1V 4W
#define CS08_DCVOL_1V_4W				B8(10000010)			// STS9 1V 4W
#define CS08_DCVOL_10V_4W				B8(00010010)			// STS10 10V 4W
#define CS08_DCVOL_100V_4W				B8(00011010)			// STS11 100V 4W
#define CS08_DCVOL_1000V_4W				B8(00010110)			// STS12 1000V 4W

// CS09 (Table 10)
#define CS09_PW_STBY					B8(00000000)			// STS65 STBY
#define CS09_PW_75V						B8(11100000)			// STS67 +-75V
#define CS09_PW_190V					B8(11000000)			// STS67 +-190V DC-

// CS10 (Table 9)
#define CS10_PW450V_STBY				B8(00000000)			// STS64 STBY
#define CS10_PW450V_READY				B8(01000000)			// STS65 READY
#define CS10_PW450V_VOLTUP				B8(11000000)			// STS66 VOLTUP

// CS11 (Table 7)
#define CS11_AMP1KV_STBY				B8(00000000)			// STS51 STBY
#define CS11_AMP1KV_DC					B8(00000001)			// STS52 1000 V DC
#define CS11_AMP1KV_30HZ_5KHZ			B8(00000110)			// STS53 30Hz...5kHz
#define CS11_AMP1KV_5KHZ_20KHZ			B8(00001010)			// STS54 5kHz...20kHz
#define CS11_AMP1KV_20KHZ_100KHZ		B8(10000010)			// STS55 20kHz...100kHz

// CS12 (Table 8)
#define CS12_TRANS1KV_STBY				B8(00000000)			// STS56 STBY
#define CS12_TRANS1KV_DC				B8(00001010)			// STS57 1000 V DC
#define CS12_TRANS1KV_30HZ_5KHZ_2W		B8(01010100)			// STS58 30Hz...5kHz 2W
#define CS12_TRANS1KV_5KHZ_20KHZ_2W		B8(00111100)			// STS59 5kHz...20kHz 2W
#define CS12_TRANS1KV_20KHZ_100KHZ_2W	B8(10110100)			// STS60 20kHz...100kHz 2W
#define CS12_TRANS1KV_30HZ_5KHZ_4W		B8(01010000)			// STS61 30Hz...5kHz 4W
#define CS12_TRANS1KV_5KHZ_20KHZ_4W		B8(00111000)			// STS62 5kHz...20kHz 4W
#define CS12_TRANS1KV_20KHZ_100KHZ_4W	B8(10110000)			// STS63 20kHz...100kHz 4W

#define ILLEGAL_STATE					0

#define HW_STBY_PWM				0	// V
#define HW_STBY_PWM_AC			0.9 // V Electrical part of device cannot operate properly when PWM set to 0
#define HW_STBY_FREQ			625 // Hz

// Custom statuses from 0x00000001 to 0x80000000
// Statuses can have same values because they used in different LL programs
#define PROG_STATUS_POWER_FREQ_RANGE	0x00000001
#define PROG_STATUS_POWER100_FREQ_RANGE	0x00000001

// Common statuses from 0x80000000 to 0x00000001
#define PROG_STATUS_SKIP_VALUE_CHANGE	0x00400000 // Is used for ACV 1000v to hold PWM on the lowest value when output is off
#define PROG_STATUS_FREQ_BAND			0x00800000
#define PROG_STATUS_FREQ				0x01000000
#define PROG_STATUS_SENSE				0x02000000
#define PROG_STATUS_BAND				0x04000000
#define PROG_STATUS_POLARITY			0x08000000
#define PROG_STATUS_VALUE				0x10000000
#define PROG_STATUS_OUT					0x20000000
#define PROG_STATUS_LOAD				0x40000000
#define PROG_STATUS_UNLOAD				0x80000000
#define PROG_STATUS_ALL					(0xFFFFFFFF & (~PROG_STATUS_SKIP_VALUE_CHANGE))

#define INVISIBILITY_FRAME_LENGTH		50

extern uint16_t g_unLenSeqBuff;
extern uint8_t g_lowLevelSeqBuff[];

typedef struct
{
	uint8_t	bandIdx;
	uint8_t	band;
} IBANDTOLLB;

typedef struct
{
	uint8_t	bandIdx;
	uint8_t	band2w;
	uint8_t	band4w;
} BANDTOLLB;

typedef struct
{
	const uint8_t	K : 4;
	const uint8_t	LM : 4;
} FREQ_COARSE_DATA;

const uint8_t sm_arrFreqCoarsePow[] =
{
	B8(10000000),
	B8(01100000),
	B8(01000000),
	B8(00100000),
	B8(00000000)
};

const FREQ_COARSE_DATA sm_arrFreqCoarseData[] =
{
	{ B8(10000000), B8(00000000) },
	{ B8(01000000), B8(10000000) },
	{ B8(11000000), B8(01000000) },
	{ B8(00100000), B8(11000000) },
	{ B8(10100000), B8(00100000) },
	{ B8(01100000), B8(10100000) },
	{ B8(11100000), B8(01100000) },
	{ B8(10110000), B8(11100000) },
	{ B8(01110000), B8(01110000) },
	{ B8(11110000), B8(11110000) },
};

/** **********************************************************************
 *
 * Low level program is responsible for internal device configuration.
 * It mapped to FUNX from "KALIBRAATORI JUHTIMISPROGRAMM VER2 23.03.14.sch.pdf" document
 * for example: FUN0 is LLProgFullStby and FUNC1 is LLProgDC10mV100V
 *
 * low level program may be loaded, unloaded and updated.
 * - load		executes all predefined steps to configure device modules and get required operation mode.
 * - unload		executes all predefined steps to shutdown device modules and get "full standby" mode.
 * - update		executes only modified lines of program which are can be updated after program load.
 *
 *********************************************************************** */
class LowLevelProgram
{
public:
	LowLevelProgram() : m_lStatusBits(0) {};
	virtual ~LowLevelProgram() {};

	virtual bool ApplyCommand(PCCOMMAND pCommand, bool bTestOnly);
	virtual PROG GetProgId() = 0;
	virtual void Load(bool bLoad);
	virtual const uint8_t* GetLowLevelSequence() const = 0;
	void CommitLowLevelSequence() { SetModified(PROG_STATUS_ALL, false); };

	virtual void Reset();

protected:
	virtual bool ApplyCommandInternal(PCCOMMAND pCommand, bool bTestOnly) = 0;
	virtual double GetStandByPwmValue() const { return HW_STBY_PWM;  };

	void SetModified(uint32_t lStatusBit, bool bModified = true)	{ m_lStatusBits = bModified ? (m_lStatusBits | lStatusBit) : (m_lStatusBits & ~lStatusBit); };
	bool GetModified(uint32_t lStatusBit) const						{ return (m_lStatusBits & lStatusBit) > 0; } ;

	void FormatByteSeq(uint32_t nCheckFlags, uint8_t unAddress, uint8_t byData) const;
	void FormatWordSeq(uint32_t unCheckFlags, uint8_t unAddress, uint16_t byData) const;
	void FormatDWordSeq(uint32_t unCheckFlags, uint8_t unAddress, uint32_t unData) const;
	void FormatWaitSeq(uint32_t unCheckFlags, uint32_t unDelay) const;
	void FormatPwmSeq(double dValue) const;
	void FormatBandSeq(uint32_t unCheckFlags, const BANDTOLLB* pBandsToLLB, uint8_t unBandIdx, STATESENSE enumSense, uint8_t unCS) const;
	void FormatBandPwmSeq(uint32_t unCheckFlags, MODE eMode, uint8_t unBandIdx, double dValue) const;
	void FormatBaseUnload() const;
	void FormatCurrentBandSeq(uint32_t unCheckFlags, const IBANDTOLLB* pBandsToLLB, uint8_t unBandIdx) const;
	void FormatFreqSeq(uint32_t unCheckFlags, double dFreq, PCBAND pBands, uint8_t bandIdxFreq) const;
	void FormatFreqSeqRegs(double dFreq, PCBAND pBand) const;
	void FormatWHInitSequence(uint32_t unCheckFlags) const;

protected:
	COMMAND m_command;

private:
	uint32_t m_lStatusBits;
};

#endif /* LOWLEVELPROGRAM_H_ */
