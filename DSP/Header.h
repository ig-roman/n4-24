
#pragma once

#ifdef __cplusplus
extern "C"
{
#endif
	__declspec(dllexport) char __stdcall CUI_WaitDisplayACKDlg();
	__declspec(dllexport) void __stdcall CUI_SendDisplayDataDlg(const char* pBuf, int length);
	__declspec(dllexport) void __stdcall CUI_GetTextExtentDlg(unsigned short* punX, unsigned short* punY);

	__declspec(dllexport) void __stdcall CUI_HandleCommandStringDlg(const char* pBuf, unsigned int length);

	__declspec(dllexport) void __stdcall CARM_SetPWMValueDlg(unsigned long unPwm);
	__declspec(dllexport) void __stdcall CARM_SPI_StopDlg();
	__declspec(dllexport) void __stdcall CARM_SPI_SendDlg(char byByte);
	__declspec(dllexport) void __stdcall CARM_SPI_StartDlg(unsigned char unAddr);

	__declspec(dllexport) bool __stdcall CARM_DelayMsDlg(unsigned long unDelay, bool bSkipLoop, bool bCheckOverload);

#ifdef __cplusplus
}
#endif