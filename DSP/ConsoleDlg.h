#pragma once


// ConsoleDlg dialog

class ConsoleDlg : public CDialog
{
	DECLARE_DYNAMIC(ConsoleDlg)

public:
	ConsoleDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~ConsoleDlg();

// Dialog Data
	enum { IDD = IDD_CONSOLE };

	void Log_DelayMs(unsigned long unDelay);
	void Log_SetPWMValue(unsigned long unPwm);
	void Log_SPI_Stop();
	void Log_SPI_Send(char byByte);
	void Log_SPI_Start(unsigned char unAddr);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
private:
	void AppendText(LPCSTR pszText);
public:
	afx_msg void OnBnClickedBtnSend();

private:
	UCHAR m_unSPAddr;
	int m_nSPLength;
	char m_arrSpiData[255];
public:
	afx_msg void OnBnClickedBtnConnect();
};
