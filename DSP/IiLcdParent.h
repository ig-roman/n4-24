#pragma once

class CDC;

class IiLcdParent
{
public:
	virtual CDC* GetDc() = 0;
	virtual void SetInvalid() = 0;
	virtual void SendDispRetData(const char * buff, int length) = 0;
};

