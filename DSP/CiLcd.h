#pragma once

#include "stdafx.h"
#include "IiLcdParent.h"
#include <vector>

#define AL_CENTER_TEXT_HORIZONTALLY			0x01
#define AL_CENTER_TEXT_VERTICALLY			0x02
#define AL_RIGHT_JUSTIFY_TEXT				0x04
#define AL_BOTTOM_JUSTIFY_TEXT				0x08 
#define AL_DO_NOT_WORD_WRAP_TEXT			0x10 
#define AL_ADD_HORIZONTAL_SPACE_FOR_BORDER	0x20
#define AL_ADD_VERTICAL_SPACE_FOR_BORDER	0x40
#define AL_TURN_ALIGNMENT_ON				0x80

#define DR_WITH_ROUNDED_CORNERS				0x01
#define DR_WITH_SHADOW						0x02
#define DR_BLANK_PIXELS_AROUND				0x04
#define DR_INNER_FILLED_BACKGROUND_COLOR	0x08
#define DR_INNER_DRAWN_FOREGROUND_COLOR		0x10
#define DR_FILLED_WITH_CURRENT_FILL_PATTERN	0x20
#define DR_DRAW_WITHOUT_FRAME				0x40

typedef std::vector<char> TCmdArray;

class CiLcd
{
public:
	CiLcd(void);
	virtual ~CiLcd(void);

	UINT HandleDisplayData(const char* pBuf, int length);
	void SendTextExtent(CSize& strSize);
	void HandleTouchEvent(CPoint point, bool bPressed);

private:
	void DrawTextEEX(CDC* pdc, const LPCWSTR pszText, CRect& rcRect, UINT unAlFlags);

public:
	IiLcdParent* m_pParent;
	CRect rect;
	CRect port;
	DWORD color;
	DWORD textColor;
	UINT unAlFlags;

	HFONT hFont1;
	HFONT hFont2;
	HFONT hFont3;
	HFONT hFontActive;
};
