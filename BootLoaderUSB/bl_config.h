//*****************************************************************************
//
// bl_config.h - The configurable parameters of the boot loader.
//
// Copyright (c) 2012-2014 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
// 
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
// 
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
// 
// This is part of revision 2.1.0.12573 of the DK-TM4C123G Firmware Package.
//
//*****************************************************************************

#ifndef __BL_CONFIG_H__
#define __BL_CONFIG_H__

//*****************************************************************************
//
// The following defines are used to configure the operation of the boot
// loader.  For each define, its interactions with other defines are described.
// First is the dependencies (in other words, the defines that must also be
// defined if it is defined), next are the exclusives (in other words, the
// defines that can not be defined if it is defined), and finally are the
// requirements (in other words, the defines that must be defined if it is
// defined).
//
// The following defines must be defined in order for the boot loader to
// operate:
//
//     One of CAN_ENABLE_UPDATE, ENET_ENABLE_UPDATE, I2C_ENABLE_UPDATE,
//            SSI_ENABLE_UPDATE, UART_ENABLE_UPDATE, or USB_ENABLE_UPDATE
//     APP_START_ADDRESS
//     VTABLE_START_ADDRESS
//     FLASH_PAGE_SIZE
//     STACK_SIZE
//
//*****************************************************************************

//*****************************************************************************
//
// The frequency of the crystal used to clock the microcontroller.
//
// This defines the crystal frequency used by the microcontroller running the
// boot loader.  If this is unknown at the time of production, then use the
// UART_AUTOBAUD feature to properly configure the UART.
//
// Depends on: None
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define CRYSTAL_FREQ            16000000

//*****************************************************************************
//
// The starting address of the application.  This must be a multiple of 1024
// bytes (making it aligned to a page boundary).  A vector table is expected at
// this location, and the perceived validity of the vector table (stack located
// in SRAM, reset vector located in flash) is used as an indication of the
// validity of the application image.
//
// The flash image of the boot loader must not be larger than this value.
//
// Depends on: None
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define APP_START_ADDRESS       0x1000 // 2800

//*****************************************************************************
//
// The address at which the application locates its exception vector table.
// This must be a multiple of 1024 bytes (making it aligned to a page
// boundary).  Typically, an application will start with its vector table and
// this value should be set to APP_START_ADDRESS.  This option is provided to
// cater for applications which run from external memory which may not be
// accessible by the NVIC (the vector table offset register is only 30 bits
// long).
//
// Depends on: None
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define VTABLE_START_ADDRESS    0x1000 //2800

//*****************************************************************************
//
// The size of a single, erasable page in the flash.  This must be a power
// of 2.
//
// Depends on: None
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define FLASH_PAGE_SIZE         0x00000400

//*****************************************************************************
//
// The number of words of stack space to reserve for the boot loader.
//
// Depends on: None
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define STACK_SIZE              128

//*****************************************************************************
//
// The number of words in the data buffer used for receiving packets.  This
// value must be at least 3.  If using auto-baud on the UART, this must be at
// least 20.  The maximum usable value is 65 (larger values will result in
// unused space in the buffer).
//
// Depends on: None
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define BUFFER_SIZE             20

//*****************************************************************************
//
// Enables updates to the boot loader.  Updating the boot loader is an unsafe
// operation since it is not fully fault tolerant (losing power to the device
// part way though could result in the boot loader no longer being present in
// flash).
//
// Depends on: None
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
//#define ENABLE_BL_UPDATE

//*****************************************************************************
//
// This definition will cause the the boot loader to erase the entire flash on
// updates to the boot loader or to erase the entire application area when the
// application is updated.  This erases any unused sections in the flash before
// the firmware is updated.
//
// Depends on: None
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
//#define FLASH_CODE_PROTECTION

//*****************************************************************************
//
// Enables the pin-based forced update check.  When enabled, the boot loader
// will go into update mode instead of calling the application if a pin is read
// at a particular polarity, forcing an update operation.  In either case, the
// application is still able to return control to the boot loader in order to
// start an update.  For applications which need to perform more complex
// checking than is possible using a single GPIO, a hook function may be
// provided using BL_CHECK_UPDATE_FN_HOOK instead.
//
// Depends on: None
// Exclusive of: None
// Requires: FORCED_UPDATE_PERIPH, FORCED_UPDATE_PORT, FORCED_UPDATE_PIN,
//           FORCED_UPDATE_POLARITY
//
//*****************************************************************************
#define ENABLE_UPDATE_CHECK

//*****************************************************************************
//
// The GPIO module to enable in order to check for a forced update.  This will
// be one of the SYSCTL_RCGC2_GPIOx values, where "x" is replaced with the port
// name (such as B).  The value of "x" should match the value of "x" for
// FORCED_UPDATE_PORT.
//
// Depends on: ENABLE_UPDATE_CHECK
// Exclusive of: None
// Requries: None
//
//*****************************************************************************
#define FORCED_UPDATE_PERIPH    SYSCTL_RCGC2_GPIOD

//*****************************************************************************
//
// The GPIO port to check for a forced update.  This will be one of the
// GPIO_PORTx_BASE values, where "x" is replaced with the port name (such as
// B).  The value of "x" should match the value of "x" for
// FORCED_UPDATE_PERIPH.
//
// Depends on: ENABLE_UPDATE_CHECK
// Exclusive of: None
// Requries: None
//
//*****************************************************************************
#define FORCED_UPDATE_PORT      GPIO_PORTD_BASE

//*****************************************************************************
//
// The pin to check for a forced update.  This is a value between 0 and 7.
//
// Depends on: ENABLE_UPDATE_CHECK
// Exclusive of: None
// Requries: None
//
//*****************************************************************************
#define FORCED_UPDATE_PIN       7

//*****************************************************************************
//
// The polarity of the GPIO pin that results in a forced update.  This value
// should be 0 if the pin should be low and 1 if the pin should be high.
//
// Depends on: ENABLE_UPDATE_CHECK
// Exclusive of: None
// Requries: None
//
//*****************************************************************************
#define FORCED_UPDATE_POLARITY  0

//*****************************************************************************
//
// This enables the use of the GPIO_LOCK mechanism for configuration of
// protected GPIO pins (for example JTAG pins).  If this value is not defined,
// the locking mechanism will not be used.  The only legal values for this
// feature are GPIO_LOCK_KEY for Fury devices and GPIO_LOCK_KEY_DD for all
// other devices except Sandstorm devices, which do not support this feature.
//
// Depends on: ENABLE_UPDATE_CHECK
// Exclusive of: None
// Requries: None
//
//*****************************************************************************
#define FORCED_UPDATE_KEY       GPIO_LOCK_KEY
//#define FORCED_UPDATE_KEY       GPIO_LOCK_KEY_DD

//*****************************************************************************
//
// Selects USB update via Device Firmware Update class.
//
// Depends on: None
// Exclusive of: CAN_ENABLE_UPDATE, ENET_ENABLE_UPDATE, I2C_ENABLE_UPDATE,
//               SSI_ENABLE_UPDATE, UART_ENABLE_UPDATE,
// Requires: CRYSTAL_FREQ, USB_VENDOR_ID, USB_PRODUCT_ID, USB_DEVICE_ID,
//           USB_MAX_POWER
//
//*****************************************************************************
#define USB_ENABLE_UPDATE

//*****************************************************************************
//
// The USB vendor ID published by the DFU device.  This value is the TI
// Tiva vendor ID.  Change this to the vendor ID you have been assigned by the
// USB-IF.
//
// Depends on: USB_ENABLE_UPDATE
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define USB_VENDOR_ID           0x1cbe

//*****************************************************************************
//
// The USB device ID published by the DFU device.  If you are using your own
// vendor ID, chose a device ID that is different from the ID you use in
// non-update operation.  If you have sublicensed TI's vendor ID, you must
// use an assigned product ID here.
//
// Depends on: USB_ENABLE_UPDATE
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define USB_PRODUCT_ID          0x00FF

//*****************************************************************************
//
// Selects the BCD USB device release number published in the device
// descriptor.
//
// Depends on: USB_ENABLE_UPDATE
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define USB_DEVICE_ID           0x0001

//*****************************************************************************
//
// Sets the maximum power consumption that the DFU device will report to the
// USB host in the configuration descriptor.  Units are milliamps.
//
// Depends on: USB_ENABLE_UPDATE
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define USB_MAX_POWER           150

//*****************************************************************************
//
// Specifies whether the DFU device reports to the host that it is self-powered
// (defined as 0) or bus-powered (defined as 1).
//
// Depends on: USB_ENABLE_UPDATE
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define USB_BUS_POWERED         1

//*****************************************************************************
//
// Specifies whether the target board uses a multiplexer to select between USB
// host and device modes.
//
// Depends on: USB_ENABLE_UPDATE
// Exclusive of: None
// Requires: USB_MUX_PERIPH, USB_MUX_PORT, USB_MUX_PIN, USB_MUX_DEVICE
//
//*****************************************************************************
//#define USB_HAS_MUX

//*****************************************************************************
//
// Specifies the GPIO peripheral containing the pin which is used to select
// between USB host and device modes.  The value is of the form
// SYSCTL_RCGC2_GPIOx, where GPIOx represents the required GPIO port.
//
// Depends on: USB_ENABLE_UPDATE, USB_HAS_MUX
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
//#define USB_MUX_PERIPH          SYSCTL_RCGC2_GPIOH

//*****************************************************************************
//
// Specifies the GPIO port containing the pin which is used to select between
// USB host and device modes.  The value is of the form GPIO_PORTx_BASE, where
// PORTx represents the required GPIO port.
//
// Depends on: USB_ENABLE_UPDATE, USB_HAS_MUX
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
//#define USB_MUX_PORT            GPIO_PORTH_BASE

//*****************************************************************************
//
// Specifies the GPIO pin number used to select between USB host and device
// modes.  Valid values are 0 through 7.
//
// Depends on: USB_ENABLE_UPDATE, USB_HAS_MUX
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
//#define USB_MUX_PIN             2

//*****************************************************************************
//
// Specifies the state of the GPIO pin required to select USB device-mode
// operation.  Valid values are 0 (low) or 1 (high).
//
// Depends on: USB_ENABLE_UPDATE, USB_HAS_MUX
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
//#define USB_MUX_DEVICE          1

//*****************************************************************************
//
// Specifies whether the target board requires configuration of the pin used
// for VBUS.  This applies to Blizzard class and later devices.
//
// Depends on: USB_ENABLE_UPDATE
// Exclusive of: None
// Requires: USB_VBUS_PERIPH, USB_VBUS_PORT, USB_VBUS_PIN
//
//*****************************************************************************
//#define USB_VBUS_CONFIG

//*****************************************************************************
//
// Specifies the GPIO peripheral containing the pin which is used for VBUS.
// The value is of the form SYSCTL_RCGCGPIO_Rx, where the Rx represents
// the required GPIO port.  This applies to Blizzard class and later 
// devices.
//
// Depends on: USB_ENABLE_UPDATE, USB_VBUS_CONFIG
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define USB_VBUS_PERIPH          SYSCTL_RCGCGPIO_R1

//*****************************************************************************
//
// Specifies the GPIO port containing the pin which is used for VBUS.  The value
// is of the form GPIO_PORTx_BASE, where PORTx represents the required GPIO
// port.
//
// Depends on: USB_ENABLE_UPDATE, USB_VBUS_CONFIG
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define USB_VBUS_PORT            GPIO_PORTB_BASE

//*****************************************************************************
//
// Specifies the GPIO pin number used for VBUS.  Valid values are 0 through 7.
//
// Depends on: USB_ENABLE_UPDATE, USB_VBUS_CONFIG
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define USB_VBUS_PIN             1

//*****************************************************************************
//
// Specifies whether the target board requires configuration of the pin used
// for ID.  This applies to Blizzard class and later devices.
//
// Depends on: USB_ENABLE_UPDATE
// Exclusive of: None
// Requires: USB_ID_PERIPH, USB_ID_PORT, USB_ID_PIN
//
//*****************************************************************************
//#define USB_ID_CONFIG

//*****************************************************************************
//
// Specifies the GPIO peripheral containing the pin which is used for ID.
// The value is of the form SYSCTL_RCGCGPIO_Rx, where the Rx represents
// the required GPIO port.  This applies to Blizzard class and later 
// devices.
//
// Depends on: USB_ENABLE_UPDATE, USB_ID_CONFIG
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
//#define USB_ID_PERIPH          SYSCTL_RCGCGPIO_R3

//*****************************************************************************
//
// Specifies the GPIO port containing the pin which is used for ID.  The value
// is of the form GPIO_PORTx_BASE, where PORTx represents the required GPIO
// port.
//
// Depends on: USB_ENABLE_UPDATE, USB_ID_CONFIG
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
//#define USB_ID_PORT            GPIO_PORTD_BASE

//*****************************************************************************
//
// Specifies the GPIO pin number used for ID.  Valid values are 0 through 7.
//
// Depends on: USB_ENABLE_UPDATE, USB_ID_CONFIG
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
//#define USB_ID_PIN             7

//*****************************************************************************
//
// Specifies whether the target board requires configuration of the pin used
// for DP.  This applies to Blizzard class and later devices.
//
// Depends on: USB_ENABLE_UPDATE
// Exclusive of: None
// Requires: USB_DP_PERIPH, USB_DP_PORT, USB_DP_PIN
//
//*****************************************************************************
#define USB_DP_CONFIG

//*****************************************************************************
//
// Specifies the GPIO peripheral containing the pin which is used for DP.
// The value is of the form SYSCTL_RCGCGPIO_Rx, where the Rx represents
// the required GPIO port.  This applies to Blizzard class and later 
// devices.
//
// Depends on: USB_ENABLE_UPDATE, USB_DP_CONFIG
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define USB_DP_PERIPH          SYSCTL_RCGCGPIO_R3

//*****************************************************************************
//
// Specifies the GPIO port containing the pin which is used for DP.  The value
// is of the form GPIO_PORTx_BASE, where PORTx represents the required GPIO
// port.
//
// Depends on: USB_ENABLE_UPDATE, USB_DP_CONFIG
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define USB_DP_PORT            GPIO_PORTD_BASE

//*****************************************************************************
//
// Specifies the GPIO pin number used for DP.  Valid values are 0 through 7.
//
// Depends on: USB_ENABLE_UPDATE, USB_DP_CONFIG
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define USB_DP_PIN             5

//*****************************************************************************
//
// Specifies whether the target board requires configuration of the pin used
// for DM.  This applies to Blizzard class and later devices.
//
// Depends on: USB_ENABLE_UPDATE
// Exclusive of: None
// Requires: USB_DM_PERIPH, USB_DM_PORT, USB_DM_PIN
//
//*****************************************************************************
#define USB_DM_CONFIG

//*****************************************************************************
//
// Specifies the GPIO peripheral containing the pin which is used for DM.
// The value is of the form SYSCTL_RCGCGPIO_Rx, where the Rx represents
// the required GPIO port.  This applies to Blizzard class and later 
// devices.
//
// Depends on: USB_ENABLE_UPDATE, USB_DM_CONFIG
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define USB_DM_PERIPH          SYSCTL_RCGCGPIO_R3

//*****************************************************************************
//
// Specifies the GPIO port containing the pin which is used for DM.  The value
// is of the form GPIO_PORTx_BASE, where PORTx represents the required GPIO
// port.
//
// Depends on: USB_ENABLE_UPDATE, USB_DM_CONFIG
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define USB_DM_PORT            GPIO_PORTD_BASE

//*****************************************************************************
//
// Specifies the GPIO pin number used for DM.  Valid values are 0 through 7.
//
// Depends on: USB_ENABLE_UPDATE, USB_DM_CONFIG
// Exclusive of: None
// Requires: None
//
//*****************************************************************************
#define USB_DM_PIN             4

#endif // __BL_CONFIG_H__
