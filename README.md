Source code for calibrator **N4-24**

Includes:

* Digital part for Tiva MCU
* Analogue part for Atmel MCU
* Common device simulator for Visual Studio
* Project for iLCD display
* Boot-loader for Tiva MCU

